<?php if($this->user_permissions->is_view('t_bankrec')){ ?>

<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

    $(function() {
        $("#from_date,#to_date,#date").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });   

</script>

<style>
    
    .tblbankrec {
        max-width:980px;
        table-layout:fixed;
        margin:auto;
    }
    
    .tblbankrec th, td {
        padding:5px 10px;        
    }
    
    .tblbankrec thead, tfoot {
        background:#f9f9f9;
        display:table;
        width:100%;
        width:calc(100% - 18px);
    }
    
    .tblbankrec tbody {
        height:300px;
        overflow:auto;
        overflow-x:hidden;
        display:block;
        width:100%;
    }
    
    .tblbankrec tbody tr {
        display:table;
        width:100%;
        table-layout:fixed;
    }

</style>


<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:170px;float:left"><span>Bank Reconciliation</span></div>     
    </div><br>

    <form method="post" action="<?=base_url()?>index.php/main/save/t_bankrec_n" id="form_">

        <table border="0" align="center" cellpadding="0" cellspacing="0" width="1000" class="tbl_reconci" borderColor="#ccc">
    
            <tr>
                <td width="70">From Date</td>
                <td width="200"><input type="text" id="from_date" name="from_date" class="input_text_regular" readonly="readonly"  value="2017-04-05"></td>
                <td width="60">To Date</td>
                <td width="200"><input type="text" id="to_date" name="to_date" class="input_text_regular" readonly="readonly" value="2017-04-30"></td>
                <td></td>
                <td width="100" align="right">No</td>
                <td width="100">    <input type="text" id="no" name="no" value="<?=$no?>" class="input_text_regular">
                                    <input type="hidden" id="code_" name="code_" value="0" ></td>
            </tr>

            <tr>
                <td width="70">Account</td>
                <td colspan="3">    <input type="text" class="input_text_regular" id="account_desc" value="30300218">
                                    <input type="hidden" id="acc_code" name="acc_code" value="30300218"></td>
                <td><input type="button" value="Load" class="btn_regular" id="btnLoad"></td>                
                <td width="100" align="right">Date</td>
                <td width="100"><input type="text" id="date" name="date" value="<?=date('Y-m-d')?>" readonly="readonly" class="input_text_regular"></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                                
                <td align="right" colspan="3"> Opening Balance <input type="text" style="font-weight:bold;width: 120px;text-align: right; " class="input_text_regular amount" id="op_bal" name="op_bal" ></td>
            </tr>

            <tr>
                <td colspan="7">
                    
                    <table width="100%" border="01" class="tblbankrec">
                        <thead>
                            <tr bgcolor="#eaeaea">
                                <th style="font-size: 14px">Date</th>
                                <th style="font-size: 14px">Transaction</th>
                                <th style="font-size: 14px">Trans No</th>
                                <th style="font-size: 14px">Description</th>
                                <th style="font-size: 14px">Dr</th>
                                <th style="font-size: 14px">Cr</th>
                                <th style="font-size: 14px">Reconcile <input type="checkbox" class="chk_all"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7">No data</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr bgcolor="#eaeaea">
                                <td></td>
                                <td></td>
                                <td align="right" style="font-weight:bold;font-size: 14px"></td>
                                <td style="font-weight:bold;font-size: 14px" align="Right" class="row_count">Total</td>
                                <td style="font-weight:bold;font-size: 14px" align="right" class="f_dr_tot">0.00</td>
                                <td style="font-weight:bold;font-size: 14px" align="right" class="f_cr_tot">0.00</td>
                                <td></td>
                            </tr>
                            <tr style="color:#ffffff;background-color:#666666">
                                <td></td>                                
                                <td align="right" colspan="2" style="font-size: 14px;font-weight: bold;"></td>
                                <td style="font-size: 14px;font-weight: bold;" align="Right" class="rec_row_count">Reconcile Total</td>
                                <td style="font-size: 14px;font-weight: bold;" align="right" class="rec_f_dr_tot">0.00</td>
                                <td style="font-size: 14px;font-weight: bold;" align="right" class="rec_f_cr_tot">0.00</td>
                                <td></td>
                            </tr>
                            <tr style="color:#ffffff;background-color:#666666">
                                <td></td>                                
                                <td align="right" colspan="2" style="font-size: 14px;font-weight: bold;"></td>
                                <td style="font-size: 14px;font-weight: bold;" align="Right" class="bal_row_count"></td>
                                <td style="font-size: 14px;font-weight: bold;" align="right" class="bal_f_dr_tot">0.00</td>
                                <td style="font-size: 14px;font-weight: bold;" align="right" class="bal_f_cr_tot">0.00</td>
                                <td></td>
                            </tr>
                        </tfoot>

                    </table>

                </td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                                
                <td align="right" colspan="3"> Closing Balance <input type="text" style="font-weight:bold;width: 120px;text-align: right; " class="input_text_regular" id="closing_bal" name="closing_bal" readonly="readonly"></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                                
                <td align="right" colspan="3"> Statement Balance <input type="text" style="font-weight:bold;width: 120px;text-align: right; " class="input_text_regular" id="statment_closing_bal" name="statment_closing_bal"></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                                
                <td align="right" colspan="3"> Different <input type="text" style="font-weight:bold;width: 120px;text-align: right; " class="input_text_regular" id="bal_diff" readonly="readonly"></td>
            </tr>


            <tr>
                <td><input type="button" value="Reconcile Now" style="width:180px" class="btn_regular" id="btnSave"></td>
                <td><input type="button" value="Print" class="btn_regular_disable" id="btnPrint" disabled="disabled">
                    <input type="button" value="Reset" class="btn_regular_disable" id="btnReset" disabled="disabled"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

        </table>

        <input type="hidden" id="total_dr" name="total_dr">
        <input type="hidden" id="total_cr" name="total_cr">
        <input type="hidden" id="reconcil_dr" name="reconcil_dr">
        <input type="hidden" id="reconcil_cr" name="reconcil_cr">
        <input type="hidden" id="grid_row_count" value="0">

    </form>

    <form id="print_rec_report" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <input type="hidden" name='by' value='t_bankrec_n'>
        <input type="hidden" name='no' id='rec_no'>        
    </form>

</div>


<?php } ?>