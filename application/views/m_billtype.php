<div class="page_contain">
	<div class="page_main_title"><span>Bill Types</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_billtype" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add New Bill Type</b>
				</td>
				<td>
					<b>Exist Bill Types</b>
				</td>			

			</tr>			
			
			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Code</span> 
						<input class="input_text_regular" type="text" name="code" id="code" value="<?=$bc_max?>" readonly="readonly" maxlength="10">
					</div>
				</td>
				<td rowspan="6">

					<div class="exits_text_box_holder">
						<div class="text_box_title_holder" style="width:40px;border:0px solid red">Code</div> 
						<input class="nic_feild input_text_regular" style="width:194px" type="text" id="Search_billtype">
						<input type="button" value="Search"	name="btnSearch" id="btnSearch" class="btn_regular">					
					</div>
					
					<div class="list_div">
						
					</div>

				</td>							
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Description</span> 
						<input class="input_text_regular" type="text" name="description" id="description" maxlength="50">
					</div>
				</td>							
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Amount From</span> 
						<input class="currency amount input_text_regular" type="text" name="amount_from" id="amount_from" maxlength="13">
					</div>
				</td>							
			</tr>
			
			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Amount To</span> 
						<input class="currency amount input_text_regular" type="text" name="amount_to" id="amount_to" maxlength="13">
					</div>
				</td>			
			</tr>			

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Period</span> 
						<input class="contct_no_feild input_text_regular NumOnly" type="text" name="period" id="period" maxlength="11">
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Interest Rate</span> 
						<input class="input_text_regular FloatVal" type="text" name="int_rate" id="int_rate" maxlength="6">
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<label><input type="checkbox" name="inactive" id="inactive"><span>Inactive</span></label> &nbsp;&nbsp;&nbsp;&nbsp;
						<label><input type="checkbox" name="tax" id="tax"><span>Tax</span></label>
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					
				</td>			
			</tr>

			<tr>
				<td>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>							
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>