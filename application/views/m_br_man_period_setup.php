<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

	$(function() {
		$( ".ddd" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2025",
			onSelect: function(date){
				
			}
		});
	});

</script>

<div class="page_contain">
<div class="page_main_title"><span>BC Manager Period Setup</span></div>	
	<?=$data?>
</div>
</body>
</html>