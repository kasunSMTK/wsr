<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>
  $(function() {
    $( "#ddate" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      //minDate: 0,
      yearRange: "1930:2050",
      onSelect: function(date){
        change_current_date(date);        
      } 
    });
  });


  $(function() {
    $( "#from_date,#to_date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,      
      yearRange: "1930:2050",
      onSelect: function(date){
        grid_data();
      } 
    });
  });

  function date_change_response_call(){
    $("#btnReset").click();
  }

</script>

<style type="text/css">

  #ffTb tbody {
  	font-family: "Roboto";
  	font-size: 13px;
  }
  .bd {
  	border-bottom: 1px dotted #ccc;
  	line-height: 10px !important;
  }
  .SlalMn {
  	background-color: #eee;
  }
  #ffTb {
  	padding-bottom: 10px;
  	overflow: hidden;
  }
  
  #ffTb thead {
  	line-height: 10px !important;
  	border: 1px solid #ccc;
  	background-color: #ffffff;
  	color: #000000;
  }

  #ffTb tbody tr:hover {
  	background-color: #eaeaea;
  }

  #GrDt tr:hover{    
    color: #000000;
  }

  #ffbtn {
  	bottom: 0px;
  	width: 100%;
  	border-top: 1px solid #eaeaea;
  	padding: 10px;
  	background-color: #FCFCFC;
  	position: fixed;
  }
  input[type='checkbox'] {
  	padding: 0px;
  	margin: 0px;
  }
  .fixed {
  	position: fixed;
  	top: 0;
  	font-family: "bitter";
  	font-size: 14px;
  	background-color: #ffffff;
  	color: #000000;
    border-bottom: 1px solid #000000;
    margin-left: -1px;
  }
  .bcred {

  	background-color: red;
  	color: #ffffff;
  	border-radius: 0px;
  	-moz-animation-name: blinker;
  	-moz-animation-duration: .5s;
  	-moz-animation-timing-function: linear;
  	-moz-animation-iteration-count: infinite;
  }
   @-moz-keyframes blinker {
     0% {
      opacity: 1.0;
    }
     50% {
      opacity: 0.0;
    }

  }

</style>
<div class="page_contain_new_pawn">
  <div class="page_main_title_new_pawn">
      
      <div style="width:240px;float:left"><span>Forfeit</span></div>   
      <div style="float:left;float:right">

        <input type="text" class="input_text_regular_new_pawn" style="border:1px solid Green;width:130px" id="no" name="no" value="<?=$max_no?>">
        <input type="text" class="input_text_regular_new_pawn" style="width:80px" id="ddate" value="<?=$current_date?>">

      </div>    

  </div>
  <div  class="" align="center">

    <form action="<?=base_url()?>index.php/main/load_data/t_forfeit/grid_data" method="post" id="form_2">
    
      <div class="rpt_critaria_bar">
          <br>
          <table border="0" cellpassing="0" cellspacing="0">
            <tr>
              <!-- <td colspan="2" width="200">
                From Date<div style="padding-top:4px"></div>
                <input name="from_date" id="from_date" class="input_text_regular_new_pawn" style="border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
              </td> -->
              <td colspan="1">
                Date<div style="padding-top:4px"></div>
                <input name="from_date" id="from_date" type="hidden" value="<?=date('Y-m-d')?>">
                <input name="to_date" id="to_date" class="input_text_regular_new_pawn" style="border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
              </td>
              <td style='padding-left:25px'>Branch<div style="padding-top:4px"></div><?=$bc_dropdown?></td>
              <td style='padding-left:25px'>Bill Type<div style="padding-top:4px"></div><?=$billtype_dropdown?></td>
              <td style='padding-left:25px'><br>
                <input type="button" id="btnSearch" src="about:blank" class="btn_regular" value="Search" style="width:184px;font-family:'bitter'">
              </td>              
            </tr>
            <tr style='height:30px'>
              <td style='width:150px'><span style='background-color: #54d0da;'>INTEREST PAID BILL<span></td>     
              <td><span style='background-color: #e1bcdb;'>POSTPONED BILL<span></td>
            </tr>
            
            <tr>
              <td colspan="4" style="padding-top:12px"></td>
            </tr>
          </table><br>

      </div>

    </form>

    <form action="<?=base_url()?>index.php/main/save/t_forfeit" method="post" id="form_">

      <table id="ffTb" class="" border="0" cellpadding="0" cellspacing="0">
        <thead class="">
          <tr width="100%">            
            











            <th width="74" class="text_box_holder_new_pawn ">Bill Type</th>
            <th width="275" class="text_box_holder_new_pawn ">Customer</th>            
            <th width="110" class="text_box_holder_new_pawn ">Bill No.</th>
            <th width="80" class="text_box_holder_new_pawn ">Date</th>
            <th width="85" class="text_box_holder_new_pawn ">Final Date</th>
            <th width="60" class="text_box_holder_new_pawn ">Period</th>
            <th width="75" class="text_box_holder_new_pawn ">T.Weight</th>
            <th width="90" class="text_box_holder_new_pawn ">Gold Value</th>

            <th width="90" class="text_box_holder_new_pawn ">T.Advance</th>
            <th width="115" class="text_box_holder_new_pawn ">Pawn Amount</th>
    <th width="115" class="text_box_holder_new_pawn ">Actual Amount</th>

            <th width="80" class="text_box_holder_new_pawn " style="padding-bottom: 14px">Action 











































            <input type='checkbox' id='selAll' /></th>
          </tr>
        </thead>
        <!-- <tbody id="GrDt"><?=$grid_data?></tbody> -->

        <tbody id="GrDt">
          
        </tbody>

      </table>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
    </form>
  </div>
  <div id="ffbtn">
    <input name="forfit" type="button" value="Forfiet" id="btnForfiet" class="btn_regular"/>

    <input type="button" value="Reset" id="btnReset" class="btn_regular_disable" disabled />

    <div style="float:right; padding:10px;" id="MsgAnLnt">
      <span id="msgMax" style="display:none;width: 300px">Max 250 </span>    
      <span id="chkLnth">0/250</span>
    </div>
  </div>
</div>
</body></html>