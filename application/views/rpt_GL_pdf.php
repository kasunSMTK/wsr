<?php
	
	/*echo var_dump($det);
	exit;*/
		
	ini_set('memory_limit', '512M');
	ini_set('max_execution_time',600);

	$process_start_time = time();

	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetAutoPageBreak(TRUE, 0);
	

	$sacc = $sbc = '';	
	$bc_tot_DR = $bc_tot_CR = 0;
	$first_bc_passed = false;
	$next_acc_reached = false;
	$bc_bal = 0;
	$acc_with_OP_bal = array(3,4,5);

	$acc_tot_dr = $acc_tot_cr = $acc_tot_bal = 0;

	$tot_bc_acc_dr_bal = $tot_bc_acc_cr_bal = 0;
	$tot_bc_acc_bal = 0;

	foreach ($det as $r) {

		$h = 4 * (	max(1,$this->pdf->getNumLines($r->description,50)) );

		if ($sacc != $r->acc_code){
			
			if ($first_bc_passed){
				
				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(50, $h, 'Period Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h, d($bc_tot_DR), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h, d($bc_tot_CR), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h, d($bc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 8);

				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->MultiCell(0, 0, '', 'T','L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(50, $h, 'Account Period Total and Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h,d($acc_tot_dr), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h,d($acc_tot_cr), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h,d($acc_tot_bal), '0','R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0, 0, '', 'B','L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 8);

				$bc_tot_DR = $bc_tot_CR = 0;

				$acc_tot_bal = $acc_tot_dr = $acc_tot_cr = 0;
			}

			$this->pdf->AddPage();
			$this->pdf->ln(2);
			$this->pdf->SetFont('helvetica', 'B', 20);
			$this->pdf->MultiCell(0, $h, $r->acc_code.' - '.$r->acc_desc, '0','L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->SetFont('helvetica', '', 8);			
			$sacc = $r->acc_code;

			$next_acc_reached = true;

		}


		if ($sbc != $r->branch_name){



			if ($next_acc_reached){
				$next_acc_reached = false;
			}else{

				if ($first_bc_passed){
					$this->pdf->SetFont('helvetica', 'B', 8);
					$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(50, $h, 'Period Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(27, $h, d($bc_tot_DR), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(27, $h, d($bc_tot_CR), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(27, $h, d($bc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->SetFont('helvetica', '', 8);

					$bc_tot_DR = $bc_tot_CR = 0;
				}

				if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){

					$this->pdf->SetFont('helvetica', 'B', 8);
					$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(50, $h, 'Closing Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(27, $h, '', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(27, $h, '', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(27, $h, d($bc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->SetFont('helvetica', '', 8);

				}


			}

			// set branch opening balance for each account			
				if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){
					$bc_bal = $bc_acc_op_bal[$r->acc_code][$r->bc];
					// $bc_bal = 36720500.00;

					$tot_bc_acc_bal += $bc_bal;
					$tot_bc_acc_dr_bal += $bc_bal;
					$acc_tot_bal += $bc_bal;

				}else{
					$bc_bal = 0;
				}
			// End set branch opening balance for each account

			$this->pdf->ln(2);
			$this->pdf->SetFont('helvetica', 'B', 10);
			$this->pdf->MultiCell(0, $h, $r->branch_name, '0','L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->ln(2);
			$sbc = $r->branch_name;


			if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){

				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(50, $h, 'Opening Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h, '', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h, '', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(27, $h, d($bc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 8);

			}



			$this->pdf->SetFont('helvetica', 'B', 8);
			$this->pdf->MultiCell(18, $h, 'Date', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(18, $h, 'Trans No', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(50, $h, 'Description', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(27, $h, 'Dr', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(27, $h, 'Cr', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(27, $h, 'Balance', '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->SetFont('helvetica', '', 8);

			$first_bc_passed = true;

		}

		$bc_bal += $r->dr_amount;
		$bc_bal -= $r->cr_amount;

		$tot_bc_acc_bal += $r->dr_amount;
		$tot_bc_acc_bal -= $r->cr_amount;

		$this->pdf->MultiCell(18, $h, $r->ddate, '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(18, $h, $r->trans_no, '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(50, $h, $r->description, '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(27, $h, d($r->dr_amount), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(27, $h, d($r->cr_amount), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(27, $h, d($bc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		
		$bc_tot_DR += $r->dr_amount;
		$bc_tot_CR += $r->cr_amount;

		$acc_tot_dr += $r->dr_amount;
		$acc_tot_cr += $r->cr_amount;

		$acc_tot_bal += $r->dr_amount;
		$acc_tot_bal -= $r->cr_amount;

		$tot_bc_acc_dr_bal += $r->dr_amount;
		$tot_bc_acc_cr_bal -= $r->cr_amount;

	}

	$this->pdf->SetFont('helvetica', 'B', 8);
	$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(50, $h, 'Period Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h, d($bc_tot_DR), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h, d($bc_tot_CR), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h, d($bc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 8);

	$bc_tot_DR = $bc_tot_CR = 0;

	if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(50, $h, 'Closing Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(27, $h, '', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(27, $h, '', '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(27, $h, d($bc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);

	}


	$this->pdf->SetFont('helvetica', 'B', 8);
	$this->pdf->MultiCell(0, 0, '', 'T','L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(50, $h, 'Account Period Total and Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h,d($acc_tot_dr), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h,d($acc_tot_cr), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h,d($acc_tot_bal), '0','R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(0, 0, '', 'B','L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 8);



	//--------------------------------------ALL BC TOTAL -----------------

	//$this->pdf->MultiCell(0, 0, '', 'B','L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->ln(2);

	$this->pdf->SetFont('helvetica', 'B', 8);
	$this->pdf->MultiCell(18, $h, '', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(18, $h, '', '0','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(50, $h, 'All Account Period Total and Balance', '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h, d($tot_bc_acc_dr_bal), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h, d($tot_bc_acc_cr_bal), '0','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(27, $h, d($tot_bc_acc_bal), '0','R', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 8);

	//----------------------------------END ALL BC TOTAL -----------------
	
	
	











	$process_end_time = time();

	$this->pdf->SetFont('helvetica', '', 5);
	$this->pdf->ln(15);
	$this->pdf->MultiCell(18, 0, ($process_end_time - $process_start_time) + $time_spend, '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

	$this->pdf->Output("bc_receipt-list.pdf", 'I');


	function d($a){
		return number_format($a,2);
	}

?>