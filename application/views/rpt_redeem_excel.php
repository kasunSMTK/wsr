<?php

    $make_excel = 1;

    $t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
    $t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';

    $t .= '<tr><td colspan="17"><h2>Redeem Activity Report</h2></td></tr>';
    $t .= '<tr><td colspan="17">Between'.$fd.' and '.$td.'</td></tr>';
    
    $t .= '<tr>';
    $t .= '<td>'. "<b>S.No" . '</b></td>';
    $t .= '<td>'. "<b>Bill Type" . '</b></td>';
    $t .= '<td>'. "<b>Bill No" . '</b></td>';
    $t .= '<td>'. "<b>Pawn Date" . '</b></td>';
    $t .= '<td>'. "<b>Redeem Date" . '</b></td>';
    $t .= '<td>'. "<b>Cus Name / Address" . '</b></td>';
    $t .= '<td>'. "<b>NIC" . '</b></td>';
    $t .= '<td>'. "<b>Mobile" . '</b></td>';
    $t .= '<td>'. "<b>Weight" . '</b></td>';
    $t .= '<td>'. "<b>Pawn Interest" . '</b></td>';
    $t .= '<td>'. "<b>Capital Amount" . '</b></td>';
    $t .= '<td>'. "<b>Stamp Fee" . '</b></td>';
    // $t .= '<td>'. "<b>Postage" . '</b></td>';
    $t .= '<td>'. "<b>Redeem Interest" . '</b></td>';
    $t .= '<td>'. "<b>Discount" . '</b></td>';
    $t .= '<td>'. "<b>Redeemed Amount" . '</b></td>';
    $t .= '<td>'. "<b>Date Time" . '</b></td>';
    $t .= '</tr>';
    
    
    $no = 1;
    $a = $b = $c = $d = $e = $f = $g = $hh = 0;
    $Aa = $Ab = $Ac = $Ad = $Ae = $Af = $Ag = $Ahh = 0;
    $s = '';

    $firs_round_passed = false;

    foreach($list as $r){


        if ($s != $r->bc_name){



            if ($firs_round_passed){

                $t .= '<tr>';
                $t .= '<td>'. ''  . '</td>';
                $t .= '<td>'. ''  . '</td>';
                $t .= '<td>'. ''  . '</td>';
                $t .= '<td>'. ''  . '</td>';
                $t .= '<td>'. ''  . '</td>';
                $t .= '<td>'. ''  . '</td>';
                $t .= '<td>'. ''  . '</td>';
                $t .= '<td><b>'. 'Total'  . '</b></td>';
                $t .= '<td><b>'. d3($a) . '</b></td>';
                $t .= '<td><b>'. d($hh)  . '</b></td>';
                $t .= '<td><b>'. d($b)  . '</b></td>';
                $t .= '<td><b>'. d($e)  . '</b></td>';
                // $t .= '<td><b>'. d($f)  . '</b></td>';
                $t .= '<td><b>'. d($g)  . '</b></td>';
                $t .= '<td><b>'. d($c)  . '</b></td>';
                $t .= '<td><b>'. d($d) . '</b></td>';
                $t .= '<td>'. '' . '</td>';
                $t .= '</tr>';
                
                
                $a = $b = $c = $d = $e = $f = $g = $hh = 0;
            }

            $firs_round_passed = true;            
            
            $t .= '<tr><td colspan="17"><b>'.$r->bc_name.'</b></td></tr>';
            $s = $r->bc_name;
            

        }



        
        
        $t .= '<tr>';
        $t .= '<td>'. $no . '</td>';
        $t .= '<td>'. $r->billtype . '</td>'; 
        $t .= '<td>'. $r->billno . '</td>';   
        $t .= '<td>'. $r->pawn_date . '</td>'; 
        $t .= '<td>'. $r->ddate . '</td>';  
        $t .= '<td>'. $r->cusname." - ".$r->address . '</td>';
        $t .= '<td>'. $r->nicno . '</td>'; 
        $t .= '<td>'. $r->mobile . '</td>'; 
        $t .= '<td>'. $r->totalweight . '</td>'; 
        $t .= '<td>'. d($r->pawn_int) . '</td>';           
        $t .= '<td>'. d($r->capital_amount) . '</td>';     
        $t .= '<td>'. d($r->stamp_fee) . '</td>';          
        // $t .= '<td>'. d($r->postage) . '</td>';            
        $t .= '<td>'. d($r->redeem_int)  . '</td>';        
        $t .= '<td>'. d($r->redeem_discount) . '</td>';    
        $t .= '<td>'. d($r->redeem_amount) . '</td>';      
        $t .= '<td>'. $r->ddate." ".$r->tm . '</td>';      
        $t .= '</tr>';

        $no++;

        $a += $r->totalweight;
        $hh += $r->pawn_int;
        $b += $r->capital_amount;
        $e += $r->stamp_fee;        
        $f += $r->postage;
        $g += $r->redeem_int;
        $c += $r->redeem_discount;
        $d += ($r->redeem_amount);


        $Aa += $r->totalweight;
        $Ahh += $r->pawn_int;
        $Ab += $r->capital_amount;
        $Ae += $r->stamp_fee;       
        // $Af += $r->postage;
        $Ag += $r->redeem_int;
        $Ac += $r->redeem_discount;
        $Ad += ($r->redeem_amount);

    }

    

    $t .= '<tr>';
    $t .= '<td>'. '' . '</td>';
    $t .= '<td>'. '' . '</td>';
    $t .= '<td>'. '' . '</td>';
    $t .= '<td>'. '' . '</td>';
    $t .= '<td>'. '' . '</td>';
    $t .= '<td>'. '' . '</td>';
    $t .= '<td>'. '' . '</td>';
    $t .= '<td><b>'. 'Total' . '</b></td>';
    $t .= '<td><b>'. d3($a) . '</b></td>';
    $t .= '<td><b>'. d($hh) . '</b></td>';
    $t .= '<td><b>'. d($b) . '</b></td>';
    $t .= '<td><b>'. d($e) . '</b></td>';
    // $t .= '<td><b>'. d($f) . '</b></td>';
    $t .= '<td><b>'. d($g) . '</b></td>';
    $t .= '<td><b>'. d($c) . '</b></td>';
    $t .= '<td><b>'. d($d) . '</b></td>';
    $t .= '</tr>';

    $t .= '<tr>';
    $t .= '<td>'. '' .'</td>';
    $t .= '<td>'. '' .'</td>';
    $t .= '<td>'. '' .'</td>';
    $t .= '<td>'. '' .'</td>';
    $t .= '<td>'. '' .'</td>';
    $t .= '<td>'. '' .'</td>';
    $t .= '<td>'. '' .'</td>';
    $t .= '<td><b>'. 'All Branch Total' . '</b></td>';
    $t .= '<td><b>'. d3($Aa) . '</b></td>';
    $t .= '<td><b>'. d($Ahh) . '</b></td>';
    $t .= '<td><b>'. d($Ab)  . '</b></td>';
    $t .= '<td><b>'. d($Ae)  . '</b></td>';
    // $t .= '<td><b>'. d($Af)  . '</b></td>';
    $t .= '<td><b>'. d($Ag)  . '</b></td>';
    $t .= '<td><b>'. d($Ac)  . '</b></td>';
    $t .= '<td><b>'. d($Ad)  . '</b></td>';
    $t .= '</tr>';

    



    function d($number) {
        return number_format($number, 2, '.', ',');
    }

    function d3($number) {
        return number_format($number, 3, '.', ',');
    }


    $t .= '</table>';

    if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'unredeem_to_forfeited.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }



?>
