<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->SetMargins(5, 5, 0, true);
$this->pdf->AddPage();

$this->pdf->SetFont('helvetica', '', 28);
$this->pdf->setY(10);
$this->pdf->setX(0);

$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
$this->pdf->Line(5, 0, 0, 0, $style);

$this->pdf->MultiCell(0, 0, "Sales Advance Item List Report", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 15);
$this->pdf->MultiCell(0, 0, "Between " . $fd . " and " . $td, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->setX(10);
$this->pdf->ln();

$this->pdf->SetFont('helvetica', 'B', 8);

$this->pdf->MultiCell(25, 1, "Category", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(70, 1, "Item Details", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(50, 1, "Bill no", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Weight", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Value", '1', 'C', 0, 1, '', '', '', '', 0);

$this->pdf->SetFont('', '', 7);

$bc = "";
$toal_p = 0;
$toal_n = 0;
$sub_p = 0;
$sub_n = 0;
foreach ($det as $key => $r) {

    $height = 2 * (max(1, $this->pdf->getNumLines($r->description, 70)));

    $this->pdf->SetFont('helvetica', '', 8);

    $this->pdf->MultiCell(25, $height, $r->category, '1', 'C', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(70, $height, $r->description, '1', 'C', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(50, $height, $r->billno, '1', 'C', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $height, $r->goldweight, '1', 'R', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $height, $r->value, '1', 'R', 0, 1, '', '', false, '', 0);

    $sub_amount += $r->value;

    if ($r->c_code != $det[$key + 1]->c_code) {
        $this->pdf->SetFont('helvetica', 'B', 9);

      
        $this->pdf->MultiCell(25, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(70, $height, "", '0', 'R', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(50, $height, "", '0', 'R', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, "Sub Total", 'B', 'R', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, number_format($sub_amount, 2), 'B', 'R', 0, 1, '', '', false, '', 0);
        $this->pdf->ln();
        $sub_amount = 0;
    
    }

    $bc = $r->bc;

    $sub_amount_net += $r->value;

}

$this->pdf->SetFont('helvetica', 'B', 9);

$this->pdf->MultiCell(25, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(70, $height, "", '0', 'R', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(50, $height, "", '0', 'R', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, "Total", 'TB', 'R', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, number_format($sub_amount_net, 2), 'TB', 'R', 0, 1, '', '', false, '', 0);

function d($number)
{
    return number_format($number, 2, '.', ',');
}

function d3($number)
{
    return number_format($number, 3, '.', '');
}

$this->pdf->Output("PDF.pdf", 'I');
