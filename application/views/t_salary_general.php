<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $("#date,#bank_date,#fd,#td").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });

</script>

<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:150px;float:left"><span>Salary Voucher</span></div>     
    </div><br>

    <form method="post" action="<?=base_url()?>index.php/main/save/t_salary_general" id="form_">

        <table border="0" align="center" cellpadding="0" cellspacing="0" width="900" class="tbl_voucher">

            <tr>
                <td colspan="4"><div class="div_vou_hin"></div></td>
            </tr>

            <tr>
                <td width="110">Payment Type</td>
                <td>
                    <select style="width: 100px;" id ="type" name ="type">                        
                        <option value="">Select</option>
                        <option value="cash">Cash</option>
                        <option value="cheque">Cheque</option>
                    </select>

                    


                </td>
                <td width="100" align="right">No</td>
                <td width="100" style="border-right: 1px solid #eaeaea">
                    <input type="text" name="nno" id="nno">
                    <input type="hidden" name="hid" id="hid" value="0">
                    <input type="hidden" name="h_load_vou" id="h_load_vou" value="0">
                    <input type="hidden" name="h_load_vou_bc" id="h_load_vou_bc" value="">
                </td>
            </tr>

            <tr>
                <td colspan="2" style="border-bottom:  1px solid #eaeaea;padding: 0px;font-family: bitter;font-size: 22px;">Paying From:</td>
                <td align="right">Date</td>
                <td style="border-right: 1px solid #eaeaea"><input type="text" id="date" name="date" value="<?=date('Y-m-d')?>" readonly="readonly"></td>                
            </tr>
            <tr>
                <td>Account</td>
                <td class="paying_acc_td"><div style="text-align:right;color: #999999">select payment type first</div></td>
                <td align="right">Ref No</td>
                <td style="border-right: 1px solid #eaeaea"><input type="text" id="ref_no" name="ref_no"></td>
            </tr>
            <tr>
                <td width="110">Balance</td>
                <td><input type="text" class="input_text_reg amount" id="acc_bal" readonly="readonly" style="font-size: 22px"></td>                
                <td colspan="2"></td>
            </tr>

            <tr>
                <td class="chq_lable">Cheque Number</td>
                <td><input type="text" name="cheque_no" id="cheque_no" disabled="disabled" style="width: 250px;">
                    <span class="chq_lable">Bank Date</span> <input type="text" name="bank_date" id="bank_date" disabled="disabled" style="width: 270px;"></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td style="border-bottom:  1px solid #eaeaea;padding: 10px 0px 10px 0px;font-family: bitter;font-size: 22px;">
                Paying To:</td>                
                <td></td>
                <td></td>
                <td></td>                
            </tr>

            
            <tr>
                <td colspan="4">
                    
                    <table border="0" width="100%" class="voucher_acc_list">
                        <tr bgcolor="#eaeaea">
                            <td style="border-left:1px solid #cccccc" colspan="5">Payee Name</td>
                        </tr>                        

                        <tbody class="voucher_acc_list_tbody">
                            <tr>
                                <td style="border-left:1px solid #cccccc"><input type="text" class="xxx" id="to_acc_desc"><input type="hidden" id="to_acc" name="to_acc[]"></td>
                                <td><input type="text" class="input_text_large amount grid_amount" id="to_amount" name="to_amount[]"></td>
                                <td><?=$grid_bc?></td>
                                <td><?=$voucher_class_dropdown?></td>
                                <td><input type="button" class="btn_regular grid_btn" value='Add' /></td>
                            </tr>
                        </tbody>
                    </table>
                    
                </td>                                                
            </tr>
            <tr>
                <td colspan="3" align="right"><br>Paying Amount</td>
                <td valign="bottom" align="right" style="border-right: 1px solid #eaeaea"><br>
                     <span class="total_vocher amount">0.00</span>
                    <input type="hidden" name="total_amount" id="total_amount" >
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <!-- <br>Voucher Description<br>
                    <textarea style="width: 100%;padding: 5px" id="voucher_desc" name="voucher_desc" ></textarea> -->
                </td>                
            </tr>
            <tr>
                <td colspan="4">
                    <!-- <br>Payee Infomation<br>
                    <textarea style="width: 100%;padding: 5px" id="payee_desc" name="payee_desc" ></textarea> -->
                    <br><br>
                </td>                
            </tr>
            <tr>
                <td colspan="4">
                    
                    <div class="div_app_controls_holder" style="float: left">
                    <input type="hidden" class="btn_regular"  id="btnSave" value='Send' disabled="disabled" />
                    <input type="button" class="btn_regular_disable"  id="btnCancel" value='Cancel' disabled="disabled" />
                    <input type="button" class="btn_regular_disable"  id="btnPrint" value='Print' disabled="disabled"/>
                    </div>

                    <div class="div_app_controls_holder" style="float: right;display: none">

                        <input type="button" class="btn_regular"            id="btnApprove" value='Approve' />
                        <input type="button" class="btn_regular"            id="btnForward" value='Forward' />
                        <input type="button" class="btn_regular"            id="btnReject"  value='Reject' />
                        
                    </div>

                    <input type="button" class="btn_regular_disable"    id="btnReset"   value='Reset' disabled="disabled"/>
                </td>                                                
            </tr>
            <tr>
                <td valign="bottom" colspan="4" height="100" style="border-bottom:  1px solid #eaeaea;padding: 10px 0px 10px 0px;font-family: bitter;font-size: 22px;"><!-- View Issued Vouchers --></td>            
            </tr>
        </table>

    </form>


    <!-- <form id="print_vou_list_pdf" action="<?php echo site_url(); ?>/reports/generate_r" method="post" target="_blank">
       <input type="hidden" name='by' value='r_voucher_list'>
        <table border="0" align="center" cellpadding="0" cellspacing="0" width="900" class="tbl_voucher">
            <tr>
                <td style="padding: 10px;width:20px">View</td>
                <td style="padding: 10px;width:20px">
                    <select name="vou_stat" class="drop_voucher_option" style="width: 100px;">
                        <option value="All">All</option>
                        <option value="W">Pending</option>
                        <option value="A">Approved</option>
                        <option value="R">Rejected</option>
                        <option value="C">Canceled</option>
                        <option value="F">Forwarded to higher level approval</option>
                    </select>
                </td>
                <td style="padding: 10px;width:20px">vouchers</td>
                <td style="padding: 10px;width:20px">from </td>
                <td style="padding: 10px;width:105px">
                    <input type="text" id="fd" name="fd" value="<?=date('Y-m-d')?>" readonly="readonly">
                </td>
                <td style="padding: 10px;width:10px">to </td>
                <td style="padding: 10px;width:105px">
                    <input type="text" id="td" name="td" value="<?=date('Y-m-d')?>" readonly="readonly">
                </td>
                <td style="padding: 10px">
                <input type="button" class="btn_regular"  id="btnShow" value='Show' />
                </td>
            </tr>

            <tr>
                <td colspan="4" height="100" style="border: none"></td>            
            </tr>
        </table>
    </form> -->



</div>

<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate_r" method="post" target="_blank">
       <input type="hidden" name='by' value='t_voucher_general' value="t_voucher_general" class="report">
       <input type="hidden" name='page' value='A4' value="A4" >
       <input type="hidden" name='orientation' value='P' value="P" >
       <input type="hidden" name='type' value='t_voucher' value="t_voucher" >
       <input type="hidden" name='recivied'  value=""  id='recivied'>
       <input type="hidden" name='header' value='false' value="false" >
       <input type="hidden" name='r_nno'  value="" id="r_nno">
       <input type="hidden" name='r_ddate'  value="" id="r_ddate">
       <input type="hidden" name='r_paid_acc' id="r_paid_acc">
       <input type="hidden" name='h_v_bc' id="h_v_bc" value="">

       
       <input type="hidden" name='voucher_type' value='' value="" id="voucher_type">
       <input type="hidden" name='dd' value="<?=date('Y-m-d')?>" id="dd" >
       <input type="hidden" name="sales_type" id="sales_type" value="" value="" >
       <input type="hidden" name='dt' value="" id="dt" >
       <input type="hidden" name='supp_id' value="" id="supp_id" >
       <input type="hidden" name='p_hid_nno' value="" id="p_hid_nno">
       <input type="hidden" name='voucher_no' value="" id="voucher_no">
       <input type="hidden" name='category_id' value="" id="category_id">
       <input type="hidden" name='cat_des' value="" id="cat_des">
       <input type="hidden" name='group_id' value="" id="group_id">
       <input type="hidden" name='group_des' value="" id="group_des">
       <input type="hidden" name='ddate' value="" id="ddate">
       <input type="hidden" name='tot' value="" id="tot">
       <input type="hidden" name='acc_code' value="" id="acc_code">
       <input type="hidden" name='acc_des' value="" id="acc_des">
       <input type="hidden" name='vou_des' value='' value="" id="vou_des">
       <input type="hidden" name='org_print' title="" value="1" id="org_print">

    </form>


</html>