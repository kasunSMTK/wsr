<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading("Pawning Report");

$r  =  $this->excel->NextRowNum();
//  $this->excel->getActiveSheet()->setCellValue('A'.$r,"Date From ".$fd." To ".$td);

$r  =  $this->excel->NextRowNum();

$this->excel->getActiveSheet()->setCellValue('A'.$r,"BC/S.No");
$this->excel->getActiveSheet()->setCellValue('B'.$r,"Bill Type");
$this->excel->getActiveSheet()->setCellValue('C'.$r,"Bill No");
$this->excel->getActiveSheet()->setCellValue('D'.$r,"Pawn Date");
$this->excel->getActiveSheet()->setCellValue('E'.$r,"Cus Name" );
$this->excel->getActiveSheet()->setCellValue('F'.$r,"Address");
$this->excel->getActiveSheet()->setCellValue('G'.$r,"NIC");
$this->excel->getActiveSheet()->setCellValue('H'.$r,"Total Weight");
$this->excel->getActiveSheet()->setCellValue('I'.$r,"Pure Weight");
$this->excel->getActiveSheet()->setCellValue('J'.$r,"Articales");
$this->excel->getActiveSheet()->setCellValue('K'.$r,"Pawn Int");
$this->excel->getActiveSheet()->setCellValue('L'.$r,"Int Discount");
$this->excel->getActiveSheet()->setCellValue('M'.$r,"Loan Amount");
$this->excel->getActiveSheet()->setCellValue('N'.$r,"App No");

$key    =   $this->excel->NextRowNum();
$n      =   0;
$st     =   "";

foreach($list as $row){    

    if ($list[$n]->bc != $st){
        // show
        $bc_name = $list[$n]->bc_name;
        $st = $list[$n]->bc;
        
        $this->excel->getActiveSheet()->setCellValue('A'.$key, $bc_name );
        $key    =   $this->excel->NextRowNum();

    }else{
        $bc_name = "";
    }
    
    $this->excel->getActiveSheet()->setCellValue('A'.$key,($n+1));
    $this->excel->getActiveSheet()->setCellValue('B'.$key,$list[$n]->billtype);
    $this->excel->getActiveSheet()->setCellValue('C'.$key,$list[$n]->billno);
    $this->excel->getActiveSheet()->setCellValue('D'.$key,$list[$n]->ddate);
    $this->excel->getActiveSheet()->setCellValue('E'.$key,$list[$n]->cusname);
    $this->excel->getActiveSheet()->setCellValue('F'.$key,$list[$n]->address);
    $this->excel->getActiveSheet()->setCellValue('G'.$key,$list[$n]->nicno);
    $this->excel->getActiveSheet()->setCellValue('H'.$key,$list[$n]->totalweight);
    $this->excel->getActiveSheet()->setCellValue('I'.$key,$list[$n]->totalpweight);
    $this->excel->getActiveSheet()->setCellValue('J'.$key,$list[$n]->items);
    $this->excel->getActiveSheet()->setCellValue('K'.$key,$list[$n]->fmintrest);
    $this->excel->getActiveSheet()->setCellValue('L'.$key,$list[$n]->int_discount);
    $this->excel->getActiveSheet()->setCellValue('M'.$key,$list[$n]->requiredamount);        
    $this->excel->getActiveSheet()->setCellValue('N'.$key,$list[$n]->app_no);
    
    
    $key++;
    $n++;

}

$this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));

?>