<?php

	$make_excel = 1;

	$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
  	$t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';
	
	$t .= "<tr>";
	$t .= "<td colspan='4'><font size='4'>Stock Valuation Report</font></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	//$t .= "<td></td>";
	$t .= "</tr>";

	$t .= "<tr>";
	$t .= "<td colspan='4'>As at ".$td."</td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	//$t .= "<td></td>";
	$t .= "</tr>";

	$t .= "<tr>";
	$t .= "<td colspan='4'><b>Rate : ".$rate."</b></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	//$t .= "<td></td>";
	$t .= "</tr>";


	$t .= "<tr>";
	$t .= "<td>Serial No</td>";
	$t .= "<td>Branch</td>";
	$t .= "<td>Date</td>";
	$t .= "<td>Bill Type</td>";
	$t .= "<td>Bill No</td>";
	$t .= "<td>Weight</td>";
	$t .= "<td>Pawning Advance</td>";
	$t .= "<td>Number of Days</td>";
	$t .= "<td>Monthly/Weekly</td>";
	$t .= "<td>Int Rate</td>";
	$t .= "<td>Weekly Int</td>";
	$t .= "<td>Int Period Changed</td>";
	$t .= "<td>Interest Charged</td>";
	//$t .= "<td></td>";
	$t .= "<td>Accrued Interest</td>";
	$t .= "<td>Total Recoverable</td>";
	$t .= "<td>Market Value</td>";
	$t .= "<td>Impairment</td>";
	$t .= "</tr>";


	$s = '';
	$s_bc = '';

	$bt_PA_tot = $AI_tot = $TR_tot = $MV_tot = $IM_tot =  0;
	$first_round_passed = $bc_first_round_passed = false;

	$bc_tot = $bc_AI_tot = $bc_TR_tot = $bc_MV_tot = $bc_IM_tot =  0;
	$nnn = 0;
	foreach ($det as $r) {


		if ($s != $r->billtype){


			if ($first_round_passed){

				$t .= "<tr>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td bgColor='Yellow'>Total</td>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td align='right' bgColor='Yellow'>".$bt_PA_tot."</td>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td></td>";
				$t .= "<td align='right' bgColor='Yellow'>".$AI_tot."</td>";
				$t .= "<td align='right' bgColor='Yellow'>".$TR_tot."</td>";
				$t .= "<td align='right' bgColor='Yellow'>".$MV_tot."</td>";
				$t .= "<td align='right' bgColor='Yellow'>".$IM_tot."</td>";	
				$t .= "</tr>";


				// bc total cals
				$bc_tot    += $bt_PA_tot;
				$bc_AI_tot += $AI_tot;
				$bc_TR_tot += $TR_tot;
				$bc_MV_tot += $MV_tot;
				$bc_IM_tot += $IM_tot;

				$bt_PA_tot = $AI_tot = $TR_tot = $MV_tot = $IM_tot = 0; 

			}


			if ( $s_bc != $r->bc ){

				if ( $bc_first_round_passed ){

					$t .= "<tr>";
					$t .= "<td></td>";
					$t .= "<td></td>";				
					$t .= "<td bgColor=' #2af797' colspan='3'><b>Branch Tottal</b></td>";				
					$t .= "<td align='right' bgColor=' #2af797'>".$bc_tot."</td>";
					$t .= "<td></td>";
					$t .= "<td></td>";
					$t .= "<td></td>";
					$t .= "<td></td>";
					$t .= "<td></td>";
					$t .= "<td></td>";
					$t .= "<td></td>";
					$t .= "<td align='right' bgColor=' #2af797'>".$bc_AI_tot."</td>";
					$t .= "<td align='right' bgColor=' #2af797'>".$bc_TR_tot."</td>";
					$t .= "<td align='right' bgColor=' #2af797'>".$bc_MV_tot."</td>";
					$t .= "<td align='right' bgColor=' #2af797'>".$bc_IM_tot."</td>";	
					$t .= "</tr>";					

					// if all branch total need assign var here
					// $all_bc_to +=$bc_tot;

					$bc_tot = 0;
					$bc_AI_tot = 0;
					$bc_TR_tot = 0;
					$bc_MV_tot = 0;
					$bc_IM_tot = 0;

				}

				$s_bc = $r->bc;
				$bc_first_round_passed = true;

			}




			/*$t .= "<tr>";
			$t .= "<td colspan='16'></td>";
			$t .= "</tr>";*/	

			$s = $r->billtype;
			$first_round_passed = true;

		}




		
		$t .= "<tr>";
		$t .= "<td>".$nnn."</td>";
		$t .= "<td>".$r->bc."</td>";
		$t .= "<td>".$r->ddate."</td>";
		$t .= "<td>".$r->billtype."</td>";
		$t .= "<td>".$r->billno."</td>";
		$t .= "<td>".$r->totalweight."</td>";
		$t .= "<td align='right'>".$r->pawning_advance."</td>";
		$t .= "<td>".$r->number_of_days."</td>";
		$t .= "<td>".$r->monthly_weekly."</td>";
		$t .= "<td>".$r->int_rate."</td>";
		$t .= "<td>".$r->Weekly_Int."</td>";


		if ($r->int_cal_changed == 1){
			$t .= "<td><b style='color:red'>Yes</b></td>";
		}else{
			$t .= "<td>No</td>";
		}
		

		$t .= "<td align='right'>".$r->interest_charged."</td>";		
		$t .= "<td align='right'>".$r->Accrued_Interest."</td>";
		$t .= "<td align='right'>".$r->Total_Recoverable."</td>";
		$t .= "<td align='right'>".$r->Market_value."</td>";
		$t .= "<td align='right'>".$r->Impairment."</td>";	
		$t .= "</tr>";

		$bt_PA_tot += $r->pawning_advance;
		$AI_tot += $r->Accrued_Interest;
		$TR_tot += $r->Total_Recoverable;
		$MV_tot += $r->Market_value;
		$IM_tot += $r->Impairment;
			

		$nnn++;

	}


	$t .= "<tr>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td bgColor='Yellow'>Total</td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td align='right' bgColor='Yellow'>".$bt_PA_tot."</td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td></td>";
	$t .= "<td align='right' bgColor='Yellow'>".$AI_tot."</td>";
	$t .= "<td align='right' bgColor='Yellow'>".$TR_tot."</td>";
	$t .= "<td align='right' bgColor='Yellow'>".$MV_tot."</td>";
	$t .= "<td align='right' bgColor='Yellow'>".$IM_tot."</td>";	
	$t .= "</tr>";

	$bc_tot    += $bt_PA_tot;
	$bc_AI_tot += $AI_tot;
	$bc_TR_tot += $TR_tot;
	$bc_MV_tot += $MV_tot;
	$bc_IM_tot += $IM_tot;

	$bt_PA_tot = $AI_tot = $TR_tot = $MV_tot = $IM_tot = 0; 


			$t .= "<tr>";
			$t .= "<td></td>";
			$t .= "<td></td>";				
			$t .= "<td bgColor=' #2af797'><b>Branch Tottal</b></td>";				
			$t .= "<td></td>";
			$t .= "<td></td>";
			$t .= "<td></td>";
			$t .= "<td align='right' bgColor=' #2af797'>".$bc_tot."</td>";
			$t .= "<td></td>";
			$t .= "<td></td>";
			$t .= "<td></td>";
			$t .= "<td></td>";
			$t .= "<td></td>";
			$t .= "<td></td>";
			$t .= "<td align='right' bgColor=' #2af797'>".$bc_AI_tot."</td>";
			$t .= "<td align='right' bgColor=' #2af797'>".$bc_TR_tot."</td>";
			$t .= "<td align='right' bgColor=' #2af797'>".$bc_MV_tot."</td>";
			$t .= "<td align='right' bgColor=' #2af797'>".$bc_IM_tot."</td>";		
			$t .= "</tr>";


	

	
	$t .= '</table>';

	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'accrued_interest.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    exit;


	function d($a){
		return number_format($a,2);
	}

?>