<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->SetMargins(5, 5, 0, true);
$this->pdf->AddPage();

$this->pdf->SetFont('helvetica', '', 28);
$this->pdf->setY(10);
$this->pdf->setX(0);

$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
$this->pdf->Line(5, 0, 0, 0, $style);

$this->pdf->MultiCell(0, 0, "Sales Details Report", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 15);
$this->pdf->MultiCell(0, 0, "Between " . $fd . " and " . $td, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->setX(10);
$this->pdf->ln();

$this->pdf->SetFont('helvetica', 'B', 8);

$this->pdf->MultiCell(20, 1, "BillNo", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "Customer Code", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(20, 1, "Auction Date", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "Auction Code", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "Article", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Sn/Imi/ Weight", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Selling Date", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Pawning Amount", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Selling Amount", '1', 'C', 0, 1, '', '', '', '', 0);

$this->pdf->SetFont('', '', 7);

$bc = "";
$toal_p = 0;
$toal_n = 0;
$sub_p = 0;
$sub_n = 0;
foreach ($det as $key => $r) {

    $height = 2 * (max(1, $this->pdf->getNumLines($r->itemname, 30)));
    $this->pdf->HaveMorePages($height);
    if ($bc != $r->bc) {
        $this->pdf->SetFont('helvetica', 'B', 8);

        $this->pdf->MultiCell(20, $height, $r->bc, '0', 'C', 0, 1, '', '', false, '', 0);
    }

    $this->pdf->SetFont('helvetica', '', 8);
    $this->pdf->MultiCell(20, $height, $r->nno, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $height, $r->cus_serno, '1', 'R', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(20, $height, $r->auction_date, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $height, $r->auction_code, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $height, $r->article, '1', 'L', 0, 0, '', '', false, '', 0);

    if ($r->gold_weight != "") {
        $this->pdf->MultiCell(30, $height, $r->gold_weight, '1', 'L', 0, 0, '', '', '', '', 0);
    } else if ($r->elec_imei != "") {
        $this->pdf->MultiCell(30, $height, $r->elec_imei, '1', 'L', 0, 0, '', '', '', '', 0);
    } else if ($r->veh_engine_no != "") {
        $this->pdf->MultiCell(30, $height, $r->veh_engine_no, '1', 'L', 0, 0, '', '', '', '', 0);
    } else {
        $this->pdf->MultiCell(30, $height, "", '1', 'L', 0, 0, '', '', '', '', 0);
    }
    $this->pdf->MultiCell(30, $height, $r->selling_date, '1', 'L', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(30, $height, $r->pawning_amount, '1', 'R', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(30, $height, $r->selling_net_amount, '1', 'R', 0, 1, '', '', false, '', 0);

    $sub_p += $r->pawning_amount;
    $sub_n += $r->selling_net_amount;
    if ($r->nno != $det[$key + 1]->nno) {
        $this->pdf->SetFont('helvetica', 'B', 9);

        $this->pdf->MultiCell(20, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(20, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, "Sub Total", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, number_format($sub_p, 2), 'B', 'R', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, number_format($sub_n, 2), 'B', 'R', 0, 1, '', '', false, '', 0);
        $this->pdf->ln();
        $sub = 0;
        $sub_p = 0;
        $sub_n = 0;
    }
    $bc = $r->bc;

    $toal_p += $r->pawning_amount;

    $toal_n += $r->selling_net_amount;

}

$this->pdf->SetFont('helvetica', 'B', 9);
$this->pdf->MultiCell(20, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, "Total", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, number_format($toal_p, 2), 'TB', 'R', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, number_format($toal_n, 2), 'TB', 'R', 0, 1, '', '', false, '', 0);

function d($number)
{
    return number_format($number, 2, '.', ',');
}

function d3($number)
{
    return number_format($number, 3, '.', '');
}

$this->pdf->Output("PDF.pdf", 'I');
