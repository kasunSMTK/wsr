<?php
	
	$this->pdf = new TCPDF("L", PDF_UNIT, 'A3', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	$this->pdf->MultiCell(0, 0, "Pawning Activity Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();

	$this->pdf->SetFont('helvetica', '', 8);
	
	$this->pdf->MultiCell(15, 1, "BC/S.No", 'B','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(25, 1, "Bill No", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Pawn Date", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(80, 1,"Cus Name / Address", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "NIC", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Mobile", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Total Weight", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Pure Weight", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(55, 1, "Articales", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Status", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Loan Amount", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Rate", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(25, 1, "Outstanding Int", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Re. Amount", 'B','R', 0, 0, '', '', '', '', 0,0);	
	$this->pdf->MultiCell(20, 1, "Re. Date", 'B','C', 0, 1, '', '', '', '', 0,0);	
	

	$no = 1;
	$st = ""; 
	$rate=(float)0;

	$sub_tot_loop_start = false;
	$first_r = false;
	$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_os = $sub_tot_ra = 0;

	foreach($list as $r){
		
		$Q1 = $this->db->query("SELECT C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,C.`customer_id`,L.int_cal_changed,L.cus_serno,L.am_allow_frst_int FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '".$r->billno."' AND L.bc = '".$r->bc."' LIMIT 1 ");
		$a['loan_sum'] = $Q1->row();

		//int_balance

		if (is_null($a['loan_sum'])){
			$int_bal = 0;
		}else{
			$payable_int_bal = 0;
			// $X->calculate->
			// interest($r->loanno,$return_mode = "json",$td, $a['loan_sum'] , $five_days_int_cal=0);
			// $int_bal = $payable_int_bal['int_balance'];
		}		

		if (strtoupper($r->bc) != strtoupper($st)){
			// show		

			if (sub_tot_loop_start){
				if ($first_r){
					$this->pdf->SetFont('helvetica', 'B', 8);						
					$this->pdf->set_tot_line(d3($sub_tot_tw), d3($sub_tot_pw), d($sub_tot_la), d($sub_tot_os), d($sub_tot_ra) );
					$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_os = $sub_tot_ra = 0;
				}

				$first_r = true;
			}

			$sub_tot_tw += $r->totalweight;		
			$sub_tot_pw += $r->pure_weight_tot;
			$sub_tot_la += $r->requiredamount;
			$sub_tot_os += $int_bal;
			$sub_tot_ra += $r->redeem_amount;
			
			$bc_name = $r->bc_name;
			$st 	 = $r->bc;
			$sub_tot_loop_start = false;

			$this->pdf->SetFont('helvetica', 'B', 8);	
			$this->pdf->MultiCell(0,5, $bc_name, $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);		

		}else{
			$sub_tot_tw += $r->totalweight;		
			$sub_tot_pw += $r->pure_weight_tot;
			$sub_tot_la += $r->requiredamount;
			$sub_tot_os += $int_bal;
			$sub_tot_ra += $r->redeem_amount;

			$bc_name = "";
			$sub_tot_loop_start = true;
		}

		$h = 5 * (max(1,$this->pdf->getNumLines(str_replace(",", "<br>", $r->items),55),$this->pdf->getNumLines($r->cusname,80)));

		$rate=($r->requiredamount * 8)/$r->pure_weight_tot;
		
		$this->pdf->SetFont('helvetica', '', 7);
		$this->pdf->MultiCell(15, $h, $no , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);		
		$this->pdf->MultiCell(25, $h, $r->billno , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->pawn_date , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(80, $h, $r->cusname. " - " . $r->address , $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->nicno , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->mobile , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->totalweight , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->pure_weight_tot , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(55, $h, $r->items, $border='1', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->status, $border='1', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->requiredamount , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, d($rate) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		

		$this->pdf->MultiCell(25, $h, number_format($int_bal,2) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		
		$this->pdf->MultiCell(20, $h, $r->redeem_amount , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->redeem_date , $border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		
		$no++;
		
	}

	$this->pdf->SetFont('helvetica', 'B', 8);	
	$this->pdf->set_tot_line(d3($sub_tot_tw), d3($sub_tot_pw), d($sub_tot_la), d($sub_tot_os), d($sub_tot_ra) );

	function get_int_bal($X,$loan_no,$int_cal_to_DATE,$LOAN_DATA){
		return $payable_int_bal;
	}

	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}

	$this->pdf->Output("PDF.pdf", 'I');

?>