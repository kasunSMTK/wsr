<?php defined('BASEPATH') OR exit('No direct script access allowed'); $menu_item_visibility = array_filter($menu_item_visibility); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<title><?=$page_title?></title>	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css" />	
	<script type="text/javascript" src="<?=base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/jquery.ui.core.min.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/jquery-ui-1.8.17.custom.min.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/validation.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/main.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/input.js"></script>	
	<script type="text/javascript" src="<?=base_url(); ?>js/<?=$js_file_name?>.js?<?=strtotime(date('Y-m-d H:i:s'))?>"></script>
	<link rel="stylesheet" href="<?= base_url(); ?>/application/3rdparty/mega-dropdown/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?= base_url(); ?>/application/3rdparty/mega-dropdown/css/style.css"> <!-- Resource style -->
	<script src="<?= base_url(); ?>/application/3rdparty/mega-dropdown/js/modernizr.js"></script> <!-- Modernizr -->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/application/3rdparty/tooltipster/dist/css/tooltipster.bundle.min.css" />	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/inputs.css" />
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/grid.css" />	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/mnu_icons.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>/application/3rdparty/mobirise/style.css">
	
</head>

<body>

	<div class='notifications' id='notifications'>
		<div class='div_shadow_hide'></div>
		<div class="notifications_title"></div>
	</div>

	<div class="left_hover_slider"></div>
	<div class="left_hover_slider_content">

		<br><br><br><br>

		<div class="left_icons">
			<a href="?" class='ti-home'></a>
		</div>

	</div>

	<div id="blocker"></div>
	<div id="serch_pop" style="width: 800px; ">
		<input type="text" id="pop_search" title="" class="input_acitve" style="width: 100%;" /><br />
		<div id="sr" style="height:400px;"></div>
		<div style="text-align: right; padding-top: 7px;"><button id="pop_close" >Close</button></div>
	</div>
	<div id="blocker2"></div>
	<div id="serch_pop2" style="width: 800px;">
		<input type="text" id="pop_search2" title=""  style="width: 100%;" class="input_acitve"/><br />
		<div id="sr2" style="height:400px;"></div>
		<div style="text-align: right; padding-top: 7px;"><button id="pop_close2" >Close</button></div>
	</div>

	<div id="blocker4"></div>
	<!-- <div id="blanket4">
	    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" />
	</div> -->
	<div id="serch_pop4" style="width: 800px;">
		<input type="text" id="pop_search4" title=""  style="width: 100%;" /><br />
		<div id="sr4" style="height:400px;"></div>
		<div style="text-align: right; padding-top: 7px;">
			<input type='button' value='Approve' title='Approve' id='approve4' style='display:none;'/>
			<button id="pop_close4" >Close</button></div>
		</div>

		<div id="blocker"></div>
	<!-- <div id="blanket">
	    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" />
	</div> -->
	<div id="serch_pop6" style="width: 800px;">
		<input type="text" id="pop_search6" title=""  style="width: 100%;" class="input_acitve"/><br />
		<div id="sr6" style="height:400px;"></div>
		<div style="text-align: right; padding-top: 7px;"><button id="pop_close6" >Close</button></div>
	</div>


	<div id="blocker"></div>
	
	<div id="serch_pop14" style="width: 800px;">
		<input type="text" id="pop_search14" title=""  style="width: 100%;" class="input_acitve"/><br />
		<div id="sr14" style="height:400px;"></div>
		<div style="text-align: right; padding-top: 7px;"><button id="pop_close14" >Close</button></div>
	</div>
	<div id="blocker"></div>
	<!-- <div id="blanket"> -->
	    <!-- <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" /> -->
	</div>
	<div id="serch_pop11" style="width: 800px;">
		<input type="text" id="pop_search11" title=""  style="width: 100%;" class="input_acitve"/><br />
		<div id="sr11" style="height:400px;"></div>
		<div style="text-align: right; padding-top: 7px;"><button id="pop_close11" >Close</button></div>
	</div>

	<div id="blocker"></div>
	<!-- <div id="blanket"> -->
	    <!-- <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" /> -->
	</div>
	<div id="serch_pop13" style="width: 800px;">
		<input type="text" id="pop_search13" title=""  style="width: 100%;" class="input_acitve"/><br />
		<div id="sr13" style="height:400px;"></div>
		<div style="text-align: right; padding-top: 7px;"><button id="pop_close13" >Close</button></div>
	</div>


	<div class="msg_pop_up_bg"></div>
	<div class="msg_pop_up_sucess">Text Here...</div>

	<div class="company_name">
		<div class="menu_holder_div">
			<div class="cd-dropdown-wrapper" style="border:0px solid red ; margin:0px;">


				<div class="cd-dropdown-trigger" href="#0">Menu</div>
				<nav class="cd-dropdown">
					<h2>Title</h2>
					<a href="#0" class="cd-close">Close</a>
					<ul class="cd-dropdown-content">
							<!-- <li>
								<form class="cd-search">
									<input type="search" placeholder="Search...">
								</form>
							</li> -->
							<!-- *** Master Data Setup Start************************************************** -->

							<?php 
							function txColor(){
								$rand = array('0');
								$color = '#'.$rand[rand(0,0)].$rand[rand(0,0)].$rand[rand(0,0)].$rand[rand(0,0)].$rand[rand(0,0)].$rand[rand(0,0)];
								return $color;
							}
							?>
							
							<!--  Admin -->							
							
							<li class="has-children">								
								
								<?php 
								$menu_items = array("m_area", "m_branches", "m_billtype_det", "m_item_category", "m_item", "m_conditions", "m_company", "r_gold_rate", "r_gold_quality","m_setup_pawning_value", "m_customer","m_remind_letter_charges","m_vendor","m_receipt_customer","m_bc_cash_acc_setup","m_branch_backdate","m_branch_receipt_update_control","m_top_customers");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>
								<a href="">Master Data Setup</a>
								<?php } ?>

								<ul class="cd-dropdown-icons is-hidden">
									
									<?php if (in_array('m_area', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-location-pin" href="?action=m_area">
											<h3>Area</h3>
											<p>Add/Edit/Delete Area</p>
										</a>
									</li>											
									<?php } ?>

									<?php if (in_array('m_branches', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-direction-alt" href="?action=m_branches" style="color: <?=txColor()?> !important;">
											<h3>Branches</h3>
											<p>Add/Edit/Delete Branches</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_billtype_det', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-files" href="?action=m_billtype_det" style="color: <?=txColor()?> !important;">
											<h3>Bill Types</h3>
											<p>Setup Bill Types Details</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_item_category', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-bag" href="?action=m_item_category">
											<h3>Item Categories</h3>
											<p>Add/Edit/Delete Item Categories</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_item', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-package" href="?action=m_item">
											<h3>Item</h3>
											<p>Add/Edit/Delete Item details</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_conditions', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-thumb-up" href="?action=m_conditions">
											<h3>Item Conditions</h3>
											<p>Add/Edit/Delete Item Conditions</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_company', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-id-badge" href="?action=m_company">
											<h3>Company Details</h3>
											<p>Add/Edit/Delete company details</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('r_gold_rate', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-stats-up" href="?action=r_gold_rate">
											<h3>Gold Rates</h3>
											<p>Setup Gold Rates</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('r_gold_quality', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-star" href="?action=r_gold_quality">
											<h3>Gold Quality</h3>
											<p>Setup Gold Quality Rates</p>
										</a>
									</li>
									<?php } ?>

									<!-- <?php if (in_array('r_stampfee', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-image" href="?action=r_stampfee">
											<h3>Stamp Fees</h3>
											<p>Setup Stamp Fees</p>
										</a>
									</li>
									<?php } ?> -->

									<!-- <?php if (in_array('r_document_charge', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-file " href="?action=r_document_charge">
											<h3>Document Charge</h3>
											<p>Setup Document Charge</p>
										</a>
									</li>
									<?php } ?> -->

									<?php if (in_array('m_setup_pawning_value', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-file " href="?action=m_setup_pawning_value">
											<h3>Setup Pawning Values</h3>
											<p>Setup Pawning Values</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_customer', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_customer">
											<h3>Customer</h3>
											<p>Customer</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_voucher_class', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_voucher_class">
											<h3>Voucher Class</h3>
											<p>Voucher Class</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_remind_letter_charges', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_remind_letter_charges">
											<h3>Remind Letter Charges</h3>
											<p>Remind Letter Charges</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_vendor', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_vendor">
											<h3>Vendor</h3>
											<p>Vendor</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_receipt_customer', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_receipt_customer">
											<h3>Receipt Customer</h3>
											<p>Receipt Customer</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_bc_cash_acc_setup', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_bc_cash_acc_setup">
											<h3>Branch Cash Book Setup</h3>
											<p>Branch Cash Book Setup</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_branch_backdate', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_branch_backdate">
											<h3>Branch Backdate</h3>
											<p>Branch Backdate</p>
										</a>
									</li>
									<?php } ?>


									<?php if (in_array('m_branch_receipt_update_control', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_branch_receipt_update_control">
											<h3>Branch Receipt Edit Control</h3>
											<p>Branch Receipt Edit Control</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_top_customers', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-user " href="?action=m_top_customers">
											<h3>Customer Control</h3>
											<p>Customer Control</p>
										</a>
									</li>
									<?php } ?>


									

								</ul>
							</li>							


							<!-- *** Master Data Setup End************************************************** -->


							<!-- *** Account Setups Start************************************************** -->


							<li class="has-children">
								
								<?php 
								$menu_items = array("m_account_type", "m_bank", "m_bank_branch", "m_default_account", "r_journal_type","t_dbl_entry_view");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>									
								<a href="">Account Setups</a>
								<?php } ?>

								<ul class="cd-dropdown-icons is-hidden">
									<li class="go-back"><a href="#0">Menu</a></li>
									<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
									<?php if (in_array('m_account_type', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-book" href="?action=m_account_type">
											<h3>Account Setup</h3>
											<p>Account Setup</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_bank', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-money" href="?action=m_bank">
											<h3>Bank</h3>
											<p>Bank</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_bank_branch', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-money" href="?action=m_bank_branch">
											<h3>Bank Branch</h3>
											<p>Bank Branch</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('m_default_account', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-world" href="?action=m_default_account">
											<h3>Default Accounts</h3>
											<p>Default Accounts</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('r_journal_type', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-book" href="?action=r_journal_type">
											<h3>Journal Type</h3>
											<p>Journal Type</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_dbl_entry_view', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-book" href="?action=t_dbl_entry_view">
											<h3>Double Entry Check</h3>
											<p>Double Entry Check</p>
										</a>
									</li>
									<?php } ?>


								</ul>
							</li>						



							<li class="has-children">
								
								<?php 
								$menu_items = array("t_new_pawn", "t_redeem", "t_part_payment", "t_customer_advance_payment", "t_amendments", "t_mark_bills", "t_forfeit", "	","t_remind_letters");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>
								<a href="" style='background-color: Green;color:#ffffff; font-family: bitter '>Pawning</a>
								<?php } ?>


								<ul class="cd-dropdown-icons is-hidden">
									<li class="go-back"><a href="#0">Menu</a></li>
									<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->

									<!-- <?php if (in_array('t_opening_pawn', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-agenda" href="?action=t_opening_pawn">
											<h3>Opening Pawn</h3>
											<p>Enter opening pawn from here</p>
										</a>
									</li>
									<?php } ?> -->

									<?php if (in_array('t_new_pawn', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-agenda" href="?action=t_new_pawn">
											<h3>New Pawn</h3>
											<p>Enter new pawn from here</p>
										</a>
									</li>
									<?php } ?>

									<li>
										<a class="cd-dropdown-item ti-agenda" href="?action=pawn_bill_int">
											<h3>Intrest Change</h3>
											<p>Change Pawn Bill Intrest</p>
										</a>
									</li>

									<?php if (in_array('t_redeem', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-bookmark-alt" href="?action=t_redeem">
											<h3>Redemption</h3>
											<p>Redemption a pawn articles</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_part_payment', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-receipt" href="?action=t_part_payment">
											<h3>Part Payment & Renew Bill</h3>
											<p>Part Payment & Renew Bill </p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_customer_advance_payment', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-receipt" href="?action=t_customer_advance_payment">
											<h3>Customer Advance Payment</h3>
											<p>Customer Advance Payment </p>
										</a>
									</li>
									<?php } ?>

									<!-- <?php if (in_array('t_amendments', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-receipt" href="?action=t_amendments">
											<h3>TFR</h3>
											<p>TFR</p>
										</a>
									</li>	
									<?php } ?> -->

									<?php if (in_array('t_mark_bills', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-write" href="?action=t_mark_bills">
											<h3>Mark Bills</h3>
											<p>Mark bills </p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_forfeit', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-calendar" href="?action=t_forfeit">
											<h3>Forfeit</h3>
											<p>Forfeit Pawn</p>
										</a>
									</li>
									<?php } ?>


									<?php if (in_array('t_remind_letters', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-printer" href="?action=t_remind_letters">
											<h3>Remind Letters</h3>
											<p>Remind Letters</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_redeem_cancel', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-printer" href="?action=t_redeem_cancel">
											<h3>Cancel a Redeemed Bill</h3>
											<p>Cancel a Redeemed Bill</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_br_receipt_general', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-printer" href="?action=t_br_receipt_general">
											<h3>Branch Receipt</h3>
											<p>Branch Receipt</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_partpay_cancel', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-printer" href="?action=t_partpay_cancel">
											<h3>Cancel a Part Payment</h3>
											<p>Cancel a Part Payment</p>
										</a>
									</li>
									<?php } ?>


									<?php if (in_array('t_day_cach_bal', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-book" href="?action=t_day_cach_bal">
											<h3>Daily Cash Entry</h3>
											<p>Daily Cash Entry</p>
										</a>
									</li>
									<?php } ?>


								</ul>
							</li>


							
							

							

							<!--  Admin -->
							<li class="has-children">
								<?php 
								$menu_items = array("t_opening_balance", "t_journal_sum", "t_receipt_general", "t_voucher_general", "t_cheque_issue", "t_fund_transfer", "t_payable_invoice", "t_receivable_invoice", "r_account_report", "r_transaction_list_voucher", "t_salary_vouchers","t_branch_current_cash_bal","t_cheque_status","t_user_activity","t_canceled_transactions");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>										
								<a href="">Transactions</a>
								<?php } ?>
								<ul class="cd-dropdown-icons is-hidden">
									<li class="go-back"><a href="#0">Menu</a></li>

									<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
									
									
									<?php if (in_array('t_opening_balance', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-shield" href="?action=t_opening_balance">
											<h3>Opening Balance</h3>
											<p>Opening Balance</p>
										</a>
									</li>
									<?php } ?>

									<?php if (in_array('t_journal_sum', $menu_item_visibility)){ ?>
									<li>
										<a class="cd-dropdown-item ti-zip" href="?action=t_journal_sum">
											<h3>Journal Entry</h3>
											<p>Journal Entry</p>
										</a>
									</li>
									<?php } ?>
										<!-- <li>
											<a class="cd-dropdown-item ti-layout-media-overlay-alt-2" href="?action=t_receipt_general">
												<h3>General Receipt</h3>
												<p>General Receipt</p>
											</a>
										</li> -->
										<?php if (in_array('t_receipt_general', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-layout-media-overlay-alt-2" href="?action=t_receipt_general">
												<h3>General Receipt</h3>
												<p>General Receipt</p>
											</a>
										</li>
										<?php } ?>
										<!-- <li>
											<a class="cd-dropdown-item ti-layout-cta-btn-left" href="?action=t_voucher_general">
												<h3>General Voucher</h3>
												<p>General Voucher</p>
											</a>
										</li> -->
										<?php if (in_array('t_voucher_general', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-layout-cta-btn-left" href="?action=t_voucher_general">
												<h3>Payment Voucher</h3>
												<p>Payment Voucher</p>
											</a>
										</li>
										<?php } ?>

										<!-- <?php if (in_array('t_pettycash', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-wallet" href="?action=t_pettycash">
												<h3>Petty Cash</h3>
												<p>Petty Cash</p>
											</a>
										</li>
										<?php } ?> -->

										<?php if (in_array('t_cheque_issue', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-wallet" href="?action=t_cheque_issue">
												<h3>Cheque Issue</h3>
												<p>Cheque Issue</p>
											</a>
										</li>
										<?php } ?>
										

										<?php if (in_array('t_fund_transfer', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-wallet" href="?action=t_fund_transfer">
												<h3>Fund Transfer</h3>
												<p>Fund Transfer</p>
											</a>
										</li>	
										<?php } ?>									
										
										

										<?php if (in_array('t_payable_invoice', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-layout-media-overlay" href="?action=t_payable_invoice">
												<h3>Payable Invoice</h3>
												<p>Payable Invoice</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('t_receivable_invoice', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-layout-cta-right" href="?action=t_receivable_invoice">
												<h3>Receivable Invoice</h3>
												<p>Receivable Invoice</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('r_account_report', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=r_account_report">
												<h3>Account Reports</h3>
												<p>Account Reports</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('r_transaction_list_voucher', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=r_transaction_list_voucher">
												<h3>Transaction Reports</h3>
												<p>Transaction Reports</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('t_salary_vouchers', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=t_salary_vouchers">
												<h3>Salary Vouchers</h3>
												<p>Generate Salary Vouchers</p>
											</a>
										</li>
										<?php } ?>


										<?php if (in_array('t_branch_current_cash_bal', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=t_branch_current_cash_bal">
												<h3>Current Cash Balance</h3>
												<p>Current Cash Balance</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('t_cheque_status', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=t_cheque_status">
												<h3>Fund Transfer History</h3>
												<p>Fund Transfer History</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('t_user_activity', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-layout-media-overlay-alt-2" href="?action=t_user_activity">
												<h3>User Activity</h3>
												<p>User Activity</p>
											</a>
										</li>
										<?php } ?>


										<?php if (in_array('t_canceled_transactions', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-layout-media-overlay-alt-2" href="?action=t_canceled_transactions">
												<h3>Canceled Transactions</h3>
												<p>Canceled Transactions</p>
											</a>
										</li>
										<?php } ?>


										<?php if (in_array('s_fund_tr_chq_search', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-layout-media-overlay-alt-2" href="?action=s_fund_tr_chq_search">
												<h3>Fund Transfer Cheque Search</h3>
												<p>Fund Transfer Cheque Search</p>
											</a>
										</li>
										<?php } ?>

									

									</ul>
								</li>

								<!-- <?php if (in_array('t_approvals', $menu_item_visibility)){ ?> -->
								<!-- <li>
									<a href="?action=t_approvals">Approval</a>
								</li> -->
								<!-- <?php } ?> -->

























								<li class="has-children">
									<?php 
									$menu_items = array("appvl_gl_r","appvl_general_voucher","appvl_fund_transfers","appvl_over_advance","appvl_salary_voucher");
									if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>										
									<a href="">Approval</a>
									<?php } ?>

									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>

										<?php if (in_array('appvl_general_voucher', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=appvl_general_voucher">
												<h3>Vouchers</h3>
												<p>Vouchers</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('appvl_salary_voucher', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=appvl_salary_voucher">
												<h3>Salary Vouchers</h3>
												<p>Salary Vouchers</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('appvl_fund_transfers', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=appvl_fund_transfers">
												<h3>Fund Transfers</h3>
												<p>Fund Transfers</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('appvl_over_advance', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=appvl_over_advance">
												<h3>Over Advance</h3>
												<p>Over Advance</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('appvl_over_advance', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=appvl_discount">
												<h3>Discount Approval</h3>
												<p>Discount Approval</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('appvl_je', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=appvl_je">
												<h3>Journal Entry Approval</h3>
												<p>Journal Entry Approval</p>
											</a>
										</li>
										<?php } ?>


										<?php if (in_array('appvl_gl_r', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=appvl_gl_r">
												<h3>General Receipt</h3>
												<p>General Receipt</p>
											</a>
										</li>
										<?php } ?>


									</ul>
								</li>





























								<li class="has-children">
									<?php 
									$menu_items = array("","");
									if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>																				
									<a href="">Letters</a>
									<?php } ?>

									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->

										<?php if (in_array('index', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-notepad" href="?action=index">
												<h3>Remind Letters</h3>
												<p>Generate Remind Letters</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('index', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-clipboard" href="?action=index">
												<h3>Letter Period Setup</h3>
												<p>Letter Period Setup</p>
											</a>
										</li>
										<?php } ?>									

									</ul>
								</li>							


								<li class="has-children">
									<?php 
									$menu_items = array("t_gold_audit","r_audit");
									if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>																														
									<a href="">Audit</a>
									<?php } ?>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>										
										
										<?php if (in_array('t_gold_audit', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-check-box" href="?action=t_gold_audit">
												<h3>Gold Audit</h3>
												<p>Gold Audit</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('index', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=r_audit">
												<h3>Reports</h3>
												<p>Reports</p>
											</a>
										</li>
										<?php } ?>
									</ul>
								</li>

<!--SALES-->

								<li class="has-children">
									<?php 
									$menu_items = array("t_tag");									
									if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>																																								
									<a href="">SALES</a>
									<?php } ?>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
										
										<?php if (in_array('t_tag', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-book" href="?action=t_tag">
												<h3>Item Tag</h3>
												<p>Item Tag</p>
											</a>
										</li>
										<?php } ?>
									</ul>
								</li>




<!--                  -->


								<li class="has-children">
									<?php 
									$menu_items = array("t_bankrec_n","t_bank_entry", "t_bankrec", "t_cheque_deposit", "t_cheque_issue_trans", "r_bank_entry");									
									if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>																																								
									<a href="">Bank</a>
									<?php } ?>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
										
										<?php if (in_array('t_bank_entry', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-book" href="?action=t_bank_entry">
												<h3>Bank Entry</h3>
												<p>Bank Entry</p>
											</a>
										</li>
										<?php } ?>

										<!-- <?php if (in_array('t_bankrec', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-money" href="?action=t_bankrec">
												<h3>Bank Reconciliation</h3>
												<p>Bank Reconciliation</p>
											</a>
										</li>
										<?php } ?> -->	

										<?php if (in_array('t_bankrec_n', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-money" href="?action=t_bankrec_n">
												<h3>Bank Reconciliation New</h3>
												<p>Bank Reconciliation New</p>
											</a>
										</li>
										<?php } ?>	

										<?php if (in_array('t_cheque_deposit', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-credit-card" href="?action=t_cheque_deposit">
												<h3>Cheque Deposit</h3>
												<p>Cheque Deposit</p>
											</a>
										</li>
										<?php } ?>	
										
										<?php if (in_array('t_cheque_issue_trans', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-credit-card" href="?action=t_cheque_issue_trans">
												<h3>Cheque Withdraw</h3>
												<p>Cheque Withdraw</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('r_bank_entry', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=r_bank_entry">
												<h3>Reports</h3>
												<p>Reports</p>
											</a>
										</li>
										<?php } ?>

									</ul>
								</li>


								<li class="has-children">
									<?php 
									$menu_items = array("t_customer_statment", "t_backup", "t_attendance_process", "t_message", "m_billno_date_setup");
									if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>																																																		
									<a href="">Utilities</a>
									<?php } ?>

									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
										
										<?php if (in_array('t_customer_statment', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-user" href="?action=t_customer_statment">
												<h3>Customer Statment</h3>
												<p>View Customer Statment</p>
											</a>
										</li>
										<?php } ?>										

										<?php if (in_array('t_backup', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-server" href="?action=t_backup">
												<h3>Database Backup</h3>
												<p>Database Backup</p>
											</a>
										</li>
										<?php } ?>																		

										<?php if (in_array('t_attendance_process', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-server" href="?action=t_attendance_process">
												<h3>Attendance Process</h3>
												<p>Attendance Process</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('t_message', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-server" href="?action=t_message">
												<h3>Create a Message</h3>
												<p>Create a Message</p>
											</a>
										</li>
										<?php } ?>
										

										<!-- <?php if (in_array('t_customer_sync_bc', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-user" href="?action=t_customer_sync_bc">
												<h3>Customer Sync</h3>
												<p>Customer Sync</p>
											</a>
										</li>
										<?php } ?> -->
										

										<?php if (in_array('m_billno_date_setup', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-server" href="?action=m_billno_date_setup">
												<h3>Bill Number Date Setup</h3>
												<p>Add Bill Number Date</p>
											</a>
										</li>
										<?php } ?>


									</ul>
								</li>

								<li class="has-children">
									<?php 
									$menu_items = array("s_role", "s_add_role", "s_module", "u_users");
									if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>										
									<a href="">User Permission</a>
									<?php } ?>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- User permissions -->
										
										<?php if (in_array('s_role', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-user" href="?action=s_role">
												<h3>Create/Manage Role</h3>
												<p>Create/Modify/Delete Role and Manage</p>
											</a>
										</li>
										<?php } ?>
										
										<?php if (in_array('s_add_role', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-server" href="?action=s_add_role">
												<h3>Assign Role</h3>
												<p>Assign a/an role(s) to a users</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('s_module', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-server" href="?action=s_module">
												<h3>Add Module</h3>
												<p>Add New Modules</p>
											</a>
										</li>
										<?php } ?>										

										<?php if (in_array('u_users', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-user" href="?action=u_users">
												<h3>User Manager</h3>
												<p>Create/Modify/Delete user accounts</p>
											</a>
										</li>
										<?php } ?>

										<?php if (in_array('com_reg_admin', $menu_item_visibility)){ ?>
										<li>
											<a class="cd-dropdown-item ti-user" href="?action=com_reg_admin">
												<h3>Branch PC Register</h3>
												<p>Branch PC Register</p>
											</a>
										</li>
										<?php } ?>

										<!-- End user permissions -->
									</ul>
								</li>
								
								<?php $menu_items=array("t_daily_cash_bal");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>
								<li>
									<a href="?action=t_daily_cash_bal">Daily Cash Balance</a>		
								</li>
								<?php } ?>
								



								<?php $menu_items=array("r_pawning");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>
								<li>
									<a href="?action=r_pawning">Reports</a>		
								</li>
								<?php } ?>								
								
								<?php $menu_items=array("t_leave_request");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>
								<li>
									<a href="?action=t_leave_request">Leave Request</a>		
								</li>
								<?php } ?>


								<?php $menu_items=array("t_print_voucher");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>
								<li>
									<a href="?action=t_print_voucher">Print Voucher</a>		
								</li>
								<?php } ?>

								<?php $menu_items=array("t_print_salary_voucher");
								if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){ ?>
								<li>
									<a href="?action=t_print_salary_voucher">Print Salary Voucher</a>		
								</li>
								<?php } ?>								



								<!-- <li><a href="http://www.softmastergroup.com/">Page 3</a></li> -->
							</ul> <!-- .cd-dropdown-content -->
						</nav> <!-- .cd-dropdown -->
					</div> <!-- .cd-dropdown-wrapper -->
				</div>

				<span class="company_name_title_small"><?=$company_name?></span>

				<div class="top_links_holder">		

					<?php if(!$is_login_view){ ?>				

					<a href="#"><div class='menu_top_link notify' style='border:0px solid #f9f9f9;border-bottom: 0px solid #eaeaea;'>&nbsp;</div>
					<div class="notifi_indicator notify_count">00</div>
					<audio id="audiotag1" src="sound/notify.wav" preload="auto"></audio>
					</a>
					<?=anchor("", 	"<div class='menu_top_link'>Home</div>"); ?>
					<?php if (in_array('my_account', $menu_item_visibility)){ ?>
					<a href="?action=my_account"><div class='menu_top_link'><?=$User?></div></a>
					<?php } ?>
					<?=anchor("main/logout", 			"<div class='menu_top_link'>Logout</div>"); ?>			

					<?php } ?>

				</div>


			</div>

			<?=$hp_msg?>
			<?=$bk_opt?>
			<?=$ud_opt?>
			

			<script src="<?= base_url(); ?>/application/3rdparty/mega-dropdown/js/jquery-2.1.1.js"></script>
			<script src="<?= base_url(); ?>/application/3rdparty/mega-dropdown/js/jquery.menu-aim.js"></script> <!-- menu aim -->
			<script src="<?= base_url(); ?>/application/3rdparty/mega-dropdown/js/main.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript" src="<?=base_url(); ?>js/jquery.cookie.js"></script>
			<script type="text/javascript" src="<?= base_url(); ?>/application/3rdparty/tooltipster/dist/js/tooltipster.bundle.min.js"></script>