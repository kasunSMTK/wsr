<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->SetMargins(5,5,0, true);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Pawning Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();

	$this->pdf->SetFont('helvetica', '', 8);	

		$this->pdf->MultiCell(10, 1, "S.No", 'B','C', 0, 0, '', '', false, '', 0);	
		$this->pdf->MultiCell(13, 1, "Bill Type", 'B','L', 0, 0, '', '', false, '', 0);//
		$this->pdf->MultiCell(24, 1, "Bill No", 'B','C', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(17, 1, "Pawn Date", 'B','C', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(50, 1,"Cus Name / Address", 'B','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(18, 1, "NIC", 'B','L', 0, 0, '', '', '', '', 0);
		$this->pdf->MultiCell(15, 1, "T.Weight", 'B','C', 0, 0, '', '', '', '', 0);
		$this->pdf->MultiCell(15, 1, "P.Weight", 'B','C', 0, 0, '', '', '', '', 0);
		$this->pdf->MultiCell(35, 1, "Articales", 'B','L', 0, 0, '', '', '', '', 0);
		$this->pdf->MultiCell(20, 1, "Pawn Int", 'B','R', 0, 0, '', '', '', '', 0);
		$this->pdf->MultiCell(20, 1, "Int Discount", 'B','R', 0, 0, '', '', '', '', 0);
		$this->pdf->MultiCell(20, 1, "Pawn Amount", 'B','R', 0, 0, '', '', '', '', 0);
		
		if ( $isAdmin == 1 ){
			$dt = "DateTime & ";
		}else{
			$dt = "";
		}


		$this->pdf->MultiCell(30, 1,$dt. "App No", 'B','R', 0, 1, '', '', '', '', 0);

	$this->pdf->SetFont('', '', 7);	



	$no = 1;
	$tot_amt = $TWeight = 0;
	$sub_tot_loop_start = false;
	$first_r = false;
	$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_int = $sub_tot_int_dis = 0;


	foreach($list as $r){

		if ( trim(strtoupper($r->bc)) != trim(strtoupper($st)) ){
			
			if (sub_tot_loop_start){
				if ($first_r){
					
					$this->pdf->SetFont('helvetica', 'B', 8);
					$this->pdf->MultiCell(10, 1, "", 'B','C', 0, 0, '', '', false, '', 0);	
					$this->pdf->MultiCell(13, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
					$this->pdf->MultiCell(24, 1, "", 'B','C', 0, 0, '', '', false, '', 0);
					$this->pdf->MultiCell(17, 1, "", 'B','C', 0, 0, '', '', false, '', 0);
					$this->pdf->MultiCell(50, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
					$this->pdf->MultiCell(18, 1, "Total", 'B','L', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(15, 1, d3($sub_tot_tw), 'B','C', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(15, 1, d3($sub_tot_pw), 'B','C', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(35, 1, "", 'B','L', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(20, 1, d($sub_tot_int), 'B','R', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(20, 1, d($sub_tot_int_dis), 'B','R', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(20, 1, d($sub_tot_la), 'B','R', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(30, 1, "", 'B','R', 0, 1, '', '', '', '', 0);

					$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_int = $sub_tot_int_dis = 0;
				}

				$first_r = true;
			}

			$sub_tot_tw += $r->totalweight;
			$sub_tot_pw += $r->totalpweight;
			$sub_tot_la += $r->requiredamount;
			$sub_tot_int += round($r->fmintrest - $r->int_discount);
			$sub_tot_int_dis += $r->int_discount;

			
			
			
			$bc_name = $r->bc_name;
			$st 	 = $r->bc;
			$sub_tot_loop_start = false;

			$this->pdf->SetFont('helvetica', 'B', 8);	
			$this->pdf->MultiCell(0,5, $bc_name, $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);		

		}else{
			$sub_tot_tw += $r->totalweight;					
			$sub_tot_pw += $r->totalpweight;					
			$sub_tot_la += $r->requiredamount;
			$sub_tot_int += round($r->fmintrest - $r->int_discount);
			$sub_tot_int_dis += $r->int_discount;

			$bc_name = "";
			$sub_tot_loop_start = true;
		}

		$this->pdf->SetFont('', '', 8);

		$h = 4 * (max(1,$this->pdf->getNumLines($r->bc_name,25),$this->pdf->getNumLines($r->items,35),$this->pdf->getNumLines($r->cusname,50)));
		
		$this->pdf->MultiCell(10, $h, $no , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(13, $h, $r->billtype , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(24, $h, $r->billno , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(17, $h, $r->ddate , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(50,$h, $r->cusname. " - " . $r->address , $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(18, $h, $r->nicno , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$this->pdf->MultiCell(15, $h, d3($r->totalweight) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(15, $h, d3($r->totalpweight) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$this->pdf->MultiCell(35, $h, $r->items , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$this->pdf->MultiCell(20, $h, d($r->fmintrest - round($r->int_discount)) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$this->pdf->MultiCell(20, $h, d($r->int_discount) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, d($r->requiredamount) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		

		if ( $isAdmin == 1 ){
			
			if ($r->app_no != ""){				
				$dta = $r->action_date." (".$r->app_no.")";
			}else{
				$dta = "";
				$dta = $r->action_date;
			}

		}else{
			if ($r->app_no != ""){
				$dta = "(".$r->app_no.")";
			}else{
				$dta = "";
			}
		}


		$this->pdf->MultiCell(30, $h, $dta , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);

		$no++;

		$tot_amt += $r->requiredamount;
		$TWeight += $r->totalweight;

		$sub_tot_tw_gra += $r->totalweight;					
		$sub_tot_pw_gra += $r->totalpweight;					
		$sub_tot_la_gra += $r->requiredamount;
		$sub_tot_int_gra += round($r->fmintrest - $r->int_discount);
		$sub_tot_int_dis_gra += $r->int_discount;


	}
	

	$this->pdf->SetFont('helvetica', 'B', 9);		
	$this->pdf->MultiCell(114, 1,"", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(18, 1, "Total", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($sub_tot_tw), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($sub_tot_pw), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(35, 1, "", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($sub_tot_int), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($sub_tot_int_dis), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($sub_tot_la), 'B','R', 0, 1, '', '', '', '', 0);


	$this->pdf->ln(3);


	$this->pdf->SetFont('helvetica', 'B', 9);		
	$this->pdf->MultiCell(104, 1,"", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(28, 1, "All Branch Total", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($sub_tot_tw_gra), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($sub_tot_pw_gra), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(35, 1, "", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($sub_tot_int_gra), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($sub_tot_int_dis_gra), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($sub_tot_la_gra), 'B','R', 0, 1, '', '', '', '', 0);



	






























	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}





	$this->pdf->Output("partpayment_and_renew.pdf", 'I');



?>