<?php

    
    
    $t = '<table border="1">';   
    
    $t .= '<tr>';
    $t .= '<td colspan="11"><h2>Customer Pawning Information Report</h2></td>';
    $t .= '</tr>';


    $t .= '<tr>';
    $t .= '<td>BC/S</td>';
    $t .= '<td width="200">Bill No</td>';
    $t .= '<td>Pawn Date</td>';
    $t .= '<td>Total Weight</td>';
    $t .= '<td>Pure Weight</td>';
    $t .= '<td>Articales</td>';
    $t .= '<td>Status</td>';
    $t .= '<td>Loan Amount</td>';
    $t .= '<td>Rate</td>';
    $t .= '<td>Re. Amount</td>';
    $t .= '<td>Re. Date</td>';
    $t .= '</tr>';

    $s = '';

    foreach ($list as $r) {
        
        $rate = ($r->requiredamount * 8)/$r->pure_weight_tot;

        $t .= '<tr>';
        $t .= '<td>'.$r->bc_name.'</td>';
        $t .= '<td>'.$r->billno.'</td>';
        $t .= '<td>'.$r->pawn_date.'</td>';
        $t .= '<td>'.d3($r->totalweight).'</td>';
        $t .= '<td>'.d3($r->pure_weight_tot).'</td>';
        $t .= '<td>'.$r->items.'</td>';
        $t .= '<td>'.$r->status.'</td>';
        $t .= '<td>'.d2($r->requiredamount).'</td>';
        $t .= '<td>'.d2($rate).'</td>';
        $t .= '<td>'.d2($r->redeem_amount).'</td>';
        $t .= '<td>'.$r->redeem_date.'</td>';        
        $t .= '</tr>';


    }



    $t .= '</table>';


    header('Content-type: application/excel');
    $filename = 'income_statement.xls';
    header('Content-Disposition: attachment; filename='.$filename);
    $data = $t;
    echo $data;
    exit;


    function d2($number) {
        return number_format($number, 2, '.', ',');
    }


       
    
    foreach($list as $r){
        
        $Q1 = $this->db->query("SELECT C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,C.`customer_id`,L.int_cal_changed,L.cus_serno,L.am_allow_frst_int FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '".$r->billno."' AND L.bc = '".$r->bc."' LIMIT 1 ");
        $a['loan_sum'] = $Q1->row();

        //int_balance

        $int_bal = 0; // remove the comment if outstanding amount needed

        /*if (is_null($a['loan_sum'])){
            $int_bal = 0;
        }else{
            $payable_int_bal = $X->calculate->
            interest($r->loanno,$return_mode = "json",$td, $a['loan_sum'] , $five_days_int_cal=0);
            $int_bal = $payable_int_bal['int_balance'];
        }*/     

        if ($r->bc != $st){
            // show     

            if (sub_tot_loop_start){
                if ($first_r){
                    $this->pdf->SetFont('helvetica', 'B', 8);                       
                    $this->pdf->set_tot_line(d3($sub_tot_tw), d3($sub_tot_pw), d($sub_tot_la), d($sub_tot_os), d($sub_tot_ra) );
                    $sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_os = $sub_tot_ra = 0;
                }

                $first_r = true;
            }

            $sub_tot_tw += $r->totalweight;     
            $sub_tot_pw += $r->pure_weight_tot;
            $sub_tot_la += $r->requiredamount;
            $sub_tot_os += $int_bal;
            $sub_tot_ra += $r->redeem_amount;
            
            $bc_name = $r->bc_name;
            $st      = $r->bc;
            $sub_tot_loop_start = false;

            $this->pdf->SetFont('helvetica', 'B', 8);   
            $this->pdf->MultiCell(0,5, $bc_name, $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);      

        }else{
            $sub_tot_tw += $r->totalweight;     
            $sub_tot_pw += $r->pure_weight_tot;
            $sub_tot_la += $r->requiredamount;
            $sub_tot_os += $int_bal;
            $sub_tot_ra += $r->redeem_amount;

            $bc_name = "";
            $sub_tot_loop_start = true;
        }

        $h = 5 * (max(1,$this->pdf->getNumLines(str_replace(",", "<br>", $r->items),55),$this->pdf->getNumLines($r->cusname,80)));

        $rate = ($r->requiredamount * 8)/$r->pure_weight_tot;
        
        $this->pdf->SetFont('helvetica', '', 7);
        $this->pdf->MultiCell(15, $h, $no , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);      
        $this->pdf->MultiCell(25, $h, $r->billno , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, $r->pawn_date , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, $r->totalweight , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, $r->pure_weight_tot , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(55, $h, $r->items, $border='1', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, $r->status, $border='1', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, $r->requiredamount , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, d($rate) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, $r->redeem_amount , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, $r->redeem_date , $border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        
        $T_totalweight      += $r->totalweight;
        $T_totalpureweight  += $r->pure_weight_tot;
        $T_requiredamount   += $r->requiredamount;
        $T_redeem_amount    += $r->redeem_amount;
        $T_rate             += $rate;

        $no++;
        
    }

    $this->pdf->SetFont('helvetica', 'B', 8);   
    
        $this->pdf->MultiCell(15, $h, "" , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);       
        $this->pdf->MultiCell(25, $h, "" , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, "" , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, d3($T_totalweight) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, d3($T_totalpureweight) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(55, $h, "", $border='1', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, "", $border='1', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, d($T_requiredamount) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, d($T_rate) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, d($T_redeem_amount) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
        $this->pdf->MultiCell(20, $h, "" , $border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);


    function get_int_bal($X,$loan_no,$int_cal_to_DATE,$LOAN_DATA){
        return $payable_int_bal;
    }

    function d($number) {
        return number_format($number, 2, '.', ',');
    }

    function d3($number) {
        return number_format($number, 3, '.', '');
    }

    $this->pdf->Output("PDF.pdf", 'I');

?>