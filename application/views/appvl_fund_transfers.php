<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $( ".date_ch_allow" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2025",
            onSelect: function(date){
                change_current_date(date);              
            }
        });
    });

    $(function() {
        $( ".date_ch_allow_n" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            minDate:'<?=$backdate_upto?>',
            maxDate:0,
            yearRange: "1930:2025",
            onSelect: function(date){
                change_current_date(date);              
            }
        });
    });

    function date_change_response_call(){
        
    }

</script>

<div class="page_contain_new_pawn">

	<div class='app_popup_bg'></div>
	<div class='app_popup'>
		
		<div class='app_c'>
			<span style='font-size: 14px;font-weight: bold'>Branch cash request forward</span><br><br><br>
			<div class="bc_cash_balance_grid"></div><br>
			Requested amount <span class='requ_amt' style="color:red"></span><br><br>

			<input name="btnForward_bc_request" type="button" value="Forward" id="btnForward_bc_request" class="btn_regular" style="margin-right: 340px" />
			<input name="btnCancelForward_bc_request" type="button" value="Close" id="btnCancelForward_bc_request" class="btn_regular"/>
		</div>

	</div>

	<div class="discount_title_holder">		
		<a href="?action=appvl_fund_transfers" class="approval_tab app_requ_link" 			style="width:150px;text-align:left"><span style="font-family: bitter; font-size: 19px;">New</span></a>
		<a href="?action=appvl_fund_transfers_approved" class="approval_tab app_requ_link" style="width:150px;text-align:left"><span style="font-family: bitter; font-size: 19px; color: #999999 ">Approved</span></a>
	
		<div style="width:590px;border:0px solid red;float:right;text-align: right"><span style="font-family: bitter; font-size: 19px; ">Cheque Withdrawal and Bank to Bank Transfer Approval</span></div>

		<!-- <div style="float: right"><input class="input_text_regular_new_pawn <?=$date_change_allow?>" type="text" id="dDate" name="dDate" style="width:80px" readonly="readonly" value="<?=$current_date?>"></div> -->

	</div><br>

	<table class="tbl_incmg_req" align="center" border="01" width="1000" cellpadding="0" cellspacing="0">    
	    <thead>
	        <tr>                                                                                               
	            <td>No</td>
	            <td>Transfer Type</td>
	            <td>Transfer No</td>
	            <td>Branch</td>
	            <td>Account Description</td>
	            <td align='center'>Cheque Number</td>
	            <td align='right'>Amount</td>
	            <td align='left'>Comment</td>
	            <td align='center'>Date and Time</td>
	            <td align='right'>Action</td>            
	        </tr>
	    </thead>                                                        
		
		<tbody class="discount_requests"> </tbody>
    
    </table>

</div>

	<input type="hidden" id="aa">
	<input type="hidden" id="bb">
	<input type="hidden" id="cc">
	<input type="hidden" id="dd">

</body>
</html>