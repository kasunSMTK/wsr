
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>
	$(function() {
		$( "#stampdate" ).datepicker({

			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:''"

		});
	});
</script>

<div class="page_contain">
	<div class="page_main_title"><span>Stamp Fee</span></div><br>

	<form method="post" action="<?=base_url()?>index.php/main/save/r_stampfee" id="form_">
		<table cellpadding="0" cellspacing="0" border="1" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add Stamp Fee</b>
				</td>
				<td>
					<b>Exist Stamp Fee</b>
				</td>			
			</tr>		

			<tr>
				<td>
					<div class="text_box_holder">
						<div class="text_box_title_holder">Branch</div> 
						<?=$bc_dropdown?>
					</div>
				</td>			
				<td rowspan="8" valign="top">					
					<div class="list_div">
						<!-- <?=$load_list?> -->
					</div>
				</td>
			</tr>			

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Amount From</span> 
						<input class="currency amount input_text_regular" type="text" name="fromvalue" id="fromvalue" maxlength="11">
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Amount To</span> 
						<input class="currency amount input_text_regular" type="text" name="tovalue" id="tovalue" maxlength="11">
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Stamp Fee</span> 
						<input class="currency amount input_text_regular" type="text" name="stampfee" id="stampfee" maxlength="7">
					</div>
				</td>			
			</tr>			

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Stamp Date</span> 
						<input class="currency input_text_regular" type="text" name="stampdate" id="stampdate">
					</div>
				</td>			
			</tr>			

			<tr>
				<td>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>			
				<td>
					
				</td>
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>