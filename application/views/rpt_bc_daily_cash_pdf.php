<?php
	
	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	$this->pdf->MultiCell(0, 0, "Branch Daily Cash Balance Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between ".$fd . ' and ' . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();

	$this->pdf->SetFont('helvetica', '', 10);
	
	$this->pdf->MultiCell(50, 1, "Branch/S.No", 'B','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(25, 1, "Date", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Amount", 'B','R', 0, 1, '', '', '', '', 0);

	$no = $bc_count = 1;
	$st = ""; 
	$rate=(float)0;

	$sub_tot_loop_start = false;
	$first_r = false;
	$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_os = $sub_tot_ra = 0;

	$T_totalweight = $T_totalpureweight = $T_requiredamount = $T_redeem_amount = 0;

	foreach($list as $r){
		
		$Q1 = $this->db->query("SELECT C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,C.`customer_id`,L.int_cal_changed,L.cus_serno,L.am_allow_frst_int FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '".$r->billno."' AND L.bc = '".$r->bc."' LIMIT 1 ");
		$a['loan_sum'] = $Q1->row();

		//int_balance

		$int_bal = 0; // remove the comment if outstanding amount needed		

		if ($r->bc != $st){
			// show		

			if (sub_tot_loop_start){
				if ($first_r){
					$this->pdf->SetFont('helvetica', 'B', 10);						
					//$this->pdf->set_tot_line(d3($sub_tot_tw), d3($sub_tot_pw), d($sub_tot_la), d($sub_tot_os), d($sub_tot_ra) );
					$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_os = $sub_tot_ra = 0;
				}

				$first_r = true;
			}

			$sub_tot_tw += $r->totalweight;		
			$sub_tot_pw += $r->pure_weight_tot;
			$sub_tot_la += $r->requiredamount;
			$sub_tot_os += $int_bal;
			$sub_tot_ra += $r->redeem_amount;
			
			$bc_name = $r->bc_name;
			$st 	 = $r->bc;
			$sub_tot_loop_start = false;

			$this->pdf->SetFont('helvetica', 'B', 10);	
			$this->pdf->MultiCell(105,5, $bc_name." (".$bc_count.")", $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);		
			$bc_count ++;
		}else{
			$sub_tot_tw += $r->totalweight;		
			$sub_tot_pw += $r->pure_weight_tot;
			$sub_tot_la += $r->requiredamount;
			$sub_tot_os += $int_bal;
			$sub_tot_ra += $r->redeem_amount;

			$bc_name = "";
			$sub_tot_loop_start = true;
		}

		$h = 5 * (max(1,$this->pdf->getNumLines(str_replace(",", "<br>", $r->items),55),$this->pdf->getNumLines($r->cusname,80)));

		
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->MultiCell(50, $h, $no , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);		
		$this->pdf->MultiCell(25, $h, $r->date , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(30, $h, $r->amount , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		
		
		
		$T_totalweight 		+= $r->totalweight;
		$T_totalpureweight 	+= $r->pure_weight_tot;
		$T_requiredamount 	+= $r->requiredamount;
		$T_redeem_amount 	+= $r->redeem_amount;
		$T_rate				+= $rate;

		$no++;
		
	}

	
		


	function get_int_bal($X,$loan_no,$int_cal_to_DATE,$LOAN_DATA){
		return $payable_int_bal;
	}

	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}

	$this->pdf->Output("PDF.pdf", 'I');

?>