<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->SetMargins(5, 5, 0, true);
$this->pdf->AddPage();

$this->pdf->SetFont('helvetica', '', 28);
$this->pdf->setY(10);
$this->pdf->setX(0);

$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
$this->pdf->Line(5, 0, 0, 0, $style);

$this->pdf->MultiCell(0, 0, "Sales Return Summery Report", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 15);
$this->pdf->MultiCell(0, 0, "Between " . $fd . " and " . $td, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->setX(10);
$this->pdf->ln();

$this->pdf->SetFont('helvetica', 'B', 8);

$this->pdf->MultiCell(10, 1, "No", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, 1, "cus.SerNo", '1', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(40, 1, "Customer", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(40, 1, "Store", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(50, 1, "Employee", '1', 'L', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(20, 1, "Note  ", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(25, 1, "T.Amount", '1', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(25, 1, "T.Discount", '1', 'L', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(25, 1, "N.Amount", '1', 'R', 0, 1, '', '', '', '', 0);

$this->pdf->SetFont('', '', 7);

$bc = "";
$toal = 0;
$discount = 0;
$net = 0;
foreach ($det as $r) {

    $height = 2 * (max(1, $this->pdf->getNumLines($r->cusname, 30)));
    if ($bc != $r->bc) {
        $this->pdf->SetFont('helvetica', 'B', 8);
        $this->pdf->MultiCell(10, $height, $r->bc, '0', 'C', 0, 1, '', '', false, '', 0);
    }
    $this->pdf->SetFont('helvetica', '', 8);

    $this->pdf->MultiCell(10, $height, $r->nno, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(20, $height, $r->cus_serno, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(40, $height, $r->cusname, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(40, $height, $r->store . " - " . $r->store_name, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(50, $height, $r->employee . " - " . $r->emp_name, '1', 'L', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(20, $height, $r->note, '1', 'C', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(25, $height, number_format($r->total_amount, 2), '1', 'R', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(25, $height, number_format($r->total_discount, 2), '1', 'R', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(25, $height, number_format($r->net_amount, 2), '1', 'R', 0, 1, '', '', '', '', 0);
    $bc = $r->bc;
    $toal += $r->total_amount;
    $discount += $r->total_discount;
    $net += $r->net_amount;

}

$this->pdf->SetFont('helvetica', 'B', 9);
$this->pdf->MultiCell(10, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(40, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(40, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(50, $height, "", '0', 'L', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(20, $height, "Total", '0', 'C', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(25, $height, number_format($toal, 2), 'B', 'R', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(25, $height, number_format($discount, 2), 'B', 'R', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(25, $height, number_format($net, 2), 'B', 'R', 0, 1, '', '', '', '', 0);
function d($number)
{
    return number_format($number, 2, '.', ',');
}

function d3($number)
{
    return number_format($number, 3, '.', '');
}

$this->pdf->Output("PDF.pdf", 'I');
