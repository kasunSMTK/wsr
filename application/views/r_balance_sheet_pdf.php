<?php

    $this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
    $this->pdf->SetPrintHeader(false);
    $this->pdf->SetPrintFooter(false);
    $this->pdf->AddPage();  

    $this->pdf->SetFont('helvetica', '', 28);   
    $this->pdf->setY(10);
    $this->pdf->setX(0);    

    $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));

    $this->pdf->Line(5, 0, 0, 0, $style);

    $this->pdf->MultiCell(0, 0, "Balance Sheet",   $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

    $this->pdf->SetFont('helvetica', '', 15);
    $this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td ,   $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

    



    








    $this->pdf->setX(0);
    $this->pdf->ln();
    $this->pdf->SetFont('helvetica', 'B', 14);
    $this->pdf->MultiCell(80, 1, "ASSET", '0','L', 0, 1, '', '', '', '', 0);

    $s1 = '';
    $tot1 = 0;

    foreach($asset as $r){
    
        if ( $r->heading != $s1 ){
            $this->pdf->ln();
            $this->pdf->SetFont('helvetica', 'B', 10);            
            $this->pdf->MultiCell(5, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
            $this->pdf->MultiCell(0, 0, $r->heading,   $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            $this->pdf->SetFont('helvetica', '', 8);
            $s1 = $r->heading;
        }
        
        $this->pdf->MultiCell(10, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
        $this->pdf->MultiCell(150, 0, $r->description." (".$r->code.")",   $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
        $this->pdf->MultiCell(30, 0, d($r->bal,2),  $border = 'B', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        $tot1 += floatval($r->bal);

    }
    
    $this->pdf->SetFont('helvetica', 'B', 10);
    $this->pdf->ln(4);
    $this->pdf->MultiCell(5, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
    $this->pdf->MultiCell(150, 0, "Total Asset",  $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    $this->pdf->MultiCell(5, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
    $this->pdf->MultiCell(30, 0, d($tot1,2),  $border = 'B', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    $this->pdf->SetFont('helvetica', '', 8);









    











    $this->pdf->ln(14);
    $this->pdf->SetFont('helvetica', 'B', 14);    
    $this->pdf->MultiCell(80, 0, strtoupper("Liabilities"),  $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    
    $s2 = '';
    $tot2 = 0;

    foreach($liabilities as $r){
    
        if ( $r->heading != $s2 ){
            $this->pdf->ln();
            $this->pdf->SetFont('helvetica', 'B', 10);            
            $this->pdf->MultiCell(5, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
            $this->pdf->MultiCell(80, 0, $r->heading,  $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

            $this->pdf->SetFont('helvetica', '', 8);
            $s2 = $r->heading;
        }           

        $this->pdf->MultiCell(10, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
        $this->pdf->MultiCell(150, 0, $r->description." (".$r->code.")",  $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        $this->pdf->MultiCell(30, 0, d($r->bal,2),  $border = 'B', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        $tot2 += floatval($r->bal);

    }

    $this->pdf->ln(5);

    //----------------------------------------

    $s3 = '';
    $tot3 = 0;
    $tot4 = 0;

    foreach($profit as $r){        
        $this->pdf->MultiCell(10, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
        $this->pdf->MultiCell(150, 0, "Profit of the period",  $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
        $this->pdf->MultiCell(30, 0, d($r->bal,2),  $border = 'B', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        $this->pdf->SetFont('helvetica', '', 8);
        $tot3 += $r->bal;
    }

        $this->pdf->MultiCell(10, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
        $this->pdf->MultiCell(150, 0, "Retained Profit/Loss",  $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
        $this->pdf->MultiCell(30, 0, d($profit0,2),  $border = 'B', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        $this->pdf->SetFont('helvetica', '', 8);
        $tot4 += $profit0;
        
    //----------------------------------------


    $this->pdf->SetFont('helvetica', 'B', 10);
    $this->pdf->ln(4);
    $this->pdf->MultiCell(5, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
    $this->pdf->MultiCell(150, 0, "Total Liabilities", 'B','L', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(5, 0, "",   $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);        
    $this->pdf->MultiCell(30, 0, d($tot2+$tot3+$tot4,2) , 'B','R', 0, 1, '', '', '', '', 0);
    $this->pdf->SetFont('helvetica', '', 8);






    











    


    function d($number) {
        return number_format($number, 2, '.', ',');
    }

    function d3($number) {
        return number_format($number, 3, '.', '');
    }





    $this->pdf->Output("PDF.pdf", 'I');



?>