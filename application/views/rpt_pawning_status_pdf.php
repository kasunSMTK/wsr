<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->SetMargins(5, 5, 0, true);
$this->pdf->AddPage();

$this->pdf->SetFont('helvetica', '', 28);
$this->pdf->setY(10);
$this->pdf->setX(0);

$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
$this->pdf->Line(5, 0, 0, 0, $style);

$this->pdf->MultiCell(0, 0, "Pawning Status Report", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 15);
$this->pdf->MultiCell(0, 0, "Between " . $fd . " and " . $td, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->setX(0);
$this->pdf->ln();

$this->pdf->SetFont('helvetica', 'B', 10);
$this->pdf->setX(20);
$this->pdf->MultiCell(20, 1, "S.No", 'B', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "Pawning Date", 'B', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, 1, "Bill Type", 'B', 'L', 0, 0, '', '', false, '', 0); //
$this->pdf->MultiCell(24, 1, "Bill No", 'B', 'C', 0, 0, '', '', false, '', 0);

$this->pdf->MultiCell(24, 1, "Loan No", 'B', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30    , 1, "Status", 'B', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30    , 1, "Description", 'B', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30    , 1, "Amount", 'B', 'R', 0, 1, '', '', '', '', 0);

$this->pdf->SetFont('', '', 7);

$no = 1;
$tot_amt = $TWeight = 0;
$sub_tot_loop_start = false;
$first_r = false;
$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $ab = $sub_tot_int_dis = 0;

foreach ($list as $r) {
    $h = 4 * (max(1,$this->pdf->getNumLines($r->bc_name,25)));
    $this->pdf->HaveMorePages($h);
    if (trim(strtoupper($r->bc)) != trim(strtoupper($st))) {

        if (sub_tot_loop_start) {
            if ($first_r) {
                $this->pdf->setX(20);
                $this->pdf->SetFont('helvetica', 'B', 8);
                $this->pdf->MultiCell(20, 1, "", 'B', 'C', 0, 0, '', '', false, '', 0);
                $this->pdf->MultiCell(30, 1, "", 'B', 'C', 0, 0, '', '', false, '', 0);
                $this->pdf->MultiCell(20, 1, "", 'B', 'L', 0, 0, '', '', false, '', 0); 
                $this->pdf->MultiCell(24, 1, "", 'B', 'C', 0, 0, '', '', false, '', 0);

                $this->pdf->MultiCell(24, 1, "", 'B', 'C', 0, 0, '', '', false, '', 0);
                $this->pdf->MultiCell(30, 1, "", 'B', 'C', 0, 0, '', '', false, '', 0);
                $this->pdf->MultiCell(30, 1, "Total", 'B', 'C', 0, 0, '', '', false, '', 0);
                $this->pdf->MultiCell(30, 1, d3($sub_tot_tw), 'B', 'R', 0, 1, '', '', '', '', 0);

                $sub_tot_tw = $sub_tot_pw = $sub_tot_la = $ab = $sub_tot_int_dis = 0;
            }

            $first_r = true;
        }

        $sub_tot_tw += $r->amount;

        $bc_name = $r->bc_name;
        $st = $r->bc;
        $sub_tot_loop_start = false;
        $this->pdf->setX(20);
        $this->pdf->SetFont('helvetica', 'B', 8);
        $this->pdf->MultiCell(25, 5, $bc_name, $border = 'B', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);

    } else {
        $sub_tot_tw += $r->amount;

        $bc_name = "";
        $sub_tot_loop_start = true;
    }

    $this->pdf->SetFont('helvetica', '', 8);
    $this->pdf->setX(20);
    $this->pdf->MultiCell(20, $h, $no, 'B', 'C', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $h, $r->ddate, 'B', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(20, $h, $r->billtype, 'B', 'L', 0, 0, '', '', false, '', 0); 
    $this->pdf->MultiCell(24, $h, $r->billno, 'B', 'L', 0, 0, '', '', false, '', 0);

    $this->pdf->MultiCell(24, $h, $r->loanno, 'B', 'R', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $h, $r->status, 'B', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $h, $r->description, 'B', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $h, $r->amount, 'B', 'R', 0, 1, '', '', '', '', 0);
    if ($isAdmin == 1) {

        if ($r->app_no != "") {
            $dta = $r->action_date . " (" . $r->app_no . ")";
        } else {
            $dta = "";
            $dta = $r->action_date;
        }

    } else {
        if ($r->app_no != "") {
            $dta = "(" . $r->app_no . ")";
        } else {
            $dta = "";
        }
    }

    // $this->pdf->MultiCell(30, $h, $dta , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);

    $no++;

    $tot_amt += $r->amount;

}
$this->pdf->setX(20);
$this->pdf->SetFont('helvetica', 'B', 8);
$this->pdf->MultiCell(20, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, 1, "", '', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(24, 1, "", '', 'C', 0, 0, '', '', false, '', 0);

$this->pdf->MultiCell(24, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "Total", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, d3($sub_tot_tw), 'B', 'R', 0, 1, '', '', '', '', 0);

$this->pdf->ln(3);
$this->pdf->setX(20);
$this->pdf->SetFont('helvetica', 'B', 8);
$this->pdf->MultiCell(20, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, 1, "", '', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(24, 1, "", '', 'C', 0, 0, '', '', false, '', 0);

$this->pdf->MultiCell(24, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "Grand Total", '', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, d3($tot_amt), 'B', 'R', 0, 1, '', '', '', '', 0);

function d($number)
{
    return number_format($number, 2, '.', ',');
}

function d3($number)
{
    return number_format($number, 3, '.', '');
}

$this->pdf->Output("PDF.pdf", 'I');
