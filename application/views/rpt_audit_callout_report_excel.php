<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

ini_set('max_execution_time', 1200);

/*var_dump($list);
exit;*/

$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading("Audit Callout Report");

$r  =  $this->excel->NextRowNum();

$c=$d=$j=$b=0; $billType_tot=(float)0; 
$sub_tot[]='';

$lettr = array("B","C","D","E", "F", "G","H", "I","J", "K","L", "M","N", "O","P", "Q","R", "S","T", "U","V", "W","X", "Y","Z", "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",);

    $x = 0;
   
    $r  =  $this->excel->NextRowNum();
    
    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("A".$r,"As at ".$td);

    $r  =  $this->excel->NextRowNum();
    
    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("A".$r,"Serial No");

    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("B".$r,"Branch");

    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("C".$r,"Date");
    
    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("D".$r,"Bill Type");

    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(18)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("E".$r,"Bill Number");

    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(60)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("F".$r,"Articles Description");

    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(17)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("G".$r,"Pawning Advance");

    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("H".$r,"Item Weight (Grams)");

    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("I".$r,"Item P.Weight (Grams)");

    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("J".$r,"Bill Total Weight (Grams)");

    $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15)->setAlign;
    $this->excel->getActiveSheet()->setCellValue("K".$r,"Number of Days");
    

    $this->excel->SetBlank();

    $r  =  $this->excel->NextRowNum();

    $st = '';

    $ra = $tw = $pw = $bt = 0;

    foreach($list as $value){

        $no_of_days = $this->db->query("select DATEDIFF(current_date,'".$value->ddate."') as n")->row()->n;
        
        
        if ($value->billno != $st){

            $this->excel->getActiveSheet()->setCellValue("A".$r,($x+1));
            $this->excel->getActiveSheet()->setCellValue("B".$r,$value->bc_name);
            $this->excel->getActiveSheet()->setCellValue("C".$r, date("d-M-y", strtotime($value->ddate)) );
            $this->excel->getActiveSheet()->setCellValue("D".$r,$value->billtype);
            $this->excel->getActiveSheet()->setCellValue("E".$r,$value->billno);
            $this->excel->getActiveSheet()->setCellValue("F".$r,$value->itemname." (".$value->qty.")");
            $this->excel->getActiveSheet()->setCellValue("G".$r,$value->requiredamount); $ra += $value->requiredamount;          
            $this->excel->getActiveSheet()->setCellValue("H".$r,$value->total_weight);  $tw += $value->total_weight;
            $this->excel->getActiveSheet()->setCellValue("I".$r,$value->pure_weight);   $pw += $value->pure_weight;            
            $this->excel->getActiveSheet()->setCellValue("J".$r,$value->billtotalweight);  $bt += $value->billtotalweight;        
            $this->excel->getActiveSheet()->setCellValue("K".$r,$no_of_days);

            $x++;
            $r++;

            $st = $value->billno;

        }else{

            $this->excel->getActiveSheet()->setCellValue("A".$r,'');
            $this->excel->getActiveSheet()->setCellValue("B".$r,'');
            $this->excel->getActiveSheet()->setCellValue("C".$r,'');
            $this->excel->getActiveSheet()->setCellValue("D".$r,'');
            $this->excel->getActiveSheet()->setCellValue("E".$r,'');
            $this->excel->getActiveSheet()->setCellValue("F".$r,$value->itemname."(".$value->qty.")");
            $this->excel->getActiveSheet()->setCellValue("G".$r,'');
            $this->excel->getActiveSheet()->setCellValue("H".$r,$value->total_weight);  $tw += $value->total_weight;
            $this->excel->getActiveSheet()->setCellValue("I".$r,$value->pure_weight);   $pw += $value->pure_weight;                    
            $this->excel->getActiveSheet()->setCellValue("J".$r,'');
            $this->excel->getActiveSheet()->setCellValue("K".$r,'');   

            
            

            $r++;

        }

        //$this->excel->SetBlank();            

        $r  =  $this->excel->NextRowNum();

        $this->excel->getActiveSheet()->setCellValue("A".$r,'');
        $this->excel->getActiveSheet()->setCellValue("B".$r,'');
        $this->excel->getActiveSheet()->setCellValue("C".$r,'');
        $this->excel->getActiveSheet()->setCellValue("D".$r,'');
        $this->excel->getActiveSheet()->setCellValue("E".$r,'');
        $this->excel->getActiveSheet()->setCellValue("F".$r,'');
        $this->excel->getActiveSheet()->setCellValue("G".$r,$ra);
        $this->excel->getActiveSheet()->setCellValue("H".$r,$tw);
        $this->excel->getActiveSheet()->setCellValue("I".$r,$pw);
        $this->excel->getActiveSheet()->setCellValue("J".$r,$bt);        
        $this->excel->getActiveSheet()->setCellValue("K".$r,'');



    }



    $this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));


    function d3($a){
        return number_format($a,3);
    }

?>