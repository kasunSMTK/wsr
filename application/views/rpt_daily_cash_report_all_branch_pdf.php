<?php


	/*foreach ($list as  $v) {
		echo $v->receipts ." ----------------------" . $v->payments . "<br>" ;
	}

	exit;*/


	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 22);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Daily Cash Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 11);	
	$this->pdf->MultiCell(0, 0, "As at " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();	

	$this->pdf->SetFont('helvetica', '', 8);		

	$this->pdf->MultiCell(10, 1, "BC", 			'T','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(10, 1, "S.No", 			'T','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Description", 	'T','L', 0, 0, '', '', false, '', 0);		
	$this->pdf->MultiCell(15, 1, "Tra.Code", 		'T','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(20, 1, "Bill No", 		'T','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(15, 1, "Bill Type", 		'T','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(22, 1, "Receipts (Rs)", 	'T','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(22, 1, "Payments (Rs)", 	'T','C', 0, 0, '', '', '', '', 0);	
	$this->pdf->MultiCell(22, 1, "Balance (Rs)", 	'T','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(26, 1, "Date Time", 		'T','R', 0, 1, '', '', '', '', 0);

	$no = 1;
	

	foreach ($bc_list as $bc_L) {

		// bc list and op bal
        $Q2 = $this->db->query("SELECT T.bc,B.name,SUM(dr_amount) - SUM(cr_amount) AS `bc_op_bal` FROM `t_account_trans` T JOIN `m_branches` B ON T.`acc_code` = B.`cash_acc` AND T.`bc` = B.`bc` WHERE T.`ddate` < '$td' AND T.bc = '$bc_L' GROUP BY T.`bc` ORDER BY T.bc");	
		
        if ($Q2->num_rows() > 0){
        	$opbal = $Q2->row()->bc_op_bal;
        }else{
        	$opbal = 0;
        }

        $bc_name = $this->db->query("SELECT `name` FROM `m_branches` WHERE bc = '$bc_L' LIMIT 1")->row()->name;

		$this->pdf->SetFont('', 'B', 7);
		$this->pdf->MultiCell(96,5, $bc_L. " - ".$bc_name , 	$border='BT', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);
		$this->pdf->MultiCell(70,5, "Opening Bal B/F (Rs.) - Sample Data:   ".number_format($opbal,2) , 				$border='BT', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);		
		$this->pdf->MultiCell(30,5, "" , 									$border='BT', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);				
		$this->pdf->SetFont('', '', 7);

		foreach($list as $r){
			if ($r->bc == $bc_L){			
				$h = 4 * (max(1,$this->pdf->getNumLines($r->description,30),$this->pdf->getNumLines($r->receipts,25)));
		
				$opbal += $r->receipts;
				$opbal -= $r->payments;

				$this->pdf->MultiCell(20, $h, $no , 					$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
				$this->pdf->MultiCell(30, $h, $r->description , 		$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
				$this->pdf->MultiCell(15, $h, $r->entry_code , 			$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);		
				$this->pdf->MultiCell(20, $h, $r->billno , 				$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
				$this->pdf->MultiCell(15, $h, $r->billtype , 			$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
				$this->pdf->MultiCell(22, $h, $r->receipts , 			$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
				$this->pdf->MultiCell(22, $h, $r->payments , 			$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
				$this->pdf->MultiCell(22, $h, number_format($opbal,2) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
				$this->pdf->MultiCell(30, $h, $r->action_date , 		$border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);				
			
				$no++;
			}
		}

		$this->pdf->SetFont('', 'B', 7);
		$this->pdf->MultiCell(96,5, "" , 																$border='T', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);
		$this->pdf->MultiCell(70,5, "Closing Bal B/F (Rs.) - Sample Data:   ".number_format($opbal,2) , $border='T', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);		
		$this->pdf->MultiCell(30,5, "" , 																$border='T', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);				
		$this->pdf->SetFont('', '', 7);
		
		$this->pdf->ln();

	}



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function get_bc_op_bal($bc){

		return 15000.00;

	}

	$this->pdf->Output("PDF.pdf", 'I');

?>