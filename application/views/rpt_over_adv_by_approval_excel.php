<?php

	$make_excel = 1;

	$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
  	$t .= '<table border="1" class="tbl_income_statment" cellspacing="0" cellpadding="0" borderColor="#CCCCCC">';
	
  	$t.='<tr><td colspan="11"><b style="font-size:24px">Over Advance by Approval</b></td></tr>';	

	$bcs = '';
	$ea=$tw=$pw=$la=0;
	$ALLea=$ALLtw=$ALLpw=$ALLla=0;
	$first_row_passed = false;

	foreach ($det as $r) {		

		if ( $bcs != $r->bc ){

			if ($first_row_passed){

				$t.= '<tr>';
				$t.= '<td></td>';
				$t.= '<td></td>';
				$t.= '<td>Total</td>';
				$t.= '<td align="right"><b>'. number_format(abs(d($ea)),2) . '</b></td>';
				$t.= '<td><b>'. number_format($la,2) . '</b></td>';
				$t.= '<td></td>';
				$t.= '<td></td>';
				$t.= '<td align="center"><b>'. d($tw) . '</b></td>';
				$t.= '<td align="center"><b>'. d($pw) . '</b></td>';
				$t.= '<td align="center"></td>';
				$t.= '<td></td>';
				$t.= '<td></td>';
				$t.= '<td></td>';
				$t.= '<td></td>';
				$t.= '</tr>';
				

				$ea=$tw=$pw=$la=0;

			}

			$first_row_passed = true;

			
			$t.='<tr><td colspan="11"><b>'.$r->bc.'</b></td></tr>';	

			$t.='<tr>';
			$t.='<td>'."<b>Branch".'</b></td>';
			$t.='<td>'."<b>Pawn Date".'</b></td>';
			$t.='<td>'."<b>Bill No".'</b></td>';
			$t.='<td align="right">'."<b>Extra Amount".'</b></td>';
			$t.='<td>'."<b>Loan Amount".'</b></td>';
			$t.='<td>'."<b>Quality".'</b></td>';
			$t.='<td>'."<b>Articles".'</b></td>';
			$t.='<td align="center">'."<b>Total Weight".'</b></td>';
			$t.='<td align="center">'."<b>Pure Weight".'</b></td>';
			$t.='<td align="center"><b>Gold Content</b></td>';
			$t.='<td>'."<b>Customer Name".'</b></td>';
			$t.='<td>'."<b>Address".'</b></td>';
			$t.='<td>'."<b>Approved by".'</b></td>';
			$t.='<td>'."<b>Operator".'</b></td>';
			$t.='</tr>';
			

			$bcs = $r->bc;
		}		
		
		$t.='<tr>';
		$t.='<td>'. $r->bc.'</td>';
		$t.='<td>'. $r->ddate.'</td>';
		$t.='<td>'. $r->billno.'</td>';
		$t.='<td align="right">'. number_format((d($r->extra_amount)),2).'</td>';
		$t.='<td>'. number_format($r->requiredamount,2).'</td>';
		
		$cc = $r->quality_changed;		

		$t.='<td align="center">'. $cc .'</td>';
		$t.='<td>'. $r->items.'</td>';
		$t.='<td align="center">'. $r->total_weight.'</td>';
		$t.='<td align="center">'. $r->pure_weight.'</td>';
		$t.='<td align="center">'. $r->printval.'</td>';
		$t.='<td>'. $r->cusname.'</td>';
		$t.='<td>'. $r->address.'</td>';
		$t.='<td>'. $r->approved_by.'</td>';
		$t.='<td>'. $r->requested_by.'</td>';
		$t.='</tr>';

		
		$ea += $r->extra_amount;
		$tw += $r->total_weight;
		$pw += $r->pure_weight;
		$la += $r->requiredamount;

		$ALLea += $r->extra_amount;
		$ALLtw += $r->total_weight;
		$ALLpw += $r->pure_weight;
		$ALLla += $r->requiredamount;
		
			
	}
		$t.= '<tr>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td>Total</td>';
		$t.= '<td align="right"><b>'. number_format(abs(d($ea)),2) . '</b></td>';
		$t.= '<td><b>'. number_format($la,2) . '</b></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td align="center"><b>'. d($tw) . '</b></td>';
		$t.= '<td align="center"><b>'. d($pw) . '</b></td>';
		$t.= '<td align="center"><b></b></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '</tr>';


		$t.= '<tr>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td>ALL BC Total</td>';
		$t.= '<td align="right"><b>'. number_format(abs(d($ALLea)),2) . '</b></td>';
		$t.= '<td><b>'. number_format($ALLla,2) . '</b></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td align="center"><b>'. d($ALLtw) . '</b></td>';
		$t.= '<td align="center"><b>'. d($ALLpw) . '</b></td>';
		$t.= '<td align="center"></b></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '<td></td>';
		$t.= '</tr>';
			
	

	
	$t .= '</table>';

	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'over_advance_approval.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    exit;
	
	function d($s){
		return $s;
	}

?>