<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>
  
  $(function() {
    $( "#dDate" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:2050",
      onSelect: function(date){
        change_current_date(date);        
      } 

    });
  });

  $(function() {
    $( "#from_date,#to_date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:2050"
    });

  });

  function date_change_response_call(){
    $("#btnReset").click();
  }

</script>
<style type="text/css">
#ffTb thead, #ffTb tbody, .selecters * {
	font-family: "Roboto";
	font-size: 14px;
}
.bd {
	border-bottom: 1px dotted #ccc;
	line-height: 20px !important;
}
.SlalMn {
	background-color: #eee;
}
#ffTb {
	padding-bottom: 10px;
	overflow: hidden;
}
#ffTb thead {
	line-height: 20px !important;
	border: 1px solid #ccc;
	background-color: #999;
	color: #ffffff;
}
#ffTb tbody tr:hover {
	background-color: #fff;
}
#ffbtn {
	bottom: 0px;
	width: 100%;
	border-top: 1px solid #eaeaea;
	padding: 10px;
	background-color: #FCFCFC;
	position: fixed;
}
input[type='checkbox'] {
	padding: 0px;
	margin: 0px;
}
.fixed {
	position: fixed;
	top: 0;
	font-family: "Roboto";
	font-size: 14px;
	background-color: #999;
	color: #ffffff;
}
.bcred {
	background-color: red;
	color: #ffffff;
	border-radius: 5px;
	-moz-animation-name: blinker;
	-moz-animation-duration: .5s;
	-moz-animation-timing-function: linear;
	-moz-animation-iteration-count: infinite;
}
 @-moz-keyframes blinker {
 0% {
 opacity: 1.0;
}
 50% {
 opacity: 0.0;
}
}
.selecters {
	border-bottom: 1px solid #eaeaea;
	padding: 0px 10px;
	background-color: #FCFCFC;
}
.tbfxsize th {
	height: 0px !important;
	padding: 0px !important;
	margin: 0px !important;
	/*background-color: red;*/
	vertical-align: top;
}
.pAndL_hold{
  border: solid 2px;
  border-color: transparent;
  border-radius: 5px;

}
</style>
<div class="page_contain_new_pawn">
  
  <div class="discount_title_holder">
    <a href="?action=t_mark_bills" class="approval_tab app_requ_link" style="width:250px;text-align: left"><span style="font-family: bitter; font-size: 19px; ">Lost / Police Bill</span></a>
    <a href="?action=t_mark_bills2" class="approval_tab app_requ_link"      style="width:250px;text-align: left"><span style="font-family: bitter; font-size: 19px; color: #999999">Paid / postponed Bills</span></a>
  
    <div style="width:190px;border:0px solid red;float:right;text-align: right"><span style="font-family: bitter; font-size: 19px; ">Mark Bills</span></div>

  </div>

  <div class="page_main_title_new_pawn">
    <!-- <div style="width:240px;float:left"><span>Mark Bills</span></div> -->
     
     <div style="float:right; font-size:10px;">
      <span style="font-size:14px;">No</span>
      <input class="input_text_regular_new_pawn" id="no" name="no" style="width:50px; margin:0 5px; text-align:center; border:1px solid green;" value="<?=$mxno?>" type="text">
      <input class="input_text_regular_new_pawn" type="text" id="dDate" name="dDate" style="width:80px" readonly="readonly" value="<?=$current_date?>">

     </div>
  </div>
</div>

<div class="selecters">
  <table border=0 cellpassing="0" cellspacing="0" align="center">
    <tr>      
        <td>
            <div class="text_box_holder_new_pawn" style="width:345px;border-bottom:none"> 
                <span class="text_box_title_holder_new_pawn">Bill Number</span>
                <input class="input_text_regular_new_pawn" value="" type="text" id="billno" style="width:332px;border:2px solid Green;">
            </div>
        </td>
        <td class="text_box_holder_new_pawn"><br>
            <input class="btn_regular" value="Find" type="button" id="mb_find"  name="mb_find"/>
        </td>
    </tr>
  </table>
</div>

<div class="selecters">
  <table border=0 cellpassing="0" cellspacing="0" align="center" width="100%">

  <tbody>
      <tr>
          <td class="div_bn_det" style="font-size: 22px; font-family: 'bitter'"></td>
      </tr>
  </tbody>

    </table>


</div>

<div style="padding: 20px">
    <table width="100%">
        <tfoot>
            <tr><td height="60" valign="bottom">Marked Bills<br><br></td></tr>
            <tr><td><?=$marked_bills?></td></tr>
        </tfoot>
  </table>
</div>



</div>
</body></html>