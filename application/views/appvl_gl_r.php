<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<style>

.bc_cls_title{
	border-bottom:  1px solid #cccccc;
	padding: 10px 0px 10px 10px;	
	font-family: 'bitter';
    font-size: 19px;
}

.fund_transfer_title_row:hover{
	background-color: #eaeaea;
}

.fund_transfer_title_row{
	display: none;
}

.bc_cls_title:hover{	
	color: #000000;
	cursor: pointer;
}

.RR_title_content{
	
}

</style>

<div class="page_contain_new_pawn">
	<div class='app_popup_bg'></div>
	<div class='app_popup'>
		
		<div class='app_c'>
			<span style='font-size: 14px;font-weight: bold'>Branch cash request forward</span><br><br><br>
			<div class="bc_cash_balance_grid"></div><br>
			Requested amount <span class='requ_amt' style="color:red"></span><br><br>

			<input name="btnForward_bc_request" type="button" value="Forward" id="btnForward_bc_request" class="btn_regular" style="margin-right: 340px" />
			<input name="btnCancelForward_bc_request" type="button" value="Close" id="btnCancelForward_bc_request" class="btn_regular"/>
		</div>

	</div>

	<div class="discount_title_holder">		
		<a href="?action=appvl_gl_r" class="approval_tab app_requ_link" 			style="width:150px;text-align: left"><span style="font-family: bitter; font-size: 19px;">New</span></a>
		<a href="?action=appvl_gl_r_approved" class="approval_tab app_requ_link" style="width:150px;text-align: left"><span style="font-family: bitter; font-size: 19px; color: #999999 ">Approved</span></a>
	
		<div style="width:190px;border:0px solid red;float:right;text-align: right"><span style="font-family: bitter; font-size: 19px; ">Receipt Approval</span></div>

	</div><br>

	<?php if ($vou_area_stat == 1){ ?>

		<div class="discount_requests_title"> 			
			
		</div>

		<div class="discount_requests"></div>	

	<?php }else{ ?>

		<table border="0" width="100%" height="100%">
			<tr>
				<td valign="middle" align="center" style="padding-top: 150px;">
					<span class='access_fobi' style='color:red;font-size:62px'>An area not assigned for view vouchers</span><br><br>
					<span class='access_fobi' style='color:#666666;font-size:17px'>Contact system administrator or head office</span>
				</td>
			</tr>
		</table>

	<?php } ?>

</div>

	<input type="hidden" id="aa">
	<input type="hidden" id="bb">
	<input type="hidden" id="cc">
	<input type="hidden" id="dd">

</body>
</html>