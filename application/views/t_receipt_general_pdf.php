<?php	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);


/*var_dump($sum);
exit;*/

		$this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(true);
        
        //print_r($det);
        $this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->AddPage("L","SEW");   // L or P amd page type A4 or A3
		
		//$this->pdf = new TCPDF("L", PDF_UNIT, 'SEW', true, 'UTF-8', false);
		// Data Tobe retrieve
		$bank_acc = "";$chq_no = "";
		foreach ($cheque as $chq) {
			$bank_acc = $chq->bank;	
			$chq_no = $chq->cheque_no ;
		}
		
		$recipient = "";

 		foreach ($company as $r) {
		    $c_name=$r->name; 
		}
		foreach ($branch as $ress) {
		    $name=$ress->name;
		    $address=$ress->address;
		    $tp= $ress->telno;
		    $fax=$ress->faxno;
		    $email=$ress->email;    
		}
		$this->pdf->headerSet4($c_name,$address, $tp, $fax, $email);

		$cus_name=$cus_address="";

		foreach($det as $row){
		$description=$row->memo;
		
		$ref_no=$row->ref_no;
		$amount=$row->amount;
		$cus=$row->name;
		}

		foreach ($sum as $r) {
			$types=$r->type;
			$chq_amount =$r->cheque_amount;
			$cash_amount =$r->cash_amount;
			$note = $r->note;
			$recipient = $r->payee_name;
			$bank_acc_name = $r->bank_acc_name;
			$approvd_by = $r->approvd_by;
			$r_no = $r->nno;
			$from_acc = $r->paid_to_acc. " - ". $r->acc_desc ;
			$ddate=$r->date;
		}

		foreach($supplier as $sup){
			$sup_name=$sup->name;
			$sup_id=$sup->code;
		}
		foreach($items as $itm){
			$itm_bal=$itm->bal;
			$dueAmount=$itm->balance;
		}
			foreach($user as $row){
		 		$operator=$row->loginName;
		}

		foreach($session as $ses){
			$invoice_no=$session[0];
		}


			$this->pdf->setY(20);
        	$this->pdf->SetFont('helvetica', 'B', 10);
			$this->pdf->Ln();
			$orgin_print=$_POST['org_print'];
			//if($orgin_print=="1"){
		 	$this->pdf->Cell(0, 5,'Receipt ('.strtoupper($types).') ',0,false, 'C', 0, '', 0, false, 'M', 'M');
			 //}else{
			//$this->pdf->Cell(0, 5,'Receipt ('.strtoupper($types).') (DUPLICATE) ',0,false, 'C', 0, '', 0, false, 'M', 'M'); 	
			 //}
			 // New Format
			$this->pdf->Ln();
		 	$this->pdf->Ln();

		 	$this->pdf->SetFont('helvetica', '', 9);
		 	
		 	// Branch
		 	$this->pdf->MultiCell(36,  0, "Branch ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(80, 0, $name,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			
			// ###
			// Voucher No.
		 	
			$this->pdf->MultiCell(0, 0, "Receipt No : ".$r_no,$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			// Date
		 	$this->pdf->MultiCell(36,  0, "Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(0, 0, $ddate ,$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

			// Bank Acc.
		 	$this->pdf->MultiCell(36,  0, "Paid from",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(0, 0, $from_acc ,$border = '0', $align = 'L', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			
			// Cheque No.
		 	$this->pdf->MultiCell(36,  0, "Cheque No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(0, 0, $chq_no ,$border = '0', $align = 'L', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		 	
		 	// ###
		 	// Name of the Recipient:
		 	$this->pdf->MultiCell(36,  0, "Name of the Recipient ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		 	$this->pdf->MultiCell(0,  0, $recipient,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


		 	
		 

		 	
		 	$this->pdf->Ln();		 
		 	$this->pdf->Ln();
		 	// 180 

			// $this->pdf->SetX(35);
			$this->pdf->Cell(150, 1,"Account Details", 'TRL', 0, 'C', 0);
		 	$this->pdf->Cell(30, 1,"Amount", '1', 0, 'C', 0);
		 	$this->pdf->Ln();
		 	$this->pdf->Cell(150, 1,"", 'RBL', 0, 'C', 0);
		 	$this->pdf->Cell(20, 1,"Rs.", '1', 0, 'C', 0);
		 	$this->pdf->Cell(10, 1,"Ct.", '1', 0, 'C', 0);
		 	$this->pdf->Ln();

			
		 	foreach ($det as $value) {		
		 		$this->pdf->SetFont('helvetica', '', 8);
		 		$this->pdf->Cell(150, 1,$value->acc_code." | ".$value->description, 'BL', 0, 'L', 0);
		 		$str = explode(".", $value->amount);
			 	$this->pdf->Cell(20, 1,$str[0], 'B', 0, 'R', 0);
			 	$this->pdf->Cell(10, 1,$str[1], 'RB', 0, 'L', 0);
			 	
			 	$this->pdf->Ln();$this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));		    
			}

		 	/*$this->pdf->Cell(45, 1, "Towards Full Payment for In No ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $vou_des, '0', 0, 'L', 0);*/
		 	
		    /*if(!empty($cheque)){	
		    	$this->pdf->Ln();$this->pdf->Ln();
		    	$this->pdf->SetFont('helvetica', 'B', 8);
		    	$this->pdf->Cell(10, 1, 'Cheque Details   ' , '0', 0, 'L', 0);
		    	$this->pdf->Ln();
		    	$this->pdf->SetFont('helvetica', '', 8);
			 	$this->pdf->Cell(30, 1,"Cheque No", '1', 0, 'C', 0);
			 	$this->pdf->Cell(90, 1,"Bank", '1', 0, 'C', 0);
			 	$this->pdf->Cell(30, 1,"Cheque Date", '1', 0, 'C', 0);
			 	$this->pdf->Cell(30, 1,"Amount", '1', 0, 'C', 0);
			 	$this->pdf->Ln();

			 	foreach ($cheque as $chq) {			   
				    $this->pdf->SetX(15);
				    $this->pdf->SetFont('helvetica', '', 8);
				    $this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));
				    $this->pdf->Cell(30, 6, $chq->cheque_no, '1', 0, 'L', 0);
				    $this->pdf->Cell(90, 6, $chq->bank." | ".$chq->description, '1', 0, 'L', 0);
				    $this->pdf->Cell(30, 6, $chq->bank_date, '1', 0, 'L', 0);
				    $this->pdf->Cell(30, 6, $chq->amount, '1', 0, 'R', 0);
				    $this->pdf->Ln();			    
				}

		    }*/
		    $this->pdf->Ln();$this->pdf->Ln();

		 	
		 	$this->pdf->Cell(150, 1,"Description :".$note, '1', 0, 'L', 0);
		 		$str = explode(".", $value->amount);
		 	$this->pdf->Cell(20, 1,"", '1', 0, 'R', 0);
		 	$this->pdf->Cell(10, 1,"", '1', 0, 'L', 0);

		 	


		 	$this->pdf->SetFont('helvetica', 'B', 8);
		 	
		 	if($chq_amount>0){
		 	// 	$this->pdf->Cell(20, 1,"Cheque Amount", 'TBL', 0, 'L', 0);
				// $this->pdf->Cell(20, 1,number_format($chq_amount,2), 'TBR', 0, 'R', 0);
		 	// 	$this->pdf->Cell(10, 1,'', '0', 0, 'L', 0);
		 		$tot = $chq_amount;
			}
			
		 	
		 	if($cash_amount>0){
			 // 	$this->pdf->Cell(20, 1,"Cash", 'TBL', 0, 'L', 0);
				// $this->pdf->Cell(20, 1,number_format($cash_amount,2), 'TBR', 0, 'R', 0);
		 		$tot = $cash_amount;
		 	}

		 	$this->pdf->Ln();
		 	$str_tot = explode(".", $tot);
		 	$this->pdf->SetFont('helvetica', 'B', 10);
		 	$this->pdf->Cell(150, 1,"Total (".$this->utility->convert_number_to_words($tot).")", '1', 0, 'L', 0);
		 		$str = explode(".", $value->amount);
		 	$this->pdf->Cell(20, 1,$str_tot[0], '1', 0, 'R', 0);
		 	$this->pdf->Cell(10, 1,$str_tot[1], '1', 0, 'L', 0);    
		 	
		 	
		 	$this->pdf->Ln();

		 	$this->pdf->SetFont('helvetica', '', 8);
		
         
		 	$this->pdf->Ln();
		 	$this->pdf->Cell(0, 1, $approvd_by, '0', 0, 'C', 0);
		 	

		 	$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '       ..........................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	//$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
			$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '       Prepared By', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '                System Approval', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '        1. Approval', '0', 0, 'L', 0);
		 	//$this->pdf->Cell(70, 1, 'Customer Signature', '0', 0, 'L', 0);
		 
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	//$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
			$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '       2. Approval', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, ' Signature of Recipient', '0', 0, 'L', 0);


		 	$this->pdf->Ln();$this->pdf->Ln();
		 	$this->pdf->Cell(20, 1, "Operator ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $this->session->userdata('user_des'), '0', 0, 'L', 0);
		 	$this->pdf->Ln();

		 	$tt = date("H:i");

		 	
		 	$this->pdf->Cell(20, 1, "Print Time ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $tt, '0', 0, 'L', 0);
		 	$this->pdf->Ln();



	//$this->pdf->footerSet($employee,$amount,$additional,$discount,$user);

	$this->pdf->Output("Voucher".date('Y-m-d').".pdf", 'I');

?>




		exit;
	
		error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
		$this->pdf->setPrintHeader($header,$type,$duration);
        $this->pdf->setPrintFooter(true);
        //print_r($det);
        $this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->AddPage("P","A4");   // L or P amd page type A4 or A3


			$this->pdf->setY(20);			
			$this->pdf->Cell(0, 5,$company->name,0,false, 'l', 0, '', 0, false, 'M', 'M');

			foreach ($branch as $ress) {
			    $name=$ress->name;
			    $address=$ress->address;
			    $tp= $ress->telno;
			    $fax=$ress->faxno;
			    $email=$ress->email;    
			}


			$this->pdf->ln();
			$this->pdf->SetFont('helvetica', '', 10);
			
			$this->pdf->Cell(50, 1, $name . " " .$address , '0', 01, 'L', 0);
			$this->pdf->Cell(50, 1, "T.P " . $tp . " Fax : " .$fax   , '0', 0, 'L', 0);
			

			//$this->pdf->Cell(0, 5,$branch,0,false, 'C', 0, '', 0, false, 'M', 'M');

			$this->pdf->setY(35);			
        	$this->pdf->SetFont('helvetica', 'BU', 10);
			$this->pdf->Ln();
			$orgin_print=$_POST['org_print'];
			

			//if($pt == 1){
				$this->pdf->Cell(0, 5,'GENERAL RECEIPT',0,false, 'L', 0, '', 0, false, 'M', 'M');
			//}elseif ($pt == 2){
				//$this->pdf->Cell(0, 5,'GENERAL RECEIPT (Duplicate) ',0,false, 'C', 0, '', 0, false, 'M', 'M');
			//}else{
				//$this->pdf->Cell(0, 5,'GENERAL RECEIPT (Draft) ',0,false, 'C', 0, '', 0, false, 'M', 'M');
			//}


			 $this->pdf->Ln();
		 	$this->pdf->Ln();
		 	$this->pdf->SetFont('helvetica', '', 8);
		 	//$this->pdf->setY(25);

		 	$this->pdf->Cell(20, 1, "Receipt Type : ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(30, 1, ucfirst($sum->type), '0', 0, 'L', 0);
		 	$this->pdf->Cell(2, 1,'', '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, "", '0', 0, 'L', 0);

		 	$this->pdf->Cell(10, 1, "Date : ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(20, 1, $sum->date, '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, '', '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, '', '0', 0, 'L', 0);
		 

		 	$this->pdf->Ln();
		 	$this->pdf->Cell(20, 1, "Receipt No : ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $sum->nno, '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1,'', '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, "", '0', 0, 'L', 0);
		 	$this->pdf->Ln();		 
		 	$this->pdf->Ln();

		 	$this->pdf->Cell(30, 1, "_________________________________________________________________________________________________________________", '0', 0, 'L', 0);
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();

		 	$this->pdf->Cell(15, 1, 'Note  : ' , '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $sum->receipt_desc, '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, "", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, '', '0', 0, 'L', 0);
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();

		 	if ($sum->type == 'cash'){
		 		$rec = $sum->cash_amount;
		 	}else{
		 		$rec = $sum->cheque_amount;
		 	}


		 	$this->pdf->Cell(41, 1, 'Received With Thanks a Sum of ' , '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $rec, '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, "", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, '', '0', 0, 'L', 0);
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();
		 

		 	$this->pdf->Cell(10, 1, 'From : ' , '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $sum->paid_to_acc." | ".$sum->acc_desc, '0', 0, 'L', 0);

		 	$this->pdf->Ln();
			$this->pdf->Ln();
			$this->pdf->Ln();

			 $this->pdf->SetX(35);
			$this->pdf->Cell(30, 1,"Account Code", '1', 0, 'C', 0);
		 	$this->pdf->Cell(70, 1,"Name", '1', 0, 'C', 0);
		 	$this->pdf->Cell(30, 1,"Amount", '1', 0, 'C', 0);
		 	$this->pdf->Ln();


		 	foreach ($det as $value) {			   
			    $this->pdf->SetX(35);
			    $this->pdf->SetFont('helvetica', '', 8);
			    $this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));
			    $this->pdf->Cell(30, 6, $value->acc_code, '1', 0, 'L', 0);
			    $this->pdf->Cell(70, 6, $value->description, '1', 0, 'L', 0);
			    $this->pdf->Cell(30, 6, $value->amount, '1', 0, 'R', 0);
			    $this->pdf->Ln();			    
			}
		 	$this->pdf->Ln();

		 	/*$this->pdf->Cell(45, 1, "Towards Full Payment for In No ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $vou_des, '0', 0, 'L', 0);
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();*/
		 	
		 	
		 	
		 	$this->pdf->Ln();

		    if(!empty($cheque)){	
		    	$this->pdf->SetX(15);
		    	$this->pdf->SetFont('helvetica', 'B', 8);
		    	$this->pdf->Cell(10, 1, 'Cheque Details   ' , '0', 0, 'L', 0);
		    	$this->pdf->Ln();
		    	$this->pdf->Ln();
		    	$this->pdf->SetFont('helvetica', '', 8);
			 	$this->pdf->Cell(20, 1,"Cheque No", '1', 0, 'C', 0);
			 	$this->pdf->Cell(50, 1,"Bank", '1', 0, 'C', 0);
			 	$this->pdf->Cell(20, 1,"Cheque Date", '1', 0, 'C', 0);
			 	$this->pdf->Cell(20, 1,"Amount", '1', 0, 'C', 0);
			 	$this->pdf->Ln();

			 	foreach ($cheque as $chq) {			   
				    $this->pdf->SetX(15);
				    $this->pdf->SetFont('helvetica', '', 8);
				    $this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));
				    $this->pdf->Cell(20, 6, $chq->cheque_no, '1', 0, 'L', 0);
				    $this->pdf->Cell(50, 6, $chq->bank." | ".$chq->description, '1', 0, 'L', 0);
				    $this->pdf->Cell(20, 6, $chq->cheque_date, '1', 0, 'L', 0);
				    $this->pdf->Cell(20, 6, $chq->amount, '1', 0, 'R', 0);
				    $this->pdf->Ln();			    
				}

			 	$this->pdf->Ln();  
			 	$this->pdf->SetFont('helvetica', 'B', 8);
			 	$this->pdf->Cell(20, 1,"Cheque Amount", 'TBL', 0, 'L', 0);
				$this->pdf->Cell(20, 1,number_format($chq_amount,2), 'TBR', 0, 'R', 0);
			 	$this->pdf->Cell(10, 1,'', '0', 0, 'L', 0);

		    }

		 	
		 	 
		 	
		 	if($sum->cash_amount>0){
			 	$this->pdf->Cell(20, 1,"Cash", 'TBL', 0, 'L', 0);
				$this->pdf->Cell(20, 1,number_format($sum->cash_amount,2), 'TBR', 0, 'R', 0);
		 	}
		 	     
		 	$this->pdf->Ln(); 
		 	$this->pdf->Ln(); 
		 	$this->pdf->SetFont('helvetica', 'B', 20);
		 	$this->pdf->Cell(40, 1, "Rs. ".$rec, '1', 0, 'R', 0);
		 	$this->pdf->Ln();

		 	$this->pdf->SetFont('helvetica', '', 8);
		
		 	$this->pdf->Ln();
		 	$this->pdf->Cell(50, 1, '...................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(50, 1, '...................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(50, 1, '...................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(50, 1, '...................................', '0', 0, 'L', 0);
			$this->pdf->Ln();
		 	$this->pdf->Cell(50, 1, '       Prepaired By', '0', 0, 'L', 0);
		 	$this->pdf->Cell(50, 1, '         Checked By', '0', 0, 'L', 0);
		 	$this->pdf->Cell(50, 1, ' Cashier Signature', '0', 0, 'L', 0);
		 	$this->pdf->Cell(50, 1, 'Customer Signature', '0', 0, 'L', 0);
		 
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();

		 	$this->pdf->Ln();
		 	$this->pdf->Cell(20, 1, "Operator ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $sum->oc, '0', 0, 'L', 0);
		 	$this->pdf->Ln();

		 	$tt = date("H:i");

		 	
		 	$this->pdf->Cell(20, 1, "Print Time ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $tt, '0', 0, 'L', 0);
		 	$this->pdf->Ln();



	//$this->pdf->footerSet($employee,$amount,$additional,$discount,$user);

	$this->pdf->Output("Voucher".date('Y-m-d').".pdf", 'I');

?>