<?php


	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setX(0);

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	
	$this->pdf->MultiCell(0, 0, "Mark Bills", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	

	$tot_transval=$tot_bal=(float)0;

	$bdr='B'; $y=0;

	$s = '';
	
	foreach($list as $r){
		
		$this->pdf->setX(15);
		if ($s != $r->type){


			if ($r->type == 'L'){ $t = 'Lost'; }
			if ($r->type == 'PO'){ $t = 'Sent To Police'; }


			$this->pdf->ln();
			$this->pdf->SetFont('helvetica', 'B', 10);		
			$this->pdf->MultiCell(0, 10, strtoupper($t) , $border='0', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=10, $valign='M', $fitcell=false);
			$this->pdf->SetFont('helvetica', '', 8);		
			$s = $r->type;

			$this->pdf->SetFont('helvetica', '', 8);	
			$this->pdf->setX(15);
			
			$this->pdf->MultiCell(15, 1, "No", 			'1','C', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(20, 1, "Bill Type", 	'1','C', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(18, 1, "Date", 		'1','C', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(30, 1, "Bill No", 	'1','C', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(70, 1, "Customer Name", 	'1','L', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(70, 1, "Address", 		'1','L', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(40, 1, "Pawn Amount", 	'1','R', 0, 1, '', '', false, '', 0);
		}


		$h = 5 * (	max(1,$this->pdf->getNumLines($r->cusname,70),$this->pdf->getNumLines($r->address,70) ) );

		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', '', 8);
		
		$this->pdf->MultiCell(15, $h, $r->no ,			 $border='1', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->billtype ,	 $border='1', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(18, $h, $r->ddate ,		 $border='1', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(30, $h, $r->billno ,		 $border='1', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(70, $h, $r->cusname ,		 $border='1', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(70, $h, $r->address ,		 $border='1', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(40, $h, $r->requiredamount,$border='1', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);

	}
	

	$this->pdf->Output("PDF.pdf", 'I');



?>