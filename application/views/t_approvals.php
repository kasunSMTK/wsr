<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<div class="page_contain_new_pawn">
	<div class='app_popup_bg'></div>
	<div class='app_popup'>
		
		<div class='app_c'>
			<span style='font-size: 14px;font-weight: bold'>Branch cash request forward</span><br><br><br>
			<div class="bc_cash_balance_grid"></div><br>
			Requested amount <span class='requ_amt' style="color:red"></span><br><br>

			<input name="btnForward_bc_request" type="button" value="Forward" id="btnForward_bc_request" class="btn_regular" style="margin-right: 340px" />
			<input name="btnCancelForward_bc_request" type="button" value="Close" id="btnCancelForward_bc_request" class="btn_regular"/>
		</div>

	</div>

	<div class="discount_title_holder">
		<a href="#l=all_requests" class="approval_tab_selected app_requ_link"><span>All Requests</span></a>
		<a href="#l=redeem_discount" class="approval_tab app_requ_link"><span>Redeem Discount</span></a>
		<a href="#l=over_advance" class="approval_tab app_requ_link"><span>Over Advance</span></a>
		<a href="#l=extra_loan" class="approval_tab app_requ_link"><span>Extra Loan Approval</span></a>
		<a href="#l=bill_cancellation" class="approval_tab app_requ_link"><span>Bill Cancellation</span></a>
		<a href="#l=fund_transfers" class="approval_tab app_requ_link"><span>Fund Transfers</span></a>
		<a href="#l=general_voucher" class="approval_tab app_requ_link"><span>General Vocuhers</span></a>
	
		<div style="width:190px;border:0px solid red;float:right"><input type="text" class="input_text_app_search">Search<br><a>Reset</a></div>

	</div><br>

	<div class="discount_requests_title">
		
		<div class="request_row" style="display: block;">			
			<div class="RR_title_content" style="text-align:center">Number</div>
			<div class="RR_title_content" style="width:100px;text-align:right">Interest Balance</div>
			<div class="RR_title_content" style="width:100px;text-align:right">Payable Amount</div>
			<div class="RR_title_content" style="width:100px;text-align:right">Gold Value</div>
			<div class="RR_title_content" style="width:120px;padding-left:20px">Requested By</div>
			<div class="RR_title_content" style="width:120px;padding-left:20px">Branch</div>
			<div class="RR_title_content" style="width:140px">Requested Date Time</div>
			<div class="RR_title_content" style="width:120px;text-align:right;margin:-12px 5px 0px 4px ;padding:10px; border:2px solid Red; border-bottom:none">Requ. Amount</div>
			<div class="RR_title_content" style="width:120px">Approve Amount</div>
			<div class="RR_title_content" style="width:140px">Approve By</div>
			<div class="RR_title_content" style="width:140px">Action</div>
		</div>

		<div class="fund_transfer_title_row" style="display: block;border-bottom: 1px solid #eaeaea; padding: 10px; height: 30px;">
			
			<div class="RR_title_content" style="width: 50px;">NO</div>
			<div class="RR_title_content" style="width:120px;">Branch</div>
			<div class="RR_title_content" style="width:150px;">Date</div>
			<div class="RR_title_content" style="width:120px;">Request From</div>
			<div class="RR_title_content" style="width:120px;">Branch From</div>
			<div class="RR_title_content" style="width:120px;">Receiving Method</div>
			<div class="RR_title_content" style="width:110px;text-align: right;margin-right:20px">Amount</div>
			<div class="RR_title_content" style="width:120px;">Request by</div>
			<div class="RR_title_content" style="width:150px;">Request Datetime</div>
			<div class="RR_title_content" style="width:200px;">Action</div>

		</div>

	</div>

	<div class="discount_requests"></div>	

</div>

	<input type="hidden" id="aa">
	<input type="hidden" id="bb">
	<input type="hidden" id="cc">
	<input type="hidden" id="dd">

</body>
</html>