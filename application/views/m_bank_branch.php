<?php if($this->user_permissions->is_view('m_bank_branch')){ ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<div class="page_main_title"><span>Bank Branch</span></div>

<form method="post" action="<?=base_url()?>index.php/main/save/m_bank_branch" id="form_">
    <table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">
 	
 		<tr>
            <td>
                <b>Add New Bank Branch</b>
            </td>
            <td>
                <b>Exist Bank Branch</b>
            </td>
        </tr>

        <tr>
                <td height="10">
                    <div class="text_box_holder">
                        <span class="text_box_title_holder">Bank (F1)</span> 
                       
                    	<input type="text" class="input_text_regular" id="sbank" name="sbank" value="" maxlength="0" style="border:1px solid #ccc;width: 90px;" />
				        <input type="hidden" name="bank" id="bank" value="0" />
				        <input type="text" class=" input_text_regular" value='' readonly="readonly" id="bank_des"  style="width: 250px;"/>
                    </div>
                </td>                           
                <td rowspan="8">

                    <div class="exits_text_box_holder">
                        <div class="text_box_title_holder" style="width:40px;border:0px solid red">Name</div> 
                        <input class="nic_feild input_text_regular" style="width:194px" type="text" id="Search_bankname">
                        <input type="button" value="Search" name="btnSearch" id="btnSearch" class="btn_regular">                    
                    </div>
                    
                    <div class="list_div">
            
                        
            
                    </div>

                </td>
            </tr>
            <tr>
				 
				<td height="10">
				  <div class="text_box_holder">
				  	<span class="text_box_title_holder">Branch Code </span> 
					<input type="text" class="input_text_regular" value="" id="code" name="code" class="input_txt" maxlength="4" style="border:1px solid #ccc;width:150px; text-transform: uppercase;" />
					<input type="text" class="input_text_regular" value='' readonly="readonly" id="branch_code"  name="branch_code" style="width: 147px; text-transform: uppercase;"/>
					<input type="hidden" name="b_branch_code" id="b_branch_code" value="" />
				  </div>
				</td>
			</tr>

			 <tr>
                <td height="10">
                    <div class="text_box_holder">
                        <span class="text_box_title_holder">Description</span> 
                        <input class="input_text_regular" type="text" name="description" id="description" maxlength="100">
                    </div>
                </td>                           
            </tr>
             <tr>
                <td>
                   <input type="button" class="btn_regular" id="btnExit" value='Exit' />
				   <?php if($this->user_permissions->is_add('m_bank_branch')){ ?><input type="button" class="btn_regular" id="btnSave" value='Save <F8>' /><?php } ?>
				   <input type="button" class="btn_regular" id="btnReset" value='Reset'>    
                    <input type="hidden" value="0" name="code_" id="code_" >
                </td>                           
            </tr>
 	</table>
 </form>


<?php } ?>
