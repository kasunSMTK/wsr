<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $( "#date,#fd,#td" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2050"
        });
    });

</script>
    

    <div class="page_contain_new_pawn">
        <div class="page_main_title_new_pawn">
            <div style="width:240px;float:left"><span>Cheque Issue</span></div>                 
        </div>
    </div>


    <form id="print_bc_issued_cheques" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">

        <table class="tbl_chq_issue" align="center" border="0" width="800" cellpadding="0" cellspacing="0">
        
            <tr>
                <td><br>View Issued Cheques<br><br></td>
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:100%;float: left;">
                        Branch<br>
                        <input type="text"      name="r_i_bc_name"    id="r_i_bc_name" style="width:280px">
                        <input type="hidden"    name="r_issue_bc"     id="r_issue_bc">

                        From    <input type="text" name="fd" id="fd" value="<?=date('Y-m-d')?>"> &nbsp;&nbsp;&nbsp;&nbsp;
                        To      <input type="text" name="td" id="td" value="<?=date('Y-m-d')?>">&nbsp;&nbsp;&nbsp;&nbsp;

                        <input type="button" value="View" id="btnViewIssued">
                        <input type='hidden' name="by"  id="by" value="print_bc_issued_cheques">            
                    </div>
                </td>            
            </tr>

        </table>
    
    </form>

    <form action="<?=base_url()?>index.php/main/save/t_cheque_issue" method="post" id="form_">
    
        <table class="tbl_chq_issue" align="center" border="0" width="800" cellpadding="0" cellspacing="0">

            <tr>
                <td><br><br>New Cheque Issue<br><br></td>
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:77%;float: left;">
                        Bank<br>
                        <input type="text" id="bank_name" style="width:600px">
                        <input type="hidden"  name="bank_acc" id="bank_acc">
                    </div>                
                    <div class="text_box_holder_commen" style="width:23%;float: right;">
                        No<br><input type="text" id="no" value="<?=$max_no?>">
                    </div>
                </td>            
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:77%;float: left;border-top: none;">
                        Issuing Branch<br>
                        <input type="text"      name="i_bc_name"    id="i_bc_name" style="width:600px">
                        <input type="hidden"    name="issue_bc"         id="issue_bc">
                    </div>                
                    <div class="text_box_holder_commen" style="width:23%;float: right;border-top: none;border-left: none;">
                        Date<br><input type="text" name="date" id="date" value="<?=date('Y-m-d')?>">
                    </div>
                </td>            
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:77%;float: left;border-top: none;">
                        Memo<br><input type="text"  name="memo" id="memo" style="width:600px"><input type="hidden"  name="" id="">
                    </div>                
                    <div class="text_box_holder_commen" style="width:23%;float: right;border-top: none;border-left: none;">
                        Reference No<br><input type="text" name="ref_no" id="ref_no">
                    </div>
                </td>            
            </tr>

            <tr>
                <td><br><div class="title_div_pp">Cheque Details</div><br></td>
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:40%;float: left">
                        Cheque Number<br><input type="text"  id="cheque_no"  style="width:305px">
                    </div>
                    <div class="text_box_holder_commen" style="width:40%;float: left;border-left: none;">
                        Amount<br><input type="text" id="c_amount" class="amount" style="width:305px">
                    </div>
                    <div class="text_box_holder_commen" style="width:20%;float: left;border-left: none;height: 48px">                    
                            No
                            <input type="text" id="no_of_cheques" class="amount" style="width:40px">
                            <input name="btnAddtoGrid" type="button" value="Add" id="btnAddtoGrid" class="btn_regular"/>                    
                    </div>                
                </td>            
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:100%;float: left;border:none;padding: 0px;">
                        <div class="cheque_grid"></div>
                    </div>                
                </td>            
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:100%;float: left;border-top: none;">
                        Received Employee<br><input type="text"  name="" id=""><input type="hidden"  name="" id="">
                    </div>                
                </td>            
            </tr>

            <tr>
                <td>
                    <div class="text_box_holder_commen" style="width:100%;float: left;border-top: none;">
                        <input name="btnSave" type="button" value="Save" id="btnSave" class="btn_regular"/>
                        <input type="hidden" id="hid" name="hid" value="0">
                    </div>                
                </td>            
            </tr>

        </table>
        
    </form>


    



</body>
</html>