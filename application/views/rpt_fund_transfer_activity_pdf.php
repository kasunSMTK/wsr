<?php


	$this->pdf = new TCPDF("L", PDF_UNIT, 'A3', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 23);	
	$this->pdf->setX(0);

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	
	$this->pdf->MultiCell(0, 0, "Fund Transfer Activity", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 10);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->ln(5);
	
	$this->pdf->MultiCell(15, $h, "No", 				'1','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, $h, "Transfer Type", 		'1','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(22, $h, "Date Time", 			'1','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(25, $h, "Amount", 			'1','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, $h, "Initiated Branch", 	'1','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(30, $h, "Initiated By", 		'1','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(30, $h, "From", 				'1','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(30, $h, "To", 				'1','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(90, $h, "Ref", 				'1','L', 0, 0, '', '', false, '', 0);		
	$this->pdf->MultiCell(90, $h, "Comment", 			'1','L', 0, 1, '', '', false, '', 0);

	foreach($list as $r){	
		
		$h = 5* (	max(1,

			$this->pdf->getNumLines($r->date_time,22),
			$this->pdf->getNumLines($r->requ_initi_bc,30),
			$this->pdf->getNumLines($r->requ_initi_by,30),
			$this->pdf->getNumLines($r->from_bc,30),
			$this->pdf->getNumLines($r->to_bc,30),
			$this->pdf->getNumLines($r->ref_no,79), 
			$this->pdf->getNumLines($r->comment,90) 

			) 

		);

		
		$this->pdf->MultiCell(15,$h, $r->no,				$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,$h, $r->transfer_type,		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(22,$h, $r->date_time,			$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25,$h, $r->amount,			$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,$h, $r->requ_initi_bc,		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,$h, $r->requ_initi_by,		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,$h, $r->from_bc,			$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,$h, $r->to_bc,				$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(90,$h, $r->ref_no,			$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(90,$h, $r->comment,			$border = '01', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);


	}
	

	$this->pdf->Output("PDF.pdf", 'I');



?>