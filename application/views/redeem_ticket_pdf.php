<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'SEW', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetAutoPageBreak(TRUE, 0);
	$this->pdf->AddPage();

	// ----------- header --------------------------				
		$Q = $this->db->query("SELECT IF (B.other_bc_name_allow = 0,C.`name`,'New Company Name') 	AS `company_name`, B.name 		AS `bc_name`,B.bc_no AS `bc_code`, B.address 	AS `bc_address`, B.telno 	AS `bc_tel`, B.faxno 	AS `bc_fax`, B.email 	AS `bc_email` FROM `m_company` C JOIN (SELECT * FROM `m_branches`) B ON B.bc = '".$R->bc."'LIMIT 1 ")->row();
		if ($is_reprint == 0){$header_custom_data['title'] = "Redeem Receipt"; }else{$header_custom_data['title'] = "Redeem Receipt (Re-Print)"; }
		//$this->pdf->sew_header($Q,$header_custom_data);
		$this->pdf->sew_header_no_com_name($Q,$header_custom_data);
	// ----------- end header ----------------------

	$this->pdf->SetFont('helvetica', '', 9);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

	$title_w = 20;
	$title_wL= 120;
	$title_wR= 40;

	
	$this->pdf->MultiCell($title_w,  0, "Pawn Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->ddate,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "Bill No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', 'B', 11);
	$this->pdf->MultiCell($title_wR, 0, substr($R->billno,0,3). " ".substr($R->billno,3,50), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	$this->pdf->SetFont('helvetica', '', 9);

	$this->pdf->MultiCell($title_w,  0, "Name",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->cusname,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+5,  0, "Redeem Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR-5, 0, $RRR->redeem_date. " " . $RRR->time, $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell($title_w,  0, "Address",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->address,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, "", $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell($title_w,  0, "NIC No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->nicno,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, "", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell($title_w,  0, "Contact No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->mobile,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, "", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	
	$this->pdf->ln();
	
	
	$this->pdf->MultiCell(30, 0, "Total Gold Weight", $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(20, 0, $R->totalweight, $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	
	$this->pdf->MultiCell(120, 0, "Loan Amount", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(30, 0, d($R->requiredamount), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);

	if ($paid_interest < 0){
		$this->pdf->MultiCell(170, 0, "Paid Interest Total", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0,d($paid_interest*-1), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}

	if ($paid_capital < 0){
		$this->pdf->MultiCell(170, 0, "Paid Capital Total", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0,d($paid_capital*-1), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}

	if ($RRR->advance_amount > 0){
		$this->pdf->MultiCell(170, 0, "Customer Advanced", $border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0, "-".d($RRR->advance_amount), $border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}

	$red_int = $stamp_duty = $doc_fee = $RD_discount = 0;

	foreach ($RR as $r) {
		if ( $r->entry_code == 'RI' ){ $red_int = $r->cr_amount; }
		if ( $r->entry_code == 'SF' ){ $stamp_duty = $r->cr_amount; }
		if ( $r->entry_code == 'DF' ){ $doc_fee = $r->cr_amount; }
		if ( $r->entry_code == 'RD' ){ $RD_discount = $r->dr_amount; }
	}

	if ($red_int > 0){
		$this->pdf->MultiCell(170, 0, "Redeem Interest", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0, d($red_int), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}

	if ($stamp_duty > 0){
		$this->pdf->MultiCell(170, 0, "Stamp Duty", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0, d($stamp_duty), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}

	if ($doc_fee > 0){
		$this->pdf->MultiCell(170, 0, "Postage", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0, d($doc_fee), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}

	if ($RD_discount > 0){
		$this->pdf->MultiCell(170, 0, "Discount", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0, "-".d($RD_discount), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}


	if ($RRR->refundable_interest > 0){
		$this->pdf->MultiCell(170, 0, "Pawning Interest", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(30, 0, "-".d($RRR->refundable_interest), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	}


	$this->pdf->SetFont('helvetica', 'B', 9);	

	$this->pdf->MultiCell(170, 0, "Payable Amount", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(30, 0, d(($RRR->redeem_amount)-$RD_discount), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);

	$amount_in_word = $this->utility->convert_number_to_words(($RRR->redeem_amount)-$RD_discount);

	$this->pdf->SetFont('helvetica', '', 8);
	$this->pdf->ln(8);
	$this->pdf->MultiCell(0,0, substr($amount_in_word, 0,1) . strtolower(substr($amount_in_word, 1,10000)) ,	$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 9);
	$this->pdf->ln(10);	

	$this->pdf->MultiCell(150,0,"I hereby relieve ".$Q->company_name." form all claims and liabilities with regard to this mortgage, since all the articles which have been mortgaged under this receipt, have all been received.",	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->ln(10);	

	$this->pdf->MultiCell(35, 0, ".......................................", $border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(63, 0, ".......................................", $border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(63, 0, ".......................................", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);

	$this->pdf->MultiCell(35, 0, "Cashier", $border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(63, 0, "Authorized Officer", $border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(63, 0, "Customer Signature", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);

	$this->pdf->SetFont('helvetica', '', 7);
	$this->pdf->ln();	
	$this->pdf->MultiCell(98, 0, "System User ".$occ, $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(63, 0, "", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	

	//$this->pdf->IncludeJS("javascript:print();");
	$this->pdf->Output("redeem_ticket.pdf", 'I');

	function d($n,$d=2){
		return 	number_format($n,$d);				
	}

?>