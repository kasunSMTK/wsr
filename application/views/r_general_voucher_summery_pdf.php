<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
$this->pdf->setPrintHeader($header,$type,$duration);
$this->pdf->setPrintHeader(true,$type);
$this->pdf->setPrintFooter(true);

$this->pdf->SetFont('helvetica', 'B', 16);
$this->pdf->AddPage($orientation,$page); 

$branch_name="";

    //set header -----------------------------------------------------------------------------------------
foreach($branch as $ress)
{
  $branch_name=$ress->name;
  $this->pdf->headerSet3($ress->name,$ress->address,$ress->tp,$ress->fax,$ress->email);
}

$this->pdf->setY(25);
$this->pdf->SetFont('helvetica', 'BUI',12);
$this->pdf->Cell(300, 1,"GENERAL VOUCHER LIST - SUMMERY",0,false, 'L', 0, '', 0, false, 'M', 'M');
$this->pdf->Ln();

$this->pdf->SetX(15);
$this->pdf->SetFont('helvetica', '',10);
$this->pdf->Cell(0, 5, 'Date   From '. $dfrom.' To '.$dto ,0,false, 'L', 0, '', 0, false, 'M', 'M');
$this->pdf->Ln();
if($branch1 !=""){
  $this->pdf->Cell(30, 6,'Branch', '0', 0, 'L', 0);
  $this->pdf->Cell(5, 6,':', '0', 0, 'L', 0);
  $this->pdf->Cell(20, 6,$branch1." - ".$branch_name, '0', 0, 'L', 0);
  $this->pdf->Ln(); 
}

$this->pdf->SetFont('helvetica', 'B',9);
$this->pdf->Cell(20, 6,"Date", '1', 0, 'C', 0);
$this->pdf->Cell(25, 6,"Voucher No", '1', 0, 'C', 0);
$this->pdf->Cell(110, 6,"Description", '1', 0, 'C', 0);
$this->pdf->Cell(30, 6,"Cash", '1', 0, 'C', 0);
$this->pdf->Cell(30, 6,"Cheque", '1', 0, 'C', 0);
$this->pdf->Cell(30, 6,"Total Paid", '1', 0, 'C', 0);
$this->pdf->Ln();

foreach($r_branch_name as $row)
{   


 $this->pdf->SetX(15);
 $this->pdf->SetFont('helvetica','',9);

 $aa = $this->pdf->getNumLines($row->note, 110);
 $heigh=6*$aa;

 $cash=$row->cash_amount;
 $cheque=$row->cheque_amount;
 $totalamt=$cash+$cheque;

 $this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));

 $this->pdf->SetFont('helvetica', '', 9);
 $this->pdf->SetX(15);

 $this->pdf->MultiCell(20, $heigh, $row->ddate,1, 'L', 0, 0, '', '', true, 0, false, true, 0);
 $this->pdf->MultiCell(25, $heigh, $row->nno, 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
 $this->pdf->MultiCell(110, $heigh, $row->note, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
 $this->pdf->MultiCell(30, $heigh, number_format($row->cash_amount,2), 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
 $this->pdf->MultiCell(30, $heigh, number_format($row->cheque_amount,2), 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
 $this->pdf->MultiCell(30, $heigh, number_format($totalamt,2), 1, 'R', 0, 1, '', '', true, 0, false, true, 0);

 $cash_tot+=$row->cash_amount;
 $cheque_tot+=$row->cheque_amount;
 $Tot_paid+=$totalamt;

 $xx++;        
}


            // total
$this->pdf->SetFont('helvetica', 'B', 9);
$this->pdf->SetX(15);

$this->pdf->MultiCell(20, $heigh, "",0, 'L', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(25, $heigh, "", 0, 'L', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(110, $heigh, "Total", 0, 'C', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(30, $heigh, number_format($cash_tot,2), 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(30, $heigh, number_format($cheque_tot,2), 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(30, $heigh, number_format($Tot_paid,2), 1, 'R', 0, 1, '', '', true, 0, false, true, 0);


$this->pdf->Ln();

$xx=0;

$cash_tot=$cheque_tot=$Tot_paid=0;

$this->pdf->SetX(15);

$this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));
$this->pdf->SetFont('helvetica','B',11);




  //============================================================================================  


$this->pdf->Output("general_reciept_summery".date('Y-m-d').".pdf", 'I');

?>



