<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<div class="page_contain">
	<div class="page_main_title"><span>Branch Cash Book Account(s) Setup</span></div><br>

	
		
		<table cellpadding="0" cellspacing="0" border="0"  align="center" style="margin:auto" width="700">

			<tr>
				<td style="text-align: right;padding-right: 20px">Branch</td>
				<td style="text-align: left;">Cash Book Account</td>
			</tr>

			<tr>
				<td style="text-align: right;padding-right: 20px">
					<?=$bc_list_dropdown?>
				</td>
				<td style="text-align: left">
					<input type="text" id="txt_bc_cash_book_acc" class="bc_acc_txt" autocomplete="off">
					<input type="hidden" id="hid_acc_code">
				</td>
				<td align="center" style="padding-left: 20px">
					<input type="button" value="Add" class="btn_regular" id="btnSave">
				</td>
			</tr>	

			<tbody class="bc_cash_book_acc_show"></tbody>
			
		</table>

	

</div>
</body>
</html>