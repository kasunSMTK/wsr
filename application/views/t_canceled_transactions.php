<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $("#bank_date,#fd,#td").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });

</script>

<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:180px;float:left"><span>Canceled Transactions</span></div>     
    </div><br>

    
    <form id="form_" action="<?=base_url()?>index.php/main/load_data/t_canceled_transactions/dta" method="post">
       
        <table border="0" align="center" cellpadding="0" cellspacing="0" width="90%" class="tbl_voucher">
            <tr>
                <td style="padding: 10px;width:20px">Module</td>
                <td style="padding: 10px;width:20px">
                    <select name="transaction" class="drop_voucher_option" style="width: 100px;">
                        
                        
                        <option value="All">All</option>
                        <option value="P">Pawning</option>
                        <option value="R">Redeem</option>                        
                        <option value="AM">TFR</option>
                        <option value="RN">Renewed</option>
                        <option value="PO">Police Bills</option>
                        <option value="L">Lost Bills</option>
                        <option value="FUND_TRANSFER">Fund Transfer</option>
                        
                        

                    </select>
                </td>
                <td align="center" width="200"><?=$bc_drop?></td>
                <td align="right" width="50">User</td>                
                <td style="padding: 10px;width:110px">
                    <input type="text" id="fd" name="fd" value="<?=date('Y-m-d')?>" readonly="readonly">
                </td>
                <td style="padding: 10px;width:10px">to </td>
                <td style="padding: 10px;width:110px">
                    <input type="text" id="td" name="td" value="<?=date('Y-m-d')?>" readonly="readonly">
                </td>
                <td style="padding: 10px" width="200">
                <input type="button" class="btn_regular"  id="btnShow" value='Show' />
                </td>
                <td></td>
            </tr>

        </table>
        
        <table width="100%">
            <tr>
                <td colspan="9" style="border:none" class="vi"></td>            
            </tr>
        </table>

    </form>



</div>


</html>