<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

	$(function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2050"
		});
	});	


	$(function() {
		$( "#valid_upto,#backdate_upto" ).datepicker({
			timeOnly: false,                                   
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm tt',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2050"
		});
	});	

</script>

<style type="text/css">
	

.dropdown {
  
}

a {
  color: #000;
}

.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: 0 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #000;
  text-decoration: none;
  outline: none;
  font-size: 12px;
}

.dropdown dt a {
  background-color: #ffffff;
  display: block;
  padding: 0px 10px 0px 10px ;
  min-height: 25px;
  line-height: 24px;
  overflow: hidden;
  border: 0;
  width: 272px;

  border: 2px solid Green;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
  display: inline-block;
  padding: 0 3px 2px 0;
}

.dropdown dd ul {
  background-color: #ffffff;
  border: 1px solid #cccccc;
  color: #000;
  display: none;
  left: 0px;
  padding: 10px;

  position: absolute;
  top: 2px;
  width: 272px;
  list-style: none;
  height: 300px;
  overflow: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #000;
}

button {
  background-color: #6BBE92;
  width: 302px;
  border: 0;
  padding: 10px 0;
  margin: 5px 0;
  text-align: center;
  color: #000;
  font-weight: bold;
}

</style>

<div class="page_contain">
	<div class="page_main_title"><span>Customer Control</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_branch_receipt_update_control" id="form_">
		<table cellpadding="0" cellspacing="0" border="01" class="tbl_master" align="center" width="100%">
			<tr>
				<td valign="top"><br><br><div class="page_main_title"><span>Top Customers</span></div></b><br></td>							
			</tr>		
		</table>

		<div class="backdate_list"></div>

	</form>

</div>


</body>
</html>