<?php

	

	$this->pdf->setPrintHeader(false);
	$this->pdf->setPrintFooter(true);
	$this->pdf->AddPage($orientation,$page);
	$this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));
	$this->pdf->SetFont('helvetica', '', 8);
	$this->pdf->setY(10);

	$total=(float)$op;
	$cr_toal=$cr;
	$dr_toal=$dr;
	$period_cr=(float)0;
	$period_dr=(float)0;
	$bc_code="";
	$sub_cr=$sub_dr=$x=0;


	$acc_no = intval(substr($_POST['acc_code'], 0,1));
	$first_round_passed = false;
	$ss = '';

	foreach($all_acc_det as $row){

		
		$heigh = 4 * (max(1,$this->pdf->getNumLines(ucfirst(strtolower($row->det)),25),$this->pdf->getNumLines($row->description,55)));
		
		if ( $ss != trim($row->bc) ){
			
			$bc_name = $row->branch_name;		

			$ss = $row->bc;

			if ($first_round_passed){

				if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){

					$this->pdf->SetFont('helvetica', 'B', 8);
					$this->pdf->Cell(17, 4, '', '1', 0, 'L', 0);
					$this->pdf->Cell(11, 4, "", '1', 0, 'L', 0);
					$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
					$this->pdf->Cell(55, 4, "Period Balance", '1', 0, 'L', 0);
					$this->pdf->Cell(25, 4, number_format($period_dr,2), '1', 0, 'R', 0);
					$this->pdf->Cell(25, 4, number_format($period_cr,2), '1', 0, 'R', 0);
					$this->pdf->Cell(24, 4, "", '1', 0, 'R', 0);
					$this->pdf->Ln();

					//$period_dr = $period_cr = 0;

					$this->pdf->SetFont('helvetica', 'B', 8);
					$this->pdf->Cell(17, 4, '', '1', 0, 'L', 0);
					$this->pdf->Cell(11, 4, "", '1', 0, 'L', 0);
					$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
					$this->pdf->Cell(55, 4, "Ending Balance", '1', 0, 'L', 0);
					$this->pdf->Cell(25, 4, number_format($dr_toal,2), '1', 0, 'R', 0);
					$this->pdf->Cell(25, 4, number_format($cr_toal,2), '1', 0, 'R', 0);
					$this->pdf->Cell(24, 4, number_format($total,2), '1', 0, 'R', 0);
					$this->pdf->ln();

					$total = 0;

				}

			}

			$first_round_passed = true;

			
			
			$op = 0;
			$period_cr = $period_dr = 0;

			foreach ($op_bal as $rrr) {
				if ( $row->bc == $rrr->bc ){
					
					if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){
						$op = $total = $rrr->op;
					}else{
						$op = $total = 0;
					}


					$dr = $rrr->dr;
					$cr = $rrr->cr;
					
				}
			}

			
			$this->pdf->Ln();
			$this->pdf->SetFont('helvetica', 'B', 14);
			$this->pdf->Cell(0, 4, $bc_name, '0', 0, 'L', 0);
			$this->pdf->SetFont('helvetica', '', 8);
			$this->pdf->Ln();
			$this->pdf->Ln();


			$this->pdf->SetFont('helvetica', 'BU',12);

			$acc_arr['303003'] = "Daily Cash Account Report";
		    $acc_arr['10102'] = "Redeem Interest Account Report";
		    $acc_arr['30201'] = "Unredeem Articles Account Report";
		    $acc_arr['10101'] = "Pawning Interest Account Report";
		    $acc_arr['40215'] = "Customer Advance Account Report";




			if ( isset($_POST['acc_code']) ){
				$acc_desc = $_POST['acc_code']. " - ".$_POST['acc_code_des'];				
			}else{

				if (empty($acc_arr[$acc_code])){				
					$d = $account_det;
				}else{
					$d = $acc_arr[$acc_code];
				}				

				
				$acc_desc = $acc_code. " - ".$d;				
			}
			
			$this->pdf->Cell(180, 1, $acc_desc ,0,false, 'L', 0, '', 0, false, 'M', 'M');
			$this->pdf->Ln();

			$this->pdf->SetFont('helvetica', 'B', 8);
			$this->pdf->Cell(162, 1,"Date From - ".$dfrom."     Date To - ".$dto,0,false, 'L', 0, '', 0, false, 'M', 'M');
			$this->pdf->Ln();
			$this->pdf->Cell(162, 1,"Time From - ".$tf."     Time To - ".$tt,0,false, 'L', 0, '', 0, false, 'M', 'M');
			$this->pdf->Ln();

			if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){

				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->Cell(17, 4, '', '1', 0, 'L', 0);
				$this->pdf->Cell(11, 4, "", '1', 0, 'L', 0);
				$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
				$this->pdf->Cell(55, 4, "Begining Balance", '1', 0, 'L', 0);
				$this->pdf->Cell(25, 4, '', '1', 0, 'R', 0);
				$this->pdf->Cell(25, 4, '', '1', 0, 'R', 0);
				$this->pdf->Cell(24, 4, number_format($op,2), '1', 0, 'R', 0);			
				$this->pdf->Ln();

			}

			$this->pdf->SetFont('helvetica', 'B', 8);
			$this->pdf->Cell(17, 4, 'Date', '1', 0, 'C', 0);
			$this->pdf->Cell(11, 4, "No", '1', 0, 'C', 0);
			$this->pdf->Cell(25, 4, "Transaction", '1', 0, 'C', 0);
			$this->pdf->Cell(55, 4, "Description", '1', 0, 'L', 0);
			$this->pdf->Cell(25, 4, "Dr Amount", '1', 0, 'C', 0);
			$this->pdf->Cell(25, 4, "Cr Amount", '1', 0, 'C', 0);
			$this->pdf->Cell(24, 4, "Balance", '1', 0, 'C', 0);
			$this->pdf->ln();			

		}


		if($row->dr_amount==0){
			$total = $total-(float)$row->cr_amount;
		}else if($row->cr_amount==0){
			$total = $total+(float)$row->dr_amount;
		}


		$period_dr +=$row->dr_amount;
		$period_cr+=$row->cr_amount;		


		$this->pdf->SetFont('helvetica', '', 8);

		$this->pdf->MultiCell(17, $heigh, $row->ddate, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
		$this->pdf->SetFont('helvetica', '', 7);
		$this->pdf->MultiCell(11, $heigh, $row->trans_no, 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->MultiCell(25, $heigh, ucfirst(strtolower($row->det)), 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
		$this->pdf->MultiCell(55, $heigh, $row->description, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
		$this->pdf->MultiCell(25, $heigh,number_format($row->dr_amount,2) , 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
		$this->pdf->MultiCell(25, $heigh,number_format($row->cr_amount,2) , 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
		$this->pdf->MultiCell(24, $heigh,number_format($total,2), 1, 'R', 0, 1, '', '', true, 0, false, true, 0);
		

	}


	if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->Cell(17, 4, '', '1', 0, 'L', 0);
		$this->pdf->Cell(11, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(55, 4, "Period Balance", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, number_format($period_dr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(25, 4, number_format($period_cr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(24, 4, "", '1', 0, 'R', 0);
		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->Cell(17, 4, '', '1', 0, 'L', 0);
		$this->pdf->Cell(11, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(55, 4, "Ending Balance", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, number_format($dr_toal,2), '1', 0, 'R', 0);
		$this->pdf->Cell(25, 4, number_format($cr_toal,2), '1', 0, 'R', 0);
		$this->pdf->Cell(24, 4, number_format($total,2), '1', 0, 'R', 0);

	}else{

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->Cell(17, 4, '', '1', 0, 'L', 0);
		$this->pdf->Cell(11, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(55, 4, "Period Total", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, number_format($period_dr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(25, 4, number_format($period_cr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(24, 4, "", '1', 0, 'R', 0);
		$this->pdf->Ln();

	}



	$this->pdf->Output("Account Details Report".date('Y-m-d').".pdf", 'I');

	
	
		


?>