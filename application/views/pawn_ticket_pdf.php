<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'SEW', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetAutoPageBreak(TRUE, 0);
$this->pdf->AddPage();

//var_dump($RR);exit;

	// ----------- header --------------------------				
$Q = $this->db->query("SELECT IF (B.other_bc_name_allow = 0,C.`name`,'Shekaran Pawn Brokers.') AS `company_name`, B.name 		AS `bc_name`,B.bc_no AS `bc_code`, B.address 	AS `bc_address`, B.telno 	AS `bc_tel`, B.faxno 	AS `bc_fax`, B.email 	AS `bc_email` FROM `m_company` C JOIN (SELECT * FROM `m_branches`) B ON B.bc = '$bc'LIMIT 1 ")->row();
// Remove != 1 and replace ==1
$str_tfr = "";
$amount_in_word = "";

$is_non_gold = $R->is_non_gold;
if ($R->is_renew == 0){
	$is_tfr = 3;	
}else{
	if ($R->increased_amount > 0){
		$is_tfr = 1;	
	}else{
		$is_tfr = 2;
	}	
}

if ($is_tfr == 1){
	$str_tfr = "TFR ";
}

if ($is_reprint == 0){
	$header_custom_data['title'] = $str_tfr."Pawn Ticket"; 
}else{
	$header_custom_data['title'] = $str_tfr."Pawn Ticket (Re-Print)"; 
}
$this->pdf->sew_header_no_com_name($Q,$header_custom_data);
	// ----------- end header ----------------------

$this->pdf->SetFont('helvetica', '', 9);	
$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

$title_w = 20;
$title_wL= 120;
$title_wR= 40;

// New Format
// Customer Name
$this->pdf->MultiCell($title_w,  0, "Name",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wL, 0, $R->cusname,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
// Pawn Date
$this->pdf->MultiCell($title_w,  0, "Date Time",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wR, 0, $R->ddate . " ".$R->time,$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

// Address
$this->pdf->MultiCell($title_w,  0, "Address",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wL, 0, $R->address,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
// TFR No
$this->pdf->SetFont('helvetica', 'B', 11);	
$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
$this->pdf->MultiCell($title_w,  0, $str_tfr."No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);



$this->pdf->MultiCell($title_wR, 0, substr($R->billno,0,3). " ".substr($R->billno,3,4). " ".substr($R->billno,7,50)   , $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);



$this->pdf->SetFont('helvetica', '', 9);	
$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

// NIC
$this->pdf->MultiCell($title_w,  0, "NIC No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wL-80, 0, $R->nicno,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

if($is_non_gold==1){
	$this->pdf->MultiCell($title_w+20,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-80, 0, "" ,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
}else{
	//Assessed Value :
	/*$this->pdf->MultiCell($title_w+20,  0, "Assessed Value :",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-80, 0, d($R->goldvalue) ,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);*/

	$this->pdf->MultiCell($title_w+20,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-80, 0, "" , $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
}

$this->pdf->MultiCell($title_w+10,  0, "Loan Amount :",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->SetFont('helvetica', 'B', 10);	
$this->pdf->MultiCell($title_wR-10, 0, d($R->requiredamount), $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
$this->pdf->SetFont('helvetica', '', 9);	

// Contact
$this->pdf->MultiCell($title_w,  0, "Contact No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wL-80, 0, $R->mobile,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
//Loan Amount:
$this->pdf->MultiCell($title_w+20,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wL-80, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
//Maturity Date
$this->pdf->MultiCell($title_w,  0, "Final Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wR, 0, $R->finaldate, $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


// Operator

$this->pdf->MultiCell($title_w,  0, "Operator: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wL, 0, $user_des, $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

$manual_billno_title = "";
$manual_billno 		 = "";

if ($R->manual_billno != ""){
	$manual_billno_title = "Manual Bill No";
	$manual_billno 		 = $R->manual_billno;
}


$this->pdf->MultiCell($title_w+5,  0, $manual_billno_title,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_wR-5, 0, $manual_billno, $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


// End New Format

$this->pdf->ln();	
$this->pdf->ln();

if($is_non_gold==1){
		/*foreach ($RR3 as $rep_data) {
			$des = $rep_data->cat_code." ".$rep_data->itemcode." ".$rep_data->con." Qty ".$rep_data->qty;			
		}	

		$this->pdf->MultiCell(115, 5, "Description of Articles Pawned           : ".$des , 	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->ln();*/

		foreach ($RR3 as $rep_data) {
			$cat = $rep_data->cat_code;
		}
		//var_dump($cat );exit;

		if($cat=='Electronics'){
			$this->pdf->MultiCell(70, 5, "Description" , 	$border = 'TB', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(33, 5, "Model" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(33, 5, "IMEI" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(33, 5, "SER.No" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			//$this->pdf->MultiCell(25, 5, "Gold Content",	$border = 'TB', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, 5, "Value" , 			$border = 'TB', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);			
		}else if($cat=='Vehicles'){
			$this->pdf->MultiCell(70, 5, "Description" , 	$border = 'TB', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(33, 5, "Model" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(33, 5, "Engine No" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(33, 5, "Chassis No" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, 5, "Value" , 			$border = 'TB', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);			

		}

		$gw = $pw = 0;
		foreach ($RR3 as $rep_data) { 
			if($rep_data->elec_model!=''){
				$this->pdf->MultiCell(70, 0,$rep_data->elec_description." ".$rep_data->itemcode." ".$rep_data->con, 		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->elec_model, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->elec_imei, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->elec_serno, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);			
				$this->pdf->MultiCell(25, 0, d($rep_data->value), 			$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			}elseif($rep_data->veh_model!=''){
				$this->pdf->MultiCell(70, 0,$rep_data->veh_description." ".$rep_data->itemcode." ".$rep_data->con, 		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->veh_model, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->veh_engine_no, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->veh_chassis_no, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);			
				$this->pdf->MultiCell(25, 0, d($rep_data->value), 			$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);			
			}else{
				$this->pdf->MultiCell(70, 0,$rep_data->itecode." ".$rep_data->itemcode." ".$rep_data->con, 		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->elec_model, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->elec_imei, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(33, 0, $rep_data->elec_serno, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);			
				$this->pdf->MultiCell(25, 0, d($rep_data->value), 			$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			}

		}
}else{
		$this->pdf->MultiCell(115, 5, "Description of Articles Pawned" , 	$border = 'TB', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, 5, "Gold Cnt.(kt)" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, 5, "G. Weight(g)" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, 5, "N. Weight(g)" , 	$border = 'TB', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(10, 5, "Qty" , 			$border = 'TB', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
			//$this->pdf->MultiCell(25, 5, "Gold Content",	$border = 'TB', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		//$this->pdf->MultiCell(30, 5, "Value" , 			$border = 'TB', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);

		$gw = $pw = 0;
		foreach ($RR as $rep_data) {
			$this->pdf->MultiCell(115, 0,$rep_data->cat_code." ".$rep_data->itemcode." ".$rep_data->con, 		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 0, $rep_data->gold_content, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 0, $rep_data->goldweight, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 0, $rep_data->pure_weight, 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(10, 0, $rep_data->qty, 			$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			//$this->pdf->MultiCell(25, 0, $rep_data->goldtype, 		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
			//$this->pdf->MultiCell(30, 0, d($rep_data->value), 			$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);

			$gw += $rep_data->goldweight;
			$pw += $rep_data->pure_weight;
		}	
		// Total
		$this->pdf->SetFont('helvetica', 'B', 9);	
		$this->pdf->MultiCell(115,0, '', 	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(25, 0, 'Total', 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(25, 0, d($gw,3), 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(25, 0, d($pw,3), 	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(10, 0, '',	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 9);	
}


if ($is_tfr == 1){

	$this->pdf->ln();

	//var_dump($R);

	$this->pdf->MultiCell($title_w+5,  0, "Previous bill ref: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-105, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+20,  0, "Previous bill value: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-100, 0, d($old_bill_value),$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+50,  0, "   Redeem interest received: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-90, 0, d($R->old_bill_redeem_int),$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->SetFont('helvetica', '', 7);
	$this->pdf->MultiCell($title_w+5,  0, $R->old_o_new_billno,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 9);

	$this->pdf->MultiCell($title_wL-105, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+20,  0, "Enhanced amount: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-100, 0, d($R->increased_amount),$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+50,  0, "   Pawning interest upto ".$R->int_paid_untill,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-90, 0, d($R->fmintrest),$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


	$this->pdf->MultiCell($title_w+5,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-105, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+20,  0, "New bill value: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-100, 0,  d($R->requiredamount) ,$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell($title_w+50,  0, "   Net payment: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-90, 0, d($R->increased_amount),$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);



	// $this->pdf->MultiCell($title_w+20,  0, "Redeem interest for ". $IIII->old_billno."(Old)",					$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'B', $fitcell = false);
	// $this->pdf->MultiCell($title_wL-90, 0, $IIII->amount, $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'B', $fitcell = false);

}elseif ($is_tfr == 2){



	// For Part payments
	$this->pdf->ln();
	// Advanced amount
	$this->pdf->MultiCell($title_w+25,  0, "Previous bill value ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-70, 0, $old_bill_value."                                     " ,$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// Pawning advance part settlement Received
	$this->pdf->MultiCell($title_w+50,  0, "Pawning advance part settlement Received",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR-40, 0, d($R->reduced_amount - $R->old_bill_redeem_int),$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// Interest upto
	$this->pdf->MultiCell($title_w+25,  0, "Interest upto (".$R->int_paid_untill.")",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-70, 0, $R->old_bill_redeem_int."                                     ",$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// PInterest received
	$this->pdf->MultiCell($title_w+45,  0, "Interest received",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR-40, 0, $R->old_bill_redeem_int,$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// Total Receivable
	$this->pdf->SetFont('helvetica', 'B', 9);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
	$this->pdf->MultiCell($title_w+25,  0, "Total Receivable",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-100, 0, d($old_bill_value+$R->old_bill_redeem_int)."   "  ,$border = 'TB', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(30, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// Total 
	$this->pdf->MultiCell($title_w+50,  0, "Total ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(15, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR-90, 0, d(($R->reduced_amount - $R->old_bill_redeem_int) + $R->old_bill_redeem_int),$border = 'TB', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->ln();
	// New Pawning Ticket value
	$this->pdf->MultiCell($title_w+25,  0, "New Pawning Ticket value",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->MultiCell($title_wL-100, 0, d($R->requiredamount),$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(15, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$amount_in_word_1 = $this->utility->convert_number_to_words($R->requiredamount);

	$this->pdf->SetFont('helvetica', '', 8);	
	$this->pdf->MultiCell($title_w+50,  0, " (". substr($amount_in_word_1, 0,1) . strtolower(substr($amount_in_word_1, 1,10000)).")",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR-40, 0, "",$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 9);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

	$this->pdf->ln();
	// Original Bill ref
	$this->pdf->MultiCell($title_w+20,  0, "Original Bill ref: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-70, 0, $R->old_o_new_billno ,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->MultiCell($title_w+50,  0, " ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR-40, 0, "",$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->ln();
	$this->pdf->MultiCell($title_w+180,  0, "Note: Original pawning ticket and last renewal should be presented to redeem your Gold Article.",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	
	
}else{
	
	// For Pawning ticket

	//var_dump($III);

	$this->pdf->ln();$this->pdf->ln();

	$this->pdf->MultiCell($title_w+5,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-105, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+20,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-100, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell($title_w+50,  0, "Net Payment ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', 'B', 10);	
	$this->pdf->MultiCell($title_wL-90, 0, number_format($R->requiredamount,2) ,$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$amount_in_word = $this->utility->convert_number_to_words($R->requiredamount);

	$this->pdf->SetFont('helvetica', '', 9);	


	$this->pdf->MultiCell($title_w+5,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-105, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+20,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-100, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell($title_w+50,  0, "Pawning Interest up to (".$R->int_paid_untill.")",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-90, 0, number_format($III->amount,2),$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	

}




	//$this->pdf->MultiCell(20, 0, $III->discount, 	$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);

$this->pdf->SetFont('helvetica', '', 8);
$this->pdf->ln(8);
$this->pdf->MultiCell(100,0, 


	
	 substr($amount_in_word, 0,1) . strtolower(substr($amount_in_word, 1,10000))



	,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->setY(110);

$this->pdf->SetFont('helvetica', '', 7);

if ($is_tfr != 2){
	$this->pdf->MultiCell(0,  0         , "I hereby pawn and hand over to the ".$Q->company_name." the above gold articles owned by me. I declare that the above particulars are true and correct and agree that the terms and conditions stated overleaf shall form part of this contract of pawn.",	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
}

$this->pdf->SetFont('helvetica', '', 9);	

$this->pdf->ln(10);

$this->pdf->MultiCell($title_w+28,  0, "..................................................",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w+28,  0, "..................................................",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w+28,  0, "..................................................",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w+28,  0, "..................................................",	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w+28,  0, "Valuer",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w+28,  0, "Cashier",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w+28,  0, "Manager",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w+28,  0, "Pawner",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


// $this->pdf->IncludeJS("javascript:print();");
$this->pdf->Output("pawn_ticket.pdf", 'I');

	
function d($n,$d=2){
	return 	number_format($n,$d);				
}



?>