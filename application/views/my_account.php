<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<div class="page_contain_new_pawn">
	
	<div class="page_main_title_new_pawn">
		<span style="width:150px;float:left">My Account</span>		
		<span style="width:auto; float:right; font-size:12px; padding-top:4px;"><?=$User?></span>	
	</div>

<div style="padding:10px;">
	<table class="tbl_my_account" border="0">
		<tr>
			<td valign="top">
				<div class="div_content_block">
					<form method="post" action="" id="frmChangePw">
						<span class="title_a">Change Password</span><br><br>

						Current Password<div style="padding:3px"></div>
						<input type="password" class="input_text_regular_myaccount" name="OlduserPassword" id="OlduserPassword" ><br><br>

						New Password<div style="padding:3px"></div>
						<input type="password" class="input_text_regular_myaccount" name="userPassword" id="userPassword" ><br><br>

						Confirm Password<div style="padding:3px"></div>
						<input type="password" class="input_text_regular_myaccount" name="ruserPassword" id="ruserPassword" ><br><br>

						<input type="button" id="btnChangePw" class="btn_regular" value="Change">
					</form>
				</div>

			</td>
			<td valign="top">
				<div class="div_content_block">
					<form method="post" action="" id="frmChangePw">
						<span class="title_a">Change Display Name</span><br><br>			


						<?php
							
							if ($display_name == ''){
								
								echo '<div class="dis_name_msg"><div style="padding:10px; background-color:red;color:#ffffff">Display name not added</div><br></div>';
								$dn = '';
							}else{
								$dn = $display_name;
							}

						?>

						<input type="text" class="input_text_regular_myaccount" id="display_name" value="<?=$dn?>"><br><br>
						<input type="button" id="btnChangeDisplayName" class="btn_regular" value="Change">
					</form>
				</div>
			</td>
			<td valign="top"></td>
		</tr>
	</table>    
</div>
</div>
</body>
</html>