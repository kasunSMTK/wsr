<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

	$(function() {
		$( ".date_ch_allow" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2025",
			onSelect: function(date){
				change_current_date(date);				
			}
		});
	});
	
</script>


<div class="search_bg"></div>
<div class="search_inner">

    <table border="0" width="100%">
        <tr>
            <td colspan="2">
                <div class="close_cus_his_window">X</div>
                <div class="search_customer_name"></div>
                <div class="search_customer_nic"></div>
            </td>            
        </tr>
        <tr>
            <td colspan="2"><span class="search_customer_address"></span></td>            
        </tr>        
        
        <tr>
            <td colspan="2"><span class="search_customer_contact"></span></td>            
        </tr>

        <tr>
            <td width="210"><br>

                <div style="width: 100%; height: 100%;border:1px solid #cccccc;padding: 15px;">
                    <div class="cus_his_cus_title">Total Pawning Amount</div>
                    <div class="cus_his_cus_val cus_total_pawn_amount">00.00</div> 
                </div>

            </td>            
            <td></td>
        </tr>

        <tr>
            <td colspan="2" class="cus_bill_list_msg"><br>

                <table width="100%">
                    <tr>
                        <td valign="top">
                            Customer's Pawning Bill(s) <br><br>
                            <div class="customer_bill_list"></div>
                        </td>

                        <td valign="top">
                            <div class="selected_bill_details"></div>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

        
    </table>

</div>


<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Search</span></div>		
	</div><br>

	<form method="post" action="<?=base_url()?>index.php/main/load_data/T_customer_statment/customer_statment" id="form_">
		
	<table width="100%" cellpadding="0" cellspacing="0" border="0" class="tbl_master_" align="center" style="">
        
       <tr>
                
            <td valign="top" width="280" style="padding-left:10px" class="search_input">
                
            	Branch<br>
                <?=$bc?>
                <br>                

                NIC<br>
                <input type="text" name="aa" id="aa" class="input_text_search" value="">                
                <br>

                Name<br>
                <input type="text" name="bb" id="bb" class="input_text_search">                
                <br>

                Address<br>
                <input type="text" name="cc" id="cc" class="input_text_search">                
                <br>

                Address 2<br>
                <input type="text" name="dd" id="dd" class="input_text_search">                
                <br>                

                Telephone<br>
                <input type="text" name="ee" id="ee" class="input_text_search">                
                <br>

                Mobile<br>
                <input type="text" name="ff" id="ff" class="input_text_search">                
                <br>
                

                <input type="button" id="btnCusDET" class="btn_regular" value="Search" style="width:60px;margin-top:5px">














                <!-- Customer Name/NIC/Address<br>
                <input type="text" name="customer_name_nic_address" id="customer_name_nic_address" class="input_text_search">
                <input type="button" id="btnCusDET" class="btn_regular" value="Search" style="width:60px;margin-top:5px">
                <br>                
            
                Pawn Action<br>
                <input type="text" name="status" id="status" class="input_text_search">
                <input type="button" id="btnStatus" class="btn_regular" value="Search" style="width:60px;margin-top:5px">
                <br>
            
                Bill Number<br>
                <input type="text" name="billno" id="billno" class="input_text_search">
                <input type="button" id="btnbillno" class="btn_regular" value="Search" style="width:60px;margin-top:5px">
                <br>
            
                Date<br>
                <input type="text" name="date" id="date" class="input_text_search">
                <input type="button" id="btnDate" class="btn_regular" value="Search" style="width:60px;margin-top:5px"><br><br>
                -->
            </td>

            <td valign="top" style="padding-right:10px">
                <input type="hidden" name="customer_id" id="customer_id">
                <div class="search_result"></div>
            </td>

        </tr>


        
    </table>

	</form>    

</div>
</body>
</html>