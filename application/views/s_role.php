<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<div class="page_contain">
    <div class="page_main_title"><span>Create/Manage Role</span></div><br>

    
    <form  method="post" id="_form" action="<?=base_url()?>index.php/main/save/s_role" >

        <div class="dframe" id="mfram" style="text-align: center;">
            <table width="95%" border="0" align="center" class='tbl_role_sum'>

                <tr>              
                    <td width="100%" style='padding-right:20px;'>
                        <div class='role_input_A'>
                            <?=$added_user_role?> <!-- <input name="code" type="text" class="input_txt" id="code" style="width: 150px;" title="code" /> --> 
                            <input type="hidden" name="des" id="des" class="input_txt" id="des" style="width: auto;" title="Description" />
                        </div>
                        <div class='role_input_B'>
                        </div>
                    </td>  
                    <td colspan="3">                                            
                        <input type="button" class="btn_regular" id="btnAddNewRole" value="Create New Role" style="width:120px" />
                        <input type="hidden" name="code_" id="code_" title="0" />
                        <input type="hidden" name="bc" id="bc" />
                    </td>
                </tr>
                <!-- <tr>
                    <td>Branch</td>
                    <td> <?=$branch;?></td>
                    <td>&nbsp;</td>
                </tr> -->

            </table>











        <table width="100%" border="0" align="center">
            <tr>
                <td colspan="2" valign="top" class="content" style="width: 1800px;">

                <div class="form" id="form">    

            <?php if(isset($_GET['key'])){ echo "<span style='color: blue;' >".base64_decode($_GET['key'])."</span>"; } ?>

            <table style="width: 100%" id="tgrid" border="0">

                <thead>

                <tr>

                    <th class="tb_head_th" style='width : 75px' >Module Id</th>

                    <th class="tb_head_th" >Name</th> 

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>View<br><input type='checkbox' id='all_view'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Add<br><input type='checkbox' id='all_add'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Edit<br><input type='checkbox' id='all_edit'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Delete<br><input type='checkbox' id='all_delete'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Print<br><input type='checkbox' id='all_print'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Re-Print<br><input type='checkbox' id='all_r_print'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Back Date<br><input type='checkbox' id='all_back_date'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Authoriser<br><input type='checkbox' id='all_authoriser'/></th>

                    <th class="tb_head_th"  style='width : 80px;text-align:center'>Accept<br><input type='checkbox' id='all_accept'/></th>

                </tr>

                </thead>

                <?php

                    if (count($table_data) > 0){

                        foreach($table_data as $r){

                            echo "<tr>";
                            echo "<td><input type='text' readonly='readonly' name='m_code".$r->m_code."'        class='g_input_num module_txt' id='m_code".$r->m_code."'        style='width : 100%;font-size:22px' value='".$r->m_code."'/></td>";
                            echo "<td><input type='text' readonly='readonly' name='m_description".$r->m_code."' class='g_input_txt module_txt' id='m_description".$r->m_code."' style='width : 100%;font-size:22px' value='".$r->module_name."'/></td>";   
                            

                            echo "<td style='text-align: center;'><input type='checkbox' name='is_view".$r->m_code."' id='is_view".$r->m_code."'            class='ob_a' value='1' /></td>";
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_add".$r->m_code."' id='is_add".$r->m_code."'              class='ob_b' value='1'/></td>";
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_edit".$r->m_code."' id='is_edit".$r->m_code."'            class='ob_e' value='1'/></td>";
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_delete".$r->m_code."' id='is_delete".$r->m_code."'        class='ob_d' value='1'/></td>";
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_print".$r->m_code."' id='is_print".$r->m_code."'          class='ob_f' value='1'/></td>";
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_re_print".$r->m_code."' id='is_re_print".$r->m_code."'    class='ob_g' value='1'/></td>";
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_back_date".$r->m_code."' id='is_back_date".$r->m_code."'  class='ob_h' value='1'/></td>";
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_authoriser".$r->m_code."' id='is_authoriser".$r->m_code."' class='ob_i' value='1'/></td>";    
                            echo "<td style='text-align: center;'><input type='checkbox' name='is_approve".$r->m_code."' id='is_approve".$r->m_code."'        class='ob_j' value='1'/></td>";        
                            echo "</tr>";

                        }

                    }else{

                            echo "<tr>";
                            echo "<td colspan='12'>No modules added</td>";        
                            echo "</tr>";

                    }

                ?>

            </table>            

                </div>

                </td>

                </tr>

        </table>

    </form>

</div><br><br><br><br>


<div style="text-align: right; padding: 7px;" class="div_s_role_fixed-controls">
    <input type="button" class="btn_regular" id="btnSave" value="Save" />    
</div>



</body>
</html>