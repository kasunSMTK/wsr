<?php

$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->SetMargins(5, 5, 0, true);
$this->pdf->AddPage();

$this->pdf->SetFont('helvetica', '', 18);
$this->pdf->setY(10);
$this->pdf->setX(0);

$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
$this->pdf->Line(5, 0, 0, 0, $style);

$this->pdf->MultiCell(0, 0, "Tag Stock Category wise Report", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 15);
$this->pdf->MultiCell(0, 0, "Between " . $fd . " and " . $td, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->setX(10);
$this->pdf->ln();

$this->pdf->SetFont('helvetica', 'B', 8);

// $this->pdf->MultiCell(20, 1, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, 1, "No", '1', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, 1, "Loan No", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, 1, "Item", '1', 'C', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(40, 1, "Item Name", '1', 'L', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "tag No", '1', 'R', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Gold Weight", '1', 'R', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, 1, "Value", '1', 'R', 0, 1, '', '', '', '', 0);

$this->pdf->SetFont('', '', 7);

$bc = "";
$cat="";
$toal_w = 0;
$toal_v = 0;
$sub_w=0;
$sub_v=0;

foreach ($det as $key=>$r) {
   
    $height = 5 * (max(1, $this->pdf->getNumLines($r->itemname, 50)));
    $this->pdf->HaveMorePages($height);
    if ($bc != $r->bc) {
        $this->pdf->SetFont('helvetica', 'B', 8); 
        // $this->pdf->MultiCell(20, 1, "", '0', 'L', 0, 0, '', '', false, '', 0);  
        $this->pdf->MultiCell(10, $height, $r->bc, '0', 'C', 0, 1, '', '', false, '', 0);
    }
    if ($cat  != $r->cat_code) {
        $this->pdf->SetFont('helvetica', 'B', 8); 
        // $this->pdf->MultiCell(20, 1, "", '0', 'L', 0, 0, '', '', false, '', 0);  
        $this->pdf->MultiCell(50, $height, $r->cat_code ."-". $r->cat_des, '0', 'L', 0, 1, '', '', false, '', 0);
    }
  
    $this->pdf->SetFont('helvetica', '', 8);
    // $this->pdf->MultiCell(20, 1, "", '0', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(20, $height, $r->nno, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(20, $height, $r->loanno, '1', 'L', 0, 0, '', '', false, '', 0);
    $this->pdf->MultiCell(30, $height, $r->item, '1', 'L', 0, 0, '', '', false, '', 0);

    $this->pdf->MultiCell(40, $height, $r->itemname, '1', 'L', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(30, $height, $r->tag_no, '1', 'R', 0,0, '', '', '', '', 0);
    $this->pdf->MultiCell(30, $height, $r->goldweight, '1', 'R', 0, 0, '', '', '', '', 0);
    $this->pdf->MultiCell(30, $height, $r->value, '1', 'R', 0, 1, '', '', '', '', 0);

   
    $cat = $r->cat_code;
    
    $bc = $r->bc;
    $sub_w +=$r->goldweight;
    $sub_v += $r->value;
    $toal_w +=$r->goldweight;
    $toal_v  += $r->value;
    if($r->cat_code !=$det[$key+1]->cat_code){

        $this->pdf->SetFont('helvetica', 'B', 8);
        // $this->pdf->MultiCell(20, 1, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(20, $height,"", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(20, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
        $this->pdf->MultiCell(30, $height, "", '0', 'L', 0, 0, '', '', false, '', 0);
    
        $this->pdf->MultiCell(40, $height, "", '0', 'L', 0, 0, '', '', '', '', 0);
        $this->pdf->MultiCell(30, $height, "Sub Total", '0', 'C', 0,0, '', '', '', '', 0);
        $this->pdf->MultiCell(30, $height, number_format($sub_w,2), 'TB', 'R', 0, 0, '', '', '', '', 0);
        $this->pdf->MultiCell(30, $height, number_format($sub_v,2), 'TB', 'R', 0, 1, '', '', '', '', 0);
        $sub_w =0;
        $sub_v = 0;

    }
    
  

}

$this->pdf->SetFont('helvetica', 'B', 8);
// $this->pdf->MultiCell(20, 1, "", '0', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, $height,"", '', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(20, $height, "", '', 'L', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(30, $height, "", '', 'L', 0, 0, '', '', false, '', 0);

$this->pdf->MultiCell(40, $height, "", '', 'L', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, $height, "Total", '', 'C', 0,0, '', '', '', '', 0);
$this->pdf->MultiCell(30, $height, number_format( $toal_w,2), 'TB', 'R', 0, 0, '', '', '', '', 0);
$this->pdf->MultiCell(30, $height, number_format( $toal_v,2), 'TB', 'R', 0, 1, '', '', '', '', 0);
$this->pdf->Output("PDF.pdf", 'I');
