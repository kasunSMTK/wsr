<?php
	
	$make_excel = true;	
	
	$t = '<table border="1">';


	$t .= '<tr>';
	$t .= '<td colspan="9"><h1>Advance Payments Report</h1></td>';
	$t .= '</tr>';
	
	$t .= '<tr>';
	$t .= '<td colspan="9"><b>From : '.$fd.' To : '.$td.'</b></td>';	
	$t .= '</tr>';




	$t .= '<tr>';
	$t .= '<td><b>S.No</b></td>';
	$t .= '<td><b>Bill Type</b></td>';
	$t .= '<td><b>Bill No</b></td>';
	$t .= '<td><b>Cus Name</b></td>';
	$t .= '<td><b>NIC</b></td>';
	$t .= '<td><b>Mobile</b></td>';
	$t .= '<td align="center"><b>Total Weight</b></td>';
	$t .= '<td><b>Paid DateTime</b></td>';
	$t .= '<td align="right"><b>Amount</b></td>';
	$t .= '</tr>';
	
	$no 	= 1;
	$bc_tot = 0;

	$first_round_passes = false;

	foreach($list as $r){

		if ($r->bc != $st){	

			$bc_name = $r->bc_name;
			$st 	 = $r->bc;
			
			//$this->pdf->SetFont('helvetica', 'B', 8);	
			
			if ( $first_round_passes ){
				
				$t .= '<tr>';
				$t .= '<td>'.'</td>';
				$t .= '<td>'.'</td>';
				$t .= '<td>'.'</td>';
				$t .= '<td>'.'</td>';
				$t .= '<td>'.'</td>';
				$t .= '<td>'.'</td>';
				$t .= '<td align="center"><b>'.dd($bc_tot_w).'</b></td>';
				$t .= '<td>'.'</td>';
				$t .= '<td align="right"><b>'.number_format($bc_tot,2).'</b></td>';
				$t .= '</tr>';
		
			}

			$t .= '<td><b>'.$bc_name.'</b></td>';
		
			$first_round_passes = true;
			$bc_tot = 0;
			$bc_tot_w = 0;

		}else{

			$bc_name = "";

		}
		
		$t .='<tr>';
		$t .='<td>'.$no.'</td>';
		$t .='<td>'.$r->billtype.'</td>';
		$t .='<td>'.$r->billno.'</td>';
		$t .='<td>'.$r->cusname.'</td>';
		$t .='<td>'.$r->nicno.'</td>';
		$t .='<td>'.$r->mobile.'</td>';
		$t .='<td align="center">'.$r->totalweight.'</td>';
		$t .='<td>'.str_replace(",", "<br>",$r->action_date).'</td>';
		$t .='<td align="right">'.number_format($r->amount,2).'</td>';
		$t .='</tr>';
		$no++;

		$bc_tot += $r->amount;
		$bc_tot_w += $r->totalweight;

	}


	// last branch total
	
	$t .='<tr>';
	$t .='<td></td>';
	$t .='<td></td>';
	$t .='<td></td>';
	$t .='<td></td>';
	$t .='<td></td>';
	$t .='<td></td>';
	$t .='<td align="center"><b>'.dd($bc_tot_w).'</b></td>';
	$t .='<td></td>';
	$t .='<td align="right"><b>'.number_format($bc_tot,2).'</b></td>';
	$t .='</tr>';



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function dd($number) {
	    return number_format($number, 3, '.', ',');
	}


	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'income_statement.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

?>