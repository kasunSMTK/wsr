<?php



// $this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
// $this->pdf->SetPrintHeader(false);
// $this->pdf->SetPrintFooter(false);
// $this->pdf->AddPage();	
$this->pdf = new TCPDF("L", PDF_UNIT, 'SEW', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetAutoPageBreak(TRUE, 0);
$this->pdf->AddPage();



$this->pdf->SetFont('helvetica', '', 28);	

$this->pdf->setY(10);

$this->pdf->setX(0);



$style = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(204, 204, 204));

if ($_POST['is_v_re_p'] == 1){
 		$is_reprint = " (Re-Print)";
 	}else{
 		$is_reprint = "";
 	}


foreach ($list as $r) {
	$no=$r->no;
	$ref_no=$r->ref_no;
	$date_time=$r->date_time;
	$requ_initi_bc=$r->requ_initi_bc;
	$ini_branch=$r->ini_branch;
	$from_branch=$r->from_branch;
	$to_branch=$r->to_branch;
	$amount=$r->amount;
	$comment=$r->comment;
	$description=$r->description;
	$to_acc=$r->to_acc;
	$cheque_no = $r->cheque_no;

	$transfer_type=$r->transfer_type;
}

if($transfer_type=="cash"){

 for($z=1; $z<2; $z++){






	$this->pdf->SetFont('helvetica', 'B', 14);
	$this->pdf->setX(10);
	$this->pdf->MultiCell(130, 1, "Fund Tranfer Voucher - Cash Collection $is_reprint", '0','L', 0, 0, '', '', false, '', 0);
	
	$this->pdf->SetFont('helvetica', '', 9);
	
	$this->pdf->MultiCell(100, 1, "  ", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->ln();
	$this->pdf->ln();
	$this->pdf->ln();


		
		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(23, 1, "Voucher No", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".$no, '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "Ref No", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(30, 1, " : ".$ref_no, '0','L', 0, 0, '', '', false, '', 0);

		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(23, 1, "Date Time", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".$date_time, '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "Request Initiated By ", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".$requ_initi_bc." - ".$ini_branch, '0','L', 0, 0, '', '', false, '', 0);


		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(23, 1, "From Branch", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".$from_branch, '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "To Branch ", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(30, 1, " : ".$to_branch, '0','L', 0, 0, '', '', false, '', 0);


		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(23, 1, "Paying Type", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".$transfer_type, '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "Amount", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(30, 1, " : ".number_format($amount,2), '0','L', 0, 0, '', '', false, '', 0);

		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(20, 1, "Memo", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(130, 1, " : ".$comment, '0','L', 0, 0, '', '', false, '', 0);
		
		$this->pdf->Ln();
	

		$tt = date('Y-m-d') . " ".date("H:i");

		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(40, 1, "Generated date and time", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(130, 1, " : ".$tt, '0','L', 0, 0, '', '', false, '', 0);
		


	

		$this->pdf->Ln();
		$this->pdf->Cell(30, 1, ".....................................", '0', 0, 'L', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "......................................", '0', 0, 'L', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "......................................", '0', 0, 'L', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "......................................", '0', 0, 'L', 0);
	 	
	 	
	 	$this->pdf->Ln();
		//$this->GetY();

	 	$this->pdf->Cell(30, 1, "Prepared By", '0', 0, 'C', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "Collector", '0', 0, 'C', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "Cash Payee", '0', 0, 'C', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "Cash Receiver", '0', 0, 'C', 0);

	 		$this->pdf->Ln();		
	 		$this->pdf->Ln();
	 		$this->pdf->Ln();
	 		$this->pdf->Ln();
	 		$this->pdf->Ln();
	 		

	}

}else if($transfer_type=="cheque_withdraw"){

//===============================================cheque withdrawal============================================================
	for($z=1; $z<2; $z++){	


	$this->pdf->SetFont('helvetica', 'B', 14);
	$this->pdf->setX(10);
	$this->pdf->MultiCell(130, 1, "Fund Tranfer Voucher - Cheque Withdraw $is_reprint", '0','L', 0, 0, '', '', false, '', 0);
	
	$this->pdf->SetFont('helvetica', '', 9);
	
	$this->pdf->MultiCell(100, 1, "  ", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->ln();
	$this->pdf->ln();
	$this->pdf->ln();


	//$this->pdf->MultiCell(0, 0, "Fund Tranfer Voucher - Cheque Withdraw", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	//$this->pdf->ln();

	//foreach($list as $r){
		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(40, 1, "Voucher No", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".$no, '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "Ref No", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(30, 1, " : ".$ref_no, '0','L', 0, 0, '', '', false, '', 0);

		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(40, 1, "Date Time", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".$date_time, '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "Request Initiated By ", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(70, 1, " : ".$requ_initi_bc." - ".$ini_branch, '0','L', 0, 0, '', '', false, '', 0);


		$this->pdf->Ln();
		$this->pdf->SetFont('helvetica', '', 9);		
		$this->pdf->MultiCell(40, 1, "Amount", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, " : ".number_format($amount,2), '0','L', 0, 0, '', '', false, '', 0);

		$this->pdf->Ln();
		$this->pdf->SetFont('helvetica', '', 9);
		$this->pdf->MultiCell(40, 1, "Cheque Number", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(130, 1, " : ".$cheque_no, '0','L', 0, 0, '', '', false, '', 0);

		$this->pdf->Ln();
		$this->pdf->SetFont('helvetica', '', 9);
		$this->pdf->MultiCell(40, 1, "Memo", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(130, 1, " : ".$comment, '0','L', 0, 0, '', '', false, '', 0);
		


		$this->pdf->Ln();

		$tt = date('Y-m-d') . " ".date("H:i");

		$this->pdf->SetFont('helvetica', '', 9);

		$this->pdf->MultiCell(40, 1, "Generated date and time", '0','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(130, 1, " : ".$tt, '0','L', 0, 0, '', '', false, '', 0);
		


	//}

		$this->pdf->Ln();
		$this->pdf->Ln();
		$this->pdf->Cell(30, 1, ".....................................", '0', 0, 'L', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "......................................", '0', 0, 'L', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "......................................", '0', 0, 'L', 0);
	 	
	 	
	 	
	 	$this->pdf->Ln();
		
	 	$this->pdf->Cell(30, 1, "Prepared By", '0', 0, 'C', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "Collector", '0', 0, 'C', 0);
	 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
	 	$this->pdf->Cell(30, 1, "Cash Receiver", '0', 0, 'C', 0);


	 	$this->pdf->Ln();		
 		$this->pdf->Ln();
 		$this->pdf->Ln();
 		$this->pdf->Ln();
 		$this->pdf->Ln();


	}

}else if($transfer_type=="bank_deposit"){

	for($z=1; $z<2; $z++){	


	$this->pdf->SetFont('helvetica', 'B', 14);
	$this->pdf->setX(10);
	$this->pdf->MultiCell(130, 1, "Fund Tranfer Voucher - Cash Deposit $is_reprint", '0','L', 0, 0, '', '', false, '', 0);
	
	$this->pdf->SetFont('helvetica', '', 9);
	
	$this->pdf->MultiCell(100, 1, "   ", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->ln();
	$this->pdf->ln();
	$this->pdf->ln();



	$this->pdf->SetFont('helvetica', '', 9);

	$this->pdf->MultiCell(40, 1, "Voucher No", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(60, 1, " : ".$no, '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "Date Time", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(60, 1, " : ".$date_time, '0','L', 0, 0, '', '', false, '', 0);

	$this->pdf->Ln();

	$this->pdf->SetFont('helvetica', '', 9);

	$this->pdf->MultiCell(40, 1, "From Branch", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(60, 1, " : ".$from_branch, '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "Request Initiated By ", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(70, 1, " : ".$requ_initi_bc." - ".$ini_branch."", '0','L', 0, 0, '', '', false, '', 0);


	$this->pdf->Ln();

	$this->pdf->SetFont('helvetica', '', 9);

	//$this->pdf->MultiCell(40, 1, "Paying Type", '0','L', 0, 0, '', '', false, '', 0);
	//$this->pdf->MultiCell(60, 1, " : ".$r->transfer_type, '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "Amount", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(60, 1, " : ".number_format($amount,2), '0','L', 0, 0, '', '', false, '', 0);

	
	$this->pdf->Ln();

	$this->pdf->SetFont('helvetica', '', 9);

	$this->pdf->MultiCell(40, 1, "Bank", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(130, 1, " : ".$description, '0','L', 0, 0, '', '', false, '', 0);
	

	$this->pdf->Ln();

	$this->pdf->SetFont('helvetica', '', 9);

	$this->pdf->MultiCell(40, 1, "Memo", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(130, 1, " : ".$comment, '0','L', 0, 0, '', '', false, '', 0);
	


	$this->pdf->Ln();

	$tt = date('Y-m-d') . " ".date("H:i");

	$this->pdf->SetFont('helvetica', '', 9);

	$this->pdf->MultiCell(40, 1, "Generated date and time", '0','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(130, 1, " : ".$tt, '0','L', 0, 0, '', '', false, '', 0);
	




	$this->pdf->Ln();
	$this->pdf->Ln();
	$this->pdf->Cell(30, 1, ".....................................", '0', 0, 'L', 0);
 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
 	$this->pdf->Cell(30, 1, "......................................", '0', 0, 'L', 0);
 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
 	$this->pdf->Cell(30, 1, "......................................", '0', 0, 'L', 0);
 	
 	
 	
 	$this->pdf->Ln();
	
 	$this->pdf->Cell(30, 1, "Prepared By", '0', 0, 'C', 0);
 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
 	$this->pdf->Cell(30, 1, "Collector", '0', 0, 'C', 0);
 	$this->pdf->Cell(20, 1, "", '0', 0, 'L', 0);
 	$this->pdf->Cell(30, 1, "Casher", '0', 0, 'C', 0);

 		$this->pdf->Ln();		
 		$this->pdf->Ln();
 		$this->pdf->Ln();
 		$this->pdf->Ln();
 		$this->pdf->Ln();

  }

}


	$this->pdf->Output("PDF.pdf", 'I');



	?>