<div class="page_contain">
  <div class="page_main_title"><span>Create User</span></div>
  <br>


  <div style="padding:20px;border: 0px solid red; float: left; width: 50%;" class="div_user_create">
  
    <form method="post" action="<?=base_url()?>index.php/main/save/u_users" id="form_">
    <table cellpadding="0" cellspacing="0" border="01" class="tbl_master_" align="center" style="margin:auto">
      <tr>
        <td><b>Add New User</b></td>
        <td></td>
      </tr>
      <tr>
        <td height="20">
          <div class="text_box_holder_">
            <div class="text_box_title_holder">Code</div> 
            <input class="input_text_regular" type="text" id="cCode" readonly="readonly" value="<?=$nxtCode?>" id="code" maxlength="3">
          </div>
        </td>    
        <td height="20" rowspan="14" valign="top"></td>         
      </tr> 
      <tr>
        <td height="20">
          <div class="text_box_holder_">
            <div class="text_box_title_holder">Branch</div> 
            <?=$bc_dropdown?>
          </div>
        </td>     

      </tr>       
      <tr>
        <td height="20"><div class="text_box_holder_">
            <div class="text_box_title_holder">User Type</div>
            <select class="input_ui_dropdown" name="isAdmin" id="isAdmin">
              <option value="">Select User Type</option>
              <option value="0">User</option>
              <option value="1">Administrator</option>
              <option value="2">Account</option>
            </select>
          </div></td>
      </tr>
      <tr>
        <td height="20"><div class="text_box_holder_"> <span class="text_box_title_holder" style="margin-top:-13px">Description</span>
            <input class="input_text_regular" type="text" name="discription" id="discription" maxlength="50">
          </div></td>
      </tr>
      <tr>
        <td height="20"><div class="text_box_holder_"> <span class="text_box_title_holder" style="margin-top:-13px">User Name</span>
            <input class="input_text_regular" type="text" name="loginName" id="loginName" maxlength="50">
          </div></td>
      </tr>
      <tr>
        <td height="20"><div class="text_box_holder_"> <span class="text_box_title_holder" style="margin-top:-13px">Password</span>
            <input class="input_text_regular" type="password" id="ruserPassword">
            <!-- <input class="btn_regular" style="width:20px; display:inline-block;" type="button" value="C" id="cp"> -->
          </div></td>
      </tr>
      <tr>
        <td height="20"><div class="text_box_holder_"> <span class="text_box_title_holder" style="margin-top:-13px">Confirm Password</span>
            <input class="input_text_regular" type="password" name="userPassword" id="userPassword">
          </div></td>
      </tr>
      <tr>
        <td height="20">
          <div class="text_box_holder_" style="border:0px solid red;height:20px;margin-top:-10px">
            <label><div style="float: left"><input type="checkbox" name="isHO" id="isHO"></div>
            <div style="float: left;margin-top:5px"><span class="text_box_title_holder" style="margin-top:-13px">H/O User</span></div></label>
          </div>
        </td>
      </tr>

      <tr>
        <td height="20">
          <div class="text_box_holder" style="border:0px solid red;height:20px;margin-top:-10px">
            <label><div style="float: left"><input type="checkbox" name="is_HO_cashier" id="is_HO_cashier"></div>
              <div style="float: left;margin-top:5px;width: 250px">
                <span class="text_box_title_holder" style="width:100px;margin-top:-13px">Is H/O Cashier</span>
              </div>
            </label>
          </div>
        </td>
      </tr>

      <tr>
        <td height="20">
          <div class="text_box_holder" style="border:0px solid red;height:20px;margin-top:-10px">
            <label><div style="float: left"><input type="checkbox" name="bc_manager" id="bc_manager"></div>
              <div style="float: left;margin-top:5px;width: 250px">
                <span class="text_box_title_holder" style="width:140px;margin-top:-13px">Branch Manager</span>
              </div>
            </label>
          </div>
        </td>
      </tr>

      <tr>
        <td height="20">
          <div class="text_box_holder" style="border:0px solid red;height:20px;margin-top:-10px">
            <label><div style="float: left"><input type="checkbox" name="multi_bc_login" id="multi_bc_login"></div>
              <div style="float: left;margin-top:5px;width: 250px">
                <span class="text_box_title_holder" style="width:160px;margin-top:-13px">Allow Multi Branch Login</span>
              </div>
            </label>
          </div>
        </td>
      </tr>

      <tr>
        <td height="20">
          <div class="text_box_holder" style="border:0px solid red;height:40px;margin-top:-10px">
          Zone <span style="color:#999999">(Optional : Currently required for voucher approval zone apply only )</span>
            <div style="width: 450px;float: left;padding-right: 10px"><?=$area?></div><div style="padding:10px"><a class='add_user_zone'>Add</a></div>
          </div>

          <div class="div_zone_list"></div>

          <br>
        </td>
      </tr>


      <tr>
         <td height="20" align="right">

         <div style="float: left"><input type="button" value="Save"   name="btnSave"   id="btnSave"   class="btn_regular"></div>
        <input type="button" value="Reset Password"   name="btnRPass"   id="btnRPass"   class="btn_regular" style="width:auto; display:none;">
        <input type="hidden" value="0"      name="hid"       id="hid" >
                 
          <label><input type="checkbox" name="disable_login" id="disable_login" disabled="disabled"> <span style="color: red">Disable Login</span></label>
          <br><br>
        </td>
      </tr>

      
    </table>
  </form>
  </div>
  

  <div style="padding:20px;border: 0px solid red; float: right; width: 50%">
    <b>Exist Users</b>
    <br>
    <input type="text" class="input_text_regular_new_pawn Search_bcname" style="border:1px solid Green;font-size: 13px;width:218px">
    <input type="button" value="Search" class="btn_regular user_search"><div class="list_div"><!-- <?=$load_list?> --></div>
  </div>


  
</div>
</body></html>