<?php

$make_excel = 1;

$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
$t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';

$t .= '<tr>';
$t .= '<td>Unredeemed Articles Maturity Analysis</td>';
$t .= '<td colspan="26">Between ' .$fd . ' and ' . $td.'</td>';
$t .= '</tr>';

$t .= '<tr>';
$t .='<td align="right" ></td>';
$t .='<td align="center" colspan="2">1-30</td>';
$t .='<td align="center" colspan="2">31-60</td>';
$t .='<td align="center" colspan="2">61-90</td>';
$t .='<td align="center" colspan="2">91-120</td>';
$t .='<td align="center" colspan="2">121-150</td>';
$t .='<td align="center" colspan="2">151-180</td>';
$t .='<td align="center" colspan="2">181-210</td>';
$t .='<td align="center" colspan="2">211-240</td>';
$t .='<td align="center" colspan="2">241-270</td>';
$t .='<td align="center" colspan="2">271-300</td>';
$t .='<td align="center" colspan="2">301-330</td>';
$t .='<td align="center" colspan="2">331-360</td>';
$t .='<td align="center" colspan="2">361-390</td>';
$t .='<td align="center" colspan="2">Over 390</td>';
$t .= '</tr>';


$t .= '<tr>';
$t .= '<td><b>Branch</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '<td align="center"><b>Weight</b></td>';
$t .= '<td align="right"><b>Amount</b></td>';

$t .= '</tr>';

$s_bc = '';

$first_round_passed = false;

$A= $B= $C= $D= $E= $F= $G= $H= $I= $J= $K= $L= $M= $N= 0;
$AWE= $BWE= $CWE= $DWE= $EWE= $FWE= $GWE= $HWE= $IWE= $JWE= $KWE= $LWE= $MWE= $NWE= 0;


foreach($list as $r){

	if ( $s_bc != $r->bc ){	
		
		$first_round_passed = true;	

		$s_bc = $r->bc;
	}

	$t .= '<tr>';
	$t .='<td>'. $r->bc_name .'</td>';
	
	$t .='<td align="center">'. number_format($r->R30WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R30,2).'</td>';

	$t .='<td align="center">'. number_format($r->R60WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R60,2).'</td>';

	$t .='<td align="center">'. number_format($r->R90WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R90,2).'</td>';

	$t .='<td align="center">'. number_format($r->R120WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R120,2).'</td>';

	$t .='<td align="center">'. number_format($r->R150WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R150,2).'</td>';

	$t .='<td align="center">'. number_format($r->R180WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R180,2).'</td>';

	$t .='<td align="center">'. number_format($r->R210WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R210,2).'</td>';

	$t .='<td align="center">'. number_format($r->R240WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R240,2).'</td>';

	$t .='<td align="center">'. number_format($r->R270WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R270,2).'</td>';

	$t .='<td align="center">'. number_format($r->R300WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R300,2).'</td>';

	$t .='<td align="center">'. number_format($r->R330WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R330,2).'</td>';

	$t .='<td align="center">'. number_format($r->R360WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R360,2).'</td>';

	$t .='<td align="center">'. number_format($r->R390WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->R390,2).'</td>';

	$t .='<td align="center">'. number_format($r->OVR390WE,2).'</td>';
	$t .='<td align="right">'. number_format($r->OVR390,2).'</td>';

	$t .= '</tr>';

	$AWE += $r->R30WE;
	$A += $r->R30;

	$BWE += $r->R60WE;
	$B += $r->R60;

	$CWE += $r->R90WE;
	$C += $r->R90;

	$DWE += $r->R120WE;
	$D += $r->R120;

	$EWE += $r->R150WE;
	$E += $r->R150;

	$FWE += $r->R180WE;
	$F += $r->R180;

	$GWE += $r->R210WE;
	$G += $r->R210;

	$HWE += $r->R240WE;
	$H += $r->R240;

	$IWE += $r->R270WE;
	$I += $r->R270;

	$JWE += $r->R300WE;
	$J += $r->R300;

	$KWE += $r->R330WE;
	$K += $r->R330;

	$LWE += $r->R360WE;
	$L += $r->R360;

	$NWE += $r->R390WE;
	$N += $r->R390;

	$MWE += $r->OVR390WE;
	$M += $r->OVR390;




}


	
	$t .= '<tr>';
	$t .= '<td><b>'. "Total" . '</b></td>';
	$t .= '<td align="center"><b>'. number_format($AWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($A,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($BWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($B,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($CWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($C,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($DWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($D,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($EWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($E,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($FWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($F,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($GWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($G,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($HWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($H,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($IWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($I,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($JWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($J,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($KWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($K,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($LWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($L,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($NWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($N,2) . '</b></td>';

	$t .= '<td align="center"><b>'. number_format($MWE,2) . '</b></td>';
	$t .= '<td><b>'. number_format($M,2) . '</b></td>';

	$t .= '</tr>';

//$this->pdf->Output("PDF.pdf", 'I');

$t .= '</table>';

if (!$make_excel){
    echo $t;
}else{
    header('Content-type: application/excel');
    $filename = 'Unredeemed Articles Maturity Analysis.xls';
    header('Content-Disposition: attachment; filename='.$filename);
    $data = $t;
    echo $data;
}


?>