<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 22);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	if ($barnch == ""){
		$barnch = "All Branch";
	}

	$this->pdf->MultiCell(0, 0, "Business Summary", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "From " .$fd." to ".$td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();	

	$this->pdf->SetFont('helvetica', 'B', 8);		

	$this->pdf->MultiCell(50, 1, "Branch", 					'B','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, "Pawning", 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, "Redeem", 					'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->MultiCell(25, 0, "Pawning Int", 			'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, "Redeem Int", 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, "Total Int",	 			'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->MultiCell(23, 0, "Gov.Tax",	 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(23, 0, "Postage",	 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->MultiCell(20, 0, "Weight", 					'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(18, 0, "Packets", 				'B','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, "Outs. Capital",		  	'B','R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	//$this->pdf->MultiCell(23, 0, "Pure Weight", 			'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	//$this->pdf->MultiCell(28, 0, "Accrued Interest",		'B','R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	

	$this->pdf->SetFont('', '', 8);
	$no = 1;

	$pa = $ra = $ia = $ts = $noi = $tp = $tw = $oc = $pi = $ri = $gt = $pst = $ti = 0;

	foreach($list as $r){	

		$h = 4 * (	max(1,$this->pdf->getNumLines($r->bc_name,50)) );

		$this->pdf->MultiCell(50, 1, $r->bc_name, 				'B','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, $h, d($r->pawn_amount), 		'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, $h, d($r->redeem_amount),		'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		
		$this->pdf->MultiCell(25, $h, d($r->p_int), 			'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, $h, d($r->r_int), 			'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, $h, d($r->int_amount), 		'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	
		$this->pdf->MultiCell(23, $h, d($r->g_tax),				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23, $h, d($r->postage),			'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);		

		$this->pdf->MultiCell(20, $h, d3($r->total_stock), 			'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(18, $h, $r->total_packets,		'B','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, $h, d($r->oc),					'B','R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		
		//$this->pdf->MultiCell(22, $h, $r->total_stock, 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		//$this->pdf->MultiCell(28, $h, d($r->accrued_interest),		'B','R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

		$no++;	

		$pa += $r->pawn_amount;
		$ra += $r->redeem_amount;

		$pi += $r->p_int;
		$ri += $r->r_int;
		$ti += $r->int_amount;

		$gt += $r->g_tax;
		$pst += $r->postage;

		$ts += $r->total_stock;		
		$tp += $r->total_packets;
		$oc += $r->oc;

		//$tw += $r->total_weight;
		//$ai += $r->accrued_interest;
	
	}

	$this->pdf->SetFont('', 'B', 8);

	$this->pdf->MultiCell(50, 1, "Total", 				'B','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, $h, d($pa), 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, $h, d($ra),				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell(25, $h, d($pi), 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, $h, d($ri), 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, $h, d($ti), 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell(23, $h, d($gt),				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(23, $h, d($pst),				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);		

	$this->pdf->MultiCell(20, $h, d3($ts), 				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(18, $h, $tp,					'B','C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, $h,d($oc),				'B','R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);






































	



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', ',');
	}


	$this->pdf->Output("PDF.pdf", 'I');

?>