<?php


	$make_excel = true;
	
	$t = '<table border="01" class="tbl_income_statment" cellspacing="1" cellpadding="1">';


	$total=(float)$op;
	$cr_toal=$cr;
	$dr_toal=$dr;
	$period_cr=(float)0;
	$period_dr=(float)0;
	$bc_code="";
	$sub_cr=$sub_dr=$x=0;


	$acc_no = intval(substr($_POST['acc_code'], 0,1));
	$first_round_passed = false;
	$ss = '';

	foreach($all_acc_det as $row){
		
		if ( $ss != trim($row->bc) ){
			
			$bc_name = $row->branch_name;		

			$ss = $row->bc;

			if ($first_round_passed){

				if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){
					
					$t .= '<tr>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td><b>Period Balance</b></td>';
					$t .= '<td><b>'.number_format($period_dr,2).'</b></td>';
					$t .= '<td><b>'.number_format($period_cr,2).'</b></td>';
					$t .= '<td></td>';
					$t .= '</tr>';
					

					//$period_dr = $period_cr = 0;

					$t .= '<tr>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td><b>Ending Balance</b></td>';
					$t .= '<td><b>'.number_format($dr_toal,2).'</b></td>';
					$t .= '<td><b>'.number_format($cr_toal,2).'</b></td>';
					$t .= '<td><b>'.number_format($total,2).'</b></td>';
					$t .= '</tr>';

					$total = 0;

				}

			}

			$first_round_passed = true;

			
			
			$op = 0;
			$period_cr = $period_dr = 0;

			foreach ($op_bal as $rrr) {
				if ( $row->bc == $rrr->bc ){
					
					if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){
						$op = $total = $rrr->op;
					}else{
						$op = $total = 0;
					}


					$dr = $rrr->dr;
					$cr = $rrr->cr;
					
				}
			}

			
			$t .= '<tr>';
			$t .= '<td colspan="7"><h3><b>'.$bc_name.'</b></h3></td>';
			$t .= '</tr>';

			$acc_arr['303003'] = "Daily Cash Account Report";
		    $acc_arr['10102'] = "Redeem Interest Account Report";
		    $acc_arr['30201'] = "Unredeem Articles Account Report";
		    $acc_arr['10101'] = "Pawning Interest Account Report";
		    $acc_arr['40215'] = "Customer Advance Account Report";




			if ( isset($_POST['acc_code']) ){
				$acc_desc = $_POST['acc_code']. " - ".$_POST['acc_code_des'];				
			}else{

				if (empty($acc_arr[$acc_code])){				
					$d = $account_det;
				}else{
					$d = $acc_arr[$acc_code];
				}				

				
				$acc_desc = $acc_code. " - ".$d;				
			}
			
			$t .= '<tr>';
			$t .= '<td colspan="7">'.$acc_desc.'</td>';
			$t .= '</tr>';

			$t .= '<tr>';
			$t .= "<td colspan='7'>Date From - ".$dfrom."     Date To - ".$dto."</td>";
			$t .= '</tr>';
			
			$t .= '<tr>';
			$t .= "<td colspan='7'>Time From - ".$tf."     Time To - ".$tt."</td>";
			$t .= '</tr>';

			if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){

				$t .= '<tr>';
				$t .= '<td>'. '' .'</td>';
				$t .= '<td>'. "" .'</td>';
				$t .= '<td>'. "" .'</td>';
				$t .= '<td><b>Begining Balance</b></td>';
				$t .= '<td>'. '' . '</td>';
				$t .= '<td>'. '' . '</td>';
				$t .= '<td><b>'. number_format($op,2).'</b></td>';
				$t .= '</tr>';

			}

			$t .= '<tr>';
			$t .= '<td><b>Date</b></td>';
			$t .= '<td><b>No</b></td>';
			$t .= '<td><b>Transaction</b></td>';
			$t .= '<td><b>Description</b></td>';
			$t .= '<td><b>DrAmount</b></td>';
			$t .= '<td><b>CrAmount</b></td>';
			$t .= '<td><b>Balance</b></td>';
			$t .= '</tr>';

		}


		if($row->dr_amount==0){
			$total = $total-(float)$row->cr_amount;
		}else if($row->cr_amount==0){
			$total = $total+(float)$row->dr_amount;
		}


		$period_dr +=$row->dr_amount;
		$period_cr+=$row->cr_amount;		


		$t .= '<tr>';
		$t .= '<td>'. $row->ddate.'</td>';
		$t .= '<td>'. $row->trans_no.'</td>';
		$t .= '<td>'. ucfirst(strtolower($row->det)).'</td>';
		$t .= '<td>'. $row->description.'</td>';
		$t .= '<td>'.number_format($row->dr_amount,2) .'</td>';
		$t .= '<td>'.number_format($row->cr_amount,2) .'</td>';
		$t .= '<td>'.number_format($total,2).'</td>';
		$t .= '</tr>';

	}


	if (  $acc_no == 3 || $acc_no == 4 || $acc_no == 5  ){

		$t .= '<tr>';
		$t .= '<td></td>';
		$t .= '<td></td>';
		$t .= '<td></td>';
		$t .= '<td><b>Period Balance</b></td>';
		$t .= '<td><b>'.number_format($period_dr,2).'</b></td>';
		$t .= '<td><b>'.number_format($period_cr,2).'</b></td>';
		$t .= '<td></td>';
		$t .= '</tr>';

		$t .= '<tr>';
		$t .= '<td></td>';
		$t .= '<td></td>';
		$t .= '<td></td>';
		$t .= '<td><b>Ending Balance</b></td>';
		$t .= '<td><b>'. number_format($dr_toal,2) .'</b></td>';
		$t .= '<td><b>'. number_format($cr_toal,2) .'</b></td>';
		$t .= '<td><b>'. number_format($total,2) .'</b></td>';
		$t .= '</tr>';

	}else{

		$t .= '<tr>';
		$t .= '<td>'. '' .'</td>';
		$t .= '<td>'. "" .'</td>';
		$t .= '<td>'. "" .'</td>';
		$t .= '<td><b>'. "Period Total" .'</b></td>';
		$t .= '<td><b>'. number_format($period_dr,2) .'</b></td>';
		$t .= '<td><b>'. number_format($period_cr,2) .'</b></td>';
		$t .= '<td>'. "" .'</td>';
		$t .= '</tr>';

	}



	$t .= '</table>';

	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'interest_income.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    exit;

	
	
		


?>