<?php



	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();

	$this->pdf->setX(0);
	$this->pdf->SetFont('helvetica', '', 28);	
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	
	$this->pdf->MultiCell(0, 0, "Customer Evaluation", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 15);	

	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(0, 0, "Amount Between " .$from_val . " and " . $to_val , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();



	if (isset($list)){

		$this->pdf->SetFont('helvetica', 'B', 15);
		$this->pdf->MultiCell(0, 0, "Pawning" , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->setX(0);
		$this->pdf->ln();

		$tot_transval=$tot_bal=$tot_redeem=(float)0;

		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', '', 8);	
		
		$this->pdf->MultiCell(50, 1, "Customer Info", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(17, 1, "Date", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "Branch", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(25, 1, "Bill No", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(18, 1, "T.Weight", '1','C', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(18, 1, "P.Weight", '1','C', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(60, 1, "Articles", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(25, 1, "Amount", '1','R', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(25, 1, "Pawn Int", '1','R', 0, 1, '', '', false, '', 0);	

		$bdr='B'; $y=0; $s = '';
		
		foreach($list as $r){
			
			$this->pdf->setX(15);
			$this->pdf->SetFont('helvetica', '', 8);		
		
			$heigh = 4 * (	max(1, $this->pdf->getNumLines($r->nicno." - ".$r->cusname,50) , $this->pdf->getNumLines($r->articles,45) , $this->pdf->getNumLines($r->bc_name,40) ) );

			if ($s != $r->nicno){
				$this->pdf->MultiCell(50, $heigh, $r->nicno." - ".$r->cusname, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
				$s = $r->nicno;
			}else{
				$this->pdf->MultiCell(50, $heigh, '', 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			}

			
			$this->pdf->MultiCell(17, $heigh, $r->ddate, 					$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(40, $heigh, $r->bc_name, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $heigh, $r->billno, 					$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(18, $heigh, $r->totalWeight, 				$border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(18, $heigh, $r->pureWeight, 				$border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(60, $heigh, $r->articles, 				$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $heigh, $r->requiredamount, 			$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $heigh, $r->fmintrest, 				$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
		
		}

		$this->pdf->ln();

	}




	if (isset($list2)){

		$this->pdf->SetFont('helvetica', 'B', 15);
		$this->pdf->MultiCell(0, 0, "Redeem" , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->setX(0);
		$this->pdf->ln();

		$tot_transval=$tot_bal=$tot_redeem=(float)0;

		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', '', 8);	
		
		$this->pdf->MultiCell(50, 1, "Customer Info", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(17, 1, "Date", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(40, 1, "Branch", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(25, 1, "Bill No", '1','L', 0, 0, '', '', false, '', 0);		
		//$this->pdf->MultiCell(60, 1, "Articles", '1','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(25, 1, "Amount", '1','R', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(25, 1, "Pawn Int", '1','R', 0, 1, '', '', false, '', 0);	

		$bdr='B'; $y=0; $s = '';
		
		foreach($list2 as $r){
			
			$this->pdf->setX(15);
			$this->pdf->SetFont('helvetica', '', 8);		
		
			$heigh = 4 * (	max(1, $this->pdf->getNumLines($r->nicno." - ".$r->cusname,50) , $this->pdf->getNumLines($r->articles,45) , $this->pdf->getNumLines($r->bc_name,40) ) );

			if ($s != $r->nicno){
				$this->pdf->MultiCell(50, $heigh, $r->nicno." - ".$r->cusname, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
				$s = $r->nicno;
			}else{
				$this->pdf->MultiCell(50, $heigh, '', 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			}

			
			$this->pdf->MultiCell(17, $heigh, $r->ddate, 					$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(40, $heigh, $r->bc_name, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $heigh, $r->billno, 					$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			//$this->pdf->MultiCell(60, $heigh, $r->articles, 				$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $heigh, $r->discount, 			$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $heigh, $r->amount, 				$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $heigh, $valign = 'M', $fitcell = false);
		
		}

	}

	

	$this->pdf->Output("PDF.pdf", 'I');



?>