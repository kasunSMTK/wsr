<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading("Periodical Expense Analysis");


$r  =  $this->excel->NextRowNum();

$this->excel->getActiveSheet()->setCellValue('A'.$r,"Date From  " .$fd. "  To ". $td);
$this->excel->SetBlank();
$r  =  $this->excel->NextRowNum();
 
$this->excel->getActiveSheet()->setCellValue('A'.$r,"Expense Account:   ".$acc);

$key    =   $this->excel->NextRowNum();

$c=$d=$j=$b=0; $billType_tot=(float)0; 
$sub_tot[]='';

$lettr = array("B","C","D","E", "F", "G","H", "I","J", "K","L", "M","N", "O","P", "Q","R", "S","T", "U","V", "W","X", "Y","Z", "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",);

//=======================================months====================================================
if($d_range=="months"){
    foreach ($months as $value) {

        $months=$value->mon."   ".$value->yr;

        $y_mon=$value->mon;
        $y_month[]= $y_mon;
        $y_monthes[]= $months; 
        $curr_month0=$y_month[0];
        $arr_con=count($y_month);  // count array elenemts   

    }

    $arr_size=sizeof($y_month);


    $r  =  $this->excel->NextRowNum();

    $this->excel->SetFont('A'.$r.":".'A'.$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");


    for($x =0; $x <=$arr_size; $x++) {
       $this->excel->SetFont($lettr[$x].$r.":".$lettr[$x].$r,"BC",12,"","");   
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,$y_monthes[$x]);
       
       $y++; $d=$x;
    }

    $this->excel->SetFont($lettr[$d].$r.":".$lettr[$d].$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");
    $this->excel->SetBlank();
    $key++;


foreach($list as $value){ 
  $curr_month=$value->mon_name;
  $bc1=$value->bc;

  if($bc1==$bc2){
      for($x=1; $x<$arr_size; $x++){
            if($y_month[$x] ==$curr_month){
               
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  (float)$value->expense);
              
                $branch_tot+=$value->expense; 
               
                $sub_tot[$x]+=(float)$value->expense;
                $curr_month=$value->mon_name;
                $bc2=$value->bc;
                $b=$b; //current row value
                $c=$x+1; //next month row 
            }else{
               // $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2)); 
                $curr_month=$value->mon_name;
                $a=$x+1; $b=$b;
            }
      }
    //$key=$b;
  }else{
    
    //==========================branch wise total===================================
    if($j==1){
        //vertical line- @the end of each branch end
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
            
            $branch_tot=0;
            $key++; 
    }
    $j=1;
    //==============================================================================

    $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
    $this->excel->SetBlank();
    

    for($x=0; $x<$arr_size; $x++){
       if($y_month[$x] ==$curr_month){
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->expense);
              
            $branch_tot+=$value->expense; 
             
            $sub_tot[$x]+=(float)$value->expense;
            $curr_month=$value->mon_name;
            $bc2=$value->bc;
            
            $c=$x+1; //next month row 
            $b=$key; //current row value


       }else{
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,0); 
            $curr_month=$value->mon_name;
            $a=$x+1; $b=$b;
       }
    }
    $key++;
  }

  
}
//==========================branch wise total===================================
    if($j==1){
        //vertical line- @ the end of last branch 
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
            //$sub_tot[$x]=0;
            $branch_tot=0;
            $key++; 
    }
    $j=1;
//==============================================================================

//==========================Grand Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
    
    for($x=0; $x<$arr_size; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot[$x]);
            $total+=$sub_tot[$x];
            $sub_tot[$x]=0;
            $a=$x+1; 
    }

    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total);
    $total=0;
    $key=$key+2;

}

//* - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - 
//=======================================years====================================================

if($d_range=="years"){

    foreach ($yrs as $value) {

        $years=$value->yr;

        $y_yrs=$value->yr;
        $y_years[]= $y_yrs; 
        
        $arr_con=count($y_years);  // count array elenemts   

    }

    $arr_size=sizeof($y_years);

    $r  =  $this->excel->NextRowNum();

    $this->excel->SetFont('A'.$r.":".'A'.$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");


    for($x =0; $x <=$arr_size; $x++) {
       $this->excel->SetFont($lettr[$x].$r.":".$lettr[$x].$r,"BC",12,"","");   
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,$y_years[$x]);
       
       $y++; $d=$x;
    }

    $this->excel->SetFont($lettr[$d].$r.":".$lettr[$d].$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");
    $this->excel->SetBlank();
    $key++;


    foreach($list as $value){
     $curr_yr=$value->yr;
     $bc1=$value->bc;

  if($bc1==$bc2){
      for($x=1; $x<$arr_size; $x++){
            if($y_years[$x] ==$curr_yr){
               
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  (float)$value->expense);
              
                $branch_tot+=$value->expense; 
               
                $sub_tot[$x]+=(float)$value->expense;
                $curr_yr=$value->yr;
                $bc2=$value->bc;
                $b=$b; //current row value
                $c=$x+1; //next month row 
            }else{
               // $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2)); 
                $curr_yr=$value->yr;
                $a=$x+1; $b=$b;
            }
      }
    //$key=$b;
  }else{
    
    //==========================branch wise total===================================
    if($j==1){
        //vertical line- @the end of each branch end
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
            
            $branch_tot=0;
            $key++; 
    }
    $j=1;
    //==============================================================================

    $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
    $this->excel->SetBlank();
    

    for($x=0; $x<$arr_size; $x++){
       if($y_years[$x] ==$curr_yr){
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->expense);
              
            $branch_tot+=$value->expense; 
             
            $sub_tot[$x]+=(float)$value->expense;
            $curr_yr=$value->yr;
            $bc2=$value->bc;
            
            $c=$x+1; //next month row 
            $b=$key; //current row value


       }else{
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,0); 
            $curr_yr=$value->yr;
            $a=$x+1; $b=$b;
       }
    }
    $key++;
  }

  
}
//==========================branch wise total===================================
    if($j==1){
        //vertical line- @ the end of last branch 
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
            //$sub_tot[$x]=0;
            $branch_tot=0;
            $key++; 
    }
    $j=1;
//==============================================================================

//==========================Grand Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
    
    for($x=0; $x<$arr_size; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"BC",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot[$x]);
            $total+=$sub_tot[$x];
            $sub_tot[$x]=0;
            $a=$x+1; 
    }

    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total);
    $total=0;
    $key=$key+2;



}
//=======================================end of years====================================================
//=======================================Quarter====================================================
if($d_range=="quarter"){

  $qtr="";
  foreach ($qtrs as $value) {

        $qtr=$value->Qurter;
    }

    $r  =  $this->excel->NextRowNum();

    $this->excel->SetFont('A'.$r.":".'A'.$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");


    for($x =0; $x <=$qtr; $x++) {
      $a=$x+1;
       $this->excel->SetFont($lettr[$x].$r.":".$lettr[$x].$r,"BC",12,"","");   
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,"Quarter ".$a);
       
       $y++; $d=$x;
    }

    $this->excel->SetFont($lettr[$d].$r.":".$lettr[$d].$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");
    $this->excel->SetBlank();
    $key++;

    foreach($list as $value){
     $curr_qtrs=$value->Qurter;
     $bc1=$value->bc;

      if($bc1==$bc2){

          for($x=0; $x<=$qtr; $x++){
                  $a=$x+1;
                  $q_num='Q'.$a;
                if($q_num ==$curr_qtrs){
                   
                    $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  (float)$value->expense);
                  
                    $branch_tot+=$value->expense; 
                   
                    $sub_tot[$x]+=(float)$value->expense;
                    $curr_qtrs=$value->Qurter;
                    $bc2=$value->bc;
                    $b=$b; //current row value
                    $c=$x+1; //next month row 
                }else{
                   // $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2)); 
                    $curr_yr=$value->yr;
                    $a=$x+1; $b=$b;
                }
          }

      }else{


        //==========================branch wise total===================================
        if($j==1){
            //vertical line- @the end of each branch end
                $key=$key-1;
                $a=$a+0;
                 $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
                $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
                
                $branch_tot=0;
                $key++; 
        }
        $j=1;
        //==============================================================================


        $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
        $this->excel->SetBlank();
        

        for($x=0; $x<=$qtr; $x++){
           $a=$x+1;
           $q_num='Q'.$a;
           //var_dump($q_num);
          // exit();
           if($q_num ==$curr_qtrs){
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->expense);
                  
                $branch_tot+=$value->expense; 
                 
                $sub_tot[$x]+=(float)$value->expense;
                 $curr_qtrs=$value->Qurter;
                $bc2=$value->bc;
                
                $c=$x+1; //next month row 
                $b=$key; //current row value


           }else{
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,0); 
                  $curr_qtrs=$value->Qurter;
                $a=$x+1; $b=$b;
           }
        }
        $key++;
  }

}// end of quarters foreach
    

  //==========================branch wise total===================================
      if($j==1){
          //vertical line- @the end of each branch end
              $key=$key-1;
              $a=$a+0;
               $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
              $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
              
              $branch_tot=0;
              $key++; 
      }
      $j=1;
  //==============================================================================

  //==========================Grand Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
    
    for($x=0; $x<=$qtr; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot[$x]);
            $total+=$sub_tot[$x];
            $sub_tot[$x]=0;
            $a=$x; 
    }

    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total);
    $total=0;
    $key=$key+2;




}

//=======================================End of Quarter====================================================

//=======================================Quarter====================================================
if($d_range=="half_year"){
  //assume that year start on 01 of janyary & End on 31 of December

  $num_hy=2; $j=0;

  $r  =  $this->excel->NextRowNum();

  $date = DateTime::createFromFormat("Y-m-d", $fd);
  $day=$date->format("Y");

  $this->excel->SetFont('A'.$r.":".'D'.$r,"BC",12,"","");   
  $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");
  $this->excel->getActiveSheet()->setCellValue('B'.$r,$day."-01-01 To  ".$day."-06-30 ");
  $this->excel->getActiveSheet()->setCellValue('C'.$r,$day."-07-01 To  ".$day."-12-31 ");  
  $this->excel->getActiveSheet()->setCellValue('D'.$r,"Total");
  $this->excel->SetBlank();
  $key++;


        foreach($list as $value){
           $curr_qtrs=$value->Qurter;
           $bc1=$value->bc;

            if($bc1==$bc2){

                for($x=0; $x<=$num_hy; $x++){
                        $a=$x+1;
                        $q_num='H'.$a;
                      if($q_num ==$curr_qtrs){
                         
                          $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  (float)$value->expense);
                        
                          $branch_tot+=$value->expense; 
                         
                          $sub_tot[$x]+=(float)$value->expense;
                         $curr_qtrs=$value->Qurter;
                          $bc2=$value->bc;
                          $b=$b; //current row value
                          $c=$x+1; //next month row 
                      }else{
                         // $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2)); 
                          $curr_yr=$value->yr;
                          $a=$x+1; $b=$b;
                      }
                }

            }else{


              //==========================branch wise total===================================
              if($j==1){
                  //vertical line- @the end of each branch end
                      $key=$key-1;
                      $a=$a+0;
                       $this->excel->SetFont('D'.$key.":".'D'.$key,"B",12,"","");
                      $this->excel->getActiveSheet()->setCellValue('D'.$key,$branch_tot);
                      
                      $branch_tot=0;
                      $key++; 
              }
              $j=1;
              //==============================================================================


              $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
              $this->excel->SetBlank();
              $bc2=$value->bc;

              for($x=0; $x<=$num_hy; $x++){
                 $a=$x+1;
                $q_num='H'.$a;
                 /* var_dump($curr_qtrs);
                exit();*/
                 if($q_num ==$curr_qtrs){
                       $this->excel->SetFont('A'.$key.":".'D'.$key,"",12,"","");
                      $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->expense);
                        
                      $branch_tot+=$value->expense; 
                       
                      $sub_tot[$x]+=(float)$value->expense;
                       $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                      
                      $c=$x+1; //next month row 
                      $b=$key; //current row value


                 }else{
                      $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,0); 
                        $curr_qtrs=$value->Qurter;
                      $a=$x+1; $b=$b;
                 }
              }
              $key++;
        }

      }// end of quarters foreach

      //==========================branch wise total===================================
              if($j==1){
                  //vertical line- @the end of each branch end
                      $key=$key-1;
                      $a=$a+0;
                       $this->excel->SetFont('D'.$key.":".'D'.$key,"B",12,"","");
                      $this->excel->getActiveSheet()->setCellValue('D'.$key,$branch_tot);
                      
                      $branch_tot=0;
                      $key++; 
              }
              $j=1;
      //==============================================================================


        //==========================Grand Total===========================================

    
        $this->excel->SetBlank();
        $this->excel->SetFont("A".$key.":".'D'.$key,"B",12,"","");
        $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
        
        for($x=0; $x<=$num_hy; $x++){
                $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"B",12,"","");
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot[$x]);
                $total+=$sub_tot[$x];
                $sub_tot[$x]=0;
                $a=$x; 
        }

        $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total);
        $total=0;
        $key=$key+2;
}


$this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));

?>