<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetAutoPageBreak(TRUE, 0);
$this->pdf->AddPage();

$this->pdf->SetFont('helvetica', '', 9);	
$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

$title_w = 30;
$border = 1;

$this->pdf->SetFont('helvetica', '', 20);	
$this->pdf->MultiCell(0,  0, "Generated Remind Letter List",	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->ln(5);
$this->pdf->SetFont('helvetica', '', 11);	

$this->pdf->MultiCell($title_w,  0, "No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, $sum->no,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, "From Date",	$border = $border, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, $sum->fd,		$border = $border, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, "To Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, $sum->td,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, "Bill Type",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, $sum->billType,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, "Letter Number",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, $sum->letter_no,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, "Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell($title_w,  0, $sum->date,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->ln(5);

$this->pdf->MultiCell(15,  0, 'No',	$border = 1, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(40,  0, 'Bill Number',	$border = 1, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(30,  0, 'Amount',	$border = 1, $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(75,  0, 'Customer Name',	$border = 1, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(100,  0, 'Customer Address',	$border = 1, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


$nno = 1;

foreach ($det as $row) {

	$h = 5 * (max(1,$this->pdf->getNumLines($r->cusname,65),$this->pdf->getNumLines($r->address,65)));

	$this->pdf->MultiCell(15,  $h, $nno,						$border = 1, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40,  $h, $row->billno,				$border = 1, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(30,  $h, number_format($row->requiredamount,2),		$border = 1, $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(75,  $h, $row->cusname,			$border = 1, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(100,  $h, $row->address,			$border = 1, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
	

	$nno++;
}






$this->pdf->Output("pawn_ticket.pdf", 'I');

	
function d($n,$d=2){
	return 	number_format($n,$d);				
}



?>