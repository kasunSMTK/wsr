<div class="page_contain">
	<div class="page_main_title"><span>Item</span></div><br>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_item" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add Item</b>
				</td>
				<td>
					<b>Exist Item</b>
				</td>
			</tr>			
			
			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Code</span> 
						<input class="input_text_regular" type="text" name="itemcode" id="itemcode" value="<?=$code_max?>" readonly="readonly" maxlength="4">
					</div>
				</td>							
				<td rowspan="8">

					<div class="exits_text_box_holder">
						<div class="text_box_title_holder" style="width:40px;border:0px solid red">Code</div> 
						<input class="input_text_regular" style="width:194px" type="text" id="Search_condition">
						<input type="button" value="Search"	name="btnSearch" id="btnSearch" class="btn_regular">					
					</div>
					
					<div class="list_div">
						<!-- <?=$load_list?> -->
					</div>

				</td>
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<div class="text_box_title_holder">Category</div> 
						<?=$category?>
					</div>
				</td>							
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Item Name</span> 
						<input class="input_text_regular" type="text" name="itemname" id="itemname" maxlength="50">
					</div>
				</td>							
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<div style="height: 30px">
							<label><div style="float: left"><input type="checkbox" name="bulk_item_code" id="bulk_item_code"></div>
							<div style="float: left;margin-top: -7px;margin-left:5px "><span class="text_box_title_holder">Is Bulk Item</span></div></label>
						</div>
					</div>
				</td>							
			</tr>

			<tr>
				<td>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>							
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>