<?php

	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setY(10);
	$this->pdf->setX(10);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Forfeited Report - Summery", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->SetFont('helvetica', '', 8);		

	
	$no = 1;
	$sb = $bcs = $bts = '';
	$bcpkt = $bcweight = $bc_amt = $baccint = $bcorcost = 0;
	$first_round_passed = false;

	foreach($list as $r){


		if ($bcs != $r->bc){

			if ($first_round_passed){

				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->MultiCell(20, 0, "Total" , 		$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20, 0, $bcpkt , 		$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20, 0, $bcweight , 	$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20, 0, $bc_amt , 		$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20, 0, $baccint , 	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20, 0, $bcorcost , 	$border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 8);
				$bcpkt = $bcweight = $bc_amt = $baccint = $bcorcost = 0;

				$this->pdf->ln(5);
				$this->pdf->MultiCell(0, 0, "Branch Manager........................................", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->ln(5);
				$this->pdf->MultiCell(0, 0, "Internal Auditor........................................", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->ln(5);
				$this->pdf->MultiCell(0, 0, "Head Office Gold Section Auditor........................................", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


			} $first_round_passed = true;

			
			$this->pdf->ln();
			$this->pdf->SetFont('helvetica', 'B', 12);
			$this->pdf->MultiCell(0, 0, $r->bc , 			$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
			$this->pdf->SetFont('helvetica', '', 8);
			$this->pdf->ln();
			$bcs = $r->bc;

			$this->pdf->SetFont('helvetica', 'B', 8);
			$this->pdf->MultiCell(20, 0, "Ledger" , 		$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, "No of PKT" , 		$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, "List Weight" , 	$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, "Amount" , 		$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, "Accrued Int " , 	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, "Other Cost " , 	$border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->SetFont('helvetica', '', 8);
		}



		$this->pdf->MultiCell(20, 0, $r->billtype , 	$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, $r->nop , 			$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d3($r->list_weight) , 	$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d($r->amount) , 		$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d($r->accrued_int) , 	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d($r->other_cost) , 	$border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		
		$bcpkt += $r->nop;
		$bcweight += $r->list_weight;
		$bc_amt += $r->amount;
		$baccint += $r->accrued_int;
		$bcorcost += $r->other_cost;

	}

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->MultiCell(20, 0, "Total" , 		$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, $bcpkt , 		$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d3($bcweight) , 	$border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d($bc_amt) , 		$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d($baccint) , 	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, d($bcorcost) , 	$border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);

		$this->pdf->ln(5);
		$this->pdf->MultiCell(0, 0, "Branch Manager........................................", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->ln(5);
		$this->pdf->MultiCell(0, 0, "Internal Auditor........................................", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->ln(5);
		$this->pdf->MultiCell(0, 0, "Head Office Gold Section Auditor........................................", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}

	$this->pdf->Output("PDF.pdf", 'I');

?>