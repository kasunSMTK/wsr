<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>
  
  $(function() {
    $( "#dDate" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:2050"

    });
  });

</script>

<style type="text/css">
    
.btn_regular_redeem{    
    background-color: #eaeaea;
    border:1px solid #ccc;
    color: #000000;
    font-family: "Roboto";
    font-size: 14px;    
    padding: 5px;
    width: 140px;border-radius: 4px;
    margin-left: 5px;
}

.btn_regular_redeem:hover{    
    background-color: red;    
    color: #ffffff;
}

.btl_pih td{
    padding: 10px;
}

.div_cus_info div h3{
    font-size: 14px;
    font-family: 'bitter';
}

.div_cus_info div span{
    font-size: 20px;
    font-family: 'bitter';
}

</style>


<div class="page_contain_new_pawn">
    
    <div class="page_main_title_new_pawn">
        <div style="width:240px;float:left"><span>Cancel a Part Payment</span></div>
        <div style="float:right; font-size:10px;">
            <span style="font-size:14px;">No</span>
            <input class="input_text_regular_new_pawn" id="no" name="no" style="width:50px; margin:0 5px; text-align:center; border:1px solid green;" value="<?=$mxno?>" type="text">
            <input class="input_text_regular_new_pawn" type="text" id="dDate" name="dDate" style="width:80px" readonly="readonly" value="<?=date('Y-m-d')?>">
        </div>
    </div>    

    <div class="selecters">
        
        <table border=0 cellpassing="0" cellspacing="0" align="center" width="600">
            <tr>      
                <td valign="top" style="padding-bottom: 10px; border-bottom: 1px solid #cccccc">
                    <br>                        
                    <span>Bill Number</span><br>
                    <input class="input_text_regular_new_pawn" type="text" id="billno" style="border:2px solid Green;">          
                    <input class="btn_regular" value="Get Bill Info" type="button" id="btnGetBillno" style="margin-top: 16px" />                        
                </td>
            </tr>

            <tr>
                <td style="padding: 10px 0px 10px 0px; border-bottom: 1px solid #cccccc">                        
                    <div class="div_cus_info" style="padding-left: 10px"></div>
                    <div class="div_pawn_info"></div>                        
                </td>
            </tr>

            <tr>
                <td style="padding: 10px 0px 10px 0px; border-bottom: 1px solid #cccccc">                                                
                    <input class="btn_regular_redeem" value="Make Cancel" type="button" id="btnCancelRedeem"  />
                    <input type="hidden" id="cus_serno">
                    <input type="hidden" id="bc">
                </td>
            </tr>

        </table>

    </div>

    

</div>
</body></html>