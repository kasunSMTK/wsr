<div class="page_contain">
	<div class="page_main_title"><span>Item Category</span></div><br>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_item_category" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add Item Category</b>
				</td>
				<td>
					<b>Exist Item Category</b>
				</td>
			</tr>			
			
			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Code</span> 
						<input class="input_text_regular" type="text" name="code" id="code" value="<?=$code_max?>" readonly="readonly"  maxlength="10">
					</div>
				</td>							
				<td rowspan="8">

					<div class="exits_text_box_holder">
						<div class="text_box_title_holder" style="width:40px;border:0px solid red">Code</div> 
						<input class="input_text_regular" style="width:194px" type="text" id="Search_condition">
						<input type="button" value="Search"	name="btnSearch" id="btnSearch" class="btn_regular">					
					</div>
					
					<div class="list_div">
						
					</div>

				</td>
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Category</span> 
						<input class="input_text_regular" type="text" name="des" id="des" maxlength="20">
					</div>
				</td>							
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<div style="height: 30px">
							<label><div style="float: left"><input type="checkbox" name="is_bulk" id="is_bulk"></div>
							<div style="float: left;margin-top: -7px;margin-left:5px "><span class="text_box_title_holder">Is Bulk Items</span></div></label>
						</div>						
						<div style="height: 30px; width:200px">
							<label><div style="float: left"><input type="checkbox" name="is_non_gold" id="is_non_gold"></div>
							<div style="float: left;margin-top: -7px;margin-left:5px "><span class="text_box_title_holder">Is Non Gold</span></div></label>
						</div>						
						<div style="height: 30px; width:400px" id= 'amt_base' name = 'amt_base'>
							<label><div style="float: left"><input type="checkbox" name="is_amount_base" id="is_amount_base"></div>
							<div style="float: left;margin-top: -7px;margin-left:5px "><span style='width:300px' class="text_box_title_holder">Amount Based interest Calculation</span></div></label>
						</div>
					</div>
				</td>							
			</tr>

			<tr>
				<td>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>							
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>