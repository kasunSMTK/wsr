<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<style type="text/css">	
	#bc{font-size: 14px;}
</style>

<script>

	$(function() {
		$( "#fd,#td" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2025",
			onSelect: function(date){
				
			}
		});
	});

</script>

<style type="text/css">
	
	.v_bc{
		border:2px solid Green;
		border-radius: 4px;
		padding: 2px;
	}

</style>

<div class="page_main_title_new_pawn" style="border:none;">
    <div style="float: left">
    	<span>Gold Audit</span><br><br>
	</div>

    <div style="float: right">
    	<input type="text" id="txtSearch" class="input_text_regular_new_pawn" style="border: 1px solid green"> 
    	<input type="button" value="Search" id="btnSearch" class="btn_regular">
    </div>

</div>


<div style="width:98%; margin:auto"></div>



<table border="0" width="98%" align="center" class="tbl_gold_audit">
<tr>

<td width="200">From <input value="<?=date('Y-m-d')?>" readonly="readonly" style="border:2px solid Green" class="input_text_regular_new_pawn" id="fd" name="fd"></td>

<td width="200">To <input value="<?=date('Y-m-d')?>" readonly="readonly" style="border:2px solid Green" class="input_text_regular_new_pawn" id="td" name="td"></td>

<td width="300">&nbsp;<?=$bc_list?></td>

<td width="90"><div style="height: 10px;"></div><label><input type="checkbox" class="chk_au" name="chk_au" value="B"> Audited </label></td>
<td width="105"><div style="height: 10px;"></div><label><input type="checkbox" class="chk_unau" name="chk_unau" value="C"> Unaudited </label></td>

<td><br><input type="button" value="Get Bills" class="btn_regular" id="btnGetBills" ></td>
</tr>
</table>

<div class="tbl_gold_bill_list"><!-- <?=$grid_data?> --></div>

</body>
</html>