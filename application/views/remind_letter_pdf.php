<?php

	$OS = 2;

	$this->pdf = new TCPDF("P", PDF_UNIT, 'LETTER', true, 'UTF-8', false);
	// remove default header/footer
	$this->pdf->setPrintHeader(false);
	$this->pdf->setPrintFooter(false);

	$this->pdf->SetAutoPageBreak(TRUE, 0);

	$this->pdf->SetFont('helvetica', '', 9);	
	



	foreach ($det as $r) {
		
		$this->pdf->AddPage();
		$this->pdf->ln(20);

		
		if ($r->letter_no == 1){			

			if ($OS == '1'){ // win 7

				$this->pdf->SetFont('helvetica', '', 10);	
				$this->pdf->MultiCell(75,  0, $r->cusname , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '20', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(75,  0, ucwords(strtolower(str_replace(",", "\n",  $r->address))) ,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '25', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 9);	

				$this->pdf->MultiCell(0,  0, ucwords(strtolower($r->bc_name . " - " . $r->bc_address)) , 	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '65', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '64', $y = '78', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '165', $y = '87', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '10', $y = '139', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '185', $y = '154', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '157', $y = '218', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '10', $y = '237', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

			}

			if ($OS == '2'){ // win xp

				$this->pdf->SetFont('helvetica', '', 11);	
				$this->pdf->MultiCell(75,  0, $r->cusname , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '20', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(75,  0, ucwords(strtolower(str_replace(",", "\n",  $r->address))) ,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '25', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 10);	

				$this->pdf->MultiCell(0,  0, ucwords(strtolower($r->bc_name . " - " . $r->bc_address)). " - " . $r->telno , 	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '70', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '55', $y = '83', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '171', $y = '93', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '0', $y = '148', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '185', $y = '163', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '169', $y = '232', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '18', $y = '253', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

			}

		}


		if ($r->letter_no == 2){

			if ($OS == 1){
				$this->pdf->SetFont('helvetica', '', 10);	
				$this->pdf->MultiCell(75,  0, $r->cusname , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '20', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(75,  0, ucwords(strtolower(str_replace(",", "\n",  $r->address))) ,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '25', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 9);	

				$this->pdf->MultiCell(0,  0, ucwords(strtolower($r->bc_name . " - " . $r->bc_address)) , 	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '65', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '64', $y = '78', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->first_let_gen_date , 										$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '84', $y = '84', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '140', $y = '92', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '100', $y = '104', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 				$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '10', $y = '137', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->first_let_gen_date ,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '180', $y = '140', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '10', $y = '159', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '64', $y = '170', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


				$this->pdf->MultiCell(0,  0, $r->billno , 				$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '140', $y = '222', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->first_let_gen_date ,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '120', $y = '231', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '82', $y = '236', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '45', $y = '251', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			}

			if ($OS == 2){

				$nh = 8;
				
				$this->pdf->SetFont('helvetica', '', 11);	
				$this->pdf->MultiCell(75,  0, $r->cusname , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '20', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(75,  0, ucwords(strtolower(str_replace(",", "\n",  $r->address))) ,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '25', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 10);	

				$this->pdf->MultiCell(0,  0, ucwords(strtolower($r->bc_name . " - " . $r->bc_address))." - ".$r->telno , 	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = 65+6, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '55', $y = 78+6, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->first_let_gen_date , 										$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '84', $y = 84+6, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '160', $y = 92+$nh, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '100', $y = 104+9, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 				$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '147', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->first_let_gen_date ,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '185', $y = '150', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '5', $y = '170', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '55', $y = '182', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


				$this->pdf->MultiCell(0,  0, $r->billno , 				$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '142', $y = '237', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->first_let_gen_date ,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '127', $y = '247', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '85', $y = '253', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate ,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '50', $y = '268', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

			}

		}



		if ($r->letter_no == 3){	

			if ($OS == 1){		

				$this->pdf->SetFont('helvetica', '', 10);	
				$this->pdf->MultiCell(75,  0, $r->cusname , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '20', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(75,  0, ucwords(strtolower(str_replace(",", "\n",  $r->address))) ,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '25', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 9);	
				
				$this->pdf->MultiCell(0,  0, ucwords(strtolower($r->bc_name . " - " . $r->bc_address)) , 	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '65', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				
				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '38', $y = '72', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '63', $y = '88', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '124', $y = '100', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '10', $y = '144', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '154', $y = '154', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '109', $y = '172', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '122', $y = '220', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '145', $y = '232', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '160', $y = '242', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

			}

			if ($OS == 2){

				$nh = 7;
				$nw = 7;

				$this->pdf->SetFont('helvetica', '', 11);	
				$this->pdf->MultiCell(75,  0, $r->cusname , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '20', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(75,  0, ucwords(strtolower(str_replace(",", "\n",  $r->address))) ,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '25', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 10);	
				
				$this->pdf->MultiCell(0,  0, ucwords(strtolower($r->bc_name . " - " . $r->bc_address)) , 	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = 65+$nh, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				
				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = 38+$nw, $y = 72+$nh, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = 63+$nw, $y = 88+$nh, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = 124+$nw, $y = 100+$nh, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = 10+$nw, $y = 144+$nh+4, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = 154+$nw, $y = 154+$nh+4, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'L', $fill = false, $ln = 1, $x = 109+$nw, $y = 172+$nh+4, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(0,  0, $r->billno , 													$border = '0', $align = 'L', $fill = false, $ln = 1, $x = 122+$nw, $y = 220+$nh+7, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = 145+$nw, $y = 232+$nh+11, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(0,  0, $r->finaldate , 												$border = '0', $align = 'C', $fill = false, $ln = 1, $x = 160+$nw, $y = 242+$nh+11, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

			}

		}
		      
      



		

	}


	$this->pdf->Output("remind_letters.pdf", 'I');

?>