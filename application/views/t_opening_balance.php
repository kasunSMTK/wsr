<?php if($this->user_permissions->is_view('t_opening_balance')){ ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/nprogress.css" />
<script type="text/javascript" src="<?=base_url()?>js/nprogress.js"></script>
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $( "#date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2025",
            onSelect: function(date){
                change_current_date(date);              
            }
        });
    });

    $('body').show();
    //$('.version').text(NProgress.version);
    NProgress.start();
    setTimeout(function() { NProgress.done(); $('.fade').removeClass('out'); }, 1000);   
</script>
<div class="page_main_title"><span>Opening Balance</span></div>
<!-- <h2 style="text-align: center;">Opening Balance</h2> -->
<div class="msgBox">
    <div class="msgInner">Saving Success</div>
</div>
<div class="dframe" id="mframe" style="width:980px;" >
    <form method="post" action="<?=base_url()?>index.php/main/save/t_opening_balance" id="form_">
        <table style="width: 100%" border="0">   
              <td width="100" style="width: 100px;">&nbsp;</td>
              <td>&nbsp;</td>

              <td style="display:none;">No</td>
              <td>&nbsp;</td>
              <!-- <td><input type="button" name="a_id" id="a_id" value="Load Opening balance" style="width:150px;"/></td> -->
              <td width="144" style="display:none;">  <input type="hidden" style='width: 150px;' class="input_active_num" name="id" id="id" value="1" />
            <input type="hidden" id="hid" name="hid" value="0" />            
            </tr>
            <tr>
                <td style="width: 100px;">Branch</td>
                <td width="638"><select name="bc" id="bc"><?=$bc_list?></select></td>
                <td width="51"><span style="width: 100px;">Date</span> <input type="hidden" name="time" id="time" value="0"> </td>
                <td>
                    <input type="text" class="input_txt" readonly="readonly" style='width: 150px; text-align:right;' name="date" id="date" value="<?=date('Y-m-d')?>" />                    
                </td>    
            </tr>
			    <tr>
                <td style="width: 100px;">&nbsp;</td>
                <td width="638">&nbsp;</td>
                <td width="51"><span style="width: 100px;">Ref no</span></td>
                <td><input type="text" class="input_txt" name="ref_no" id="ref_no" value="" style="width: 150px; text-align:right;" maxlength="10"/></td>
            </tr>
            <tr>
                <td><span style="width: 100px;">Note</span></td>
                <td colspan="2"><input type="text" class="input_txt" name="note" id="note" value="" style="width: 360px;" maxlength="80" /></td>
                <td ></td>
            </tr>
         </table>
         
           <div class="tgrid" style="width:100%;">
                    <table style="width: 875px;" id="tgrid">
                        <thead>
                            <tr>
                                <th class="tb_head_th" style="width: 180px;">Code</th>
                                <th class="tb_head_th" style="width: 270px;">Description</th>
                                <th class="tb_head_th" style="width: 120px;">Type</th>
                                <th class="tb_head_th" style="width: 180px;">Dr Amount</th>
                                <th class="tb_head_th" style="width: 180px;">Cr Amount</th>
                                
                            </tr>
                        </thead><tbody>
                            <?php
                                //if will change this counter value of 25. then have to change edit model save function.
								 //$y=$grid->value;
                                for($x=0; $x<150; $x++){
                                    echo "<tr>";
                                        echo "<td><input type='hidden' name='h_".$x."' id='h_".$x."' value='0' />
                                                <input type='text' class='g_input_txt fo' id='0_".$x."' name='0_".$x."'  style='width:180px;' readonly='readonly'/></td>";
                                        echo "<td style='background-color: #f9f9ec;'><input type='text' class='g_input_txt'  id='n_".$x."' name='n_".$x."' maxlength='150' readonly='readonly' style='width:265px;'/></td>";
                                        echo "<td style='background-color: #f9f9ec;'><input type='text' class='g_input_txt' readonly='readonly'  id='3_".$x."' name='3_".$x."' style='width:120px;'/>
                                              <input type='hidden' class='g_input_txt' readonly='readonly'  id='4_".$x."' name='4_".$x."' /></td>";
                                        echo "<td><input type='text' class='g_input_amo dr amount' id='1_".$x."' name='1_".$x."' style='width:180px;' maxlength='15'/></td>";
                                        echo "<td><input type='text' class='g_input_amo cr amount' id='2_".$x."' name='2_".$x."' style='width:180px;' maxlength='15'/></td>";
                                        
                                        
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
            </div>                
            <div style="text-align: left; padding-top: 7px;">

            			
                        <input type="button" class="btn_regular" id="btnExit" value="Exit" />
                        <input type="button" class="btn_regular" id="btnReset" value="Reset" />
                        <?php if($this->user_permissions->is_delete('t_opening_balance')){ ?>
                        <!-- <input type="button" id="btnDelete1" value="Delete" /> -->
                        <?php } ?>
                        <?php if($this->user_permissions->is_re_print('t_opening_balance')){ ?>
                        <input type="button" class="btn_regular" id="btnPrint" value="Print" />
                        <?php } ?>
                        <?php if($this->user_permissions->is_add('t_opening_balance')){ ?>
                        <input type="button" class="btn_regular"  id="btnSave" value='Save <F8>' />
                        <?php } ?>
                        <span style=" font-weight: bold; margin-left:165px;">    
			             Total
            		<input type="text" class="g_input_amo" name="tot_dr" id="tot_dr" value="0.00" style=" font-weight: bold; width:200px; text-align:right;"/>
            		<input type="text" class="g_input_amo" name="tot_cr" id="tot_cr" value="0.00" style="  font-weight: bold; width:180px; text-align:right;"/>
            		<input type="hidden" class="g_input_txt" name="grid_row" id="grid_row" value="150" style="width:100px;"/>
                        </span>
            		<br/>            	</td>
            </tr>
            <tr>
                <td colspan="5">
                   <span style=" font-weight: bold; margin-left:502px;">Balance<input type="text" class="g_input_amo" value="0.00" name="balance" id="balance" style=" font-weight: bold; width:180px; text-align:right;"/></span></td>
            </tr>
            <?php 
                    if($this->user_permissions->is_print('t_opening_balance')){ ?>
                    <input type="hidden" name='is_prnt' id='is_prnt' value="1" value="1">
                <?php } ?> 
            </div>
    </form>
</div>
<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">

    <input type="hidden" name='by' value='t_opening_balance' value="t_opening_balance" class="report">
    <input type="hidden" name='page' value='A4' value="A4" >
    <input type="hidden" name='orientation' value='P' value="P" >
    <input type="hidden" name='type' value='' value="" >
    <input type="hidden" name='header' value='false' value="false" >
    <input type="hidden" name='qno' value='' value="" id="qno" >
    <input type="hidden" name='rep_sup' value='' value="" id="rep_sup" >
    <input type="hidden" name='rep_ship_bc' value='' value="" id="rep_ship_bc" >
    <input type="hidden" name='inv_date' value='' value="" id="inv_date" >
    <input type="hidden" name='inv_nop' value='' value="" id="inv_nop" >
    <input type="hidden" name='po_nop' value='' value="" id="po_nop" >
    <input type="hidden" name='po_dt' value='' value="" id="po_dt" >
    <input type="hidden" name='credit_prd' value='' value="" id="credit_prd" >
    <input type="hidden" name='rep_deliver_date' value='' value="" id="rep_deliver_date" >
    <input type="hidden" name='jtype' value='' value="" id="jtype" >
    <input type="hidden" name='jtype_desc' value='' value="" id="jtype_desc" >

</form>
<?php } ?>