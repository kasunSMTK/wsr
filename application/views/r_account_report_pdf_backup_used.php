<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);


$this->pdf->setPrintFooter(true);



$this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->AddPage($orientation,$page);   // L or P amd page type A4 or A3
		foreach ($company as $r) {
			$c_name=$r->name; 
		}
		foreach ($branch as $ress) {
			$name=$ress->name;
			$address=$ress->address;
			$tp= $ress->telno;
			$fax=$ress->faxno;
			$email=$ress->email;    
		}

		foreach($branch as $ress){
			$this->pdf->headerSet3($name,$address,$tp,$fax,$email,$c_name);
		}
		foreach ($op as $key){
			$op = (float)$key->op;
			$cr = (float)$key->cr;
			$dr = (float)$key->dr;
		}


		$this->pdf->Ln();
		$this->pdf->Ln();
		$this->pdf->SetFont('helvetica', 'BU',12);

		$acc_arr['303003'] = "Daily Cash Account Report";
	    $acc_arr['10102'] = "Redeem Interest Account Report";
	    $acc_arr['30201'] = "Unredeem Articles Account Report";
	    $acc_arr['10101'] = "Pawning Interest Account Report";
	    $acc_arr['40215'] = "Customer Advance Account Report";

		if ( isset($_POST['bc_acc_det_list']) ){
			$acc_desc = $_POST['bc_acc_det_list']. " - ".$acc_arr[$_POST['bc_acc_det_list']];
		}else{

			if (empty($acc_arr[$acc_code])){				
				$d = $account_det;
			}else{
				$d = $acc_arr[$acc_code];
			}
			

			$acc_desc = $acc_code. " - ".$d;
		}

		$this->pdf->Cell(180, 1, $acc_desc ,0,false, 'L', 0, '', 0, false, 'M', 'M');
		$this->pdf->Ln();


		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', 'B', 8);

		$this->pdf->Cell(162, 1,"From - ".$dfrom."     To - ".$dto,0,false, 'L', 0, '', 0, false, 'M', 'M');
		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', 'B', 8);


		$this->pdf->Cell(18, 4, 'Date', '1', 0, 'C', 0);
		$this->pdf->Cell(10, 4, "No", '1', 0, 'C', 0);
		$this->pdf->Cell(25, 4, "Transaction", '1', 0, 'C', 0);
		$this->pdf->Cell(55, 4, "Description", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "Dr Amount", '1', 0, 'C', 0);
		$this->pdf->Cell(25, 4, "Cr Amount", '1', 0, 'C', 0);
		$this->pdf->Cell(24, 4, "Balance", '1', 0, 'C', 0);
		$this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));

		$this->pdf->Ln();
		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->Cell(18, 4, '', '1', 0, 'L', 0);
		$this->pdf->Cell(10, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(55, 4, "Begining Balance", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, number_format($dr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(25, 4,  number_format($cr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(24, 4,  number_format($op,2), '1', 0, 'R', 0);
		$this->pdf->Ln();
		$this->pdf->Ln();

		$total=(float)$op;
		$cr_toal=$cr;
		$dr_toal=$dr;
		$period_cr=(float)0;
		$period_dr=(float)0;
		$bc_code="";
		$sub_cr=$sub_dr=$x=0;

		foreach($all_acc_det as $row){
			if($row->dr_amount==0){
				$total=$total-(float)$row->cr_amount;
			}else if($row->cr_amount==0){
				$total=$total+(float)$row->dr_amount;
			}

			$cr_toal=$cr_toal+(float)$row->cr_amount;
			$dr_toal=$dr_toal+(float)$row->dr_amount;

			$period_cr+=(float)$row->cr_amount;
			$period_dr+=(float)$row->dr_amount;


			$this->pdf->SetFont('helvetica', '', 8);

			$aa=$this->pdf->getNumLines($row->description, 55); 
			$bb=$this->pdf->getNumLines($row->det, 33); 

			if($aa>$bb){

				$heigh=4*$aa;
			}else{
				$heigh=4*$bb;
			}


			if($bc_code == $row->bc){


				$this->pdf->SetFont('helvetica', '', 8);
				$this->pdf->MultiCell(18, $heigh, $row->ddate, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(10, $heigh, $row->trans_no, 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(25, $heigh, ucfirst(strtolower($row->det)), 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(55, $heigh, $row->description, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(25, $heigh,number_format($row->dr_amount,2) , 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(25, $heigh,number_format($row->cr_amount,2) , 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(24, $heigh,number_format($total,2), 1, 'R', 0, 1, '', '', true, 0, false, true, 0);
				
				$sub_dr=(float)$sub_dr+=$row->dr_amount;
				$sub_cr=(float)$sub_cr+=$row->cr_amount;

			}else{
				
				if($x!=0){
					$this->pdf->SetFont('helvetica', 'B', 8);
					$this->pdf->Cell(18, 4, '', '1', 0, 'L', 0);
					$this->pdf->Cell(10, 4, "", '1', 0, 'L', 0);
					$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
					$this->pdf->Cell(55, 4, "Total", '1', 0, 'L', 0);
					$this->pdf->Cell(25, 4, number_format($sub_dr,2), '1', 0, 'R', 0);
					$this->pdf->Cell(25, 4, number_format($sub_cr,2), '1', 0, 'R', 0);
					$this->pdf->Cell(24, 4, number_format($total,2), '1', 0, 'R', 0);
					$this->pdf->Ln();
					$this->pdf->Ln();
				}
				
				$x=1;
				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->Cell(18, 4, 'Branch', '0', 0, 'L', 0);
				$this->pdf->Cell(18, 4, $row->bc."-". $row->branch_name, '0', 0, 'L', 0);
				$this->pdf->Ln();

				$sub_dr=$sub_cr=0;
				$this->pdf->Cell(18, 4, 'Date', '1', 0, 'C', 0);
				$this->pdf->Cell(10, 4, "No", '1', 0, 'C', 0);
				$this->pdf->Cell(25, 4, "Transaction", '1', 0, 'C', 0);
				$this->pdf->Cell(55, 4, "Description", '1', 0, 'L', 0);
				$this->pdf->Cell(25, 4, "Dr Amount", '1', 0, 'C', 0);
				$this->pdf->Cell(25, 4, "Cr Amount", '1', 0, 'C', 0);
				$this->pdf->Cell(24, 4, "Balance", '1', 0, 'C', 0);
				$this->pdf->Ln();
				$this->pdf->SetFont('helvetica', '', 8);
				$this->pdf->MultiCell(18, $heigh, $row->ddate, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(10, $heigh, $row->trans_no, 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(25, $heigh,  ucfirst(strtolower($row->det)), 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(55, $heigh, $row->description, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(25, $heigh,number_format($row->dr_amount,2) , 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(25, $heigh,number_format($row->cr_amount,2) , 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
				$this->pdf->MultiCell(24, $heigh,number_format($total,2), 1, 'R', 0, 1, '', '', true, 0, false, true, 0);

				$sub_dr=(float)$sub_dr+=$row->dr_amount;
				$sub_cr=(float)$sub_cr+=$row->cr_amount;

			}

			$bc_code=$row->bc;
		}

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->Cell(18, 4, '', '1', 0, 'L', 0);
		$this->pdf->Cell(10, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(55, 4, "Total", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, number_format($sub_dr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(25, 4, number_format($sub_cr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(24, 4, number_format($total,2), '1', 0, 'R', 0);
		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->Cell(18, 4, '', '1', 0, 'L', 0);
		$this->pdf->Cell(10, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(55, 4, "Period Balance", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, number_format($period_dr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(25, 4, number_format($period_cr,2), '1', 0, 'R', 0);
		$this->pdf->Cell(24, 4, "", '1', 0, 'R', 0);
		$this->pdf->Ln();

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->Cell(18, 4, '', '1', 0, 'L', 0);
		$this->pdf->Cell(10, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, "", '1', 0, 'L', 0);
		$this->pdf->Cell(55, 4, "Ending Balance", '1', 0, 'L', 0);
		$this->pdf->Cell(25, 4, number_format($dr_toal,2), '1', 0, 'R', 0);
		$this->pdf->Cell(25, 4, number_format($cr_toal,2), '1', 0, 'R', 0);
		$this->pdf->Cell(24, 4, number_format($total,2), '1', 0, 'R', 0);


		$this->pdf->Output("Account Report".date('Y-m-d').".pdf", 'I');

		?>