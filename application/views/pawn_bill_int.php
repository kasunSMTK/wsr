<div class="page_contain">
	<div class="page_main_title"><span>Intrest change</span></div><br>

	<form method="post" action="<?=base_url()?>index.php/main/save/pawn_bill_int" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add Bill No</b>
				</td>
				
			</tr>			
			
			<tr>
				<td height="80">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Bill Number</span> 
						<input class="input_text_regular" type="text" name="billno" id="billno" value="" maxlength="20">
					</div>
				</td>
				<td height="80">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Branch</span> 
						<input class="input_text_regular" type="text" name="branch" id="branch" value="" maxlength="10" readonly="readonly">
					</div>
				</td>							
				
			</tr>

			<tr>
				<td height="80">
					<div class="txt_box_holder_intrate">
						<div class="text_box_title_holder">Bill Type</div> 
						<input class="input_text_regular" type="text" name="billtype" id="billtype" value="" maxlength="5" readonly="readonly">
					</div>
				</td>

				<td height="80">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Bill Amount</span> 
						<input class="input_text_regular" type="text" name="billamount" id="billamount" maxlength="50" readonly="readonly">
					</div>
				</td>							
			</tr>

			<tr>
				<td height="80">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Name</span> 
						<input class="input_text_regular" type="text" name="cuname" id="cuname" maxlength="50" readonly="readonly">
					</div>
				</td>

				<td height="80">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Loan No</span> 
						<input class="input_text_regular" type="text" name="loanno" id="loanno" maxlength="50" readonly="readonly">
					</div>
				</td>

											
			</tr>

			<tr>
				<td height="80">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Pawn Date</span> 
						<input class="input_text_regular" type="text" name="pdate" id="pdate" maxlength="50" readonly="readonly">
					</div>
				</td>

				<td height="80">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Final Date</span> 
						<input class="input_text_regular" type="text" name="fdate" id="fdate" maxlength="50" readonly="readonly">
					</div>
				</td>

											
			</tr>

			<tr>
				<td height="30">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Current int rate</span>
						 
						<input class="input_text_regular" type="text" name="currate" id="currate" maxlength="50" readonly="readonly">
					</div>
				</td>

				
				<td height="30">
					<div class="txt_box_holder_intrate">
						<div class="text_box_title_holder">New rate</div> 
						<SELECT id='newintrate' name='newintrate' class='input_ui_dropdown'>
							<option value="">Select New int rate</option>
							<?php
							for($i=0.5;$i<=30;$i=$i+0.5)
							{
								?>
								<option value="<?php echo $i;?>"><?php echo $i;?></option>
								<?php
							}
							?>
						</select>
					</div>
				</td>							
									
			</tr>

			<tr>
				<td height="70">
					<div class="txt_box_holder_intrate">
						<span class="text_box_title_holder">Note</span> 
						<input class="input_text_regularnote" type="text" name="note" id="note" 
						maxlength="100">
					</div>
				</td>

			</tr>
			

			<tr>
				<td height="100">
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">
					<input type="button" value="Search" 	name="btnSearch" 	 id="btnSearch" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>							
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>