<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
$this->pdf->setPrintHeader($header,$type,$duration);
$this->pdf->setPrintHeader(true,$type);
$this->pdf->setPrintFooter(true);

$this->pdf->SetFont('helvetica', 'B', 16);
$this->pdf->AddPage($orientation,$page); 



    //set header -----------------------------------------------------------------------------------------
foreach ($company as $r) {
  $c_name=$r->name; 
}
foreach ($branch as $ress) {
  $name=$ress->name;
  $address=$ress->address;
  $tp= $ress->tplno;
  $fax=$ress->faxno;
  $email=$ress->email;    
}
$this->pdf->headerSet3($name,$address, $tp, $fax, $email,$c_name);
$this->pdf->SetLineStyle(array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

$this->pdf->setY(25);
$this->pdf->SetFont('helvetica', 'BUI',12);
$this->pdf->Cell(180, 1,"GENERAL RECEIPT SUMMERY   ",0,false, 'L', 0, '', 0, false, 'M', 'M');
$this->pdf->Ln();




    //----------------------------------------------------------------------------------------------------
$this->pdf->SetX(20);
$this->pdf->setY(34);
$this->pdf->SetFont('helvetica', '',10);
$this->pdf->Cell(0, 5, 'Date   From '. $dfrom.' To '.$dto ,0,false, 'L', 0, '', 0, false, 'M', 'M');
$this->pdf->Ln();
if($branch1 !=""){
 
  $this->pdf->Cell(30, 6,'Branch', '0', 0, 'L', 0);
  $this->pdf->Cell(5, 6,':', '0', 0, 'L', 0);
  $this->pdf->Cell(50, 6,$branch1." - ".$branch_name, '0', 0, 'L', 0);
  $this->pdf->Ln();
}

$this->pdf->SetFont('helvetica','B',8);
$this->pdf->Cell(15, 6,"No", '1', 0, 'C', 0);
$this->pdf->Cell(20, 6,"Date", '1', 0, 'C', 0);
$this->pdf->Cell(15, 6,"Type", '1', 0, 'C', 0);
$this->pdf->Cell(90, 6,"Description", '1', 0, 'C', 0);
$this->pdf->Cell(30, 6,"Acc No", '1', 0, 'C', 0);
$this->pdf->Cell(60, 6,"Acc Name", '1', 0, 'C', 0); 
$this->pdf->Cell(25, 6,"Amount", '1', 0, 'C', 0);
$this->pdf->Ln();

foreach ($sum as $value) 
{

 $this->pdf->SetX(15);
 $this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));
 $this->pdf->SetFont('helvetica','',9);


 $aa=$this->pdf->getNumLines($value->description,60); 
 $bb=$this->pdf->getNumLines($value->note, 90); 

 if($aa<$bb)
  $heigh=6*$bb;
else 
  $heigh=6*$aa;

        // Deatils loop---------------------------------
$this->pdf->MultiCell(15, $heigh, $value->nno, 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(20, $heigh, $value->ddate, 1, 'R', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(15, $heigh, $value->type, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(90, $heigh, $value->note, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(30, $heigh, $value->paid_acc, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(60, $heigh, $value->description, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);

if($value->type=="cash")
  $this->pdf->MultiCell(25, $heigh, $value->cash_amount, 1, 'R', 0, 1, '', '', true, 0, false, true, 0);
else

  $this->pdf->MultiCell(25, $heigh, $value->cheque_amount, 1, 'R', 0, 1, '', '', true, 0, false, true, 0);

$xx++;

$tot+=(float)$value->cash_amount+$value->cheque_amount;

    }//end foreach

 
      $this->pdf->SetFont('helvetica','B',10);             
      $this->pdf->SetX(15);
      $this->pdf->Cell(15, 6, "", '0', 0, 'L', 0);
      $this->pdf->Cell(20, 6, "", '0', 0, 'R', 0);
      $this->pdf->Cell(15, 6, "", '0', 0, 'R', 0);
      $this->pdf->Cell(90, 6, "", '0', 0, 'R', 0);
      $this->pdf->Cell(30, 6, "", '0', 0, 'R', 0);
      $this->pdf->Cell(60, 6, "Total ", '0', 0, 'R', 0);
      $this->pdf->Cell(25, 6, number_format($tot,2), '1', 0, 'R', 0);
      
      $this->pdf->Ln();
   



    $this->pdf->Output("general_recipt_summery".date('Y-m-d').".pdf", 'I');

    ?>



