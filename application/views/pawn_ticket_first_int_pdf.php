<?php

	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 8);	
	$this->pdf->setY(0);
	$this->pdf->setX(0);

	if ($fullmonth_int == 1){
		$rpd = "Interest for first month";
	}else{
		$rpd = "Interest for first 7 days";
	}

	$this->pdf->MultiCell(0, 0, "Receipt - ".$rpd, 	$border = '1', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);

	$this->pdf->MultiCell(40, 0, "Customer Name", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->cusname, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell(40, 0, "Address", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->address, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell(40, 0, "NIC", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->nicno, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Contact Number", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->mobile, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		

	/*$this->pdf->MultiCell(40, 0, "cus_serno", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->cus_serno, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	*/

	$this->pdf->MultiCell(40, 0, "Branch", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->bc, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Pawn Date", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->ddate, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Final Date", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->finaldate, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Bill type", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->billtype, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Bill Number", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->billno, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Loan Number", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->loanno, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Advance Amount", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->requiredamount, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Interest Rate", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->fmintrate, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	
	$this->pdf->MultiCell(40, 0, "Period", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->period, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(40, 0, "Issued Date Time", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $R->action_date, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	
	$this->pdf->ln();

	$this->pdf->setX(0);
	$this->pdf->MultiCell(30, 0, "Date" , 		$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(25, 0, "Transe Code" , 		$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);	
	$this->pdf->MultiCell(18, 0, "amount" , 		$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(20, 0, "discount" , 			$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	
		
	$this->pdf->setX(0);
	$this->pdf->MultiCell(30, 0, $RR->ddate, 		$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(25, 0, $RR->transecode, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(18, 0, $RR->amount, 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);
	$this->pdf->MultiCell(20, 0, $RR->discount, 	$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false);

		
		
	

	$this->pdf->Output("partpayment_ticket.pdf", 'I');

?>