<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>
last_max_no = "<?=$max_no?>";

$(function() {
    $(".date_ch_allow").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: "1930:2050",
        onSelect: function(date) {
            change_current_date(date);
        }
    });
});

$(function() {
    $(".date_ch_allow_n").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        minDate: '<?=$backdate_upto?>',
        maxDate: 0,
        yearRange: "1930:2025",
        onSelect: function(date) {
            change_current_date(date);
        }
    });
});

function date_change_response_call() {
    $("#btnReset").click();
}
</script>


<style type="text/css">
#ffTb tbody {
    font-family: "Roboto";
    font-size: 13px;
}

.bd {
    border-bottom: 1px dotted #ccc !important;
    line-height: 10px !important;
}

.SlalMn {
    background-color: #eee;
}

#ffTb {
    padding-bottom: 10px;
    overflow: hidden;
}

#ffTb thead {
    line-height: 10px !important;
    border: 1px solid #ccc;
    background-color: #ffffff;
    color: #000000;
}

#ffTb tbody tr:hover {
    background-color: #eaeaea;
}

#GrDt tr:hover {
    color: #000000;
}

#ffbtn {
    bottom: 0px;
    width: 100%;
    border-top: 1px solid #eaeaea;
    padding: 10px;
    background-color: #FCFCFC;
    position: fixed;
}

input[type='checkbox'] {
    padding: 0px;
    margin: 0px;
}

.fixed {
    position: fixed;
    top: 0;
    font-family: "bitter";
    font-size: 14px;
    background-color: #ffffff;
    color: #000000;
    border-bottom: 1px solid #000000;
    margin-left: -1px;
}

.input_text_regular_new_pawn_solid {
    @extend .input_text_regular_new_pawn;
    border: 1px solid green;
    border-radius: 4px;
    font-family: "Roboto";
    width: 180px;
    padding: 5px;
    font-size: 12px;
}

.text_right {

    text-align: right;
}

.bcred {

    background-color: red;
    color: #ffffff;
    border-radius: 0px;
    -moz-animation-name: blinker;
    -moz-animation-duration: .5s;
    -moz-animation-timing-function: linear;
    -moz-animation-iteration-count: infinite;
}

@-moz-keyframes blinker {
    0% {
        opacity: 1.0;
    }

    50% {
        opacity: 0.0;
    }

}

</style>



<div class="multiple_billtype_selection"></div>
<div id="pdfdiv_content">
    <iframe name="     " id="receiver" width="0" height="0"></iframe>
</div>

<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:150px;float:left"><span>Advance Payment</span></div>
        <div style="float:left;float:right"><input type="text"
                class="input_text_regular_new_pawn <?=$date_change_allow?>" style="width:80px" id="ddate"
                value="<?=$current_date?>"></div>
    </div>

    <form method="post" action="<?=base_url()?>index.php/main/save/t_sales_advance_pay" id="form_">

        <table border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" align="center">
            <tr>
                <td valign="top" align="left" style="width: 950px">

                    <div class="pawn_msg"></div>
                    <div class="billType_det_holder">
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="text_box_holder_new_pawn" style="width:873px; border:none">
                                                    &nbsp;
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            <td align="right">No&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="time"
                                                    id="time"> </td>
                                </td>
                                <td>
                                    <div class="text_box_holder_new_pawn" style="width:224px;border:none;float:left">
                                        <input class="input_text_regular_new_pawn_solid" type="text" name="nno" id="nno"
                                            value="<?=$max_no?>" style='width:100px; border:1px solid green'>
                                        <input type="hidden" name="hid" id="hid" value="0">
                                        <input type="hidden" id="hid_type">
                                        <input type="hidden" id="hid_rows" name='hid_rows'>
                                        <input type="hidden" id="hid_bc" name="hid_bc">
                                        <input type="hidden" id="no" name="no" style="width:65px" value="">

                                    </div>

                                </td>
                            </tr>
                        </table>


                        <table>
                            <tr>
                                <td>
                                    <div class="text_box_holder_new_pawn" style="width:600px; border:none">
                                        <span class="text_box_title_holder_new_pawn">Customer</span>
                                        <input class="input_text_regular_new_pawn_solid" type="text" name="customer_id"
                                            id="customer_id" style="width:342px" value="">
                                        <input type="hidden" name="cus_serno" id="cus_serno" value="">
                                    </div>
                                </td>
                                <td>
                                    <div class="text_box_holder_new_pawn" style="width:273px; border:none">
                                        <!-- <span class="text_box_title_holder_new_pawn">Is Forfeit</span>
                                        <input type="checkbox" name="is_redeem" id="is_redeem" value="R"> -->

                                    </div>
                                </td>
                                <td>
                                <td align="right">Date <input type="hidden" name="time" id="time"> </td>
                </td>
                <td>
                    <div class="text_box_holder_new_pawn" style="width:224px;border:none;float:left">
                        <input class="input_text_regular_new_pawn_solid  <?=$date_change_allow?>" type="text" id="date"
                            name="date" style="width:100px" readonly="readonly" value="<?=$current_date?>">
                    </div>
                </td>
            </tr>
        </table>

        </td>
        </tr>
        </table>
</div>

<div class="loan_dtl_contain" style='width:100%'>
    <table width="90%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td width="145px">
                <div class="text_box_holder_new_pawn" style="width:100%;border:1px solid #ccc">
                    <span class="text_box_title_holder_new_pawn">Category</span>
                    <input class="input_text_regular_new_pawn" type="text" id="cat_code_desc" style="width:145px"
                        value="">
                    <input type="hidden" id="cat_code" name="cat_code" value="">
                    <input type="hidden" id="is_bulk_cat" value="0">
                </div>
            </td>
            <td width="145px">
                <div class="text_box_holder_new_pawn" style="width:100%;border:1px solid #ccc">
                    <span class="text_box_title_holder_new_pawn">Item</span>
                    <input class="input_text_regular_new_pawn" type="text" id="item_tag" name="item_tag"
                        style="width:145px">
                    <input type='hidden' id='tag_no' name='tag_no'>
                </div>
            </td>

            <td width="250">
                <div class="text_box_holder_new_pawn" style="width:100%;border:1px solid #ccc;border-left:none">
                    <span class="text_box_title_holder_new_pawn">Description</span>
                    <input class="input_text_regular_new_pawn" type="text" id="description" name="description"
                        style="width:250px">
                </div>
            </td>
            <td width="250">
                <div class="text_box_holder_new_pawn" style="width:100%;border:1px solid #ccc;border-left:none">
                    <span class="text_box_title_holder_new_pawn">Bill No</span>
                    <input class="input_text_regular_new_pawn" type="text" id="billno" name="billno" style="width:100px"
                        readonly="readonly">
                </div>
            </td>
            <td width="20">
                <div class="text_box_holder_new_pawn" style="width:100%;border:1px solid #ccc;border-left:none">
                    <span class="text_box_title_holder_new_pawn">Balance</span>
                    <input class="input_text_regular_new_pawn" type="text" id="requiredamount" name="requiredamount"
                        style="width:98%" readonly="readonly">
                </div>
            </td>
            <td width="100">
                <div class="text_box_holder_new_pawn text_right"
                    style="width:123px;border:1px solid #ccc;border-left:none">
                    <span class="text_box_title_holder_new_pawn">Amount</span>
                    <input class="input_text_regular_new_pawn text_right" type="text" id="amount" name="amount"
                        style="width:98%" readonly="readonly">
                </div>
            </td>

            <td width="75">
                <div class="text_box_holder_new_pawn" style="width:75px;border:1px solid #ccc;border-left:none;">
                    <span class="text_box_title_holder_new_pawn">Action</span>
                    <input type="button" value="Add" class="btn_regular" style="width:50px" id="btnAddtoList"
                        name="btnAddtoList">
                </div>

            </td>
        </tr>
    </table>
</div>

<div class="item_list_holder" style='width:100%'>
    <table width='90%' 0cellpadding='0' cellspacing='0' border='0' id="itemlist" name="itemlist">

        <!-- tabel footer input  -->

    </table>
    <table width='93%' 0cellpadding='0' cellspacing='0' border='0' style="margin-top: 30px;">
        <tr>
            <td width="200"></td>
            <td width="500"></td>
            <td width="100" style="text-align: left;">Item Total</td>
            <td width="100">
                <input class="input_text_regular_new_pawn_solid" type="text" id="total_amount" name="total_amount"
                    readonly="readonly" style="text-align: right;"></td>
        </tr>
        <tr>
            <td width="200"></td>
            <td width="500"></td>
            <td width="100" style="text-align: left;">Discount</td>
            <td width="100"><input class="input_text_regular_new_pawn_solid" type="text" id="discount_amount"
                    name="discount_amount" style="text-align: right;"></td>
        </tr>
        <tr>
            <td width="200"></td>
            <td width="500"></td>
            <td width="100" style="text-align: left;">Net Amount</td>
            <td width="100"> <input class="input_text_regular_new_pawn_solid" type="text" id="net_amount"
                    name="net_amount" readonly="readonly" style="text-align: right;"></td>
        </tr>
        <tr>
            <td width="200"></td>
            <td width="500"></td>
            <td width="100"></td>
            <td width="100"></td>
        </tr>
        <tr>
            <td width="200"></td>
            <td width="500"></td>
            <td width="100" style="text-align: left;">Payment Amount</td>
            <td width="100"> <input class="input_text_regular_new_pawn_solid" step="any" type="number"
                    id="payment_amount" name="payment_amount" style="text-align: right;"></td>
        </tr>
        <tr>
            <td width="200"></td>
            <td width="500"></td>
            <td width="100" style="text-align: left;">Balance Amount</td>
            <td width="100"> <input class="input_text_regular_new_pawn_solid" type="text" id="balance_amount"
                    name="balance_amount" readonly="readonly" style="text-align: right;"></td>
        </tr>
    </table>
    <!-- item list -->
</div>

<input type="button" value="Generate Item Tags" class="btn_regular" id="btngentag" style='width:100%;display:none'>
<!-- <div class="pp_pawn_det_holder" id = 'item_data'></div> -->
<br><br>

<table border=0 width='1000px'>
    <tr>
        <td width=150px><label><span class="text_box_title_holder_new_pawn" style="width: 150px;">Store</span></label>
        </td>
        <td><?=$store?>
        </td>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    <tr>
    <tr>
        <td><label><span class="text_box_title_holder_new_pawn" style="width: 150px;">Employee</span></label></td>
        <td><?=$employee?>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    <tr>
    <tr>
        <td><label><span class="text_box_title_holder_new_pawn" style="width: 150px">Note</span></label></td>
        <td><textarea id="note" name="note" style="width:400px;"></textarea></td>
    </tr>
    </tr>
</table>

<br><br>
<div class="">
    <input type="button" value="Save" class="btn_regular" id="btnSave">
    <input type="button" value="Re-Print" class="btn_regular_disable" id="btnPrint">
    <!-- <div style="float:right; padding-right:20px"> -->
    <input type="button" value="Reset" class="btn_regular_disable" id="btnReset">
    <input type="button" value="Cancel" class="btn_regular_disable" id="btnCancel">
    <!-- </div>-->
    <input type="hidden" name="hid" id="hid" value="0">
    <input type="hidden" name="rowCount" id="rowCount" value="0">

    <input type="hidden" name="no_of_int_cal_days" id="no_of_int_cal_days" value="0">


</div>
</td>
<!--
				<td valign="top" style="border-left:1px solid #ccc;background-color:#fcfcfc;width: 250px">

				</td>
				-->
</tr>
</table>

</form>

<!-- <form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="receiver"> -->
<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
    <div style="text-align:left; padding-top: 7px;">
        <input type='hidden' name='by' id='by' value='r_sales_advance' />
        <input type="hidden" name="r_no" id="r_no" />
        <input type="hidden" name="is_reprint" id="is_reprint" value="0" />

    </div>
</form>

</div>
</body>

</html>
