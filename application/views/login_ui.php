<?php defined('BASEPATH') OR exit('No direct script access allowed');


if (!$reg_required){  ?>

<script src="<?=base_url()?>js/jquery.cookie.js"></script>

    
    <div class="middleBox">

        <div class="middle_logo"></div>

        <div class="input_holder">

            <span class="loginSpan">Username</span><br>
            <input type="text" class="loginTxt" id="txtUserName"> <br><br>

            <span class="loginSpan">Password</span><br>
            <input type="password" class="loginTxt" id="txtPassword" autocomplete="off"> <br><br>

            <span class="loginSpan">Branch</span><br>
            <input type="text" class="loginTxt" id="txtBranch" autocomplete="off"> <br><br>

            <button class="btnLogin" id="btnLogin"> Login </button><br><br>

            <div id="div_login_msg"></div>

        </div>        

    </div>


    <a target="self" href="http://www.softmastergroup.com"><div class="solution_by"><img src="images/soft-master.png" width="100" alt="www.softmastergroup.com"></div></a>

<?php }else{ ?>

    
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css" />    
    <script type="text/javascript" src="<?=base_url(); ?>js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/auth.js"></script>
    <div class="auth_holder">

    <table border="0" class="tbl_com_reg">

        <tr>
            <td colspan="2"><span class="auth_title">Computer Register</span><br><br>
        </tr>

        <tr>
            <td width="600" valign="top" style="padding-right: 50px">
                <div style="padding: 10px; background-color: red;color: #ffffff;font-size: 15px;font-family: 'roboto'">This computer required register with head office. Please follow the below instructions.</div>            

                <ol type="1" style="font-size: 15px; font-family: 'roboto';">

                    <li>Call head office</li>
                    <li>Tell them 'Your Code' section value from your right side </li>
                    <li>Them enter the code given by head office in 'Register Code' section </li>
                    <li>Then hit the 'Register Now' button</li>
                    <li>If your registration is success, you will redirected to the login screen</li>

                </ol>

            </td>
            <td width="300" style="border-left:1px solid #cccccc;">
                <div  style="text-align: center;">
                    
                    <span class="loginSpan">Your Code</span><br>
                    <input type="text" class="auth_txt" id="auth_your_code" readonly="readonly"><br><br>

                    <span class="loginSpan">Register Code</span><br>
                    <input type="text" class="auth_txt" id="reg_code"> <br><br>

                    <button class="btnLogin" id="btnRegCom"> Register Now </button><br><br>            

                </div>        
            </td>
        </tr>
    </table>

    </div>


<?php } ?>


</body>
</html>