<?php
$this->pdf = new TCPDF("P", PDF_UNIT, 'SEW', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(true);
$this->pdf->SetPrintFooter(true);
$this->pdf->SetAutoPageBreak(true, 0);
$this->pdf->AddPage();

$this->pdf->SetFont('helvetica', '', 8);
$this->pdf->MultiCell(0, 10, "$RRR->name <br> $RRR->address <br> Tel : $RRR->telno, Fax : $RRR->faxno, Email : $RRR->email ", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 10, $valign = 'M', $fitcell = false);

$this->pdf->SetFont('helvetica', 'B', 10);
$this->pdf->setY(25);

if ($is_reprint == 1) {
    $this->pdf->MultiCell(0, 0, "Advance Payment (Re Print)", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
} else {
    $this->pdf->MultiCell(0, 0, "Advance Payment", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
}
$this->pdf->SetFont('helvetica', '', 8);
$this->pdf->ln(5);
$this->pdf->MultiCell(15, 0, "No :", $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(15, 0, " " . $nno, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->MultiCell(15, 0, "Date :", $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(20, 0, " " . $data1->ddate, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->MultiCell(15, 0, "Customer:", $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(60, 0, " " . $data1->cusname, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(15, 0, " ", $border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(50, 0, " " . $data1->nicno, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

// if ($is_reprint == 0){
//     $header_custom_data['title'] = $str_tfr."TAG";
// }else{
//     $header_custom_data['title'] = $str_tfr."TAG (Re-Print)";
// }
$this->pdf->SetFont('helvetica', 'B', 8);
$this->pdf->ln(5);
$title_w = 15;

$this->pdf->MultiCell(15, 0, 'Loan No', $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(25, 0, 'Category', $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(30, 0, 'Item', $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(20, 0, 'Bill No', $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(15, 0, 'Tag No', $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(15, 0, 'Amount', $border = 'B', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->ln(2);
$this->pdf->SetFont('helvetica', '', 8);
foreach ($data as $r) {
    $this->pdf->MultiCell(15, 0, $r->loanno, $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    $this->pdf->MultiCell(25, 0, $r->cat_code . ' - ' . $r->des, $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    $this->pdf->MultiCell(30, 0, $r->item . ' - ' . $r->itemname, $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    $this->pdf->MultiCell(20, 0, $r->billno, $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    $this->pdf->MultiCell(15, 0, $r->tag_no, $border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
    $this->pdf->MultiCell(15, 0, $r->amount, $border = 'B', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
}

$this->pdf->ln(5);
$this->pdf->SetFont('helvetica', 'B', 8);
$this->pdf->MultiCell(70, 0, " ", $border = '', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(30, 0, "Total :", $border = '', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(20, 0, " " . $data1->net_amount, $border = '', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->ln(2);
$this->pdf->SetFont('helvetica', 'B', 10);
$this->pdf->MultiCell(70, 0, " ", $border = '', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(30, 0, "Payment :", $border = '', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(20, 0, " " . $data1->payment_amount, $border = '', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->ln(25);
$this->pdf->SetFont('helvetica', '', 8);
$this->pdf->MultiCell(75, 0, "...........................................", $border = '0', $align = 'C', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(75, 0, "...........................................", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->MultiCell(75, 0, "Prepared By", $border = '0', $align = 'C', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(75, 0, "Approved By", $border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->Output("tag.pdf", 'I');

function d($n, $d = 2)
{
    return number_format($n, $d);
}
