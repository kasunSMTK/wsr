<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

	last_max_no = "<?=$max_no ?>"; 

	$(function() {
		$( ".date_ch_allow" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,			
			yearRange: "1930:2050",
			onSelect: function(date){
				change_current_date(date);				
			}	
		});
	});

	$(function() {
		$( ".date_ch_allow_n" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			minDate:'<?=$backdate_upto?>',
			maxDate:0,
			yearRange: "1930:2025",
			onSelect: function(date){
				change_current_date(date);				
			}
		});
	});

	function date_change_response_call(){
		$("#btnReset").click();
	}

</script>


<div class="multiple_billtype_selection"></div>

<div id="pdfdiv_content">
	<iframe name="receiver" id="receiver" width="0" height="0"></iframe>
</div>

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Redeem</span></div>		
		<div style="float:left;float:right"><input type="text" class="input_text_regular_new_pawn <?=$date_change_allow?>" style="width:80px" id="ddate" value="<?=$current_date?>"></div>				
	</div>

	<form method="post" action="<?=base_url()?>index.php/main/save/t_redeem" id="form_">
		
		<table border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" align="center">
			<tr>
				<td valign="top" align="left" style="width: 740px">

					<div class="pawn_msg"></div>
					<div class="billType_det_holder">
						<table>
							<tr>								
								<td>
									<!--<div class="text_box_holder_new_pawn" style="width:468px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Bill Type</span>
										<input class="input_text_regular_new_pawn" type="text" id="bill_code_desc" style="width:455px">
										<input type="hidden" name="billtype" id="billtype">
									</div>-->
								</td>
								<td>

									<table>

										<tr>

											<td>

												<div class="text_box_holder_new_pawn" style="width:100px;border-bottom:none;float:left">
													<span class="text_box_title_holder_new_pawn">Branch Code</span>
													<input class="input_text_regular_new_pawn txt_bc_no" type="text" id="bc_no" name="bc_no" style="width:90px" value="<?=$bc_no?>" readonly="readonly" maxlength="3">
													
												</div>

											</td>

											<td>											
												<div class="text_box_holder_new_pawn" style="width:204px;border-bottom:none;float:left">
													<span class="text_box_title_holder_new_pawn">Bill Number</span>
													
													<div class='opn_bill_msg'></div>

													<!-- <span class="text_box_title_holder_new_pawn">Bill Number</span>-->

													<input class="input_text_regular_new_pawn" type="text" id="billno" name="billno" style="width:197px;border:1px solid green">
												
													<input type="hidden" id="hid_loan_no">
													<input type="hidden" id="hid_cus_serno" name="hid_cus_serno">
													<input type="hidden" id="bill_code_desc">
													<input type="hidden" id="billtype" name="billtype" >
													<input type="hidden" id="is_bt_choosed" name="is_bt_choosed" value="0">
													<input type="hidden" id="time" name="time" value="0">
													<input type="hidden" id="billcode" name="billcode" >

												</div>

											</td>

											<td>
												<div class="text_box_holder_new_pawn" style="width:204px;border-bottom:none;float:left">
													<label><span class="text_box_title_holder_new_pawn" style="width: 200px">Manual Book Number</span></label>
													<input class="input_text_regular_new_pawn" type="text" id="manual_bill_no" style="width:197px;border:1px solid green">
												</div>
											</td>
										</tr>

									</table>

								</td>																
								<td>
									<div class="text_box_holder_new_pawn" style="width:110px;border:none">
										<!-- <span class="text_box_title_holder_new_pawn">No</span> -->
										<input class="input_text_regular_new_pawn" type="hidden" id="no" name="no" style="width:65px" value="<?=$max_no?>">
									</div>
								</td>
							</tr>
						</table>
					</div>



					<div class="pp_pawn_det_holder">
						
						<br><div class="title_div_pp">Pawn</div>

						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Pawn Date</span>
										<input class="input_text_regular_new_pawn" type="text" id="dDate" name="dDate" style="width:145px">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Period</span>
										<input class="input_text_regular_new_pawn" type="text" id="period" name="period" style="width:145px">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Final Date</span>
										<input class="input_text_regular_new_pawn" type="text" id="finaldate" name="finaldate" style="width:145px">
									</div>
								</td>

								<td width="100">
									<div class="text_box_holder_new_pawn" style="width:120px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Gold Value</span>
										<input class="input_text_regular_new_pawn" type="text" id="goldvalue" name="goldvalue" style="width:110px" readonly="readonly">
									</div>
								</td>

								<td width="100%">
									<div class="text_box_holder_new_pawn" style="width:100%;border:1px solid #ccc;border-left:none;border-right:none">
										<span class="text_box_title_holder_new_pawn">Number of Days</span>
										<input class="input_text_regular_new_pawn" type="text" id="no_of_days" name="no_of_days" style="width:100px" readonly="readonly">
									</div>
								</td>
							</tr>
							<tr>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-top:none">
										<span class="text_box_title_holder_new_pawn">Capital Amount</span>
										<input class="input_text_regular_new_pawn" type="text" id="loan_amount" name="loan_amount" style="width:145px">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none;border-top:none">
										<span class="text_box_title_holder_new_pawn">Interest Rate</span>
										<input class="input_text_regular_new_pawn" type="text" id="fmintrate" name="fmintrate" style="width:145px">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none;border-top:none">
										<span class="text_box_title_holder_new_pawn" style="width:150px;">Interest <!-- <span style="font-size:11px">(Up to <?=date('Y-m-d')?>)</span> --></span>
										<input class="input_text_regular_new_pawn" type="text" id="interest" name="interest" style="width:145px">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:100%;border-bottom:0px solid #ccc;border-left:none;padding-bottom: 11px;">										
										<label>
											<div style="font-size:13px; float: left; width: 285px;position: absolute; border:0px solid red; margin-top: -22px">
												<input type="checkbox" class="cal_5" > Allow interest calculate for days <div style="height: 18px"></div>
											</div>
										</label>
									</div>
								</td>
							</tr>						
							
						</table>

					</div>


					<div class="pp_pawn_det_holder" style="border-top:none">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">

							<tr>
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:155px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Redeem Interest</span>
										<input class="input_text_large" type="text" id="balance" name="balance"  style="width:144px" readonly="readonly">								
									</div>
								</td>

								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:156px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Stamp Duty</span>
										<div style="float: right; margin-top: -5px"><input type="checkbox" class="nostampdutycal tooltip" title="By clicking this checkbox stamp duty will not calculate"></div>
										<input class="input_text_large" type="text" id="stamp_fee" name="stamp_fee"  style="width:145px" readonly="readonly">
									</div>
								</td>

								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:156px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Document Fees</span>
										<input class="input_text_large" type="text" id="doc_fee_tot" style="width:145px" readonly="readonly">
									</div>
								</td>

								<td style="border-top:1px solid #cccccc">
									<div class="text_box_holder_new_pawn" style="width:295px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn" style="width:296px">Customer Advance</span>
										<input class="input_text_large" type="text" id="customer_advance" name="customer_advance" style="width:285px" readonly="readonly"> 
										<input type="hidden" id="customer_advance_paying" name="customer_advance_paying">
										<input type="hidden" id="card_number" name="card_number">
										<input type="hidden" id="card_amount" name="card_amount">
										<input type="hidden" id="cash_amount" name="cash_amount">										
									</div>
								</td>
							</tr>
							<tr>
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:155px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn" style="width:200px">Paid Interest Total</span>
										<input class="input_text_large" type="text" id="paid_interest" name="paid_interest"  style="width:144px" readonly="readonly">								
									</div>
								</td>

								<td>
									<div class="text_box_holder_new_pawn" style="width:156px;border-bottom:1px solid #ccc;background-color:#eaeaea">
										<span class="text_box_title_holder_new_pawn">Discount</span>
										<input class="input_text_large amount" type="text" id="discount" name="discount"  style="width:145px">								
										<input type="hidden" value="0" id="is_discount_approved">
									</div>
								</td>
								<td style="border-bottom: 1px solid #ccc">
									<div class="text_box_holder_new_pawn" style="width:155px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn" style="width:200px">Refundable Interest</span>
										<input class="input_text_large" type="text" id="refundable_int" name="refundable_int"  style="width:144px" readonly="readonly">								
									</div>
								</td>
								<td>							
									<div class="text_box_holder_new_pawn" style="width:295px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Payable Amount</span>
										<input class="input_text_large" type="text" id="payable_amount" name="payable_amount"  style="width:285px" readonly="readonly">								
									</div>
								</td>								
							</tr>
							
						</table>

					</div>

					<div class="div_discount_reason_outer"  style="border:1px solid #cccccc;border-top: none;height: 0px;overflow:hidden">
						<div style="padding: 10px;">
							<span>Reason for discount</span><br>
							<textarea id="discount_reason" style="width: 100%;padding: 4px"></textarea>
						</div>
					</div>
					
					<br><br>
					<div class="">
						<input type="button" value="Save" class="btn_regular" id="btnSave">												
						<input type="button" value="Re-Print" class="btn_regular_disable" id="btnPrint">						
						<div style="float:right; padding-right:20px"><input type="button" value="Reset" class="btn_regular_disable" id="btnReset"></div>
						<input type="hidden" name="hid" id="hid" value="0">

						<input type="hidden" name="no_of_int_cal_days" id="no_of_int_cal_days" value="0">


					</div>

				</td>

				<td valign="top" style="border-left:1px solid #ccc;background-color:#fcfcfc;width: 250px">
					<div class="div_right_details">
						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Information</a></div>
							<div class="div_new_pawn_info_holder" id="cus_info_div"></div>
						</div>

						<div>							
							<div style="padding:10px"><a class="slider_tab">Bill Type Information</a></div>
							<div class="div_new_pawn_info_holder" id="billtype_info_div"></div>
						</div>

						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Pawn Information</a></div>
							<div class="div_new_pawn_info_holder" id=""></div>
						</div>
					</div>
				</td>
			</tr>
		</table>

	</form>

	<!-- <form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="receiver"> -->
	<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' 		id='by'  />            
            <input type="hidden" name="r_no" 	id="r_no"  />
            <input type="hidden" name="ln" 		id="ln"  />
            <input type="hidden" name="doc_fee_r" 	id="doc_fee_r"  />
            <input type="hidden" name="is_reprint" 	id="is_reprint" value="0" />

        </div>                  
    </form>

</div>
</body>
</html>