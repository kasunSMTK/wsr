<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetAutoPageBreak(TRUE, 0);
	$this->pdf->AddPage();

	$this->pdf->SetFont('helvetica', 'B', 10);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
	
	$this->pdf->MultiCell(0,  0, "Issued Voucher List ", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
	$this->pdf->ln(4);
	$this->pdf->SetFont('helvetica', '', 8);		


	$this->pdf->MultiCell(10,  0, "No",					$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(12,  0, "Type",				$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(15,  0, "Status",				$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(60,  0, "Paid From",			$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(60,  0, "Paid To",			$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(30,  0, "Voucher Branch",		$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(30,  0, "Voucher Class",		$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(30,  0, "Amount",				$border = 'B', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(30,  0, "Date Time",			$border = 'LB', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	

	foreach ($det as $r) {


		if ($r->bc_name != $st){
		
			$bc_name = $r->bc_name;
			$st 	 = $r->bc;			

			$this->pdf->SetFont('helvetica', 'B', 8);	
			$this->pdf->MultiCell(0,5, $bc_name, $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);		

		}else{
			$bc_name = "";			
		}


		$h = 4 * 	(	max(1,	

							$this->pdf->getNumLines($r->note,120),
							$this->pdf->getNumLines($r->payee_desc,100),

							$this->pdf->getNumLines($r->paid_acc_desc,60),
							$this->pdf->getNumLines($r->acc_code,60),
							$this->pdf->getNumLines($r->v_bc_name,30),
							$this->pdf->getNumLines($r->v_class,30)			
						) 
					);
		
		if ($r->status == "R"){
			$status = "Rejected";
		}else if ($r->status == "A"){
			$status = "Approved";
		}else if($r->status == "W"){
			$status = "Pending";
		}else{
			$status = $r->status;
		}


		$this->pdf->MultiCell(10,  $h, $r->nno,				$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(12,  $h, $r->type,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(15,  $h, $status,				$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(60,  $h, $r->paid_acc_desc,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(60,  $h, $r->acc_code,		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  $h, $r->v_bc_name,		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  $h, $r->v_class,			$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		
		$this->pdf->SetFont('helvetica', 'B', 10);		
		$this->pdf->MultiCell(30,  $h, $r->amount,			$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);		

		$this->pdf->MultiCell(30,  $h, $r->action_date,		$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

		$this->pdf->SetFont('helvetica', 'B', 8);		
		$this->pdf->MultiCell(30,  $h, "Voucher Description",			$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);		

		$this->pdf->MultiCell(120,$h, $r->note,							$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		
		$this->pdf->SetFont('helvetica', 'B', 8);		
		$this->pdf->MultiCell(28,  $h, "Payee Information",				$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);		

		$this->pdf->MultiCell(100,$h, $r->payee_desc,					$border = 'B', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		
	
	}


	$this->pdf->Output("remind_letters.pdf", 'I');

?>