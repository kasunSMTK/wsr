<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

	$(function() {
		$( "#dDate" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2025",
			onSelect: function(date){
				//change_current_date(date);				
				$("#customer_id").focus().select();
			}
		});
	});

	names 		= [<?=$item_category?>];
	bcCustomer 	= [<?=$bcCustomer?>];	
	gold_q 		= [<?=$gold_q?>];

	function date_change_response_call(){
		//$("#btnReset").click();
	}

</script>

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Pawn Calculator</span></div>		
	</div>

	<form method="post" action="<?=base_url()?>index.php/main/save/t_opening_pawn" id="form_">
		
		<table border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" style="margin:auto" width="800">
			<tr>
				<td valign="top">

					<div id="add_new_cus_front_pawn">
						    
					<div style="margin:20px">						   
						
							<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto;">

								<tr>
									<td>
										<b>Add New Customer</b><br><br>
									</td>
									<td></td>				
								</tr>		

								<tr>									
									
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder" id="SP454">Title</span> 						
											<div style="padding:5px;">
												<label style="margin-right:50px;"><input type="radio" name="title" value="Mrs. " tabindex="3"> Mrs</label>
												<label style="margin-right:50px;"><input type="radio" name="title" value="Miss. "  tabindex="4"> Miss</label>
												<label style="margin-right:00px;"><input type="radio" name="title" value="Mr. "  tabindex="5"> Mr</label>
											</div>						
										</div>										
									</td>

									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Other Names</span> 
											<input class="input_text_regular_pawn_front" type="text" id="other_name">
										</div>
									</td>

								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<div class="input_title_holder">
												
												<select class="customer_id_dropdown" id="customer_id_type">
													<option value="nic">NIC</option>
													<option value="pp">Passport</option>
													<option value="dl">Drving License</option>
													<option value="gm">Grama Niladari #####</option>
												</select>

											</div> 
											<input class="nic_feild input_text_regular_pawn_front" type="text" id="nicno" tabindex="1">
										</div>
									</td>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Phone No</span> 
											<input class="contct_no_feild input_text_regular_pawn_front" type="text" id="telNo">
										</div>
									</td>

								</tr>

								<tr>
									<td>										
										<div class="input_div_holder">
											<span class="input_title_holder">Full Name</span> 
											<input class="input_text_regular_pawn_front" type="text" id="cusname" tabindex="2">
										</div>
									</td>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Address 2</span> 
											<input class="input_text_regular_pawn_front" type="text"  id="address2">
										</div>										
									</td>

								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Address 1</span> 
											<input class="input_text_regular_pawn_front" type="text" id="address" tabindex="6">
										</div>
									</td>			
									<td>					
										
									</td>
								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Mobile No</span> 
											<input class="contct_no_feild input_text_regular_pawn_front" type="text" id="mobile" tabindex="7">
										</div>						
									</td>			
									<td>
									</td>
								</tr>			

								<tr>
									<td>
										<input type="button" value="Save" 	id="btnFrontSave" 			class="btn_regular" tabindex="8">
										<input type="button" value="Cancel" id="btnCancelFrontCusInput"	class="btn_regular">
										<input type="hidden" value="0" 		id="hid" >
									</td>
								</tr>		
								
							</table>

						</form>
					</div>

				</div>

					<div class="canceled_bill_msg"><div class="msg_txt" style="padding:10px;">This bill was canceled</div></div>

					<div class="loan_dtl_contain">

						<div class="text_box_holder_new_pawn" style="width:155px;float:left">
							<span class="text_box_title_holder_new_pawn">Category</span>
							<input class="input_text_regular_new_pawn" type="text" id="cat_code_desc" style="width:140px" value="">
							<input type="hidden" id="cat_code" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:195px;float:left">
							<span class="text_box_title_holder_new_pawn">Item</span>
							<select class="select_drp" style="width:185px" id="item_code"><option value="">Select Item</option></select>
						</div>

						<div class="text_box_holder_new_pawn" style="width:150px;float:left">
							<span class="text_box_title_holder_new_pawn">Condition</span>
							<input class="input_text_regular_new_pawn" type="text" id="condition_desc" style="width:137px" value="">
							<input type="hidden" id="condition" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:110px;float:left">
							<span class="text_box_title_holder_new_pawn">Karatage</span>
							<input class="input_text_regular_new_pawn" type="text" id="gold_type_desc" style="width:97px" value="">
							<input type="hidden" id="goldcatagory" value="">
							<input type="hidden" id="gold_rate" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:60px;float:left">
							<span class="text_box_title_holder_new_pawn">Quality</span>
							<input class="input_text_regular_new_pawn" type="text" id="gold_qulty" style="width:47px" value="100">
							<input type="hidden" id="gold_qulty_rate" value="100.00">
							<input type="hidden" id="gold_qulty_code" value="NQ1">							
						</div>

						<div class="text_box_holder_new_pawn" style="width:60px;float:left">
							<span class="text_box_title_holder_new_pawn">Qty</span>
							<input class="input_text_regular_new_pawn NumOnly" type="text" id="qty" style="width:47px" maxlength="5" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:76px;float:left">
							<span class="text_box_title_holder_new_pawn">T.Weight</span>
							<input class="input_text_regular_new_pawn FloatVal3" type="text" id="t_weigth" style="width:63px" maxlength="7" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">P.Weight</span>
							<input class="input_text_regular_new_pawn FloatVal3" type="text" id="p_weigth" style="width:60px" maxlength="7" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:110px;float:left">
							<span class="text_box_title_holder_new_pawn">Value</span>
							<input class="input_text_regular_new_pawn" type="text" id="value" style="width:100px; text-align:right" readonly="readonly" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:50px;float:left">
							<span class="text_box_title_holder_new_pawn">Action</span>
							<input type="button" value="Add" class="btn_regular" style="width:60px" id="btnAddtoList">
						</div>

					</div>

					<div id="item_list_holder" style="height: auto"></div>
					

				</td>
			</tr>

			<tr>
				<td align="right"><br><input type="text" id="goldvalue" style="border: none;font-family: 'Bitter';font-size: 42px;text-align: right"></input></td>
			</tr>

		</table>

	</form>

	   

</div>
</body>
</html>