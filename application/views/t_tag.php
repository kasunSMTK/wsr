<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

	last_max_no = "<?=$max_no ?>"; 

	$(function() {
		$( ".date_ch_allow" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,			
			yearRange: "1930:2050",
			onSelect: function(date){
				change_current_date(date);				
			}	
		});
	});

	$(function() {
		$( ".date_ch_allow_n" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			minDate:'<?=$backdate_upto?>',
			maxDate:0,
			yearRange: "1930:2025",
			onSelect: function(date){
				change_current_date(date);				
			}
		});
	});

	function date_change_response_call(){
		$("#btnReset").click();
	}

</script>


<style type="text/css">

  #ffTb tbody {
  	font-family: "Roboto";
  	font-size: 13px;
  }

  .bd {
  	border-bottom: 1px dotted #ccc !important;
  	line-height: 10px !important;
  }
  .SlalMn {
  	background-color: #eee;
  }
  #ffTb {
  	padding-bottom: 10px;
  	overflow: hidden;
  }
  
  #ffTb thead {
  	line-height: 10px !important;
  	border: 1px solid #ccc;
  	background-color: #ffffff;
  	color: #000000;
  }

  #ffTb tbody tr:hover {
  	background-color: #eaeaea;
  }

  #GrDt tr:hover{    
    color: #000000;
  }

  #ffbtn {
  	bottom: 0px;
  	width: 100%;
  	border-top: 1px solid #eaeaea;
  	padding: 10px;
  	background-color: #FCFCFC;
  	position: fixed;
  }
  input[type='checkbox'] {
  	padding: 0px;
  	margin: 0px;
  }
  .fixed {
  	position: fixed;
  	top: 0;
  	font-family: "bitter";
  	font-size: 14px;
  	background-color: #ffffff;
  	color: #000000;
    border-bottom: 1px solid #000000;
    margin-left: -1px;
  }
  .bcred {

  	background-color: red;
  	color: #ffffff;
  	border-radius: 0px;
  	-moz-animation-name: blinker;
  	-moz-animation-duration: .5s;
  	-moz-animation-timing-function: linear;
  	-moz-animation-iteration-count: infinite;
  }
   @-moz-keyframes blinker {
     0% {
      opacity: 1.0;
    }
     50% {
      opacity: 0.0;
    }

  }

</style>


<div class="multiple_billtype_selection"></div>
<div id="pdfdiv_content">
	<iframe name="receiver" id="receiver" width="0" height="0"></iframe>
</div>

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Item tag</span></div>		
		<div style="float:left;float:right"><input type="text" class="input_text_regular_new_pawn <?=$date_change_allow?>" style="width:80px" id="ddate" value="<?=$current_date?>"></div>				
	</div>

	<form method="post" action="<?=base_url()?>index.php/main/save/t_tag" id="form_">
		
		<table border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" align="center">
			<tr>
				<td valign="top" align="left" style="width: 950px">

					<div class="pawn_msg"></div>
					<div class="billType_det_holder">
						<table>
							<tr>
								<td>
									<table>
										<tr>
											<td>
												<div class="text_box_holder_new_pawn" style="width:100px;border:none;float:left">
												</div>
											</td>
											<td>											
												<div class="text_box_holder_new_pawn" style="width:204px;border-left:none;float:left">
												</div>
											</td>

											<td>
												<div class="text_box_holder_new_pawn" style="width:204px;border-left:none;float:left">
												</div>
											</td>											
											<td>
												<div class="text_box_holder_new_pawn" style="width:204px;border-left:none;float:left">
													<label><span class="text_box_title_holder_new_pawn" style="width: 200px">&nbsp;</span></label>													
												</div>
											</td>
											<td><td align="right">No&nbsp;&nbsp;&nbsp;&nbsp; </td></td>											
											<td>
												<div class="text_box_holder_new_pawn" style="width:100px;border-left:none;float:left">
													<input type="text" name="nno" id="nno" value="<?=$max_no?>" style='border:1px solid green' readonly='readonly'>						                    
								                    <input type="hidden" name="hid" id="hid" value="0">                    
								                    <input type="hidden" id="hid_type">
								                    <input type="hidden" id="hid_bc" name="hid_bc">
								                    <input type="hidden" id="is_bulk" name="is_bulk" value = 0>
								                    <input class="input_text_regular_new_pawn" type="hidden" id="no" name="no" style="width:65px" value="">

												</div>
											</td>
										</tr>
									</table>


									<table>
										<tr>
											<td>
												<div class="text_box_holder_new_pawn" style="width:100px;border:none;border-left:1;float:left">
													<span class="text_box_title_holder_new_pawn">Branch Code</span>
													<input class="input_text_regular_new_pawn txt_bc_no" type="text" id="bc_no" name="bc_no" style="width:90px" value="<?=$bc_no?>" readonly="readonly" maxlength="3">
													
												</div>
											</td>
											<td>											
												<div class="text_box_holder_new_pawn" style="width:204px;border:none;float:left">
													<span class="text_box_title_holder_new_pawn">Bill Number</span>
													
													<div class='opn_bill_msg'></div>
													<input class="input_text_regular_new_pawn" type="text" id="billno" name="billno" style="width:197px;border:1px solid green">
													<input type="hidden" name="loanno" id="loanno" value="0">
													<input type="hidden" name="goldvalue" id="goldvalue" value="0">
													<input type="hidden" name="billtype" id="billtype" value="0">

													<input type="hidden" id="hid_loan_no">
													<input type="hidden" id="hid_cus_serno" name="hid_cus_serno">
													<input type="hidden" id="bill_code_desc">
													<!-- <input type="hidden" id="billtype" name="billtype" > -->
													<input type="hidden" id="is_bt_choosed" name="is_bt_choosed" value="0">
													<input type="hidden" id="time" name="time" value="0">
													<input type="hidden" id="billcode" name="billcode" >
												</div>
											</td>
											<td>
												<div class="text_box_holder_new_pawn" style="width:204px;border:none;float:left">
													<label><span class="text_box_title_holder_new_pawn" style="width: 200px">Manual Book Number</span></label>
													<input class="input_text_regular_new_pawn" type="text" id="manual_bill_no" style="width:197px;border:1px solid green">
												</div>
											</td>
											<!--
											<td>
												<div class="text_box_holder_new_pawn" style="width:204px;border:none;float:left">
													<label><span class="text_box_title_holder_new_pawn" style="width: 200px">Generate Tag numbers</span></label>
													<input type="button" value="Generate" class="btn_regular" id="btngentag">
												</div>
											</td>
											-->
											<td>
												<div class="text_box_holder_new_pawn" style="width:204px;border-bottom:none;float:left">
													<label><span class="text_box_title_holder_new_pawn" style="width: 200px">&nbsp;</span></label>													
												</div>
											</td>		
											<td><td align="right">Date <input type="hidden" name="time" id="time"> </td></td>									
											<td>
												<div class="text_box_holder_new_pawn" style="width:204px;border:none;float:left">
													<input class="input_text_regular_new_pawn" type="text" id="date" name="date" style="width:100%" readonly="readonly" value="<?=$current_date?>">
												</div>
											</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>
					</div>

					<div class="loan_dtl_contain">
						<table width="98%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Pawn Date</span>
										<input class="input_text_regular_new_pawn" type="text" id="pawn_date" name="pawn_date" style="width:145px">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Period</span>
										<input class="input_text_regular_new_pawn" type="text" id="period" name="period" style="width:145px">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Forfeit Date</span>
										<input class="input_text_regular_new_pawn" type="text" id="forfeit_date" name="forfeit_date" style="width:145px">
									</div>
								</td>

								<td width="100">
									<div class="text_box_holder_new_pawn" style="width:120px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Int rate</span>
										<input class="input_text_regular_new_pawn" type="text" id="int_rate" name="goldvalue" style="width:110px" readonly="readonly">
									</div>
								</td>								
								<td width="140">
									<div class="text_box_holder_new_pawn" style="width:180px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Customer</span>
										<input class="input_text_regular_new_pawn" type="text" id="customer" name="customer" style="width:170px" readonly="readonly">
									</div>
								</td>								
								<td width="100">
									<div class="text_box_holder_new_pawn" style="width:120px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Amount</span>
										<input class="input_text_regular_new_pawn" type="text" id="amount" name="amount" style="width:110px" readonly="readonly">
									</div>
								</td>
								<td width="100">
									<div class="text_box_holder_new_pawn" style="width:120px;border:1px solid #ccc;border-left:none;">
										<span class="text_box_title_holder_new_pawn">Interest</span>
										<input class="input_text_regular_new_pawn" type="text" id="interest" name="interest" style="width:100px" readonly="readonly">
									</div>
								</td>
							</tr>
						</table>
					</div>
					
					<input type="button" value="Generate Item Tags" class="btn_regular" id="btngentag" style='width:100%;display:none'>
					
					
					<div class="pp_pawn_det_holder" id = 'item_data'></div>
					<br><br>					
					<table border = 0 width='1000px'>
						<tr>
							<td width=150px><label><span class="text_box_title_holder_new_pawn" style="width: 150px">Store</span></label></td>
							<td><?=$store?>
						</td>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><tr>
						<tr ><td><label><span class="text_box_title_holder_new_pawn" style="width: 150px">Employee</span></label></td>
							<td><?=$employee?><td>&nbsp;</td>
						</tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><tr>
						<tr>
							<td><label><span class="text_box_title_holder_new_pawn" style="width: 150px">Note</span></label></td>
							<td><textarea id="note"  style="width:400px;"></textarea></td>
						</tr>
					</tr>
					</table>

					<br><br>
					<div class="">
						<input type="button" value="Save" class="btn_regular" id="btnSave">												
						<input type="button" value="Re-Print" class="btn_regular_disable" id="btnPrint">						
						<div style="float:right; padding-right:20px"><input type="button" value="Reset" class="btn_regular_disable" id="btnReset"></div>
						<input type="hidden" name="hid" id="hid" value="0">
						<input type="hidden" name="rowCount" id="rowCount" value="0">

						<input type="hidden" name="no_of_int_cal_days" id="no_of_int_cal_days" value="0">


					</div>
				</td>
				<!--
				<td valign="top" style="border-left:1px solid #ccc;background-color:#fcfcfc;width: 250px">
					
				</td>
				-->
			</tr>
		</table>

	</form>

	<!-- <form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="receiver"> -->
	<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' 		id='by'  value='r_tag'  />            
            <input type="hidden" name="r_no" 	id="r_no"  />
            <input type="hidden" name="is_reprint" 	id="is_reprint" value="0" />

        </div>                  
    </form>

</div>
</body>
</html>