<div class="page_contain">
	<div class="page_main_title"><span>Bill Types</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_billtype_det" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add New Bill Type Rates</b>

					<div style="float:right;"><input type="text"class="input_text_regular_new_pawn" style="border:1px solid #000; width:50px; text-align:center" name="no" id="no" value="<?=$maxno?>"></div>

				</td>
				<td>
					<b>Exist Bill Types Rates</b>
				</td>			

			</tr>			
			
			<tr>
				<td>
					<div class="text_box_holder">

					<table>
						<tr>
							<td width="350">
								<span class="text_box_title_holder"  style="border:0px solid red;width:70px;">Bill Type</span> 
								<input class="input_text_regular" type="text" name="billtype" id="billtype" value="" maxlength="10"  style="width:100px" tabindex="1">
							</td>
							<td width="400">
								<span class="text_box_title_holder"  style="border:0px solid red;width:80px;">Bill Type No</span> 
								<input class="input_text_regular" type="text" name="billtypeno" id="billtypeno" value="" maxlength="10"  style="width:100px" tabindex="1">
							</td>
						</tr>
					</table>

						
						
					</div>
				</td>
				<td rowspan="6" valign="top">							
					<div class="list_div"></div>
				</td>							
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder"  style="border:0px solid red;width:100px;">Amount From</span> 
						<input class="input_text_regular amount" type="text" name="amt_from" id="amt_from" value="" maxlength="10"  style="width:300px;text-align:right" tabindex="2">
					</div>					
				</td>							
			</tr>

			<tr>
				<td>					
					<div class="text_box_holder">
						<span class="text_box_title_holder"  style="border:0px solid red;width:100px;">Amount To</span> 
						<input class="input_text_regular amount" type="text" name="amt_to" id="amt_to" value="" maxlength="10"  style="width:300px;text-align:right" tabindex="3">
					</div>
				</td>							
			</tr>

			<tr>
				<td>					
					<div class="text_box_holder">
						<span class="text_box_title_holder"  style="border:0px solid red;width:100px;">Period</span> 
						<input class="input_text_regular" type="text" name="period" id="period" value="" maxlength="10"  style="width:300px" tabindex="4">
					</div>
				</td>							
			</tr>

			<tr>
				<td>

					<table style='border:1px solid red; margin-left:10px'>
							<tr>
								<td rowspan=2>
									<div>
										<label><input type="radio" class='rd' name="rd" id="lt_opt" value="L" checked="checked" tabindex="6"> Lessthan</label><br>
										<label><input type="radio" class='rd' name="rd" id="gt_opt" value="G" tabindex="7"> Greaterthan</label><br>
										<label><input type="radio" class='rd' name="rd" id="bt_opt" value="B" tabindex="8"> Between</label>
									</div>
								</td>
								<td>
									<span class="text_box_title_holder"  style="border:0px solid red;width:120px;">Percentage(%)</span> 
									<input class="input_text_regular" type="text" name="percentage" id="percentage" maxlength="6"  style="border:1px dotted #ccc;width:200px" tabindex="5">
								</td>
							</tr>
							<tr>
								
								<td>
									<span class="text_box_title_holder"  style="border:0px solid red;width:120px;">Percentage To (%)</span> 
									<input class="input_text_regular" type="text" readonly="readonly" name="percentage_to" id="percentage_to" maxlength="6"  style="border:1px dotted #ccc;width:200px" tabindex="5">
								</td>
							</tr>
						</table>

					<div class="text_box_holder"></div>
				</td>							
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">						
						<div class="bt_bc_list_holder">
							
							<div style="margin-bottom:5px; border:1px solid #fff">
								<span class="text_box_title_holder"  style="border:0px solid red;width:100px;">Branch</span>
							</div>
							
							<div style="text-align:right; border-bottom:1px dotted #aeaeae;padding-bottom:10px; margin-bottom:10px;"> 
								<label><input type="checkbox" class="chkbc_select"> <span class="chk_main_lable">Select All</span></label> 
							</div> 
							
							<div class="bc_list_holder">
								<?=$branches_list_chk?>
							</div>

						</div>
					</div>
				</td>							
			</tr>

			<tr>
				<td>					
					<div class="text_box_holder" style="height: 35px">
						<div style="float: left">
						<label>
							<input type="checkbox" name="weekly_cal"><div style="float: right;">
							<span class="text_box_title_holder"  style="border:0px solid red;width:100px;margin-top:-7px">&nbsp;Weekly Interest</span> 							
							</div>
						</label>
						</div>
						<div style="float: left">
						<label>
							<input type="checkbox" name="is_old_bill"><div style="float: right;">
							<span class="text_box_title_holder"  style="border:0px solid red;width:100px;margin-top:-7px">&nbsp;Old Billtype</span> 							
							</div>
						</label>
						</div>
					</div>
				</td>							
			</tr>

			<tr>
				<td style="padding-left: 16px"><br><br>
					
					<table class="clz_billt">
						<thead>
							<tr>
								<td>Days From</td>
								<td>Days To</td>
								<td>interest Rate</td>
								<td style="text-align:center"><!-- Is Monthly --></td>
							</tr>
						</thead>

						<tbody>
							
							<?php for($no = 0 ; $no < 5 ; $no++){ ?>
							<tr>
								<td><input id = "A<?=$no?>" name="day_from[]" 	type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;margin-bottom:5px;"></td>
								<td><input id = "B<?=$no?>" name="day_to[]" 	type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td>
								<td><input id = "C<?=$no?>" name="rate[]"		type="text"  class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td>								
								<td><a class="btnremoveRow">remove</a></td>								
							</tr>

							<?php } ?>							

						</tbody>

						<tfoot>
							<tr>
								<td colspan="4"><a class="btnAddaRow">Add Row</a></td>								
							</tr>
						</tfoot>

						
					</table>
					<br><br>
				</td>			
			</tr>		
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width='950' class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Remind Letters Period Setup</b><br><br>
					<div style="padding: 10px; background-color: green;color:#ffffff;width:520px"><span>Remind letter will generate base on bill's final date calculating with below values</span></div>
				</td>
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder"  style="border:0px solid red;width:100px;">1st Letter</span> 
						<input class="input_text_regular" type="text" name="r1" id="r1" maxlength="10"  style="width:300px;text-align:right"> Days
					</div>					
				</td>
			</tr>
			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder"  style="border:0px solid red;width:100px;">2nd Letter</span> 
						<input class="input_text_regular" type="text" name="r2" id="r2" maxlength="10"  style="width:300px;text-align:right"> Days
					</div>					
				</td>
			</tr>
			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder"  style="border:0px solid red;width:100px;">3rd Letter</span> 
						<input class="input_text_regular" type="text" name="r3" id="r3" maxlength="10"  style="width:300px;text-align:right"> Days
					</div>					
				</td>
			</tr>

			<tr>
				<td height="50"></td>
			</tr>				

			<tr>
				<td>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">
					<input type="button" value="Reset" 	name="btnReset"  id="btnReset" 	class="btn_regular_disable">
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>							
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>