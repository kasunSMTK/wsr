<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>
last_max_no = "<?=$max_no?>";


function date_change_response_call() {
    $("#btnReset").click();
}
</script>


<style type="text/css">
#ffTb tbody {
    font-family: "Roboto";
    font-size: 13px;
}

.bd {
    border-bottom: 1px dotted #ccc !important;
    line-height: 10px !important;
}

.SlalMn {
    background-color: #eee;
}

#ffTb {
    padding-bottom: 10px;
    overflow: hidden;
}

#ffTb thead {
    line-height: 10px !important;
    border: 1px solid #ccc;
    background-color: #ffffff;
    color: #000000;
}

#ffTb tbody tr:hover {
    background-color: #eaeaea;
}

#GrDt tr:hover {
    color: #000000;
}

#ffbtn {
    bottom: 0px;
    width: 100%;
    border-top: 1px solid #eaeaea;
    padding: 10px;
    background-color: #FCFCFC;
    position: fixed;
}

input[type='checkbox'] {
    padding: 0px;
    margin: 0px;
}

.fixed {
    position: fixed;
    top: 0;
    font-family: "bitter";
    font-size: 14px;
    background-color: #ffffff;
    color: #000000;
    border-bottom: 1px solid #000000;
    margin-left: -1px;
}

.input_text_regular_new_pawn_solid {
    @extend .input_text_regular_new_pawn;
    border: 1px solid green;
    border-radius: 4px;
    font-family: "Roboto";
    width: 180px;
    padding: 5px;
    font-size: 12px;
}

.text_right {

    text-align: right;
}

.bcred {

    background-color: red;
    color: #ffffff;
    border-radius: 0px;
    -moz-animation-name: blinker;
    -moz-animation-duration: .5s;
    -moz-animation-timing-function: linear;
    -moz-animation-iteration-count: infinite;
}

@-moz-keyframes blinker {
    0% {
        opacity: 1.0;
    }

    50% {
        opacity: 0.0;
    }

}

</style>



<div class="multiple_billtype_selection"></div>
<div id="pdfdiv_content">
    <iframe name="     " id="receiver" width="0" height="0"></iframe>
</div>

<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:200px;float:left"><span>Mark Damage Item</span></div>
        <div style="float:left;float:right"><input type="text"
                class="input_text_regular_new_pawn <?=$date_change_allow?>" style="width:80px" id="ddate"
                value="<?=$current_date?>"></div>
    </div>

    <form method="post" action="<?=base_url()?>index.php/main/save/t_damage" id="form_">

        <table border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" align="center">
            <tr>
                <td>
                    <div class="text_box_holder_new_pawn" style="width:600px; border:none">
                        <span class="text_box_title_holder_new_pawn">Item</span>
                        <input class="input_text_regular_new_pawn_solid" type="text" name="item" id="item"
                            style="width:342px" value="">
                        <input type="hidden" name="item_tag" id="item_tag" value="">
                    </div>
                </td>
            <tr>
                <td>
                    <div class="text_box_holder_new_pawn" style="width:600px; border:none">
                        <span class="text_box_title_holder_new_pawn">Narration</span>
                        <input class="input_text_regular_new_pawn_solid" type="text" name="damage_narration"
                            id="damage_narration" style="width:342px" value="">

                    </div>
                </td>
            </tr>


        </table>
        <br><br>
        <br><br>
        <table align="center">
            <tr>
                <td>
                    <div class="">
                        <input type="button" value="Save" class="btn_regular" id="btnSave">
                        <input type="button" value="Reset" class="btn_regular_disable" id="btnReset">
                        <input type="button" value="Cancel" class="btn_regular_disable" id="btnCancel">

                    </div>
                </td>
            </tr>
        </table>
    </form>
</div>

</body>

</html>
