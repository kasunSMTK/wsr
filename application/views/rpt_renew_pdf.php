<?php



	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);

	$this->pdf->SetPrintHeader(false);

	$this->pdf->SetPrintFooter(false);

	$this->pdf->AddPage();	



	$this->pdf->SetFont('helvetica', '', 28);	

	$this->pdf->setY(10);

	$this->pdf->setX(0);

	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));

	$this->pdf->Line(5, 0, 0, 0, $style);



	$this->pdf->MultiCell(0, 0, "Renew Bill Details Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	

	$this->pdf->SetFont('helvetica', '', 15);	

	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->setX(0);

	$this->pdf->ln();

	



	$this->pdf->SetFont('helvetica', '', 9);	

	$this->pdf->MultiCell(20, 1, "BC", 'B','L', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(30, 1, "Bill No", 'B','L', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(20, 1, "Billtype", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(20, 1, "Date", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(30, 1, "Transaction", 'B','L', 0, 0, '', '', false, '', 0);	

	$this->pdf->MultiCell(30, 1, "Amount", 'B','R', 0, 0, '', '', false, '', 0);

	

	$this->pdf->MultiCell(40, 1, "Date Time", 'B','R', 0, 1, '', '', false, '', 0);

	

	$s = '';
	$billtype="";



	foreach($list as $r){



		if ($s != $r->billno || $billtype !=$r->billtype ){

			

			$s = $r->billno;
			$billtype = $r->billtype;

			$this->pdf->MultiCell(20, 0, $r->bc, $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(30, 0, $r->billno, $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(20, 0, $r->billtype, $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(20, 0, $r->ddate, $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(30, 0, $r->transaction, $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(30, 0, $r->amount, $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(40, 0, $r->action_date, $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);





			//$this->pdf->MultiCell(10, 0, $no , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);



		}else{

			

			$this->pdf->MultiCell(20, 0, '', $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(30, 0, '', $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(20, 0, '', $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(20, 0, $r->ddate, $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(30, 0, $r->transaction, $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(30, 0, $r->amount, $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);

			$this->pdf->MultiCell(40, 0, $r->action_date, $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='M', $fitcell=false);



		}

		







	}



	

	







	function d($number) {

	    return number_format($number, 2, '.', ',');

	}



	function d3($number) {

	    return number_format($number, 3, '.', ',');

	}





	$this->pdf->Output("PDF.pdf", 'I');



?>

