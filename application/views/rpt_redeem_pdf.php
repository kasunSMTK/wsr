<?php



	$this->pdf = new TCPDF("L", PDF_UNIT, 'A3', true, 'UTF-8', false);

	$this->pdf->SetPrintHeader(false);

	$this->pdf->SetPrintFooter(false);

	$this->pdf->AddPage();	



	$this->pdf->SetFont('helvetica', '', 28);	

	$this->pdf->setY(10);

	$this->pdf->setX(0);

	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));

	$this->pdf->Line(5, 0, 0, 0, $style);



	$this->pdf->MultiCell(0, 0, "Redeem Activity Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	

	$this->pdf->SetFont('helvetica', '', 15);	

	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->setX(0);

	$this->pdf->ln();

	



	$this->pdf->SetFont('helvetica', '', 9);	

	$this->pdf->MultiCell(10, 1, "S.No", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(15, 1, "Bill Type", 'B','L', 0, 0, '', '', false, '', 0);//

	$this->pdf->MultiCell(27, 1, "Bill No", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(19, 1, "Pawn Date", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(22, 1, "Redeem Date", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(70, 1, "Cus Name / Address", 'B','L', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(21, 1, "NIC", 'B','C', 0, 0, '', '', '', '', 0);

	$this->pdf->MultiCell(20, 1, "Mobile", 'B','C', 0, 0, '', '', '', '', 0);

	$this->pdf->MultiCell(20, 1, "Weight", 'B','C', 0, 0, '', '', '', '', 0);		

	

	$this->pdf->MultiCell(23, 1, "Pawn Interest", 'B','R', 0, 0, '', '', '', '', 0);	

	$this->pdf->MultiCell(24, 1, "Capital Amount", 'B','R', 0, 0, '', '', '', '', 0);	

	$this->pdf->MultiCell(18, 1, "Stamp Fee", 'B','R', 0, 0, '', '', '', '', 0);

	// $this->pdf->MultiCell(14, 1, "Postage", 'B','R', 0, 0, '', '', '', '', 0);		

	$this->pdf->MultiCell(26, 1, "Redeem Interest", 'B','R', 0, 0, '', '', '', '', 0);	

	$this->pdf->MultiCell(20, 1, "Discount", 'B','R', 0, 0, '', '', '', '', 0);	

	$this->pdf->MultiCell(30, 1, "Redeemed Amount", 'B','R', 0, 0, '', '', '', '', 0);

	$this->pdf->SetFont('helvetica', '', 7);	

	$this->pdf->MultiCell(25, 1, "Date Time", 'B','C', 0, 1, '', '', '', '', 0);

	$this->pdf->SetFont('helvetica', '', 9);

	

	$this->pdf->SetFont('', '', 9);

	



	$no = 1;

	$a = $b = $c = $d = $e = $f = $g = $hh = 0;

	$Aa = $Ab = $Ac = $Ad = $Ae = $Af = $Ag = $Ahh = 0;

	$s = '';



	$firs_round_passed = false;



	foreach($list as $r){





		if ($s != $r->bc_name){







			if ($firs_round_passed){



				$this->pdf->SetFont('helvetica', 'B', 9);

				$this->pdf->MultiCell(10, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(15, $h, '' , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(27, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(19, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(22, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(70, $h, '', 	$border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(21, $h, '' , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(20, $h, 'Total' , 	$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(20, $h, d3($a), $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(23, $h, d($hh) ,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(24, $h, d($b) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(18, $h, d($e) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				// $this->pdf->MultiCell(14, $h, d($f) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(26, $h, d($g) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(20, $h, d($c) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				$this->pdf->MultiCell(30, $h, d($d),  $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				

				$this->pdf->SetFont('helvetica', '', 7);

				$this->pdf->MultiCell(25, $h, '',  $border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

				

				$this->pdf->SetFont('helvetica', '', 9);

				$a = $b = $c = $d = $e = $f = $g = $hh = 0;

			}





			$firs_round_passed = true;







			

			$this->pdf->SetFont('helvetica', 'B', 9);

			$this->pdf->MultiCell(0, 0, $r->bc_name  	,$border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

			$s = $r->bc_name;

			$this->pdf->SetFont('helvetica', '', 9);



		}







		$h = 5 * (max(1,$this->pdf->getNumLines($r->cusname." - ".$r->address,80)));

		

		$this->pdf->MultiCell(10, $h, $no , 			$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(15, $h, $r->billtype , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(27, $h, $r->billno , 		$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(19, $h, $r->pawn_date , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(22, $h, $r->ddate , 		$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(70, $h, $r->cusname." - ".$r->address, $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(21, $h, $r->nicno , 		$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(20, $h, $r->mobile , 		$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(20, $h, $r->totalweight , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	

		$this->pdf->MultiCell(23, $h, d($r->pawn_int) 			,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);		

		$this->pdf->MultiCell(24, $h, d($r->capital_amount) 	,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(18, $h, d($r->stamp_fee) 			,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		// $this->pdf->MultiCell(14, $h, d($r->postage) 			,$border='B',$align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(26, $h, d($r->redeem_int) 		,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(20, $h, d($r->redeem_discount + $r->refund_amount) 	,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->MultiCell(30, $h, d($r->redeem_amount - $r->refund_amount)  	,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		

		$this->pdf->SetFont('helvetica', '', 7);

		$this->pdf->MultiCell(30, $h, $r->ddate." ".$r->tm  	,$border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

		$this->pdf->SetFont('helvetica', '', 9);



		$no++;



		$a += $r->totalweight;

		$hh += $r->pawn_int;

		$b += $r->capital_amount;

		$e += $r->stamp_fee;		

		$f += $r->postage;

		$g += $r->redeem_int;

		$c += $r->redeem_discount + $r->refund_amount;

		$d += ($r->redeem_amount - $r->refund_amount);





		$Aa += $r->totalweight;

		$Ahh += $r->pawn_int;

		$Ab += $r->capital_amount;

		$Ae += $r->stamp_fee;		

		$Af += $r->postage;

		$Ag += $r->redeem_int;

		$Ac += $r->redeem_discount + $r->refund_amount;

		$Ad += ($r->redeem_amount - $r->refund_amount);



	}



	$this->pdf->SetFont('', 'B', 9);



	$this->pdf->MultiCell(10, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(15, $h, '' , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(27, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(19, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(22, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(70, $h, '', 	$border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(21, $h, '' , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(20, $h, 'Total' , 	$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	

	$this->pdf->MultiCell(20, $h, d3($a), $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(23, $h, d($hh) ,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(24, $h, d($b) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(18, $h, d($e) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	// $this->pdf->MultiCell(14, $h, d($f) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(26, $h, d($g) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(20, $h, d($c) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(30, $h, d($d),  $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);





	$this->pdf->ln(5);





	$this->pdf->SetFont('', 'B', 9);



	$this->pdf->MultiCell(10, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(15, $h, '' , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(27, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(19, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(22, $h, '' , 	$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(70, $h, '', 	$border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(21, $h, '' , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(20, $h, 'All Branch Total' , 	$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	

	$this->pdf->MultiCell(20, $h, d3($Aa), $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(23, $h, d($Ahh) ,$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(24, $h, d($Ab) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(18, $h, d($Ae) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	// $this->pdf->MultiCell(14, $h, d($Af) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(26, $h, d($Ag) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(20, $h, d($Ac) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);

	$this->pdf->MultiCell(30, $h, d($Ad),  $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);





	







	function d($number) {

	    return number_format($number, 2, '.', ',');

	}



	function d3($number) {

	    return number_format($number, 3, '.', ',');

	}





	$this->pdf->Output("PDF.pdf", 'I');



?>

