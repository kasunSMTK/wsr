
<?php defined('BASEPATH') OR exit('No direct script access allowed'); $menu_item_visibility = array_filter($menu_item_visibility); ?>

<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script> 

	$(function() {
		$( "#from_date,#to_date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2050"
		});
	});	

</script>

<style type="text/css">
	

.dropdown {
  
}

a {
  color: #000;
}

.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: 0 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #000;
  text-decoration: none;
  outline: none;
  font-size: 12px;
}

.dropdown dt a {
  background-color: #ffffff;
  display: block;
  padding: 0px 10px 0px 10px ;
  min-height: 25px;
  line-height: 24px;
  overflow: hidden;
  border: 0;
  width: 272px;

  border: 2px solid Green;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
  display: inline-block;
  padding: 0 3px 2px 0;
}

.dropdown dd ul {
  background-color: #ffffff;
  border: 1px solid #cccccc;
  color: #000;
  display: none;
  left: 0px;
  padding: 10px;

  position: absolute;
  top: 2px;
  width: 272px;
  list-style: none;
  height: 300px;
  overflow: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #000;
}

button {
  background-color: #6BBE92;
  width: 302px;
  border: 0;
  padding: 10px 0;
  margin: 5px 0;
  text-align: center;
  color: #000;
  font-weight: bold;
}

</style>

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Pawning Reports</span></div>	
	</div>


	<div class="rpt_form" style="padding-top: 0px">
		<form id="print_report_pdf" action="<?php echo site_url(); ?>/reports/generate_r" method="post" target="_blank">

			<div class="rpt_critaria_bar">
				<br>
				<table border="0" cellpassing="0" cellspacing="0">
					<tr>
						<td colspan="2" style="padding-right: 3px" valign="top">
							From Date<div style="padding-top:4px"></div>
							<input name="from_date" id="from_date" class="input_text_regular_new_pawn" style="width:75px;border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
							
						</td>
						<td colspan="1" valign="top">
							To Date<div style="padding-top:4px"></div>
							<input name="to_date" id="to_date" class="input_text_regular_new_pawn" style="width:75px;border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
							
						</td>
						

						<td style='padding-left:0px' valign="top"><!-- Branch<div style="padding-top:4px"></div><?=$bc_dropdown?> --></td>

						<td style='padding-left:25px' valign="top">Branch<div class="div_rpt_makr_red"></div><div style="padding-top:4px"></div>
							<dl class="dropdown">

							  <dt>
							    <a href="#">

								    <?php if ( $is_bc_user != '' ){ ?>								      	
								      	<p class="multiSel"><?= $is_bc_user ?></p>  
								   	<?php }else{ ?>
								   		<span class="hida">Select</span>    
								      	<p class="multiSel"></p>
								   	<?php } ?>

							    </a>
							  </dt>

							  <dd>
							    <div class="mutliSelect">
							      <ul>

							      	<?=$bc_dropdown_multi?>

							      </ul>
							    </div>
							  </dd>
						  
						  

						</dl>

						</td>



						<td style='padding-left:25px' valign="top">Bill Type<div style="padding-top:4px"></div><?=$billtype_dropdown?></td>
						<td style='padding-left:25px' valign="top">Expense Account<div style="padding-top:4px"></div>

						<?=$expense_dropdown?>

						</td>

						<td style="padding-left: 25px" valign="top">						

							<div class='C_from_time' style="float: left;margin-right:5px;display: none;">
								From Time<div style="padding-top:4px"></div>
								
								<div style="border:2px solid Green;padding: 1px;border-radius: 4px;">
								<select name="f_hour" class="f_hour" style="width: auto;"><option value="">hour</option><option value="0">00</option><option value="1">01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select> : 

								<select name="f_minute" class="f_minute" style="width: auto;"><option value="">minute</option><option value="0">00</option><option value="1">01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>
								</div>
							</div>

							<div class='C_from_time' style="float: left;margin-right:5px;display: none;">
								To Time<div style="padding-top:4px"></div>
								
								<div style="border:2px solid Green;padding: 1px;border-radius: 4px;">
								<select name="t_hour" class="t_hour" style="width: auto;"><option value="">hour</option><option value="0">00</option><option value="1">01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select> : 

								<select name="t_minute" class="t_minute" style="width: auto;"><option value="">minute</option><option value="0">00</option><option value="1">01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>
								</div>
							</div>						

						</td>

						<td valign="top">All Accounts <div style="padding-top:4px"></div>
							<input type="text" class="txtmultiaccselector input_text_regular_new_pawn" style="border:2px solid Green; padding: 4px;">
						</td>

					</tr>
				</table><br>

			</div><div class="zxc"></div>

				<table border="0" cellpassing="0" cellspacing="0" class='tbl_page_holder'>
					
					<?php $menu_items=array("rpt_pawnings"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<div class="div_rpt_makr_red"></div><label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_pawnings"> Pawning </div> </label> 
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_canceled_pawnings"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_canceled_pawnings"> Canceled Pawning </div> </label>
						</td>				
					</tr>
					<?php } ?>
					
					<!-- <?php $menu_items=array("rpt_pawning_activity"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_pawning_activity"> Pawning Activity Report </div> </label>						
						</td>				
					</tr>
					<?php } ?> -->

					<?php $menu_items=array("rpt_redeem"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_redeem"> Redeem </div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_renew"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_renew"> Renew History </div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_part_payments"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_part_payments"> Advance Payments (System) </div> </label>
						</td>				
					</tr>
					<?php } ?>

					<!-- <?php $menu_items=array("rpt_part_payments_details"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_part_payments_details"> Advance Payments Details </div> </label>
						</td>				
					</tr>
					<?php } ?> -->

					<?php $menu_items=array("rpt_daily_cash_report"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">
							<div class="opt_holder">
							<label>
							<input type="radio" name="by" value="rpt_daily_cash_report">
								
								<select name="bc_acc_det_list" id="bc_acc_det_list">
			                        <option value="303003">Daily Cash Account Report</option>
			                        <option value="10102">Redeem Interest Account Report</option>
			                        <option value="30201">Unredeem Articles Account Report</option>
			                        <option value="10101">Pawning Interest Account Report</option>
			                        <option value="40215">Customer Advance Account Report</option>
			                        <option value="20305">Redeem Discount Account Report</option>

			                        <option value="40212">Stamp Fee Account Report</option>
			                        <option value="30211">postage Receivable Account Report</option>

			                        
			                    </select>								

			                </label>
							</div>
						</td>
					</tr>
					<?php } ?>					

					<?php $menu_items=array("rpt_forfeited_pawnings"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="2">						
							<div class="opt_holder">
								<label><input type="radio" name="by" value="rpt_forfeited_pawnings">Forfeited Pawning Details</label>
							</div>
						</td>
						<td colspan="2">						
							<div class="opt_holder">
								<label><input type="radio" name="by" value="rpt_forfeited_pawnings_summery">Forfeited Pawning Summery</label>
							</div>
						</td>
					</tr>
					<?php } ?>
					
					<!-- <?php $menu_items=array("rpt_current_stock"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_current_stock">Current Stock</div></label>
						</td>				
					</tr>
					<?php } ?> -->

					<!-- <?php $menu_items=array("rpt_trail_balance"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_trail_balance">Outstanding Stock</div></label>
						</td>				
					</tr>
					<?php } ?> -->

					<?php $menu_items=array("rpt_daily_summery_total"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label>
								<div class="opt_holder">
									<input type="radio" name="by" value="rpt_daily_summery_total">
									Business Summary									
								</div>
							</label>

						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_daily_summery_total"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label>
								<div class="opt_holder">
									<input type="radio" name="by" value="rpt_daily_summery_total_n">
									Business Summary with Manual Transactions
								</div>
							</label>

						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_audit_callout_report"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<div class="div_rpt_makr_red"></div>
							<label>
								<div class="opt_holder">
									<input type="radio" name="by" value="rpt_audit_callout_report">
									Audit Callout Report
								</div>
							</label>

						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_customer_information_report"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> 
								<div class="opt_holder"> 
									<input type="radio" name="by" value="rpt_customer_information_report">
									Customer Information Report &nbsp;

									<input name="txt_rpt_cus_info_info" id="txt_rpt_cus_info_info" class="input_text_regular_new_pawn" style="border:1px solid Green" value="">

									
									&nbsp; EMI No/ Searing No &nbsp;

									<input name="txt_rpt_cus_emi_no" id="txt_rpt_cus_emi_no" class="input_text_regular_new_pawn" style="border:1px solid Green" value="">

								</div>
							</label>
						</td>				
					</tr>
					<?php } ?>

					<!-- <?php $menu_items=array("rpt_bc_daily_cash"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> 
								<div class="opt_holder"> 
									<input type="radio" name="by" value="rpt_bc_daily_cash">
									Branch Daily Cash
								</div>
							</label>
						</td>				
					</tr>
					<?php } ?> -->

					<?php $menu_items=array("rpt_interest_incme"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_interest_incme"> Interest Income </div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_interest_incme_psum"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_interest_incme_psum"> Interest Income Schedule Summary- Pawning 
								&nbsp;
								<select name="data_range" id="data_range">
									<!--   <option value='0'>- - - - - - -</option> -->
									<option value='months'>Months</option>
									<option value='years'>Years</option>
									<option value='quarter'>Quarter</option>
									<option value='half_year'>Half Year</option>

								</select>
							</div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_interest_incme_p"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<!-- <tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_interest_incme_p"> Interest Income Schedule Details- Pawning 
								&nbsp;
								<select>									
									<option value='1'>Months</option>
									<option value='2'>Years</option>
									<option value='3'>Quarter</option>
									<option value='4'>Half Quarter</option>

								</select>
							</div> </label>
						</td>				
					</tr> -->
					<?php } ?>

					<?php $menu_items=array("rpt_interest_incme_rsum"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_interest_incme_rsum"> Interest Income Schedule Summary- Redeem 
								&nbsp;
								<select name="r_data_range" id="r_data_range">
									<!-- <option value='0'>- - - - - - -</option> -->
									<option value='months'>Months</option>
									<option value='years'>Years</option>
									<option value='quarter'>Quarter</option>
									<option value='half_year'>Half Year</option>

								</select>
							</div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_interest_incme_tsum"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_interest_incme_tsum"> Interest Income Schedule Summary- Total 
								&nbsp;
								<select name="t_data_range" id="t_data_range">
									<!-- <option value='0'>- - - - - - -</option> -->
									<option value='months'>Months</option>
									<option value='years'>Years</option>
									<option value='quarter'>Quarter</option>
									<option value='half_year'>Half Year</option>

								</select>
							</div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_pwing_periodical"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_pwing_periodical"> Pawning Periodical Summary
								&nbsp;
								<select name="p_data_range" id="p_data_range">
									<!--   <option value='0'>- - - - - - -</option> -->
									<option value='months'>Months</option>
									<option value='years'>Years</option>
									<option value='quarter'>Quarter</option>
									<option value='half_year'>Half Year</option>

								</select>
							</div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_rdming_periodical"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_rdming_periodical"> Redeeming Periodical Summary
								&nbsp;
								<select name="rdm_data_range" id="rdm_data_range">
									<!--   <option value='0'>- - - - - - -</option> -->
									<option value='months'>Months</option>
									<option value='years'>Years</option>
									<option value='quarter'>Quarter</option>
									<option value='half_year'>Half Year</option>

								</select>
							</div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_pawning_analysis"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_pawning_analysis"> Given Period Pawning Analysis </div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_unredeem_analysis"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<!-- <tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_unredeem_analysis"> Unredeem Artical Balance </div> </label>
						</td>				
					</tr> -->
					<?php } ?>

					<?php $menu_items=array("rpt_expense_analysis"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<!-- <tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_expense_analysis"> Branch wise Expense Analysis </div> </label>
						</td>				
					</tr> -->
					<?php } ?>

					<?php $menu_items=array("rpt_pcl_expense_analysis"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_pcl_expense_analysis_n"> Periodical Expense Analysis 
								&nbsp;
								<select name="a_data_range" id="a_data_range">
									<!--   <option value='0'>- - - - - - -</option> -->
									<option value='months'>Months</option>
									<option value='years'>Years</option>
									<option value='quarter'>Quarter</option>
									<option value='half_year'>Half Year</option>

								</select>
							</div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_ntranse_periodical"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_ntranse_periodical"> N of Transactions with in the Period
								&nbsp;
								<!-- <select name="n_data_range" id="n_data_range">									
									<option value='months'>Months</option>
									<option value='year'>Years</option>
									<option value='quarter'>Quarter</option>
									<option value='half_year'>Half Year</option>
								</select> -->
							Time range from : <input type="text" class="tf" name="tf">
							Time range to : <input type="text" class="tt" name="tt">
							</div> </label>
						



						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_daily_cash_rec"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_daily_cash_rec">Daily Cash Reconciliation </div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_black_list_customer"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_black_list_customer">Black listed Customer List </div> </label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_customer_hitory"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_customer_hitory">Customer History
								<input name="txt_rpt_cus_info" id="txt_rpt_cus_info" class="input_text_regular_new_pawn" style="border:1px solid Green" value="">
							</div></label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_pawning_adv_age"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_pawning_adv_age">Unredeemed Articles Maturity Analysis
							</div></label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_unredeem_forfeited"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_unredeem_forfeited"> Unredeemed Article to be forfeited
							</div></label>
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_customer_evaluation"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_customer_evaluation">Customer Evaluation
								<input id="is_act" type="checkbox" name="is_act" title="1" value="0">
								<input name="txt_rpt_from_r" id="txt_rpt_from_r" class="input_text_regular_new_pawn" style="border:1px solid Green; text-align: right;" value="" placeholder="From value Range" readonly>
								<input name="txt_rpt_to_r" id="txt_rpt_to_r" class="input_text_regular_new_pawn" style="border:1px solid Green; text-align: right;" value="" placeholder="To Value Range" readonly>
								&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="checkbox" name="cus_eva_pawn">Pawning</label>
								&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="checkbox" name="cus_eva_redm">Redeem</label>
							</div></label>


						</td>				
					</tr>
					<?php } ?>

					<!-- <?php $menu_items=array("rpt_over_advance"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_over_advance">Over Advance Report </div> </label>
						</td>				
					</tr>
					<?php } ?> -->

					<?php $menu_items=array("rpt_zone_voucher_list"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label>

								<div class="opt_holder"> <input type="radio" name="by" value="rpt_zone_voucher_list">Zone Voucher List

									&nbsp;
									
									<select name="vou_stat">
				                        <option value="All">All</option>
				                        <option value="W">Pending</option>
				                        <option value="A">Approved</option>
				                        <option value="R">Rejected</option>
				                        <option value="C">Canceled</option>
				                        <option value="F">Forwarded to higher level approval</option>
				                    </select>

								</div>

							</label>

						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_mark_bills_list"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label>

								<div class="opt_holder"> <input type="radio" name="by" value="rpt_mark_bills_list">Mark Bills
									&nbsp;									
									<select name="mark_bill_type">
				                        <option value="All">All</option>
				                        <option value="P">Police</option>
				                        <option value="L">Lost</option>				                        
				                    </select>
								</div>

							</label>

						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_fund_transfer_activity"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label>

								<div class="opt_holder"> <input type="radio" name="by" value="rpt_fund_transfer_activity">Fund Transfer Activity
									&nbsp;									
									<select name="fund_transfer_status">
				                        <option value="">All</option>
				                        <option value="1">Waiting</option>
				                        <option value="2">Accepted transfer requests</option>
				                        <option value="3">Rejected transfer requests</option>
				                        <option value="4">Voucher printed</option>
				                        <option value="5">Request forwarded</option>
				                        <option value="10">Transaction completed</option>				                        
				                    </select>
								</div>

							</label>

						</td>				
					</tr>
					<?php } ?>




					<?php $menu_items=array("rpt_income_statement"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label>

								<div class="opt_holder"> <input type="radio" name="by" value="rpt_income_statement">
								Income Statement (testing only)
								</div>

							</label>

						</td>				
					</tr>
					<?php } ?>


					<?php $menu_items=array("rpt_income_statement"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label>

								<div class="opt_holder"> <input type="radio" name="by" value="rpt_income_statement_2">
								Income Statement - Distributed (testing only)
								</div>

							</label>

						</td>				
					</tr>
					<?php } ?>


					<!-- <?php $menu_items=array("rpt_unredeem_forfeited"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_unredeem_forfeited_n"> Unredeemed Article to be forfeited (new)
							</div></label>
						</td>				
					</tr>
					<?php } ?> -->



					<?php $menu_items=array("rpt_daily_cash_bal"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_daily_cash_bal"> Daily Cash Balance Report (New) </div> </label> 
						</td>				
					</tr>
					<?php } ?>

					<?php $menu_items=array("rpt_issued_vouchers"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_issued_vouchers"> Issued Vouchers </div> </label> 
						</td>				
					</tr>
					<?php } ?>


					<?php $menu_items=array("rpt_issued_branch_receipt"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_issued_branch_receipt"> Branch Receipt </div> </label> 
						</td>				
					</tr>
					<?php } ?>



					<?php 

					// var_dump($menu_item_visibility);
					// exit;

					$menu_items=array("rpt_gl"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_gl"> GL </div> </label> 
						</td>				
					</tr>
					<?php } ?>


					<?php

					$menu_items=array("rpt_ai"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> 
							<div class="opt_holder" style="height: 32px; overflow: hidden; "> 

								<input type="radio" name="by" value="rpt_AI" class="rd_ai_click"> Accrued Interest 							

								<dir>
									<?=$set_AI_gold_rate_inputs?>
								</dir>

							</div>
							</label>
						</td>				
					</tr>
					<?php } ?>



					<?php

					$menu_items=array("rpt_ecd"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> 
							<div class="opt_holder" style="height: 34px">
								
								<div style="float: left">
									<input type="radio" name="by" value="rpt_ecd" class=""> Expense Class Distribution 
								</div>

								<div style="float: left;margin-left:10px">
									<?=$set_ecd_dropdown?>
								</div>

							</div>
							</label>
						</td>				
					</tr>
					<?php } ?>


					<?php

					$menu_items=array("rpt_over_adv_by_approval"); if (count(array_intersect($menu_items,$menu_item_visibility)) > 0){?>
					<tr>					
						<td colspan="4">						
							<label> 
							<div class="opt_holder" style="height: 34px">
								
								<div style="float: left">
									<input type="radio" name="by" value="rpt_over_adv_by_approval" class="">Over Advance by Approval
								</div>

							</div>
							</label>
						</td>				
					</tr>
					<?php } ?>


					<tr>
						<td colspan="4" style="height: 100px;"></td>
					</tr>

				</table>

				<div class="ccc"></div>

				<div class="r_btn_holder">
					<div style="text-align:left; padding-top: 7px;">
						<input type="button" id="btnGenerate" 	src="about:blank" class="btn_regular" value="Generate PDF" style="width:184px;font-family:'bitter'">
						<input type="button" id="btnGenerateEx" src="about:blank" class="btn_regular" value="Generate Excel" style="width:183px;font-family:'bitter'">	        	
					</div>
				</div>

			</form>
		

			

		</div>


		<form id="print_excel_form" method="post" target="_blank">		
			
			<input type="hidden" name="from_date" id="from_date_ex">
			<input type="hidden" name="to_date" id="to_date_ex">
			<input type="hidden" name="h_bc" id="h_bc">
			<input type="hidden" name="h_range" id="h_range">
			<input type="hidden" name="bill_types" id="bill_types">
			<input type="hidden" name="h_acc" id="h_acc">
			<input type="hidden" name="by" id="by_ex">
			<input type="hidden" name="bc_arry" id="bc_arry">
			<input type="hidden" name="txt_rpt_cus_info_info" id="txt_rpt_cus_info_info_ex">
			<input type="hidden" name="ccgr_id" id="AA">
			<input type="hidden" name="ccgr" id="BB">
			<input type="hidden" name="ecd" id="CC">
			<input type="hidden" name="bc_acc_det_list_hid" id="DD">
			<input type="hidden" name="data_range_hid" id="EE">

			<input type="hidden" name="h_tf" id="h_tf">
			<input type="hidden" name="h_tt" id="h_tt">

			

		</form>



	</div>

	<script type="text/javascript">
		/*
	Dropdown with Multiple checkbox select with jQuery - May 27, 2013
	(c) 2013 @ElmahdiMahmoud
	license: http://www.opensource.org/licenses/mit-license.php
*/

$(".dropdown dt a").on('click', function() {
  $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function() {
  $(".dropdown dd ul").hide();
});

$(document).on("click",".all_bc_select",function(){

	if ( $(this).is(":checked") ){

		$(".mutliSelect").find('li').each(function(){
			$(this).find('input[type="checkbox"]').prop("checked",true);
		});

		$(".multiSel").html("<div class='all_bc_div'>All Branches <input type='hidden' value='' name='bc_n'></div>");

	}else{

		$(".mutliSelect").find('li').each(function(){
			$(this).find('input[type="checkbox"]').prop("checked",false);
		});

		$(".multiSel").find('.all_bc_div').remove();


	}

});

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});

$('.mutliSelect input[type="checkbox"]').on('click', function() {

  $(".multiSel").parent().find('.all_bc_div').remove();

  var 	title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    	title = $(this).val() + ",";

  if ($(this).is(':checked')) {
    
    var html = '<span title="' + title + '">' + title + '</span><input type="hidden" name="bc_arry[]" value="' + title + '" title="' + title + '">';
    
    $('.multiSel').append(html);
    $(".hida").hide();
  
  } else {
    
    $('span[title="' + title + '"]').remove();
    $('input[title="' + title + '"]').remove();
    var ret = $(".hida");
    
    $('.dropdown dt a').append(ret);

  }

});
	</script>

</body>
</html>