<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>
	$(function() {
        $("#date").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });
</script>

<style>
#bc{
	margin-top: 10px;	
    border: none;
    font-family: "bitter";
    font-size: 20px;    
}
</style>

<div class="page_contain_new_pawn">

	<div class="msg_pop_up_bg_PP"></div>
	<div class="msg_pop_up_sucess_PP">Text Here...</div>

	<div class="page_main_title_new_pawn">
		<div style="width:200px;float:left"><span>Salary Vouchers</span></div>		
		<div style="float:left;float:right"></div>		
	</div>


		
		<form method="post" action="<?=base_url()?>index.php/main/save/t_salary_vouchers" id="form_">
			
			<div style="width:900px;margin:auto;margin-top:20px">
				<table border="0" align="center" cellpadding="0" cellspacing="0"  class="tbl_sv" width="100%">

					<thead>
						<tr>
							<td colspan="4" style="text-align:right">
								<input type="text" id="date" name="date" value="<?=date('Y-m-d')?>" class="input_text_gl">
							</td>
						</tr>
						<tr>
							<th>Year &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Month<br><?=$YM['Y']?><?=$YM['M']?></th>
							<th>Branches <br><?=$bc?></th>
							<th colspan="3"><input type="button" value="Import" class="btn_regular" id="btnImport" style="padding:10px;margin-top:12px;width:100%"></th>
						</tr>
						<tr><td style="border-bottom:1px solid #cccccc" colspan="5"></td></tr>
					</thead>

				</table>
			</div>

			<div style="width:900px;margin:auto;margin-top:20px">
				<table border="0" align="center" cellpadding="0" cellspacing="0"  class="tbl_sv" width="100%">

					<tbody>
						<tr>				
							<td>No</td>
							<td>Emp Number</td>
							<td>Emp Name</td>
							<td align="right">Amount</td>
							<td>Action</td>
						</tr>
					</tbody>

					<tfoot>
						<tr>
							<td colspan="5" align="center">Data not imported  </td>
						</tr>
					</tfoot>			
				
				</table>
			</div>

			<div style="width:900px;margin:auto;margin-top:20px;padding-bottom:200px;">
				<table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							<input type="button" value="Send to Voucher Approval" class="btn_regular_disable" disabled="disabled" id="btnSendtoApproval" style="padding:10px;margin-top:12px;width:200px;font-family: 'bitter'">
						</td>
					</tr>				
				</table>
			</div>

		</form>	

	
    

</div>
</body>
</html>