<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'A3', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->AddPage();	
$this->pdf->SetFont('helvetica', '', 28);	
$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(204, 204, 204));

$this->pdf->setX(5);
$this->pdf->MultiCell(0, 0, "Unredeemed Articles Maturity Analysis", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 9);	
$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->SetFont('helvetica', 'B', 8);
$this->pdf->setX(5);
$this->pdf->MultiCell(35,1, "Branch" , 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "1- 30" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "31-60" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "61-90" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "91-120" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "121-150" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "151-180" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "181-210" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "211-240" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "241-270" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "271-300" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "301-330" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "331-360" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "361-390" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(26,1, "Over 390" , 	$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 8);




$s_bc = '';

$first_round_passed = false;



$A= $B= $C= $D= $E= $F= $G= $H= $I= $J= $K= $L= $M= $N= 0;


foreach($list as $r){

	if ( $s_bc != $r->bc ){

		if ($first_round_passed){
			/*$this->pdf->setX(5);
			$this->pdf->SetFont('helvetica', 'B', 8);
			$this->pdf->MultiCell(15,1, "Total" , 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($A,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($B,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($C,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($D,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($E,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($F,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($G,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($H,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($I,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($J,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($K,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(23,1, number_format($L,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
			$this->pdf->MultiCell(23,1, number_format($M,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
			$this->pdf->SetFont('helvetica', '', 8);*/

			//$A= $B= $C= $D= $E= $F= $G= $H= $I= $J= $K= $L=$M= 0;
		}
		
		$first_round_passed = true;

		

		/*$this->pdf->ln();
		$this->pdf->setX(5);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->MultiCell(100,1, $r->bc_name , $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->ln();*/

		/*$this->pdf->setX(5);
		$this->pdf->MultiCell(15,1, "Bill Type" , 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "1- 30" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "31-60" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "61-90" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "91-120" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "121-150" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "151-180" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "181-210" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "211-240" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "241-270" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "271-300" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "301-330" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "331-360" , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(23,1, "Over 360" , 	$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);*/

		$s_bc = $r->bc;
	}

	$this->pdf->setX(5);
	$this->pdf->MultiCell(35,1, $r->bc_name , 				$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R30,2) , 	$border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R60,2) , 	$border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R90,2) , 	$border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R120,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R150,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R180,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R210,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R240,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R270,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R300,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R330,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R360,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->R390,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($r->OVR390,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$A += $r->R30;
	$B += $r->R60;
	$C += $r->R90;
	$D += $r->R120;
	$E += $r->R150;
	$F += $r->R180;
	$G += $r->R210;
	$H += $r->R240;
	$I += $r->R270;
	$J += $r->R300;
	$K += $r->R330;
	$L += $r->R360;
	$N += $r->R390;
	$M += $r->OVR390;



}


	$this->pdf->setX(5);
	$this->pdf->SetFont('helvetica', 'B', 9);
	$this->pdf->MultiCell(35,1, "Total" , 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($A,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($B,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($C,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($D,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($E,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($F,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($G,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($H,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($I,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($J,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($K,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(26,1, number_format($L,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	$this->pdf->MultiCell(26,1, number_format($N,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	$this->pdf->MultiCell(26,1, number_format($M,2) , 	$border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	$this->pdf->SetFont('helvetica', '', 8);

$this->pdf->Output("Unredeemed Articles Maturity Analysis.pdf", 'I');



?>