<?php
		error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
		$this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(true);
        //print_r($det);
        $this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->AddPage("L","SEW");   // L or P amd page type A4 or A3
		//$this->pdf = new TCPDF("L", PDF_UNIT, 'SEW', true, 'UTF-8', false);
		// Data Tobe retrieve
		$bank_acc = "";$chq_no = "";
		foreach ($cheque as $chq) {
			$bank_acc = $chq->bank;	
			$chq_no = $chq->cheque_no ;
		}
		
		$recipient = "";

 		foreach ($company as $r) {
		    $c_name=$r->name; 
		}
		foreach ($branch as $ress) {
		    $name=$ress->name;
		    $address=$ress->address;
		    $tp= $ress->telno;
		    $fax=$ress->faxno;
		    $email=$ress->email;    
		}
		$this->pdf->headerSet4($c_name,$address, $tp, $fax, $email);

		$cus_name=$cus_address="";

		foreach($det as $row){
		$description=$row->memo;
		$date=$row->ddate;
		$ref_no=$row->ref_no;
		$amount=$row->amount;
		$cus=$row->name;
		}

		foreach ($sum as $r) {
			$types=$r->type;
			$chq_amount =$r->cheque_amount;
			$cash_amount =$r->cash_amount;
			$note = $r->note;
			$recipient = $r->payee_name;
			$bank_acc_name = $r->bank_acc_name;
			$approvd_by = $r->approvd_by;
		}

		foreach($supplier as $sup){
			$sup_name=$sup->name;
			$sup_id=$sup->code;
		}
		foreach($items as $itm){
			$itm_bal=$itm->bal;
			$dueAmount=$itm->balance;
		}
			foreach($user as $row){
		 		$operator=$row->loginName;
		}

		foreach($session as $ses){
			$invoice_no=$session[0];
		}


			$this->pdf->setY(20);
        	$this->pdf->SetFont('helvetica', 'B', 10);
			$this->pdf->Ln();
			$orgin_print=$_POST['org_print'];
			if($orgin_print=="1"){
		 	$this->pdf->Cell(0, 5,'Payment Voucher ('.strtoupper($types).') ',0,false, 'C', 0, '', 0, false, 'M', 'M');
			 }else{
			$this->pdf->Cell(0, 5,'Payment Voucher ('.strtoupper($types).') (DUPLICATE) ',0,false, 'C', 0, '', 0, false, 'M', 'M'); 	
			 }
			 // New Format
			$this->pdf->Ln();
		 	$this->pdf->Ln();

		 	$this->pdf->SetFont('helvetica', '', 9);
		 	
		 	// Branch
		 	$this->pdf->MultiCell(36,  0, "Branch ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(80, 0, $name,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			
			// ###
			// Voucher No.
		 	
			$this->pdf->MultiCell(0, 0, "Voucher No : ".$invoice_no,$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			// Date
		 	$this->pdf->MultiCell(36,  0, "Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(0, 0, $ddate ,$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

			// Bank Acc.
		 	$this->pdf->MultiCell(36,  0, "Account",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(0, 0, $bank_acc." - ".$bank_acc_name ,$border = '0', $align = 'L', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			
			// Cheque No.
		 	$this->pdf->MultiCell(36,  0, "Cheque No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(0, 0, $chq_no ,$border = '0', $align = 'L', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		 	
		 	// ###
		 	// Name of the Recipient:
		 	$this->pdf->MultiCell(36,  0, "Name of the Recipient ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		 	$this->pdf->MultiCell(0,  0, $recipient,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


		 	
		 

		 	
		 	$this->pdf->Ln();		 
		 	$this->pdf->Ln();
		 	// 180 

			// $this->pdf->SetX(35);
			$this->pdf->Cell(150, 1,"Account Details", 'TRL', 0, 'C', 0);
		 	$this->pdf->Cell(30, 1,"Amount", '1', 0, 'C', 0);
		 	$this->pdf->Ln();
		 	$this->pdf->Cell(150, 1,"", 'RBL', 0, 'C', 0);
		 	$this->pdf->Cell(20, 1,"Rs.", '1', 0, 'C', 0);
		 	$this->pdf->Cell(10, 1,"Ct.", '1', 0, 'C', 0);
		 	$this->pdf->Ln();

			
		 	foreach ($dets as $value) {		
		 		$this->pdf->SetFont('helvetica', '', 8);
		 		$this->pdf->Cell(150, 1,$value->acc_code." | ".$value->description, 'BL', 0, 'L', 0);
		 		$str = explode(".", $value->amount);
			 	$this->pdf->Cell(20, 1,$str[0], 'B', 0, 'R', 0);
			 	$this->pdf->Cell(10, 1,$str[1], 'RB', 0, 'L', 0);
			 	
			 	$this->pdf->Ln();$this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));		    
			}

		 	/*$this->pdf->Cell(45, 1, "Towards Full Payment for In No ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $vou_des, '0', 0, 'L', 0);*/
		 	
		    /*if(!empty($cheque)){	
		    	$this->pdf->Ln();$this->pdf->Ln();
		    	$this->pdf->SetFont('helvetica', 'B', 8);
		    	$this->pdf->Cell(10, 1, 'Cheque Details   ' , '0', 0, 'L', 0);
		    	$this->pdf->Ln();
		    	$this->pdf->SetFont('helvetica', '', 8);
			 	$this->pdf->Cell(30, 1,"Cheque No", '1', 0, 'C', 0);
			 	$this->pdf->Cell(90, 1,"Bank", '1', 0, 'C', 0);
			 	$this->pdf->Cell(30, 1,"Cheque Date", '1', 0, 'C', 0);
			 	$this->pdf->Cell(30, 1,"Amount", '1', 0, 'C', 0);
			 	$this->pdf->Ln();

			 	foreach ($cheque as $chq) {			   
				    $this->pdf->SetX(15);
				    $this->pdf->SetFont('helvetica', '', 8);
				    $this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));
				    $this->pdf->Cell(30, 6, $chq->cheque_no, '1', 0, 'L', 0);
				    $this->pdf->Cell(90, 6, $chq->bank." | ".$chq->description, '1', 0, 'L', 0);
				    $this->pdf->Cell(30, 6, $chq->bank_date, '1', 0, 'L', 0);
				    $this->pdf->Cell(30, 6, $chq->amount, '1', 0, 'R', 0);
				    $this->pdf->Ln();			    
				}

		    }*/
		    $this->pdf->Ln();$this->pdf->Ln();

		 	
		 	$this->pdf->Cell(150, 1,"Description :".$note, '1', 0, 'L', 0);
		 		$str = explode(".", $value->amount);
		 	$this->pdf->Cell(20, 1,"", '1', 0, 'R', 0);
		 	$this->pdf->Cell(10, 1,"", '1', 0, 'L', 0);

		 	


		 	$this->pdf->SetFont('helvetica', 'B', 8);
		 	
		 	if($chq_amount>0){
		 	// 	$this->pdf->Cell(20, 1,"Cheque Amount", 'TBL', 0, 'L', 0);
				// $this->pdf->Cell(20, 1,number_format($chq_amount,2), 'TBR', 0, 'R', 0);
		 	// 	$this->pdf->Cell(10, 1,'', '0', 0, 'L', 0);
		 		$tot = $chq_amount;
			}
			
		 	
		 	if($cash_amount>0){
			 // 	$this->pdf->Cell(20, 1,"Cash", 'TBL', 0, 'L', 0);
				// $this->pdf->Cell(20, 1,number_format($cash_amount,2), 'TBR', 0, 'R', 0);
		 		$tot = $cash_amount;
		 	}

		 	$this->pdf->Ln();
		 	$str_tot = explode(".", $tot);
		 	$this->pdf->SetFont('helvetica', 'B', 10);
		 	$this->pdf->Cell(150, 1,"Total (".$this->utility->convert_number_to_words($tot).")", '1', 0, 'L', 0);
		 		$str = explode(".", $value->amount);
		 	$this->pdf->Cell(20, 1,$str_tot[0], '1', 0, 'R', 0);
		 	$this->pdf->Cell(10, 1,$str_tot[1], '1', 0, 'L', 0);    
		 	
		 	
		 	$this->pdf->Ln();

		 	$this->pdf->SetFont('helvetica', '', 8);
		
         
		 	$this->pdf->Ln();
		 	$this->pdf->Cell(0, 1, $approvd_by, '0', 0, 'C', 0);
		 	

		 	$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '       ..........................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	//$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
			$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '       Prepared By', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '                System Approval', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '        1. Approval', '0', 0, 'L', 0);
		 	//$this->pdf->Cell(70, 1, 'Customer Signature', '0', 0, 'L', 0);
		 
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();
		 	$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
		 	//$this->pdf->Cell(70, 1, '...................................', '0', 0, 'L', 0);
			$this->pdf->Ln();
		 	$this->pdf->Cell(70, 1, '       2. Approval', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, '', '0', 0, 'L', 0);
		 	$this->pdf->Cell(70, 1, ' Signature of Recipient', '0', 0, 'L', 0);


		 	$this->pdf->Ln();$this->pdf->Ln();
		 	$this->pdf->Cell(20, 1, "Operator ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $this->session->userdata('user_des'), '0', 0, 'L', 0);
		 	$this->pdf->Ln();

		 	$tt = date("H:i");

		 	
		 	$this->pdf->Cell(20, 1, "Print Time ", '0', 0, 'L', 0);
		 	$this->pdf->Cell(1, 1, $tt, '0', 0, 'L', 0);
		 	$this->pdf->Ln();



	//$this->pdf->footerSet($employee,$amount,$additional,$discount,$user);

	$this->pdf->Output("Voucher".date('Y-m-d').".pdf", 'I');

?>