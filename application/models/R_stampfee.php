<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_stampfee extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){		

        $this->load->model("m_branches");
        $a['bc_dropdown'] = $this->m_branches->select();
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){
        if ($_POST['hid'] == 0){
            unset($_POST['hid']);
        	$Q = $this->db->insert("r_stampfee",$_POST);
        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("recid",$hid)->update("r_stampfee",$_POST);
        }

        if ($Q){
            echo 1;
        }else{
            echo 0;
        }
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $recid = $_POST['recid'];

       if ($this->db->query("DELETE FROM`r_stampfee` WHERE recid = '$recid' ")){
            echo 1;
       }else{
            echo 0;
       }
    }

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `r_stampfee` ORDER BY `recid` DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->recid . " </td><td class='D'>-</td><td class='Des'> (" .$R->fromvalue. " - ". $R->tovalue .") = ". $R->stampfee . "</td><td class='ED'><a href=# onClick=setEdit('".$R->recid."')>Edit</a> | <a href=# onClick=setDelete('".$R->recid."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
        $T .= "<table>";    
        return $T;
    }
    
    public function set_edit(){
        $recid = $_POST['recid'];
        $R = $this->db->query("SELECT * FROM `r_stampfee` WHERE recid = '$recid' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `r_stampfee` WHERE nicno like '%$k%'");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->recid . " </td><td class='D'>-</td><td class='Des'> (" .$R->fromvalue. " - ". $R->tovalue .") = ". $R->stampfee . "</td><td class='ED'><a href=# onClick=setEdit('".$R->recid."')>Edit</a> | <a href=# onClick=setDelete('".$R->recid."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
        $T .= "<table>";  
    
        echo $T;
            
    }

}