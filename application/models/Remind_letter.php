<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class remind_letter extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report(){

        $no = $_POST['l_no'];        
        $bc = $this->sd['bc'];
        
        $q = $this->db->query("SELECT B.`telno`, S.`no`, D.`billno`, D.`letter_no`, L.`cus_serno`, L.`requiredamount`,CONCAT(C.`title`, C.`cusname`) AS cusname, C.`address`, L.`finaldate`, L.`ddate` , L.`period` , 

                                                        

                                                        IFNULL((SELECT `date` FROM t_remind_letter_sum WHERE NO = FLD.no),'')  AS first_let_gen_date,



                                                        B.`name` AS `bc_name` , B.`address` AS `bc_address`

                                FROM `t_remind_letter_sum` S 

                                JOIN `t_remind_letter_det` D ON S.`no` = D.`no` AND S.`bc` = D.`bc` 
                                LEFT JOIN loan_tblz_union L ON D.`billno` = L.`billno` AND D.`bc` = L.`bc`
                                LEFT JOIN m_customer C ON L.`cus_serno` = C.`serno`
                                JOIN m_branches B ON S.`bc` = B.`bc`
                                
                                LEFT JOIN (     SELECT `no`,letter_no,billno
                                                FROM t_remind_letter_det d 
                                                WHERE letter_no = 1
                                                GROUP BY NO,letter_no,billno   ) FLD ON D.`billno` = FLD.billno

                                WHERE S.`bc` = '$bc' AND S.`no` = $no 
                                ORDER BY L.`cus_serno` DESC ");


        /*$q = $this->db->query("SELECT `no`,`billno`, `letter_no`, `cus_serno`, `requiredamount`, `cusname`, `address`, `finaldate`, `ddate` , `period` ,`first_let_gen_date`,`bc_name` , `bc_address`

FROM s ");*/





        if($q->num_rows()>0){
            $r_detail['det'] = $q->result();
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}