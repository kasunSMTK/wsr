<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class calculate extends CI_Model {

        private $sd;    
        private $max_no;    

        function __construct(){
          parent::__construct();		
          $this->sd = $this->session->all_userdata();		
      }

    public function base_details(){
       $a['a'] = 1;        
        return $a;
    }


    public function calculate_interest( $req_amount='', $billtype='', $pawn_date = '', $current_date = '' , $first_mon_int_cal_range = 3 , $loanno = '' , $is_day_cal_allow = false , $int_paid_untill , $is_amount_base_int_cal , $fm_int_paid = 0 , $ori_pwn_date , $data = ''  ){

        if ($data != ''){
            $_POST['cat_code'] = $data->cat_code;
            $_POST['is_non_gold'] = $data->is_non_gold;
        }else{
            $_POST['cat_code'] = '';
            $_POST['is_non_gold'] = '';
        }

        if ($req_amount == ''){ echo 'invalid amount'; exit; }
        if ($billtype == ''){ echo 'invalid bill type'; exit; }
        if ($pawn_date == ''){ echo 'invalid pawn date'; exit; }
        
        if (isset($_POST['req_amount'])){$req_amount = $_POST['req_amount']; }else{$req_amount = $req_amount; }
        
        $query = $this->db->query("SELECT * FROM m_bill_type  WHERE billtype = '".$billtype."' AND bc= '".$this->sd['bc']."' LIMIT 1 ");
        
        $first_month_int = $query->row()->first_month_int;
        $next_months_int = $query->row()->next_months_int;
        $next_months_int_gp2 = $query->row()->next_months_int_gp2;
        
        $gp1 = $query->row()->gp1;
        $gp1_int_rate = $query->row()->gp1_int_rate;
        
        $gp2 = $query->row()->gp2;
        $gp2_int_rate = $query->row()->gp2_int_rate;
        
        $period = $query->row()->period        ;
        
        $amount_from = $query->row()->amount_from;
        $amount_to = $query->row()->amount_to;

        $fm_interest = 0;
        $nm_interest = 0;
        $next_mon_days = 0;
        $no_full_months = 0;
        $remain_month = 0;
        $total_int = 0;
        $int_bal = 0;
        $paid_int = $this->paid_interest($loanno); // get paid int by with sum of paid int cr tot
        $a['ib'] = $ib = $this->get_int_balance($loanno);
        $a['fm_int'] = $fm_interest;

        if($query->num_rows()){
            
            $NOD = intval($this->db->query(" SELECT DATEDIFF('$current_date','$pawn_date')+1 AS `no_of_days` ")->row()->no_of_days);

            if ( $is_amount_base_int_cal == 1  ){

                if ( $req_amount < 50000 ){                    
                    $first_month_int = 10; // set this val value from billtype setup in the future
                    $next_months_int = 10;  // set this val value from billtype setup in the future
                }

                if ( $req_amount >= 50000 ){                    
                    $first_month_int = 5; // set this val value from billtype setup in the future
                    $next_months_int = 5; // set this val value from billtype setup in the future
                }

            }



            // 1st Month

            if ( $is_day_cal_allow ){

                if ( $NOD <= $gp1 ){
                    // first month grace period 1 int cal
                    $fm_interest = ($req_amount * $gp1_int_rate) / 100;
                }elseif ( $NOD > $gp1 && $NOD <= $gp2 ){
                    // first month grace period 2 int cal
                    $fm_interest = ($req_amount * $gp2_int_rate) / 100;
                }else{
                    // First full month interest
                    $fm_interest = ( $req_amount * $first_month_int ) / 100;
                }

            }else{

                $fm_interest = ( $req_amount * $first_month_int ) / 100;

            }

            // End 1st Month



        }


        //-----------------------------------------------------------------------------------------------

        $pawn_date = $int_paid_untill;


        $date_info = $this->db->query("SELECT (CASE WHEN (DAY('$current_date') < DAY('$pawn_date') ) THEN PERIOD_DIFF( DATE_FORMAT('$current_date','%Y%m') , DATE_FORMAT('$pawn_date','%Y%m') ) -1 ELSE PERIOD_DIFF( DATE_FORMAT('$current_date','%Y%m') , DATE_FORMAT('$pawn_date','%Y%m') ) END)  AS noofmonths , DATE_ADD( '$pawn_date' , INTERVAL (CASE WHEN (DAY('$current_date') < DAY('$pawn_date') ) THEN PERIOD_DIFF( DATE_FORMAT('$current_date','%Y%m') , DATE_FORMAT('$pawn_date','%Y%m') ) -1 ELSE PERIOD_DIFF( DATE_FORMAT('$current_date','%Y%m') , DATE_FORMAT('$pawn_date','%Y%m') ) END ) MONTH) AS last_month_date , DATEDIFF( '$current_date' , DATE_ADD( '$pawn_date' , INTERVAL (CASE WHEN (DAY('$current_date') < DAY('$pawn_date') ) THEN PERIOD_DIFF( DATE_FORMAT('$current_date','%Y%m') , DATE_FORMAT('$pawn_date','%Y%m') ) -1 ELSE PERIOD_DIFF( DATE_FORMAT('$current_date','%Y%m') , DATE_FORMAT('$pawn_date','%Y%m') ) END ) MONTH) ) AS days_remain");

        $a['noof_months'] = intval($date_info->row()->noofmonths) < 0 ? 0 : intval($date_info->row()->noofmonths);
        $a['days_remain'] = 0;
        $add_month        = 0;


        if ( $this->sd['current_date'] == $ori_pwn_date ){

            if ( $this->sd['current_date'] < $int_paid_untill ){
                $add_month = 0;
            }else{
                $add_month = 1;
            }

        }else{

            if ($pawn_date < $current_date){
            
                if($a['noof_months']  < 0){
                    $a['noof_months'] = 0;
                    $a['days_remain'] = 0;
                }else{
                    $a['days_remain'] = intval($date_info->row()->days_remain);
                }


                if ( $a['noof_months'] == 0 ){
                    $add_month = 1;
                }        


                if ($a['noof_months'] > 0 &&  $a['days_remain'] > 0 ){
                    $add_month = 1;
                }

            }

        }
        
        $a['no_of_full_months'] = $a['noof_months'] + $add_month;



        if ( $_POST['cat_code'] == 15 || $_POST['cat_code'] == 16 || $_POST['cat_code'] == 17 || $_POST['cat_code'] == 24 ){

            if ( $req_amount <= 500 ){
                $a['monthly_int'] = 50; // use dynamic var to here later
            }else{
                $a['monthly_int'] = (($req_amount * $first_month_int ) / 100);                
            }

        }else{

            if ( $_POST['is_non_gold'] == 0 ){

                if ($req_amount <= 1000){
                    $a['monthly_int'] = 20;
                }else{
                    $a['monthly_int'] = (($req_amount * $first_month_int ) / 100);
                }

            }else{
                $a['monthly_int'] = (($req_amount * $first_month_int ) / 100);
            }


        }



        $a['fm_deduct_int']     = 0; // ($a['monthly_int'] - $fm_interest);        

        $a['total_due']         = $total_due = floatval($this->get_total_due($loanno));
        

        // Check the bill type for non gold items ------------
        // $_POST['cat_code'] == 15 || $_POST['cat_code'] == 16 || $_POST['cat_code'] == 17 || $_POST['cat_code'] == 24
        if ( $billtype = 'N1'){
            if ( $req_amount <= 500 ){
                $IB = $this->roundUpToAny(( 50 * $a['no_of_full_months']) - $a['fm_deduct_int'] , 5 );
            }else{
                $IB = $this->roundUpToAny(((($req_amount * $first_month_int ) / 100) ) - $a['fm_deduct_int'] , 5 ) * $a['no_of_full_months'];
            }
            
        }else{

            if ( $_POST['is_non_gold'] == 0 ){

                if ( $req_amount < 1000 ){                
                    $IB = $this->roundUpToAny(( 20 * $a['no_of_full_months']) - $a['fm_deduct_int'] , 5 );
                }else{
                    $IB = $this->roundUpToAny(((($req_amount * $first_month_int ) / 100) ) - $a['fm_deduct_int'] , 5 ) * $a['no_of_full_months'];
                }

            }else{
                $IB = $this->roundUpToAny(((($req_amount * $first_month_int ) / 100) ) - $a['fm_deduct_int'] , 5 ) * $a['no_of_full_months'];
            }
        }


        $a['undue_payable_int'] = $int_bal = $IB;



        $a['payable_tot_int_balance'] = number_format(($int_bal+$a['ib']),2,".","") < 0 ? 0 : number_format(($int_bal+$a['ib']),2,".","");

        $a['int_within_10days'] = false;
        $a['no_of_days']        = $NOD;
        $a['over_one_month_int']= $nm_interest;
        $a['paid_int']          = $paid_int;
        $a['refundable_int']    = 0;
        $a['total_int']         = $fm_interest + $nm_interest;
        $a['is_day_cal_allow']  = $is_day_cal_allow;
        $a['capital_bal']       = $req_amount;

        

        $a['no_of_months_to_int_cal'] = 0;
        $a['int_rate'] = 0;

        
        return ($a);


        /*function printNestedArray($a) {
            echo '<pre>';
            foreach ($a as $key => $value) {
                echo htmlspecialchars("$key: ");
                if (is_array($value)) {
                    printNestedArray($value);
                } else {
                    echo htmlspecialchars($value) . '<br />';
                }
            }
            echo '</pre>';
        } 

        printNestedArray($a);*/


    }

    
    public function get_bill_transaction_history($bc,$loan_no){

        return $this->db->select( array("*","ifnull(discount,0) as discount","CONCAT(ddate,' ', SUBSTR(action_date,12,8))AS trans_datetime") )->where(array("bc"=>$bc,"loanno"=>$loan_no))->where_in(array("transecode"=>"PP","transecode"=>"P"))->where("is_pawn_int !=",1)->order_by("action_date","desc")->get("t_loantranse")->result();

    }












    public function ALLOW_FIRST_MONTH_PRE_INTEREST($bill_type,$int_cal_changed,$bc=""){

        if ($bc == ""){
            $bc = $this->sd['bc'];
        }else{
            $bc = $bc;
        }

        $Q = $this->db->query("SELECT weekly_cal FROM `r_bill_type_sum` WHERE billtype = '$bill_type' AND bc = '$bc' LIMIT 1");

        if ($Q->num_rows() > 0){

            if ($Q->row()->weekly_cal == 1){

                if ($int_cal_changed == 1){
                    return false;
                }else{
                    return true;
                }

            }else{

                if ($int_cal_changed == 1){
                    return true;
                }else{
                    return false;
                }
                
            }

        }else{
            echo "Error, Billtype not found";
        }
    }

    public function roundDownToAny($n,$x=5){
        return floor($n/5) * 5;
    }

    public function roundUpToAny($n,$x=5) {

        $n = preg_replace('/(\.\d\d).*/', '$1', $n);

        if ( fmod( $n , $x ) <= 0 ){
            return number_format($n,2,".","");
        }else{
            return number_format( round(($n+$x/2)/$x)*$x , 2 , "." , "");       
        }
        
    }

    public function set_amount($n){
        return floatval(number_format($n,2,".",""));
    }

    public function interest_balance($loan_no,$five_days_int_cal,$int_cal_changed = 0){
        $bc = $this->sd['bc'];
        $int_bal = 0;
        $LOAN_DATA = $this->db->query("SELECT L.nmintrate, L.is_weelky_int_cal, L.old_bill_age, L.is_renew, C.customer_id, L.billno,L.bc,nicno,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.int_cal_changed,L.am_allow_frst_int,L.`int_paid_untill`,L.fmintrest FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `m_bill_type` B ON L.`billtype` = B.`billtype` WHERE L.loanno = '$loan_no' AND L.bc = '$bc' LIMIT 1 ")->row();

        $INT_BAL = $this->interest($loan_no,"r","",$LOAN_DATA,$five_days_int_cal) - $this->paid_interest($loan_no);

        if ($INT_BAL > 0){
            $int_bal = $INT_BAL;
        }
        
        return number_format($int_bal,2,".","");
    }

    
    public function get_total_due($loan_no){
        $bc = $this->sd['bc'];
        $Q1 = $this->db->query("SELECT ifnull(SUM(amount),0) AS `total_due` FROM `t_loantranse` WHERE loanno=$loan_no AND bc='$bc' AND transecode IN ('PD')");

        if ($Q1->num_rows() > 0){
            return floatval($Q1->row()->total_due);
        }else{
            return 0;
        }
    }

    public function get_int_balance($loan_no){
        $bc = $this->sd['bc'];
        $Q1 = $this->db->query("SELECT ifnull(SUM(amount),0) AS `int_bal` FROM `t_loantranse` WHERE loanno=$loan_no AND bc='$bc' AND transecode IN ('PP','PD')");

        if ($Q1->num_rows() > 0){
            return floatval($Q1->row()->int_bal);
        }else{
            return 0;
        }
    }

    public function paid_interest($loan_no){
        $bc = $this->sd['bc'];
        $Q1 = $this->db->query("SELECT IFNULL(SUM(amount)*-1,0) AS `paid_int` FROM `t_loantranse` WHERE bc = '$bc' AND  loanno = '$loan_no' AND `transecode` = 'PP'");

        if ($Q1->num_rows() > 0){
            return floatval($Q1->row()->paid_int);
        }else{
            return 0;
        }
    }

    public function customer_advance($customer_id,$billno){            
        $bc = $this->sd['bc'];        
        $Q = $this->db->query("SELECT ifnull(SUM(cr_amount) - SUM(dr_amount),0) AS `adv_bal` FROM t_loan_advance_customer WHERE bc = '$bc' /*AND client_id='$customer_id'*/ AND NOT is_delete = 1 AND billno='$billno'");
        $a['balance'] = 0;
        if ($Q->num_rows() > 0){$a['balance'] =  $Q->row()->adv_bal;; }
        return $a;
    }

    public function getMonthsCount($d1,$d2){

        $d1 = new DateTime($d1);
        $d2 = new DateTime($d2);

        $interval = $d2->diff($d1);
        $interval->format('%m months');
        
        $mc = $interval->m;
        if ($interval->d > 0){ $mc++; }
        
        return $mc;

    }
    

    public function OLD_LOAN_TO_NEW_LOAN_AM($loanno,$balance_amount,$current_gold_value,$tr_no,$billtype,$loan_amount,$_POST_,$module_status,$am_allow_frst_int = 1){

        /*echo $_POST['data']['loan_amount']."<br>";
        echo $_POST['data']['int_bal'];
        exit;*/

        $new_loan_no    = $this->max_no = $this->get_next_new_loan_number();        
        $dDate          = $this->sd['current_date'];
        $bc             = $this->sd['bc'];
        $oc             = $this->sd['oc'];
        $action_date    = date('Y-m-d H:i:s');
        $time           = $_POST['time'] = date('H:i:s');
        $current_gold_value = floatval(str_replace(",", "", $current_gold_value));

        $a['is_tfr'] = 0;

        $card_amount            = $_POST_['card_amount'] ;
        $advance_amount         = $_POST_['customer_advance'];
        $cash_amount            = $_POST_['cash_amount'];
        $extra_required_amount  = $_POST_['extra_required_amount'];
        $int_discount           = floatval( $_POST['int_discount'] );
        $obn                    = $_POST_['obn'];

        $is_renew               = 1;
        $old_bill_pawn_date     = $_POST_['data']['pDate'];
        $old_bill_redeem_int    = $_POST_['data']['int_bal'];

        $old_bill_age           = 0;// $this->db->query("SELECT DATEDIFF('$dDate','$old_bill_pawn_date') AS `old_bill_age`")->row()->old_bill_age;

        $this->load->model("t_new_pawn");
        $B = $this->t_new_pawn->setBillTypeValue($loan_amount,$current_gold_value,"return");
        
        $billtype = $B['bt_sum'][0]->billtype;
        $newprd = $B['bt_sum'][0]->period;
        $bt_letter= ""; //$B['bt_sum'][0]->letter;
        
        $billno   = $this->next_billno($bc,$billtype);

        $this->db->trans_begin();
        
        $B          = $this->get_bill_no_by_billtype($billtype,$bc);
        $billno     = $B['billno'];        
        $billtypeno = $B['billtypeno'];

        if ($module_status == "AM"){
            $int_cal_changed = "1";
            $a['is_tfr'] = 1;
        }else{
            $int_cal_changed = "`int_cal_changed`";
        }

        if (isset($_POST_['data']['first_week_int_charge'])){
            if ($_POST_['data']['first_week_int_charge'] == true){
                $cal_week_int = 0;
            }else{
                $cal_week_int = 1;
            }
        }else{
            $cal_week_int = 1;
        }


        $nla = $_POST_['nla'];
        $extra_required_amount = $_POST_['extra_required_amount'];        

        $int_ = round($_POST_['fmintrest']); //$this->cal_interest($nla,$_POST_,$_POST_['fmintrate']);


        if ($_POST['data']['is_weelky_int_cal'] == 1){
            $int_paid_untill = 7;

            $int_upto_date = " DATE_ADD('$dDate', INTERVAL $int_paid_untill DAY) ";

        }else{

            $old_bill_age = $this->db->query("SELECT DATEDIFF('$dDate','$old_bill_pawn_date') AS `old_bill_age`")->row()->old_bill_age;

            if ( $old_bill_age >= 15 ){
                $int_paid_untill = 30;
                $int_upto_date = " DATE_ADD('$dDate', INTERVAL $int_paid_untill DAY) ";
            }else{
                $int_paid_untill = 0;
                $int_upto_date = "  `int_paid_untill`  ";
            }


        }


        $Q  = $this->db->query("INSERT INTO `t_loan`        SELECT '',`bc`,'$billtype', '$billno' ,'$new_loan_no','$dDate',`cus_serno`,`cus_address`,DATE_ADD('$dDate', INTERVAL $newprd MONTH) AS `finaldate`,`period`,`totalweight`,'$current_gold_value', '$loan_amount' as `requiredamount`,`fmintrate`,


            '$int_' 


            AS `fmintrest`



            ,`nmintrate`,'P','0','0','0',`int_with_amt`,`dis_days`,'$int_discount','$is_renew','$obn','$oc','$action_date','' AS `audit`,'' AS `audit_no`,'' AS `audit_amount`,'' AS `audit_pkt_no`,'' AS `audit_memo`,'$bt_letter',`fmintrate2`,0,$int_cal_changed,'$billtypeno','$am_allow_frst_int','$cal_week_int',
            '$extra_required_amount',

            $int_upto_date

            , $old_bill_age , '$dDate' , '$old_bill_redeem_int' , `manual_billno` , 0 , '$time','0'

            FROM `t_loan` L WHERE L.`loanno` = '$loanno' LIMIT 1 ");
        

        $Q2 = $this->db->query("INSERT INTO `t_loanitems`   
            SELECT '','$new_loan_no',`bc`,'$billtype','$billno',`itemcode`,`cat_code`,`con`,`goldweight`,`pure_weight`,`qty`,`goldtype`,`quality`,GR.goldrate as `value`,(GR.`goldrate` * (LI.`pure_weight` / 8) * LI.`quality`) / 100 AS `goldvalue`,`audit_goldtype`,`audit_pure_weight`,`audit_comment`,`bulk_items`,`denci_weight`,`audit_overadvance`,`audit_goldweight`

            FROM `t_loanitems` LI 
            JOIN `r_gold_rate` GR ON LI.`goldtype` = GR.`id` 
            WHERE loanno = '$loanno'");
        
        


    //------ 2016-12-28 new discount for pawn int --------------


        if ( isset($_POST['int_discount']) && floatval($int_discount) > 0 ){

            $Q3 = $this->db->query("

                INSERT INTO `t_loantranse`

                SELECT  '','$new_loan_no',bc,'$billtype','$billno','$dDate','$int_discount' AS `amount` ,'0','ID', (   SELECT IFNULL(MAX(`transeno`)+1,1) AS `max_no` FROM `t_loantranse` WHERE bc = '$bc' AND `transecode` = 'ID') AS `transeno`,'0',  '0','0', '$action_date','0','','','0','0','0','$billtypeno'

                FROM `t_loan` 
                WHERE loanno = '$loanno' AND `status` = 'P' LIMIT 1 ");

        }


    //------ End 2016-12-28 new discount for pawn int ----------


        

        $Q3 = $this->db->query("INSERT INTO `t_loantranse`  


            SELECT '','$new_loan_no',bc,'$billtype','$billno','$dDate','$loan_amount' AS `amount`,pp_n_p_transno,transecode,
            (SELECT IFNULL(MAX(`transeno`)+1,1) AS `max_no` FROM `t_loantranse` WHERE bc = '$bc' AND `transecode` = 'P') AS `transeno`
            ,'0','0','0','$action_date','0',`onefive_day_int`,`onefive_days`, `card_amount`,`advance_amount`,`cash_amount`,'$billtypeno' 

            FROM `t_loantranse` 

            WHERE loanno = '$loanno' AND transecode = 'P' LIMIT 1 ");



        if ($int_ > 0){

            $Q5 = $this->db->query("INSERT INTO `t_loantranse`    

                SELECT '','$new_loan_no',bc,'$billtype','$billno','$dDate','$int_' AS `amount` ,'0','A',  
                (SELECT IFNULL(MAX(`transeno`)+1,1) AS `max_no` FROM `t_loantranse` WHERE bc = '$bc' AND `transecode` = 'A') AS `transeno`
                ,  '0',  '0',  '0',  '$action_date','1','','','$card_amount','$advance_amount','$cash_amount','$billtypeno' 

                FROM `t_loan` 

                WHERE loanno = '$loanno' AND `status` = 'P' LIMIT 1 ");

        }


        
        $this->loan_bill_int($new_loan_no,$bc,$billtype);


        // Accoun Update
        
        $_POST['hid'] = 0;
        $_POST['ln'] = $new_loan_no;
        $_POST['bt'] = $billtype;
        $_POST['bn'] = $billno;
        
        if (isset($_POST['ddate'])){
            $_POST['date'] = $_POST['ddate'];
        }else{
            $_POST['date'] = $_POST['data']['ddate'];
        }
        
        $_POST['requiredamount'] = $loan_amount;
        
        $qq = $this->db->query(" SELECT amount FROM t_loantranse WHERE bc = '$bc' AND transecode = 'A' AND loanno = '$new_loan_no' LIMIT 1 ");

        if ($qq->num_rows() > 0){
            $_POST['fmintrest'] = $qq->row()->amount;
        }else{
            $_POST['fmintrest'] = 0;
        }

        $account_update =   $this->renew_account_update_am(0);                
        
        if($account_update!=1){
            $a['s'] = 0;
            $a['m'] = "Invalid account entries";
            $this->db->trans_rollback();
            $a['s'] = "error";
            echo json_encode($a);
            exit;
        }else{
            $account_update =   $this->renew_account_update_am(1);
        }

        
        $Q4 = $this->db->query("UPDATE `t_loan` SET `status` = '$module_status', `old_o_new_billno` = '$billno' WHERE `bc` = '$bc' AND `loanno` = '$loanno' Limit 1");
        // Do update old loan status as '$module_status' 

        // Move old loan

        
        

        $redm_data['DDATE'] = $dDate;
        $redm_data['AMOUNT'] = $_POST['data']['loan_amount'];
        $redm_data['DISCOUNT'] = 0;
        $redm_data['R_INT'] = $_POST['data']['int_bal'];        

        $this->update_customer_pawn_details($_POST_['data']['hid_customer_serno'],'R',$billno);
        $this->move_loan_data_to_RE_FO($loanno,$module_status,$redm_data);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{            
            $this->db->trans_commit();
            $a['s']         = 1;
            $a['max_no']    = $this->getPartPayTransNo();
            $a['no']        = $tr_no;
            $a['loanno']    = $loanno;
            $a['new_loanno']= $new_loan_no;
            $a['int_tr_no'] = $_POST_['int_tr_no'];
            $this->utility->user_activity_log($module='TFR',$action='insert',$trans_code='1',$trans_no=$billno,$note='Old bill number '.$_POST['data']['billno']);
        }

        $a['xx'] = $int_cal_changed;

        echo json_encode($a);  
    }

    public function PREVIOUS_GOLD_OF_A_LOAN($loanno){
        return $this->db->query("SELECT L.`goldvalue` as `loan_gold_value` FROM `t_loan` L WHERE loanno = '$loanno' LIMIT 1")->row()->loan_gold_value;
    }

    public function CURRENT_GOLD_OF_A_LOAN($loanno){
        return $this->db->query("SELECT SUM((GR.`goldrate` * (LT.`pure_weight` / 8) * LT.`quality`) / 100)  AS `current_gold_value`
            FROM `t_loanitems` LT JOIN `r_gold_rate` GR ON LT.`goldtype` = GR.`id`
            WHERE LT.loanno = '$loanno'")->row()->current_gold_value;
    }

    public function get_pawn_articles($loanno){

        if (isset($_POST['loanno'])){
            $loanno = $_POST['loanno'];
        }else{
            $loanno = $loanno;
        }


        return $this->db->query("SELECT 
          I.`itemname` AS `itemcode`,  IC.`des` AS `cat_code`,  C.`des` AS `con`,  LI.goldweight,  LI.pure_weight,  LI.qty,
          GR.`goldcatagory` AS `goldtype`,  GQ.`code` AS `quality`,  LI.goldvalue,  LI.value FROM  `t_loanitems` LI 
          JOIN `r_items` I     ON LI.`itemcode` = I.`itemcode`   JOIN `r_itemcategory` IC 
          ON LI.`cat_code` = IC.`code`   JOIN `r_condition` C     ON LI.`con` = C.`code` 
          JOIN `r_gold_rate` GR     ON LI.`goldtype` = GR.`id`   JOIN `r_gold_quality` GQ 
          ON LI.`quality` = GQ.`rate` WHERE loanno = '$loanno' ")->result();       
    }

    public function getPartPayTransNo(){
        $bc = $this->sd['bc'];
        return $this->db->query("SELECT IFNULL(MAX(`transeno`)+1,1) AS `max_no` FROM `t_loantranse` WHERE bc = '$bc' AND `transecode` = 'A'")->row()->max_no;
    }

    public function move_loan_data_to_RE_FO($lnNo,$status,$redm_data=''){

        if ( is_array($lnNo) ){

            $lnNo_expled = implode(',', $lnNo);
            
            foreach ($lnNo as $value) {
                $data[]= array(
                    'loanno' => $value,
                    'status' =>$status ,
                    'action_date'=>date('Y-m-d h:i:s')
                );            
            }

            $Qry = ' `loanno` IN ('.$lnNo_expled.') ';


        }else{
            $Qry = " `loanno` = '$lnNo' ";
            $data[]= array('loanno' => $lnNo,'status' =>$status ,'action_date'=>date('Y-m-d h:i:s'));
        }        

        $this->db->trans_begin();
        
        // 2017-05-18 this code was modified to make RN and AM bills show as redeened bills
        // $this->db->query("INSERT INTO `t_loantranse_re_fo` SELECT * FROM `t_loantranse` WHERE $Qry ");
        
        // if ( strtoupper($this->sd['bc']) == strtoupper('tst') ){

        if ( $redm_data != '' ){

            $this->load->model("t_redeem");                

            $DDATE = $redm_data['DDATE'];
            $AMOUNT = ($redm_data['AMOUNT'] + $redm_data['R_INT']);
            $TRNO = $this->t_redeem->getRedeemTransNo();
            $DISCOUNT = $redm_data['DISCOUNT'];
            $R_INT = $redm_data['R_INT'];

            $this->db->query("INSERT INTO `t_loantranse_re_fo` 

                SELECT * FROM `t_loantranse` WHERE $Qry
                UNION ALL 
                (SELECT (`auto_no`+5),`loanno`,`bc`,`billtype`,`billno`,'$DDATE','$AMOUNT',`pp_n_p_transno`,'R','$TRNO','$DISCOUNT','$R_INT',`app_rec_id`,NOW(),0,0,0,0,0,0,`billtypeno` FROM `t_loantranse` WHERE $Qry LIMIT 1)
                ");


        }else{
            $this->db->query("INSERT INTO `t_loantranse_re_fo` SELECT * FROM `t_loantranse` WHERE $Qry ");
        }

        /*}else{
            $this->db->query("INSERT INTO `t_loantranse_re_fo` SELECT * FROM `t_loantranse` WHERE $Qry ");
        }*/

        // 2017-05-18 this code was modified to make RN and AM bills show as redeened bills

        $this->db->query("INSERT INTO `t_loanitems_re_fo` SELECT * FROM `t_loanitems` WHERE $Qry ");
        $this->db->query("INSERT INTO `t_loan_re_fo` SELECT * FROM `t_loan` WHERE $Qry ");

        if ($this->db->trans_status() === FALSE){
            $a['s'] =0;
            $this->db->trans_rollback();
        }else{
            $this->db->query("DELETE FROM `t_loantranse` WHERE $Qry ");
            $this->db->query("DELETE FROM `t_loanitems` WHERE $Qry ");
            $this->db->query("DELETE FROM `t_loan` WHERE $Qry ");

            if ($this->db->trans_status() === FALSE){
                $a['s'] =0;
                $this->db->trans_rollback();
            }else{
                $this->db->update_batch('t_loan_re_fo', $data,'loanno'); 
            }

            $this->db->trans_commit();
            $a['s'] =1; 
        }

        $a['n'] = $this->db->conn_id->errno; 
        $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno);
        $a['lnNoRm'] = is_array($lnNo) ? $lnNo_expled : $lnNo;
        
        return json_encode($a);

    }

    public function update_customer_pawn_details($customer_id,$s,$billno=""){

        $bc = $this->sd['bc'];

        $Q = $this->db->query("SELECT `customer_id` FROM `t_cus_pawn_details_sum` WHERE `customer_id` = '$customer_id' LIMIT 1");
        
        if ($Q->num_rows() == 0){
            $this->db->query("INSERT INTO t_cus_pawn_details_sum(`customer_id`,`pawnings`,`last_billno`,`last_show`,`last_bill_bc`) values('$customer_id',1,'$billno','".date('Y-m-d h:i:s')."','$bc')");
        }else{

            $Q2 = $this->db->query("SELECT IFNULL(MAX(`pawnings`)+1,1) as `P` , IFNULL(MAX(`redeemed`)+1,1) as `R` , IFNULL(MAX(`forfiedted`)+1,1) as `F` , IFNULL(MAX(`canceled`)+1,1) as `C` FROM `t_cus_pawn_details_sum` WHERE `customer_id` = '$customer_id' LIMIT 1");
            
            if ( $s == 'P' ){ $coloum = "pawnings";     $d = $Q2->row()->P;  }
            if ( $s == 'R' ){ $coloum = "redeemed";     $d = $Q2->row()->R;  }
            if ( $s == 'F' ){ $coloum = "forfiedted";   $d = $Q2->row()->F;  }            
            if ( $s == 'C' ){ $coloum = "canceled";     $d = $Q2->row()->C;  }

            if ( $s == 'P' ){
                $q = " `last_billno` = '$billno' ";
            }else{
                $q = " `last_billno` = `last_billno` ";
            }

            $this->db->query("UPDATE `t_cus_pawn_details_sum` SET `$coloum` = '$d',$q,`last_show` = '".date('Y-m-d h:i:s')."', `last_bill_bc` = '$bc' WHERE `customer_id` = '$customer_id' LIMIT 1");
        }

    }


    public function renew_account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 1);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',1);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => $_POST['time'],
            "trans_code" => 1,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no"    => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);

        $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');
        $redeem_interest    = $this->utility->get_default_acc('REDEEM_INTEREST');
        $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');
        
        if (isset($_POST['data']['amount'])){
            $paying_amount = $_POST['data']['amount'];
        }else{
            $paying_amount = 0;
        }


        if (isset($_POST['data']['loan_amount'])){
            $loan_amount = $_POST['data']['loan_amount'];
        }else{
            $loan_amount = 0;
        }


        
        $new_bill_value = floatval(str_replace(",","",$_POST['nla']));

        $this->account->set_value2("Part Payment", $paying_amount , "dr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"PP");
        
        if ( floatval(str_replace(",","",$_POST['int_balance'])) > 0 ){
            $this->account->set_value2("Redeem Interest", $_POST['int_balance'] , "cr", $redeem_interest,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"RI");
        }
        
        $this->account->set_value2("Pawning", $loan_amount , "cr", $pawn_stock ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"P");
        $this->account->set_value2("Pawning", $new_bill_value , "dr", $pawn_stock ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"P");

        if (isset($_POST['int_discount'])){

            if (floatval($_POST['int_discount']) > 0){

                //$pawn_int_discount  = $this->utility->get_default_acc('PAWN_INT_DISCOUNT');
                //$this->account->set_value2("Pawning Interest Discount", floatval($_POST['int_discount']), "dr", $pawn_int_discount,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"P");
                //$this->account->set_value2("Pawning Interest Discount", floatval($_POST['int_discount']), "cr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"P");        

            }

        }



        if($condition==0){

            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='1'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 1);
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }




    public function renew_account_update_am($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 1);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',1);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => $_POST['time'],
            "trans_code" => 1,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no"    => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);

        $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');
        $redeem_interest    = $this->utility->get_default_acc('REDEEM_INTEREST');
        $pawning_interest   = $this->utility->get_default_acc('PAWNING_INTEREST');
        $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');
        $advance_received   = $this->utility->get_default_acc('ADVANCE_RECEIVED');
        
        $new_bill_value = floatval(str_replace(",","",$_POST['nla']));
        
        if (isset($_POST['data']['loan_amount'])){
            $old_bill_value = $_POST['data']['loan_amount'];
        }elseif (isset($_POST['data']['data']['loan_amount'])) {
            $old_bill_value = $_POST['data']['data']['loan_amount'];
        }else{
            $old_bill_value = 0;
        }

        if (isset($_POST['data']['int_bal'])){            
            $old_bill_red_int = $_POST['data']['int_bal'];        
        }elseif (isset($_POST['data']['data']['int_bal'])) {            
            $old_bill_red_int = $_POST['data']['data']['int_bal'];        
        }else{            
            $old_bill_red_int = 0;        
        }

        
        if (isset($_POST['data']['customer_advance'])){            
            $customer_advance = $_POST['data']['customer_advance'];        
        }elseif (isset($_POST['data']['data']['customer_advance'])) {            
            $customer_advance = $_POST['data']['data']['customer_advance'];        
        }else{            
            $customer_advance = 0;        
        }


        $new_bill_pawn_int = $_POST['new_pawn_int'];

        if (isset($_POST['data']['amount'])){
            $paying_amount = $_POST['data']['amount'];
        }elseif (isset($_POST['data']['data']['amount'])) {
            $paying_amount = $_POST['data']['data']['amount'];
        }else{
            $paying_amount = 0;
        } 


        
        /*echo "UNREDEEM_ARTICLES Dr ".$new_bill_value."<br>";
        echo "ADVANCE_RECEIVED Dr ".$customer_advance."<br>";
        echo "UNREDEEM_ARTICLES Cr ".$old_bill_value."<br>";
        echo "REDEEM_INTEREST Cr ".$old_bill_red_int."<br>";
        echo "PAWNING_INTEREST Cr ".$new_bill_pawn_int."<br>";
        echo "CASH_IN_HAND Cr ".$paying_amount."<br>";
        exit;*/
        
        // echo "UNREDEEM_ARTICLES Dr ".$new_bill_value."<br>"; echo "UNREDEEM_ARTICLES Cr ".$old_bill_value."<br>"; echo "REDEEM_INTEREST   Cr ".$old_bill_red_int."<br>"; echo "PAWNING_INTEREST  Cr ".$new_bill_pawn_int."<br>"; echo "CASH IN HAND      Cr ".$paying_amount."<br>";
        
        $this->account->set_value2("Pawning", $new_bill_value , "dr", $pawn_stock ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"P");
        $this->account->set_value2("Pawning", $old_bill_value , "cr", $pawn_stock ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"P");
        
        if (floatval($old_bill_red_int) > 0){
            $this->account->set_value2("Redeem Interest", $old_bill_red_int , "cr", $redeem_interest ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"RI");
        }

        if (floatval($customer_advance) > 0){
            $this->account->set_value2("Advance Received", $customer_advance , "dr", $advance_received ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"ADV_R");
        }

        $this->account->set_value2("Pawning Interest", $new_bill_pawn_int , "cr", $pawning_interest ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"A");
        
        $this->account->set_value2("Increased Value", $paying_amount , "cr", $cash_book ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"P");

        if($condition==0){

            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='1'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 1);
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }


    public function get_pay_option($bc,$tr_no,$cus_serno,$card_amount, $advance_amount, $cash_amount,$trans_type){

        $a = "";

        if ($card_amount > 0){
            $a['card_details'] = $this->db->query("SELECT card_no,card_amount FROM `t_credit_card` WHERE bc = '$bc' AND trans_no = '$tr_no' LIMIT 1")->row();
        }

        if ($advance_amount > 0){
            $a['advance_amount'] = $this->db->query("SELECT cr_amount FROM `t_loan_advance_customer` WHERE bc = '$bc' AND trans_type = '$trans_type' AND trans_no = '$tr_no' LIMIT 1")->row();
        }

        return $a;
    }


    public function cr_advance_bal($cus_serno,$paying_amount,$tr_no,$trans_type,$billno=0){

        $bc = $this->sd['bc'];

        $Q = $this->db->query("SELECT trans_no,client_id, IF(SUM(cr_amount) - SUM(dr_amount) > 0,SUM(cr_amount) - SUM(dr_amount),0) AS `bal` FROM `t_loan_advance_customer` WHERE bc = '$bc' AND client_id = (SELECT customer_id FROM `m_customer` WHERE serno = '$cus_serno' LIMIT 1) GROUP BY trans_no HAVING SUM(cr_amount) - SUM(dr_amount) > 0 ORDER BY trans_no,`date`");

        $adv_data_array = array();

        foreach ($Q->result() as $R) {

            $amt = 0;

            if ($paying_amount > 0){

                if ($paying_amount > $R->bal){
                    $paying_amount -= $R->bal;
                    $amt = $R->bal;
                }else{
                    $amt = $paying_amount;                
                    $paying_amount = 0;
                }

                $adv_data_array[] = array(
                    "client_id" =>$R->client_id, 
                    "trans_type" =>$trans_type, 
                    "trans_no" =>$R->trans_no, 
                    "date" =>$_POST['ddate'], 
                    "cr_amount" =>0, 
                    "dr_amount" =>$amt, 
                    "note" =>"Advance amount settle", 
                    "is_post" =>"0", 
                    "is_delete" =>"0", 
                    "bc" =>$bc, 
                    "oc" =>$this->sd['oc'] ,
                    "billno" => $billno,
                    "time" => date('H:i:s')
                ); 
            }
        }

        if (isset($adv_data_array) and count($adv_data_array) > 0){
            $this->db->insert_batch("t_loan_advance_customer",$adv_data_array);
        }

    }

    public function add_credit_card_entry($cus_serno,$card_amount,$tr_no){
        $bc = $this->sd['bc'];
        $no = $this->db->query("SELECT IFNULL(MAX(NO)+1,1) AS `max_no` FROM `t_credit_card` WHERE bc = '$bc'")->row()->max_no;
        $card_data = array("no" =>  $no, "serno" => $cus_serno, "trans_no" => $tr_no, "trans_type" => "R", "card_amount" => $card_amount, "card_no" => $_POST['card_number'], "bc" => $bc );
        $this->db->insert("t_credit_card",$card_data);
    }


    public function get_bill_no_by_billtype($billtype,$bc){

        $cd = $this->sd['current_date'];       

        $Q = $this->db->query("SELECT (SELECT bc_no FROM `m_branches` WHERE bc = '$bc' LIMIT 1) AS `bc_no`,
(SELECT billtype_in_no FROM `m_bill_type` WHERE billtype = '$billtype' LIMIT 1) AS billtype_in_no,
(SELECT (IFNULL(MAX(billtypeno) + 1, 1)) AS `L1_max_no` FROM `loan_tblz_union` L  WHERE L.bc = '$bc' AND L.`billtype` = '$billtype') AS `billno`")->row();

        
        $a['billno']     = $Q->bc_no.$Q->billtype_in_no.$Q->billno;
        $a['billtypeno'] = $Q->billno;

        return $a;    
    }

    private function set_date_code($current_date){
        $datetime1 = date_create('1900-01-01');
        $datetime2 = date_create($current_date);
        $interval  = date_diff($datetime1, $datetime2);
        return intval($interval->format('%a')) - 42000;
    }


    public function next_billno($bc,$billtype){

        return "";

        /*return $this->db->query("SELECT IF (IFNULL(MAX(billno)+1,1) > (SELECT IFNULL(MAX(billno)+1,1) AS `billno` FROM `t_loan_re_fo` WHERE bc = '$bc' AND `billtype` = '$billtype') , IFNULL(MAX(billno)+1,1),(SELECT IFNULL(MAX(billno)+1,1) AS `billno` FROM `t_loan_re_fo` WHERE bc = '$bc' AND `billtype` = '$billtype') ) AS `billno` FROM `t_loan` WHERE bc = '$bc' AND `billtype` = '$billtype'")->row()->billno;*/

    }


    public function advance_max_no(){
        return $this->db->query("SELECT IFNULL(MAX(`trans_no`)+1 ,1) AS `max_no` FROM `t_loan_advance_customer`")->row()->max_no;
    }

    

























































    public function add_customer_advance_amount(){       

        $tr_no = $this->max_no = $this->advance_max_no();

        $_PST_A['client_id']    = $_POST['customer_id'];
        $_PST_A['trans_type']   = "ADV_P";
        $_PST_A['trans_no']     = $tr_no;
        $_PST_A['date']         = $_POST['ddate'];
        $_PST_A['time']         = $_POST['time'];
        $_PST_A['dr_amount']    = 0;
        $_PST_A['cr_amount']    = $_POST['paying_amount'] = $_POST['cr_amount'];        
        $_PST_A['note']         = "Customer advance payment";
        $_PST_A['bc']           = $this->sd['bc'];
        $_PST_A['oc']           = $this->sd['oc'];
        $_PST_A['billno']       = $_POST['bn'] = $_POST['billno'];
        $_POST['ln']            = $_POST['loanno'];
        $_POST['bt']            = $_POST['billtype'];

        // customer advance Account Update here

        $_POST['hid']       = "";

        $account_update_int = $this->account_update_advance_receving(0);                                

        if($account_update_int!=1){
            $a['s'] = 0; $a['m'] = "Invalid account entries"; $this->db->trans_rollback(); $a['s'] = "error";
            echo json_encode($a);
            exit;
        }else{
            $account_update_int =   $this->account_update_advance_receving(1);
        }
        
        $this->db->insert("t_loan_advance_customer",$_PST_A);
        $this->utility->user_activity_log($module='Customer Advance',$action='insert',$trans_code='24',$trans_no=$tr_no,$note=' adv for bill number '.$_PST_A['billno']);

        $a['action']    = "advance_dr";
        $a['max_no']    = $this->advance_max_no();
        $a['no']        = $tr_no;
        $a['loanno']    = $_POST['loanno'];

        return $a;

    }

    public function account_update_advance_receving($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 24);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',24);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['ddate'],
            "time" => $_POST['time'],
            "trans_code" => 24,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);

        $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');
        $ADVANCE_RECEIVED   = $this->utility->get_default_acc('ADVANCE_RECEIVED');

        $this->account->set_value2("Advance Received", $_POST['paying_amount'], "dr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"ADV_R");                
        


        $this->account->set_value2("Advance Received", $_POST['paying_amount'], "cr", $ADVANCE_RECEIVED,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"ADV_R");

        if($condition==0){

            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='24'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 24);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }
















































    public function check_availability_of_capital_deduct_bal($bal_amt,$billno,$loan_amount){

        return true;

        // remove at 2017-02-20

        /*if ($loan_amount >= 20000){
            
            if ( $bal_amt >= (($loan_amount * 5)/100) ){
                return true;
            }else{
                return false;
            }

        }else{
            
            if ($bal_amt >= 1000){
                return true;
            }else{
                return false;
            }
            
        }*/

    }

    public function claim_int(){

        $tr_no = $this->getPartPayTransNo();

        $_PST['loanno']    = $_POST['loanno'];
        $_PST['bc']        = $this->sd['bc']; 
        $_PST['billtype']  = $_POST['billtype']; 
        $_PST['billno']    = $_POST['billno']; 
        $_PST['ddate']     = $_POST['ddate']; 
        $_PST['amount']    = $_POST['amount']; 
        $_PST['transecode']= "A"; 
        $_PST['transeno']  = $tr_no; 
        $_PST['discount']  = 0;  
        $_PST['is_pawn_int'] = 0;

        $_PST['onefive_day_int']    = $_POST['onefive_day_int'];
        $_PST['onefive_days']       = $_POST['onefive_days'];

        $cus_serno      = $_POST['data']['hid_customer_serno'];
        unset($_POST['data']['hid_customer_serno'],$_POST['onefive_day_int'],$_POST['onefive_days']);
        
        if ( $_POST['claim_by'] == "advance" ){

            if ($cus_serno == ""){echo "invalid cus_serno "; exit; }
            $_PST['advance_amount']    =    $_POST['amount'];
            $this->cr_advance_bal($cus_serno,$_PST['advance_amount'],$tr_no,"ADV_R",$_PST['billno']);
        }        

        if ( floatval($_POST['data']['card_amount']) > 0 ){
            if ($cus_serno == ""){echo "invalid cus_serno "; exit; }
            $_PST['card_amount']    = $_POST['data']['card_amount'];
            $this->add_credit_card_entry($cus_serno,$_PST['card_amount'],$tr_no);
        }        

        if ( floatval($_POST['data']['cash_amount']) > 0 ){
            $_PST['cash_amount']    = $_POST['data']['cash_amount']; 
        }

        if ($_POST['amount'] > 0){
            $Q1 = $this->db->insert("t_loantranse",$_PST); // LOANTRANSE TABLE
            $this->db->set("int_paid_untill",$_POST['new_int_paid_untill'])->where(array("loanno"=>$_PST['loanno'],"billno"=>$_PST['billno']))->limit(1)->update("t_loan");
        }

        unset($_PST);        
        
        $this->max_no = $tr_no;        
        $_POST['paying_amount'] = $_POST['amount'];
        $_POST['hid'] = 0;

        $_POST['ln'] = $_POST['loanno'];
        $_POST['bt'] = $_POST['billtype'];
        $_POST['bn'] = $_POST['billno'];
        
        if (isset($_POST['ddate'])){
            $_POST['date'] = $_POST['ddate'];
        }else{
            $_POST['date'] = $_POST['data']['ddate'];
        }

        if (floatval($_POST['paying_amount']) > 0){
            $account_update_int =   $this->account_update_int(0);                                
            
            if($account_update_int!=1){
                $a['s'] = 0; $a['m'] = "Invalid account entries"; $this->db->trans_rollback(); $a['s'] = "error";
                echo json_encode($a);
                exit;
            }else{
                $account_update_int =   $this->account_update_int(1);
            }
        }        

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{            
            $this->db->trans_commit();
            $a['s']         = 1;
            $a['max_no']    = $this->getPartPayTransNo();
            $a['no']        = $tr_no;
            $a['loanno']    = $_POST['loanno'];
        }        
        
        return $a;        

    }




    public function account_update_int($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 4);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',4);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "trans_code" => 4,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);

        $cash_book          = $this->utility->get_acc('cash_acc',$this->sd['bc']);
        $interest_received  = $this->utility->get_acc('interest_acc',$this->sd['bc']);        

        $this->account->set_value2("Interest Received", $_POST['paying_amount'], "dr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"A");                
        $this->account->set_value2("Interest Received", $_POST['paying_amount'], "cr", $interest_received,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"A");

        if($condition==0){

            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='4'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 4);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }

    public function get_int_paid_till_date($last_int_paid_date,$is_weelky,$is_first_cal = true,$num_of_days_passed = 0,$num_of_months,$pawn_date,$five_days_int_cal = 0,$is_renew_bill = 0){

        $current_date       = $this->sd['current_date'];
        $sql1 = "SELECT 12 * (YEAR ('$current_date') - YEAR ('$last_int_paid_date')) + (MONTH('$current_date') - MONTH('$last_int_paid_date')) AS months";
        //$x['sql1'] = $sql1;
        $num_of_months      = floatval( $this->db->query($sql1)->row()->months );
        $days_till_last_pay = $this->db->query("SELECT DATEDIFF('$last_int_paid_date','$pawn_date') as `days_till_last_pay` ")->row()->days_till_last_pay;
        //$x['days_till_last_pay'] = 
        $days_till_last_pay += 1;

        $days_4m_pawn_date  = $this->db->query("SELECT DATEDIFF('$current_date','$pawn_date') as `days_4m_pawn_date` ")->row()->days_4m_pawn_date;
        //$x['days_4m_pawn_date'] = 
        $days_4m_pawn_date += 1;

        /*---------------*/

        $pd = $pawn_date;
        $lp = $last_int_paid_date;
        $cd = $current_date;

        /*---------------*/


        // $days_add, this var must get by billtype if number of days changes

        if ( $is_renew_bill == 1 ){ // this section added on 2017-02-20 

            $x['section']   = 3;    // remove

            if ( ($current_date - $pawn_date) <= 7 ){

                $days_add = 23;

            }else{

                $days_add = 30;

            }


        }else if ($is_weelky == 1){

            if ($is_first_cal){ // most use in new pawning
                $x['section']   = 1;    // remove
                $last_int_paid_date = $last_int_paid_date;
                $days_add       = 7; 
                $n_days         = 1;
            }else{

                $n_days         = 0;

                $Q_DATA['q_data'] = $this->db->query("SELECT 

                    @x := DATEDIFF('$cd','$pd')   AS `days_4m_pawn_date` ,
                    @y := DATEDIFF('$cd','$lp')+1  AS `days_afr_lst_int_date` ,
                    @z := DATEDIFF('$lp','$pd')+1   AS `days_till_last_pay` ,

                    @months := IF (@x <= 30,  /*check here*/
                    0,            
                    IF (@z <= 30,            
                    IF (@x <= 30,1,IF (@x % 30 > 0,(FLOOR( @x / 30 )),(FLOOR( @y / 30 )))),    
                    IF (@y % 30 > 0,(FLOOR( @y / 30 )+1),(FLOOR( @y / 30 )))
                    )            

                    ) AS `months_to_be_added`,  

                    @remain_days := @y % 30,  
                    FLOOR(IF (@x <= 30, /* check here */ 23,IF (@z < 30,(@months * 30) + 23,@months * 30))) AS `m`")->row();

                $x['num_of_months'] = $num_of_months = $Q_DATA['q_data']->months_to_be_added ;

                $days_add = $Q_DATA['q_data']->m;

                if ( $five_days_int_cal == 1 ){

                    $days_add -= 30;                    
                    $days_add += $_POST['no_of_int_cal_days'];

                }
                

            }

        }else{

            $days_add = 30;           

            if ($is_first_cal){
                $n_days = 1;
            }else{
                $n_days = 0;
            }

        }

        $sql = $x['sql'] = "SELECT ADDDATE('$last_int_paid_date', INTERVAL ($days_add) DAY) AS `till_date`";

        $x['till_date']     = $this->db->query($sql)->row()->till_date;
        
        
        return $x;        

    }

    public function loan_bill_int($loan_no,$bc,$billtype){

        $QQ  = $this->db->query("INSERT INTO t_loan_bill_int SELECT '','$loan_no',billtype,period,day_from,day_to AS `first_int_charge_period`,rate,  allow_pre_int,  '$bc'FROM `r_bill_type_sum` BTS JOIN `r_bill_type_det` BTD ON BTS.`no` = BTD.`no` WHERE BTS.bc = '$bc' AND BTS.billtype = '$billtype' ORDER BY CAST(day_from AS UNSIGNED) ASC ");

        if (!$QQ){
            echo "Table 't_loan_bill_int' entry not found";
            $this->db->trans_rollback();
            exit;
        }

    }

    public function cal_interest($loan_amount,$LOAN_DATA,$int_rate,$if_fm_int = false){

        if (is_array($LOAN_DATA)){
            $customer_id = $LOAN_DATA['customer_id'];
            $billno = $LOAN_DATA['billno'];
            $fmintrest = $LOAN_DATA['fmintrest'];
        }else{
            $customer_id = $LOAN_DATA->customer_id;
            $billno      = $LOAN_DATA->billno;
            $fmintrest   = $LOAN_DATA->fmintrest;
        }        
        

        /*------ remove comment if interest balance must deduct from customer advance amount -----*/
        $adv_balance    = 0;
            //$adv_balance    = $this->calculate->customer_advance($customer_id,$billno);
            //$adv_balance    = $adv_balance['balance'];        
        /* ------------------------------------------------------------------------------------ */

        $x = $int_rate/100;
        $x = 1 - $x;

        $l = $loan_amount;
        //$l = $loan_amount - ($fmintrest);    // Remove this line if interest calculate with first int
        //$l = $loan_amount - $adv_balance;    // Remove this line if interest calculate deduct with advance

        // $x          = $l / $x;
        // $interest   = $x - $l;
        // $interest   = ($l * $int_rate) / 100; // New calcuation changed at 2016-08-24
        
        if ($if_fm_int){
            $interest   = ($l * $int_rate) / 100 ;//  New calcuation changed at 2016-09-06
        }else{
            $l          = $loan_amount - $adv_balance;
            $interest   = ($l * $int_rate) / 100 ;//  New calcuation changed at 2016-09-06
        }

        return $this->set_amount($interest);
    }


    public function get_next_new_loan_number(){
        return $this->db->query("SELECT IF (IFNULL(MAX(a.`loanno`) + 1, 1) > (SELECT IFNULL(MAX(loanno) + 1, 1) AS `B` FROM `t_loan_re_fo`), IFNULL(MAX(a.`loanno`) + 1, 1), (SELECT IFNULL(MAX(loanno) + 1, 1) AS `B` FROM `t_loan_re_fo`) ) AS `loan_no` FROM `t_loan` a ")->row()->loan_no;
    }



    public function customer_settle_by_advance($advance_dr_amt,$billno,$customer_id){

        $Q = $this->db->query("SELECT T.`trans_no`,SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM `t_loan_advance_customer` T WHERE T.`client_id` = '$customer_id' AND T.`billno` = '$billno'GROUP BY T.`trans_no` HAVING SUM(cr_amount) - SUM(dr_amount) > 0 ");

        if ($Q->num_rows() > 0){

            foreach($Q->result() as $R){

                if ($advance_dr_amt > 0){

                    if ($advance_dr_amt > $R->trans_no){
                        $settle_amount = $R->bal;                    
                    }else{
                        $settle_amount = $advance_dr_amt;                    
                    }

                    $dr_data_arr[] = array(
                        'client_id'     => $customer_id,
                        'trans_type'    => "ADV_R",
                        'trans_no'      => $R->trans_no,
                        'date'          => $_POST['ddate'],
                        'dr_amount'     => $settle_amount,
                        'cr_amount'     => 0,        
                        'note'          => "Customer advance settlement",
                        'bc'            => $this->sd['bc'],
                        'oc'            => $this->sd['oc'],
                        'billno'        => $_POST['billno']                                    
                    );

                }

                $advance_dr_amt -= $settle_amount;

            }

        }

        if (isset($dr_data_arr)){
            $this->db->insert_batch("t_loan_advance_customer",$dr_data_arr);
        }

    }































}