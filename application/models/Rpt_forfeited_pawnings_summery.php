<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_forfeited_pawnings_summery extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function PDF_report($_POST_){

		ini_set("max_execution_time",600);
		
		if (isset($_POST['bc_n'])){
	        if ( $_POST['bc_n'] == "" ){        
	            $bc = "";
	        }        
	    }else{

	        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
	            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
	        }

	        $bc = implode(',', $bc_ar);

	        if ($bc == ""){
	            $bc   = "";
	        }else{
	            $bc   = " FS.bc IN ($bc) AND  ";
	        }
	    }

    // r_billtype filteration 
    $billtype = "";

    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }

		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];

		$Q = $this->db->query("SELECT billtype, SUM(nop) as `nop` , SUM(list_weight) as `list_weight` ,  SUM(amount) as `amount` , SUM(int_up_to_date) as `accrued_int` ,  SUM(other_cost) as `other_cost` , bc
FROM (

SELECT L.billtype AS `billtype`,COUNT(FD.`billno`) AS `nop`,SUM(LI.pure_weight) AS `list_weight`,SUM(L.`requiredamount`) AS `amount`,0 AS `int_up_to_date`,SUM(IFNULL((SELECT SUM(D.document_charge) AS `document_charge` FROM `t_remind_letter_det` D WHERE D.`billno` = FD.`billno` AND D.`bc` = FS.`bc` AND D.`ignore_doc_charge` = 0),0)) AS `other_cost`,B.`name` AS bc
FROM t_forfeit_sum FS 
JOIN t_forfeit_det FD ON FS.`no` = FD.`no` 
JOIN (	SELECT SUM(l.`requiredamount`) AS requiredamount , l.`billtype`,l.`ddate`,l.`billno`,l.`bc` FROM t_loan_re_fo l GROUP BY l.`billno`) L ON FD.`billno` = L.billno 
JOIN (	SELECT li.`billno`,li.`itemcode`,i.`itemname`,li.`qty`,SUM(li.`pure_weight`) AS `pure_weight`,li.`value`
		FROM t_loanitems_re_fo li 
		JOIN r_items i ON li.`itemcode` = i.`itemcode` 
		GROUP BY li.`billno` ) LI ON FD.`billno` = LI.billno 
JOIN m_branches B ON L.bc = B.`bc`
WHERE $bc FS.`date` BETWEEN '$fd' AND '$td'
GROUP BY L.bc, L.billtype

UNION ALL

SELECT L.billtype,'','','', SUM(acurd_int_cal (FS.`bc`, FD.billno, FS.`date`)) AS `int_up_to_date`,'',B.`name` AS bc
FROM t_forfeit_sum FS 
JOIN t_forfeit_det FD ON FS.`no` = FD.`no` 
JOIN (	SELECT * FROM t_loan_re_fo l GROUP BY l.`billno`) L ON FD.`billno` = L.billno 
JOIN (	SELECT li.`billno`,li.`itemcode`,i.`itemname`,li.`qty`,li.`pure_weight`,li.`value` 
	FROM t_loanitems_re_fo li 
	JOIN r_items i ON li.`itemcode` = i.`itemcode`
	GROUP BY li.billno ) LI ON FD.`billno` = LI.billno 
JOIN m_branches B ON L.bc = B.`bc` 
WHERE $bc FS.`date` BETWEEN '$fd' AND '$td' 
GROUP BY L.billtype ) a GROUP BY a.bc,a.billtype ORDER BY bc,billtype ");

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;

			$this->load->view($_POST['by'].'_'.'pdf',$r_data);
		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}