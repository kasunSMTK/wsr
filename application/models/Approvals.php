<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class approvals extends CI_Model {
    
    private $sd;
    private $request_id;    
    private $max_no;
    private $bc_glob;
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();     
    }
    
    public function base_details(){     
        $a['a'] = 1;        
        return $a;
    }


    public function add_approval(){

        if (isset($_POST['billno'])){
            $last_issued_billno = $_POST['billno'];
        }else{
            $last_issued_billno = "";
        }

        $date_time = date("Y-m-d H:i:s");

        $_POST['auto_no'] = "";
        $_POST['record_status'] = "A";
        $_POST['bc'] = $this->sd['bc'];
        $_POST['requested_by'] = $this->sd['oc'];
        $_POST['requested_datetime'] = $date_time;        
        $_POST['approved_amount'] = $_POST['requested_amount'] ;
        $_POST['approved_by'] = $this->sd['oc'];
        $_POST['approved_datetime'] = $date_time;
        $_POST['action_date'] = $date_time;


        if ( isset( $_POST['discount_reason'] ) ){
            $_POST['over_adv_item_list'] = $_POST['discount_reason'];
            unset($_POST['discount_reason']);
        }

        if (isset($_POST['di_billno'])){
            $_POST['di_billno'] = $_POST['di_billno'];
        }else{
            $_POST['di_billno'] = '';
        }

        $_POST['nno'] = $this->app_max_no();

        $q = $this->db->select("loanno")->where(array("bc" => $this->sd['bc'], "loanno" => $_POST['loanno'], "record_status" => "W"))->limit(1)->get("t_approval")->num_rows();
        
        if ($q > 0){
            $this->db->where(array("bc" => $this->sd['bc'], "loanno" => $_POST['loanno'], "record_status" => "W"))->delete("t_approval");    
        }

        $Q_bn = $this->db->select("billno")->where("bc",$this->sd['bc'])->get("t_last_bill_update");

        if ($Q_bn->num_rows() > 0){
            $bn = $Q_bn->row()->billno;
        }else{
            $bn = "";
        }


        /*$DATA['bc']                 = 'HO';
        $DATA['action_taken_by']    = $this->sd['oc'];
        $DATA['related_model']      = "extra_amount_required";
        $DATA['action_window']      = "appvl_over_advance";
        $DATA['noti_description']   = "Extra loan amount approval request sent by ".$this->sd['bc'];
        $DATA['view_to_HO']         = 1;
        $DATA['view_to_own_bc']     = 0;

        $this->utility->add_notification($DATA);*/




        if ($last_issued_billno == $bn ){

            unset($_POST['billno'],$_POST['time']);
            
            if ($this->db->insert("t_approval",$_POST)){
                $a['s'] = 1;
                $a['nno'] = $_POST['nno'];
            }else{
                $a['s'] = 0;                        
                $this->load->model("utility");
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Approval request already sent for this bill type and bill number","");
            }

        }else{

            unset($_POST['time'],$_POST['refundable_int'],$_POST['billcode']);

            if ($this->db->insert("t_approval",$_POST)){
                $a['s'] = 1;
                $a['nno'] = intval($_POST['nno']);
                $this->utility->user_activity_log($module='discount(redeem)',$action='request',$trans_code='',$trans_no=$_POST['nno'],$note='');

                $a['app_auto_no'] = $this->db->insert_id();
            }else{
                $a['s'] = 2;
            }

        }
    
        echo json_encode($a);

    }

    public function get_approval_response(){

        $bc = $this->sd['bc'];
        $no = $this->request_id = $_POST['loanno'];

        $Q = $this->db->query("SELECT `auto_no`,`record_status`,`approved_amount`,`request_type` FROM `t_approval` 
        WHERE bc = '$bc' AND `nno` = '$no' AND (`record_status` = 'A' OR `record_status` = 'R') LIMIT 1 ");

        if ($Q->num_rows() > 0){
            $a['approval_id']     = $Q->row()->auto_no;
            $a['approval_status'] = $Q->row()->record_status;
            $a['approved_amount'] = $Q->row()->approved_amount;
            $a['request_type']    = $Q->row()->request_type;            

            if ($a['approval_status'] == "A"){
                
                $this->db->query("INSERT INTO `t_approval_history` SELECT * FROM `t_approval` WHERE `bc` = '$bc' AND `nno` = '$no' AND `record_status` = 'A' LIMIT 1");

                $this->db->query("DELETE FROM `t_approval` WHERE `bc` = '$bc' AND `nno` = '$no' AND `record_status` = 'A' LIMIT 1");
            }

        }else{            
            $a['approval_status'] = 0;
            $a['request_id'] = $this->request_id;
        }
        
        echo json_encode($a);        

    }
        
    public function getApprovalRequests(){
        
        if ($_POST['page_name'] == "discount_approval"){            
            
            $Q = $this->db->query("
                SELECT 


                *, B.`name` AS `bc_name`, U.`discription` AS `requ_by` ,

                DA.`nno`,
                DA.requested_amount

                FROM t_approval DA 
                JOIN m_branches B ON DA.`bc` = B.`bc` 
                JOIN u_users U ON DA.`requested_by` = U.`cCode` 
                WHERE DA.`request_type` = 'DI' AND DA.`record_status` = 'W' AND NOW() < (`requested_datetime` + INTERVAL 8 MINUTE) 
                ORDER BY DA.`action_date` DESC ");
            
            if ($Q->num_rows() > 0){
                $a['discount'] = $Q->result();
            }else{
                $a['discount'] = 0;       
            }                        
            
            echo json_encode($a);            
            exit;             
        }

        



























































        if ($_POST['page_name'] == "fund_transfers"){

            $bc = $this->sd['bc'];
            $T = "";

            /*$Q = $this->db->query(" SELECT S.no, S.`requ_initi_date_time`, S.`ref_no`, CRD.`bank` as `bank_acc`, S.`comment` AS `memo`, CRD.`cheque_no` as `chq_no`, CRD.`amount`, ACC.description, BC.name, IFNULL(S.`status`, 0) AS `status`, S.comment, S.`to_bc`, B.`name` AS `bc_name`, S.`requ_initi_date_time`, S.`no` AS `requ_no`

FROM `t_fund_transfer_sum` S 

JOIN `t_fnd_trnsfr_rqsts_chq_det`   CRD ON S.`no`   = CRD.`no`  AND S.`requ_initi_bc`   = CRD.`bc`


JOIN(SELECT * FROM m_account WHERE is_bank_acc = 1) ACC ON CRD.`bank` = ACC.code 
JOIN(SELECT * FROM m_branches)      BC ON S.`requ_initi_bc` = BC.bc 
JOIN m_branches             B ON S.`to_bc` = B.`bc`

WHERE S.`transfer_type` = 'cheque_withdraw' AND S.`status` = 1
ORDER BY S.`requ_initi_bc` , S.`no` , CRD.`cheque_no`   ");*/

        
        $Q = $this->db->query("(SELECT S.no, S.`requ_initi_date_time`, S.`ref_no`, CRD.`bank` AS `bank_acc`, S.`comment` AS `memo`, CRD.`cheque_no` AS `chq_no`, CRD.`amount`, ACC.description, BC.name, IFNULL(S.`status`, 0) AS `status`, S.comment, S.`to_bc`, B.`name` AS `bc_name`, S.`requ_initi_date_time`, S.`no` AS `requ_no`,S.`fund_tr_opt_no`
FROM `t_fund_transfer_sum` S 
JOIN `t_fnd_trnsfr_rqsts_chq_det`   CRD ON S.`no`   = CRD.`no`  AND S.`requ_initi_bc`   = CRD.`bc`
JOIN(SELECT * FROM m_account WHERE is_bank_acc = 1) ACC ON CRD.`bank` = ACC.code 
JOIN(SELECT * FROM m_branches)      BC ON S.`requ_initi_bc` = BC.bc 
JOIN m_branches             B ON S.`to_bc` = B.`bc`

WHERE S.`transfer_type` = 'cheque_withdraw' AND S.`status` = 1
ORDER BY S.`requ_initi_bc` , S.`no` , CRD.`cheque_no`   )

UNION ALL

(SELECT 

S.no, S.`requ_initi_date_time`, S.`ref_no`, '' AS `bank_acc`, S.`comment` AS `memo`, '' AS `chq_no`, S.`amount`, CONCAT('From ', accA.`description` , ' to ' , accB.`description`  ) AS description, B.name, IFNULL(S.`status`, 0) AS `status`, S.comment, '' AS `to_bc`, B.name AS `bc_name`, S.`requ_initi_date_time`, S.`no` AS `requ_no`,S.`fund_tr_opt_no`

FROM `t_fund_transfer_sum` S 

JOIN m_account accA ON S.`from_acc` = accA.`code`
JOIN m_account accB ON S.`to_acc` = accB.`code`
JOIN m_branches             B ON S.from_bc = B.`bc`

WHERE S.`fund_tr_opt_no` = 5 AND `status` = 1)");            


            if ($Q->num_rows() > 0){
                
                $n = 0;
                
                foreach($Q->result() as $r){
                    
                    $action = "";                    

                    
                    if ($r->fund_tr_opt_no == 3){
                        $action   .= "<input type='button' value='Approve Request' class='approve_chq_wdr_rqu btn_reg_hover_highlight' no='".$r->requ_no."' bc='".$r->to_bc."'>";
                    }

                    if ($r->fund_tr_opt_no == 5){
                        $action   .= "<input type='button' value='Approve Request' class='approve_BtoB_rqu btn_reg_hover_highlight' no='".$r->requ_no."' bc='".$r->to_bc."'>";
                    }

                    $action   .= "<input type='button' value='Reject' class='reject_chq_wdr_rqu btn_reg_reject_hover_highlight' no='".$r->requ_no."' bc='".$r->to_bc."' tr_type='".$r->fund_tr_opt_no."'>";
                    $trf_typ   = 'unknown';
                    
                    if ($r->fund_tr_opt_no == 3){ $trf_typ = 'Cheque Withdraw'; }
                    if ($r->fund_tr_opt_no == 5){ $trf_typ = 'Bank to Bank'; }


                    $T .= " <tr>          
                                <td>".($n+1)."</td>
                                <td>".$trf_typ."</td>
                                <td>".$r->requ_no."</td>
                                <td>".$r->bc_name."</td>
                                <td>".$r->description." 
                                    <input type='hidden' class='bank_code' value='".$r->bank_acc."'>
                                    <input type='hidden' class='chq_no'     value='".$r->chq_no."'>
                                    <input type='hidden' class='amount'    value='".$r->amount."'>
                                </td>            
                                <td align='center'>".$r->chq_no."</td>
                                <td align='right'>".number_format($r->amount,2)."</td>                                                                
                                <td>".$r->comment."</td>
                                <td aling='center'>".$r->requ_initi_date_time."</td>
                                <td align='right'>$action</td>            
                            </tr>";

                    $n++;
                }

                $T .="<input type='hidden' id='chq_num_rows' value='".$n."'>";

            }else{

            $T .= ' <div style="height:24px">
                        <div class="text_box_holder_commen" style="width:100%;float: left;border-top: none;">
                            <span>No cheques added for this branch, Please contact head office</span>                        
                        </div>
                    </div>';

            }

            if ($Q->num_rows() > 0){
                $a['data'] = $T;
            }else{
                $a['data'] = 0;       
            }                        
            
            echo json_encode($a);            
            exit;             
        }

































































        if ($_POST['page_name'] == "fund_transfers_approved"){

            $bc = $this->sd['bc'];
            $T = "";

            $Q = $this->db->query(" 

SELECT * FROM (

(SELECT S.no, S.`requ_initi_date_time`, S.`ref_no`, CRD.`bank` as `bank_acc`, S.`comment` AS `memo`, CRD.`cheque_no` as `chq_no`, CRD.`amount`, ACC.description, BC.name, IFNULL(S.`status`, 0) AS `status`, S.comment, S.`to_bc`, B.`name` AS `bc_name`, S.`no` AS `requ_no`,S.`fund_tr_opt_no`

FROM `t_fund_transfer_sum` S 

JOIN `t_fnd_trnsfr_rqsts_chq_det`   CRD ON S.`no`   = CRD.`no`  AND S.`requ_initi_bc`   = CRD.`bc`


JOIN(SELECT * FROM m_account WHERE is_bank_acc = 1) ACC ON CRD.`bank` = ACC.code 
JOIN(SELECT * FROM m_branches)      BC ON S.`requ_initi_bc` = BC.bc 
JOIN m_branches             B ON S.`to_bc` = B.`bc`

WHERE S.`transfer_type` = 'cheque_withdraw' AND S.`status` = 10
ORDER BY S.`requ_initi_bc` , S.`no` , CRD.`cheque_no`)

union all


(SELECT 

S.no, S.`requ_initi_date_time`, S.`ref_no`, '' AS `bank_acc`, S.`comment` AS `memo`, '' AS `chq_no`, S.`amount`, CONCAT('From ', accA.`description` , ' to ' , accB.`description`  ) AS description, B.name, IFNULL(S.`status`, 0) AS `status`, S.comment, '' AS `to_bc`, B.name AS `bc_name`, S.`no` AS `requ_no`,S.`fund_tr_opt_no`

FROM `t_fund_transfer_sum` S 

JOIN m_account accA ON S.`from_acc` = accA.`code`
JOIN m_account accB ON S.`to_acc` = accB.`code`
JOIN m_branches             B ON S.from_bc = B.`bc`

WHERE S.`fund_tr_opt_no` = 5 AND `status` = 10)

) a     ORDER BY a.bc_name


   ");

            if ($Q->num_rows() > 0){
                
                $n = 0;
                $s = '';
                $cls = '';

                $title_print = false;
                
                foreach($Q->result() as $r){
                    
                    $action = "";                    
                    
                    $action   .= '<input type="button" value="Cancel Approval" no="'.$r->requ_no.'" id="btnfundtrfApproveCancel" class="btn_regular" style="width: 130px; background-color:red; color: #ffffff; border: 1px solid red">';

                    if ( $s != $r->bc_name ){

                        $cls = str_replace(" ","",$r->bc_name);

                        $T .= " <tr bgColor='#f1f1f1' class='bc_name_cls' cn = '$cls'>          
                                <td colspan='10'><span>".$r->bc_name."</span></td>            
                            </tr>";

                        $s = $r->bc_name;

                        $title_print = true;
                        

                    }

                    $trf_typ = 'unknown';
                    if ($r->fund_tr_opt_no == 3){ $trf_typ = 'Cheque Withdraw'; }
                    if ($r->fund_tr_opt_no == 5){ $trf_typ = 'Bank to Bank'; }

                    if ( $title_print ){
                    
                        $T .="<tr class='$cls all_rows' style='display:none'>                                                                                               
                            <td>No</td>
                            <td>Transfer Type</td>
                            <td>Transfer No</td>
                            <td>Branch</td>
                            <td>Account Description</td>
                            <td align='center'>Cheque Number</td>
                            <td align='right'>Amount</td>
                            <td align='left'>Comment</td>
                            <td align='center'>Date and Time</td>
                            <td align='right'>Action</td>            
                        </tr>";

                        $title_print = false;
                    }


                    $T .= " <tr class='$cls all_rows' style='display:none'>          
                                <td>".($n+1)."</td>
                                <td>".$trf_typ."</td>
                                <td>".($r->requ_no)."</td>
                                <td>".$r->bc_name."</td>
                                <td>".$r->description." 
                                    <input type='hidden' class='bank_code' value='".$r->bank_acc."'>
                                    <input type='hidden' class='chq_no'     value='".$r->chq_no."'>
                                    <input type='hidden' class='amount'    value='".$r->amount."'>
                                </td>            
                                <td align='center'>".$r->chq_no."</td>
                                <td align='right'>".number_format($r->amount,2)."</td>                                                                
                                <td>".$r->comment."</td>
                                <td aling='center'>".$r->requ_initi_date_time."</td>
                                <td align='right'>$action</td>            
                            </tr>";

                    $n++;
                }

                $T .="<input type='hidden' id='chq_num_rows' value='".$n."'>";

            }else{

            $T .= ' <div style="height:24px">
                        <div class="text_box_holder_commen" style="width:100%;float: left;border-top: none;">
                            <span>No cheques added for this branch, Please contact head office</span>                        
                        </div>
                    </div>';

            }

            if ($Q->num_rows() > 0){
                $a['data'] = $T;
            }else{
                $a['data'] = 0;       
            }                        
            
            echo json_encode($a);            
            exit;             
        }


        if ($_POST['page_name'] == "general_voucher"){

            $oc = $this->sd['oc'];
            
            $q = $this->db->query("SELECT `area` FROM `u_users` U WHERE `cCode` = '$oc' ");
            if ($q->num_rows() > 0){                
                $z = $q->row()->area;
                $z = explode(",",$z);
                $zz = array();
                for ($n = 0 ; $n < count($z); $n++){ $zz[$n] = "'".$z[$n]."'"; }
                $z = implode(",",$zz);                
            }else{
                $z = '';
            }

            $this->load->model('user_permissions');        
            
            if ($this->user_permissions->is_auth('level_1_authorization') == 1){
                $q1 = " AND (S.`status` = 'F' OR S.`is_HO_cashier` = 1 )";
            }else{
                if ($this->user_permissions->is_auth('level_2_authorization') == 1){
                    $q1 = " AND S.`status` = 'FF' ";
                }else{
                    $q1 = "";
                }
            }
            
            $Q = $this->db->query("SELECT S.time, S.no,S.nno,B.`name` AS `bc_name` , trim(S.`bc`) as `bc`, S.`date`, UPPER(S.`request_from`) AS `request_from`, UPPER(S.`method`) AS `receiving_method`,S.`method` as `type`, S.`amount` AS `amount`, U.`discription` AS `request_by`, S.`request_datetime`, S.status,S.`paid_acc` 
                FROM `t_gen_vou_app_rqsts_sum` S 

                JOIN m_branches B ON B.`bc` = S.`bc`
                JOIN u_users U ON S.`request_by` = U.`cCode`

                WHERE S.vocuher_type = 'gl_vouchers' AND NOT (S.`status` = 'A' OR S.`status` = 'RE' OR S.`status` = 'R') AND S.`bc` IN (SELECT B.bc FROM  m_branches B WHERE B.`area` IN ($z) ) 
                $q1
                ORDER BY S.`bc` , S.`method`, S.`nno` DESC, S.`date` DESC, S.`request_datetime` ASC ");
            
            if ($Q->num_rows() > 0){
                $a['discount'] = $Q->result();
            }else{
                $a['discount'] = 0;       
            }            
            
            echo json_encode($a);            
            exit;             
        }

        if ($_POST['page_name'] == "general_voucher_approved"){

            $oc = $this->sd['oc'];
            
            $q = $this->db->query("SELECT `area` FROM `u_users` U WHERE `cCode` = '$oc' ");
            if ($q->num_rows() > 0){                
                $z = $q->row()->area;
                $z = explode(",",$z);
                $zz = array();
                for ($n = 0 ; $n < count($z); $n++){ $zz[$n] = "'".$z[$n]."'"; }
                $z = implode(",",$zz);                
            }else{
                $z = '';
            }

            $this->load->model('user_permissions');        
            
            if ($this->user_permissions->is_auth('level_1_authorization') == 1){
                $q1 = " AND (S.`status` = 'F' OR S.`is_HO_cashier` = 1 )";
            }else{
                if ($this->user_permissions->is_auth('level_2_authorization') == 1){
                    $q1 = " AND S.`status` = 'FF' ";
                }else{
                    $q1 = "";
                }
            }
            
            $Q = $this->db->query("SELECT S.no,S.nno,UPPER(B.`name`) AS `bc_name` , UPPER(S.`bc`) as `bc`, S.`date`, UPPER(S.`request_from`) AS `request_from`, UPPER(S.`method`) AS `receiving_method`,S.`method` as `type`, S.`amount` AS `amount`, U.`discription` AS `request_by`, S.`request_datetime`, S.status,S.`paid_acc`,IF (S.`method` = 'cheque' , concat(' - ',I.`cheque_no`),'') AS `chq_no` 
                FROM `t_gen_vou_app_rqsts_sum` S 

                JOIN m_branches B ON B.`bc` = S.`bc`
                JOIN u_users U ON S.`request_by` = U.`cCode`
                LEFT JOIN t_cheque_issued I ON S.`nno` = I.`trans_no` AND I.`trans_code` = 47

                WHERE S.vocuher_type = 'gl_vouchers' AND NOT S.`status` = 'W' AND S.`bc` IN (SELECT B.bc FROM  m_branches B WHERE B.`area` IN ($z) ) 
                $q1
                ORDER BY S.`bc` , S.`method`, S.`nno` DESC, S.`date` DESC, S.`request_datetime` ASC ");
            
            if ($Q->num_rows() > 0){
                $a['discount'] = $Q->result();
            }else{
                $a['discount'] = 0;       
            }            
            
            echo json_encode($a);            
            exit;             
        }


        























        if ($_POST['page_name'] == "general_receipt"){

            $oc = $this->sd['oc'];            
            $Q  = $this->db->query("SELECT *, B.`name` AS `bc_name` FROM `t_receipt_gl_sum` S JOIN m_branches B ON S.`bc` = B.`bc` WHERE `status` = 1");
            
            if ($Q->num_rows() > 0){
                $a['discount'] = $Q->result();
            }else{
                $a['discount'] = 0;       
            }            
            
            echo json_encode($a);            
            exit;             
        }


        if ($_POST['page_name'] == "general_receipt_approved"){

            $oc = $this->sd['oc'];            
            $Q  = $this->db->query("SELECT *, B.`name` AS `bc_name` FROM `t_receipt_gl_sum` S JOIN m_branches B ON S.`bc` = B.`bc` WHERE `status` = 3");
            
            if ($Q->num_rows() > 0){
                $a['discount'] = $Q->result();
            }else{
                $a['discount'] = 0;       
            }            
            
            echo json_encode($a);            
            exit;             
        }

















































        if ($_POST['page_name'] == "salary_voucher"){

            $oc = $this->sd['oc'];
            
            $q = $this->db->query("SELECT `area` FROM `u_users` U WHERE `cCode` = '$oc' ");
            if ($q->num_rows() > 0){                
                $z = $q->row()->area;
                $z = explode(",",$z);
                $zz = array();
                for ($n = 0 ; $n < count($z); $n++){ $zz[$n] = "'".$z[$n]."'"; }
                $z = implode(",",$zz);                
            }else{
                $z = '';
            }

            $this->load->model('user_permissions');        
            
            if ($this->user_permissions->is_auth('level_1_authorization') == 1){
                $q1 = " AND (S.`status` = 'F' OR S.`is_HO_cashier` = 1 )";
            }else{
                if ($this->user_permissions->is_auth('level_2_authorization') == 1){
                    $q1 = " AND S.`status` = 'FF' ";
                }else{
                    $q1 = "";
                }
            }
            
            $Q = $this->db->query("SELECT CONCAT(VS.`sal_vou_yr` , '-' , VS.`sal_vou_mon`) AS `ym`, S.no,S.nno,B.`name` AS `bc_name` , S.`bc`, S.`date`, UPPER(S.`request_from`) AS `request_from`, UPPER(S.`method`) AS `receiving_method`, S.`amount` AS `amount`, U.`discription` AS `request_by`, S.`request_datetime`, S.status,S.`paid_acc` 
                FROM `t_gen_vou_app_rqsts_sum` S 

                JOIN m_branches B ON B.`bc` = S.`bc`
                JOIN u_users U ON S.`request_by` = U.`cCode`
                JOIN `t_voucher_gl_sum` VS ON S.`bc` = VS.`bc` AND VS.`nno` = S.`no` AND VS.`paid_acc` = S.`paid_acc`
                WHERE S.vocuher_type = 'salary_vouchers' AND NOT (S.`status` = 'A' OR S.`status` = 'RE' OR S.`status` = 'R') AND S.`bc` IN (SELECT B.bc FROM  m_branches B WHERE B.`area` IN ($z) ) 
                $q1
                ORDER BY S.`bc` , S.`method`, s.`nno` DESC, S.`date` DESC, S.`request_datetime` ASC ");
            
            if ($Q->num_rows() > 0){
                $a['discount'] = $Q->result();
            }else{
                $a['discount'] = 0;       
            }            
            
            echo json_encode($a);            
            exit;             
        }

        if ($_POST['page_name'] == "all_requests"){ $Q = ""; }        
        if ($_POST['page_name'] == "redeem_discount"){$Q = " AND request_type='DI' "; }
        if ($_POST['page_name'] == "over_advance"){$Q = " AND request_type='EL' "; }
        if ($_POST['page_name'] == "extra_loan"){$Q = " AND request_type='EX' "; }
        if ($_POST['page_name'] == "bill_cancellation"){$Q = " AND request_type='CN' "; }

        $Q = $this->db->query("SELECT *  FROM `t_approval` WHERE record_status = 'W' $Q AND NOW() < (`requested_datetime` + INTERVAL 10 MINUTE) ORDER BY requested_datetime");
        
        if ($Q->num_rows() > 0){
            $a['discount'] = $Q->result();
            $a['user'] = $this->sd['oc'];
        }else{
            $a['discount'] = 0;
        }    


        echo json_encode($a);

    }

    public function makeAproveOrReject(){

        $loanno = $_POST['loanno'];        
        
        if (isset($_POST['approved_amount'])){ $approved_amount = $_POST['approved_amount']; }else{$approved_amount = "";}
        
        $record_status              = $_POST['record_status'];
        $date_time                  = date("Y-m-d H:i:s");
        $_POST['record_status']     = $record_status;        
        $_POST['approved_amount']   = $approved_amount ;
        $_POST['approved_by']       = $this->sd['oc'] ;
        $_POST['approved_datetime'] = $date_time;
        $_POST['action_date']       = $date_time;

        if ($record_status == "A"){
            $this->utility->user_activity_log($module='over advance',$action='approve',$trans_code='1',$trans_no=$loanno,$note='search approval history list with '.$loanno);
        }

        if ($record_status == "R"){
            $this->utility->user_activity_log($module='over advance',$action='reject',$trans_code='1',$trans_no=$loanno,$note='search approval history list with '.$loanno);
        }


        if ($this->db->where(array("record_status"=>"W","loanno"=>$loanno))->set($_POST)->limit(1)->update("t_approval")){
            $a['s'] = 1;
        }        
    
        echo json_encode($a);
    }

    public function isDiscountApproved($loanno){
        $bc = $this->sd['bc'];
        $Q = $this->db->query("SELECT record_status FROM `t_approval_history` WHERE bc = '$bc' AND loanno = '$loanno' AND record_status = 'A'");
        if ($Q->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function makeAproveOrRejectFT(){

        //echo "App"; exit;
        
        $no = $_POST['no'];
        
        if (isset($_POST['approved_amount'])){ 
            $approved_amount = $_POST['approved_amount']; 
        }else{
            $approved_amount = "";
        }        
        
        $date_time = date("Y-m-d H:i:s");

        $_POST['approved_by'] = $this->sd['oc'] ;
        $_POST['approved_datetime'] = $date_time;        

        if ($_POST['status'] == "A"){            

        }

        $_POST['from_bc'] = $this->db->query("SELECT bc FROM `m_branches` WHERE is_ho = 1 LIMIT 1")->row()->bc;

        if ($_POST['r_bc'] != ""){
            $_POST['from_bc'] = $_POST['r_bc'];
        }

        unset($_POST['approved_amount'],$_POST['hid'],$_POST['ln'],$_POST['bt'],$_POST['bn'],$_POST['chq_amount'],$_POST['r_bc']);

        $this->db->where(array("status"=>"W","no"=>$no))->set($_POST)->limit(1)->update("t_fnd_trnsfr_rqsts_sum");            
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{            
            $this->db->trans_commit();            
            $a['s'] = 1;
        }               
    
        echo json_encode($a);
    }

    public function get_bc_list(){
        $a['bc_cash_balance_grid'] = 0;
        echo json_encode($a);
    }



    public function appr_vou(){             

        $this->db->trans_begin();

        $no             = $_POST['no'];
        $S              = $_POST['record_status'];
        $app_by         = $this->sd['oc'];
        $app_datetime   = date('Y-m-d h:i:s');
        $type           = $_POST['type'];
        $bc             = $_POST['v_bc'] = $_POST['bc'];
        $SS             = $S;
        $paid_acc       = $_POST['paid_acc'];

        if ($_POST['type'] == 'cheque'){$tc = 47; }else{$tc = 48; }

        if ($S == "APP_CANCEL"){
            $S = "W";
        }

        if ($S == "F"){
            $this->utility->user_activity_log($module='General Voucher',$action='forward',$trans_code=$tc,$trans_no=$no,$note='');
        }

        if ($S == "R"){
            $this->utility->user_activity_log($module='General Voucher',$action='rejected',$trans_code=$tc,$trans_no=$no,$note='');
        }

        
        $Q = $this->db->query("    UPDATE `t_gen_vou_app_rqsts_sum` SET `approved_by`='$app_by', `approved_datetime`='$app_datetime', `status`='$S' WHERE bc = '$bc' AND method = '$type' AND `nno` = '$no' LIMIT 1 ");        

        if ($S == "A"){


            if ($_POST['type'] == "cheque"){

                /*$qaz = $this->db->query("SELECT trans_no FROM `t_cheque_issued` WHERE trans_code = 47 AND account = '".$_POST['paid_acc']."' 
                          AND cheque_no = '".$_POST['cheque_no']."' AND status = 1 LIMIT 1 ");

                if ($qaz->num_rows() > 0){
                    $a['s'] = 3;
                    $a['tr'] = $qaz->row()->trans_no;
                    echo json_encode($a);
                    exit;
                }*/
            }

            
            $this->max_no = $no;

            $this->db->where(array("bc"=>$bc,"type"=>$type,"nno"=>$no))->set("is_approved",1)->limit(1)->update("t_voucher_gl_sum");

            $this->db->where(array("bc"=>$bc, "trans_code"=>47, "trans_no"=>$no,"issued_to_acc"=>$_POST['paid_acc']))->set("status",1)->limit(1)->update("t_cheque_issued");

            $this->utility->user_activity_log($module='General Voucher',$action='approve',$trans_code=$tc,$trans_no=$no,$note='');
            
            $this->account_update(1);
        }


        if ($SS == "APP_CANCEL"){
            
            $this->max_no = $no;

            $this->db->where(array("bc"=>$bc,"type"=>$type,"nno"=>$no))->set("is_approved",0)->limit(1)->update("t_voucher_gl_sum");

            $this->db->where(array("bc"=>$bc, "trans_code"=>47, "trans_no"=>$no,"issued_to_acc"=>$_POST['paid_acc']))->set("status",2)->limit(1)->update("t_cheque_issued");

            $this->utility->user_activity_log($module='General Voucher',$action='app_cancel',$trans_code=$tc,$trans_no=$no,$note='');
            
            $this->account_update_app_cancel(1);
        }

        
        if ($this->db->trans_status() === FALSE){
            
            $this->db->trans_rollback();
            $a['s'] = "error";
            $a['s'] = 0;

        }else{

            $this->db->trans_commit();        
            $a['s'] = 1;

        }

        echo json_encode($a);
        
    }



    public function account_update_app_cancel($condition){

    $nno = $this->max_no;
    $_POST['hid'] = "";    
    $_POST['ref_no'] = "";
    $bc = $_POST['v_bc'];
    
    if ($_POST['type'] == 'cash'){
        $trans_code = 48;
    }else{
        $trans_code = 47;
    }

    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $_POST['v_bc']);
    $this->db->where("trans_code", $trans_code);
    $this->db->where("trans_no", $this->max_no);
    $this->db->where("entry_code", $_POST['paid_acc']);    
    $this->db->delete("t_check_double_entry");
    
    if ($condition == 1) {
      if ($_POST['hid'] != "0" || $_POST['hid'] != "") {
        /*$this->db->where('cl', $this->sd['cl']);
        $this->db->where('bc', $_POST['v_bc']);
        $this->db->where('trans_code', $trans_code);
        $this->db->where('trans_no', $this->max_no);
        $this->db->where("entry_code", $_POST['paid_acc']);
        $this->db->delete('t_account_trans');*/
      }
    }
    
    $config = array(
      "ddate" => $_POST['date'],
      "time" => date('H:i:s'),
      "trans_code" => $trans_code,
      "trans_no" => $this->max_no,
      "op_acc" => 0,
      "reconcile" => 0,
      "cheque_no" => 0,
      "narration" => "",
      "ref_no" => $_POST['ref_no']
    );
    
    
    $this->load->model('account');
    $this->account->set_data($config);

  
    // $q = $this->db->query("SELECT * FROM t_voucher_gl_sum WHERE bc = '$bc' AND nno = '$nno' ");

    $QD = $this->db->query("SELECT IF (GL_VOU.`type` = 'cheque',`cheque_acc`,`cash_acc`) AS `acc_code`, IF (GL_VOU.`type` = 'cheque',`cheque_amount`,`cash_amount`) AS `amount` FROM `t_voucher_gl_sum` GL_VOU WHERE bc = '$bc' AND type = '".$_POST['type']."' AND nno = $nno ")->row();
    
    $_POST['cash_acc'] = $QD->acc_code;
    $_POST['cash_amount'] = $QD->amount;


    
    $QD2 = $this->db->query("   SELECT V.acc_code,V.amount, CONCAT(A.`code`,' - ', A.`description`) AS `acc_desc` , V.v_bc,V.v_class
                                FROM `t_voucher_gl_det` V
                                JOIN m_account A ON V.`acc_code` = A.`code`
                                WHERE V.bc = '$bc' AND V.type='".$_POST['type']."' AND V.nno = $nno ");

    $paid_acc_desc = '';
    foreach ($QD2->result() as $R) {        
        $this->account->set_value4("Approval cancel ".$R->acc_desc, $R->amount, "cr", $R->acc_code, $condition, "AA","",$_POST['paid_acc'],$R->v_bc,$R->v_class);    
        $paid_acc_desc .= $R->acc_desc." (".$R->amount.") \n";
    }

    $this->account->set_value4("Approval cancel \n".$paid_acc_desc, $_POST['cash_amount'] , "dr", $_POST['cash_acc'] , $condition, "AA","",$_POST['paid_acc'],$bc,'');

    
    if ($condition == 0) {
      $query = $this->db->query("
           SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok 
           FROM `t_check_double_entry` t
           LEFT JOIN `m_account` a ON t.`acc_code` = a.`code`
           WHERE  t.`cl`='" . $this->sd['cl'] . "'  AND t.`bc`='" . $_POST['v_bc'] . "'  AND t.`trans_code`='$trans_code'  AND t.`trans_no` ='" . $this->max_no . "' AND 
           a.`is_control_acc`='0' AND entry_code = '".$_POST['paid_acc']."'  ");
      
      if ($query->row()->ok == "0") {
        $this->db->where('cl', $this->sd['cl']);
        $this->db->where('bc', $_POST['v_bc']);
        $this->db->where('trans_code', $trans_code);
        $this->db->where('trans_no', $this->max_no);
        $this->db->where("entry_code", $_POST['paid_acc']);
        $this->db->delete("t_check_double_entry");
        return "0";
      } else {
        return "1";
      }
    }
  }




    public function account_update($condition){

    $bc = $_POST['v_bc'];
    $nno = $this->max_no;

    $_POST['hid'] = "";    
    $_POST['ref_no'] = "";


    if ($_POST['type'] == 'cash'){
        $trans_code = 48;
    }else{
        $trans_code = 47;
    }

    
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $_POST['v_bc']);
    $this->db->where("trans_code", $trans_code);
    $this->db->where("trans_no", $this->max_no);
    $this->db->where("entry_code", $_POST['paid_acc']);    
    $this->db->delete("t_check_double_entry");
    
    if ($condition == 1) {
      if ($_POST['hid'] != "0" || $_POST['hid'] != "") {
        $this->db->where('cl', $this->sd['cl']);
        $this->db->where('bc', $_POST['v_bc']);
        $this->db->where('trans_code', $trans_code);
        $this->db->where('trans_no', $this->max_no);
        $this->db->where("entry_code", $_POST['paid_acc']);
        $this->db->delete('t_account_trans');
      }
    }
    

    if ($_POST['time'] == NULL || $_POST['time'] == "" ){
        $_POST['time'] = date('H:i:s');
    }

    $config = array(
      "ddate" => $_POST['date'],
      "time" => $_POST['time'],
      "trans_code" => $trans_code,
      "trans_no" => $this->max_no,
      "op_acc" => 0,
      "reconcile" => 0,
      "cheque_no" => 0,
      "narration" => "",
      "ref_no" => $_POST['ref_no']
    );
    
    
    $this->load->model('account');
    $this->account->set_data($config);

  
    // $q = $this->db->query("SELECT * FROM t_voucher_gl_sum WHERE bc = '$bc' AND nno = '$nno' ");

    $QD = $this->db->query("SELECT IF (GL_VOU.`type` = 'cheque',`cheque_acc`,`cash_acc`) AS `acc_code`, IF (GL_VOU.`type` = 'cheque',`cheque_amount`,`cash_amount`) AS `amount` FROM `t_voucher_gl_sum` GL_VOU WHERE bc = '$bc' AND type='".$_POST['type']."' AND nno = $nno ")->row();
    
    $_POST['cash_acc'] = $QD->acc_code;
    $_POST['cash_amount'] = $QD->amount;

    $QD2 = $this->db->query("   SELECT V.acc_code,V.amount, CONCAT(A.`code`,' - ', A.`description`) AS `acc_desc`, V.v_bc,V.v_class
                                FROM `t_voucher_gl_det` V
                                JOIN m_account A ON V.`acc_code` = A.`code`
                                WHERE V.bc = '$bc' AND V.type='".$_POST['type']."' AND V.nno = $nno ");

    $paid_acc_desc = "";

    foreach ($QD2->result() as $R) {        
        $dess = $R->acc_desc;
        $this->account->set_value4($dess, $R->amount, "dr", $R->acc_code, $condition, "AA","",$_POST['paid_acc'],$R->v_bc,$R->v_class);    
        
        $paid_acc_desc .= $R->acc_desc." (".$R->amount.") \n";
    }


    $this->account->set_value4($paid_acc_desc, $_POST['cash_amount'] , "cr", $_POST['cash_acc'] , $condition, "AA","",$_POST['paid_acc'],$bc,"");    


    
    if ($condition == 0) {
      $query = $this->db->query("
           SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok 
           FROM `t_check_double_entry` t
           LEFT JOIN `m_account` a ON t.`acc_code` = a.`code`
           WHERE  t.`cl`='" . $this->sd['cl'] . "'  AND t.`bc`='" . $_POST['v_bc'] . "'  AND t.`trans_code`='$trans_code'  AND t.`trans_no` ='" . $this->max_no . "' AND 
           a.`is_control_acc`='0' AND entry_code = '".$_POST['paid_acc']."'  ");
      
      if ($query->row()->ok == "0") {
        $this->db->where('cl', $this->sd['cl']);
        $this->db->where('bc', $_POST['v_bc']);
        $this->db->where('trans_code', $trans_code);
        $this->db->where('trans_no', $this->max_no);
        $this->db->where("entry_code", $_POST['paid_acc']);
        $this->db->delete("t_check_double_entry");
        return "0";
      } else {
        return "1";
      }
    }
  }



    public function app_action_dis(){

        $stat = $_POST['stat'];
        $nno  = $_POST['nno'];        

        if ($stat == 'A'){
            $approved_amount = $_POST['approved_amount'];
        }else{
            $approved_amount = 0;
        }
        
        $Q = $this->db->query("     UPDATE `t_approval` 

                                    SET `record_status` = '$stat',                                        
                                        `approved_amount` = '$approved_amount',
                                        `approved_by` = '".$this->sd['oc']."',
                                        `approved_datetime` = '".date('Y-m-d H:i:s')."'

                                    WHERE `nno` = '$nno' 
                                    LIMIT 1 ");

        $a['stat']  = $stat;
        
        if ($Q){
            $this->utility->user_activity_log($module='Discount(Redeem)',$action='approve',$trans_code='2',$trans_no=$nno,$note='');
            $a['s']     = 1;
        }else{
            $a['s']     = 0;
        }

        echo json_encode($a);

    }

    public function view_articles_app_window(){

        $no = $_POST['no'];

        $a['list'] = $this->db->query("SELECT `over_adv_item_list` FROM `t_approval` WHERE loanno = '$no' LIMIT 1 ")->row()->over_adv_item_list;

        echo json_encode($a);

    }


    public function app_max_no(){

        return $this->db->query("SELECT GREATEST((SELECT IFNULL(MAX(nno)+1,1) FROM t_approval),(SELECT IFNULL(MAX(nno)+1,1) FROM t_approval_history)) AS `nno_max_no`")->row()->nno_max_no;

    }


    public function cancel_chq_withdraw_approval(){

        $no = $this->max_no = $_POST['no'];

        $this->db->trans_begin();    

            $account_update =   $this->account_update_cancel_cheque_withdrawal(0);

            if($account_update!=1){
                $a['s'] = 0;
                $a['m'] = "Invalid account entries";

                exit;

            }else{
                
                $account_update = $this->account_update_cancel_cheque_withdrawal(1);

                $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 1 WHERE `no` = '$no' LIMIT 1 ");
                // update t_fnd_trnsfr_rqsts_chq_det status update here to 1
            }

        if ($this->db->trans_status() === FALSE){
            
            $this->db->trans_rollback();
            $a['s'] = "error";

        }else{

            $this->db->trans_commit();        
            $a['s'] = 1;

        }

        echo json_encode($a);





    }


    public function account_update_cancel_cheque_withdrawal($condition) {
    
        $R1 = $this->db->query("SELECT * FROM `t_fund_transfer_sum` WHERE transfer_type = 'cheque_withdraw' AND `status` = 10 AND `no` = '".$this->max_no."'  LIMIT 1 ")->row();

        $config = array(
            "ddate" => $R1->added_date,
            "time" => date('H:i:s'),
            "trans_code" => 9,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        $this->load->model('account');
        $this->account->set_data($config);
        
        $Q2 = $this->db->query("SELECT * FROM t_account_trans WHERE bc = '".$R1->to_bc."' AND trans_code = 9 AND trans_no = '".$this->max_no."' LIMIT 2 ");

        foreach($Q2->result() as $R2){

            if ( $R2->dr_amount > 0 ){
                $this->account->set_value2("Cancel - ".$R2->description, $R2->dr_amount , "cr", $R2->acc_code,$condition,"","","","","",$R1->to_bc);                
            }

            if ( $R2->cr_amount > 0 ){
                $this->account->set_value2("Cancel - ".$R2->description, $R2->cr_amount , "dr", $R2->acc_code,$condition,"","","","","",$R1->to_bc);                
            }

        }       


        return 1;

    }


    public function app_action_dis_view(){

        $billno = $_POST['billno'];
        $T      = '';

        $Q = $this->db->query("SELECT 

        IC.`des` AS `cat`,
        I.`itemname`,
        C.`des` AS `condition`,
        GR.`printval`,
        LI.`quality`,
        LI.`qty`,
        LI.`goldweight`,
        LI.`pure_weight`,
        LI.`denci_weight`,
        LI.`value`,

        CUS.`cusname`,
        CUS.`address`,
        CUS.`nicno`,
        L.`ddate`,
        L.`requiredamount`

        FROM t_loanitems LI 


        JOIN `r_itemcategory`   IC ON LI.`cat_code` = IC.`code`
        JOIN `r_items`      I ON LI.`itemcode` = I.`itemcode`
        JOIN `r_condition`  C ON LI.`con` = C.`code`
        JOIN `r_gold_rate`  GR ON LI.`goldtype` = GR.`id`

        JOIN ( SELECT l.`requiredamount`, l.`ddate`,billno,l.`cus_serno` FROM t_loan l GROUP BY l.`billno`) L ON LI.`billno` = L.billno

        JOIN m_customer CUS ON L.cus_serno = CUS.serno

        WHERE LI.billno = '$billno' ");


        foreach ($Q->result() as $r) {
            
            $T .= '<div id="pn1" style="border-bottom:1px dotted #ccc;height:47px">';
                
                $T .= '<div style="width:135px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->cat.'" style="width:120px" id="cat_code_desc" class="input_text_regular_new_pawn catC" readonly="readonly"></div>';
                $T .= '<div style="width:175px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->itemname.'" style="width:165px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn itemC"></div>';
                $T .= '<div style="width:140px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->condition.'" style="width:130px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn conC"></div>';
                $T .= '<div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->printval.'" style="width:97px" id="gold_type_desc" readonly="readonly" class="input_text_regular_new_pawn gtypeC"></div>';
                $T .= '<div style="width:60px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->quality.'" style="width:47px" id="gold_qlty" readonly="readonly" class="input_text_regular_new_pawn gquliC"></div>';
                $T .= '<div style="width:50px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->qty.'" style="width:37px" id="qty" readonly="readonly" class="input_text_regular_new_pawn qtyC"></div>';
                $T .= '<div style="width:76px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->goldweight.'" style="width:63px" id="t_weigth" readonly="readonly" class="input_text_regular_new_pawn t_weigthC"></div>';
                $T .= '<div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->pure_weight.'" style="width:60px" id="p_weigth" readonly="readonly" class="input_text_regular_new_pawn p_weigthC"></div>';
                $T .= '<div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->denci_weight.'" style="width:60px" id="d_weigth" readonly="readonly" class="input_text_regular_new_pawn d_weigthC"></div>';
                $T .= '<div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'.$r->value.'" style="width:100px; text-align:right" id="value" readonly="readonly" class="input_text_regular_new_pawn valueC"></div>';

            $T .='</div>';

        }


        $a['s'] = 1;
        $a['list'] = $T;

        $a['sum'] = $Q->row();

        echo json_encode($a);

    }


    public function appr_receipt(){

        $no = $this->max_no = $_POST['hid'] = $_POST['no'];
        $st = $_POST['st'];
        $bc = $this->bc_glob = $_POST['bc'];

        $q = $this->db->query("SELECT * FROM `t_receipt_gl_sum` WHERE nno = '$no' AND bc = '$bc' AND `status` = 1 LIMIT 1 ")->row();
        
        if ($q){
            
            
            $_POST['date'] = $q->date;
            $_POST['time'] = $q->time;
            $_POST['ref_no'] = $q->ref_no;

            $_POST['total_amount'] = $q->cash_amount;
            $_POST['DR_acc'] = $q->paid_to_acc;
            $_POST['type'] = $q->type;

            $_POST['v_bc'] = $q->bc;
            
            $qq  = $this->db->query("UPDATE `t_receipt_gl_sum` SET `status` = '$st' WHERE nno = '$no' AND bc = '$bc' AND `status` = 1 LIMIT 1 ");
            $this->receipt_account_update(1);

            $a['s'] = 1;

        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }

    public function receipt_account_update($condition){

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 49);
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $this->bc_glob);
        $this->db->delete("t_check_double_entry");
        
        if ($condition == 1) {
          if ($_POST['hid'] != "0" || $_POST['hid'] != "") {
            $this->db->where('cl', $this->sd['cl']);
            $this->db->where('bc', $this->bc_glob);
            $this->db->where('trans_code', 49);
            $this->db->where('trans_no', $this->max_no);
            $this->db->where('entry_code',0);
            $this->db->delete('t_account_trans');
          }
        }
        
        $config = array(
          "ddate" => $_POST['date'],
          "time" => $_POST['time'],
          "trans_code" => 49,
          "trans_no" => $this->max_no,
          "op_acc" => 0,
          "reconcile" => 0,
          "cheque_no" => 0,
          "narration" => "",
          "ref_no" => $_POST['ref_no']
        );
        
        $des = "General Receipt";
        $this->load->model('account');
        $this->account->set_data($config);        
        
        $Q = $this->db->query(" SELECT * FROM `t_receipt_gl_det` WHERE bc = '$this->bc_glob' AND nno = '$this->max_no' ");

        $dess = "General Receipt";

        foreach ($Q->result() as $R) {
            $this->account->set_value4($dess, $R->amount ,"cr", $R->acc_code,$condition,"","",0,$R->v_bc,$R->v_class);           
        }        

        if ($_POST['type'] == "cash"){            
            $this->account->set_value4($des, $_POST['total_amount'], "dr", $_POST['DR_acc'], $condition,"","",0,$this->bc_glob,"");
        }

        if ($_POST['type'] == "cheque"){  
            $recev_chq_acc_code = $this->utility->get_default_acc("CHEQUE_IN_HAND");    
            $this->account->set_value4($des, $_POST['total_amount'], "dr", $recev_chq_acc_code, $condition,"","",0,$this->bc_glob,"");
        }    
        
        if ($condition == 0) {

            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='".$this->sd['cl']."'  AND t.`bc`='".$this->bc_glob."' AND t.`trans_code`='49'  AND t.`trans_no` ='" . $this->max_no . "' AND t.entry_code = '0' AND a.`is_control_acc`='0'");

            if ($query->row()->ok == "0") {        
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 49);
                $this->db->where("cl", $this->sd['cl']);
                $this->db->where("bc", $this->bc_glob);
                $this->db->delete("t_check_double_entry");        
                return "0";
            } else {        
                return "1";
            }

        }

    }


    public function reject_receipt(){

        $nno    = $_POST['nno'];
        $bc     = $_POST['bc'];
        $DT     = date('Y-m-d h:i:s');
        $OC     = $this->sd['oc'];
        

        $qq     = $this->db->query("UPDATE `t_receipt_gl_sum` SET `status` = '1' WHERE nno = '$nno' AND bc = '$bc' AND `status` = 3 LIMIT 1 ");

        if ($qq){

            $Q = $this->db->query("INSERT INTO t_account_trans SELECT '',`loanno`, `trans_no`, `trans_code`, date('$DT'), `cr_amount`,`dr_amount`, `acc_code`, CONCAT('Canceled ',`description`), `op_acc`, `reconcile`, `ref_no`, `cheque_no`, `is_control_acc`, `narration`, `sub_no`, `bank_rec_no`, '$DT', '$OC', `bc`, `cl`, `billno`, `billtype`, `entry_code`, time('$DT') , `v_bc`, `v_class` FROM t_account_trans WHERE trans_code = 49 AND trans_no = $nno AND bc = '$bc' ");
        }

        if ($Q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);


    }









}