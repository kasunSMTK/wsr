<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class rpt_sales_current_stock extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    // public function Excel_report()
    // {

    //     $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
    //     $this->PDF_report($_POST, 'XL');
    // }

    public function PDF_report($a, $rt = 'PDF')
    {

        if ($_POST['bc_arry'] === null) {
            $BC = " ";
        } else {

            for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                $bc_ar[] = "'" . str_replace(",", "", $_POST['bc_arry'][$n]) . "'";
            }

            $bc = implode(',', $bc_ar);
            $BC = " AND d.bc IN ($bc)";
        }

        // var_dump($bc,$BC);
        // exit();
       // $fd = $_POST["from_date"];
        $td = $_POST["to_date"];

        $q = $this->db->query("SELECT 

        i.`itemcate`,i.`itemcode`,i.`itemname`,i.`bc`,SUM(i.`qty`) AS qty,i.`ddate`, i.`cate_name`
        
        FROM `z_current_item_wise_stock` i
        
        WHERE i.`bc` = '$bc' 

        and  i.`ddate` <= '$td'
        
        GROUP BY i.`itemcode`
        
        ORDER BY i.`itemname` ");

        if ($q->num_rows() > 0) {

            $r_detail['det'] = $q->result();
            $r_detail['td'] = $td;
            if ($rt == "PDF") {
                $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
            } else {
                echo "<script>alert('Excel Report Not Found');close();</script>";
            }

        } else {
            echo "<script>alert('No data found');close();</script>";
        }

    }

}
