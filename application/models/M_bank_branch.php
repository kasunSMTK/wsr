<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_bank_branch extends CI_Model {
    
    private $sd;
    private $mtb;
    private $tb_bank;
    
    function __construct(){
	parent::__construct();
	
	$this->sd = $this->session->all_userdata();
	$this->load->database($this->sd['db'], true);
	$this->load->model('user_permissions');
	$this->mtb = $this->tables->tb['m_bank_branch'];
    $this->tb_bank = $this->tables->tb['m_bank'];
    }
    
    public function base_details(){
        $a['load_list'] = $this->load_list();
    	return $a;
    }

    public function load(){
       echo $this->load_list();  
    }
    

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM m_bank_branch ORDER BY code DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;        
        
        return json_encode($a);   
    }

   
     public function set_edit(){
        $code = $_POST['code'];
        $this->db->select(array($this->mtb.'.code', $this->mtb.'.description', $this->mtb.'.bank',$this->tb_bank.'.description as bank_des',$this->mtb.'.branch_code'));
        $this->db->where($this->mtb.'.code', $_POST['code']);
        $this->db->join($this->tb_bank,$this->tb_bank.'.code='.$this->mtb.'.bank');
        $this->db->limit(1);
        echo json_encode($this->db->get($this->mtb)->first_row());
     }
   

    public function bank_branch_list(){
	$this->db->order_by('code');
	
	$res = $tres = array(); $mcat = "";
	foreach($this->db->get($this->mtb)->result() as $r){
	    if($mcat != $r->code && $mcat != ""){
		$res[$mcat] = $tres;
		$tres = array();
	    }
	    $mcat = $r->code;
	    
	    $tres[] = array($r->Bank, $r->Description);
	}
	$res[$mcat] = $tres;
	
	echo json_encode($res);
    }
    
    public function save(){
        $this->db->trans_begin();
        error_reporting(E_ALL); 
        function exceptionThrower($type, $errMsg, $errFile, $errLine) { 
          throw new Exception($errMsg); 
        }
        set_error_handler('exceptionThrower'); 
        try {    
            $_POST['code'] = strtoupper($_POST['code']);
            $_POST['branch_code']  = strtoupper($_POST['branch_code']);

            if($_POST['code_'] == "0" || $_POST['code_'] == ""){
                if($this->user_permissions->is_add('m_bank_branch')){

                    $data=$arrayName = array(
                        'bank' =>$_POST['sbank'], 
                        'code'=>$_POST['branch_code'],
                        'branch_code'=>$_POST['code'],
                        'description'=>$_POST['description']
                    );
                    unset($_POST['code_']);
                    $this->db->insert($this->mtb, $data);
                    echo $this->db->trans_commit();
                }else{
                    echo "No permission to save records";
                    $this->db->trans_commit();
                }
            }else{
                if($this->user_permissions->is_edit('m_bank_branch')){
                    $data=$arrayName = array(
                        'bank' =>$_POST['sbank'],
                        'description'=>$_POST['description']
                    );
                    $this->db->where("code", $_POST['code_']);
                    unset($_POST['code_']);
                    $this->db->update($this->mtb, $data);
                    echo $this->db->trans_commit();
                }else{
                    echo "No permission to edit records";
                    $this->db->trans_commit();
                }
            }
            
        }catch(Exception $e){ 
          $this->db->trans_rollback();
          echo "Operation fail please contact admin"; 
        }    
	
    }
    
    public function check_code(){
    	$this->db->where('code', $_POST['code']);
    	$this->db->limit(1);
    	
    	echo $this->db->get($this->mtb)->num_rows();
    }
    
 
    public function delete(){
        $this->db->trans_begin();
        error_reporting(E_ALL); 
        function exceptionThrower($type, $errMsg, $errFile, $errLine) { 
          throw new Exception($errMsg); 
        }
        set_error_handler('exceptionThrower'); 
        try { 
            if($this->user_permissions->is_delete('m_bank_branch')){
            	$this->db->where('code', $_POST['code']);
            	$this->db->limit(1);
            	$this->db->delete($this->mtb);
                echo $this->db->trans_commit();
            }else{
                echo "No permission to delete records";
                $this->db->trans_commit();
            }
        }catch(Exception $e){ 
          $this->db->trans_rollback();
          echo "Operation fail please contact admin"; 
        }   
    }

     public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM m_bank_branch WHERE (code like '%$k%' OR description like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }
    
   
}



