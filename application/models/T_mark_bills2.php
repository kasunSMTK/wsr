<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_mark_bills2 extends CI_Model {

	private $sd;
	private $mtb;
    private $max_no;

	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
	}


	public function base_details(){		
		$a['current_date'] = $this->sd['current_date'];        
        $a['mxno']='';//$this->utility->get_mark_max('F');
        $this->load->model('m_branches');
        $a['bc'] = $this->m_branches->select();

        $a['marked_bills'] = $this->marked_bills();

        return $a;
    }

    public function marked_bills(){

        $Q = $this->db->query(" SELECT *, m_branches.name FROM t_loan_mark_paid 
                                JOIN m_branches ON t_loan_mark_paid.`bc` = m_branches.`bc` 
                                ORDER BY no DESC ");

        if ( $Q->num_rows() > 0 ){

            $t  = '<table class="tbl_mark_bill_list" border="1" align="center" width="100%">';            
            $t .= '<tr>';
            $t .= '<td>No</td>';
            $t .= '<td>Type</td>';
            $t .= '<td>Date</td>';
            $t .= '<td>Branch</td>';
            $t .= '<td>Bill Number</td>';
            $t .= '<td>Date and Time</td>';
            $t .= '<td>Status</td>';
            $t .= '<td>Action</td>';
            $t .= '</tr>';

            foreach ($Q->result() as $R) {
                
                // `type`,`no`,`ddate`,`oc`,`bc`,`action_date`,`comment`,`loan_no`,`billno`,

                $ttype =($R->type=='PI')?'INTEREST PAID':'POSTPONED';

                $t .= '<tr>';
                $t .= '<td>'.$R->no.'</td>';
                $t .= '<td>'.$ttype.'</td>';
                $t .= '<td>'.$R->ddate.'</td>';
                $t .= '<td>'.$R->bc.' - '.$R->name.' </td>';
                $t .= '<td>'.$R->billno.'</td>';
                $t .= '<td>'.$R->action_date.'</td>';
                
                if ($R->is_delete == 1){
                    $st = "<span style='color:red'>Canceled</span>";
                    $link = '';
                }else{
                    $st = "<span style='color:Green'>Marked</span>";
                    $link = '<a class="mark_bill_stat_change" no='.$R->no.' type='.$R->type.' loanno = '.$R->loan_no.' >Unmark this bill</a>';
                }

                $t .= '<td>'.$st.'</td>';


                $t .= '<td>'.$link.'</td>';
                $t .= '</tr>';
            }

            return $t;

        }else{

            return "No marked bills found";

        }

    } 

    public function grid_data(){          
    	
        $bn = $_POST['bn'];

    	$Q = $this->db->query("

            SELECT L.`bc`, L.`loanno`, L.`billno`, L.`ddate`, L.`finaldate`, L.`period`, L.`totalweight`, L.`goldvalue`, L.`billtype`, C.`title`, C.`cusname`, L.`requiredamount`
            FROM `t_loan` L 
            INNER JOIN `m_customer` C ON (L.`cus_serno` = C.`serno`) 
            WHERE L.`status` = 'P' AND L.`billno` = '$bn'
            ORDER BY L.`billtype`,L.`ddate` 
            LIMIT 1

        ");
        
    	if ($Q->num_rows() > 0){

            $R = $Q->row();
            $gd ="<br>";            

            $gd.="<div class='div_bn_mark_view_h' style='text-align:right; margin-right:10px'>";
            $gd.="<table>" ;
            $gd.="<tr><td style='width:100px;text-align:left'>billtype</td><td>".$R->billtype."</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>customer</td><td>".$R->title.$R->cusname."</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>Bill number</td><td>".$R->billno."</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>Date</td><td>".$R->ddate."</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>''</td><td>''</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>Final date</td><td>".$R->finaldate."</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>Period</td><td>".$R->period."</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>Total weight</td><td>".$R->totalweight."</td></tr>";
            $gd.="<tr><td style='width:100px;text-align:left'>Gold weight</td><td>".$R->goldvalue."</td></tr>";
            $gd.="</table>" ;            
            $gd.="<div>";

            $gd.='<br><br><br><br>
                    <div style="border:0px solid red; text-align:center">
                        <div class="pAndL_hold" style="width:auto; display:inline-block">
                            <label for="mb_paid" style="width:100px; display:inline-block">
                                <div class="opt_holder" style="margin:2px;">
                                <input class="" type="radio" id="mb_paid"  name="mb_l_PI" value="PI" />Int Paid
                                </div>
                            </label>
                            <label for="mb_post" style="width:120px; display:inline-block">
                                <div class="opt_holder" style="margin:2px;">
                                <input class="" type="radio" id="mb_post"  name="mb_l_PI" value="PL" />Postpone
                                </div>
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="btnMark" type="button" value="Mark" id="btnMark" class="btn_regular"/>
                            <input name="btnReset" type="button" value="Reset" id="btnReset" class="btn_regular"/>
                        </div>
                    </div>
            ';

            $a['s']       = 1;
    		$a['data']    = $gd;

    	}else{
    		$a['s']       = 0;
            $a['data']    = "No data found";
    	}
    	echo json_encode($a);
    }    

    public function load_old_data(){}

    public function max_no(){
        return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM t_loan_mark_paid")->row()->max_no;
    }

    public function save(){

        $a['s'] = 1;

        echo json_encode($a);

        exit;
        
        $data=$lnNoRm='';
        $lnNo = array();
        $lnNoRm=$_POST['act'];
        $mb_l_P=$_POST['mb_l_P'];
        $no = $this->max_no = $this->utility->get_mark_max($mb_l_P);        
        $ddate=$this->sd['current_date'];
        $oc=$this->sd['oc'];
        $bc=$this->sd['bc'];
        $comment=(empty($_POST['comment']))?"":$_POST['comment'];
        $n = 0;

        $_POST['hid'] = 0;
        $_POST['date'] = $_POST['dDate'];
        
        foreach ($_POST['act'] as $value) {
            
            $lnNo[] = $value;
            
            $dataUp[]= array(
                'loanno' => $value,
                'status' => $mb_l_P,
                'action_date'=>date('Y-m-d h:i:s')
            );
            
            $dataIns[]= array(
                'type'=>$mb_l_P,
                'no' => $no,
                'ddate' => $ddate,
                'oc' => $oc,
                'bc' => $_POST['bcc'][$n],
                'action_date'=>date('Y-m-d h:i:s'),
                'comment' => $comment,
                'loan_no' => $value
            );           
            $n++;        
        }

        $this->db->insert_batch('t_loan_mark_paid', $dataIns);
      
        echo $aa; 
    }

    

    public function mark_bill(){

        $billno = $_POST['billno'];
        $type   = $_POST['type'];
        $r      = $this->db->where('billno',$billno)->limit(1)->get('t_loan')->row();

        $_POST['hid'] = "";
        $_POST['type'] = $type;
        $_POST['no']   = $this->max_no = $this->max_no();
        $_POST['ddate'] = $_POST['date'] = $this->sd['current_date'];
        $_POST['oc'] = $this->sd['oc'];
        $_POST['bc'] = $r->bc;
        $_POST['loan_no'] = $r->loanno;

        $_POST['loan_amount'] = $r->requiredamount;
        $_POST['mark_status'] = $type;
        $_POST['ln'] = $_POST['loan_no'];
        $_POST['bccx'] = $_POST['bc'];
        $_POST['bn']   = $_POST['billno'];

        if ($type == "PI"){ $_POST['trans_code'] = $tc = 100; }
        if ($type == "PL"){ $_POST['trans_code'] = $tc = 101; }

       
        unset($_POST['hid'],$_POST['date'],$_POST['loan_amount'],$_POST['mark_status'],$_POST['ln'],$_POST['bccx'],$_POST['trans_code'],$_POST['bn']);

        $q = $this->db->insert('t_loan_mark_paid',$_POST);    

        if ($q){
            $this->utility->user_activity_log($module='Mark Bill',$action='insert',$trans_code=$tc,$trans_no=$this->max_no,$note='');
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }
        echo json_encode($a);
    }

    public function change_mark_status(){

        $this->db->trans_begin();

        $no         = $_POST['no'];
        $type       = $_POST['type'];
        $loanno     = $_POST['loanno'];

        $Q = $this->db->query("     UPDATE `t_loan_mark_paid`
                                    SET is_delete = 1
                                    WHERE `no` = $no AND `type` = '$type' AND loan_no = '$loanno'
                                    LIMIT 1 ");


        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = 0;
        }else{            
            $this->db->trans_commit();
            if ($type == 'PI'){ $tc = 100; }
            if ($type == 'PL'){ $tc = 101; }
            $this->utility->user_activity_log($module='Mark Bill',$action='cancel',$trans_code=$tc,$trans_no=$no,$note='');
            $a['s'] = 1;            
        }
        echo json_encode($a);
    }
}
