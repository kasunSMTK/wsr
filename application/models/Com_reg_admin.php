<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Com_reg_admin extends CI_Model {

	private $sd;

    function __construct(){
        parent::__construct();		
        $this->sd = $this->session->all_userdata();		
    }

    public function base_details(){
        $a['current_date'] = $this->sd['current_date'];
        return $a;
    }

    public function load(){

        $bc_code = $_POST['bc_code'];
        $Q       = $this->db->query(" SELECT RIGHT(auth_code,10) AS `reg_code` FROM `com_auth` WHERE isAuth = 0 AND LEFT(auth_code,10) = '$bc_code' LIMIT 1 ");

        if ( $Q->num_rows() > 0 ){

            $a['s'] = 1;
            $a['reg_code'] = $Q->row()->reg_code;

        }else{

            $a['s'] = 0;

        }

        echo json_encode($a);
        
    }

}
