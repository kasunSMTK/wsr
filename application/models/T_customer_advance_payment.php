<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_customer_advance_payment extends CI_Model {
    
    private $sd;    
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();   
    }
    
    public function base_details(){             
        $a['current_date'] = $this->sd['current_date'];
        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        //$a['bcCustomer'] = $this->getBcCustomer();
        $a['max_no'] = $this->max_no();
        $a['s'] = 1;
        $a['backdate_upto'] = $this->sd['backdate_upto'];
        return $a;
    }

    public function max_no(){
        return $this->db->query("SELECT IFNULL(MAX(`trans_no`)+1 ,1) AS `max_no` FROM `t_loan_advance_customer`")->row()->max_no;
    }
    
    public function save(){

        $a['s'] = 33;
        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this advance payment bill and you might required re-login to continue";

        if (isset($this->sd['current_date'])){
            if (!$this->utility->is_valid_date($this->sd['current_date'])){
                echo json_encode($a);
                exit;
            }
        }else{            
            echo json_encode($a);
            exit;
        }   $a['s'] = ''; $a['msg_33'] = '';
        
        $this->db->trans_begin();

        $this->load->model('calculate');

        $_POST['customer_id']  = $_POST['client_id'];               
        $_POST['ddate']        = $_POST['date'];
        $_POST['time']         = date('H:i:s');
        $_POST['dr_amount']    = 0;
        $_POST['cr_amount']    = $paying_amount = floatval($_POST['cr_amount']);
        $_POST['note']         = "Customer advance payment";
        $_POST['bc']           = $this->sd['bc'];
        $_POST['oc']           = $this->sd['oc'];
        $_POST['billno']       = $_POST['billno'];
        $_POST['billtype']     = $_POST['billtype_opt'];
        $_POST['loanno']       = $_POST['loanno'];        
        
        /*------ remove comment if interest balance must deduct from paying advance amount -----*/
            $int_balance    = 0;
            $int_balance    = floatval($this->calculate->interest_balance($_POST['loanno'] ,0,0));         
        /* ------------------------------------------------------------------------------------ */

        $_POST['data']['hid_customer_serno']    = "";
        $_POST['data']['card_amount']           = 0;
        $_POST['data']['cash_amount']           = 0;
        $_POST['data']['ddate']                 = $_POST['date'];

        /*if ( $paying_amount >= $int_balance){

            echo "$paying_amount >= $int_balance";  exit;

            $int_paid_untill = $this->db->query("SELECT IFNULL(L.`int_paid_untill`,CURRENT_DATE) as `int_paid_untill` FROM t_loan L WHERE L.`bc` = '".$_POST['bc']."' AND L.`loanno` = '".$_POST['loanno']."' LIMIT 1 ")->row()->int_paid_untill;

            $x = $this->calculate->get_int_paid_till_date($int_paid_untill,0,false,0,0,$_POST['date']);
            $_POST['new_int_paid_untill'] = $x['till_date']; 

            $_POST['claim_by']  = "";                    
            $_POST['amount']    = $int_balance;
            $rtn                = $this->calculate->claim_int();

            $paying_amount -= $int_balance;

            if ($paying_amount  > 0 ){
                $_POST['cr_amount'] = $paying_amount;
                $rtn = $this->calculate->add_customer_advance_amount();                
            }

        }else{
            $rtn = $this->calculate->add_customer_advance_amount();
        }*/


        $rtn = $this->calculate->add_customer_advance_amount();

        $a['max_no'] = $rtn['max_no'];
        $a['no']     = $rtn['no'];
        $a['loanno'] = $rtn['loanno'];
        $a['action'] = $rtn['action'];

        $this->db->trans_commit();        

        echo json_encode($a);
    }

    public function getBcCustomer(){        
        $Q = $this->db->query("SELECT customer_id,nicno,cusname FROM m_customer ORDER BY cusname");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->customer_id ." - ".$R->nicno." -  ".$R->cusname."',"; }
        return  $d;
    }

    public function cus_info_and_det(){

        $bc = $this->sd['bc'];
        $customer_id = $_POST['customer_id'];

        $Q = $this->db->query("SELECT C.*,L.`billtype`, L.`billno`,L.`ddate`,L.`requiredamount`,L.`loanno` FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` WHERE L.bc = '$bc' AND C.`customer_id` = '$customer_id'");
            
        if ($Q->num_rows() > 0){                
            $T = "";
            $R = $Q->row();
            $T .= '<div><h3>Full Name</h3><span>'.$R->title . " " . $R->cusname .'</span></div>
                <div><h3>NIC</h3><span>'.$R->nicno.'</span></div>
                <div><h3>Address 1</h3><span>'.$R->address.'</span></div>
                <div><h3>Address 2</h3><span>'.$R->address2.'</span></div>
                <div><h3>Mobile</h3><span>'.$R->mobile.'</span></div>
                <div><h3>Phone</h3><span>'.$R->telNo.'</span></div>';
            $a['cus_bill_det'] = $Q->result();
            $a['rec']       = $T;
            $a['status']    = 1;
            $a['serno']     = $R->serno;
            $a['customer_id'] = $R->serno . " - " .$R->nicno . " - " . $R->cusname ;
        }else{            
            $a['status']    = 0;
        }

        //----------------------------------------- DET --------------------------------------------

        $Q2 = $this->adv_paid_list($bc,$customer_id);

        if ( $Q2->num_rows() >0 ){
            $a['cus_his'] = $Q2->result();
        }else{
            $a['cus_his'] = 0;
        }

        $this->load->model('user_permissions');
        
        if ($this->user_permissions->is_approve('paid_advance_remove_allowed') == 0){            
            $a['remove_allowed'] = 0;
        }else{
            $a['remove_allowed'] = 1;
        }

        echo json_encode($a);
    }


    public function adv_paid_list($bc,$customer_id){

        return $this->db->query("SELECT bc,is_delete,billno, trans_no,`date`,SUM(cr_amount) as `cr_amount`, SUM(dr_amount) as `dr_amount`, note
FROM `t_loan_advance_customer` 
WHERE /*bc = '$bc' AND */ client_id = '$customer_id' 
GROUP BY trans_no ,trans_type
ORDER BY trans_no desc,`date` desc ");


    }

    public function load(){
        
        $no = $_POST['no'];
        $bc = $this->sd['bc'];
        
        $Q = $this->db->query("SELECT *, C.`cusname`,C.`nicno` , L.loanno
            FROM `t_loan_advance_customer` LAC
            JOIN m_customer C ON LAC.`client_id` = C.`customer_id`
            JOIN `t_loan` L ON L.`billno` = LAC.`billno`
            WHERE LAC.bc = '$bc' AND LAC.trans_no = $no ");

        if ($Q->num_rows() > 0){

            if ($Q->num_rows() > 0){
                $a['sum'] = $Q->row();
                $a['s'] = 1;
            }else{
                $a['s'] = 0;
            }

            $Q2 = $this->adv_paid_list($bc,$Q->row()->client_id);

            if ( $Q2->num_rows() >0 ){
                $a['cus_his'] = $Q2->result();
            }else{
                $a['cus_his'] = 0;
            }

        }else{

            $a['s'] = 0;

        }

        echo json_encode($a);
    }
    

    public function remove_paid_advance(){
        
        $bc     = $_POST['bc'];
        $id     = $_POST['id'];
        $billno = $_POST['billno'];
    
        $this->db->trans_begin();

        // Update "t_loan_advance_customer" id_delete
        $this->db->where(array('trans_type'=>'ADV_P','trans_no'=>$id,'billno'=>$billno,'bc'=>$bc ) )->set('is_delete',1)->limit(1)->update('t_loan_advance_customer');

        // Update account trans 
        $this->db   ->where(array('bc'=>$bc,'trans_code'=>'24','trans_no'=>$id,'billno'=>$billno))
                    ->delete( 't_account_trans' );

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{
            $this->db->trans_commit();
            $this->utility->user_activity_log($module='Customer Advance',$action='remove',$trans_code='24',$trans_no=$id,$note='adv for bill number '.$billno);
            $a['s'] = 1;
        }

        echo json_encode($a);

    }


    public function set_int_bal(){

        $loanno = $_POST['loanno'];

        /*------ remove comment if interest balance must deduct from paying advance amount -----*/
                $a['int_bal'] = 0;
                // $this->load->model('calculate');
                //$a['int_bal'] = $this->calculate->interest_balance($_POST['loanno'] ,0,0);        
        /*--------------------------------------------------------------------------------------*/

        echo json_encode($a);

    }

    

    public function set_bill_adv_total(){

        $billno = $a['bn'] = $_POST['billno'];               
        
        $a['bill_adv_bal'] = floatval($this->db->query("SELECT IFNULL(SUM(a.`cr_amount`) - SUM(a.dr_amount),0) AS `adv_bal` FROM `t_loan_advance_customer` a WHERE a.`is_delete` = 0 AND billno = '$billno'")->row()->adv_bal);
        
        echo json_encode($a);

    }


}