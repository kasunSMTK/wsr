<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

class t_print_salary_voucher extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->sd = $this->session->all_userdata();    
    }

    public function base_details(){
        $a['a'] = 1;
        $a['vou_print_list'] = $this->vou_print_list();
        return $a;
    }


    public function vou_print_list(){

        $bc = $this->sd['bc'];

        $Q = $this->db->query("SELECT 
        
        RS.`bc`,
        B.`name` AS `bc_name`,
        RS.`nno`,
        RS.`paid_acc`,
        CONCAT(RS.`paid_acc`, ' - ' , ACC.`description`) AS `paid_acc_desc`,
        VS.`sal_vou_yr`,
        VS.`sal_vou_mon`,
        VS.`emp_no`,
        CONCAT(VS.`emp_no` , ' - ', VS.note ) AS `paid_to_desc`,
        VS.`payee_desc`,
        RS.`amount`,
        RS.`status`,
        VS.`is_printed`


        FROM `t_gen_vou_app_rqsts_sum` RS

        JOIN `t_voucher_gl_sum`     VS ON RS.`no` = VS.`nno` AND RS.`bc` = VS.`bc` AND RS.`paid_acc` = VS.`paid_acc`
        JOIN `m_branches`       B  ON RS.`bc` = B.`bc`
        JOIN `m_account`        ACC ON RS.`paid_acc` = ACC.`code`

        WHERE RS.vocuher_type = 'salary_vouchers' AND RS.`status` = 'A' AND RS.`bc` = '$bc' ");


        $T = '';

        if ( $Q->num_rows() > 0 ){

            foreach ($Q->result() as $R ) {
                
                $T .= "<tr>";
                $T .= "<td>".$R->bc_name."</td>";
                $T .= "<td>".$R->nno."</td>";
                $T .= "<td>".$R->paid_acc_desc."</td>";
                $T .= "<td>".$R->paid_to_desc."</td>";                
                $T .= "<td align='right'>".$R->amount."</td>";
                $T .= "<td>".$R->is_printed."</td>";
                //$T .= "<td><input type='checkbox' class='chk_each_vou_print'  style='width:20px;height:20px'></td>";
                
                if ($R->is_printed == 0){
                    $T .= "<td><input type='button' value='Print'       class='btn_vou_print btn_regular' bc='".$R->bc."' nno='".$R->nno."' paid_acc='".$R->paid_acc."' v_type='cash' tot='".$R->amount."' is_printed='".$R->is_printed."'>";
                    $T .= "<input type='hidden'     value='".$R->is_printed."'  name='is_printed[]'>";
                    $T .= "<input type='hidden'     value='cash'              name='v_type[]'>";
                    $T .= "<input type='hidden'     value='".$R->bc."'          name='bc[]'>";
                    $T .= "<input type='hidden'     value='".$R->nno."'         name='nno[]'>";
                    $T .= "<input type='hidden'     value='".$R->paid_acc."'    name='paid_acc[]'></td>";
                }else{
                    $T .= "<td><input type='button' value='Re-Print'    class='btn_vou_print btn_regular' bc='".$R->bc."' nno='".$R->nno."' paid_acc='".$R->paid_acc."' v_type='cash' tot='".$R->amount."'  is_printed='".$R->is_printed."'>";
                    $T .= "<input type='hidden'     value='".$R->is_printed."'  name='is_printed[]'>";
                    $T .= "<input type='hidden'     value='cash'              name='v_type[]'>";
                    $T .= "<input type='hidden'     value='".$R->bc."'          name='bc[]'>";
                    $T .= "<input type='hidden'     value='".$R->nno."'         name='nno[]'>";
                    $T .= "<input type='hidden'     value='".$R->paid_acc."'    name='paid_acc[]'></td>";
                }

                $T .= "</tr>";

            }

        }


        return $T;

    }



    public function save(){

        

    }




    public function PDF_report(){

    if (isset($_POST['h_v_bc'])){
      if ($_POST['h_v_bc'] != ""){
        $bc = $_POST['h_v_bc'];
      }else{
        $bc = $this->sd['branch'];
      }
    }else{
      $bc = $this->sd['branch'];
    }
    
    $this->db->select(array('name'));
    $r_detail['company'] = $this->db->get('m_company')->result();
    $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $bc);
    $r_detail['branch'] = $this->db->get('m_branches')->result();
    
    $invoice_number      = $this->utility->invoice_format($_POST['r_nno']);
    
    $session_array       = array(      
      $invoice_number
    );

    $r_detail['session'] = $session_array;
    
    $this->db->where("code", $_POST['sales_type']);
    $query = $this->db->get('t_trans_code');
    
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $r_detail['r_type'] = $row->description;
      }
    }
 
    
    $r_detail['type']         = $_POST['type'];
    $r_detail['dt']           = $_POST['dt'];
    $r_detail['qno']          = $_POST['qno'];
    $r_detail['voucher_type'] = $_POST['voucher_type'];
    
    $r_detail['voucher_no']  = $_POST['qno'];
    $r_detail['category_id'] = $_POST['category_id'];
    $r_detail['cat_des']     = $_POST['cat_des'];
    $r_detail['group_id']    = $_POST['group_id'];
    $r_detail['group_des']   = $_POST['group_des'];
    $r_detail['ddate']       = $_POST['r_ddate'];
    $r_detail['tot']         = $_POST['tot'];
    
    
    
    $r_detail['num'] = $_POST['tot'];
    
    $num = $_POST['tot'];
    
    $this->utility->num_in_letter($num);
    
//-------------------------------------------------

    $r_detail['rec'] = convertNum($num);
    ;
    
    $r_detail['page']        = $_POST['page'];
    $r_detail['header']      = $_POST['header'];
    $r_detail['orientation'] = $_POST['orientation'];
    
    $r_detail['acc_code'] = $_POST['acc_code'];
    $r_detail['acc_des']  = $_POST['acc_des'];
    $r_detail['vou_des']  = $_POST['vou_des'];
    
     $sql=" SELECT VD.acc_code, VD.amount, MA.description 
            FROM t_voucher_gl_det VD
            JOIN m_account MA ON VD.acc_code = MA.code 
            WHERE VD.cl='".$this->sd['cl']."' AND VD.bc='".$bc."' AND VD.`type`= '".$_POST['voucher_type']."'  AND VD.`paid_acc` = '".$_POST['r_paid_acc']."' AND VD.`nno` = ".$_POST['r_nno']." ";

    
    $r_detail['dets'] = $this->db->query($sql)->result(); 

    $sql="SELECT * FROM t_voucher_gl_sum 
          WHERE cl='".$this->sd['cl']."' AND bc='".$bc."' 
          AND `nno` = ".$_POST['r_nno']." AND `type`= '".$_POST['voucher_type']."'
          AND paid_acc = '".$_POST['r_paid_acc']."' ";
    
    $r_detail['sum'] = $this->db->query($sql)->result(); 
    
  
    $sql="SELECT * FROM t_cheque_issued WHERE trans_code='48' 
          AND cl='".$this->sd['cl']."' AND bc='".$bc."'
          AND account = '".$_POST['r_paid_acc']."' AND trans_no = '".$_POST['r_nno']."'";
    
    $r_detail['cheque'] = $this->db->query($sql)->result();       



    // $this->db->select(array(
    //   'name'
    // ));
    // $this->db->where("code", $_POST['salesp_id']);
    // $query = $this->db->get('m_employee');
    
    //foreach ($query->result() as $row) {
      //$r_detail['employee'] = $row->name;
   // }
    
    $this->db->select(array('loginName'));
    $this->db->where('cCode', $this->sd['oc']);
    $r_detail['user'] = $this->db->get('u_users')->result();
    
    $this->db->where(array(
        "nno"       => $_POST['r_nno'],
        "bc"        => $_POST['bc'],
        "paid_acc"  => $_POST['r_paid_acc'])

    )->set("is_printed",1)->limit(1)->update('t_voucher_gl_sum');
    
    $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);

  }

            

}