
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_general_recipt_Details extends CI_Model{
    
  private $sd;
  private $mtb;
  
  private $mod = '003';
  
  function __construct(){
    parent::__construct();

    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);
    $this->mtb = $this->tables->tb['t_privilege_card'];
    $this->m_customer = $this->tables->tb['m_customer'];
    $this->t_sales_sum=$this->tables->tb['t_sales_sum'];
    $this->t_previlliage_trans=$this->tables->tb['t_previlliage_trans'];
    $this->t_privilege_card=$this->tables->tb['t_privilege_card']; 
  }
  
  public function base_details(){
      
    $a['table_data'] = $this->data_table();

    return $a;
  }
  

  public function PDF_report(){
        
    $this->db->select(array('name'));
    $r_detail['company'] = $this->db->get('m_company')->result();
    $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $this->sd['branch']);
    $r_detail['branch'] = $this->db->get('m_branches')->result();

    $cl=$this->sd['cl'];
    $bc=$this->sd['branch'];

    $r_detail['page']='A4';
    $r_detail['orientation']='L'; 

    $cluster=$this->sd['cl'];
    $branch=$_POST['branch'];
    $acc=$_POST['acc_code'];
    $no_range_frm=$_POST['t_range_from'];
    $no_range_to=$_POST['t_range_to'];

    $r_detail['acc']=$_POST['acc_code'];
    $r_detail['t_no_from']=$_POST['t_range_from'];
    $r_detail['t_no_to']=$_POST['t_range_to'];

    $r_detail['cluster1']=$cluster;
    $r_detail['branch1']=$branch;
    $r_detail['acc']=$_POST['acc_code'];
    $r_detail['acc_des']=$_POST['acc_code_des'];
    
   

    $date_range=$_POST['from'];
    $acc_code=$_POST['acc_code'];
    $no_range=$_POST['t_range_from'];

     if(isset($_POST['chknumRange']))
    {
      $checkR=1;
    }else{
      $checkR=0;
    }

    if(isset($_POST['chkdate1']))
    {
      $checkD=1;
    }else{
      $checkD=0;
    }

    if($checkD==1)
    {

      $r_detail['dfrom']=$_POST['from'];
      $r_detail['dto']=$_POST['to'];

      $date1="AND ddate between'".$_POST['from']."' AND '".$_POST['to']."'";
    }
    else
    {
      $date1="";

      $r_detail['dfrom']="";
      $r_detail['dto']="";
    }

    
   
    if($acc_code!=""){
      $acc1="AND pettycash_account='".$_POST['acc_code']."'";
    }
    else
      $acc1="";

    if($checkR==1){

      $r_detail['t_no_from']=$_POST['t_range_from'];
      $r_detail['t_no_to']=$_POST['t_range_to'];

      $num_range="AND nno between'".$_POST['t_range_from']."' AND '".$_POST['t_range_to']."'";
      }
    else
    {
      $num_range="";
      $r_detail['t_no_from']="";
      $r_detail['t_no_to']="";
    }
    
    if(!empty($cluster)) 
    {
       $cluster1= "AND cl ='".$_POST['cluster']."'";
    }
    else
     $cluster1="";

    if(!empty($branch)) 
    {
       $branch1= " AND bc ='".$_POST['branch']."'";
    }
    else
     $branch1="";

  
    
    
    $sql="SELECT
            (SELECT
                  COUNT(*) 
                FROM
                t_receipt_gl_sum
                WHERE `is_cancel` = '0' $date1 $num_range $acc1
                 $cluster1 $branch1) AS counts,  
            RGS.`nno`,
            RGS.`ddate`,
            RGS.`type`,
            RGS.`note`,
            RGS.`paid_acc`,
            RGS.`cheque_amount`,
            RGS.`cash_amount`,
            MA.`description`,
            RGD.`acc_code`,
            RGD.`amount`,
            RGD.`cl`,
            RGD.`bc`,
            RGS.`cl` AS cl,
            RGS.`bc` AS bc,
            RGD.`nno` AS Dnno,
            RGD.`ref_no`,
            MAC.`description` AS acc_name,
            MBC.`name`AS bc_name, 
            MCL.`description` AS cl_description  
            FROM
            `t_receipt_gl_sum` AS RGS
            LEFT JOIN m_account AS MA 
                ON (MA.code=RGS.`paid_acc`)
            LEFT JOIN t_receipt_gl_det AS RGD 
                ON (RGS.`nno`=RGD.`nno`)
            LEFT JOIN m_account AS MAC 
                ON (MAC.code=RGD.`acc_code`)
            INNER JOIN `m_branches` MBC 
                ON RGS.bc = MBC.bc 
            INNER JOIN `m_cluster` MCL 
                ON (MBC.`cl` = MCL.`code`)
        
            WHERE `is_cancel` = '0'";

        if($checkD==1)
        {
           $sql.=" AND RGS.ddate between '".$_POST['from']."' AND '".$_POST['to']."'";
        }
        if(!empty($_POST['acc_code']))
        {
           $sql.=" AND MA.`code` = '$acc'";
        }
        
        if(!empty($cluster))
        {
           $sql.=" AND RGS.cl = '$cluster' ";
        }
       if(!empty($branch))
        {
            $sql.=" AND  RGS.bc = '$branch'";
        }
         
        if(!empty($_POST['t_range_from']) && empty($_POST['t_range_to']))
        {
           $sql.=" AND  RGS.nno >= '$no_range_frm'";
        }
        
         if(!empty($_POST['t_range_to']) && empty($_POST['t_range_from']))
        {
           $sql.=" AND RGS.nno <= '$no_range_to'";
        }

        if(!empty($_POST['t_range_to']) && !empty($_POST['t_range_to']))
        {
           $sql.=" AND  RGS.nno BETWEEN  '$no_range_frm' AND '$no_range_to'";
        }
         $sql.=" ORDER BY RGS.cl,RGS.bc,RGS.nno ASC";
            
        $query=$this->db->query($sql);


    $sql_branch="SELECT 
                        MBC.`name`,
                        MCL.`description`,
                        MCL.`code`,
                         MBC.`bc`   
                        FROM
                            `m_branches` MBC
                            INNER JOIN `m_cluster` MCL
                                ON (MBC.`cl` = MCL.`code`) WHERE MBC.`bc`='$bc' AND MCL.`code`='$cl'";  
        
        
                    
          
    $r_detail['sum']=$this->db->query($sql)->result();       
    $r_detail['r_branch_name']=$this->db->query($sql_branch)->result();       
    
   
    //$this->load->view($_POST['by'].'_'.'pdf',$r_detail);

    if($this->db->query($sql)->num_rows()>0)
    {
        $this->load->view($_POST['by'].'_'.'pdf',$r_detail);
    }

    else
    {
        echo "<script>alert('No Data');window.close();</script>";
    }
  }
}