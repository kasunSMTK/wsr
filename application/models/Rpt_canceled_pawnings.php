<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_canceled_pawnings extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function base_details(){		
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    
    
    // if ( $_POST['bc'] == "" ){        
    //     $bc = "";
    // }else{
        
    //     $bc   = "L.bc = '".$_POST['bc']."' AND ";
    // }

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){ $bc = "";}        
    }else{
        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }
        $bc = implode(',', $bc_ar);
        if ($bc == ""){$bc   = "";}else{$bc   = "   L.bc IN ($bc) AND ";}
    }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    $Q = $this->db->query("SELECT U.`display_name`,L.bc,B.`name` AS `bc_name`,L.`loanno`,  L.`billtype`,L.`billno`,L.`bt_letter`,  L.`ddate`,C.`cusname`,  C.`address`,C.`nicno`,  C.`mobile`,L.`totalweight`,  LI.items AS `items`,L.`requiredamount`
FROM `t_loan_re_fo` L 
  
JOIN (  SELECT GROUP_CONCAT(CONCAT(i.`itemname`,'(',li.`pure_weight`,')')) AS `items`,li.`bc`,li.`billtype`,li.`billno` , SUM( li.pure_weight ) as `totalweight`
    FROM `t_loanitems_re_fo` li 
    JOIN `r_items` i ON li.`itemcode` = i.`itemcode`
    GROUP BY li.`bc`,li.`billtype`,li.`billno` 

) LI ON L.`bc` = LI.`bc` AND LI.`billtype` = L.`billtype` AND LI.`billno` = L.`billno`

JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
JOIN `m_branches` B ON L.`bc` = B.`bc`

JOIN `t_bill_cancel_log` CL ON L.`billno` = CL.`billno`
JOIN u_users U ON CL.`cancelled_by` = U.`cCode`

WHERE  $bc L.ddate BETWEEN '$fd' AND '$td' AND L.`status` = 'C' $billtype
GROUP BY L.`bc`,L.`billtype`,L.`billno`
ORDER BY L.bc, L.loanno, L.ddate ");

    if($Q->num_rows() > 0){

        $r_data['list'] = $Q->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

}