<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_unredeem_forfeited extends CI_Model {

  private $sd;    

  function __construct(){
    parent::__construct();        
    $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
  }   


  function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,'XL');        
    }

  public function PDF_report($_POST_,$type='PDF'){

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = " AND a.bc IN ($bc)  ";
        }
    }

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];


    if(!empty($_POST['r_billtype'])){
      $bt = " AND a.billtype='".$_POST['r_billtype']."'";
    }else{
      $bt ="";
    }

    //$tbl = 'loan_tblz_union';
    $tbl = 't_loan';


    $Q = $this->db->query("SELECT a.billtype, a.bc, B.`name` AS bc_name ,  a.bc , SUM(a.no_of_bills_30) AS `no_of_bills_30` , SUM(a.no_of_bills_60) AS `no_of_bills_60` , SUM(a.no_of_bills_90) AS `no_of_bills_90` , SUM(a.no_of_bills_120) AS `no_of_bills_120` , SUM(no_bills_over_120) AS `no_bills_over_120` FROM (

SELECT L.billtype , L.`bc`, L.requiredamount AS `no_of_bills_30` , 0 AS `no_of_bills_60` , 0 AS no_of_bills_90 , 0 AS no_of_bills_120 , 0 AS no_bills_over_120 , ADDDATE('$td',INTERVAL 30 DAY)
FROM $tbl L
WHERE L.`finaldate` BETWEEN '$td' AND ADDDATE('$td',INTERVAL 30 DAY)

UNION ALL

SELECT L.billtype , L.`bc`, 0 AS `no_of_bills_30` , L.requiredamount AS `no_of_bills_60` , 0 AS no_of_bills_90 , 0 AS no_of_bills_120 , 0 AS no_bills_over_120 , ADDDATE(ADDDATE('$td',INTERVAL 31 DAY), INTERVAL 30 DAY)
FROM $tbl L
WHERE L.`finaldate` BETWEEN ADDDATE('$td',INTERVAL 31 DAY) AND ADDDATE(ADDDATE('$td',INTERVAL 31 DAY), INTERVAL 30 DAY)

UNION ALL

SELECT L.billtype , L.`bc`, 0 AS `no_of_bills_30` , 0 AS `no_of_bills_60` , L.requiredamount AS no_of_bills_90 , 0 AS no_of_bills_120 , 0 AS no_bills_over_120 , ADDDATE(ADDDATE(ADDDATE('$td',INTERVAL 32 DAY), INTERVAL 30 DAY),INTERVAL 30 DAY)
FROM $tbl L
WHERE L.`finaldate` BETWEEN ADDDATE(ADDDATE('$td',INTERVAL 32 DAY), INTERVAL 30 DAY) AND ADDDATE(ADDDATE(ADDDATE('$td',INTERVAL 32 DAY), INTERVAL 30 DAY),INTERVAL 30 DAY)

UNION ALL

SELECT L.billtype , L.`bc`, 0 AS `no_of_bills_30` , 0 AS `no_of_bills_60` , 0 AS no_of_bills_90 , L.requiredamount AS no_of_bills_120 , 0 AS no_bills_over_120 , ADDDATE(ADDDATE(ADDDATE(ADDDATE('$td',INTERVAL 33 DAY), INTERVAL 30 DAY),INTERVAL 30 DAY),INTERVAL 30 DAY)
FROM $tbl L
WHERE L.`finaldate` BETWEEN ADDDATE(ADDDATE(ADDDATE('$td',INTERVAL 33 DAY), INTERVAL 30 DAY),INTERVAL 30 DAY) AND ADDDATE(ADDDATE(ADDDATE(ADDDATE('$td',INTERVAL 33 DAY), INTERVAL 30 DAY),INTERVAL 30 DAY),INTERVAL 30 DAY)

UNION ALL

SELECT L.billtype , L.`bc`, 0 AS `no_of_bills_30` , 0 AS `no_of_bills_60` , 0 AS no_of_bills_90 , 0 AS no_of_bills_120 , L.requiredamount AS no_bills_over_120 , ADDDATE(ADDDATE(ADDDATE(ADDDATE('$td',INTERVAL 33 DAY), INTERVAL 30 DAY),INTERVAL 30 DAY),INTERVAL 30 DAY)
FROM $tbl L
WHERE L.`finaldate` > ADDDATE(ADDDATE(ADDDATE(ADDDATE('$td',INTERVAL 33 DAY), INTERVAL 30 DAY),INTERVAL 30 DAY),INTERVAL 30 DAY)

) a 

JOIN m_branches B ON a.bc = B.bc

WHERE 1 = 1 $bc $bt

GROUP BY a.bc

ORDER BY a.bc ");

    
    if($Q->num_rows() > 0){

      $r_data['list'] = $Q->result();      
      $r_data['td'] = $td;

      if ($type == 'PDF'){
        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
      }else{
        $this->load->view($_POST['by'].'_'.'excel',$r_data);
      }

    }else{
      echo "<script>location='default_pdf_error'</script>";
    }

  }

}