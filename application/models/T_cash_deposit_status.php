<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_cash_deposit_status extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
        $a['chq_stat'] = $this->chq_stat();
		return $a;
    }



    public function chq_stat(){

        if (isset($_POST['json'])){

            $fd = $_POST['fd'];
            $td = $_POST['td'];
            $json = true;
            $Q1   = " AND A.`status` = 10 ";

        }else{
            
            $fd = date('Y-m-d');
            $td = date('Y-m-d');
            $json = false;
            $Q1 = "";

        }

        $q = $this->db->query(" SELECT 

F.`no`,
F.`date_time`,
B1.`name` AS `from_bc_name`,
B1.`bc`,
F.`ref_no`,
F.`amount`,
F.`status` as `ss`

FROM t_fund_transfer_sum F

JOIN m_branches B1 ON F.`from_bc` = B1.`bc`

WHERE transfer_type = 'bank_deposit' AND DATE(F.`date_time`) BETWEEN '$fd' AND '$td'

ORDER BY B1.`name` ");

        if ($q->num_rows() > 0){

            if ($json){
                $ttx = '<span class="title_hid_txt">(Approved)</span>';
            }else{
                $ttx = '';
            }


                $t  = '<table class="tbl_cheque_status" width="100%" id="table">';

                $s = $sx = '';

            foreach ($q->result() as $r) {

                if ($s != $r->from_bc_name){
                    $t .= '<tr class="row_bc" cl="'.str_replace(" ", "", $r->from_bc_name ).'">';
                    $t .= '<td colspan="3">'.$r->from_bc_name.'</td>';
                    $t .= '<td width="150"><input placeholder="Search amount" class="chq_stat_cchu_no_search" cl = "'.$r->from_bc_name.'_chq" ></td>';
                    $t .= '<td colspan="2" align="right"><input type="checkbox" value="'.$r->bc.'" class="print_bc"></td>';
                    $t .= '</tr>';
                    
                    $t .= '<tr class="'.str_replace(" ", "", $r->from_bc_name).' all_rows" style="display:none" bgColor="#f9f9f9">';
                    $t .= '<td>Fund Request No</td>';
                    $t .= '<td>Depositing Branch</td>';
                    $t .= '<td>Deposited Bank</td>';
                    $t .= '<td>Amount</td>';                
                    $t .= '<td>Status</td>';
                    $t .= '<td>Date amd Time</td>';
                    $t .= '</tr>';
                    
                    $s = $r->from_bc_name;
                    $sx= $r->from_bc_name."_chq";
                }                

                $t .= '<tr class="'.str_replace(" ", "", $r->from_bc_name).' all_rows" style="display:none">';
                $t .= '<td>'.$r->no.'</td>';
                $t .= '<td>'.$r->from_bc_name.'</td>';
                $t .= '<td>'.$r->ref_no.'</td>';
                $t .= '<td class="'.$sx.'">'.$r->amount.'</td>';                
                
                $st = '';

                if ($r->ss == 1){
                    $st = '<span style="color:#000000">Branch waiting for head office respond</span>';
                }elseif ($r->ss == 2) {
                    $st = '<span style="color:green">Accepted by head office. Branch must print voucher</span>';
                }elseif ($r->ss == 3) {
                    $st = '<span style="color:red">Request reject by head office</span>';
                }elseif ($r->ss == 4) {
                    $st = '<span style="color:#333333">Voucher print from branch.</span>';
                }elseif ($r->ss == 6) {
                    $st = '<span style="color:red">Request canceled by head office</span>';
                }elseif ($r->ss == 10) {
                    $st = '<span style="color:green"><b>Transaction completed</b></span>';                
                }else{
                    $st = $r->ss;
                }


                $t .= '<td>'.$st.'</td>';


                $t .= '<td>'.$r->date_time.'</td>';
                $t .= '</tr>';

            }

                $t .= '</table>';

        }else{

            $t = 'No data found';

        }

        


        if ($json){
            $a['data'] = $t;
            echo json_encode($a);
        }else{
            return $t;
        }

    }
        
    
}