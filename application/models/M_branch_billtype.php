<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_branch_billtype extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){

        $_POST['oc'] = $this->sd['oc'];
       
            if ($_POST['hid'] == "0" && $_POST['hid2'] == "0"){

/*                $QS = $this->db->query("SELECT bc,bill_type FROM m_bill_type_branch WHERE bc = '".$_POST['bc']."' AND bill_type = '".$_POST['bill_type']."' ")->num_rows();
                
                if ($QS == 0){
                    unset($_POST['hid'],$_POST['hid2']);
                	$Q = $this->db->insert("m_bill_type_branch",$_POST);
                    if ($Q){ echo 1; }else{ echo 0; }
                }else{
                    echo 3;
                }*/

            unset($_POST['hid'],$_POST['hid2']);

            if ($this->db->insert("m_bill_type_branch",$_POST)) {                
                 $a['s'] = 1;
                }
            else {
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Branch, Bill Type");
            }




            }else{
                $hid = $_POST['hid'];
                $hid2 = $_POST['hid2'];
                unset($_POST['hid'],$_POST['hid2']);
                $this->db->where(array("bc"=>$hid , "bill_type"=>$hid2 ))->update("m_bill_type_branch",$_POST);
                $a['s'] = 1;
            }

          echo json_encode($a);          
        
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $bc = $_POST['bc'];
       $bill_type = $_POST['bill_type'];

       if ($this->db->query("DELETE FROM`m_bill_type_branch` WHERE bc = '$bc' AND bill_type = '$bill_type' ")){
             $a['s'] = 1;

        }else {

            $a['s'] = 0; 
            $a['n'] = $this->db->conn_id->errno; 
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Branch");
        }
        echo json_encode($a);
    }

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `m_bill_type_branch` ORDER BY `bc`,`bill_type` DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->bc . "</td><td class='D'>-</td><td class='Des'>" .$R->bill_type. "</td><td class='ED'><a href=# onClick=setEdit('".$R->bc."','".$R->bill_type."')>Edit</a> | <a href=# onClick=setDelete('".$R->bc."','".$R->bill_type."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No branch bill types found</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;        
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $bc = $_POST['bc'];
        $bill_type = $_POST['bill_type'];
        $R = $this->db->query("SELECT * FROM `m_bill_type_branch` WHERE bc = '$bc' AND bill_type = '$bill_type' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `m_bill_type_branch` WHERE (bc like '%$k%' OR bill_type like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->bc . "</td><td class='D'>-</td><td class='Des'>" .$R->bill_type. "</td><td class='ED'><a href=# onClick=setEdit('".$R->bc."','".$R->bill_type."')>Edit</a> | <a href=# onClick=setDelete('".$R->bc."','".$R->bill_type."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No branch bill types found</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }     
    
    

}