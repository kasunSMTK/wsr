<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_pwing_periodical extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function base_details(){		
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $bc = $_POST_['h_bc'];
    
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    $data_range=$_POST_['h_range'];

    if ($bc == ""){
        $QBC = "";        
    }else{
        $QBC = "l.`bc`='$bc' AND ";        
    }

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        if ( !is_array($_POST['bc_arry']) ){
            
            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
            
            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
            }

            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
        }

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bcc   = "";
        }else{
            $bcc   = " L.bc IN ($bc) AND ";
        }
    }   

    if ($data_range == "months") {
        $Q =$this->db->query(" 
        SELECT L.bc, DATE_FORMAT(ddate,'%Y-%m') AS `m`, SUM(L.`requiredamount`) AS `pawn_amount` 
        FROM `loan_tblz_union` L 
        WHERE $bcc L.`status` IN ('P','AM','R','RN','L','PO') AND L.ddate BETWEEN '$fd' AND '$td' 
        GROUP BY bc,DATE_FORMAT(ddate,'%Y-%m') ");
    
    }

    if ($data_range == "years") {
        $Q =$this->db->query(" 
        SELECT L.bc, DATE_FORMAT(ddate,'%Y') AS `m`, SUM(L.`requiredamount`) AS `pawn_amount` 
        FROM `loan_tblz_union` L 
        WHERE $bcc L.`status` IN ('P','AM','R','RN','L','PO') AND L.ddate BETWEEN '$fd' AND '$td' 
        GROUP BY bc,DATE_FORMAT(ddate,'%Y') ");
    
    }

    if ($data_range == "quarter") {
        $Q =$this->db->query(" 
        SELECT bc,YEAR(ddate) AS `y`, QUARTER(ddate) AS `q`, SUM(L.requiredamount) AS `pawn_amount`
        FROM `loan_tblz_union` L 
        WHERE $bcc L.`status` IN ('P','AM','R','RN','L','PO') AND L.ddate BETWEEN '$fd' AND '$td' 
        GROUP BY bc,YEAR(ddate), QUARTER(ddate) ");
    
    }
    
    if($data_range == "half_year"){ 
        $Q =$this->db->query("SELECT bc, YEAR(ddate) AS `y`,FLOOR( ( MONTH(ddate) - 1 ) / 6 ) AS q, SUM(L.`requiredamount`) AS `pawn_amount` FROM `loan_tblz_union` L WHERE $bcc L.`status` IN ('P','AM','R','RN','L','PO') AND L.ddate BETWEEN '$fd' AND '$td'GROUP BY bc,YEAR(ddate), FLOOR( ( MONTH(ddate) - 1 ) / 6 ) ORDER BY `y`, q"); 
    }


    if($Q->num_rows() > 0){

        $r_data['list'] = $Q->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;
        $r_data['bc_list'] = $this->db->query('SELECT bc,`name` AS `bc_name` FROM m_branches ORDER BY NAME')->result();
        $r_data['bcc']  = $this->db->select(array('bc','name'))->get('m_branches')->result();
        $r_data['d_range'] = $data_range;
        $r_data['tmp_tbl_id'] = $tmp_tbl_id;

        $this->load->view($_POST['by'].'_'.'excel',$r_data);
    

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

public function Excel_report()
    {
        $this->PDF_report($_POST,"Excel");
    }

}