<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class rpt_sales_receipt_sum extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    public function Excel_report()
    {

        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST, 'XL');
    }

    public function PDF_report($a, $rt = 'PDF')
    {

        if ($_POST['bc_arry'] === null) {
            $BC = " ";
        } else {

            for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                $bc_ar[] = "'" . str_replace(",", "", $_POST['bc_arry'][$n]) . "'";
            }

            $bc = implode(',', $bc_ar);
            $BC = " AND s.bc IN ($bc)  ";
        }

        // var_dump($bc,$BC);
        // exit();
        $bc = $_POST["from_date"];
        $td = $_POST["to_date"];
        $fd = $_POST["from_date"];

        $q = $this->db->query("
                                SELECT s.* ,c.`cusname`, t.`net_amount`,cs.`sub_trans_no`,cs.`trans_no`

                        FROM `t_sales_receipt_sum` s
                        INNER JOIN `m_customer` c ON c.`serno` =s.`cus_serial`
                        INNER JOIN `t_sales_cus_settlement` cs ON cs.`trans_no` = s.`nno`
                        INNER JOIN `t_sales_sum` t ON t.`nno` = cs.`sub_trans_no`

                        WHERE $bc  AND s.`date` 
                         BETWEEN  '$fd' AND '$td' 
                         AND cs.`trans_code`=107

                        GROUP BY s.`nno`
                    ");

        if (($q->num_rows() > 0)) {

            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            if ($rt == "PDF") {
                $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
            } else {
                echo "<script>alert('Excel Report Not Found');close();</script>";
            }

        } else {
            echo "<script>alert('No data found');close();</script>";
        }

    }

}
