<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_audit extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1; 
        $a['IfPDF'] = $this->utility->IfPDF();
        $a['isAdmin'] = $this->sd['isAdmin'];

        $this->load->model('m_branches');
        $a['bc_dropdown'] = $this->m_branches->select_own_bc();
        $a['billtype_dropdown'] = $this->billtype_dropdown();

		return $a;
    }   

    public function billtype_dropdown(){


        $T = "<select id='r_billtype' name='r_billtype'  class='input_ui_dropdown input_text_regular_new_pawn' style='border:2px solid Green; padding:3px'>";

        $q = $this->db->query("SELECT billtype FROM `r_bill_type_sum` GROUP BY billtype");
        
        $T .= "<option value=''>All</option>";
        foreach ($q->result() as $r) {
            $T .= "<option value='".$r->billtype."'>".$r->billtype."</option>";            
        } $T .= "</select>";

        return $T;
    }
    
}