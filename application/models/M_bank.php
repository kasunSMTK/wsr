<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_bank extends CI_Model {
    
    private $sd;
    private $mtb;
    private $m_item;
    private $m_sub;
    
    function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->mtb = 'tblusers';  
    // $this->load->database($this->sd['db'], true);
    $this->load->model('user_permissions');    
    $this->mtb = $this->tables->tb['m_bank'];

    }
    
    public function base_details(){
        $a['load_list'] = $this->load_list();
        return $a;
    }

   public function load(){
       echo $this->load_list();  
    }
    
   
    public function load_list(){
        $Q = $this->db->query("SELECT * FROM m_bank ORDER BY code DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;        
        
        return json_encode($a);   
    }


      public function set_edit(){
        $code = $_POST['code'];
         
        $R = $this->db->query("SELECT * FROM `m_bank` WHERE code = '$code' LIMIT 1")->row();
        echo json_encode($R);

    }
    
    public function check_exist($root){
    $this->db->select('code');
    $this->db->where('description', $root);
    $this->db->or_where('code', $root);
    $this->db->limit(1);
    $query = $this->db->get($this->mtb);
    
    if($query->num_rows){
        return $query->first_row()->code;
    }else{
        return false;
    }
    }


    
    public function save(){
        if($_POST['hid'] == "0" || $_POST['hid'] == ""){
            if($this->user_permissions->is_add('m_bank')){
                unset($_POST['hid']);
                echo $this->db->insert($this->mtb, $_POST);
            }else{
                echo "No permission to save records";
            }
        }else{
            if($this->user_permissions->is_edit('m_bank')){

                $this->db->where("code", $_POST['hid']);
                unset($_POST['hid']);
                echo  $this->db->update($this->mtb, $_POST);
            }else{
                echo "No permission to edit records";
            }
        }
    }
    
    public function check_code(){
    $this->db->where('code', $_POST['code']);
    $this->db->limit(1);
    echo $this->db->get($this->mtb)->num_rows();
    }
    

    // public function delete_validation(){
    //     $status=1;
    //     $codes=$_POST['code'];
    //     $check_cancellation = $this->utility->check_account_trans($codes,'bank','m_bank','opt_credit_card_det','bank_id');
    //     if ($check_cancellation != 1) {
    //       return $check_cancellation;
    //     }
    //     return $status;
        
    // }
    
    public function delete(){
        $this->db->trans_begin();
        error_reporting(E_ALL); 
        function exceptionThrower($type, $errMsg, $errFile, $errLine) { 
            throw new Exception($errLine); 
        }
        set_error_handler('exceptionThrower'); 
        try {
            if($this->user_permissions->is_delete('m_bank')){
                //$delete_validation_status=$this->delete_validation();
                //$delete_validation_status=1;
               //  if($delete_validation_status==1){
                    $this->db->where('code', $_POST['code']);
                    $this->db->limit(1);
                    $this->db->delete($this->mtb);
                    echo $this->db->trans_commit();
                // }else{
                //     echo $delete_validation_status;
                //     $this->db->trans_commit();
                // }    
            }else{
                echo "No permission to delete records";
                $this->db->trans_commit();
            }
        } catch ( Exception $e ) { 
            $this->db->trans_rollback();
            echo $e->getMessage(). "Operation fail please contact admin"; 
        }     
    }


    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `m_bank` WHERE (code like '%$k%' OR description like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No Bank added</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }
    
    
    public function select($name="bank",$id="bank",$class=""){

        $q = $this->db->query(" SELECT `code`,`description` FROM `m_bank` ORDER BY description ");

        $t  = '<select name="'.$name.'" id="'.$id.'" class="'.$class.'">';

        if ($q->num_rows() > 0 ){
            
                $t .= '<option value="">Select</option>';

            foreach($q->result() as $r){
                $t .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }
        }else{
            $t .= '<option value="">No bank added</option>';
        }

        $t .= '</select>';

        return $t;

    } 
 


}