<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class rpt_pawning_status extends CI_Model
{

    private $sd;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['max_no'] = 1;
        return $a;
    }

    public function PDF_report($_POST_, $r_type = "pdf")
    {

        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '384M');

        if (isset($_POST['bc_n'])) {
            if ($_POST['bc_n'] == "") {
                $bc = "";
            }
        } else {

            if (!is_array($_POST['bc_arry'])) {

                $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);

                for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                    $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n] . ",";
                }

                $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
            }

            for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                $bc_ar[] = "'" . str_replace(",", "", $_POST['bc_arry'][$n]) . "'";
            }

            $bc = implode(',', $bc_ar);

            if ($bc == "") {
                $bc = "";
            } else {
                $bc = "   t.bc IN ($bc) AND ";
            }
        }

        // r_billtype filteration

        $fd = $_POST_['from_date'];
        $td = $_POST_['to_date'];

        $ftime = $_POST['ftime'];
        $ttime = $_POST['ttime'];

        $Qtime = "";

        // if ( $this->sd['isAdmin'] == 1 ){
        //     if (!$ftime == ""){
        //         $Qtime = " AND TIME(L.`action_date`) BETWEEN '$ftime' AND '$ttime'";
        //     }
        // }

        $qry = "SELECT  
        a.loanno,
        a.ddate ,
        a.bc,
        a.billtype,
        a.billno,
        a.amount,

        CASE
    WHEN  a.status = 'P' THEN 'Pawning' 
    WHEN  a.status = 'R'  THEN 'Redeem' 
    WHEN  a.status = 'F' THEN 'ForFeited' 
  
    ELSE '' 
  END AS status,
        CASE
    WHEN  a.description = 'PO' THEN 'Police Bill' 
    WHEN  a.description = 'L'  THEN 'Lost' 
    WHEN  a.description = 'PI' THEN 'Interest Paid' 
    WHEN  a.description ='PL' THEN 'Post Poned' 
    ELSE 'No Description' 
  END AS description,
        a.bc_name FROM ( SELECT
            t.`loanno`,
            t.bc,
            t.`billtype`,
            t.`billno`,
            t.`ddate`,
            t.`amount`,
            t.`transecode` AS STATUS ,
  m.`type` AS description,
  B.`name` AS `bc_name`
  FROM
    t_loantranse t
    LEFT JOIN t_loan_mark m ON m.`bc`=t.`bc` AND m.`loan_no`=t.`loanno`
    JOIN `m_branches` B ON t.`bc` = B.`bc`

  WHERE $bc t.`ddate` BETWEEN '$fd' AND '$td'
    AND t.transecode IN ('P', 'F', 'R')

    UNION ALL

    SELECT
    t.`loanno`,
    t.bc,
    t.`billtype`,
    t.`billno`,
    t.`ddate`,
    t.`amount`,
    t.`transecode` AS STATUS ,
  m.`type` AS description,
  B.`name` AS `bc_name`
  FROM
    t_loantranse_re_fo t
    LEFT JOIN t_loan_mark m ON m.`bc`=t.`bc` AND m.`loan_no`=t.`loanno`
    JOIN `m_branches` B ON t.`bc` = B.`bc`
  WHERE $bc  t.`ddate` BETWEEN '$fd' AND '$td'
    AND t.transecode IN ('P', 'F', 'R')  ) a

ORDER BY  a.ddate , a.loanno

  ";

        // $qry = "SELECT * FROM
        //         (SELECT LT.actual_balance, L.int_discount, L.bc, B.`name` AS `bc_name`,L.`loanno`,  L.`billtype`,L.`billno`,L.`bt_letter`,  L.`ddate`,C.`cusname`,  C.`address`,C.`nicno`,  C.`mobile`,LI.`totalweight`,  LI.items AS `items`,L.`requiredamount`, LI.`totalpweight`,L.`fmintrest`,'' AS `app_no`, L.`action_date`
        //         FROM `t_loan` L

        //         LEFT JOIN ( SELECT bc,billno,SUM(amount) AS actual_balance FROM t_loantranse WHERE transecode = 'P' GROUP BY billno) LT ON L.`bc` = LT.bc AND L.`billno` = LT.billno

        //         LEFT JOIN (  SELECT
        //             GROUP_CONCAT(CONCAT(i.`itemname`,
        //                 IF(ISNULL(li.pure_weight) OR li.pure_weight=0,'', CONCAT(' (',li.pure_weight,')')),
        //                 IF(ISNULL(li.elec_model) OR li.elec_model='','', CONCAT(' (',li.elec_model,')'))
        //             ))
        //             AS `items`,li.`bc`,li.`billtype`,li.`billno` , SUM( li.pure_weight ) as `totalpweight`, SUM( li.goldweight ) as `totalweight`
        //             FROM `t_loanitems` li
        //             JOIN `r_items` i ON li.`itemcode` = i.`itemcode`
        //             GROUP BY li.`bc`,li.`billtype`,li.`billno`

        //         ) LI ON L.`bc` = LI.`bc` AND LI.`billtype` = L.`billtype` AND LI.`billno` = L.`billno`

        //         JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
        //         JOIN `m_branches` B ON L.`bc` = B.`bc`

        //         WHERE  $bc L.ddate BETWEEN '$fd' AND '$td' AND L.`status` = 'P' $billtype $Qtime

        //         GROUP BY L.`bc`,L.`billtype`,L.`billno`

        //         UNION ALL

        //         SELECT LT.actual_balance, L.int_discount,L.bc,B.`name` AS `bc_name`,L.`loanno`,  L.`billtype`,L.`billno`,L.`bt_letter`,  L.`ddate`,C.`cusname`,  C.`address`,C.`nicno`,  C.`mobile`,L.`totalweight`,  LI.items AS `items`,L.`requiredamount`, LI.`totalpweight`,L.`fmintrest`,'' AS `app_no`, L.`action_date`
        //         FROM `t_loan_re_fo` L

        //         LEFT JOIN ( SELECT bc,billno,SUM(amount) AS actual_balance FROM t_loantranse WHERE transecode = 'P' GROUP BY billno) LT ON L.`bc` = LT.bc AND L.`billno` = LT.billno

        //         JOIN (  SELECT
        //             GROUP_CONCAT(CONCAT(i.`itemname`,
        //                 IF(ISNULL(li.pure_weight) OR li.pure_weight=0,'', CONCAT(' (',li.pure_weight,')')),
        //                 IF(ISNULL(li.elec_model) OR li.elec_model='','', CONCAT(' (',li.elec_model,')'))
        //             ))
        //             AS `items`,li.`bc`,li.`billtype`,li.`billno` , SUM( li.pure_weight ) as `totalpweight`, SUM( li.goldweight ) as `totalweight`
        //             FROM `t_loanitems_re_fo` li
        //             JOIN `r_items` i ON li.`itemcode` = i.`itemcode`
        //             GROUP BY li.`bc`,li.`billtype`,li.`billno`

        //         ) LI ON L.`bc` = LI.`bc` AND LI.`billtype` = L.`billtype` AND LI.`billno` = L.`billno`

        //         JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
        //         JOIN `m_branches` B ON L.`bc` = B.`bc`

        //         WHERE  $bc L.ddate BETWEEN '$fd' AND '$td' AND L.`status` IN ('P','AM','R','RN','L','PO') $billtype  $Qtime

        //         GROUP BY L.`bc`,L.`billtype`,L.`billno`

        //         ) aa

        //         ORDER BY aa.bc, aa.loanno, aa.ddate

        //         ";

        // var_dump($qry);exit();
        $Q = $this->db->query($qry);

        if ($Q->num_rows() > 0) {

            $r_data['list'] = $Q->result();
            $r_data['fd'] = $fd;
            $r_data['td'] = $td;
            $r_data['isAdmin'] = $this->sd['isAdmin'];

            if ($r_type == "pdf") {
                $this->load->view($_POST['by'] . '_' . 'pdf', $r_data);
            } else {
                $this->load->view($_POST['by'] . '_' . 'excel', $r_data);
            }

        } else {
            echo "<script>location='default_pdf_error'</script>";
        }

        ini_set('memory_limit', '128M');

    }

    public function Excel_report()
    {
        $this->PDF_report($_POST, "Excel");
    }

}
