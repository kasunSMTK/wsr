<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class t_dbl_entry_view extends CI_Model{
  
  private $sd;
  private $mtb;
  private $max_no;
  private $bc_glob;
   
  function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);    
    $this->load->model('user_permissions');
    
  }
  
  public function base_details(){
    $a['current_date']              = $this->sd['current_date'];
    $a['date_change_allow']         = $this->sd['date_chng_allow'];
    $a['backdate_upto']             = $this->sd['backdate_upto'];

    $this->load->model('M_branches');
    $a['bc_drop'] = $this->M_branches->select();
    $a['mod_drop'] = $this->get_MODs();

    return $a;
  }

  public function get_MODs(){

    $Q = $this->db->query("SELECT * FROM `t_trans_code` ORDER BY description");

    $t = '<select id="dmod">';

    if ($Q->num_rows() > 0){
        
        $t .= '<option value="">Select transaction</option>';
        foreach ($Q->result() as $R) {        
            $t .= '<option value="'.$R->code.'">'.$R->description.'</option>';
        }
    }else{
        $t .= '<option value="">No transactions</option>';
    }

    $t .= '</select>';

    return $t;

  }


    public function dta(){

        $bc  = $_POST['bc'];
        $mod = $_POST['transaction'];        
        $fd  = $_POST['fd'];
        $td  = $_POST['td'];
        $t   = '';


        $sql = " SELECT t_user_activity_log.*, t_trans_code.`description` AS `trans_det` , u_users.`display_name` AS `oc`  , m_branches.`name` AS `bc`
FROM `t_user_activity_log`  
LEFT JOIN t_trans_code ON t_user_activity_log.`trans_code` = t_trans_code.`code`
JOIN u_users ON t_user_activity_log.`oc` = u_users.`cCode`
JOIN m_branches ON t_user_activity_log.`bc` = m_branches.`bc`

WHERE `action` = 'cancel' AND LEFT(date_time,10) BETWEEN '$fd' AND '$td' ";

if ($bc != ''){
    $sql .= " AND t_user_activity_log.bc = '$bc'";
}

        

        $sql .= "  ORDER BY log_no DESC";


        $q = $this->db->query($sql);

        $t  = "<br><br><table border='0' class='tbl_user_activity' width='90%' align='center'>";

        $t .= "<tr>";        
        $t .= "<td>Date Time</td>";
        $t .= "<td>Module</td>";        
        $t .= "<td>Trans Code</td>";
        $t .= "<td>Trans No</td>";
        $t .= "<td>Branch</td>";
        $t .= "<td>Operator</td>";
        $t .= "<td>Action</td>";
        $t .= "</tr>";


        if ( $q->num_rows() > 0 ){

            foreach ($q->result() as $r) {
                
                $t .= "<tr>";                
                $t .= "<td>".$r->date_time."</td>";
                $t .= "<td>".$r->module."</td>";                
                $t .= "<td>".$r->trans_det."</td>";
                $t .= "<td>".$r->trans_no."</td>";
                $t .= "<td>".$r->bc."</td>";
                $t .= "<td>".$r->oc."</td>";
                

                if ($r->module == 'Pawning'){
                    $view = "<a target='self' href='?action=t_new_pawn&bn=".$r->trans_no."'>View</a>";
                }else{
                    $view = "";
                }



                $t .= "<td>".$view."</td>";


                $t .= "</tr>";

            }

        }


        $t .= "</table>";



        $a['d'] = $t;
        echo json_encode($a);



    }

    


    public function dbev(){

        $mod = $_POST['mod'];
        $no  = $_POST['no'];
        $bc  = $_POST['bc'];


        /*if ($mod == 1 || $mod == 2 ){
            $Qx = " AND ACC.billno = $no ";
        }else{
            $Qx = " AND ACC.trans_no = $no ";
        }*/

        $Qx = " AND ACC.trans_no = $no ";


        $Q = $this->db->query("SELECT ACC.`trans_no`,ACC.`trans_code`,TC.`description` AS `tc_desc`,ACC.`ddate`,ACC.`dr_amount`,ACC.`cr_amount`,ACC.`acc_code`,MA.`description` AS `acc_desc`,ACC.`description`,ACC.`bc`,B.`name` AS `bc_name`,ACC.`v_bc`,B2.`name` AS `v_bc_name`,ACC.`v_class`,VC.`description` AS `v_class_desc`

FROM `t_account_trans` ACC

JOIN `t_trans_code` TC ON ACC.`trans_code` = TC.`code`
JOIN m_account MA ON ACC.`acc_code` = MA.`code`
JOIN m_branches B ON ACC.`bc` = B.`bc`
LEFT JOIN m_branches B2 ON ACC.`v_bc` = B2.`bc`
LEFT JOIN m_voucher_class VC ON ACC.`v_class` = VC.`code`

WHERE ACC.trans_code = $mod $Qx AND ACC.bc ='$bc' ");


        $t = '<table border="1" width="100%" class="tblenrv" borderColor="#cccccc">';

        $t .= '<td>Date</td>';
        $t .= '<td style="text-align:right">Dr Amount</td>';
        $t .= '<td style="text-align:right">Cr Amount</td>';        
        $t .= '<td>Acc Desc</td>';
        $t .= '<td>Entry Description</td>';
        $t .= '<td>BC Code</td>';
        $t .= '<td>BC Name</td>';
        $t .= '<td>Ref BC</td>';
        $t .= '<td>Ref BC Name</td>';
        
        $t .= '<td>Ref Class Desc</td>';


        if ($Q->num_rows() > 0){

            $totdr_amount = $totcr_amount = 0;
            
            foreach ($Q->result() as $R) {
                
                $t .= '<tr>';                
                $t .= '<td>'.$R->ddate.'</td>';
                $t .= '<td style="text-align:right">'.number_format($R->dr_amount,2).'</td>';
                $t .= '<td style="text-align:right">'.number_format($R->cr_amount,2).'</td>';                
                $t .= '<td>'.$R->acc_code. ' - ' .$R->acc_desc.'</td>';
                $t .= '<td>'.$R->description.'</td>';
                $t .= '<td>'.$R->bc.'</td>';
                $t .= '<td>'.$R->bc_name.'</td>';
                $t .= '<td>'.$R->v_bc.'</td>';
                $t .= '<td>'.$R->v_bc_name.'</td>';
                
                $t .= '<td>'.$R->v_class_desc.'</td>';
                $t .= '</tr>';


                $totdr_amount += $R->dr_amount;
                $totcr_amount += $R->cr_amount;

            }

                $t .= '<tr bgColor="#eaeaea">';                
                $t .= '<td>Total</td>';
                $t .= '<td style="text-align:right">'.number_format($totdr_amount,2).'</td>';
                $t .= '<td style="text-align:right">'.number_format($totcr_amount,2).'</td>';                
                $t .= '<td></td>';
                $t .= '<td></td>';
                $t .= '<td></td>';
                $t .= '<td></td>';
                $t .= '<td></td>';
                $t .= '<td></td>';
                $t .= '<td></td>';
                
                $t .= '</tr>';



        }else{
                $t .= '<tr>';
                $t .= '<td colspan="15"><span style="color:red">No data found</span></td>';
                $t .= '</tr>';
        }

        $a['d'] = $t;

        echo json_encode($a);

    }









    

}