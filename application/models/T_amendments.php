<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class T_amendments extends CI_Model {
    
    private $sd;    
    private $max_no;
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();   
    }
    
    public function base_details(){     
        $a['current_date'] = $this->sd['current_date'];
        $a['max_no'] = $this->getPartPayTransNo();  
        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        $a['bc_no'] = $this->sd['bc_no'];
        $a['backdate_upto'] = $this->sd['backdate_upto'];
        return $a;
    }
    
    public function save(){

        $a['s'] = 33;
        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this TFR bill and you might required re-login to continue";

        if (isset($this->sd['current_date'])){
            if (!$this->utility->is_valid_date($this->sd['current_date'])){
                echo json_encode($a);
                exit;
            }
        }else{            
            echo json_encode($a);
            exit;
        }   $a['s'] = ''; $a['msg_33'] = '';
        
        $bc         = $this->sd['bc']; 
        $loanno     = $_POST['loanno'];
        $INT_RATES  = $this->db->query("SELECT L.`fmintrate`,L.`fmintrate2` FROM `t_loan` L WHERE bc = '$bc' AND loanno = $loanno LIMIT 1")->row();


        $_POST['billno']        = $_POST['bc_no'].$_POST['billno'];
        $five_days_int_cal      = $_POST['five_days_int_cal'];
        $this->load->model("calculate");
        $int_balance            = floatval($this->calculate->interest_balance($loanno,$five_days_int_cal,1));
        
        $r_amount               = $_POST['amount'];        
        $old_advance_amount     = $_POST['loan_amount'];
        
        $pawn_date              = $_POST['pDate'];
        $current_date           = $_POST['ddate'];
        $NODs                   = $this->date_df($pawn_date,$current_date)+1;        
        $first_week_int_charge  = true;
        $is_weelky_int_cal      = $_POST['is_weelky_int_cal'];
        $balance_amount         = 0;        
        $_POST['data']          = $_POST;        
        $paying_amount          = floatval($_POST['amount']);
        $action_date            = date('Y-m-d h:i:s');
        $a['bill_status']       = "";

        $int_bal                = $_POST['int_bal'];
        $_POST['extra_required_amount'] = 0;
        
        $_POST['first_week_int_charge'] = $first_week_int_charge;

        






        // WEEKLY INTEREST CHARGED BILL        

        if ($is_weelky_int_cal == 1){

            // if need an aditional amount redeem bill and re-pawn with old pawn amount + newly required amount
            
            // New Capital
            $t_new_capital = $new_capital = ((($_POST['loan_amount']+$int_balance) - $_POST['customer_advance']) + $_POST['amount']) 
                                                            / 
                                          (1- ($INT_RATES->fmintrate / 100));

            $new_capital = $new_capital - $_POST['customer_advance'];

            // New Interest
            $new_int     = ($t_new_capital * $INT_RATES->fmintrate) / 100;

            $new_capital =  ((($_POST['loan_amount'] + $_POST['amount']) - $_POST['customer_advance']) + $int_balance) + $new_int;

            

            if ($r_amount > 0){

                $_POST['extra_required_amount'] = $r_amount;
                
                $a['redeem_int_bal'] = floatval($_POST['int_bal']);
                $a['loan_stat']      = "To be renew with adding extra required amount";                
                
                $extra_amount_int    = round($new_int);
                $new_advance_amount  = $new_capital;
                
                $_POST['fmintrest']     = $new_int;
                $a['int_for_new_loan']  = floatval($extra_amount_int);
                $a['bill_status']       = "REDEEM_AND_ISSUE_NEW";
                $a['a']                 = 1;

            }            

        }





        // MONTHLY INTEREST CHARGED BILL        

        if ($is_weelky_int_cal == 0){

            $a['bill_status']       = "REDEEM_AND_ISSUE_NEW";

            if ($NODs <= 15){ // lessthan 15 days, charge interest for only adtional amount
                
                $days  = $this->db->query("SELECT (30- (DATEDIFF('$current_date','$pawn_date'))) / 30 AS `days`")->row()->days;

                // create interest amount
                $new_int =  ((($_POST['amount'] - ($_POST['customer_advance'])) / (1-(($INT_RATES->fmintrate+$INT_RATES->fmintrate2)/100))) - ($_POST['amount'] - ($_POST['customer_advance'])) ) * $days ;
                
                // create new pawning amount
                
                $new_capital = $new_advance_amount = ($_POST['loan_amount'] + ($_POST['amount'] - $_POST['customer_advance'])) + $new_int;


                $a['a'] = 1;

                //echo $new_capital;
                //echo $new_int;
                         
                $a['amt'] = $_POST['fmintrest'] = $new_int;
                $a['new_capital_plus_int']  = $new_capital + $new_int;
                $a['new_capital']           = $new_capital;
                $a['extra_required_amount'] = $_POST['amount'];
                $a['int_for_new_loan']  = floatval(round($new_int));

            }elseif($NODs > 15 && $NODs < 31 ){

                $new_capital = (($_POST['loan_amount'] + $_POST['amount']) - ($_POST['customer_advance'])) / (1-(($INT_RATES->fmintrate+$INT_RATES->fmintrate2)/100));
                $new_int     = ($new_capital - (($_POST['loan_amount'] + $_POST['amount']) - ($_POST['customer_advance'])));

                
                $a['a'] = 1;

                $a['amt'] = $_POST['fmintrest'] = $new_int;
                $a['new_capital_plus_int']  = $new_capital + $new_int;
                $a['new_capital']           = $new_advance_amount = $new_capital;
                $a['extra_required_amount'] = $_POST['amount'];
                $a['int_for_new_loan']  = floatval(round($new_int));

            }else{
                
                // create new pawning amount
                $new_capital = ((($_POST['loan_amount'] + $int_balance) - ($_POST['customer_advance'])) + $_POST['amount']) / (1-(($INT_RATES->fmintrate+$INT_RATES->fmintrate2)/100));

                // create interest amount                
                $new_int = ($new_capital * ($INT_RATES->fmintrate+$INT_RATES->fmintrate2)) / 100;
                
                $a['a']                     = 1;
                $a['amt'] = $_POST['fmintrest'] = $new_int;
                $a['new_capital_plus_int']  = $new_capital + $new_int;
                $a['new_capital']           = $new_advance_amount = $new_capital;
                $a['extra_required_amount'] = $_POST['amount'];
                $a['int_for_new_loan']  = floatval(round($new_int));

            }

        }
        










        $a['nods'] = $NODs;

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{            

            if ($a['a'] == 0){
                $this->db->trans_commit();
                $a['s']         = 1;
                $a['max_no']    = $this->getPartPayTransNo();
                $a['no']        = $tr_no;
                $a['loanno']    = $loanno;
            }else{
                $a['a']         = 1;
                $a['lgv']       = number_format($this->calculate->PREVIOUS_GOLD_OF_A_LOAN($loanno),2);
                $a['cgv']       = number_format($this->calculate->CURRENT_GOLD_OF_A_LOAN($loanno),2);
                $a['articles']  = $this->calculate->get_pawn_articles($loanno);                
                $a['new_loan_amount'] = number_format(round($new_advance_amount), 2, '.', ',');                

                $a['msg_html']      = $this->msg_html();
                $a['paying_amount'] = $paying_amount;
                $a['balance_amount']= $balance_amount;
                $a['data']          = $_POST;
            }

        }        

        echo json_encode($a);
    

    }































    public function proceed_to_renew_loan(){

        $a['s'] = 33;
        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this advance payment bill and you might required re-login to continue";

        if (isset($this->sd['current_date'])){
            if (!$this->utility->is_valid_date($this->sd['current_date'])){
                echo json_encode($a);
                exit;
            }
        }else{            
            echo json_encode($a);
            exit;
        }   $a['s'] = ''; $a['msg_33'] = '';

        $loanno         = $_POST['data']['loanno'];        
        $loan_amount    = $_POST['data']['loan_amount'];
        $nla            = floatval(str_replace(",", "", $_POST['nla']));
        $balance_amount = $_POST['balance_amount'];
        $billtype       = $_POST['data']['billtype'];
        $current_gold_value = $_POST['current_gold_value'];
        $tr_no          = $this->getPartPayTransNo();
        $action_date    = date('Y-m-d h:i:s');
        $bc             = $this->sd['bc']; 

        $LOAN_DATA = $this->db->query("SELECT L.is_weelky_int_cal, L.old_bill_age, L.is_renew, L.fmintrest,C.customer_id,L.billno,L.bc,nicno,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.int_cal_changed,L.am_allow_frst_int,L.`int_paid_untill` FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype` WHERE L.loanno = '$loanno' AND L.bc = '$bc' LIMIT 1 ")->row();
        
        $this->load->model("calculate");
        $a['int'] = $this->calculate->interest($loanno,"json",$this->sd['current_date'], $LOAN_DATA,0);

        $no_of_days = $a['int']['no_of_days'];
        $paid_int   = $a['int']['paid_int'];
        $int_bal = $a['int']['int_balance']; // optional for now, use if old loan int is chargeable
       
        $_POST['data']['no_of_days'] = $no_of_days;
        $_POST['data']['nla'] = $nla;
        $_POST['data']['extra_required_amount'] = $_POST['data']['amount'];
        $_POST['data']['fmintrate'] = $LOAN_DATA->fmintrate;
        $_POST['data']['int_tr_no'] = "";
        $_POST['data']['obn'] = $LOAN_DATA->billno;

        $am_allow_frst_int = 1;

        // below this section commented on 2017-03-15, this amount added as 'interest received' in account update process
        
        /*if (floatval($_POST['data']['int_bal']) > 0){            
            $this->add_data_to_trans_tbls("return",$_POST['data'],floatval($_POST['data']['int_bal']),0,$tr_no,$action_date,0);
                $_POST['data']['int_tr_no'] = $tr_no;
        }*/
        
        $this->calculate->OLD_LOAN_TO_NEW_LOAN_AM($loanno,$balance_amount,$current_gold_value,$tr_no,$billtype,$nla,$_POST['data'],"AM",$am_allow_frst_int);        
        
    }


    public function pay_only_interest(){
        $paying_amount = $_POST['paying_amount'];
        $balance_amount = $_POST['balance_amount'];
        $tr_no = $this->getPartPayTransNo();
        $action_date = date('Y-m-d h:i:s');
        $this->add_data_to_trans_tbls("json",$_POST['data'],$paying_amount,$balance_amount,$tr_no,$action_date,0); // This part should trigger on "Pay Interest Only" button press
    }

    public function add_data_to_trans_tbls($return_type,$_POST_,$paying_amount,$balance_amount,$tr_no,$action_date,$is_pawn_int){
        
        $this->db->trans_begin();

        $_PST['loanno']    = $_POST_['loanno'];$_PST['bc'] = $this->sd['bc']; $_PST['billtype']  = $_POST_['billtype']; $_PST['billno']    = $_POST_['billno']; $_PST['ddate']     = $_POST_['ddate']; $_PST['amount']    = $paying_amount; $_PST['transecode']= "A"; $_PST['transeno']  = $tr_no; $_PST['discount']  = 0;  $_PST['is_pawn_int'] = $is_pawn_int;

        $cus_serno      = $_POST['data']['hid_customer_serno'];
        unset($_POST['data']['hid_customer_serno']);
        
        if ( floatval($_POST['data']['customer_advance_paying']) > 0 ){
            $_PST['advance_amount']    =    $_POST['data']['customer_advance_paying'];
            $this->calculate->cr_advance_bal($cus_serno,$_PST['advance_amount'],$tr_no,"ADV_R");
        }        

        if ( floatval($_POST['data']['card_amount']) > 0 ){
            $_PST['card_amount']    = $_POST['data']['card_amount'];
            $this->calculate->add_credit_card_entry($cus_serno,$_PST['card_amount'],$tr_no);
        }        

        if ( floatval($_POST['data']['cash_amount']) > 0 ){
            $_PST['cash_amount']    = $_POST['data']['cash_amount']; 
        }

        if ($paying_amount > 0){
            $Q1 = $this->db->insert("t_loantranse",$_PST); // LOANTRANSE TABLE
        }

        unset($_PST);        
        
        $this->max_no = $tr_no;        
        $_POST['paying_amount'] = $paying_amount;
        $_POST['hid'] = 0;

        $_POST['ln'] = $_POST_['loanno'];
        $_POST['bt'] = $_POST_['billtype'];
        $_POST['bn'] = $_POST_['billno'];
        
        if (isset($_POST['ddate'])){
            $_POST['date'] = $_POST['ddate'];
        }else{
            $_POST['date'] = $_POST['data']['ddate'];
        }


        if (floatval($_POST['paying_amount']) > 0){
            $account_update =   $this->account_update(0);                                
            
            if($account_update!=1){
                $a['s'] = 0; $a['m'] = "Invalid account entries"; $this->db->trans_rollback(); $a['s'] = "error";
                echo json_encode($a);
                exit;
            }else{
                $account_update =   $this->account_update(1);
            }
        }        

        if ($balance_amount > 0){
            $_PST['client_id'] = $_POST_['customer_id']; $_PST['trans_type'] = "A";$_PST['trans_no'] = $tr_no;$_PST['loan_no'] = $_POST_['loanno'];$_PST['date'] = $this->sd['current_date'];$_PST['dr_amount'] = 0;$_PST['cr_amount'] = $balance_amount ;$_PST['note'] = "";$_PST['is_post'] = 0;$_PST['is_delete'] = 0;$_PST['bc'] =  $this->sd['bc'];$_PST['oc'] = $this->sd['oc'];$_PST['action_date'] = $action_date;
            $Q2 = $this->db->insert("t_loan_advance_trans",$_PST); // ACCOUNT TRANS TABLE                
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{            
            $this->db->trans_commit();
            $a['s']         = 1;
            $a['max_no']    = $this->getPartPayTransNo();
            $a['no']        = $tr_no;
            $a['loanno']    = $_POST_['loanno'];
        }

        
        if ($return_type == "json"){            
            echo json_encode($a);
        }else{
            return $a;
        }

    }

    private function msg_html(){

        return '<div class="title_div_pp">Amendments</div>

            <p>Message here </p> <br><br>
            
            <div style="height:100px;border:0px solid red;">
                
                <div style="width:232px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn">
                    <span class="text_box_title_holder_new_pawn" style="width:150px">New Loan Amount</span>
                    <input type="text" id="nla" class="input_text_large_s amount" style="width:200px">                               
                </div>

                <div style="width:232px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn">
                    <span class="text_box_title_holder_new_pawn" style="width:150px">Previous Gold Value</span>
                    <input type="text" readonly="readonly" id="lgv" class="input_text_large_s" style="width:200px">                               
                </div>

                <div style="width:232px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn">
                    <span class="text_box_title_holder_new_pawn" style="width:150px">Current Gold Value</span>
                    <input type="text" readonly="readonly" id="cgv" class="input_text_large_s" style="width:200px">                               
                </div>

            </div>

            <div class="optional_content_show"></div>

            <div id="div_itm_contain" style="border:0px solid Green;margin-bottom:20px;width:672px;height:44px; overflow:hidden">
                <div class="show_pawn_articles_div">Show Pawn Articles</div>
                <div id="div_show_items"></div>
            </div>  


            <br><br>
            <div>            
                <div style="float:left">
                    <input type="button" id="btnProceedRenew" class="btn_regular" value="Proceed to Renew" style="width:150px">                
                </div>
                
                <div style="float:right">
                <input type="button" id="btnPPcancel" class="btn_regular" value="Cance">
                </div>
            </div>';

    }

    public function LOAD_LOAN(){
        
        $billtype           = $_POST['billtype'];
        $billno             = $_POST['bc_no'].$_POST['billno'];
        $int_cal_to_DATE    = $_POST['dDate'];
        $five_days_int_cal  = $_POST['five_days_int_cal'];
        $bc                 = $this->sd['bc'];

        $Q1 = $this->db->query("SELECT L.goldvalue, L.old_bill_age, L.is_renew, L.billno, L.bc,C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,C.`customer_id`,L.int_cal_changed,L.cus_serno,L.am_allow_frst_int,L.`is_weelky_int_cal`,L.`int_paid_untill`,
            L.`fmintrest`, L.time , L.tfr_res FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '$billno' AND L.bc = '$bc' LIMIT 1 ");

        if ($Q1->num_rows() > 0){
            $a['loan_sum'] = $Q1->row();
            $loan_no = $Q1->row()->loanno;            
            
            $this->load->model("calculate");
            $this->load->model("t_new_pawn");
            $this->load->model("m_billtype_det");
            
            $a['int'] = $this->calculate->interest($loan_no,"json",$int_cal_to_DATE, $a['loan_sum'],$five_days_int_cal);
            $a['customer_advance'] = $this->calculate->customer_advance($Q1->row()->customer_id,$billno);
            $a['partpay_histomer'] = $this->db->select( array("amount","discount","action_date","bc") )->where(array("bc"=>$bc,"loanno"=>$loan_no,"transecode"=>"A"))->where("is_pawn_int !=",1)->order_by("action_date","desc")->get("t_loantranse")->result();
            $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;
            $a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);
            $a['s'] = 1;
        }else{

            $Q2 = $this->db->query("SELECT L.is_weelky_int_cal,L.goldvalue, L.old_bill_age,L.is_renew, L.fmintrest,L.billno,L.int_paid_untill, L.bc,C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,L.int_cal_changed ,  

                L.old_o_new_billno AS `new_bill_no`

                ,L.am_allow_frst_int, L.time , L.tfr_res FROM `t_loan_re_fo` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '$billno' AND L.bc = '$bc' LIMIT 1 ");

            if ($Q2->num_rows()>0){
                $a['loan_sum'] = $Q2->row();
                $loan_no = $Q2->row()->loanno;            
            
                $this->load->model("calculate");
                $this->load->model("t_new_pawn");
                $this->load->model("m_billtype_det");
                
                $a['int'] = $this->calculate->interest($loan_no,"json",$int_cal_to_DATE, $a['loan_sum'],$five_days_int_cal);

                $a['customer_advance'] = $this->calculate->customer_advance($Q2->row()->customer_id,$billno);
                $a['partpay_histomer'] = $this->db->select( array("amount","discount","action_date","bc") )->where(array("bc"=>$bc,"loanno"=>$loan_no,"transecode"=>"A"))->where("is_pawn_int !=",1)->order_by("action_date","desc")->get("t_loantranse")->result();
                $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;
                $a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);
                $a['s'] = 1;
            }else{
                $a['s'] = 0;
            }

        }

        echo json_encode($a);
    }    
    
    public function getSavedPartPay(){
        $bc      = $this->sd['bc'];
        $tr_no   = $_POST['tr_no'];

        $Q1 = $this->db->query("SELECT L.bc,
        LT.`ddate` AS `pp_paid_date`,   
        LT.`amount`,
        LT.`discount`,  
        LT.`bc` AS `pp_added_bc`,        
        L.`ddate` AS `pawn_date`,
        L.`ddate` AS `ddate`,
        L.`billtype`,
        L.`billno`,
        L.`loanno`,
        L.`requiredamount`,
        L.`fmintrate`,
        L.`fmintrate2`,
        L.`finaldate`,
        L.`period` ,
        L.`int_cal_changed`,
        LT.`card_amount`,
        LT.`advance_amount`,
        LT.`cash_amount`,
        L.cus_serno

      
        FROM `t_loantranse` LT 

        JOIN `t_loan` L ON LT.`bc` = L.`bc` AND LT.`billtype` = L.`billtype` AND LT.`billno` = L.`billno`        

        WHERE LT.bc = '$bc' AND LT.`transeno` = '$tr_no' AND LT.`transecode` = 'A' and LT.`is_pawn_int` != 1 ");

        if ($Q1->num_rows() > 0){            
            $a['sum'] = $Q1->row();
            $this->load->model("calculate");
            $a['int'] = $this->calculate->interest($Q1->row()->loanno,"json",$Q1->row()->pp_paid_date,$a['sum']);
            
            $a['pay_option'] = $this->calculate->get_pay_option(

                $bc, 
                $tr_no, 
                $a['sum']->cus_serno, 
                $a['sum']->card_amount, 
                $a['sum']->advance_amount, 
                $a['sum']->cash_amount,
                "A" 

            );
            
            if ($a['sum']->cash_amount > 0){ 
                $a['pay_option']['cash_amount'] = $a['sum']->cash_amount; 
            }

            $a['s'] = 1;            
        }else{
            $a['s'] = 0;
        }

       echo json_encode($a);

    }

    public function getPartPayTransNo(){
        $bc = $this->sd['bc'];
        return $this->db->query("SELECT IFNULL(MAX(`transeno`)+1,1) AS `max_no` FROM `t_loantranse` WHERE bc = '$bc' AND `transecode` = 'A'")->row()->max_no;
    }

    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 4);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',4);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "trans_code" => 4,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);
        
        $cash_book          = $this->utility->get_acc('cash_acc',$this->sd['bc']);
        $ADVANCE_RECEIVED   = $this->utility->get_default_acc('ADVANCE_RECEIVED');

        $this->account->set_value2("Advance Payments", $_POST['paying_amount'] , "dr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"A");                
        $this->account->set_value2("Advance Payments", $_POST['paying_amount'], "cr", $ADVANCE_RECEIVED,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"A");
                
        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='4'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 4);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }

    private function date_df($pawn_date,$current_date){
        $date1=date_create($pawn_date);
        $date2=date_create($current_date);
        $diff=date_diff($date1,$date2);
        return intval($diff->format("%a "));
    }


    public function discount_approval(){

        $dis_amount = $_POST['dis_amount'];

        $app_no = $this->max_dis_app_no();

        $sum_arry = array(

            'nno' => $app_no,
            'record_status' => 'W',
            'bc' => $this->sd['bc'],
            'discount_amount' => $dis_amount,
            'requested_by' => $this->sd['oc'],
            'requested_datetime' => date('Y-m-d H:i:s'),
            'request_type' => 'TFR',
            'approved_amount' => 0,
            'approved_by' => '',
            'approved_datetime' => '',
            'action_date' => date('Y-m-d H:i:s')

        );


        $Q = $this->db->insert('t_discount_approval',$sum_arry);

        if ($Q){
            $a['s'] = 1;
            $a['app_no'] = $app_no;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }

    public function max_dis_app_no(){

        return $this->db->query("SELECT IFNULL(MAX(nno)+1,1) AS `max_dis_app_no` FROM `t_discount_approval`")->row()->max_dis_app_no;

    }

    public function call_dis_app_response_monitor(){

        $app_no = $_POST['app_no'];
        $bc     = $this->sd['bc'];

        $Q = $this->db->query("SELECT * FROM `t_discount_approval` DA WHERE DA.`nno` = '$app_no' AND DA.`bc` = '$bc' LIMIT 1 ");

        if ( $Q->num_rows() > 0 ){
            $a['r'] = $Q->row();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }


}