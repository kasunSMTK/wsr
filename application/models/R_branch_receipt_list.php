<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_branch_receipt_list extends CI_Model {

    private $sd;
    private $mtb;    
    
    function __construct(){
        parent::__construct();	
        $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
        $a['report'] = '';	
        return $a;
    }
    
    function PDF_report(){

        $bc = $this->sd['bc'];
        $fd = $_POST['fd'];
        $td = $_POST['td'];
        $vs = $_POST['vou_stat'];

        if ($vs == "All"){
            $QC = "";
        }else{
            $QC = " AND VS.`status` = '$vs' ";
        }



        $no = $_POST['l_no'];        
        $bc = $this->sd['bc'];
        
        $q = $this->db->query("SELECT 
            sm.`nno`,
            sm.`date`,
            sm.`ref_no`,
            sm.`type`,
            sm.`customer`,
            sm.`paid_to_acc`,
            sm.`cash_amount`,
            dt.`acc_code`,
            dt.`amount`,
            ma.`description` AS paid_acc,
            maa.`description` AS det_acc,
            sm.action_date
            FROM `t_br_receipt_sum` sm
            JOIN `t_br_receipt_det` dt ON dt.`nno`=sm.`nno` AND dt.`bc`=sm.`bc`
            JOIN m_account ma ON ma.`code`=sm.`paid_to_acc` 
            JOIN m_account maa ON maa.`code`=dt.`acc_code`
            WHERE sm.`date` BETWEEN '$fd' AND '$td'
            AND sm.`bc` = '$bc'
            ORDER BY sm.`nno` ");      

        if($q->num_rows()>0){
            $r_detail['det'] = $q->result();
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}