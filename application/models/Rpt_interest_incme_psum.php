<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_interest_incme_psum extends CI_Model {

    private $sd;    
    
    function __construct(){
        parent::__construct();		
        $this->sd = $this->session->all_userdata();		
    }

    public function base_details(){		
        $a['max_no'] = 1;        
        return $a;
    }   

    public function PDF_report($_POST_,$type='pdf'){


        ini_set('max_execution_time', 600 );


        // var_dump($_POST_['bc_arry']);

        $st  = '';

        if($_POST['bc_arry'] === null){
            $BC = " ";            
        }else{
            
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            
            $BC = " bc IN ($bc) AND  ";
        }

        

        $xbc = array();
        $nn = 0;
        foreach ( $_POST['bc_arry'] as $b ){
            $xbc[$nn] = $b;
            $nn++;
        }


      
        $fd = $_POST['from_date'];
        $td = $_POST['to_date'];        
        $data_range = $_POST['h_range'];  

            
        
        if ($data_range == "months") {
            $Q =$this->db->query("SELECT bc , DATE_FORMAT(ddate,'%Y-%m') AS `m` ,  SUM(cr_amount - dr_amount) AS `pawn_int` FROM t_account_trans WHERE $BC acc_code = '10101' AND ddate BETWEEN '$fd' AND '$td'GROUP BY bc,DATE_FORMAT(ddate,'%Y-%m') ");
        }

        if ($data_range == "years") {
            $Q =$this->db->query("SELECT bc , DATE_FORMAT(ddate,'%Y') AS `m` ,  SUM(cr_amount - dr_amount) AS `pawn_int` FROM t_account_trans WHERE $BC acc_code = '10101' AND ddate BETWEEN '$fd' AND '$td'GROUP BY bc,DATE_FORMAT(ddate,'%Y') ");   
        }

        if($data_range == "quarter"){
            $Q =$this->db->query("SELECT bc,YEAR(ddate) AS `y`, QUARTER(ddate) AS `q`, SUM(cr_amount) - SUM(dr_amount) AS `pawn_int` FROM t_account_trans WHERE $BC acc_code = '10101' AND ddate BETWEEN '$fd' AND '$td'GROUP BY bc,YEAR(ddate), QUARTER(ddate) ");        
        }


        if($data_range == "half_year"){
            $Q =$this->db->query("SELECT bc, YEAR(ddate) AS y,FLOOR( ( MONTH(ddate) - 1 ) / 6 ) AS q, SUM(cr_amount) - SUM(dr_amount) AS `pawn_int` FROM t_account_trans WHERE $BC acc_code = '10101' AND ddate BETWEEN '$fd' AND '$td'GROUP BY bc,YEAR(ddate), FLOOR( ( MONTH(ddate) - 1 ) / 6 ) ORDER BY y, q ");
        }

            

        if ($Q->num_rows() > 0){

            $r_data['list']     = $Q->result();
            $r_data['fd']       = $fd;
            $r_data['td']       = $td;
            $r_data['d_range']  = $data_range;
            $r_data['bcc']  = $this->db->select(array('bc','name'))->get('m_branches')->result();
            $r_data['selct_bc'] = $xbc;

            
            if ($type == 'Excel'){
                $this->load->view($_POST['by'].'_'.'excel',$r_data);       
            }else{
                echo 'PDF version not available for this report';
            }


        }else{
            echo "<script>alert('No data');</script>";
        }

      

    }


    public function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,"Excel");
    }

}