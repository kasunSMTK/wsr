<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class t_damage extends CI_Model
{

    private $sd;
    private $max_no;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['current_date'] = $this->sd['current_date'];

        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        $a['bc_no'] = $this->sd['bc_no'];
        $a['backdate_upto'] = $this->sd['backdate_upto'];
        $a['max_no'] = $this->get_max_no();
        $a['current_date'] = $this->sd['current_date'];
        $this->load->model('m_bank');

        return $a;
    }

    public function get_max_no()
    {
        return $this->db->query("SELECT IFNULL(MAX(nno)+1,1) AS `max_no` FROM `t_sales_sum` where bc = '" . $this->sd['bc'] . "'")->first_row()->max_no;

    }
    public function getItemList()
    {
        $q = $_GET['term'];
        $query = $this->db->query("SELECT i.`cat_code`,i.`loanno`,i.`nno`,f.`itemcode`,f.`itemname`,i.`tag_no`,i.`ser_no` FROM `t_tag` i
        INNER JOIN `r_items` f ON f.`itemcode` = i.`item`
        WHERE i.`is_damage`='0' AND i.`tag_no` LIKE '%$q%' OR i.`ser_no` LIKE '%$q%' OR f.`itemname` LIKE '%$q%' ORDER BY i.`tag_no` LIMIT 100");
        $ary = array();

        foreach ($query->result() as $R) {
            $ary[] = $R->tag_no . " - " . $R->ser_no . " - " . $R->itemname;
        }

        echo json_encode($ary);

    }

    public function save()
    {

        try {
            $this->db->trans_begin();
            $data = array('is_damage' => true, 'damage_narration' => $_POST['damage_narration']);
            $this->db->where('tag_no', $_POST['item_tag']);
            $this->db->update('t_tag', $data);
            $this->db->trans_commit();
            echo 1;
        } catch (Exception $e) {
            $this->db->trans_rollBack();
            echo 0;
        }

    }

}
