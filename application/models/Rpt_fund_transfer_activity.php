<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_fund_transfer_activity extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    $status = $_POST['fund_transfer_status'];

    $Q1 = "";

    if ($status != ""){
      $Q1 = " AND S.`status` = $status ";
    }

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = " AND S.`requ_initi_bc` IN($bc) ";
        }
    }

    $sql = "SELECT 

S.`no`,
S.`ref_no`,
S.`date_time`,
B1.`name` AS `from_bc`,
B2.`name` AS `to_bc`,
S.`transfer_type`,
S.`amount`,
S.`comment`,
B3.`name` AS `requ_initi_bc`,
U.`discription` AS `requ_initi_by`

FROM `t_fund_transfer_sum` S

JOIN m_branches B1 ON S.`from_bc` = B1.`bc`
JOIN m_branches B2 ON S.`to_bc` = B2.`bc`
JOIN m_branches B3 ON S.`requ_initi_bc` = B3.`bc`

LEFT JOIN `u_users` U ON S.`requ_initi_by` = U.`cCode`

WHERE S.`added_date` BETWEEN '$fd' AND '$td' $Q1 $bc

ORDER BY S.no DESC, S.`requ_initi_bc` ";

    $query = $this->db->query($sql);
    
    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}