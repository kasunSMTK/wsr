<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class s_role extends CI_Model {
    private $sd;
    private $mtb;
    private $tb_sum;
    private $tb_det;
    private $mod = '018';

    function __construct(){
        parent::__construct();
        
        $this->sd = $this->session->all_userdata();
        $this->mtb = $this->tables->tb['u_modules'];
        $this->tb_det = $this->tables->tb['u_user_role_detail'];
        $this->tb_sum = $this->tables->tb['u_user_role'];
        $this->load->model('user_permissions');
    }

    public function base_details(){       

        $this->load->model('m_branches');
        $a['table_data'] = $this->data_table();
        $a['branch'] = $this->m_branches->select();
        $a['added_user_role'] = $this->added_user_role();
    
        return $a;
    }

    public function added_user_role(){

        $Q = $this->db->query("SELECT * FROM u_user_role");


        if ($Q->num_rows() > 0){
            
            $T = '<select name="code" type="text"  id="code" title="code" class="input_ui_dropdown">';
            $T .= "<option value=''>Select a user role</option>";
            
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->role_id."' id_det = '".$R->id."' >".$R->description."</option>";
            }
            
            $T .= "</select>";
        
        }else{

            $T = '<input name="code" type="text" class="input_txt" id="code" style="width: 150px;" title="code" />';

        }

        return $T;

    }

    public function check_role(){
        $q = $this->db->where('role_id', $this->input->post('code'))->get($this->tb_sum);
        echo $q->num_rows;
    }

    public function data_table(){
        return $this->db->order_by("m_code desc")->get($this->mtb)->result();
    }


    public function save(){

        //$this->user_permissions->verify_permission("save",$this->mod);

        $a_sum = array(
            "role_id"=>$this->input->post('code'),
            "description"=>$this->input->post('des'),
            "bc"=>$this->input->post('bc'),
            "oc"=>$this->sd['oc']
        );

        $this->set_delete();
        $this->db->insert($this->tb_sum, $a_sum);
        $lid = $this->db->insert_id();
        $a_det = array();

        foreach($this->data_table() as $r){

            if ($this->input->post('is_view'.$r->m_code) == null){ $is_view = 0; }else{ $is_view = $this->input->post('is_view'.$r->m_code);}
            if ($this->input->post('is_add'.$r->m_code) == null){ $is_add = 0; }else{ $is_add = $this->input->post('is_add'.$r->m_code);}
            if ($this->input->post('is_edit'.$r->m_code) == null){ $is_edit = 0; }else{ $is_edit = $this->input->post('is_edit'.$r->m_code);}
            if ($this->input->post('is_delete'.$r->m_code) == null){ $is_delete = 0; }else{ $is_delete = $this->input->post('is_delete'.$r->m_code);}
            if ($this->input->post('is_print'.$r->m_code) == null){ $is_print = 0; }else{ $is_print = $this->input->post('is_print'.$r->m_code);}
            if ($this->input->post('is_re_print'.$r->m_code) == null){ $is_re_print = 0; }else{ $is_re_print = $this->input->post('is_re_print'.$r->m_code);}
            if ($this->input->post('is_back_date'.$r->m_code) == null){ $is_back_date = 0; }else{ $is_back_date = $this->input->post('is_back_date'.$r->m_code);}
            if ($this->input->post('is_authoriser'.$r->m_code) == null){ $is_authoriser = 0; }else{ $is_authoriser = $this->input->post('is_authoriser'.$r->m_code);}
            

            if ($this->input->post('is_approve'.$r->m_code) == null){ 
                $is_approve = 0;
            }else{
                $is_approve = $this->input->post('is_approve'.$r->m_code) ;
            }

            
            $a_det[] = array(
                "id"=>$lid,
                "role_id"=>$this->input->post('code'),
                "module_id"=>$this->input->post('m_code'.$r->m_code),
                "module_name"=>$this->input->post('m_description'.$r->m_code),
                
                "is_view" => $is_view,
                "is_add" => $is_add,
                "is_edit" => $is_edit,
                "is_delete" => $is_delete,
                "is_print" => $is_print,
                "is_re_print" => $is_re_print,
                "is_back_date" => $is_back_date,
                "is_authoriser" => $is_authoriser,
                "is_approve" => $is_approve
            );

        }

        if(count($a_det)){ 
            $this->db->insert_batch($this->tb_det, $a_det); 
        }

        echo 1;

    }

    private function set_delete(){
        
        $this->db->where('role_id', $this->input->post('code'))->delete($this->tb_det);
        $this->db->where('id',      $this->input->post('code_'))->delete($this->tb_sum);
        
    }

    public function auto_com(){
        
        $this->db->like("role_id", $_GET['term']);
        $this->db->or_like("description", $_GET['term']);
        $this->db->limit(25);        
        $query = $this->db->get($this->tb_sum);
        
        foreach($query->result() as $r){ 
            $ary [] = $r->id . " - ". $r->role_id . " - ".$r->description . " - "; 
                 
        }

        echo json_encode($ary);

    }

    public function dates(){
        $t = time(); 
        $a = array(); 
        $x = 0;

        foreach($this->db->get($this->mtb)->result() as $r){
        $std = new stdClass;

        if($r->type == 2){ 
            $r->range *= 7; 
        }

        $tt = $t - ($r->range * 84600);

        $std->date = date("Y-m-d", $tt);
        $std->description = $r->description;
        $std->key = substr(md5($r->description.$t), 0, 5);
        $a[] = $std; $x++;
    }

    return $a;
    }   



    public function load(){
        $q = $this->db->where('id', $this->input->post('id'))->get($this->tb_det);
        echo json_encode($q->result());
    }



}