<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_backup extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
        $this->load->dbutil();
    }
    
    public function base_details(){
        $a['s'] = 1;
		return $a;
    }    
    
    public function backup_db(){        

        $prefs = array(
            'tables'      => array(),                   // Array of tables to backup.
            'ignore'      => array(),                   // List of tables to omit from the backup
            'format'      => 'txt',                     // gzip, zip, txt
            'filename'    => 'mybackup.sql',            // File name - NEEDED ONLY WITH ZIP FILES
            'add_drop'    => TRUE,                      // Whether to add DROP TABLE statements to backup file
            'add_insert'  => TRUE,                      // Whether to add INSERT data to backup file
            'newline'     => "\n"                       // Newline character used in backup file
          );

        
        $backup =& $this->dbutil->backup($prefs);        
        
        $this->load->helper('file');
        
        write_file('application/backup/mybackup.gz', $backup);        
        
        $this->load->helper('download');
        
        force_download('mybackup.gz', $backup);
    }

}