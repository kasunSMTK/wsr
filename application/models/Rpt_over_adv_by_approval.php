<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_over_adv_by_approval extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }

    function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,'XL');        
    }
    
    function PDF_report($a,$rt = 'PDF'){

        if ($rt == 'PDF'){
            echo "This report only available in Excel format";
            exit;
        }

        if($_POST['bc_arry'] === null){
            $BC = " ";            
        }else{
            
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            $BC = " AND L.bc IN ($bc)  ";
        }

        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];


        $q = $this->db->query("SELECT L.`requiredamount`, B.`name` AS bc, L.ddate, L.billno, IF((requiredamount - L.goldvalue) > 0 ,(requiredamount - L.goldvalue),0) AS `extra_amount`  , LI.items,LI.total_weight, LI.pure_weight,C.`cusname`, C.`address`, ( SELECT uuu.discription FROM u_users uuu WHERE uuu.cCode = L.oc ) AS requested_by,


IF (  requiredamount > L.goldvalue , 

    IF ( requiredamount - L.goldvalue <= 1000 , 
        
        IF (AP.`approved_datetime` > '2018-10-20 00:00:00' ,        
        CONCAT('Manager approval by: ', U.`display_name`, '(', AP.`approved_by`,')' ,' ',AP.comment ),      
        IFNULL(`get_manager_by_date`(L.bc,L.ddate),'Manager approval')) , 

        IF (AP.`approved_datetime` > '2018-10-20 00:00:00' ,        
        CONCAT('H/O approval by: ', U.`display_name`, '(', AP.`approved_by`,')' , ' ', AP.comment ),      
        'Head Office Approval')
        
    )

 , '' ) AS `approved_by` , AP.`approved_datetime` , IF (LI.quality_changed = 1, LI.quality ,'100') AS quality_changed , LI.printval


FROM `t_loan` L

LEFT JOIN (  SELECT  li.`goldtype` , gr.`printval` , li.`quality`,SUM(li.`goldweight`) AS `total_weight` , SUM(li.`pure_weight`) AS `pure_weight`,bc,billno,IF((SUM(LI.quality) - COUNT(LI.billno) * 100) <> 0 , 1,0) AS `quality_changed` ,  GROUP_CONCAT(i.`itemname` ,'(',li.`pure_weight`,')',' ' , con.`des` , ' Q' , li.`quality`, '\n') AS `items` FROM t_loanitems li JOIN r_items i ON li.`itemcode` = i.`itemcode` JOIN `r_condition` con ON li.`con` = con.`code` LEFT JOIN `r_gold_rate` gr ON li.`goldtype` = gr.`id` GROUP BY li.`bc`, li.`billno`) LI ON L.`bc` = LI.`bc` AND L.`billno` = LI.`billno`

JOIN m_customer C ON L.`cus_serno` = C.`serno`
JOIN m_branches B ON L.`bc` = B.`bc`
LEFT JOIN ( SELECT * FROM t_approval_history GROUP BY auto_no ) AP ON L.`bc` = AP.bc AND L.`by_approval` = AP.auto_no
LEFT JOIN u_users U ON AP.`approved_by` = U.`cCode`

WHERE L.`status` = 'P' AND L.`ddate` BETWEEN '$fd' AND '$td' AND (requiredamount - L.goldvalue) > 0 $BC ");


        if($q->num_rows()>0){
            
            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            

            $this->load->view($_POST['by'].'_'.'excel',$r_detail);

        }else{
            echo "<script>alert('No data found');close();</script>";
        }        

    }   
    
}