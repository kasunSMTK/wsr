<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class appvl_je_approved extends CI_Model {
    
    private $sd;    
    private $max_no;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;
		return $a;
    }


    public function getApprovalRequests(){

    	$Q = $this->db->query("SELECT * , m_branches.`name` AS `bc`, u_users.`display_name` AS `oc` , a.bc AS `bc_` FROM `t_journal_entry_sum` 

JOIN ( SELECT bc,v_bc,NO FROM t_journal_entry_det GROUP BY bc,`no` ) a ON t_journal_entry_sum.`bc` = a.`bc` AND t_journal_entry_sum.`no` = a.`no`
JOIN m_branches ON t_journal_entry_sum.`bc` = m_branches.`bc`
JOIN u_users ON t_journal_entry_sum.`oc` = u_users.`cCode`

WHERE is_approved = 1 ORDER BY t_journal_entry_sum.bc,t_journal_entry_sum.`no` DESC  ");

    	if ($Q->num_rows() > 0){
    		$a['det'] = $Q->result();
    		$a['s'] = 1;
    	}else{
    		$a['s'] = 0;
    	}

    	echo json_encode($a);

    }


    public function makeAppCancel(){

        $this->db->trans_begin();

        $this->max_no = $_POST['no'];

        $Q = $this->db->query("UPDATE t_journal_entry_sum SET is_approved = 0 WHERE bc = '".$_POST['bc']."' AND `no` = '".$_POST['no']."' LIMIT 1");        

        if ($Q){            
            $a['s'] = 1;
            $this->db->trans_commit();
            $this->account_update_remove(1);
        }else{
            $a['s'] = 0;
            $this->db->trans_rollback();
        }

        echo json_encode($a);

    }


    public function account_update_remove($condition){

        $no = $this->max_no;
        $bc = $_POST['bc'];
        $Q1 = $this->db->query("DELETE FROM t_account_trans WHERE trans_code = 37 AND trans_no = '$no' AND bc = '$bc' ");    

    }

    
}