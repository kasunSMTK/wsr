<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_daily_summery_total_n extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function Excel_report($_POST_){
		
		$DATA['from_date'] 	= $_POST['from_date'];
		$DATA['to_date'] 	= $_POST['to_date'];
		$DATA['rtype']		= "excel";

		$this->PDF_report($DATA);
	}

	public function PDF_report($_POST_){

		if (!isset($_POST_['rtype'])){
			$_POST_['rtype'] = 'pdf';
		}

		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];		

		if (isset($_POST['bc_n'])){
	        if ( $_POST['bc_n'] == "" ){        
	            $QBC  = "";
	            $QBC2 = "";
	        }        
	    }else{

	        if ( !is_array($_POST['bc_arry']) ){
	            
	            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
	            
	            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
	                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
	            }

	            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
	        }

	        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
	            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
	        }

	        $bc = implode(',', $bc_ar);

	        if ($bc == ""){
	            $QBC   = "";
	            $QBC2  = "";
	        }else{
	            $QBC   = " L.bc IN ($bc) AND ";
	            $QBC2  = " lt.`bc` IN ($bc) AND ";
	        }
	    }


	    if ( $this->sd['isAdmin'] != 1 ){

	    	$QBC   = " L.bc IN ('".$this->sd['bc']."') AND ";
	        $QBC2  = " lt.`bc` IN ('".$this->sd['bc']."') AND ";

	    }



		/* check for old backup rpt_daily_summery_total.php for php based accrued_interest calculation */

		ini_set('max_execution_time', 300);

		$Q  = $this->db->query("SELECT 	B.`name` AS `bc_name`, 
	a.bc, 
	SUM(a.pawn_amount) AS `pawn_amount`, 
	SUM(a.redeem_amount) AS `redeem_amount`, 
	SUM(a.p_int) AS `p_int`,
	SUM(a.r_int) AS `r_int`,
	(SUM(a.p_int) + SUM(a.r_int)) AS `int_amount`, 
	SUM(a.g_tax) AS `g_tax`,
	SUM(a.postage) AS `postage`,
	
	SUM(a.total_stock) AS `total_stock`, 
	SUM(a.total_weight) AS `total_weight`,
	a.total_packets AS `total_packets`,
	sum(a.oc) AS `oc`	

	FROM (	
	
	SELECT a.bc, 0 AS `pawn_amount`, 0 AS `redeem_amount`, 0 AS `p_int`, 0 AS `r_int`, 0 AS `int_amount`, 0 AS `g_tax`, 0 AS `postage` , SUM(a.goldweight) AS `total_stock`, SUM(a.pure_weight) AS `total_weight`, SUM(a.total_packets) AS `total_packets`, 0 AS `oc` FROM  (	SELECT 	L.bc,LI.W AS `goldweight`,LI.P AS `pure_weight`, 1 AS total_packets FROM `t_loan` L JOIN (SELECT li.bc,li.`billno`, SUM(li.`goldweight`) AS `W`, li.pure_weight AS `P` FROM `t_loanitems` li GROUP BY li.`bc`,li.`billno`) LI ON L.`bc` = LI.bc AND L.`billno` = LI.billno

	WHERE $QBC L.`ddate` <= '$td' 

	UNION ALL 

	SELECT L.bc,LI.W AS `goldweight`,LI.P AS `pure_weight`,1 AS total_packets FROM `t_loan_re_fo` L JOIN 	  (	SELECT li.bc,li.`billno`,SUM(li.`goldweight`) AS `W`,li.pure_weight AS `P` FROM `t_loanitems_re_fo` li GROUP BY li.`bc`,li.`billno`) LI ON L.`bc` = LI.bc AND L.`billno` = LI.billno LEFT JOIN ( SELECT lt.ddate, lt.`bc`,lt.billno FROM `t_loantranse_re_fo` lt WHERE $QBC2 lt.`ddate` <= '$td' AND lt.`transecode` = 'R' GROUP BY lt.`bc`,lt.`billno` ) LT ON L.`bc` = LT.bc AND LT.billno = L.billno

	WHERE $QBC NOT L.`status` = 'C' AND L.`ddate` <= '$td'  AND ISNULL(LT.billno) ) a GROUP BY a.bc 
	
	


	


	UNION ALL
	
	/* pawning acc */
	/*SELECT bc, SUM(dr_amount) - SUM(cr_amount) AS `pawn_amount` ,0,0,0,0,0,0,0,0,0,0
	FROM t_account_trans L WHERE $QBC entry_code IN ('P','C') AND acc_code = '30201' AND ddate BETWEEN '$fd' AND '$td'
	GROUP BY bc,acc_code

	UNION ALL

	SELECT  bc, SUM(requiredamount) AS `pawn_amount` ,0,0,0,0,0,0,0,0,0,0
	FROM `loan_tblz_union` L WHERE $QBC `status` IN ('RN','AM') AND ddate BETWEEN '$fd' AND '$td'*/

	SELECT 	L.bc, SUM(L.`requiredamount`) AS `pawn_amount` ,0,0,0,0,0,0,0,0,0,0
	FROM `loan_tblz_union` L
	WHERE $QBC L.`status` IN ('P','AM','R','RN','L','PO') AND L.ddate BETWEEN '$fd' AND '$td'
	GROUP BY L.bc

	/*UNION ALL

	SELECT L.bc, SUM(L.`dr_amount`  - L.cr_amount) AS `pawn_amount` ,0,0,0,0,0,0,0,0,0,0
	FROM t_account_trans L 
	WHERE  $QBC L.`trans_code` = 37 AND L.`acc_code` = '30201' AND L.`ddate` BETWEEN '$fd' AND '$td'
	GROUP BY bc*/


	/* end pawning acc */

	UNION ALL
	
	/* redeem acc */
	SELECT bc,0, SUM(cr_amount) - SUM(dr_amount) AS `redeem_amount`  ,0,0,0,0,0,0,0,0,0
	FROM t_account_trans L
	WHERE $QBC trans_code IN (2,96) AND entry_code != 'C' AND acc_code = '30201' AND ddate BETWEEN '$fd' AND '$td'
	GROUP BY bc,acc_code

	UNION ALL

	SELECT  bc,0, SUM(requiredamount) AS `pawn_amount` ,0,0,0,0,0,0,0,0,0
	FROM `loan_tblz_union` L WHERE $QBC `status` IN ('RN','AM') AND ddate BETWEEN '$fd' AND '$td'
	GROUP BY bc
	/* end redeem acc */
	
	UNION ALL
	
	/* pawn int acc */
	SELECT bc,0,0, SUM(cr_amount) - SUM(dr_amount) AS `p_int`,0,0,0,0,0,0,0,0
	FROM t_account_trans L
	WHERE $QBC acc_code = '10101' AND ddate BETWEEN '$fd' AND '$td'
	GROUP BY bc,acc_code
	
	UNION ALL
	
	/* redeem int acc */
	SELECT bc,0,0,0, SUM(cr_amount) - SUM(dr_amount) AS `r_int`,0,0,0,0,0,0,0
	FROM t_account_trans L
	WHERE $QBC acc_code = '10102' AND ddate BETWEEN '$fd' AND '$td'
	GROUP BY bc,acc_code	
	
	 UNION ALL
	
	/* stamp fee (g_tax) acc 40212 */
	SELECT bc,0,0,0,0,0, SUM(cr_amount) - SUM(dr_amount) AS `g_tax`,0,0,0,0,0
	FROM t_account_trans L
	WHERE $QBC acc_code = '40212' AND ddate BETWEEN '$fd' AND '$td'
	GROUP BY bc,acc_code
	
	UNION ALL
	
	/* postage acc (doc charge) 10205 */
	SELECT bc,0,0,0,0,0,0, SUM(cr_amount) - SUM(dr_amount) AS `postage`,0,0,0,0
	FROM t_account_trans L
	WHERE $QBC acc_code = '10205' AND ddate BETWEEN '$fd' AND '$td'
	GROUP BY bc,acc_code

	UNION ALL

	/* outstanding capital */
	SELECT bc,0,0,0,0,0,0,0,0,0,0,SUM(dr_amount) - SUM(cr_amount) AS `oc`
	FROM t_account_trans L
	WHERE $QBC acc_code = '30201' AND ddate <= '$td'
	GROUP BY bc,acc_code


	
		


) a LEFT JOIN m_branches B ON a.bc = B.`bc` 

GROUP BY a.bc ");
		

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;
			$r_data['barnch']= $this->db->select("name")->where("bc",$bc)->get("m_branches")->row()->name;
			$r_data['bc'] = $QBC;
			
			
			if ($_POST_['rtype'] == 'pdf' ){
				$this->load->view($_POST['by'].'_'.'pdf',$r_data);				
			}else{
				$this->load->view($_POST['by'].'_'.'excel',$r_data);
			}

		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}