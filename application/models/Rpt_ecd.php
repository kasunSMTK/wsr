<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_ecd extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }

    function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,'XL');        
    }
    
    function PDF_report($a,$rt = 'PDF'){

        if ($rt == 'PDF'){
            echo "This report only available in Excel format";
            exit;
        }
        

        if($_POST['bc_arry'] === null){
            $BC = " ";            
        }else{
            
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            $BC = " ACC.v_bc IN ($bc) AND  ";
        }
        
        

        if ( $_POST['ecd'] == '' ){
            $ecd = '';            
        }else{
            $ecd = "VC.`code` = '".$_POST['ecd']."' AND ";
        }

        $fd  = $_POST["from_date"];
        $td  = $_POST["to_date"];


        /*$q = $this->db->query(" SELECT ACC.`v_class`, VC.`description` AS `vc_desc` , B.bc ,  b.name AS `bc_desc` ,acc_code,A.`description` AS `acc_desc`, SUM(dr_amount - cr_amount) AS `bal` FROM t_account_trans ACC

JOIN `m_voucher_class` VC ON ACC.`v_class` = VC.`code`
JOIN `m_account` A ON ACC.`acc_code` = A.`code`
JOIN m_branches B ON ACC.`v_bc` = B.`bc`

WHERE $BC $ecd ACC.`ddate` BETWEEN '$fd' AND '$td' AND A.`code` IN (SELECT CODE FROM `m_account` A WHERE A.`is_control_acc` = 0 AND LEFT(`type`,1) = 2)

GROUP BY ACC.`bc`,ACC.`acc_code`
ORDER BY ACC.bc,ACC.`acc_code` ");*/

        $q = $this->db->query("SELECT ACC.`v_class`, VC.`description` AS `vc_desc` , B.bc ,  b.name AS `bc_desc` ,acc_code,m_account_type.`heading` AS `acc_desc`, SUM(dr_amount - cr_amount) AS `bal` 

FROM t_account_trans ACC 

JOIN m_voucher_class VC ON ACC.`v_class` = VC.`code` 
JOIN m_account_type ON ACC.`acc_code` = m_account_type.`code` 
JOIN `m_branches` B ON ACC.`v_bc` = B.`bc`

WHERE $ecd ACC.bc IN (  SELECT bc FROM m_branches WHERE is_cost_center = 1) AND is_exp = 1 AND m_account_type.`rtype` = 2 AND ddate BETWEEN '$fd' AND '$td' 


GROUP BY v_bc,ACC.`acc_code`,ACC.v_class

ORDER BY ACC.`acc_code`


");


        if($q->num_rows()>0){
            
            $r_detail['list'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;            
            $r_detail['ecd'] = $ecd;
            $r_detail['bcc'] = $this->db->query("SELECT bc,NAME AS `bc_name` FROM m_branches")->result();
            $r_detail['acc'] = $this->db->query("SELECT `code`,`description` FROM m_account WHERE is_control_acc = 0 AND LEFT(TYPE,1) = 2")->result();


            $this->load->view($_POST['by'].'_'.'excel',$r_detail);

        }else{
            echo "<script>alert('No data found');close();</script>";
        }        

    }   
    
}