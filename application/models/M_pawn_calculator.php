<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_pawn_calculator extends CI_Model {
    
    private $sd;
    private $mtb;
    private $max_no;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){		
        


        $a['item_category']     = $this->getItemCategory();
        $a['gold_q']            = $this->getGoldQuality();
        $a['bcCustomer']        = $this->getBcCustomer();        
        
        
		
        return $a;
    }
    
       
    
    public function load(){
	   echo $this->load_list();  
    }
    
    

    

    public function getItemCategory(){
        $Q = $this->db->query("SELECT * FROM r_itemcategory ORDER BY des");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->code ." - ".$R->des." - ".$R->is_bulk."',"; }
        return  $d;
    }

    public function getGoldQuality(){
        $Q = $this->db->query("SELECT * FROM r_gold_quality ORDER BY code");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->rate ." - ".$R->code."',"; }
        return  $d;
    }

    public function getBcCustomer(){        
        $Q = $this->db->query("SELECT serno,nicno,cusname FROM m_customer ORDER BY cusname");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->serno ." - ".$R->nicno." -  ".$R->cusname."',"; }
        return  $d;
    }

    

    
    

}