<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_customer extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();
        $this->mtb = 'tblusers';        
    }
    
    public function base_details(){     
        $a['customer'] = $this->getCustomer();
        $a['load_list'] = $this->load_list();
        return $a;
    }
    
    public function save(){
        unset($_POST['cus_serno']);        
        $_POST['oc'] = $this->sd['oc'];

        if (!isset($_POST['bc'])){
            $_POST['bc'] = $this->sd['bc'];
        }else{

            if ($_POST['bc'] == "" || $_POST['bc'] == null){
                $_POST['bc'] = $this->sd['bc'];  
            }else{
                $_POST['bc'] = $_POST['bc'];
            }

        }

        //--------------------------------------

        $customer = $this->getCustomer();

        $_POST['customer_id'] = $customer['customer_id'];
        $_POST['customer_no'] = $customer['customer_no'];
        $_POST['cusname']     = str_replace("'", "", $_POST['cusname']);

        if (isset($_POST['isblackListed'])){
            $_POST['isblackListed'] = 1;
        }else{
            $_POST['isblackListed'] = 0;
        }


        $_POST['nicno'] = trim($_POST['nicno']);


        $Q_CHK_EXIT = $this->db->query("SELECT nicno FROM m_customer WHERE nicno = '".$_POST['nicno']."' ")->num_rows();
        
            if ($_POST['hid'] == "0"){
                
                if ($Q_CHK_EXIT == 0){
                
                    unset($_POST['hid']);

                    if($this->db->insert("m_customer",$_POST)){
                         $a['s'] = 1;
                         $a['cus_serno'] = $this->db->insert_id();
                         $a['customer_id'] = $_POST['customer_id'];
                    }else {
                        $a['s'] = 0; 
                        $a['n'] = $this->db->conn_id->errno; 
                        $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Customer (NIC) ");                
                    }

                }else{
                    $a['s'] = 0; 
                    $a['n'] = ""; 
                    $a['m'] = "Customer ID already added";            
                }

            }else{
                $hid = $_POST['hid'];
                unset($_POST['hid']);
                $this->db->where("customer_id",$hid)->update("m_customer",$_POST);
                $a['s'] = 1;            
            }

        

        echo json_encode($a);
    }
    
    
    public function load(){
       echo $this->load_list();  
    }
    
    public function delete(){
       $customer_id = $_POST['customer_id'];

       if ($this->db->query("DELETE FROM`m_customer` WHERE customer_id = '$customer_id' ")){
            $a['s'] = 1;
        }else{       
            $a['s'] = 0;            
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Customer");
        }

        echo json_encode($a);
    }

    public function load_list(){
        
        $Q = $this->db->query("SELECT C.*,IFNULL(B.`name`,'Unknown') AS `bc` FROM `m_customer` C LEFT JOIN `m_branches` B ON C.`bc` = B.`bc` ORDER BY `action_date` DESC LIMIT 10");
        
        $T = "<table class='tbl_customer'>";
        
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Des' style='padding-left:7px'>

                        " .$R->nicno . " - " .$R->cusname. "<br>" . $R->address . "<div class='div_cus_bc'>".$R->bc."</div>



                        </td><td class='ED' style='padding-right:7px'><a href=# onClick=setEdit('".$R->customer_id."')>Edit</a> | <a href=# onClick=setDelete('".$R->customer_id."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No customers found</center>";
        }
        
        $T .= "</table>";    
        
        $a['T'] =  $T;
        
        $a['customer'] = $this->getCustomer();

        return json_encode($a);
    }
    
    public function set_edit(){
        
        $customer_id = $_POST['customer_id'];

        $a['sum'] = $this->db->query("SELECT * FROM `m_customer` WHERE customer_id = '$customer_id' LIMIT 1")->row();       
        

        echo json_encode($a);
    }

    public function customer_pawn_statistics($customer_id){

        $Q = $this->db->query("SELECT 0 as pawn_amount_total,C.`serno`,C.`title`,C.`cusname`,C.`nicno`,C.`address`,C.`address2`,C.`mobile`,C.`telNo`,BB.name as `bc`,C.`nicno`,C.`isblackListed`, IFNULL(PDS.`pawnings`,0) AS `pawnings`, IFNULL(PDS.`redeemed`,0) AS `redeemed`, IFNULL(PDS.`forfiedted`,0) AS `forfiedted`, IFNULL(PDS.`canceled`,0) AS `canceled`, IFNULL(PDS.`last_show`,'') AS `last_show`, IFNULL(PDS.`last_billno`,'') AS `last_billno`, IFNULL(B.`name`,'') AS `last_bill_bc`, IF (TIMEDIFF(NOW(),last_show) < TIME('01:00:00'),'red','green') AS `alert_color`,IFNULL((PDS.`pawnings` - (PDS.`redeemed`+PDS.`canceled`)),0) AS `cus_pawn_count`, IF (C.max_allowed_pawn = 0,(SELECT PV.default_pawn_limit FROM m_pawning_values PV),C.max_allowed_pawn) AS `max_allowed_pawn` FROM m_customer C 

            LEFT JOIN t_cus_pawn_details_sum PDS ON C.`serno` = PDS.`customer_id`
            LEFT JOIN m_branches B ON PDS.`last_bill_bc` = B.`bc` 
            LEFT JOIN m_branches BB ON C.bc = BB.`bc`            

            WHERE C.`customer_id` = '$customer_id' LIMIT 1");

        return $Q->row();
        
    }    
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT C.*,IFNULL(B.`name`,'Unknown') AS `bc` FROM `m_customer` C LEFT JOIN `m_branches` B ON C.`bc` = B.`bc` WHERE (nicno like '%$k%' OR cusname like '%$k%' OR C.address like '%$k%' OR mobile like '%$k%') ");

        $T = "<table>";
        
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Des'>".$R->nicno . " - " .$R->cusname. "<br>" . $R->address . "<div class='div_cus_bc'>".$R->bc."</div></td><td class='ED'><a href=# onClick=setEdit('".$R->customer_id."')>Edit</a> | <a href=# onClick=setDelete('".$R->customer_id."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No customers found</center>";
        }
        $T .= "</table>";    
        echo $T;
            
    }

    public function getCustomer(){
        $max_no = $this->db->query("SELECT IFNULL(MAX(`customer_no`)+1,1) AS `maxno` FROM `m_customer`")->row()->maxno;
            
        $customer_no = $max_no;

        if (strlen($max_no) == 1){ $max_no = "00".$max_no; }
        if (strlen($max_no) == 2){ $max_no = "0".$max_no; }
        if (strlen($max_no) == 3){ $max_no = "".$max_no; }
        $customer_id = $this->sd['bc']."-".$max_no;
        
        $a['customer_id'] = $customer_id;
        $a['customer_no'] = $customer_no;

        return $a;
    }

}