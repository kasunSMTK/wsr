<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class R_tag extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    public function PDF_report($loanno, $fullmonth_int = 0)
    {
        /*var_dump($_POST);
        exit;*/
        $bc = $this->sd['bc'];

        $lno = 0;
        if (isset($_POST['r_no'])) {
            $lno = $_POST['r_no'];
        }

//$_POST['r_no'] = 101123624;

        $data = $this->db->query("  SELECT t.loanno, t.cat_code, cat.des, t.ser_no, t.item, t.tag_no,i.itemname ,
                                             c.`cusname`,c.`nicno`,d.amount,
                                            CONCAT(c.`title`,' . ',c.`cusname`) AS cusname, c.`address` AS `address`, s.nno , td.billno
                                            FROM `t_sales_sum` s
                                            INNER JOIN `t_sales_det` d ON s.`nno` = d.`nno`
                                            INNER JOIN t_tag AS t ON d.`tag_no` = t.`tag_no`
                                            INNER JOIN `r_itemcategory`  AS cat ON  t.cat_code  = cat.`code`
                                            INNER JOIN `r_items` AS i ON i.`itemcode` = t.item
                                            INNER JOIN `m_customer` c ON c.`serno` = s.`cus_serno`
                                            INNER JOIN `t_tag_det` AS td ON t.idno = td.auto_no
                                            WHERE s.`nno`= " . $lno . " and s.bc='" . $bc . "'
                                            UNION ALL
                                               SELECT 0 AS loanno, i.itemcate AS cat_code, cat.des, 0 AS ser_no, i.`itemcode` AS item, i.`itemcode` AS tag_no,i.itemname ,
                                               c.`cusname`,c.`nicno`,d.amount,
                                               CONCAT(c.`title`,' . ',c.`cusname`) AS cusname, c.`address` AS `address`, s.nno , d.billno
                                               FROM `t_sales_sum` s
                                               INNER JOIN `t_sales_det` d ON s.`nno` = d.`nno`
                                               INNER JOIN `r_items` AS i ON i.`itemcode` = d.item
                                               INNER JOIN `r_itemcategory`  AS cat ON  i.itemcate  = cat.`code`
                                               INNER JOIN `m_customer` c ON c.`serno` = s.`cus_serno`
                                               WHERE s.`nno`= " . $lno . " and s.bc='" . $bc . "'
                                               ORDER BY cat_code , ser_no ");

        $data1 = $this->db->query("  SELECT  c.`cusname`,c.`nicno`,ddate ,s.net_amount,
                                            CONCAT(c.`title`,' . ',c.`cusname`) AS cusname, c.`address` AS `address`, s.nno
                                            FROM `t_sales_sum` s
                                            INNER JOIN `m_customer` c ON c.`serno` = s.`cus_serno`
                                            WHERE s.`nno`= " . $lno . " and s.bc='" . $bc . "' ");

        // $data3 = $this->db->query("  SELECT 0 as loanno, i.itemcate as cat_code, cat.des, 0 as ser_no, i.`itemcode` as item, i.`itemcode` as tag_no,i.itemname ,
        //                                             c.`cusname`,c.`nicno`,d.amount,
        //                                             CONCAT(c.`title`,' . ',c.`cusname`) AS cusname, c.`address` AS `address`, s.nno , d.billno
        //                                             FROM `t_sales_sum` s
        //                                             INNER JOIN `t_sales_det` d ON s.`nno` = d.`nno`
        //                                             INNER JOIN `r_items` AS i ON i.`itemcode` = d.item
        //                                             INNER JOIN `r_itemcategory`  AS cat ON  i.itemcate  = cat.`code`
        //                                             INNER JOIN `m_customer` c ON c.`serno` = s.`cus_serno`
        //                                             WHERE s.`nno`= ".$lno." and s.bc='".$bc."'
        //                                             ORDER BY cat_code , ser_no
        //                                 ");

        if ($data->num_rows() > 0) {
            $r_detail['data'] = $data->result();
            $r_detail['nno'] = $lno;
            $r_detail['data1'] = $data1->row();
            $r_detail['is_reprint'] = $_POST['is_reprint'];
            $r_detail['RRR'] = $this->db->query("SELECT * FROM `m_branches` WHERE bc = '$bc'")->row();

            $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
        } else {

            echo "<script>alert('No data found');close();</script>";

        }

    }

}
