<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_new_pawn extends CI_Model {
    
    private $sd;
    private $mtb;
    private $max_no;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){
        $a['current_date'] = $this->sd['current_date'];
        $a['item_category'] = $this->getItemCategory();
        $a['gold_q'] = $this->getGoldQuality();
        //$a['bcCustomer'] = $this->getBcCustomer();
        $this->load->model("m_customer");
        $a['customer'] = $this->m_customer->getCustomer();
        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        $a['conditions']        = $this->conditions_dropdown();
        $a['extra_pawn_manager_allowed_amount'] = $this->extra_pawn_manager_allowed_amount();
        $a['backdate_upto'] = $this->sd['backdate_upto'];

		return $a;
    }

    public function extra_pawn_manager_allowed_amount(){
        return $this->db->select('extra_pawn_manager_allowed_amount')->get('m_pawning_values')->row()->extra_pawn_manager_allowed_amount;
    }

    public function conditions_dropdown(){

        $Q = $this->db->query("SELECT * FROM `r_condition`");

        $t = "<select class='select_drp' style='width:130px' id='condition_desc'>";

        if ($Q->num_rows() > 0){

            $t .= "<option value=''>Select condition</option>";

            foreach ($Q->result() as $R) {
                $t .= "<option value='".$R->code."'>".$R->code. " ". $R->des."</option>";
            }

        }else{
            $t .= "<option value=''>No conditions added</option>";
        }

        $t .= "</select>";

        return $t;
    }
    
    public function save(){


        $a['s'] = 33;
        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this bill and you might required re-login to continue";

        if (isset($this->sd['current_date'])){
            if (!$this->utility->is_valid_date($this->sd['current_date'])){
                echo json_encode($a);
                exit;
            }
        }else{            
            echo json_encode($a);
            exit;
        }   $a['s'] = ''; $a['msg_33'] = '';

        

        $db_debug = $this->db->db_debug; // AP
        $this->db->db_debug = FALSE;        // AP
        
        $_POST['stamp_fee'] = 0;

        //ini_set('max_execution_time', 150); //150 seconds = 2.5 minutes

        $this->db->trans_begin();   
        
        $this->load->model(array('calculate','user_permissions'));
        /*echo $this->user_permissions->is_add($this->uri->segment(3));
        exit;*/

        $loan_no        = $this->calculate->get_next_new_loan_number();
        $this->max_no   = $loan_no;
        $current_date   = $this->sd['current_date'];

        $bc             = $this->sd['bc'];
        $customer_id    = $_POST['customer_id'];
        
        $billtype       = $_POST['billtype'];         
        
        $B                      = $this->calculate->get_bill_no_by_billtype($billtype,$bc);                
        $billno                 = $_POST['billno'] = $B['billno'];
        $_POST['billtypeno']    = $B['billtypeno'];
        
        $dDate          = $_POST['dDate'];
        $_POST['time']  = date('H:i:s');
        $INT            = "";   
        $action_date    = date('Y-m-d H:i:s');
        $with_int       = 0;

        $_POST['ori_pwn_date'] = $_POST['dDate'];

        if ( $_POST['approval_id'] != 0 ){
            $_POST['requiredamount'] = $_POST['goldvalue'] + $_POST['extra_approved_amount'];
        }

        if (isset($_POST['allow_cal_with_advance'])){
            $_POST['int_with_amt'] = 1;
        }else{
            $_POST['int_with_amt'] = 0;
        }

        unset($_POST['allow_cal_with_advance'],$_POST['get_r_billtype']);

        for ( $no = 0 ; $no < count( $_POST['A'] ) ; $no++ ){            

            if ($_POST['is_bulk'] != 1){
                $itemcode_ = $_POST['B'][$no];
                $_POST['bulk_items'] = "";
            }else{
                $itemcode_ = $this->db->query("SELECT itemcode FROM `r_items` WHERE bulk_item_code = 1 LIMIT 1")->row()->itemcode;
                $_POST['bulk_items'] = $_POST['B'][$no];
            }

            $loan_item_det[] = array('loanno'=>$loan_no,'bc'=>$bc,'billtype'=>$_POST['billtype'], 'billno'=>$billno, 'cat_code'=>$_POST['A'][$no], 'itemcode'=>$itemcode_,'con'=>$_POST['C'][$no],'goldweight'=>$_POST['F'][$no],'pure_weight'=>$_POST['G'][$no],'denci_weight'=>$_POST['L'][$no], 'qty'=>$_POST['E'][$no] , 'goldtype'=>$_POST['D'][$no],'quality'=>$_POST['I'][$no],'goldvalue'=>$_POST['J'][$no],'value' => $_POST['H'][$no],'bulk_items' => $_POST['bulk_items'] );
        }

        $_POST['loanno']    = $loan_no;
        $_POST['bc']        = $bc;        
        $_POST['finaldate'] = date('Y-m-d',strtotime("+".$_POST['period']." months", strtotime($dDate)));
        $_POST['status']    = "P";
        $_POST['nmintrate'] = $_POST['fMintrate'];
        
        if ($_POST['is_pre_intst_chargeable'] == 1){
            $_POST['fmintrest'] = round( floatval($_POST['requiredamount']) * floatval($_POST['fMintrate']) / 100 );
        }else{
            $_POST['fmintrest'] = round( floatval($_POST['requiredamount']) * floatval($_POST['fMintrate']+$_POST['fMintrate2']) / 100 );
        }       

        if (isset($_POST['allow_full_month'])){
            $_POST['is_weelky_int_cal'] = 0;
        }else{
            $_POST['is_weelky_int_cal'] = 1;
        }



        $x = $this->calculate->get_int_paid_till_date($last_int_paid_date = $current_date,$_POST['is_weelky_int_cal'],true,0,0,$_POST['dDate']);

         $_POST['int_paid_untill'] = $x['till_date'];

        $_POST['oc'] = $this->sd['oc']; 
        $_POST['by_approval'] = $_POST['approval_id'] != 0 ? $_POST['approval_id'] : 0;
        
        unset( $_POST['A'],$_POST['B'],$_POST['C'],$_POST['D'],$_POST['E'],$_POST['F'],$_POST['G'],$_POST['H'],$_POST['I'],$_POST['J'],$_POST['L'],$_POST['bulk_items'],$_POST['is_bulk'],$_POST['customer_id'],$_POST['is_pre_intst_chargeable'] , $_POST['approval_id'],$_POST['allow_full_month'] , $_POST['extra_approved_amount']);

        $tmp_hid = $_POST['hid'];


        








        $sql = "SELECT loanno FROM t_loan L WHERE L.`bc` = '".$this->sd['bc']."' AND L.`ddate` = '".$_POST['dDate']."' AND L.`cus_serno` = '".$_POST['cus_serno']."' AND L.`totalweight` = '".$_POST['totalweight']."' AND L.`requiredamount` = '".$_POST['requiredamount']."'AND TIMEDIFF(NOW(), L.`action_date` ) < '00:04:59'LIMIT 1  ";

          $Q44 = $this->db->query($sql);

        if ( $Q44->num_rows() > 0 ){

            $data = "User:".$this->sd['oc']."|bc:".$this->sd['bc']."|ddate:".$_POST['dDate']."|cus_serno:".$_POST['cus_serno']."|totalweight:".$_POST['totalweight']."|requiredamount:".$_POST['requiredamount']."|datetime:".date('Y-m-d h:m:i');

            $this->db->query("INSERT INTO `t_bill_repeat_log_n`(`data`) VALUES ( '$data' ); ");
            $this->db->trans_commit();

            $a['s'] = 55;
            $a['er'] = 55;
            $a['q'] = $sql;
            $a['er_section'] = 2;
            echo json_encode($a);
            exit;
        }









        if ($_POST['hid'] == 0){
            unset($_POST['hid']);

            // loan tbale
        	$Q   = $this->db->insert("t_loan",$_POST); 
            $a['er'] = $this->db->error(); // AP

            $QQ  = $this->update_latest_bc_billno($billno,$bc);
            $this->calculate->loan_bill_int($loan_no,$bc,$billtype);


            if ($Q){

                // Loan Items Table
                $Q = $this->db->insert_batch("t_loanitems",$loan_item_det);
                $a['er'] = $this->db->error(); // AP

                // Loan Trans Table advance entry ( )            
                $_POST_TRANS['loanno'] = $loan_no; $_POST_TRANS['bc'] = $bc; $_POST_TRANS['billtype'] = $_POST['billtype'];$_POST_TRANS['billno'] = $billno; $_POST_TRANS['ddate'] = $dDate; $_POST_TRANS['amount'] = $_POST['requiredamount'];$_POST_TRANS['billextendedperiod'] = ""; $_POST_TRANS['transecode'] = "P"; $_POST_TRANS['transeno'] = $loan_no;$_POST_TRANS['discount'] = 0; $_POST_TRANS['app_rec_id'] = ""; $_POST_TRANS['action_date'] = $action_date; $_POST_TRANS['billtypeno'] = $_POST['billtypeno'];
                $Q = $this->db->insert("t_loantranse",$_POST_TRANS);
                $a['er'] = $this->db->error(); // AP

                // Loan trans table first ## days interest                
                
                $paying_amount = floatval($_POST['fmintrest']);

                $_PST['loanno']    = $_POST['loanno']; 
                $_PST['bc']        = $this->sd['bc'];
                $_PST['billtype']  = $_POST['billtype'];
                $_PST['billno']    = $_POST['billno']; 
                $_PST['billtypeno']= $_POST['billtypeno'];
                $_PST['ddate']     = $dDate; // ask what date should here  loan date + 8 days or loan date
                $_PST['amount']    = $paying_amount;
                $_PST['transecode']= "A";
                
                $this->load->model("t_part_payment");                
                
                $_PST['transeno']  = $this->t_part_payment->getPartPayTransNo();
                $_PST['discount']  = 0;
                $_PST['is_pawn_int'] = 1;
                                              
                if ($paying_amount > 0){    
                    $Q1       = $this->db->insert("t_loantranse",$_PST);  
                    $a['er']  = $this->db->error(); // AP
                    $with_int = 1;   
                }
                
                $_POST['hid']   = $tmp_hid;
                $_POST['date']  = $_POST['dDate'];
                
                $_POST['ln'] = $_PST['loanno'];
                $_POST['bt'] = $billtype;
                $_POST['bn'] = $billno;

                
                

                $account_update =   $this->account_update(0);
                
                if($account_update!=1){
                    $a['s'] = 0;
                    $a['m'] = "Invalid account entries";
                    $this->db->trans_rollback();
                    $a['s'] = "error";
                    echo json_encode($a);
                    exit;
                }else{
                    $account_update =   $this->account_update(1);
                }



            }else{
                $a['s'] = 0;
            }

        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("recid",$hid)->update("t_loan",$_POST);
        }


        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{
            
            $this->db->trans_commit();

            if ($Q){
                $a['s'] = 1;
                $a['loan_no'] = $loan_no;
                $a['INT'] = $INT;              
                
                $this->calculate->update_customer_pawn_details($_POST['cus_serno'],'P',$billno);

            }else{
                $a['s'] = 0;
            }
        }
        
        $a['with_int'] = $with_int;
        
        $this->db->db_debug = $db_debug;     // AP    

        echo json_encode($a);

    }

    public function setBillTypeValue($amount="",$goldvalue="",$return_type=""){

        $bc = $this->sd['bc'];
        if (isset($_POST['amount'])){ $amount = $_POST['amount']; }else{ $amount = $amount; }
        if (isset($_POST['goldvalue'])){ $goldvalue  = $_POST['goldvalue']; }else{ $goldvalue  = $goldvalue; }
        
        $Q = $this->db->query("SELECT S.billtype, S.period, D.`day_from`, D.`day_to`, D.`rate`, IF ( '$amount' <= (SELECT `min_advance_amount` FROM `m_pawning_values`), 0, 1 ) AS `weekly_cal` FROM  `r_bill_type_sum` S JOIN `r_bill_type_det` D ON S.`no` = D.`no` WHERE  '$amount' BETWEEN `amt_from` AND `amt_to` AND CASE WHEN `gratr_lesstn` = 'B' THEN (('$amount' / '$goldvalue') * 100) BETWEEN `percentage` AND `percentage_to` WHEN `gratr_lesstn` = 'L' THEN (('$amount' / '$goldvalue') * 100) <= `percentage` ELSE  (('$amount' / '$goldvalue') * 100) > `percentage` END AND bc = '$bc' AND S.`is_old_bill` = 0 AND S.is_active = 1 AND NOT is_R_bill = 1 ");
        
        if ($Q->num_rows() > 0){

            $this->load->model('calculate');

            $billtype        = $Q->first_row()->billtype;                        
            $B               = $this->calculate->get_bill_no_by_billtype($billtype,$bc);
            $billno          = $_POST['billno'] = $B['billno'];
            $a['bt_sum']     = $Q->result();
            $a['bt_max']     = $B['billno'];
            $a['billtypeno'] = $B['billtypeno'];
            $a['s']          = 1;

        }else{
            $a['s']          = 0;
        }        

        if ($return_type == ""){
            echo json_encode($a);
        }else{
            return $a;
        }
    }

    public function LOAD_LOAN(){        
        $billno = $_POST['billno'];
        $bc      = $this->sd['bc'];

        if ($this->db->query("SELECT billno FROM t_loan WHERE billno = '$billno' LIMIT 1")->num_rows() != 0){
            $tbl_name_L     = "t_loan";
            $tbl_name_LT    = "t_loantranse";
            $tbl_name_LI    = "t_loanitems";
        }else{
            $tbl_name_L     = "t_loan_re_fo";
            $tbl_name_LT    = "t_loantranse_re_fo";
            $tbl_name_LI    = "t_loanitems_re_fo";
        }


        //------------------------

            $this->load->model('user_permissions');
        
            if ($this->user_permissions->is_approve('multiple_bill_cancel') == 0){
                // if not approved
                $bc1 = "bc      = '$bc' AND ";
                $bc2 = "L.`bc`  = '$bc' AND ";
            }else{
                $bc1 = $bc2 = "";
            }

        //------------------------

        $Q1 = $this->db->query("SELECT * , LT.`transeno`,(SELECT IF (COUNT(transecode) > 0, transecode ,L.status) FROM t_loantranse_re_fo WHERE $bc1 billno = '$billno' AND transecode NOT IN('P','A')) AS `status`,L.`billtypeno` FROM `$tbl_name_L` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` JOIN `$tbl_name_LT` LT ON L.`billno` = LT.`billno` AND L.`bc` = LT.`bc` WHERE $bc2 L.billno = '$billno' LIMIT 1 ");
        
        if ($Q1->num_rows() > 0){
            $a['loan_sum'] = $Q1->row();
            
            $Q2 = $this->db->query("SELECT LI.`itemcode`,I.`itemname`,LI.`cat_code`,IC.`des`,C.`des` as `cdes`,LI.`con`,LI.`goldweight`,LI.`pure_weight`,LI.`denci_weight`,LI.`goldtype`,LI.`quality`,LI.`goldvalue`,LI.`value`
            
            ,GR.`id`,GR.`goldcatagory`
            ,GC.`code`,GC.`rate`,LI.`qty`,LI.`bulk_items`

            FROM `$tbl_name_LI` LI
                JOIN `r_itemcategory` IC ON LI.`cat_code` = IC.`code`
                JOIN `r_items` I ON LI.`itemcode` = I.`itemcode`
                JOIN `r_condition` C ON LI.`con` = C.`code`
                JOIN `r_gold_rate` GR ON LI.`goldtype` = GR.`id`
                JOIN `r_gold_quality` GC ON LI.`quality` = GC.`rate`
            WHERE billno = '$billno'");

            $a['loan_det'] = $Q2->result();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }

    public function CANCEL_BILL(){

        $loan_no = $_POST['loan_no'];
        $transeno= $_POST['transeno'];
        $bc      = $_POST['bcc'];
        
        $_POST['hid']= 0;                
        $_POST['ln'] = $loan_no;
        $_POST['bt'] = $_POST['billtype'];
        $_POST['bn'] = $_POST['billno'];

        $_POST['time'] = $_POST['time'];

        $prev_billno = $_POST['previous_billno'];

        $this->db->trans_begin();

        $this->load->model('user_permissions');

        
        if ($this->user_permissions->is_approve('multiple_bill_cancel') == 0){ // if not approved to delete multiple bills

            $Q = $this->db->query("SELECT billno FROM `t_last_bill_update` WHERE bc = '$bc' LIMIT 1");    
            
            if ($Q->num_rows() > 0){

                $last_bc_billno = $Q->row()->billno;

                if ( $last_bc_billno != $_POST['billno']){
                    $a['s'] = 2;
                    echo json_encode($a);        
                    exit;                
                }else{
                    $cancelled_with = "Branch last bill cancel permission";
                }

            }else{
                $cancelled_with = "Branch last bill cancel permission";
            }

        }else{
            $cancelled_with = "Multiple bill cancel permission";
        }

        $cancelled_by = $this->sd['oc'];
        
        $C = $this->db->query("SELECT loanno FROM `t_loantranse` WHERE bc = '$bc' AND NOT transecode = 'P' 
            AND NOT transeno = '$transeno' AND loanno = '$loan_no'");
          
        if ($C->num_rows() == 0){  
            
            // 2. if no, Rollback account trans table entry.
            //$Q1 = $this->db->query("DELETE FROM `t_account_trans` WHERE bc = '$bc' AND loanno = '$loan_no'");

            $this->max_no = $loan_no;            

            $account_update_rollback =   $this->account_update_rollback(0);                
                
            if($account_update_rollback != 1){
                $a['s'] = 0;
                $a['m'] = "Invalid account entries";
                $this->db->trans_rollback();
                $a['s'] = "error";
                echo json_encode($a);
                exit;
            }else{
                $account_update_rollback =   $this->account_update_rollback(1);
                $Q1 = true;
            }

            // 3. Update loan table status
            $Q2 = $this->db->query("UPDATE `t_loan` SET `status`='C' WHERE bc = '$bc' AND loanno = '$loan_no' LIMIT 1");


            // if current bill added by partpaymet move previous bill back to P status

            if ( $_POST['is_renew'] == 1 AND $_POST['reduced_amount'] > 0 ){

                $Q1 = $this->db->query("INSERT INTO t_loan       SELECT * FROM t_loan_re_fo L   WHERE L.`billno` = $prev_billno LIMIT 1 ");
                $Q2 = $this->db->query("INSERT INTO t_loanitems  SELECT * FROM t_loanitems_re_fo L WHERE L.`billno` = $prev_billno ");
                $Q3 = $this->db->query("INSERT INTO t_loantranse SELECT * FROM t_loantranse_re_fo L WHERE L.`billno` = $prev_billno ");

                if ($Q1 && $Q2 && $Q3){
                    $Q1 = $this->db->query("DELETE FROM t_loan_re_fo           WHERE `billno` = $prev_billno LIMIT 1 ");
                    $Q2 = $this->db->query("DELETE FROM t_loanitems_re_fo      WHERE `billno` = $prev_billno ");
                    $Q3 = $this->db->query("DELETE FROM t_loantranse_re_fo     WHERE `billno` = $prev_billno ");
                }


                $this->db->query("UPDATE t_loan SET status = 'P' WHERE billno = $prev_billno LIMIT 1 ");

            }


            // 4. Move data to re_fo table

            $this->load->model('calculate');
            $this->calculate->move_loan_data_to_RE_FO($loan_no,'C');
            $this->calculate->update_customer_pawn_details($_POST['cus_serno'],'C');
            $this->bill_cancel_log($bc,$_POST['billno'],$cancelled_by,$cancelled_with);
            
            if ($Q1 && $Q2){
                $this->db->trans_commit();
                $a['s'] = 1;
            }else{
                $this->db->trans_rollback();
                $a['s'] = 0;
            }

        }else{            
            // 1. Check this bill got any transactions? if yes, MUST UNABLE to cancel. return 0
            $a['s'] = 0;
        }        

        echo json_encode($a);
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $recid = $_POST['recid'];

       if ($this->db->query("DELETE FROM`t_loan` WHERE recid = '$recid' ")){
            echo 1;
       }else{
            echo 0;
       }
    }

    public function set_edit(){
        $recid = $_POST['recid'];
        $R = $this->db->query("SELECT * FROM `t_loan` WHERE recid = '$recid' LIMIT 1")->row();
        echo json_encode($R);
    }



    public function getCustomerInfo($cus_nic = ""){       

        ini_set('max_execution_time', 150); //150 seconds = 2.5 minutes
        
        if($_POST['chkShowSugg'] == 1){
            $QW = " C.`customer_id` = '".$_POST['cus_serno']."' ";
        }else{
            $QW = " C.`nicno`       = '".$_POST['customer_id']."' ";            
        }
        
        $Q = $this->db->query("SELECT C.`serno`,C.`customer_id`,C.`customer_no`,C.`nicno` FROM m_customer C WHERE $QW LIMIT 1");

        if ($Q->num_rows() > 0){
            $a['status']    = 1;
            $serno          = $Q->row()->serno;
            $customer_id    = $Q->row()->customer_id;
            $customer_no    = $Q->row()->customer_no;
            $nicno          = $Q->row()->nicno;
            

            $this->load->model(array("m_customer","t_message"));
            $data = $this->m_customer->customer_pawn_statistics($customer_id);
            $a['customer_pawn_statistics'] = $data;

            if ($data->last_show != ""){
                $a['tm'] = $this->t_message->calculate_time_span($data->last_show)." ago";
            }else{
                $a['tm'] = "";
            }

        }else{
            $a['status'] = 0;
        }

        echo json_encode($a);

    }      



    public function getCustomerInfoByNic($cus_nic = ""){       

        ini_set('max_execution_time', 150); //150 seconds = 2.5 minutes
        
        $nic = $cus_nic;            
            
        $Q = $this->db->query("SELECT * FROM `m_customer` WHERE nicno = '$nic' LIMIT 1");
        
        if ($Q->num_rows() > 0){                
            $T = "";
            $R = $Q->row();
            
            $T .= ' <div><h3>Full Name</h3><span>'.$R->title . " " . $R->cusname .'</span></div>
                    <div><h3>NIC</h3><span>'.$R->nicno.'</span></div>
                    <div><h3>Address 1</h3><span>'.$R->address.'</span></div>
                    <div><h3>Address 2</h3><span>'.$R->address2.'</span></div>
                    <div><h3>Mobile</h3><span>'.$R->mobile.'</span></div>
                    <div><h3>Phone</h3><span>'.$R->telNo.'</span></div>';
            
            $a = $T;
            
        }else{            
            $a['status']    = 0;
        }

        return $a;

    }



    public function getItemCategory(){
        $Q = $this->db->query("SELECT * FROM r_itemcategory WHERE is_bulk = 0 ORDER BY des ");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->code ." - ".$R->des." - ".$R->is_bulk."',"; }
        return  $d;
    }

    public function getGoldQuality(){
        $Q = $this->db->query("SELECT * FROM r_gold_quality ORDER BY code");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->rate ." - ".$R->code."',"; }
        return  $d;
    }

    public function getBcCustomer(){
        $q = $_GET['term'];
        $query = $this->db->query("SELECT customer_id,nicno,cusname FROM m_customer WHERE nicno LIKE '%$q%' OR cusname LIKE '%$q%' ORDER BY cusname limit 100");                
        $ary = array();    
        
        foreach($query->result() as $R){ 
            $ary[] = $R->customer_id . " - ". $R->nicno . " - " . $R->cusname ; 
        }
        
        echo json_encode($ary);
    }


    





















    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 1);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',1);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => $_POST['time'],
            "trans_code" => 1,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);

        


        $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');
        $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND'); // cash in hand
        $interest_received  = $this->utility->get_default_acc('PAWNING_INTEREST');
        

        if ($_POST['int_with_amt'] == 1){
            
            $pawning_amount_DR  = $_POST['requiredamount'];            
            $pawning_amount_CR  = $_POST['requiredamount'] - $_POST['fmintrest'];
            
            $this->account->set_value2("Pawning value",     $pawning_amount_DR,   "dr",   $pawn_stock,        $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'],"P");
            $this->account->set_value2("Pawning",           $pawning_amount_CR,   "cr",   $cash_book,         $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'],"P");        
            $this->account->set_value2("Interest Received", $_POST['fmintrest'],  "cr",   $interest_received, $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'],"A");
            
            $section = "A";

        }else{
            
            $pawning_amount  = $_POST['requiredamount'];           
            
            $this->account->set_value2("Pawning value",     $pawning_amount,     "dr",   $pawn_stock,        $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'],"P");
            $this->account->set_value2("Interest Received", $_POST['fmintrest'], "cr",   $interest_received, $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'],"A");
            $this->account->set_value2("Pawning",           $pawning_amount,     "cr",   $cash_book,         $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'],"P");        
            $this->account->set_value2("Pawning Interest",  $_POST['fmintrest'], "dr",   $cash_book,         $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'],"P");

            $section = "B";

        }  


        /*echo "UNREDEEM_ARTICLES Dr  ".($pawning_amount_DR - $_POST['fmintrest'])."<br>";
        echo "PAWNING_INTEREST  Cr  ".$_POST['fmintrest']."<br>";
        echo "CASH_BOOK         Cr  ".$pawning_amount_CR."<br>";
        echo "CASH_BOOK         Dr  ".$_POST['fmintrest']."<br>";

        exit;*/

        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='1'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 1);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }


    public function account_update_rollback($condition) {

        $QQQ = $this->db->query("SELECT * FROM `t_account_trans` WHERE trans_code = 1 AND  bc = '".$_POST['bcc']."' AND billno = '".$_POST['bn']."' LIMIT 40 ");

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 1);        
        $this->db->where("bc", $_POST['bcc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$_POST['bcc']);
            $this->db->where('trans_code',1);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => date('H:i:s'),
            "trans_code" => 1,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        $this->load->model('account');
        $this->account->set_data($config);

        
        foreach ($QQQ->result() as $R){

            if ($R->dr_amount > 0){
                $this->account->set_value2("Canceled ".$R->description, $R->dr_amount , "cr", $R->acc_code ,$condition,"",           $_POST['ln'],$_POST['bt'],$_POST['bn'],"C","",$_POST['bcc']);
            }

            if ($R->cr_amount > 0){
                $this->account->set_value2("Canceled ".$R->description, $R->cr_amount , "dr", $R->acc_code ,$condition,"",           $_POST['ln'],$_POST['bt'],$_POST['bn'],"C","",$_POST['bcc']);
            }
        
        }

        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $_POST['bcc'] . "'  AND t.`trans_code`='1'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 1);                
                $this->db->where("bc",$_POST['bcc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }


    public function update_latest_bc_billno($billno,$bc){

        $Q = $this->db->query("SELECT billno FROM `t_last_bill_update` WHERE bc = '$bc'");

        if ($Q->num_rows() > 0){
            $Q2 = $this->db->query("UPDATE `t_last_bill_update` SET `billno`='$billno' WHERE bc='$bc' LIMIT 1");        
        }else{
            $Q2 = $this->db->query("INSERT INTO `t_last_bill_update`(`bc`,`billno`) VALUES('$bc','$billno')");
        }

    }   

    private function bill_cancel_log($a,$b,$c,$d){
        $DATA['bc'] = $a;
        $DATA['billno'] = $b;
        $DATA['cancelled_by'] = $c;
        $DATA['cancelled_with'] = $d;
        $this->db->insert('t_bill_cancel_log',$DATA);
    }

    public function check_for_R_billtype_allow(){

        $bc = $this->sd['bc'];
        
        $Q = $this->db->query("SELECT BTS.billtype, BTS.period, BTD.`day_from`, BTD.`day_to`, BTD.`rate`, 1 AS `weekly_cal` FROM `r_bill_type_sum` BTS JOIN `r_bill_type_det` BTD ON BTS.`no` = BTD.`no` WHERE BTS.`bc` = '$bc' AND BTS.`is_R_bill` = 1");
        
        if ($Q->num_rows() > 0){

            $this->load->model('calculate');

            $billtype        = $Q->first_row()->billtype;                        
            $B               = $this->calculate->get_bill_no_by_billtype($billtype,$bc);
            $billno          = $_POST['billno'] = $B['billno'];
            $a['bt_sum']     = $Q->result();
            $a['bt_max']     = $B['billno'];
            $a['billtypeno'] = $B['billtypeno'];

            $a['R_gold_rates'] = $this->get_R_gold_rates();

            $a['s']          = 1;

        }else{
            $a['s']          = 0;
        }        

        
        echo json_encode($a);

    }

    public function get_R_gold_rates(){
        $Q = $this->db->query("SELECT * FROM `r_gold_rate` GR WHERE GR.`R_billtype_only` = 1");

        if ($Q->num_rows() > 0){
            return $Q->result();
        }else{
            return 0;
        }
    }


    public function varify_man_crdi(){

        $bc = $this->sd['bc'];
        $un = $_POST['un'];
        $pw = $_POST['pw'];

        $q = $this->db->query("SELECT cCode FROM `u_users` U 
            WHERE U.`bc` = '$bc' AND U.`bc_manager` = 1 AND U.`loginName` = '$un' AND U.`userPassword` = MD5('$pw') AND disable_login = 0 ");

        if ($q->num_rows() > 0){
            $a['s'] = 1;
            $a['cCode'] = $q->row()->cCode;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }

    public function get_system_billno(){

        $bc  = $this->sd['bc'];
        $MBN = $_POST['manual_bill_no'];
        $q   = $this->db->query(" SELECT billno FROM t_loan WHERE bc = '$bc' AND manual_billno = '$MBN' LIMIT 1 ");

        if ($q->num_rows() > 0){
            
            if ($_POST['cut_bc_no'] == 0){
                $a['billno'] = $q->row()->billno;
            }else{
                $a['billno'] = substr($q->row()->billno , 3,30);
            }

            $a['s'] = 1;
        }else{
            $q = $this->db->query(" SELECT billno FROM t_loan_re_fo WHERE bc = '$bc' AND manual_billno = '$MBN' LIMIT 1 ");            

            if ($q->num_rows() > 0){
                $a['billno'] = $q->row()->billno;
                $a['s'] = 1;
            }else{
                $a['s'] = 0;
            }
        }

        echo json_encode($a);

    }

    
}