<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_black_list_customer extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

   
    $sql="SELECT 

C.bc,
B.name,
C.`nicno`,
C.`customer_no` AS cus_serno,
C.`address`,
C.cusname,

COUNT(L1.billno) AS `ntranse` , 
IFNULL(SUM(requiredamount),0) AS `traseval` , 
IFNULL(SUM(bal),0) AS `rdmval` ,
IFNULL(SUM(requiredamount),0) - IFNULL(SUM(bal),0) AS `bal` 

FROM m_customer C

LEFT JOIN (   SELECT billno, requiredamount , requiredamount AS `bal` , cus_serno FROM t_loan GROUP BY billno 
    UNION ALL
    SELECT billno, requiredamount , 0 AS `bal` , cus_serno FROM t_loan_re_fo GROUP BY billno
    ) L1 ON C.`serno` = L1.cus_serno
  
JOIN m_branches B ON C.bc = B.bc

WHERE C.`isblackListed` = 1 ";
  
if($_POST['bc']!=""){
    $sql.="  AND  C.bc='".$_POST['bc']."'";
}

$sql .= "GROUP BY C.`nicno`";
$sql .= " ORDER BY C.bc";

    $query = $this->db->query($sql);


    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}