<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class remind_letter_list extends CI_Model {
    
    private $sd;
   
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){	   
	   
    }
    
    function PDF_report(){        

        $no = $_POST['l_list_no'];
        
        $Q1 = $this->db->query("SELECT * FROM `t_remind_letter_sum` WHERE `no` = '$no' LIMIT 1");

        if($Q1->num_rows()>0){

            $Q2 = $this->db->query("    SELECT DET.* , C.`cusname` , C.`address` , L.`requiredamount`
                                        FROM `t_remind_letter_det` DET
                                        JOIN t_loan L       ON DET.`billno` = L.`billno`
                                        JOIN m_customer C   ON L.`cus_serno` = C.`serno`
                                        WHERE DET.`no` = '$no'    ");
            
            $r_detail['sum'] =   $Q1->row();
            $r_detail['det'] =   $Q2->result();
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}