<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_customer_information_report extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function Excel_report(){
		$this->PDF_report($_POST_,'excel');
	}

	public function PDF_report($_POST_,$r_type = "pdf"){

		if (!is_array($_POST['bc_arry'])){
		 	$_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
		}		 

		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];
		$nic = $_POST['txt_rpt_cus_info_info'];

		if (isset($_POST['bc_n'])){
	        if ( $_POST['bc_n'] == "" ){        
	            $bc = "";
	        }        
	    }else{

	        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
	            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
	        }

	        $bc = implode(',', $bc_ar);

	        if ($bc == ""){	        	
	            $bc   = "";
	        }else{	        	
	            $bc   = "   L.bc IN ($bc) AND ";
	        }
	    }


	    // r_billtype filteration 
	    $billtype = "";
	    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
	        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
	    }

	    $qry1 = "
			SELECT * FROM (
				SELECT L.bc,B.`name` AS `bc_name`,L.loanno,LT.`billno`,L.`ddate` AS `pawn_date`,
				L.`totalweight`,L.`requiredamount`, '' AS `redeem_amount`, '' AS `status`,
				'' AS `redeem_date`,
				LI.`items`,L.`cus_serno`,C.`cusname`,C.`address`,C.`nicno`,C.`mobile`,
				'###.##' AS `outstanding_int`,LI.pure_weight_tot,'Ax' AS `tb`

				FROM `t_loan` L

				JOIN (  
					SELECT GROUP_CONCAT(lt.`transecode`) AS transecode ,lt.amount,lt.ddate,
					lt.`bc`,lt.`billtype`,lt.`billno`
    				FROM `t_loantranse` lt 
    				GROUP BY lt.`loanno`      
     			) LT 
     			ON L.`bc` = LT.bc AND L.`billtype` = LT.billtype AND L.`billno` = LT.billno
    
				JOIN (  
					SELECT  
					GROUP_CONCAT(CONCAT(' ', i.`itemname`,             
					  IF(ISNULL(li.pure_weight) OR li.pure_weight=0,'', CONCAT(' (',li.pure_weight,')')),
                      IF(ISNULL(li.elec_imei) OR li.elec_imei='','', CONCAT(' (IMEI No-',li.elec_imei,')')),
                      IF(ISNULL(li.elec_serno) OR li.elec_serno='','', CONCAT(' (SER.No-',li.elec_serno,')')),  
                      IF(ISNULL(li.veh_engine_no) OR li.veh_engine_no='','', CONCAT(' (ENG.No-',li.veh_engine_no,')')),  
                      IF(ISNULL(li.veh_chassis_no) OR li.veh_chassis_no='','', CONCAT(' (CHAS.No-',li.veh_chassis_no,')'))  
                      )) 
					AS `items`, 
					li.`bc`,li.`billtype`,li.`billno`, SUM(li.`pure_weight`) AS `pure_weight_tot` 
					FROM `t_loanitems` li 
    				JOIN `r_items` i ON li.`itemcode` = i.`itemcode`    
    				GROUP BY li.`loanno`  
     			) LI ON L.`bc` = LI.bc AND L.`billtype` = LI.billtype AND L.`billno` = LI.billno
     
				JOIN `m_branches` B     ON L.`bc` = B.`bc`
				JOIN `m_customer` C     ON L.`cus_serno` = C.`serno` 

				WHERE 	$bc 
						L.status != 'C' AND L.cus_serno = ( SELECT m_customer.`serno` FROM m_customer WHERE nicno = '$nic' LIMIT 1 )  
						$billtype
				

				GROUP BY L.`bc`,L.`billtype`,L.`billno`

				UNION ALL

				SELECT L.bc,B.`name` AS `bc_name`,L.loanno,LT.`billno`,L.`ddate` AS `pawn_date`,
				L.`totalweight`,L.`requiredamount`, LT. `redeem_amount`, LT.`status`,
				LT.`redeem_date`, LI.`items`,L.`cus_serno`,C.`cusname`,C.`address`,C.`nicno`,
				C.`mobile`,'###.##' AS `outstanding_int`,LI.pure_weight_tot,'Bx' AS `tb`

				FROM `t_loan_re_fo` L

				JOIN (  
					SELECT 
					GROUP_CONCAT(lt.`transecode`) AS transecode ,MAX(IF (lt.`transecode` IN ('R'), lt.amount ,'')) AS `redeem_amount`,
					MAX(IF (lt.`transecode` IN ('R'),'Redeemed','')) AS `status`,
					MAX(IF (lt.`transecode` = 'R',lt.`ddate`,'')) AS `redeem_date`,lt.
					`bc`,lt.`billtype`,lt.`billno`
    				FROM `t_loantranse_re_fo` lt 
    				GROUP BY lt.`loanno`      
     			) LT 
     			ON L.`bc` = LT.bc AND L.`billtype` = LT.billtype AND L.`billno` = LT.billno
    
				JOIN (  
					SELECT  
					GROUP_CONCAT(CONCAT(' ', i.`itemname`,
					  IF(ISNULL(li.pure_weight) OR li.pure_weight=0,'', CONCAT(' (',li.pure_weight,')')),
                      IF(ISNULL(li.elec_imei) OR li.elec_imei='','', CONCAT(' (IMEI No-',li.elec_imei,')')),
                      IF(ISNULL(li.elec_serno) OR li.elec_serno='','', CONCAT(' (SER.No-',li.elec_serno,')')),  
                      IF(ISNULL(li.veh_engine_no) OR li.veh_engine_no='','', CONCAT(' (ENG.No-',li.veh_engine_no,')')),  
                      IF(ISNULL(li.veh_chassis_no) OR li.veh_chassis_no='','', CONCAT(' (CHAS.No-',li.veh_chassis_no,')'))  
					)) 
					AS `items`, 
					li.`bc`,li.`billtype`,li.`billno`,SUM(li.`pure_weight`) AS `pure_weight_tot`
					FROM `t_loanitems_re_fo` li 
    				JOIN `r_items` i ON li.`itemcode` = i.`itemcode`    
    				GROUP BY li.`loanno`  
     			) LI 
     			ON L.`bc` = LI.bc AND L.`billtype` = LI.billtype AND L.`billno` = LI.billno
     
				JOIN `m_branches` B     ON L.`bc` = B.`bc`
				JOIN `m_customer` C     ON L.`cus_serno` = C.`serno` 

				WHERE 	$bc L.status != 'C' AND L.cus_serno = ( SELECT m_customer.`serno` FROM m_customer WHERE nicno = '$nic' LIMIT 1 ) $billtype
				
				GROUP BY L.`bc`,L.`billtype`,L.`billno`
				) AS AA 
				
				ORDER BY AA.bc,AA.pawn_date,AA.billno";
//	    var_dump($qry1); exit();
		$Q  = $this->db->query($qry1);
		

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;			

			$r_data['cus_info'] = $this->db->query("SELECT nicno,title,cusname,address,mobile,telNo FROM `m_customer` WHERE nicno = '$nic' LIMIT 1")->row();

			if ($r_type == "pdf"){            
	            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
	        }else{
	            $this->load->view($_POST['by'].'_'.'excel',$r_data);
	        }


		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}