<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class t_part_payment extends CI_Model {

    

    private $sd;

    

    function __construct(){

        parent::__construct();      

        $this->sd = $this->session->all_userdata();   

    }

    

    public function base_details(){     

        $a['current_date'] = $this->sd['current_date'];

        $a['max_no'] = $this->get_trans_no();

        $a['date_change_allow'] = $this->sd['date_chng_allow'];

        $a['bc_no'] = $this->sd['bc_no'];

        $a['backdate_upto'] = $this->sd['backdate_upto'];

        return $a;

    }





    public function remove_pp_transaction(){



        $tcode = $_POST['tcode'];

        $tno = $_POST['tno'];        

        $bc = $_POST['bc'];

        $bn = $_POST['bn'];

        $lst_pd_date_whn_update = $_POST['lst_pd_date_whn_update'];



        $this->db->trans_begin();



        $q = $this->db->query(" DELETE FROM `t_loantranse` 

                                WHERE bc = '$bc' AND billno = '$bn' AND model_trans_code = $tcode AND transeno = $tno AND billtypeno = 0 ");



        if ($q){

            

            $q = $this->db->query(" DELETE FROM `t_account_trans` 

                                    WHERE bc = '$bc' AND billno = '$bn' AND trans_code = 99 AND trans_no = $tno ");        

            

            $q = $this->db->query(" UPDATE t_loan 

                                    SET int_paid_untill = '$lst_pd_date_whn_update' 

                                    WHERE bc = '$bc' AND billno = '$bn' 

                                    LIMIT 1 ");            

        }



        if ($this->db->trans_status() === FALSE){

            

            $this->db->trans_rollback();

            $a['s'] = 0;

        

        }else{

            

            $this->db->trans_commit();

            $a['s'] = 1;

        }       



        echo json_encode($a);



    }





    public function save(){



        $a['s'] = 33;

        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this part payment bill and you might required re-login to continue";



        if (isset($this->sd['current_date'])){

            if (!$this->utility->is_valid_date($this->sd['current_date'])){

                echo json_encode($a);

                exit;

            }

        }else{            

            echo json_encode($a);

            exit;

        }   $a['s'] = ''; $a['msg_33'] = '';





        $paying_part = true;        



        if ( $_POST['transaction_type'] == 'rn' ){ 



            if ( $_POST['is_non_gold'] == 1 ){



                //if ( $_POST['cat_code'] == 15 ){



                    $qd = $this->db->query("SELECT ADDDATE('".$_POST['dDate']."', INTERVAL 1 MONTH) AS `m` ")->row()->m;



                    //echo $this->sd['current_date']." > ".$qd;



                    if ( $this->sd['current_date'] > $qd ){ // not allow to make renew

                        $a['s']         = 378;

                        $a['msg_33']    = 'Unable to renew this bill. Montht grace period has expired';

                        echo json_encode($a);

                        exit;

                    }else{



                        if (($_POST['loan_amount'] + $_POST['amount']) > $_POST['goldvalue']){



                            $a['s']         = 378;

                            $a['msg_33']    = 'Unable to renew this bill. Capital exceed item value';

                            echo json_encode($a);

                            exit;



                        }else{



                            $paying_part = false;



                        }



                    }



                //}



            }



        }





        $this->db->trans_begin();



        $this->load->model('calculate');        



        $pp_amount  = $_POST['amount'];

        $billno     = $_POST['bc_no'].$_POST['billno'];

        $billtype   = $_POST['billtype'];

        $loanno     = $_POST['loanno'];        



        $_POST['date'] = $this->sd['current_date'];

        $_POST['time'] = date("H:i:s");



        $trans_no = $this->get_trans_no();



        $last_int_paid_upto_date = $_POST['int_paid_untill'];



        if ($paying_part){



            // retrive interest data for this bill



            $a['int'] = $this->calculate->calculate_interest( $_POST['loan_amount'], $_POST['billtype'], $_POST['dDate'], $current_date = $this->sd['current_date'], $first_mon_int_cal_range = '', $_POST['loanno'], 0, $_POST['int_paid_untill'], $_POST['is_amount_base_int_cal'],$_POST['fm_int_paid'], $_POST['ori_pwn_date'] );

                    

            $payable_tot_int_balance    = floatval($a['int']['payable_tot_int_balance']);

            $undue_payable_int          = floatval($a['int']['undue_payable_int']);

            $capital_deduct_amount      = 0;



            if ( $payable_tot_int_balance < $pp_amount ){

                

                $capital_deduct_amount  = $pp_amount - $payable_tot_int_balance;

                $pp_amount              = $payable_tot_int_balance;



            }



            if ( $undue_payable_int > 0 ){                



                $this->db->insert('t_loantranse',   array(  'loanno' => $loanno,'bc' => $this->sd['bc'], 'billtype' => $billtype, 

                                                            'billno' => $billno, 'ddate' => $this->sd['current_date'], 

                                                            'amount' => $undue_payable_int, 

                                                            'transecode' => 'PD', 

                                                            'transeno' => $trans_no,

                                                            'model_trans_code' => 99,

                                                            'lst_pd_date_whn_update' => $last_int_paid_upto_date

                                                        ));

            }



            if ( $pp_amount > 0){



                



                $this->db->insert('t_loantranse',   array(  'loanno' => $loanno, 'bc' => $this->sd['bc'], 'billtype' => $billtype, 

                                                            'billno' => $billno, 'ddate' => $this->sd['current_date'], 

                                                            'amount' => $pp_amount * -1, 

                                                            'transecode' => 'PP', 

                                                            'transeno' => $trans_no,

                                                            'model_trans_code' => 99,

                                                            'lst_pd_date_whn_update' => $last_int_paid_upto_date

                                                        ));





                if ( $this->sd['current_date'] == $_POST['dDate'] ){                

                    $this->update_paid_untill_date($billno);

                }elseif( $this->sd['current_date'] > $_POST['int_paid_untill']  ){

                    $this->update_paid_untill_date($billno);

                }else{

                    // Do nothing

                }

            

                // acc

                $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');

                $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');

                $interest_received  = $this->utility->get_default_acc('PAWNING_INTEREST');



                $config = array("ddate" => $_POST['date'], "time" => $_POST['time'], "trans_code" => 99, "trans_no" => $trans_no, "op_acc" => 0, "reconcile" => 0, "cheque_no" => 0, "narration" => "", "ref_no" => '');

                

                $this->load->model('account');

                $this->account->set_data($config);



                $this->account->set_value2( "interest received", $pp_amount,   "cr",   $interest_received,  1,"",  $loanno,$billtype,$billno,"PP");

                $this->account->set_value2( "interest received", $pp_amount,   "dr",   $cash_book,          1,"",  $loanno,$billtype,$billno,"PP");

                // END ACCOUNT



            }



            if ( $capital_deduct_amount > 0 ){



                



                $this->db->insert('t_loantranse',   array(  'loanno' => $loanno, 

                                                                'bc' => $this->sd['bc'], 

                                                                'billtype' => $billtype, 

                                                                'billno' => $billno, 

                                                                'ddate' => $this->sd['current_date'], 

                                                                'amount' => $capital_deduct_amount * -1, 

                                                                'transecode' => 'P', 

                                                                'transeno' => $trans_no,

                                                                'model_trans_code' => 99,

                                                                'lst_pd_date_whn_update' => $last_int_paid_upto_date ));

                // acc

                $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');

                $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');

                $interest_received  = $this->utility->get_default_acc('PAWNING_INTEREST');



                $config = array("ddate" => $_POST['date'], "time" => $_POST['time'], "trans_code" => 99, "trans_no" => $trans_no, "op_acc" => 0, "reconcile" => 0, "cheque_no" => 0, "narration" => "", "ref_no" => '');

                

                $this->load->model('account');

                $this->account->set_data($config);



                $this->account->set_value2( "Capital received", $capital_deduct_amount,   "cr",   $pawn_stock,  1,"",  $loanno,$billtype,$billno,"PP");

                $this->account->set_value2( "Capital received", $capital_deduct_amount,   "dr",   $cash_book,   1,"",  $loanno,$billtype,$billno,"PP");

                // END ACCOUNT



            }



        }else{



            



            $this->db->insert('t_loantranse',   array(  'loanno' => $loanno, 

                                                                'bc' => $this->sd['bc'], 

                                                                'billtype' => $billtype, 

                                                                'billno' => $billno, 

                                                                'ddate' => $this->sd['current_date'], 

                                                                'amount' => $pp_amount, 

                                                                'transecode' => 'P', 

                                                                'transeno' => $trans_no,

                                                                'model_trans_code' => 99,

                                                                'lst_pd_date_whn_update' => $last_int_paid_upto_date ));



            // acc

                $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');

                $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');                



                $config = array("ddate" => $_POST['date'], "time" => $_POST['time'], "trans_code" => 99, "trans_no" => $trans_no, "op_acc" => 0, "reconcile" => 0, "cheque_no" => 0, "narration" => "", "ref_no" => '');

                

                $this->load->model('account');

                $this->account->set_data($config);



                $this->account->set_value2( "Capital Increase", $pp_amount,   "dr",   $pawn_stock,  1,"",  $loanno,$billtype,$billno,"PP");

                $this->account->set_value2( "Capital Increase", $pp_amount,   "cr",   $cash_book,   1,"",  $loanno,$billtype,$billno,"PP");

                // END ACCOUNT



        }





        if ($this->db->trans_status() === FALSE){

            

            $this->db->trans_rollback();

            $a['s'] = "error";

        

        }else{

            

            $this->db->trans_commit();

            $a['s'] = 1;

        }



        echo json_encode($a);



    }





    public function update_paid_untill_date($billno){



        // int_paid untill date update after one month period



        $current_date           = $this->sd['current_date'];                

        $int_paid_untill        = $this->db->query("SELECT DATE_ADD('".$_POST['int_paid_untill']."', INTERVAL -1 MONTH) AS `d`")->row()->d;

        $next_int_untill_date   = '';



//        $interval = date_diff(date('Y-m-d',strtotime($current_date) ), date('Y-m-d',strtotime($int_paid_untill)));

//        var_dump($interval);

//        exit();



        for( $i = 0 ; $i < 100 ; $i++  ){ 



            $d = $int_paid_untill = $this->db->query("SELECT DATE_ADD('$int_paid_untill', INTERVAL 1 MONTH) AS `d`")->row()->d;



            //echo $d.'<br>';



            if ( date("Y-m",strtotime($d)) == date("Y-m",strtotime($current_date)) ){

                $next_int_untill_date = $d;

                if(date("Y-m",strtotime($_POST['int_paid_untill'])) == date("Y-m",strtotime($current_date)) ){

                    $next_int_untill_date = $this->db->query("SELECT DATE_ADD('$d', INTERVAL 1 MONTH) AS `d`")->row()->d;

                }

                else if(date("d",strtotime($d)) < date("d",strtotime($current_date)) ){

                    $next_int_untill_date = $this->db->query("SELECT DATE_ADD('$d', INTERVAL 1 MONTH) AS `d`")->row()->d;

                }

            }



        }



        if ($next_int_untill_date != ''){

            $this->db->where( array('billno' => $billno ,'bc' => $this->sd['bc'] ) )->limit(1)->update('t_loan' ,array("int_paid_untill" => $next_int_untill_date) );

        }

        

        // end int_paid untill date update after one month period



    }

    

    

    public function LOAD_LOAN(){

        

        $billtype = $_POST['billtype'];

        $billno = $_POST['bc_no'].$_POST['billno'];

        $int_cal_to_DATE = $_POST['dDate'];

        $five_days_int_cal = $_POST['five_days_int_cal'];

        $bc = $this->sd['bc'];



        $Q1 = $this->db->query("SELECT L.cat_code, L.is_non_gold, L.ori_pwn_date, L.is_amt_base, L.fm_int_paid, L.nmintrate, L.time, L.goldvalue, L.old_bill_age, L.is_renew,L.billno,L.bc,C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,LT.amount AS requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,C.`customer_id`,L.int_cal_changed,L.cus_serno,L.am_allow_frst_int,L.int_paid_untill,L.is_weelky_int_cal,L.fmintrest FROM `t_loan` L 



            JOIN (SELECT billno,bc, SUM(amount) AS `amount` FROM t_loantranse WHERE transecode = 'P' GROUP BY billno ) LT ON L.`billno` = LT.`billno` AND L.`bc` = LT.`bc`



            JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `m_bill_type` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE L.billno = '$billno' AND L.bc = '$bc' LIMIT 1 ");



        if ($Q1->num_rows() > 0){

            

            $a['loan_sum'] = $Q1->row();

            $loan_no = $Q1->row()->loanno;            

            

            $this->load->model("calculate");

            $this->load->model("t_new_pawn");

            $this->load->model("m_billtype_det");            

            

            $a['int'] = $this->calculate->calculate_interest(   $req_amount = $Q1->row()->requiredamount, 

                                                                $billtype = $Q1->row()->billtype, 

                                                                $pawn_date = $Q1->row()->ddate, 

                                                                $current_date = $this->sd['current_date'], 

                                                                $first_mon_int_cal_range = '',

                                                                $loanno = $Q1->row()->loanno,

                                                                $five_days_int_cal,

                                                                $int_paid_untill = $Q1->row()->int_paid_untill,

                                                                $is_amount_base_int_cal = $Q1->row()->is_amt_base,

                                                                $fm_int_paid = $Q1->row()->fm_int_paid,

                                                                $Q1->row()->ori_pwn_date



                                                            );





            $a['customer_advance'] = $this->calculate->customer_advance($Q1->row()->customer_id,$billno);

            



            $a['partpay_histomer'] = $this->calculate->get_bill_transaction_history($bc,$loan_no,$billno);



            

            $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;

            $a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);

            $a['s'] = 1;

            $a['section'] = 1;

        

        }else{



            $Q2 = $this->db->query("SELECT L.cat_code,L.is_non_gold,L.ori_pwn_date,L.is_amt_base,L.fm_int_paid, L.nmintrate,L.time,L.goldvalue, L.old_bill_age, L.is_renew, L.billno, L.bc,C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,L.int_cal_changed ,  (SELECT IF (COUNT(transecode) > 0, transecode ,L.status) FROM t_loantranse_re_fo WHERE bc = '$bc' AND `billno` = '$billno' AND transecode NOT IN('P','A','ADV','AS','PD','PP')) AS `status`,L.am_allow_frst_int,L.int_paid_untill,L.fmintrest  FROM `t_loan_re_fo` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `m_bill_type` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE    billno = '$billno' AND L.bc = '$bc' LIMIT 1 ");



            if ($Q2->num_rows()>0){

                $a['loan_sum']  = $Q2->row();

                $loan_no        = $Q2->row()->loanno;            

            

                $this->load->model("calculate");

                $this->load->model("t_new_pawn");

                $this->load->model("m_billtype_det");

                

                //$a['int'] = $this->calculate->interest($loan_no,"json",$int_cal_to_DATE, $a['loan_sum'],$five_days_int_cal);

                $a['int'] = $this->calculate->calculate_interest(   $req_amount = $Q2->row()->requiredamount, 

                                                                    $billtype = $Q2->row()->billtype, 

                                                                    $pawn_date = $Q2->row()->ddate, 

                                                                    $current_date = $this->sd['current_date'], 

                                                                    $first_mon_int_cal_range = '',

                                                                    $loanno = $Q2->row()->loanno,

                                                                    $five_days_int_cal,

                                                                    $int_paid_untill = $Q2->row()->int_paid_untill,

                                                                    $is_amount_base_int_cal = $Q2->row()->is_amt_base,

                                                                    $fm_int_paid = $Q2->row()->fm_int_paid,

                                                                    $Q2->row()->ori_pwn_date

                                                            );

                $a['customer_advance'] = $this->calculate->customer_advance($Q2->row()->customer_id,$billno);

                $a['partpay_histomer'] = $this->calculate->get_bill_transaction_history($bc,$loan_no);

                $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;

                $a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);

                $a['s'] = 1;

                $a['section'] = 2;

            }else{

                $a['s'] = 0;

            }



        }



        echo json_encode($a);

    }    

    

    

    public function get_trans_no(){



        $bc = $this->sd['bc'];



        return $this->db->query("SELECT GREATEST(



                                (SELECT IFNULL(MAX(`transeno`)+1,1) FROM `t_loantranse`         WHERE bc = '$bc' AND `model_trans_code` = 99),

                                (SELECT IFNULL(MAX(`transeno`)+1,1) FROM `t_loantranse_re_fo`   WHERE bc = '$bc' AND `model_trans_code` = 99)



                                ) AS `max_no`")->row()->max_no;



    }







    



    public function load_transaction(){



        $a['s'] = 0;



        echo json_encode($a);



    }



    





    





}