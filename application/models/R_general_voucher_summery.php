
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_general_voucher_summery extends CI_Model{

  private $sd;
  private $mtb;
  
  private $mod = '003';
  
  function __construct(){
    parent::__construct();

    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);
    
  }
  
  public function base_details(){

    return $a;
  }
  
  

  public function PDF_report(){

    $this->db->select(array('name'));
    $r_detail['company'] = $this->db->get('m_company')->result();
    $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $this->sd['branch']);
    $r_detail['branch'] = $this->db->get('m_branches')->result();

    $cl=$this->sd['cl'];
    $bc=$this->sd['branch'];

    $r_detail['page']='A4';
    $r_detail['orientation']='L';  

    $cluster=$_POST['cluster'];
    $branch=$_POST['branch'];
    $acc=$_POST['acc_code'];
    $no_range_frm=$_POST['t_range_from'];
    $no_range_to=$_POST['t_range_to'];

    $r_detail['cluster1']=$cluster;
    $r_detail['branch1']=$branch;
    $r_detail['acc']=$_POST['acc_code'];
    $r_detail['acc_des']=$_POST['acc_code_des'];
    $r_detail['t_no_from']=$_POST['t_range_from'];
    $r_detail['t_no_to']=$_POST['t_range_to'];
    $r_detail['dfrom']=$_POST['from'];
    $r_detail['dto']=$_POST['to'];

    $date_range=$_POST['from'];
    $acc_code=$_POST['acc_code'];
    $no_range=$_POST['t_range_from'];

    if(!empty($_POST['branch'])){

      $this->db->select('name');
      $this->db->where('bc',$_POST['branch']);
      $r_detail['branch_name']=$this->db->get('m_branches')->first_row()->name;
    }


    $sql1="SELECT

    sm.`nno`,
    sm.`ddate`,
    sm.`note`,
    sm.`cheque_amount`,
    sm.`cash_amount`,
    sm .cl,
    sm .bc,
    MBC.`name`AS bc_name, 
    MCL.`description` AS cl_description  
    FROM `t_voucher_gl_sum` sm
    INNER JOIN `m_branches` MBC  ON sm .bc = MBC.bc 
    INNER JOIN `m_cluster` MCL ON MBC.`cl` = MCL.`code`
    WHERE `is_cancel` != '1' ";

    $sql1.=" AND sm.ddate between '".$_POST['from']."' AND '".$_POST['to']."'";




    if(!empty($branch))
    {
      $sql1.=" AND sm.bc = '$branch'";
    }

    $sql1.="ORDER BY sm.bc,sm.nno ASC";


    $query=$this->db->query($sql1); 

    $r_detail['r_branch_name']=$this->db->query($sql1)->result();       



    if($this->db->query($sql1)->num_rows()>0)
    {
      $this->load->view($_POST['by'].'_'.'pdf',$r_detail);
    }
    else
    {
      echo "<script>alert('No Data');window.close();</script>";
    }
  }
}