<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class T_day_cach_bal extends CI_Model
{

  private $sd;
  private $mtb;
  private $max_no;
  private $bc_glob;

  function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);    
    $this->load->model('user_permissions');
    
  }
  
  public function base_details(){

    $this->load->model(array('m_branches','m_voucher_class','m_bank'));   
    $a['get_max_no']= $this->get_max_no();
    $a['sys_cash']= $this ->load_sys_cash();
    $a['list'] = $this->cash_list();

    $a['current_date']              = $this->sd['current_date'];
    $a['date_change_allow']         = $this->sd['date_chng_allow'];
    $a['backdate_upto']             = $this->sd['backdate_upto'];
    return $a;
  }

public function cash_list(){

    $q = $this->db->query(" SELECT t_day_cash_bal.*, u_users.`discription` FROM t_day_cash_bal 

LEFT JOIN u_users ON t_day_cash_bal.`oc` = u_users.`cCode`

WHERE t_day_cash_bal.bc = '".$this->sd['bc']."'

ORDER BY nno DESC ");


    $T = '';

    if ($q->num_rows() > 0){

        $T = '<style>

        .tbl_cash_bal td{
            padding:6px;
            border:1px solid #cccccc;
        }

        .tbl_cash_bal tr:hover{
            background-color:#eaeaea;
        }

        </style>';


        $T .= "<table border='1' class='tbl_cash_bal' width='100%'>";
        $T .= "<tr>";
        $T .= "<td>No</td>";
        $T .= "<td>Date</td>";
        $T .= "<td align='right'>System Cash</td>";
        $T .= "<td align='right'>Manual Cash</td>";
        $T .= "<td align='right'>Difference</td>";
        $T .= "<td>Status</td>";
        $T .= "<td>Added By</td>";
        $T .= "<td>Added Date Time</td>";
        $T .= "</tr>";

        foreach($q->result() as $r){

            $T .= "<tr>";
            $T .= "<td>".$r->nno."</td>";
            $T .= "<td>".$r->ddate."</td>";
            $T .= "<td align='right'>".$r->system_cash."</td>";
            $T .= "<td align='right'>".$r->maunal_cash."</td>";
            $T .= "<td align='right'>".$r->difference."</td>";
            
            if ($r->is_cencel == 1){
                $T .= "<td>Canceled</td>";
            }else{
                $T .= "<td>On</td>";
            }

            $T .= "<td>".$r->discription."</td>";
            $T .= "<td>".$r->actiondate."</td>";
            $T .= "</tr>";

        }

        $T  .= "</table>";

    }else{

    $T = 'Data not found';

    }


    

    return $T;

}

  public function save(){

    if ( floatval($_POST['dif_cash']) >= 250 || floatval($_POST['dif_cash']) <= -250 ){
      $a['s'] = 3;
      echo json_encode($a);
      exit;
    }

    $_POST['bc'] = $this->bc_glob = $this->sd['bc'];

    if ( $_POST['hid'] == "0" || $_POST['hid'] == "") {
      $_POST['nno'] = $this->max_no   = $this->get_max_no('t_day_cash_bal');
    }else{
      $_POST['nno'] = $this->max_no   = $_POST['hid'];
    }

     $a = array(
          "nno"           =>$this->max_no,
          "ddate"         =>$_POST['date'],
          "bc"            =>$_POST['bc'],
          "system_cash"   => $_POST['sys_cash'],
          "maunal_cash"   => $_POST['manual_cash'],
          "difference"    => $_POST['dif_cash'],
          "acc"           => $_POST['to_acc'],
          "oc"            => $this->sd['oc'],
          "time"          => $_POST['time']
         );

    
    $q = false;

    $_POST_TMP['time'] = $_POST['time'];
    $_POST['time'] = date('H:i:s');

    if ($this->validation() == 1) {
      
      if ( $_POST['hid'] == "0" || $_POST['hid'] == "") {
          $a['time'] = date('H:i:s');
          $this->account_update(1);
          $this->db->insert("t_day_cash_bal", $a);
          $this->utility->user_activity_log($module='Daily Cash Balance',$action='insert',$trans_code='98',$trans_no=$_POST['nno'],$note='');

      }else{
          $_POST['time'] = $_POST_TMP['time'];
          $this->account_update(1);
          unset($_POST['hid']);
          $this->db->where('nno', $_POST['nno']);
          $this->db->where('bc',$_POST['bc']);
          $this->db->delete("t_day_cash_bal");
          $this->db->insert("t_day_cash_bal", $a);
          $this->utility->user_activity_log($module='Daily Cash Balance',$action='update',$trans_code='98',$trans_no=$_POST['nno'],$note='');
      }

    }

    $a['update_det'] = "Cash Balance updated at ".date('Y-m-d H:i:s');
    $a['s'] = 1;
    $a['update_det'] = "";            


    echo json_encode($a);

  }

         //-----------------------------load default acc-------------------------------------------


        public function load_def_acc(){
          $sql="SELECT 
                m.acc_code,
                m.description AS acc_desc
                FROM m_default_account m
                WHERE m.code='DAY_CASH_BALANCE'";
          $query    = $this->db->query($sql);
          $a['det'] = $query->result();

          echo json_encode($a);
        }
        //----------------------------------------------------------------------------------------


         //-----------------------------load system Cash-------------------------------------------


        public function load_sys_cash(){
         
           $bc = $this->sd['bc'];
           $ddate=date('Y-m-d');

          $sql="SELECT 
                  IFNULL((SUM(a.dr_amount) - SUM(a.cr_amount)),0) AS balance 
                FROM
                  t_account_trans a 
                 LEFT JOIN m_default_account d ON  a.acc_code = d.acc_code
                WHERE d.code = 'CASH_IN_HAND' 
                  AND a.ddate <= ' $ddate' 
                  AND a.bc = '$bc'";

          $query_det =$this->db->query($sql);

          $bal =$query_det->first_row()->balance;
          return   $bal;
        }
        //----------------------------------------------------------------------------------------


        public function account_update($condition){       

          $this->db->where("trans_no", $this->max_no);
          $this->db->where("trans_code", 98);
          $this->db->where("cl", $this->sd['cl']);
          $this->db->where("bc", $this->bc_glob);
          $this->db->delete("t_check_double_entry");

          if ($condition == 1) {
            if ($_POST['hid'] != "0" || $_POST['hid'] != "") {
              $this->db->where('cl', $this->sd['cl']);
              $this->db->where('bc', $this->bc_glob);
              $this->db->where('trans_code', 98);
              $this->db->where('trans_no', $this->max_no);
              $this->db->where('entry_code',0);
              $this->db->delete('t_account_trans');
            }
          }

          $config = array(
            "ddate"         => $_POST['date'],
            "time"         => $_POST['time'],
            "trans_code"    => 98,
            "trans_no"      => $this->max_no,
            "op_acc"        => 0,
            "reconcile"     => 0,
            "cheque_no"     => 0,
            "narration"     => "",
            "ref_no"        => 0
          );

          
          $this->load->model('account');
          $this->account->set_data($config);   

          $dess = "Difference Account";
      
          if ($_POST['dif_cash'] < 0 ){            
            
            $this->account->set_value2($dess,abs($_POST['dif_cash']), "cr", $_POST['to_acc'],$condition,"","","","","");                       
            $this->account->set_value2($dess,abs($_POST['dif_cash']), "dr", $this->utility->get_default_acc("CASH_IN_HAND"),$condition,"","","","","");

          }else{            
            
            $this->account->set_value2($dess,abs($_POST['dif_cash']), "cr", $this->utility->get_default_acc("CASH_IN_HAND"), $condition,"","","","","");
            $this->account->set_value2($dess,abs($_POST['dif_cash']), "dr", $_POST['to_acc'], $condition,"","","","","");

          }
           
          if ($condition == 0) {

            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='".$this->sd['cl']."'  AND t.`bc`='".$this->bc_glob."' AND t.`trans_code`='98'  AND t.`trans_no` ='" . $this->max_no . "' AND t.entry_code = '0' AND a.`is_control_acc`='0' ");

            if ($query->row()->ok == "0") {        
              $this->db->where("trans_no", $this->max_no);
              $this->db->where("trans_code", 98);
              $this->db->where("cl", $this->sd['cl']);
              $this->db->where("bc", $this->bc_glob);
              $this->db->delete("t_check_double_entry");        
              return "0";
            } else {        
              return "1";
            }

          }

        }

        public function account_cancel($condition){

          $bc = $this->sd['bc'];   

          $config = array(
            "ddate" => $this->sd['current_date'],
            "trans_code" => 98,
            "trans_no" => $_POST['nno'],
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => $_POST['ref_no']
            );

          $des = "Difference Account - Cancel ";
          $this->load->model('account');
          $this->account->set_data($config);

          $q = $this->db->where(array("trans_code" => 98,"trans_no" => $_POST['nno'],"bc"=>$bc))->get('t_account_trans');

          foreach($q->result() as $r){        

            if ($r->dr_amount !=  0){
              $dess = "Difference Account - Cancel";
              $this->account->set_value4($dess,$r->dr_amount,"cr",$r->acc_code, $condition,"","", 0);
            }

            if ($r->cr_amount !=  0){
              $dess = "Difference Account - Cancel";
              $this->account->set_value4($dess,$r->cr_amount,"dr",$r->acc_code, $condition,"","", 0);
            }

          }


        }

        public function get_max_no(){
          $bc = $this->sd['bc'];
          return $this->db->query(" SELECT IFNULL(MAX(nno)+1,1) AS `max_no` FROM `t_day_cash_bal` V WHERE  bc = '$bc' ")->row()->max_no; 
        }


//--------------------------new pdf report 17_03_2017----------------------------------------------------
      public function load_receipt(){

        $bc  = $this->sd['bc'];
        $nno = $_POST['nno'];

        $q = $this->db->query(" SELECT s.*, AC.description AS acc_desc FROM t_day_cash_bal s

          LEFT JOIN m_account AC ON s.acc = AC.code 

          WHERE s.bc = '$bc' AND s.nno = '$nno' LIMIT 1 ");

        if ($q->num_rows() > 0){

          $a['sum'] = $q->row();
          $a['s']   = 1;

        }else{

          $a['s'] = 0;

        }

        echo json_encode($a);

      }



      public function validation(){

        $status         = 1;
        $account_update = $this->account_update(0);
        
        if ($account_update != 1){
          return "Invalid account entries";
        }
        
        return $status;

      }


    public function cancel_entry(){

        $bc = $this->sd['bc'];
        $no = $_POST['no'];

        $q  = $this->db->query("UPDATE `t_day_cash_bal` SET `is_cencel` = 1 WHERE bc = '$bc' AND nno = '$no' LIMIT 1 ");

        if ($q){
            $q2 = $this->db->query("DELETE FROM t_account_trans WHERE bc='$bc' AND trans_code=98 AND trans_no=$no LIMIT 2 ");
        }

        if ($q2){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }



}