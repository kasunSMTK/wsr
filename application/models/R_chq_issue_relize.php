<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  r_chq_issue_relize  extends CI_Model{

  private $sd;
  private $mtb;
  
  private $mod = '003';
  
  function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);

    //$this->m_customer = $this->tables->tb['m_customer'];
   // $this->t_sales_sum=$this->tables->tb['t_sales_sum'];

  }
  
  public function base_details(){

    $a['table_data'] = $this->data_table();
    
    return $a;
  }
  

  public function PDF_report(){

        //company details=====
/*    $this->db->select('name');
    $query_det = $this->db->get('s_company');
    $r_detail['company'] =$query_det->row()->name; */
    
    $this->db->select(array('name','address','telno','faxno','email'));
    $this->db->where("cl",$this->sd['cl']);
    $this->db->where("bc",$this->sd['branch']);
    $r_detail['branch']=$this->db->get('m_branches')->result();

    
    $r_detail['page']='A4';
    $r_detail['orientation']='L';  

    $r_detail['dfrom']=$_POST['from'];
    $r_detail['dto']=$_POST['to'];
    $r_detail['type']="r_chq_issue_relize";          
    $bank=$_POST['bank_id'];
    $r_detail['branch_code']=$_POST['branch'];
    
    $cl=$this->sd['cl'];
    $bc=$this->sd['branch'];

    if(!empty($_POST['branch'])){
      $this->db->select(array('name'));
      $this->db->where("bc",$_POST['branch']);
      $r_detail['branch_name']=$this->db->get('m_branches')->first_row()->name;

      $branch = "AND CI.bc='".$_POST['branch']."'";
    }else{
      $branch="";
    }

    $sql = "SELECT 

    CI.`ddate` AS `date`,
    CI.`bank` AS `bank_code`,
    CI.`account` AS `acc_no`,
    CI.`cheque_no`,
    CI.`amount`,
    CI.`bank_date` AS `realize_date`

    FROM `t_cheque_issued` CI
    WHERE CI.`bank_date` BETWEEN '".$_POST['from']."' AND '".$_POST['to']."'
    $branch
    ORDER BY  CI.`ddate` ";


    
    $r_detail['row']=$this->db->query($sql)->result(); 
    

    if($this->db->query($sql)->num_rows()>0)
    {
      $this->load->view($_POST['by'].'_'.'pdf',$r_detail);
    }
    else
    {
      echo "<script>alert('No Data');window.close();</script>";
    }

  }
}