<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class s_users extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'u_users';        
    }
    
    public function base_details(){		
		$a['a'] = 1;    	
		return $a;
    }    
    
    public function authenticate(){

    	$IP_AUTHENDICATION_ALLOW 	= false;
    	$ip_auth_verify_required_bc = array('PR','PMN');
    	$login_bc 					= trim(strtoupper($_POST['company']));

    	// ------------- start Branch IP confirmation - 2017-11-10 ---------------------
    	
    	if (in_array($login_bc, $ip_auth_verify_required_bc)){
	    	
	    	// get last saved IP from branch
	    	
	    	if ($IP_AUTHENDICATION_ALLOW){

		    	$last_IP_Q 			= $this->db->query("SELECT ip FROM `ip_table` LIMIT 1");   	
				$last_IP 			= $last_IP_Q->num_rows() > 0 ? $last_IP_Q->row()->ip : '';
		    	$request_branch_IP 	= $_SERVER['REMOTE_ADDR'];

		    	if ( $last_IP != $request_branch_IP ){
		    		$a['s'] = 5;
		    		echo json_encode($a);
		    		exit;
		    	}

		    }

		}

    	// ------------- end Branch IP confirmation - 2017-11-10 ---------------------

    	$ROWS 		= false;
    	$Q 			= $this->db->query("SET time_zone ='+05:30'");
    	$login_bc 	= trim(strtoupper($_POST['company']));
		$Q1 		= $this->db->query("SELECT U.multi_bc_login, U.`area`, U.`isAdmin`, U.`cCode`, U.discription, U.bc, B.`name` , U.`is_HO_cashier`, U.`isHO`, B.`bc_no`, B.`other_bc_name_allow`,U.`disable_login` FROM `".$this->mtb."` U JOIN `m_branches` B ON U.`bc` = B.`bc` WHERE `loginName` = ".$this->db->escape($_POST['userName'])." AND `userPassword` = md5(".$this->db->escape($_POST['userPassword']).") AND 1 = (SELECT COUNT(bc) FROM m_branches WHERE bc='$login_bc' LIMIT 1 ) LIMIT 1 ");

		$disable_login = '';

		if ( $Q1->num_rows() > 0 ){			

			if ( $Q1->row()->multi_bc_login == 1 ){
				$ROWS = true;				
				$result = $Q1;
			}else{
				$query 	= "SELECT U.`area`, U.`isAdmin`, U.`cCode`, U.discription, U.bc, B.`name` , U.`is_HO_cashier`, U.`isHO`, B.`bc_no`, B.`other_bc_name_allow` FROM `".$this->mtb."` U JOIN `m_branches` B ON U.`bc` = B.`bc` WHERE `loginName` = ".$this->db->escape($_POST['userName'])." AND `userPassword` = md5(".$this->db->escape($_POST['userPassword']).") AND U.`bc` = '".$login_bc."' LIMIT 1 ";
				$result = $this->db->query($query);

				if ($result->num_rows() > 0){
					$ROWS = true;
				}else{
					$ROWS = false;
				}

			}

			$disable_login = $Q1->row()->disable_login;
			
		}


		if ( $disable_login == 1 ){

			$a['s'] = 4;

		}elseif($ROWS){
		    
		    $r = $result->first_row();	    

		    $b = $this->db->query("SELECT B.`name`,B.`bc_no`, B.`other_bc_name_allow` FROM `m_branches` B WHERE B.`bc` = '$login_bc' LIMIT 1 ")->row();

		    if ($r->isAdmin == 1){ 

		    	$dca = 'date_ch_allow' ; 
		    	$backdate_upto = '';
		    
		    }else{

		    	$Q = $this->db->query("SELECT IF (ISNULL(MAX(backdate_upto)),1,0) AS `no_data`,MIN(backdate_upto) AS `backdate_upto`  FROM `m_branch_backdate` WHERE `status` = 'active' AND bc LIKE '%".$result->first_row()->bc."%' AND LEFT(`valid_upto`,10) >= '".date('Y-m-d')."' ");

		    	if ( $Q->num_rows() > 0 ){

		    		if ($Q->row()->no_data == 0){
		    			$dca = 'date_ch_allow_n' ;
		    			$backdate_upto = $Q->row()->backdate_upto;
		    		}else{
		    			$dca = '' ;
		    			$backdate_upto = date('Y-m-d');
		    		}

		    	}else{
		    		$dca = '' ; 		    		
		    		$backdate_upto = date('Y-m-d');
		    	}

		    }

		  //  $SQL = "SELECT isAuth FROM `com_auth` WHERE `com_id` = '".$_POST['com_id']."' LIMIT 1 ";
		    // remove below line and Main.php line 110, to enable pc reg option
		    $SQL = "SELECT 1 as `isAuth`";

		    $Q2 = $this->db->query($SQL);
		    
		    if ($Q2->num_rows() > 0){

		    	if ($Q2->first_row()->isAuth == 1){

		    		if ($r->discription == 'no_name_added'){
		    			$user_des = $r->cCode;
		    		}else{
		    			$user_des = $r->discription;
		    		}
		    		
				    $session_data = array(
						"is_login"=>true,
						"isAdmin"=>$r->isAdmin,
						"oc"=>$r->cCode,
						"user_des"=>$user_des,
						"bc"=> strtoupper($login_bc),
						"sto"=>0,
						"db" => "default",
						"cl" => "C1",
						"branch" => strtoupper($login_bc),
						"current_date" => date('Y-m-d'),
						"user_branch" => $b->name,
						"isHO" =>$r->isHO,
						"date_chng_allow" => $dca,
						"bc_no" => $b->bc_no,
						"is_HO_cashier" => $r->is_HO_cashier,
						"zone" => $r->area,
						"other_bc_name_allow" => $b->other_bc_name_allow,
						"backdate_upto" => $backdate_upto
				    );

		    		$this->session->set_userdata($session_data);
				    $this->load->model('user_permissions');
				    $this->user_permissions->add_permission($r->cCode);
		    		
		    		$a['s'] = 1;

		    		$this->utility->user_activity_log(
		    			$module='Authentication', 
		    			$action='login', 
		    			$trans_code='', 
		    			$trans_no='', 
		    			$note='',
		    			$oc=$r->cCode,
		    			$bc=strtoupper($login_bc)
		    		);

		    	}else{
		    		
		    		$this->session->set_userdata(array("reg_required"=>true));
		    		$a['s'] = 3;

		    	}

		    }else{
		    	
		    	$this->session->set_userdata(array("reg_required"=>true));
		    	$a['s'] = 3;

		    }

		}else{
		    $a['s'] = 0;
		}

		echo json_encode($a);
    }    


    public function select(){
    	$query = $this->db->order_by("loginName")->get($this->mtb);

    	$s = "<select name='loginName' id='user'>";
    	$s .= "<option value='0'>---</option>";
    	foreach($query->result() as $r){
    		$s .= "<option title='".$r->loginName."' value='".$r->cCode."'>".$r->loginName." - ".$r->display_name."</option>";
    	}
    	$s .= "</select>";

    	return $s;
    }


}