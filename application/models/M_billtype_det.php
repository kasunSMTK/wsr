<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_billtype_det extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){			
        $a['maxno'] = $this->max_no();
        $a['load_list'] = $this->load_list();
        $this->load->model("m_branches");
        $a['branches'] = $this->m_branches->select();
        $a['branches_list_chk'] = $this->branches_list();
        $a['bt_no'] = $this->bt_no();
		return $a;
    }

    public function bt_no(){

        return $this->db->query('SELECT IFNULL(MAX( billtype_in_no )+1,100) AS `bt_no` FROM `m_bill_type`')->row()->bt_no;

    }

    public function branches_list(){
        $Q = $this->db->query("SELECT bc,`name` FROM `m_branches` WHERE bc_type = 1 ORDER BY `name`"); 
        if ($Q->num_rows() > 0){
            $t = ""; foreach ($Q->result() as $R) {
                $t .= '<div class="lbl_bcs" style="border:0px solid red"><label><input type="checkbox" class="chkbc_" name="chk_bc[]" value="'.$R->bc.'"> '.strtoupper($R->bc).' - '.strtoupper($R->name).' </label></div>';
                 } }else{$t = 'No branches found'; }
        return $t;
    }

    public function max_no(){

        return $this->db->query("SELECT IFNULL(MAX(CONVERT(no, SIGNED INTEGER))+1,1) AS `max_no` FROM `m_bill_type` ")->row()->max_no;

    }



    public function save(){

        $hid                        = $_POST['hid']; unset($_POST['hid']);
        $_POST_SUM['no']            = $_POST['no'] = $this->max_no();        
        $_POST['created_by']        = $this->sd['oc'];
        $_POST['created_datetime']  = $datetime = date('Y-m-d H:i:s');

        if (isset($_POST['chk_bc'])){
            
            if ( $hid != "0" ){

                $this->db->query("INSERT INTO m_bill_type_log SELECT * FROM m_bill_type WHERE billtype = '".$_POST['billtype']."' ");
                $modified_by        = $this->sd['oc'];
                $modified_datetime  = date('Y-m-d H:i:s');        
                $this->db->query("UPDATE m_bill_type_log SET modified_datetime = '$modified_datetime' , modified_by = '$modified_by' WHERE billtype = '".$_POST['billtype']."' ");
                $this->db->query("DELETE FROM m_bill_type WHERE billtype = '".$_POST['billtype']."' ");

            }
            
            $bc_checked = $_POST['chk_bc']; unset($_POST['chk_bc']);

            for ($num = 0 ; $num < count( $bc_checked ) ; $num++){
                $_POST['bc'] = $bc_checked[$num];                    
                $this->db->insert("m_bill_type",$_POST);
            }   

            $a['s'] = 1;

        }else{

            $a['s'] = 2;

        }        

        echo json_encode($a);

        exit;
        
    }

    private function check_for_bc_exist($bc,$billtype){

        $q = $this->db->query("SELECT bc FROM `r_bill_type_sum` WHERE bc = '$bc' AND  billtype = '$billtype' ");

        if ($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    private function check_for_billtype_exist($billtype){

        $q = $this->db->query("SELECT billtype FROM `r_bill_type_sum` WHERE billtype = '$billtype' LIMIT 1 ");

        if ($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }
    
    
    public function load(){
	   echo $this->load_list($_POST['bc']);  
    }
    
    public function delete(){

	   $billtype = $_POST['billtype'];

       if ($this->used_billtype_check($billtype)){
            $a['s'] = 0; 
            $a['n'] = $this->db->conn_id->errno; 
            $a['m'] = "Unable to delete, bill type in use";

       }else{

           if ($this->db->query("DELETE FROM`r_bill_type_det` WHERE NO IN (SELECT NO FROM `r_bill_type_sum` WHERE billtype = '$billtype')")){
                $this->db->query("DELETE FROM `r_bill_type_sum` WHERE billtype = '$billtype' ");
                $a['s'] = 1;
            }else {
                
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Bill Type");
            }
        }

        echo json_encode($a);
    }

    public function load_list($bc=""){

        $Q = $this->db->query("SELECT no, billtype, period, amount_from, amount_to FROM `m_bill_type` group by billtype");

        $T = "<table border=1 class='tbl_bt'>";

        $T .= "<tr>";
        $T .= "<td>Bill Type</td>";
        $T .= "<td style='text-align:right' >Amt From</td>";
        $T .= "<td style='text-align:right' >Amt To</td>";
        $T .= "<td style='text-align:right' >Period</td>";        
        $T .= "<td style='text-align:right' >Action</td>";
        $T .= "</tr>";
        

        if ($Q->num_rows() > 0){            
            
            foreach($Q->result() as $R){
                

                $T .= "<tr>";
                $T .= "<td>".$R->billtype."</td>";
                $T .= "<td style='text-align:right' >".$R->amount_from."</td>";
                $T .= "<td style='text-align:right' >".$R->amount_to."</td>";
                $T .= "<td style='text-align:center' >".$R->period."</td>";                
                $T .= "<td style='text-align:right' ><a href=# onClick=setEdit('".$R->no."')>Edit</a> | <a href=# onClick=setDelete('".$R->no."')>Delete</a></td>";
                $T .= "</tr>";

            //    $T .= "<tr><td class='Cod'>".$R->no . "</td><td class='D'>-</td><td class='Des'>Rs " .$R->amt_from. " To Rs".$R->amt_to." </td><td class='ED'><a href=# onClick=setEdit('".$R->no."')>Edit</a> | <a href=# onClick=setDelete('".$R->no."')>Delete</a></td></tr>";
            

            }

        }else{
            $T = "<center>No bill type rates found</center>";
        }
        




        $T .= "</table>";
        $a['T'] = $T;        
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $no = $_POST['code'];
        $R = $this->db->query("SELECT * FROM `r_bill_type_sum` WHERE no = '$no' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `bill_types` WHERE (code like '%$k%' OR description like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code . "</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No bill types found</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }     

    public function getMaxno(){
        return $max_no = $this->db->query("SELECT IFNULL(MAX(CONVERT(NO, SIGNED INTEGER))+1,1) AS `max_no` FROM `r_bill_type_sum` ")->row()->max_no;
    }

    public function autocomplete(){               
        $q = $_GET['term'];
        $bc = $this->sd['bc'];

        $query = $this->db->query("SELECT * FROM r_bill_type_sum WHERE billtype like '%$q%' and bc = '$bc' ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->billtype; }    
        echo json_encode($ary);
    }

    public function autocomplete_pawn_add_bill_type(){               
        $q = $_GET['term'];
        $bc = $this->sd['bc'];
        
        $query = $this->db->query("SELECT billtype FROM `r_bill_type_sum` WHERE billtype LIKE '%$q%' AND (bc = '$bc' OR bc = 'all') ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->billtype; }    
        echo json_encode($ary);
    }

    public function getBillTypeInfo(){                
        $bill_type_code = $_POST['bill_type_code'];        
        $bc = $this->sd['bc']; 
        $Q = $this->getBillDayRange($bill_type_code);
        
        if ($Q->num_rows() > 0){            
            $T = "";
            $R = $Q->row();

            $T .= "Code <br><span>" . $R->billtype . "</span><br><br>";            
            $T .= "Period <br><span>" . $R->period . "</span><br><br>"; 
            $T .= "Rate <br><span>" . $R->rate . "</span><br><br>";

            $a['rec']       = $T;
            $a['status']    = 1;
            $a['det']       = $R;
            
            $billtype =  $bill_type_code;

            $this->load->model('Calculate');
            
            $a['bill_max_no'] = $this->Calculate->next_billno($bc,$billtype);
        
        }else{            
            $a['status']    = 0;
        }
        echo json_encode($a);
    }


    public function getBillDayRange($bill_type_code,$loan_no="",$bc = ""){
        if ($bc == ""){
            $bc = $this->sd['bc'];
        }else{
            $bc = $bc;
        }
        
        if (isset($bill_type_code)){ 
            $bill_type_code = $bill_type_code; 
        }else{ 
            $bill_type_code = $_POST['bill_type_code'];
        }

        if ($loan_no == ""){
            return $this->db->query("SELECT billtype,period , day_from, day_to AS `first_int_charge_period` ,rate,allow_pre_int FROM `r_bill_type_sum` BTS JOIN `r_bill_type_det` BTD ON BTS.`no` = BTD.`no` WHERE BTS.bc = '$bc' AND BTS.billtype = '$bill_type_code' ORDER BY CAST(day_from AS UNSIGNED) ASC");
        }else{
            return $this->db->query("SELECT billtype,period , day_from, day_to AS `first_int_charge_period` ,rate,allow_pre_int FROM `t_loan_bill_int` BTS WHERE BTS.bc = '$bc' AND BTS.billtype = '$bill_type_code' AND loan_no = '$loan_no'ORDER BY CAST(day_from AS UNSIGNED) ASC");
        }
    }

    public function getBillTypeInfo_all($bt){                
        $bill_type_code = $bt;        
        $bc = $this->sd['bc'];
        $Q = $this->db->query("SELECT billtype,period , MIN(day_from), day_to AS `first_int_charge_period` ,rate FROM `r_bill_type_sum` BTS JOIN `r_bill_type_det` BTD ON BTS.`no` = BTD.`no` WHERE BTS.billtype = '$bill_type_code' AND BTS.`bc` = '$bc' LIMIT 1 ");
        if ($Q->num_rows() > 0){            
            $T = "";
            $R = $Q->row();
            $T = '<div><h3>Code</h3><span>'.$R->billtype.'</span></div> <div><h3>Period</h3><span>'.$R->period.'</span></div> <div><h3>Interest Rate</h3><span>'.$R->rate.'</span></div>';
        }else{            
            $T    = 0;
        }
        return  $T;
    }


    public function setUpdate(){
        
        $billtype = $_POST['billtype'];
        
        $Q = $this->db->query("SELECT * FROM `m_bill_type` WHERE no = '$billtype'");

        if ($Q->num_rows() > 0){
            $a['sum'] = $Q->result();           
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }

    private function used_billtype_check($billtype){

        $q = $this->db->query("SELECT billtype FROM `t_loan` WHERE billtype = '$billtype' LIMIT 1");

        if ($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    private function check_amount_range_exist($newStart,$newEnd,$bc_in_list,$pr,$billtype="",$is_old_bill){
        
        if ($billtype == ""){
            $qury = "";
        }else{
            $qury = " AND NOT billtype = '$billtype' ";
        }

        if ($is_old_bill == 1){ // Modified 2016-09-08
            $btL = "billtype = '$billtype' AND ";
        }else{
            $btL = "";
        }

        $q2 = $this->db->query("SELECT billtype FROM `r_bill_type_sum` WHERE $btL bc IN ($bc_in_list) AND `gratr_lesstn` = '$pr' AND ((amt_from BETWEEN $newStart AND $newEnd) OR (amt_to BETWEEN $newStart AND $newEnd) OR ($newStart BETWEEN amt_from AND amt_to)) $qury AND NOT is_old_bill = 1 ");
        
        if ($q2->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }


    public function load_bc_billtypes(){
        
        $bc = $_POST['bc'];

        if ($bc == ""){
            $q = $this->db->query("SELECT billtype FROM `m_bill_type` GROUP BY billtype");
        }else{        
            $q  = $this->db->query("SELECT billtype FROM `m_bill_type` BTS WHERE BTS.`bc` = '$bc' ");
        }
        
        $a['bt_det'] = $q->result();
        
        echo json_encode($a);

    }

    public function get_bt_log_no(){

        return $this->db->query("SELECT IFNULL(MAX(log_no)+1,1) AS `log_no` FROM `r_bill_type_sum_log`")->row()->log_no;

    }

}