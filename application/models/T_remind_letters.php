<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_remind_letters extends CI_Model {
    
    private $sd;
        
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){        
        $a['no'] = $this->max_no();
        $a['bt'] = $this->bc_billtype();
        return $a;
    }
    

    public function max_no(){
        return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM `t_remind_letter_sum`")->row()->max_no;
    }

    public function bc_billtype($bc = ""){

        if ($bc == ""){ $bc = $this->sd['bc']; }
        $q = $this->db->query("SELECT billtype,r1,r2,r3 FROM `m_bill_type` BT WHERE BT.`bc` = '$bc'");

        if ($q->num_rows() > 0){
            $t = "<option value=''>Select</option>";
            foreach ($q->result() as $r) {                
                $t .= "<option r1='".$r->r1."' r2='".$r->r2."' r3='".$r->r3."' value='".$r->billtype."'>".$r->billtype."</option>";
            }
        }else{
            $t = "<option value=''>Billtypes not found</option>";
        }

        return $t;
    }


    public function get_bills(){

        $fd          = $_POST['fd'];
        $td          = $_POST['td'];
        $billtype    = $_POST['billtype'];
        $letter_no   = $_POST['letter_no'];
        $days_to_cal = $_POST['days_to_cal'];
        $bc          = $this->sd['bc'];



        if ($letter_no == 1){
            $Q = "  ";
        }

        if ($letter_no == 2){
            $Q = " HAVING FIND_IN_SET('1',IFNULL(LD.letters,0)) = (".$letter_no."-1) ";
        }

        if ($letter_no == 3){
            $Q = " HAVING FIND_IN_SET('2',IFNULL(LD.letters,0)) = (".$letter_no."-1) ";
        }



        $q = $this->db->query("     SELECT LD.letters,L.`billno`, C.`cusname`, C.`nicno`, L.`ddate`, L.`finaldate`, L.`requiredamount`, CONCAT(U.loginName, ' - ', U.`discription`) AS `op`,
 
                                    @L := IFNULL(LD.letters,0),
                                    @L := IFNULL(LD.letters,0) AS `a`,
                                    IF ( FIND_IN_SET('1',@L) , 'OK','NO') AS `FL` ,
                                    IF ( FIND_IN_SET('2',@L) , 'OK','NO') AS `SL` ,
                                    IF ( FIND_IN_SET('3',@L) , 'OK','NO') AS `TL`


                                    FROM `t_loan` L 

                                    JOIN m_customer C ON L.`cus_serno` = C.`serno` 
                                    LEFT JOIN u_users U ON L.`oc` = U.`cCode` 
                                    LEFT JOIN   (   SELECT bc,billno,GROUP_CONCAT( letter_no ) AS `letters` 
                                                    FROM t_remind_letter_det 
                                                    GROUP BY billno ) LD ON L.`bc` = LD.`bc` AND L.`billno` = LD.`billno`
                                    
                                    WHERE L.bc = '$bc' AND L.`billtype` = '$billtype' AND  ADDDATE(L.`finaldate`, INTERVAL $days_to_cal DAY)  <= '$td' $Q


                                    /*AND NOT L.`billno` IN (SELECT RLD.billno FROM t_remind_letter_det RLD WHERE RLD.`bc` = '$bc' AND RLD.`letter_no` >= '$letter_no' ) */


                                    ");

        if ($q->num_rows() > 0){
            $a['s'] = 1;
            $a['d'] = $q->result();
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }



    public function load_bills(){

        $fd          = $_POST['fd'];
        $td          = $_POST['td'];
        $billtype    = $_POST['billtype'];
        $letter_no   = $_POST['letter_no'];        
        $bc          = $this->sd['bc'];

        $q = $this->db->query("SELECT 

D.`no`,
S.`date`,
D.`letter_no`,
D.`billno`, 
L.`requiredamount`,
C.`cusname`, 
C.`nicno`, 
L.`ddate`, 
L.`finaldate`

FROM `t_remind_letter_sum` S

JOIN t_remind_letter_det   D    ON S.`no`   = D.`no` AND S.`bc` = D.`bc`
JOIN t_loan            L    ON D.`billno`   = L.`billno` AND D.`bc` = L.`bc`
JOIN m_customer        C    ON L.`cus_serno` = C.`serno`

WHERE S.`bc` = '$bc' AND S.letter_no = $letter_no AND S.`billType` = '$billtype' AND (S.`fd` BETWEEN '$fd' AND '$td') AND (S.`td` BETWEEN '$fd' AND '$td')");


        if ($q->num_rows() > 0){
            $a['s'] = 1;
            $a['d'] = $q->result();
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }



    public function save(){

        $_POST['no']    = $this->max_no();
        $bills          = $_POST['billno'];
        $selected_bills = $_POST['chk_seleted_bills'];
        $letter_no      = $_POST['letter_no'];
        $_POST['bc']    = $this->sd['bc'];
        $_POST['oc']    = $this->sd['oc'];


        unset($_POST['billno'],$_POST['chk_seleted_bills'],$_POST['days_to_cal'],$_POST['hid']);

        $q = $this->db->insert("t_remind_letter_sum",$_POST);

        if ($q){

            $document_charge = $this->db->query(" SELECT `amount` 
                                    FROM `m_remind_letter_charges` 
                                    WHERE letter_no = $letter_no 
                                    LIMIT 1 ")->row()->amount;

            foreach ($selected_bills as $sb){                
                
                $det[]=array(   
                                "no"=>$_POST['no'],
                                "billno"=>$sb,
                                "letter_no"=>$letter_no,
                                "bc"=>$_POST['bc'],
                                "document_charge" => $document_charge
                            );
            }

            if (isset($det)){
                $this->db->insert_batch("t_remind_letter_det",$det);
            }

            $this->utility->user_activity_log($module='Remind Letters',$action='generate',$trans_code='',$trans_no=$_POST['no'],$note='');

            $a['s']         = 1;
            $a['no']        = $this->max_no();
            $a['no_saved']  = $_POST['no'];

        }else{
            $a['s'] = 0;
        }


        echo json_encode($a);

    }

}