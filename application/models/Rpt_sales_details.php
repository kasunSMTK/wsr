<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class rpt_sales_details extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    public function Excel_report()
    {

        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST, 'XL');
    }

    public function PDF_report($a, $rt = 'PDF')
    {

        if ($_POST['bc_arry'] === null) {
            $BC = " ";
        } else {

            for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                $bc_ar[] = "'" . str_replace(",", "", $_POST['bc_arry'][$n]) . "'";
            }

            $bc = implode(',', $bc_ar);
            $BC = " AND s.bc IN ($bc)  ";
        }

        // var_dump($bc,$BC);
        // exit();
        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];

        $q = $this->db->query("SELECT
        s.bc,
         s.`nno` AS nno,
        fo.`ddate` AS pawning_date,
        tran.`ddate` AS auction_date,
        i.`itemname` AS article,
        tran.`transeno` AS auction_code,
        i_fo.`goldweight` AS gold_weight,
        i_fo.`elec_imei`,
        i_fo.`veh_engine_no`,
        fo.`requiredamount` AS pawning_amount,
        d.`amount` AS selling_net_amount,
        s.nno AS selling_bill_no,
        s.ddate AS selling_date,
        s.`cus_serno`,
        fo.`billno`
      FROM
        t_sales_det d
        JOIN t_sales_sum s
          ON s.bc = d.bc
          AND s.nno = d.`nno`
        JOIN t_tag t
          ON t.`tag_no` = d.`tag_no`
        JOIN r_items i
          ON i.`itemcode` = t.`item`
        JOIN `t_loanitems_re_fo` i_fo
          ON i_fo.`loanno` = t.`loanno`
          AND t.`bc` = i_fo.`bc`
        JOIN t_loan_re_fo fo
          ON fo.`loanno` = t.`loanno`
          AND fo.`bc` = t.`bc`
        LEFT JOIN t_loantranse_re_fo tran
          ON tran.`loanno` = t.`loanno`
          AND tran.`bc` = t.`bc`
          AND tran.`transecode` = 'F'
      WHERE d.is_ret='0' AND s.`ddate`  BETWEEN '" . $fd . "' AND '" . $td . "' $BC
      union all
      SELECT
      s.bc,
        s.`nno` as nno,
        '' AS pawning_date,
        '' AS auction_date,
        i.`itemname` AS article,
        '' AS auction_code,
        '' AS gold_weight,
        '' AS elec_imei,
        '' AS veh_engine_no,
        0 AS pawning_amount,
        d.`amount` AS selling_net_amount,
        s.nno AS selling_bill_no,
        s.ddate AS selling_date,
        s.`cus_serno`,
        d.`billno`
      FROM
        `t_sales_det` AS d
        inner JOIN t_sales_sum s
          ON s.bc = d.bc
          AND s.nno = d.`nno`
        inner join `r_items` as i
          ON d.`item` = i.itemcode
      WHERE d.is_ret = '0'
        AND s.`ddate`  BETWEEN '" . $fd . "' AND '" . $td . "' $BC

        ORDER BY nno ");

        if (($q->num_rows() > 0)) {

            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            if ($rt == "PDF") {
                $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
            } else {
                echo "<script>alert('Excel Report Not Found');close();</script>";
            }

        } else {
            echo "<script>alert('No data found');close();</script>";
        }

    }

}
