<?php

public function cal_interest($loan_amount,$LOAN_DATA,$int_rate,$if_fm_int = false){

        if (is_array($LOAN_DATA)){
            $customer_id = $LOAN_DATA['customer_id'];
            $billno = $LOAN_DATA['billno'];
            $fmintrest = $LOAN_DATA['fmintrest'];
        }else{
            $customer_id = $LOAN_DATA->customer_id;
            $billno      = $LOAN_DATA->billno;
            $fmintrest   = $LOAN_DATA->fmintrest;
        }        
        
        $adv_balance    = $this->calculate->customer_advance($customer_id,$billno);
        $adv_balance    = $adv_balance['balance'];        

        $x = $int_rate/100;
        $x = 1 - $x;

        $l = $loan_amount;
        //$l = $loan_amount - ($fmintrest);    // Remove this line if interest calculate with first int
        //$l = $loan_amount - $adv_balance;    // Remove this line if interest calculate deduct with advance

        // $x          = $l / $x;
        // $interest   = $x - $l;
        // $interest   = ($l * $int_rate) / 100; // New calcuation changed at 2016-08-24
        
        if ($if_fm_int){
            $interest   = ($l * $int_rate) / 100 ;//  New calcuation changed at 2016-09-06
        }else{
            $l          = $loan_amount - $adv_balance;
            $interest   = ($l * $int_rate) / 100 ;//  New calcuation changed at 2016-09-06
        }

        return $this->set_amount($interest);
    }