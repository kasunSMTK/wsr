<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_mark_bills extends CI_Model {

	private $sd;
	private $mtb;
    private $max_no;

	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
	}


	public function base_details(){		
		$a['current_date'] = $this->sd['current_date'];        
        $a['mxno']='';//$this->utility->get_mark_max('F');
        $this->load->model('m_branches');
        $a['bc'] = $this->m_branches->select();

        $a['marked_bills'] = $this->marked_bills();

        return $a;
    }

    public function marked_bills(){

        $Q = $this->db->query("SELECT *, m_branches.name FROM t_loan_mark 
JOIN m_branches ON t_loan_mark.`bc` = m_branches.`bc` ORDER BY no DESC ");

        if ( $Q->num_rows() > 0 ){

            $t  = '<table class="tbl_mark_bill_list" border="1" align="center" width="100%">';
            
            $t .= '<tr>';
            $t .= '<td>No</td>';
            $t .= '<td>Type</td>';
            $t .= '<td>Date</td>';
            $t .= '<td>Branch</td>';
            $t .= '<td>Bill Number</td>';
            $t .= '<td>Date and Time</td>';
            $t .= '<td>Status</td>';
            $t .= '<td>Action</td>';
            $t .= '</tr>';

            foreach ($Q->result() as $R) {
                
                // `type`,`no`,`ddate`,`oc`,`bc`,`action_date`,`comment`,`loan_no`,`billno`,

                $t .= '<tr>';
                $t .= '<td>'.$R->no.'</td>';
                $t .= '<td>'.$R->type.'</td>';
                $t .= '<td>'.$R->ddate.'</td>';
                $t .= '<td>'.$R->bc.' - '.$R->name.' </td>';
                $t .= '<td>'.$R->billno.'</td>';
                $t .= '<td>'.$R->action_date.'</td>';
                
                if ($R->is_delete == 1){
                    $st = "<span style='color:red'>Canceled</span>";
                    $link = '';
                }else{
                    $st = "<span style='color:Green'>Marked</span>";
                    $link = '<a class="mark_bill_stat_change" no='.$R->no.' type='.$R->type.' loanno = '.$R->loan_no.' >Unmark this bill</a>';
                }

                $t .= '<td>'.$st.'</td>';


                $t .= '<td>'.$link.'</td>';
                $t .= '</tr>';
            }


            return $t;

        }else{

            return "No marked bills found";

        }

    } 

    public function grid_data(){          
    	
        $bn = $_POST['bn'];

    	$Q = $this->db->query("

            SELECT L.`bc`, L.`loanno`, L.`billno`, L.`ddate`, L.`finaldate`, L.`period`, L.`totalweight`, L.`goldvalue`, L.`billtype`, C.`title`, C.`cusname`, L.`requiredamount`
            FROM `t_loan` L 
            INNER JOIN `m_customer` C ON (L.`cus_serno` = C.`serno`) 
            WHERE L.`status` = 'P' AND L.`billno` = '$bn'
            ORDER BY L.`billtype`,L.`ddate` 
            LIMIT 1

        ");

        
    	if ($Q->num_rows() > 0){


            $R = $Q->row();

            $gd ="<br>";
            
            $gd.="<div class='div_bn_mark_view_h' style='text-align:right; margin-right:10px'>";
            $gd.="<span>billtype</span> <br>".$R->billtype."<br><br>";
            $gd.="<span>customer</span> <br>".$R->title.$R->cusname."<br><br>";            
            $gd.="<span>bill number</span> <br>".$R->billno."<br><br>";
            $gd.="<span>date</span> <br>".$R->ddate."<br><br>";
            $gd.="</div>";
            
            $gd.="<div class='div_bn_mark_view_h' style='text-align:left; margin-left:10px'>";
            $gd.="<span>final date</span> <br>".$R->finaldate."<br><br>";
            $gd.="<span>period</span> <br>".$R->period."<br><br>";
            $gd.="<span>total weight</span> <br>".$R->totalweight."<br><br>";
            $gd.="<span>gold weight</span> <br>".$R->goldvalue."<br><br>";
            $gd.="</div>";

            $gd.='<br><br><br><br>
            <div style="border:0px solid red; text-align:center">
              <div class="pAndL_hold" style="width:auto; display:inline-block">
                <label for="mb_lost" style="width:100px; display:inline-block">
                <div class="opt_holder" style="margin:2px;">
                  <input class="" type="radio" id="mb_lost"  name="mb_l_P" value="L" />
                  Lost</div>
                </label>
                <label for="mb_police" style="width:100px; display:inline-block">
                <div class="opt_holder" style="margin:2px;">
                  <input class="" type="radio" id="mb_police"  name="mb_l_P" value="PO" />
                  Police</div>
                </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input name="btnMark" type="button" value="Mark" id="btnMark" class="btn_regular"/>
              <input name="btnReset" type="button" value="Reset" id="btnReset" class="btn_regular"/>
              </div>
              </div>
            ';
                

            $a['s']       = 1;
    		$a['data']    = $gd;

    	}else{
    		$a['s']       = 0;
            $a['data']    = "No data found";
    	}

    	echo json_encode($a);

    }

    

    public function load_old_data(){}


    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", $_POST['trans_code'] );        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',$_POST['trans_code']);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => date("H:i:s"),
            "trans_code" => $_POST['trans_code'],
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        $des = "Mark pawning - (".$_POST['mark_status'].") - ". $this->max_no;
        $this->load->model('account');
        $this->account->set_data($config);

        $pawn_stock               = $this->utility->get_default_acc('UNREDEEM_ARTICLES');
        $POLICE_AND_ILLEGAL_ITEMS   = $this->utility->get_default_acc('POLICE_AND_ILLEGAL_ITEMS');        
        
        $this->account->set_value2($des, $_POST['loan_amount'], "cr", $pawn_stock,$condition,"",$_POST['ln'],"",$_POST['bn'],"","",$_POST['bccx']);
        $this->account->set_value2($des, $_POST['loan_amount'] , "dr", $POLICE_AND_ILLEGAL_ITEMS,$condition,"",$_POST['ln'],"",$_POST['bn'],"","", $_POST['bccx']);
        
        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='".$_POST['trans_code']."'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", $_POST['trans_code']);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }


    }

    public function max_no(){
        return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM t_loan_mark")->row()->max_no;
    }

    public function save(){

        $a['s'] = 1;

        echo json_encode($a);

        exit;
        
        $data=$lnNoRm='';
        $lnNo = array();
        $lnNoRm=$_POST['act'];
        $mb_l_P=$_POST['mb_l_P'];
        $no = $this->max_no = $this->utility->get_mark_max($mb_l_P);        
        $ddate=$this->sd['current_date'];
        $oc=$this->sd['oc'];
        $bc=$this->sd['bc'];
        $comment=(empty($_POST['comment']))?"":$_POST['comment'];
        $n = 0;

        $_POST['hid'] = 0;
        $_POST['date'] = $_POST['dDate'];
        
        foreach ($_POST['act'] as $value) {
            
            $lnNo[] = $value;
            
            $dataUp[]= array(
                'loanno' => $value,
                'status' => $mb_l_P,
                'action_date'=>date('Y-m-d h:i:s')
            );
            
            $dataIns[]= array(
                'type'=>$mb_l_P,
                'no' => $no,
                'ddate' => $ddate,
                'oc' => $oc,
                'bc' => $_POST['bcc'][$n],
                'action_date'=>date('Y-m-d h:i:s'),
                'comment' => $comment,
                'loan_no' => $value
            );


           
            $n++;

        
        }




        $this->load->model("calculate");
        $aa = $this->calculate->move_loan_data_to_RE_FO( $lnNo , $mb_l_P );        
        $this->db->insert_batch('t_loan_mark', $dataIns);
      
        echo $aa; 
    }

    

    public function mark_bill(){

        $billno = $_POST['billno'];
        $type   = $_POST['type'];
        $r      = $this->db->where('billno',$billno)->limit(1)->get('t_loan')->row();

        $_POST['hid'] = "";
        $_POST['type'] = $type;
        $_POST['no']   = $this->max_no = $this->max_no();
        $_POST['ddate'] = $_POST['date'] = $this->sd['current_date'];
        $_POST['oc'] = $this->sd['oc'];
        $_POST['bc'] = $r->bc;
        $_POST['loan_no'] = $r->loanno;

        $_POST['loan_amount'] = $r->requiredamount;
        $_POST['mark_status'] = $type;
        $_POST['ln'] = $_POST['loan_no'];
        $_POST['bccx'] = $_POST['bc'];
        $_POST['bn']   = $_POST['billno'];

        if ($type == "L"){ $_POST['trans_code'] = $tc = 6; }
        if ($type == "PO"){ $_POST['trans_code'] = $tc = 7; }

        $account_update =   $this->account_update(0);                
        
        if($account_update!=1){
            $a['s'] = 0;
            $a['m'] = "Invalid account entries";
            $this->db->trans_rollback();
            $a['s'] = "error";
            echo json_encode($a);
            exit;
        }else{
            $account_update =   $this->account_update(1);
        }

        $this->load->model("calculate");
        $aa = $this->calculate->move_loan_data_to_RE_FO( $r->loanno , $type );

        unset($_POST['hid'],$_POST['date'],$_POST['loan_amount'],$_POST['mark_status'],$_POST['ln'],$_POST['bccx'],$_POST['trans_code'],$_POST['bn']);

        $q = $this->db->insert('t_loan_mark',$_POST);    

        if ($q){
            $this->utility->user_activity_log($module='Mark Bill',$action='insert',$trans_code=$tc,$trans_no=$this->max_no,$note='');
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);


    }



    public function change_mark_status(){

        $this->db->trans_begin();

            $no         = $_POST['no'];
            $type       = $_POST['type'];
            $loanno     = $_POST['loanno'];

            $Q = $this->db->query("     UPDATE `t_loan_mark`
                                        SET is_delete = 1
                                        WHERE `no` = $no AND `type` = '$type' AND loan_no = '$loanno'
                                        LIMIT 1 ");


            $Q1 = $this->db->query("INSERT INTO t_loan       SELECT * FROM t_loan_re_fo L   WHERE L.`loanno` = $loanno LIMIT 1 ");
            $Q2 = $this->db->query("INSERT INTO t_loanitems  SELECT * FROM t_loanitems_re_fo L WHERE L.`loanno` = $loanno ");
            $Q3 = $this->db->query("INSERT INTO t_loantranse SELECT * FROM t_loantranse_re_fo L WHERE L.`loanno` = $loanno ");

            if ($Q1 && $Q2 && $Q3){
                $Q1 = $this->db->query("DELETE FROM t_loan_re_fo           WHERE `loanno` = $loanno LIMIT 1 ");
                $Q2 = $this->db->query("DELETE FROM t_loanitems_re_fo      WHERE `loanno` = $loanno ");
                $Q3 = $this->db->query("DELETE FROM t_loantranse_re_fo     WHERE `loanno` = $loanno ");
            }

            $this->db->query("UPDATE t_loan SET `status` = 'P' WHERE loanno = '$loanno' LIMIT 1 ");

            if ( $Q ){
                $this->account_update_cancel();
            }else{
                $a['s'] = 0;
                $this->db->trans_rollback();
            }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = 0;
        }else{            
            $this->db->trans_commit();

            if ($type == 'PO'){ $tc = 7; }
            if ($type == 'L'){ $tc = 6; }

            $this->utility->user_activity_log($module='Mark Bill',$action='cancel',$trans_code=$tc,$trans_no=$no,$note='');
            $a['s'] = 1;            
        }

        echo json_encode($a);

    }


    public function account_update_cancel(){

        $no         = $_POST['no'];
        $type       = $_POST['type'];
        $loanno     = $_POST['ln'] = $_POST['loanno'];


        if ($type == "PO"){$tc = 7; }
        if ($type == "L"){$tc = 6; }        

        $QQQ = $this->db->query("SELECT * FROM t_account_trans WHERE trans_code = '".$tc."' AND trans_no = ".$no." AND loanno = '".$loanno."' LIMIT 2 ");

        $bcc = $_POST['bcc'] = $QQQ->row()->bc;
        $_POST['bn'] = $QQQ->row()->billno;
        $_POST['bt'] = $QQQ->row()->billtype;

        $this->db->where("trans_no", $no);
        $this->db->where("trans_code", $tc);        
        $this->db->where("bc", $bcc);
        $this->db->delete("t_check_double_entry");


        $config = array(
            "ddate" => $_POST['date'],
            "time" => date("H:i:s"),
            "trans_code" => $tc,
            "trans_no" => $no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        $this->load->model('account');
        $this->account->set_data($config);

        $condition = 1;
        
        foreach ($QQQ->result() as $R){

            if ($R->dr_amount > 0){
                $this->account->set_value2("Canceled ".$R->description, $R->dr_amount , "cr", $R->acc_code ,$condition,"",           $_POST['ln'],$_POST['bt'],$_POST['bn'],"C","",$_POST['bcc']);
            }

            if ($R->cr_amount > 0){
                $this->account->set_value2("Canceled ".$R->description, $R->cr_amount , "dr", $R->acc_code ,$condition,"",           $_POST['ln'],$_POST['bt'],$_POST['bn'],"C","",$_POST['bcc']);
            }
        
        }

        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $bcc . "'  AND t.`trans_code`='".$tc."'  AND t.`trans_no` ='" . $no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $no);
                $this->db->where("trans_code", $tc);                
                $this->db->where("bc",$bcc);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }




    }





}
