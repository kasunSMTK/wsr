<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_forfeit extends CI_Model {

    private $sd;
    private $mtb;
    private $max_no;

    function __construct(){
        parent::__construct();		
        $this->sd = $this->session->all_userdata();
        $this->mtb = 'tblusers';        
    }


    public function base_details(){		
        $a['max_no'] = $this->max_no();
        $a['current_date'] = $this->sd['current_date'];
        //$a['grid_data']=$this->grid_data();
        $this->load->model('m_branches');
        $a['bc_dropdown'] = $this->m_branches->select_own_bc();
        $a['billtype_dropdown'] = $this->billtype_dropdown();
        return $a;
    }

    public function billtype_dropdown(){
        $T = "<select id='r_billtype' name='r_billtype'  class='input_ui_dropdown input_text_regular_new_pawn' style='border:2px solid Green; padding:3px'>";
        $T .= "<option value=''>All</option>";
        $T .= "</select>";

        return $T;
    }

    public function grid_data(){

        $bc = $_POST['bc'];
        $fd = $_POST['from_date'];
        $td = $_POST['to_date'];       
        $bt = $_POST['r_billtype'];

        if ($bc == ""){
            $QBC = "";            
        }else{
            $QBC = "L.`bc`='$bc' AND ";            
        }

        if ($bt == ""){
            $QBT = "";            
        }else{            
            $QBT = "L.`billtype`='$bt' AND ";
        }        

        $Q = $this->db->query(" SELECT L.`loanno`, L.`billno`, L.`ddate`, L.`finaldate`, L.`period`, L.`totalweight`, L.`goldvalue`, L.`billtype`, C.`title`,C.`cusname`,L.`cus_serno`,L.`requiredamount`,IFNULL(SUM(AC.`cr_amount`),0) AS `adv_tot` 
                                FROM `t_loan` L 
                                INNER JOIN `m_customer` C ON (L.`cus_serno` = C.`serno`) 
                                LEFT JOIN `t_loan_advance_customer` AC ON L.`bc` = AC.`bc` AND L.`billno` = AC.`billno` AND AC.`is_delete` = 0
                                WHERE $QBC $QBT L.`status` = 'P' 

                                AND DATE_ADD(L.`finaldate`, INTERVAL 0 DAY) <= '$td' 
                                
                                GROUP BY L.`billno`

                                ORDER BY L.`billtype`,L.`ddate`,L.`billno`  ");


        
        
        $gd         =   "";
        $lstCode    =   "";
        
        if ($Q->num_rows() > 0){            
            
            foreach($Q->result() as $R){
                
                if ($lstCode!=$R->billtype) {                
                    
                    $gd.="<tr class='SlalMn bt_".$R->billtype."' >";

                    $gd.="<td width='74' class='text_box_holder_new_pawn bd'>".$R->billtype."</td>";
                    $gd.="<td width='275' class='text_box_holder_new_pawn bd'>&nbsp;</td>";                    
                    $gd.="<td width='110' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='80' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='85' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='60' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='75' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='90' class='text_box_holder_new_pawn bd'>&nbsp;</td>";                    

                    $gd.="<td width='90' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='115' class='text_box_holder_new_pawn bd'>&nbsp;</td>";

                    $gd.="<td width='80' class='text_box_holder_new_pawn bd' align='center'>


                    <input type='checkbox' class='Sl' onclick='SlalMn($(this))' value='".$R->billtype."' /></td>";
                    $gd.="</tr>";  

                    $gd.="<tr id='ln_".$R->loanno."' Bilt='".$R->billtype."'>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>&nbsp;</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='left'>".$R->title.$R->cusname."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->billno."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->ddate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->finaldate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->period."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->totalweight."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".number_format($R->goldvalue,2)."</td>";

                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".number_format($R->adv_tot,2)."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".number_format($R->requiredamount,2)."</td>";


                    
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>
                        <input name='act[]' type='checkbox' class='Sl slSub sel_".$R->billtype."' value='".$R->loanno."' />
                        <input type='hidden' value='".$R->cus_serno."' name='cus_serno[]'>
                        <input type='hidden' value='".$R->requiredamount."' name='loan_amount[]'>
                        <input type='hidden' value='".$R->billtype."' name=billtype_ar[]>
                        <input type='hidden' value='".$R->billno."' name=billno_ar[]>

                        </td>";
                    $gd.="</tr>";
                
                }else{

                    $gd.="<tr id='ln_".$R->loanno."' Bilt='".$R->billtype."'>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>&nbsp;</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='left'>".$R->title.$R->cusname."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->billno."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->ddate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->finaldate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->period."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->totalweight."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".number_format($R->goldvalue,2)."</td>";

                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".number_format($R->adv_tot,2)."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".number_format($R->requiredamount,2)."</td>";
                    
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>
                        <input name='act[]' type='checkbox' class='Sl slSub sel_".$R->billtype."' value='".$R->loanno."' />
                        <input type='hidden' value='".$R->cus_serno."' name='cus_serno[]'>
                        <input type='hidden' value='".$R->requiredamount."' name='loan_amount[]'>
                        <input type='hidden' value='".$R->billtype."' name=billtype_ar[]>
                        <input type='hidden' value='".$R->billno."' name=billno_ar[]>

                        </td>";
                    $gd.="</tr>";
                 
                }
                $lstCode=$R->billtype;   

            }
        }else{
            $gd= "<tr><td colspan='11' class='text_box_holder_new_pawn bd' align='center'><h1>No data found</h1></td></tr>";
        }
    
        $a['rows'] = $gd;
    
        echo json_encode($a);



    }

    














    public function save(){

        $bc = $_POST['bc']; // change this section to dropdwn branches

        $data=$lnNo=$lnNoRm='';
        $lnNoRm=$_POST['act'];
        $no = 0;
        $cus_serno = array();
        $loan_amount = array();

        $trans_batch_loans = array();
        $foreit_det = array();
        $this->max_no = $this->max_no();
        $foreit_sum = array("no"   => $this->max_no, "date" => $_POST['ddate'], "bc"   => $bc, "oc"   => $this->sd['oc'] );
        
        foreach ($_POST['act'] as $value) {
            
            if ($lnNo!=""){
                $lnNo.=",";
            }

            $lnNo.="'".$value."'";
            
            $cus_serno[$no]     = $_POST['cus_serno'][$no];
            $loan_amount[$no]   = $_POST['loan_amount'][$no];
            
            $bt                 = $_POST['billtype_ar'][$no];
            $bn                 = $_POST['billno_ar'][$no];
            $la                 = $_POST['loan_amount'][$no];

            $ln[$no]            = $value;
            $billtype[$no]      = $bt;
            $billno[$no]        = $bn;

            $trans_batch_loans[] = array(
                'loanno'    => $value,
                'bc'        => $bc,
                'billtype'  => $bt,
                'billno'    => $bn,
                'ddate'     => $_POST['ddate'],
                'amount'    => $la, 
                'transecode'=> 'F',
                'transeno'  => $this->max_no,
                'discount'  => 0
            );

            $foreit_det[] = array(
                "no" => $this->max_no, 
                "billtype" => $bt,
                "billno" => $bn,
                "loanno" => $value
            );

            $no++;
        }

        $this->db->trans_begin();
        // $this->db->trans_start(TRUE); // Query will be rolled back

            $this->db->query("INSERT INTO `t_loantranse_re_fo` SELECT * FROM `t_loantranse` WHERE `loanno` IN (".$lnNo.")");
            $this->db->query("INSERT INTO `t_loanitems_re_fo` SELECT * FROM `t_loanitems` WHERE `loanno` IN (".$lnNo.")");
            $this->db->query("INSERT INTO `t_loan_re_fo` SELECT * FROM `t_loan` WHERE `loanno` IN (".$lnNo.")");

        if ($this->db->trans_status() === FALSE){
            $a['s'] =0;
            $this->db->trans_rollback();
        }else{
            $this->db->query("DELETE FROM `t_loantranse` WHERE `loanno` IN (".$lnNo.")");
            $this->db->query("DELETE FROM `t_loanitems` WHERE `loanno` IN (".$lnNo.")");
            $this->db->query("DELETE FROM `t_loan` WHERE `loanno` IN (".$lnNo.")");

            if ($this->db->trans_status() === FALSE){
                $a['s'] =0;
                $this->db->trans_rollback();
            }else{
                //$this->db->update_batch('t_loan_re_fo', $data,'loanno');

                $this->db->insert_batch('t_loantranse_re_fo',$trans_batch_loans);
                $this->db->insert('t_forfeit_sum',$foreit_sum);
                $this->db->insert_batch('t_forfeit_det',$foreit_det);

                for ($nno = 0 ; $nno < count($loan_amount) ; $nno++){


                    $_POST['loan_amount'] = $loan_amount[$nno];
                    $_POST['hid']  = 0;
                    $_POST['date'] = $_POST['ddate'];
                    
                    $_POST['bn']   = $billno[$nno];
                    $_POST['ln']   = $ln[$nno];
                    $_POST['bt']   = $billtype[$nno];

                    $account_update =   $this->account_update(1);                
                    
                    /*if($account_update!=1){
                        $a['s'] = 0;
                        $a['m'] = "Invalid account entries";
                        $this->db->trans_rollback();
                        $a['s'] = "error";
                        echo json_encode($a);
                        exit;
                    }else{
                        $account_update = $this->account_update(1);
                    }*/

                    $this->utility->user_activity_log($module='Forfeited',$action='insert',$trans_code='5',$trans_no=$this->max_no,$note='');

                }

            }

            $this->load->model('calculate');            
            foreach ($cus_serno as $value) {$this->calculate->update_customer_pawn_details($value,'F'); }            

            $this->db->trans_commit();
            $a['s'] =1;        
        }

        $a['n'] = $this->db->conn_id->errno; 
        $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno);
        $a['lnNoRm'] = $lnNoRm;
        $a['max_no'] = $this->max_no();
    
        echo json_encode($a);
    }


    public function load(){
        $no = $_POST['no'];    
        $Q = $this->load_saved_grid_data($no);
        
        if ($Q != "0"){
            $a['f_data'] = $Q;
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }


    public function max_no(){        
        return $this->db->query("SELECT IFNULL(MAX(NO)+1,1) AS `max_no` FROM `t_forfeit_sum`")->first_row()->max_no;
    }


    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no); 
        $this->db->where("trans_code", 5);        
        $this->db->where("bc", $_POST['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$_POST['bc']);
            $this->db->where('trans_code',5);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => date('Y-m-d H:i:s'),
            "trans_code" => 5,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);

        $FORFEITED_STOCK    = $this->utility->get_default_acc('FORFEITED_STOCK');
        $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');
                
        $this->account->set_value2("Forfeit", $_POST['loan_amount'] , "dr", $FORFEITED_STOCK,   $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'], "F","",$_POST['bc']);
        $this->account->set_value2("Forfeit", $_POST['loan_amount'] , "cr", $pawn_stock,        $condition,"",  $_POST['ln'],$_POST['bt'],$_POST['bn'], "F","",$_POST['bc']);
        
        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $_POST['bc'] . "'  AND t.`trans_code`='5'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 5);                
                $this->db->where("bc", $_POST['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }








    public function load_saved_grid_data($no){

        $Q       = $this->db->query("SELECT L.`loanno`, L.`billno`, L.`ddate`, L.`finaldate`, L.`period`, L.`totalweight`, L.`goldvalue`, L.`billtype`, C.`title`,C.`cusname`,L.`cus_serno`,L.`requiredamount` FROM `t_forfeit_det` FD LEFT JOIN `t_loan_re_fo` L ON FD.`loanno` = L.`loanno` JOIN `m_customer` C ON L.`cus_serno` = C.`serno` WHERE FD.no = '$no' ");
        $gd      = "";
        $lstCode = "";
        
        if ($Q->num_rows() > 0){            
            
            foreach($Q->result() as $R){
                
                if ($lstCode!=$R->billtype) {                
                    $gd.="<tr class='SlalMn bt_".$R->billtype."' >";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>".$R->billtype."</td>";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    //$gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='60' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='125' class='text_box_holder_new_pawn bd'>&nbsp;</td>";
                    $gd.="<td width='80' class='text_box_holder_new_pawn bd' align='center'></td>";
                    $gd.="</tr>";  

                    $gd.="<tr id='ln_".$R->loanno."' Bilt='".$R->billtype."'>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>&nbsp;</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='left'>".$R->title.$R->cusname."</td>";
                    //$gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->loanno."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->billno."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->ddate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->finaldate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->period."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->totalweight."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->goldvalue."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right' style='color:red'>Forfeited Value</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>

                        
                        <input type='hidden' value='".$R->cus_serno."'>
                        <input type='hidden' value='".$R->requiredamount."' >
                        <input type='hidden' value='".$R->billtype."' >
                        <input type='hidden' value='".$R->billno."' >

                        </td>";
                    $gd.="</tr>";
                }else{
                    $gd.="<tr id='ln_".$R->loanno."' Bilt='".$R->billtype."'>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>&nbsp;</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='left'>".$R->title.$R->cusname."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->loanno."</td>";
                    $gd.="<td widows: idth='' class='text_box_holder_new_pawn bd' align='center'>".$R->billno."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->ddate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->finaldate."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>".$R->period."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->totalweight."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right'>".$R->goldvalue."</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='right' style='color:red'>Forfeited Value</td>";
                    $gd.="<td width='' class='text_box_holder_new_pawn bd' align='center'>

                        
                        <input type='hidden' value='".$R->cus_serno."' >
                        <input type='hidden' value='".$R->requiredamount."' >
                        <input type='hidden' value='".$R->billtype."' >
                        <input type='hidden' value='".$R->billno."' >

                        </td>";
                    $gd.="</tr>";
                 
                }
                $lstCode=$R->billtype;   

            }
        
            return $gd;

        }else{
            return 0;
        }
        
        
    }





}
