<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class index extends CI_Model {
    
    private $sd;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
   
    }
    
    public function base_details(){		
        $this->load->model('t_message');        
        $a['msg'] =  $this->t_message->new_messages();
		return $a;
    }
}