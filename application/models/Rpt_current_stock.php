<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_current_stock extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function PDF_report($_POST_){

		$bc = $_POST_['bc'];
    
	    if ($bc == ""){
	        $bc = "";
	    }else{
	        $bc1 = "L.bc = '$bc' AND ";
	        $bc2 = "LT.bc = '$bc' AND ";
	    }

		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];

		$Q  = $this->db->query("SELECT COUNT(LI.`itemcode`) AS `pcs`, SUM(LI.`qty`) AS `qty`, LI.`itemcode`, I.`itemname`, SUM(LI.`pure_weight`) AS `p_weight` FROM `t_loan` L JOIN `t_loanitems` LI ON L.`bc` = LI.`bc` AND L.`loanno` = LI.`loanno` JOIN r_items I ON LI.`itemcode` = I.`itemcode` WHERE $bc1 L.`status` = 'P'GROUP BY LI.`itemcode` ORDER BY I.`itemname`");
		
		$Q2 = $this->db->query("SELECT COUNT(LI.`itemcode`) AS `pcs`, SUM(LI.`qty`) AS `qty`, LI.`itemcode`, I.`itemname`, SUM(LI.`pure_weight`) AS `p_weight` 

FROM `t_loantranse_re_fo` LT 
JOIN `t_loanitems_re_fo` LI ON LT.`bc` = LI.`bc` AND LT.`loanno` = LI.`loanno`
JOIN `r_items` I ON LI.`itemcode` = I.`itemcode`

WHERE $bc2 LT.`transecode` = 'F'

GROUP BY LI.`itemcode`
ORDER BY I.`itemname`");

		if($Q->num_rows() > 0 || $Q2->num_rows() > 0){

			$r_data['list'] = $Q->result();
			$r_data['list_ftd'] = $Q2->result();
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;

			$this->load->view($_POST['by'].'_'.'pdf',$r_data);
		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}