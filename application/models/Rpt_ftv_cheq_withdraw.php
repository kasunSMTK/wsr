<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_ftv_cheq_withdraw  extends CI_Model {

  private $sd;    

  function __construct(){
    parent::__construct();        
    $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
  }   

  public function PDF_report($_POST_){


     $bc=$this->sd['bc'];
   
  
    $Q = $this->db->query("SELECT
                              f.fund_tr_opt_no,
                              f.ref_no,
                              f.date_time,
                              f.requ_initi_bc,
                              bb.name AS ini_branch,
                              f.transfer_type,
                              f.amount,
                              f.comment
                            FROM t_fund_transfer_sum f 
                            LEFT JOIN m_branches bb ON bb.bc = f.requ_initi_bc 
                            WHERE f.from_bc='$bc' AND  f.transfer_type='cheque_withdraw' AND f.fund_tr_opt_no='".$_POST['inv_no']."' LIMIT 1");

    
    if($Q->num_rows() > 0){

      $r_data['list'] = $Q->result();
    

      $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
      echo "<script>location='default_pdf_error'</script>";
    }

  }

}