<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_gold_quality extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){		

        $this->load->model("m_branches");
        $a['bc_dropdown'] = $this->m_branches->select();
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){

        $_POST['code'] = strtoupper($_POST['code']);

        if ($_POST['hid'] == "0"){
            unset($_POST['hid']);
        	if($this->db->insert("r_gold_quality",$_POST)){
                $a['s'] = 1;
            }else {
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Gold Quality");
            }
        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $this->db->where("code",$hid)->update("r_gold_quality",$_POST);
            $a['s'] = 1;            
        }

        echo json_encode($a);
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $code = $_POST['code'];

       if ($this->db->query("DELETE FROM`r_gold_quality` WHERE code = '$code' ")){
             $a['s'] = 1;

        }else {

            $a['s'] = 0; 
            $a['n'] = $this->db->conn_id->errno; 
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Gold Quality");
        }
        echo json_encode($a);
    }

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `r_gold_quality` ORDER BY `code` DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code . " </td><td class='D'>-</td><td class='Des'> " .$R->rate. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
         $T .= "<table>";   
        return $T;
    }
    
    public function set_edit(){
        $code = $_POST['code'];
        $R = $this->db->query("SELECT * FROM `r_gold_quality` WHERE code = '$code' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `r_gold_quality` WHERE (code like '%$k%' OR rate like '%$k%') LIMIT 10 ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code . " </td><td class='D'>-</td><td class='Des'> " .$R->rate. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
         $T .= "<table>";   
    
        echo $T;
            
    }

}