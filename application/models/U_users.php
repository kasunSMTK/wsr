<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class U_users extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
 

    public function base_details(){		
        $this->load->model("m_branches");
        $a['bc_dropdown'] = $this->m_branches->select();
        $a['nxtCode'] = $this->get_next_code();

        $this->load->model('m_area');
        $a['area'] = $this->m_area->getArea('user_area_dropdown','user_area_dropdown','');

		return $a;
    }

    public function save(){

        if (isset($_POST['user_zone'])){
            $_POST['area'] = implode(",",$_POST['user_zone']);
        }else{
            $_POST['area'] = "";
        }

        unset($_POST['user_area_dropdown'],$_POST['user_zone']);

        if (isset($_POST['isHO'])){
            $_POST['isHO'] = 1;
        }else{
            $_POST['isHO'] = 0;
        }

        if (isset($_POST['is_HO_cashier'])){
            $_POST['is_HO_cashier'] = 1;
        }else{
            $_POST['is_HO_cashier'] = 0;
        }

        if (isset($_POST['bc_manager'])){
            $_POST['bc_manager'] = 1;
        }else{
            $_POST['bc_manager'] = 0;
        }

        if (isset($_POST['multi_bc_login'])){
            $_POST['multi_bc_login'] = 1;
        }else{
            $_POST['multi_bc_login'] = 0;
        }


        if (isset($_POST['disable_login'])){
            $_POST['disable_login'] = 1;
        }else{
            $_POST['disable_login'] = 0;
        }


        if ($_POST['hid'] == 0){
            unset($_POST['hid']);
            if (isset($_POST['userPassword'])) {
            	$_POST['userPassword']=md5($_POST['userPassword']); 
            }
            $_POST['cCode']=$this->get_next_code();
            $_POST['cl']=$this->sd["cl"];
            $_POST['crBy']=$this->sd["oc"];                
            if ($this->db->insert("u_users",$_POST)) {                
                 $a['s'] = 1;
                }
            else {
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno);
            }

        }else{

            $hid = $_POST['hid'];

            if(isset($_POST['hid']))unset($_POST['hid']);
            if(isset($_POST['bc']))unset($_POST['bc']);
            if(isset($_POST['isAdmin']))unset($_POST['isAdmin']);
            if(isset($_POST['loginName']))unset($_POST['loginName']);

            if (isset($_POST['userPassword'])) {
            	$_POST['userPassword']=md5($_POST['userPassword']); 
            }
            $this->db->where("cCode",$hid)->update("u_users",$_POST);
            $a['s'] = 1;  
        }
    
        echo json_encode($a);
    }

    public function load_list($q){


        if ($q == ""){
            $qq = '';
        }else{
            $qq = "WHERE loginName LIKE '%$q%' OR discription LIKE '%$q%'";
        }

        
        $Q = $this->db->query(" SELECT U.*, B.`name` AS `bc_name` FROM `u_users` U 
                        JOIN m_branches B ON U.`bc` = B.`bc` 
                        WHERE ccode NOT IN
                        (SELECT IF(ccode='".$this->sd['oc']."','',ccode)  FROM u_users WHERE loginName ='Developer') 
                        $qq   
                        ORDER BY U.bc, U.`cCode` DESC ");
        
        $t = ''; $b = '';

        if ( $Q->num_rows() > 0 ){


                foreach ($Q->result() as $R) {                    
                    
                    if ($R->bc != $b){                    
                        $t .= '<br><div><div class="user_list_row_group">'.$R->bc_name.'</div><br>';
                        $b = $R->bc;
                    }

                    $stylex = '';

                    if ($R->disable_login == 1){
                        $stylex = 'style="color:red;text-decoration:underline"';
                    }


                    $t .= "<div class='user_list_row'>

                        <span><b>".strtoupper($R->loginName)."</b></span>
                        <span class='user_list_display_name' $stylex> ".strtoupper($R->display_name)." </span>


                        <div class='user_list_link'>
                            <a href='#' onClick=setEdit('".$R->cCode."')>Edit</a> | 
                            <a onClick=setDelete('".$R->cCode."')>Delete</a>
                        </div>


                    </div>";                    
                    
                }

                $t .= '</div>';

            

        }else{
            $t .= '<br>No data found <div style="float:right"><a class="reset_search">Reset Search</siv></a>';
        }

        

        $a['t'] = $t;
        $a['max_no'] = $this->get_next_code();;

        echo json_encode($a);

    }


    public function load(){
	   echo $this->load_list($_POST['q']);  
    }

    public function get_next_code(){
        $sql="SELECT LPAD((SELECT MAX(CAST(cCode AS UNSIGNED))+1 FROM `u_users`),3,0) AS v";
        $code=$this->db->query($sql)->first_row()->v;
        return $code; 

    }

    public function set_edit(){
        $cCode = $_POST['cCode'];
        $R = $this->db->query("SELECT `disable_login`,`bc`,`cCode`,`loginName`,`discription`,`isAdmin`,`isHO`,`area`,`is_HO_cashier`,`bc_manager`,`multi_bc_login` FROM `u_users` WHERE cCode = '$cCode' LIMIT 1")->row();
        echo json_encode($R);
    }

    public function delete(){
	   $cCode = $_POST['cCode'];

       // if ($this->db->query("DELETE FROM `u_users` WHERE cCode = '$cCode' ")){
       if ($this->db->delete('u_users', array('cCode' => $cCode))){       	
            $a['s'] = 1;
       }else{
	        $a['s'] = 0; 
	        $a['n'] = $this->db->conn_id->errno; 
	        $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Unable to delete this user!");
       }

        echo json_encode($a);
    }

}