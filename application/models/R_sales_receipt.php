<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class r_sales_receipt extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    public function PDF_report($loanno, $fullmonth_int = 0)
    {
        /*var_dump($_POST);
        exit;*/
        $bc = $this->sd['bc'];

        $lno = 0;
        if (isset($_POST['r_no'])) {
            $lno = $_POST['r_no'];
        }

//$_POST['r_no'] = 101123624;

        $data = $this->db->query(" SELECT d.nno,
                                    CONCAT(
                                    d.tag_no,
                                    ' - ',
                                    IFNULL(r.itemname, '')
                                    ) description,
                                    d.amount,
                                    rd.amount AS payment 
                                
                                FROM
                                    `t_sales_det` AS `d` 
                                    JOIN `t_sales_receipt_det` rd 
                                    ON rd.`sales_id` = d.`nno` 
                                    LEFT JOIN `t_tag` `t` 
                                    ON `t`.`tag_no` = `d`.`tag_no` 
                                    LEFT JOIN `r_itemcategory` `c` 
                                    ON `c`.`code` = `t`.`cat_code` 
                                    LEFT JOIN `r_items` r 
                                    ON r.`itemcode` = d.`item` 
                                    LEFT JOIN `r_itemcategory` `cc` 
                                    ON `cc`.`code` = r.`itemcate` 
                                WHERE `rd`.`nno` = " . $lno . " 
                                    AND `rd`.`bc` = '" . $bc . "' ");

        $data1 = $this->db->query("
                                                    SELECT c.nicno,rs.date ,rs.cash_amount,
                                                    CONCAT(c.title,' . ',c.cusname) AS cusname, c.`address` AS `address`, rs.nno

                                                    FROM t_sales_receipt_sum rs
                                                    INNER JOIN m_customer c ON c.`serno` = rs.`cus_serial`

                                                    WHERE rs.nno= " . $lno . " AND rs.bc='" . $bc . "' ");

        if ($data->num_rows() > 0) {
            $r_detail['data'] = $data->result();
            $r_detail['nno'] = $lno;
            $r_detail['data1'] = $data1->row();
            $r_detail['is_reprint'] = $_POST['is_reprint'];
            $r_detail['RRR'] = $this->db->query("SELECT * FROM `m_branches` WHERE bc = '$bc'")->row();

            $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
        } else {

            echo "<script>alert('No data found');close();</script>";

        }

    }

}
