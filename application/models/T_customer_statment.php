<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class T_customer_statment extends CI_Model
{

    private $sd;

    function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {

        $this->load->model("m_branches");

        $a['bc'] = $this->m_branches->select();

        $a['a'] = 1;
        return $a;
    }

    public function cus_det()
    {

        $bc = $_POST['bc'];

        if ($bc != "") {
            $Q_BC = " c.`bc` = '$bc' AND ";
        } else {
            $Q_BC = "";
        }

        $Q = $this->db->query("SELECT c.`cusname` , c.`serno`, c.`nicno`, c.`address`, c.`address2`, c.`mobile`, c.`telNo`, b.`name` AS `added_bc` FROM m_customer c LEFT JOIN `m_branches` b ON c.`bc` = b.`bc` WHERE $Q_BC (c.`nicno`       LIKE '%" . $_POST['aa'] . "%' AND c.`cusname`     LIKE '%" . $_POST['bb'] . "%' AND c.`address`     LIKE '%" . $_POST['cc'] . "%' AND c.`address2`    LIKE '%" . $_POST['dd'] . "%' AND c.`telNo`       LIKE '%" . $_POST['ee'] . "%' AND c.`mobile`      LIKE '%" . $_POST['ff'] . "%')");

        if ($Q->num_rows() > 0) {

            $data = "";
            $data .= "<table class='tbl_search'>";
            $data .= "<tr>";
            $data .= "<td>Name</td>";
            $data .= "<td>NIC</td>";
            $data .= "<td>Address 1</td>";
            $data .= "<td>Address 2</td>";
            $data .= "<td>Mobile</td>";
            $data .= "<td>Telephone</td>";
            $data .= "<td>Added Branch</td>";
            $data .= "</tr>";

            foreach ($Q->result() as $R) {

                $data .= "<tr class='cus_selected_row' nic='" . $R->serno . "' >";
                $data .= "<td valign='top'>" . strtoupper($R->cusname) . "</td>";
                $data .= "<td valign='top'>" . $R->nicno . "</td>";
                $data .= "<td valign='top'>" . str_replace(",", "<br>", $R->address) . "</td>";
                $data .= "<td valign='top'>" . str_replace(",", "<br>", $R->address2) . "</td>";
                $data .= "<td valign='top'>" . $R->mobile . "</td>";
                $data .= "<td valign='top'>" . $R->telNo . "</td>";
                $data .= "<td valign='top'>" . $R->added_bc . "</td>";
                $data .= "</tr>";

            }

            $data .= "</table>";

        } else {
            $data = "No data found for this criteria";
        }


        $a['s'] = 1;
        $a['data'] = $data;
        echo json_encode($a);
    }


    public function get_customer_bill_details()
    {
        $cusno = $_POST['cus_ser_no'];
        $Q = $this->db->query("SELECT c.`customer_id`,c.`serno`, c.`cusname` , c.`nicno`, c.`address`, c.`address2`, concat(c.`mobile`, if (c.`telNo` = '', '', concat(' / ',c.`telNo`) ) ) as contact FROM  m_customer c WHERE c.`serno` = '$cusno'");

        $a['customer_data'] = $Q->row();

        //-------------------------------------------------

        $QQ = $this->db->query("SELECT * FROM (
                
                SELECT L.billtype, L.`ddate`,L.`billno`,L.`requiredamount`, 1 as `count_for_pawn_value` FROM `t_loan` L WHERE L.`cus_serno` = '$cusno'
                Union all
                SELECT L.billtype,L.`ddate`,L.`billno`,L.`requiredamount`, 0 as `count_for_pawn_value` FROM `t_loan_re_fo` L WHERE L.`cus_serno` = '$cusno'

            ) a ORDER BY a.billno DESC ");

        if ($QQ->num_rows() > 0) {
            $a['cus_bills'] = $QQ->result();
        } else {
            $a['cus_bills'] = 0;
        }

        //-------------------------------------------------


        $a['s'] = 1;

        echo json_encode($a);
    }


    public function get_bill_statment()
    {
        $this->load->model('calculate');


        $billtype = $_POST['billtype'];
        $billno = $_POST['billno'];
        $customer_id = $_POST['customer_id'];

        $Q1 = $this->db->query("SELECT * FROM (SELECT *,'$customer_id' as `customer_id`, IFNULL(L.int_paid_untill,'not available')  AS `int_paid_untill_new`, 'P' as pawningStatus FROM `t_loan` L 
                                UNION ALL 
                                SELECT *,'$customer_id' as `customer_id`, IFNULL(LL.int_paid_untill,'not available')  AS `int_paid_untill_new`, 'R' as pawningStatus FROM `t_loan_re_fo` LL ) a WHERE a.billno = '$billno' LIMIT 1");

        $query2 = "SELECT * FROM (

    SELECT LT.`ddate`,
IF (LT.`transecode` = 'P','PAWNING',    
    IF (LT.`transecode` = 'A','Interest Payment',   
        IF (LT.`transecode` = 'R','REDEEM', 
    LT.`transecode`))   
) AS `trans_code`,

LT.`amount`,
LT.`action_date` 
FROM `t_loantranse` LT 
WHERE LT.billno = '$billno'

UNION ALL

    SELECT LT.`ddate`,
IF (LT.`transecode` = 'P','PAWNING',    
    IF (LT.`transecode` = 'A','Interest Payment',   
        IF (LT.`transecode` = 'R','REDEEM', 
    LT.`transecode`))   
) AS `trans_code`,

LT.`amount`,
LT.`action_date` 
FROM `t_loantranse_re_fo` LT 
WHERE LT.billno = '$billno'

UNION ALL 

    SELECT ADV.`date`,

    IF (    ADV.`trans_type` = 'ADV_P',
            'Advance Received',
            IF (ADV.`trans_type` = 'ADV_R',
                'Advance Amount Used to Settle', ADV.`trans_type`)     ) AS `trans_code`,

    IF (    ADV.`trans_type` = 'ADV_P',
            ADV.`cr_amount`,
            IF (ADV.`trans_type` = 'ADV_R',
                ADV.`dr_amount`, 'dr/cr')   ) AS `cr_amount`,

    ADV.`action_date` 

    FROM `t_loan_advance_customer` ADV 
    WHERE ADV.billno = '$billno'

    UNION ALL 

SELECT DATE_FORMAT(LFO.`action_date`,'%Y-%m-%d') AS `ddate`,'BILL RENEWED','', LFO.`action_date`  
FROM `t_loan_re_fo` LFO 
WHERE `status` = 'RN' AND  billno = '$billno'


) a ORDER BY a.ddate,a.action_date ";

        $Q2 = $this->db->query($query2);

//        $Q3 = $this->calculate->interest($Q1->row()->loanno, "json", "", $Q1->row(), 0);
//        $Q3 = $this->calculate->calculate_interest($Q1->row()->loanno, "json", "", $Q1->row(), 0);
        $Q3 = $this->calculate->calculate_interest(   $req_amount = $Q1->row()->requiredamount,
            $billtype = $Q1->row()->billtype,
            $pawn_date = $Q1->row()->ddate,
            $current_date = $this->sd['current_date'],
            $first_mon_int_cal_range = '',
            $loanno = $Q1->row()->loanno,
            3,
            $int_paid_untill = $Q1->row()->int_paid_untill,
            $is_amount_base_int_cal = $Q1->row()->is_amt_base,
            $fm_int_paid = $Q1->row()->fm_int_paid,
            $Q1->row()->ori_pwn_date,
            $Q1->row()
        );
        $Q4 = $this->calculate->customer_advance($Q1->row()->cus_serno, $billno);

//        var_dump($Q3); exit();
        //$Q_previous = $this->db->query("SELECT `status`,`billno` FROM `t_loan_re_fo` WHERE `old_o_new_billno` = '$billno' LIMIT 1")->row();

        //$Q_next     = $this->db->query("SELECT * FROM (SELECT `status`,`billno` FROM `t_loan` WHERE `old_o_new_billno` = '$billno'LIMIT 1 UNION ALL SELECT `status`,`old_o_new_billno` AS `billno` FROM `t_loan_re_fo` WHERE `billno` = '$billno'LIMIT 1) a HAVING NOT a.billno = 0 ")->row();


        $a['sum'] = $Q1->row();
        $a['det'] = $Q2->result();
        $a['int'] = $Q3;
        $a['customer_advance'] = $Q4;

        $a['previous_bill'] = '';
        $a['next_bill'] = '';

        $a['s'] = 1;


        echo json_encode($a);

    }


}    