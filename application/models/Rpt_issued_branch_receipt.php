<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_issued_branch_receipt extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report(){

        if($_POST['bc_arry'] === null){
            $BC = " ";
        }else{
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            $BC = " S.bc IN ($bc) AND ";
        }        

        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];
        

        

        /*echo $fd;
        echo $td;
        echo $BC;

        exit;*/
        
        $q = $this->db->query("SELECT S.`nno`,S.`date`,S.bc,S.`paid_to_acc`,S.`cash_amount`,S.`receipt_desc`,S.`payee_info`,S.`r_name`,S.`r_billno`,S.`r_pawn_date`, D.bc , D.det , S.bc AS `bcc`,B.`name` AS `b_name`

FROM `t_br_receipt_sum` S 

LEFT JOIN ( SELECT d.bc,d.nno, d.amount,GROUP_CONCAT(CONCAT( d.`acc_code`,' - ',ma.`description`,'(',d.amount,')')) AS `det` 
        FROM `t_br_receipt_det` d 
        JOIN m_account ma ON d.`acc_code` = ma.`code`
        WHERE d.`amount` > 0 GROUP BY d.bc, d.`nno` ) D ON S.`bc` = D.bc AND S.`nno` = D.nno 

JOIN m_branches B ON S.`bc` = B.`bc`

WHERE $BC S.`date` BETWEEN '$fd' AND '$td'

ORDER BY S.`bc` , S.`date` ");      

        if($q->num_rows()>0){
            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}