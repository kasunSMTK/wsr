<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class print_bc_issued_cheques extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report(){

        $fd = $_POST['fd'];
        $td = $_POST['td'];
        $bc = $_POST['r_issue_bc'];
    
        $q = $this->db->query("
SELECT S.`no`,S.`date`,S.`bank_acc`,AC.`description` AS `acc_desc`,S.`memo`,S.`employee`,S.`action_datetime`,S.`oc`,D.`amount`,D.`chq_no` , B.`name` AS `bc_name` 
FROM t_chq_reg_sum S

JOIN m_branches B ON S.`issue_bc` = B.`bc`
JOIN m_account AC ON S.`bank_acc` = AC.`code`
JOIN (  SELECT d.`branch`,d.`no`,d.`amount`, GROUP_CONCAT(CONCAT('',d.`chq_no`,' ')) AS `chq_no` 
        FROM t_chq_reg_det d 
        WHERE d.branch = '$bc'
        GROUP BY d.`branch`,d.`no`

    ) D ON S.`issue_bc` = D.`branch` AND S.`no` = D.`no`

WHERE S.`issue_bc` = '$bc' AND S.`date` BETWEEN '$fd' AND '$td'
ORDER BY S.`issue_bc`,S.`date` DESC ");        
        
        if ($q->num_rows() > 0){            
            $r_detail['Q']	=	$q->result();            
            $r_detail['fd']  =   $fd;            
            $r_detail['td']  =   $td;
            $r_detail['bc_name']  =   $q->row()->bc_name;
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);
        }else{
            echo "<script>close();</script>";
        }        		 

    }
    
}