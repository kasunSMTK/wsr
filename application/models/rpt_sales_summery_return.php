<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class rpt_sales_summery_return extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    public function Excel_report()
    {

        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST, 'XL');
    }

    public function PDF_report($a, $rt = 'PDF')
    {

        if ($_POST['bc_arry'] === null) {
            $BC = " ";
        } else {

            for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                $bc_ar[] = "'" . str_replace(",", "", $_POST['bc_arry'][$n]) . "'";
            }

            $bc = implode(',', $bc_ar);
            $BC = " AND s.bc IN ($bc)  ";
        }

        // var_dump($bc,$BC);
        // exit();
        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];

        $q = $this->db->query("SELECT
        s.bc,
        s.nno,
        s.cus_serno,
        c.`cusname`,
        f.store,
        st.`description` AS store_name,
        s.employee,
        e.`name` AS emp_name,
        s.note,
        s.amount AS total_amount,
        s.total_discount,
        s.net_amount
      FROM
      t_sales_ret_sum s
      JOIN m_customer c ON c.`serno`=s.`cus_serno`
      JOIN m_employee e ON e.`code`=s.`employee`
      join t_sales_sum f on f.nno= s.sales_sum
      JOIN m_store st ON st.`code`=f.`store`
      WHERE  s.ddate BETWEEN '" . $fd . "'
        AND '" . $td . "'
        order by s.nno ");

        if ($q->num_rows() > 0) {

            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            if ($rt == "PDF") {
                $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
            } else {
                echo "<script>alert('Excel Report Not Found');close();</script>";
            }

        } else {
            echo "<script>alert('No data found');close();</script>";
        }

    }

}
