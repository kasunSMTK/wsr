<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_print_fund_transfer_report extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report(){        

        $fd = $_POST['fd_r'];
        $td = $_POST['td_r'];
        $ty = $_POST['tr_type'];



        if ( $ty == "r_print_cheque"){


            if ( !is_array($_POST['bc_r']) ){
            
                $_POST['bc_arry'] = explode(",",$_POST['bc_r']);
                
                for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                    $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
                }

                $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
            }

            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);




            if ($bc == "''"){
                $bc   = "";
            }else{            
                $bc   = " AND A.`to_bc` IN ($bc) ";            
            }


            //echo $bc;   



            $q = $this->db->query(" SELECT A.`no`,D.description AS `bank`,B.cheque_no,B.amount,C.`name` AS `bc_name`, A.`date_time`,A.`status` AS `ss`,A.`to_bc` FROM t_fund_transfer_sum A JOIN t_fnd_trnsfr_rqsts_chq_det B ON A.`no` = B.`no` JOIN m_branches C ON A.`to_bc` = C.`bc` JOIN m_account D ON B.`bank` = D.`code` WHERE A.`transfer_type` = 'cheque_withdraw' AND DATE(A.`date_time`) BETWEEN '$fd' AND '$td' $bc ORDER BY A.`to_bc`,A.`date_time` DESC ");

            if ($q->num_rows() > 0){

                if ($json){
                    $ttx = '<span class="title_hid_txt">(Approved)</span>';
                }else{
                    $ttx = '';
                }


                    $t  = '<table class="tbl_cheque_status" width="100%" id="table" border="1">';

                    $t .= '<tr>';
                    $t .= '<td colspan="6"><h1>Cheque Withdrawal Status from '.$fd.' to '.$td.'</h1></td>';
                    $t .= '</tr>';

                    $s = $sx = '';

                foreach ($q->result() as $r) {

                    if ($s != $r->to_bc){
                        $t .= '<tr bgColor="#cccccc" class="row_bc" cl="'.str_replace(" ", "", $r->bc_name ).'">';
                        $t .= '<td colspan="2"><br><br>'.$r->bc_name.'</td>';
                        $t .= '<td width="150"></td>';
                        $t .= '<td colspan="3"></td>';
                        $t .= '</tr>';
                        
                        $t .= '<tr class="'.str_replace(" ", "", $r->bc_name).' all_rows" style="" bgColor="#f9f9f9">';
                        $t .= '<td>Fund Request No</td>';
                        $t .= '<td>Bank</td>';
                        $t .= '<td align="center">Cheque No</td>';
                        $t .= '<td align="right">Amount</td>';                
                        $t .= '<td align="center">Status</td>';
                        $t .= '<td align="center">Date amd Time</td>';
                        $t .= '</tr>';
                        
                        $s = $r->to_bc;
                        $sx= $r->to_bc."_chq";
                    }                

                    $t .= '<tr class="'.str_replace(" ", "", $r->bc_name).' all_rows" style="">';
                    $t .= '<td align="center">'.$r->no.'</td>';
                    $t .= '<td>'.$r->bank.'</td>';
                    $t .= '<td align="center" class="'.$sx.'">'.$r->cheque_no.'</td>';
                    $t .= '<td>'.$r->amount.'</td>';                
                    
                    $st = '';

                    if ($r->ss == 1){
                        $st = '<span style="color:#000000">Branch waiting for head office respond</span>';
                    }elseif ($r->ss == 2) {
                        $st = '<span style="color:green">Accepted by head office. Branch must print voucher</span>';
                    }elseif ($r->ss == 3) {
                        $st = '<span style="color:red">Request reject by head office</span>';
                    }elseif ($r->ss == 4) {
                        $st = '<span style="color:#333333">Voucher print from branch.</span>';
                    }elseif ($r->ss == 6) {
                        $st = '<span style="color:red">Request canceled by head office</span>';
                    }elseif ($r->ss == 10) {
                        $st = '<span style="color:green"><b>Transaction completed</b></span>';                
                    }else{
                        $st = $r->ss;
                    }


                    $t .= '<td align="center">'.$st.'</td>';


                    $t .= '<td align="center">'.$r->date_time.'</td>';
                    $t .= '</tr>';

                }

                    $t .= '</table>';

            }else{

                $t = 'No data found';

            }

        }

        

        if ( $ty == "r_print_cash_transfer"){

            if ( !is_array($_POST['bc_r']) ){
            
                $_POST['bc_arry'] = explode(",",$_POST['bc_r']);
                
                for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                    $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
                }

                $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
            }

            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);




            if ($bc == "''"){
                $bc   = "";
            }else{            
                $bc   = " AND F.`to_bc` IN ($bc) ";            
            }


            //echo $bc;
            
            $q = $this->db->query(" SELECT F.`no`, F.`date_time`, F.`from_bc`, B1.`name` AS `from_bc_name`, F.`to_bc`, B2.`name` AS `to_bc_name`, F.`amount`, F.`status` AS `ss` FROM t_fund_transfer_sum F JOIN m_branches B1 ON F.`from_bc` = B1.`bc` JOIN m_branches B2 ON F.`to_bc` = B2.`bc` WHERE transfer_type = 'cash' AND DATE(F.`date_time`) BETWEEN '$fd' AND '$td' $bc ORDER BY F.`from_bc` ");

            if ($q->num_rows() > 0){

                if ($json){
                    $ttx = '<span class="title_hid_txt">(Approved)</span>';
                }else{
                    $ttx = '';
                }


                    $t  = '<table class="tbl_cheque_status" width="100%" id="table" border="1">';

                    $t .= '<tr>';
                    $t .= '<td colspan="6"><h1>Cash Transfer Status from '.$fd.' to '.$td.'</h1></td>';
                    $t .= '</tr>';

                    $s = $sx = '';

                foreach ($q->result() as $r) {

                    if ($s != $r->from_bc){
                        
                        $t .= '<tr bgColor="#cccccc" class="row_bc" cl="'.str_replace(" ", "", $r->from_bc_name ).'">';
                        $t .= '<td colspan="3">'.$r->from_bc_name.'</td>';
                        $t .= '<td width="150"></td>';
                        $t .= '<td colspan="2"></td>';
                        $t .= '</tr>';
                        
                        $t .= '<tr bgColor="#eaeaea" class="'.str_replace(" ", "", $r->from_bc_name).' all_rows" style="" bgColor="#f9f9f9">';
                        $t .= '<td>Fund Request No</td>';
                        $t .= '<td>Sending Branch</td>';
                        $t .= '<td>Receiving Branch</td>';
                        $t .= '<td align="right">Amount</td>';                
                        $t .= '<td align="center">Status</td>';
                        $t .= '<td>Date amd Time</td>';
                        $t .= '</tr>';
                        
                        $s = $r->from_bc;
                        $sx= $r->from_bc."_chq";
                    }                

                    $t .= '<tr class="'.str_replace(" ", "", $r->from_bc_name).' all_rows" style="">';
                    $t .= '<td align="center">'.$r->no.'</td>';
                    $t .= '<td>'.$r->from_bc_name.'</td>';
                    $t .= '<td>'.$r->to_bc_name.'</td>';
                    $t .= '<td  align="right" class="'.$sx.'">'.$r->amount.'</td>';                
                    
                    $st = '';

                    if ($r->ss == 1){
                        $st = '<span style="color:#000000">Branch waiting for head office respond</span>';
                    }elseif ($r->ss == 2) {
                        $st = '<span style="color:green">Accepted by head office. Branch must print voucher</span>';
                    }elseif ($r->ss == 3) {
                        $st = '<span style="color:red">Request reject by head office</span>';
                    }elseif ($r->ss == 4) {
                        $st = '<span style="color:#333333">Voucher print from branch.</span>';
                    }elseif ($r->ss == 5) {
                        $st = '<span style="color:#333333">Request forwarded</span>';
                    }elseif ($r->ss == 6) {
                        $st = '<span style="color:red">Request canceled by head office</span>';
                    }elseif ($r->ss == 10) {
                        $st = '<span style="color:green"><b>Transaction completed</b></span>';                
                    }else{
                        $st = $r->ss;
                    }


                    $t .= '<td align="center">'.$st.'</td>';


                    $t .= '<td>'.$r->date_time.'</td>';
                    $t .= '</tr>';

                }

                    $t .= '</table>';

            }else{

                $t = 'No data found';

            }
        }


        if ( $ty == "r_print_cash_deposit"){

            if ( !is_array($_POST['bc_r']) ){
            
                $_POST['bc_arry'] = explode(",",$_POST['bc_r']);
                
                for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                    $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
                }

                $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
            }

            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);




            if ($bc == "''"){
                $bc   = "";
            }else{            
                $bc   = " AND F.`from_bc` IN ($bc) ";            
            }


            //echo $bc;

            $q = $this->db->query(" SELECT F.`no`, F.`date_time`, B1.`name` AS `from_bc_name`, F.`ref_no`, F.`amount`, F.`status` as `ss` FROM t_fund_transfer_sum F JOIN m_branches B1 ON F.`from_bc` = B1.`bc` WHERE transfer_type = 'bank_deposit' AND DATE(F.`date_time`) BETWEEN '$fd' AND '$td'  $bc  ORDER BY B1.`name` ");

            if ($q->num_rows() > 0){

                if ($json){
                    $ttx = '<span class="title_hid_txt">(Approved)</span>';
                }else{
                    $ttx = '';
                }


                    $t  = '<table class="tbl_cheque_status" width="100%" id="table" border="1">';

                    $t .= '<tr>';
                    $t .= '<td colspan="6"><h1>Cash Deposit Status from '.$fd.' to '.$td.'</h1></td>';
                    $t .= '</tr>';

                    $s = $sx = '';

                foreach ($q->result() as $r) {

                    if ($s != $r->from_bc_name){
                        $t .= '<tr bgColor="#cccccc" class="row_bc" cl="'.str_replace(" ", "", $r->from_bc_name ).'">';
                        $t .= '<td colspan="3"><h2>'.$r->from_bc_name.'</h2></td>';
                        $t .= '<td width="150"></td>';
                        $t .= '<td colspan="2"></td>';
                        $t .= '</tr>';
                        
                        $t .= '<tr class="'.str_replace(" ", "", $r->from_bc_name).' all_rows" style="" bgColor="#f9f9f9">';
                        $t .= '<td>Fund Request No</td>';
                        $t .= '<td>Depositing Branch</td>';
                        $t .= '<td>Deposited Bank</td>';
                        $t .= '<td align="right">Amount</td>';                
                        $t .= '<td align="center">Status</td>';
                        $t .= '<td>Date amd Time</td>';
                        $t .= '</tr>';
                        
                        $s = $r->from_bc_name;
                        $sx= $r->from_bc_name."_chq";
                    }                

                    $t .= '<tr class="'.str_replace(" ", "", $r->from_bc_name).' all_rows" style="">';
                    $t .= '<td align="center">'.$r->no.'</td>';
                    $t .= '<td>'.$r->from_bc_name.'</td>';
                    $t .= '<td>'.$r->ref_no.'</td>';
                    $t .= '<td align="right" class="'.$sx.'">'.$r->amount.'</td>';                
                    
                    $st = '';

                    if ($r->ss == 1){
                        $st = '<span style="color:#000000">Branch waiting for head office respond</span>';
                    }elseif ($r->ss == 2) {
                        $st = '<span style="color:green">Accepted by head office. Branch must print voucher</span>';
                    }elseif ($r->ss == 3) {
                        $st = '<span style="color:red">Request reject by head office</span>';
                    }elseif ($r->ss == 4) {
                        $st = '<span style="color:#333333">Voucher print from branch.</span>';
                    }elseif ($r->ss == 6) {
                        $st = '<span style="color:red">Request canceled by head office</span>';
                    }elseif ($r->ss == 10) {
                        $st = '<span style="color:green"><b>Transaction completed</b></span>';                
                    }else{
                        $st = $r->ss;
                    }


                    $t .= '<td align="center">'.$st.'</td>';


                    $t .= '<td>'.$r->date_time.'</td>';
                    $t .= '</tr>';

                }

                    $t .= '</table>';

            }else{

                $t = 'No data found';

            }

        }


        header('Content-type: application/excel');
        $filename = 'income_statement.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;


        /*if($data1->num_rows()>0){
            
            $r_detail['user_des'] = $this->sd['user_des'];
            $this->load->view($_POST['by'].'_'.'excel',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }	*/	 

    }   
    
}