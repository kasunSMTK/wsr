<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_account_report extends CI_Model {

    private $tb_items;
    private $tb_storse;
    private $tb_department;
    private $sd;
    private $w = 297;
    private $h = 210;
    
    function __construct(){
        parent::__construct();
        
        $this->load->database();
        $this->load->library('useclass');
        
        $this->sd = $this->session->all_userdata();
        $this->load->database($this->sd['db'], true);

        $this->tb_items = $this->tables->tb['m_items'];
        // $this->tb_storse = $this->tables->tb['m_stores'];
    }
    
    public function base_details(){
      $this->load->model('m_stores');
      $this->load->model('m_branches');
      $a['cluster']=$this->get_cluster_name();
      $a['branch']=$this->get_branch_name();
        // $a['stores']=$this->get_stores();
      $a['d_cl']=$this->sd['cl'];
      $a['d_bc']=$this->sd['branch'];
      return $a;
  }


  public function get_cluster_name(){
      $sql="  SELECT m.code,m.description AS dis
      FROM m_cluster m
      JOIN u_branch_to_user u ON u.cl = m.code
      WHERE user_id='".$this->sd['oc']."'
      GROUP BY m.code";
      $query=$this->db->query($sql);

      $s = "<select name='cluster' id='cluster' style='display:none;' >";
      $s .= "<option value='0'>---</option>";
      foreach($query->result() as $r){
        $s .= "<option title='".$r->dis."' value='".$r->code."'>".$r->code.'-'.$r->dis."</option>";
    }
    $s .= "</select>";
       //$s="C1";
    return $s;
}


public function get_branch_name(){
  $this->db->select(array('bc','name'));
  $query = $this->db->get('m_branches');

  $s = "<select name='branch' id='branch'>";
  $s .= "<option value='0'>---</option>";
  foreach($query->result() as $r){
    $s .= "<option title='".$r->name."' value='".$r->bc."'>".$r->bc.'-'.$r->name."</option>";
}
$s .= "</select>";

return $s;

}

public function get_branch_name2(){
    $sql="  SELECT m.`bc`,name 
    FROM m_branch m
    JOIN u_branch_to_user u ON u.bc = m.bc
    WHERE user_id='".$this->sd['oc']."' AND m.cl='".$_POST['cl']."'
    GROUP BY m.bc";
    $query=$this->db->query($sql);

    $s = "<select name='branch' id='branch' >";
    $s .= "<option value='0'>---</option>";
    foreach($query->result() as $r){
        $s .= "<option title='".$r->name."' value='".$r->bc."'>".$r->bc.'-'.$r->name."</option>";
    }
    $s .= "</select>";

    echo $s;
}

public function get_branch_name3(){
    $sql="  SELECT m.`bc`,name 
    FROM m_branch m
    JOIN u_branch_to_user u ON u.bc = m.bc
    WHERE user_id='".$this->sd['oc']."'
    GROUP BY m.bc";
    $query=$this->db->query($sql);

    $s = "<select name='branch' id='branch' >";
    $s .= "<option value='0'>---</option>";
    foreach($query->result() as $r){
        $s .= "<option title='".$r->name."' value='".$r->bc."'>".$r->bc.'-'.$r->name."</option>";
    }
    $s .= "</select>";

    echo $s;
}


  // public function get_stores(){
        // $this->db->select(array('code','description'));
        // $query = $this->db->get('m_stores');

        // $s = "<select name='store' id='store' >";
  //       $s .= "<option value='0'>---</option>";
  //       foreach($query->result() as $r){
  //           $s .= "<option title='".$r->description."' value='".$r->code."'>".$r->code.'-'.$r->description."</option>";
     //    }
  //       $s .= "</select>";

  //       return $s;

  //   }

public function get_account(){   

    if($_POST['search'] == 'Key Word: code, name'){$_POST['search'] = "";}

      //$sql = "SELECT m.`code`,IFNULL(c.`nicno`,'') AS nic,m.`description` FROM m_account m LEFT JOIN m_customer c ON c.`code` = m.`code` WHERE m.code LIKE '%$_POST[search]%' OR nicno LIKE '%$_POST[search]%'OR description LIKE '%$_POST[search]%'LIMIT 25";

    $sql = "SELECT m.`code`, m.`description`, '' as `nic` FROM m_account m 
    WHERE m.code LIKE '%".$_POST['search']."%' OR m.description LIKE '%".$_POST['search']."%'LIMIT 25";

    $query = $this->db->query($sql);

    $a  = "<table id='item_list' style='width : 100%' >";
    $a .= "<thead><tr>";
    $a .= "<th class='tb_head_th'>Code</th>";
    $a .= "<th class='tb_head_th' colspan='2'>Description</th>";
    $a .= "</thead></tr><tr class='cl'><td>&nbsp;</td><td colspan='2'>&nbsp;</td><td>&nbsp;</td></tr>";

    foreach($query->result() as $r){
      $a .= "<tr class='cl'>";
      $a .= "<td>".$r->code."</td>";
      $a .= "<td colspan='2'>".$r->description."</td>";
      $a .= "</tr>";
  }
  $a .= "</table>";
  echo $a;
  
}    


public function account_type(){
    $this->db->like('code', $_GET['q']);
    $this->db->or_like('heading', $_GET['q']);
    $query = $this->db->select(array('code','heading'))->get("m_account_type");
    $abc = "";
    foreach($query->result() as $r){
        $abc .= $r->code."|".$r->heading;
        $abc .= "\n";
    }
    echo $abc;
} 


public function trans_type(){
    $this->db->like('code', $_GET['q']);
    $this->db->or_like('description', $_GET['q']);
    $query = $this->db->select(array('code','description'))->get("t_trans_code");
    $abc = "";
    foreach($query->result() as $r){
        $abc .= $r->code."|".ucwords(strtolower($r->description));
        $abc .= "\n";
    }
    echo $abc;
}


public function auto_com(){
    $this->db->like('code', $_GET['q']);
    $this->db->or_like('description', $_GET['q']);
    $query = $this->db->select(array('code','description'))->get('m_account');
    $abc = "";
    foreach($query->result() as $r){
        $abc .= $r->code."|".$r->description;
        $abc .= "\n";
    }
    echo $abc;
}  

public function acc_cat(){
    $this->db->like('code', $_GET['q']);
    $this->db->or_like('description', $_GET['q']);
    $query = $this->db->select(array('code','description'))->get('m_account_category');
    $abc = "";
    foreach($query->result() as $r){
        $abc .= $r->code."|".$r->description;
        $abc .= "\n";
    }
    echo $abc;
}  

public function cntrl_acc(){
    $this->db->like('code', $_GET['q']);
    $this->db->or_like('description', $_GET['q']);
    $this->db->where('is_control_acc','1');
    $query = $this->db->select(array('code','description'))->get('m_account');
    $abc = "";
    foreach($query->result() as $r){
        $abc .= $r->code."|".$r->description;
        $abc .= "\n";
    }
    echo $abc;
} 


public function get_account_det(){
    $this->db->select(array('m_account.code','m_account.description','m_account_type.heading as heading','m_account_type.code as type'));
    $this->db->where('m_account.code',$_POST['code']);
    $this->db->from('m_account');
    $this->db->join('m_account_type','m_account_type.code=m_account.type');
    $this->db->join('m_account_category','m_account_category.code=m_account.category');
    $query=$this->db->get();
    if($query->num_rows()>0){
        $a['acc']=$query->result();
    }else{
        $a['acc']=2;
    }
    echo json_encode($a);
}



public function PDF_report(){

    ini_set('memory_limit', '750M');

        $_POST_['bc'] = $_POST['branchs'];
        $_POST_['from_date'] = $_POST['from'];
        $_POST_['to_date'] = $_POST['to'];      

        $branch = $_POST['branchs'];

        if ($branch != ''){
            
            $bcC    = " ta.bc = '$branch' AND ";            
            $bc_for_op_bal_query = " AND t_account_trans.bc = '$branch'"; 

        }else{

            $bcC    = "";            
            $bc_for_op_bal_query = ""; 

        }



    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND ta.`billtype`='".$_POST['r_billtype']."'";
    }

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    $ftime = $_POST['ftime'];
    $ttime = $_POST['ttime'];

    $Qtime = "";
    
    if (!$ftime == ""){        
        $op_bal_time_range = " TIMESTAMP(`ddate`,`time`) <= '$td $ftime'  ";
        $date_time_range = " AND TIMESTAMP(`ddate`,`time`) BETWEEN '$fd $ftime' AND '$td $ttime' ";
    }else{        
        $op_bal_time_range = " ddate<'$fd' ";
        $date_time_range = " AND ta.ddate BETWEEN '$fd' AND '$td' ";
    }
    
    $cluster=$_POST['cluster'];
    //$branch=$_POST['branch'];

    $cl=$this->sd['cl'];
    $bc=$branch;

    $this->db->select(array('name'));
    $r_detail['company'] = $this->db->get('m_company')->result();

    $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $this->sd['branch']);
    $r_detail['branch'] = $this->db->get('m_branches')->result();

    $bc_acc_code = $_POST['acc_code'];


    if ($bc_acc_code == ''){
        $q_acc = '';
    }else{
        $q_acc = " AND acc_code='$bc_acc_code'" ;
    }

    $this->db->where('code','$bc_acc_code');

    $query=$this->db->get('m_account');
    foreach($query->result() as $row){
        $r_detail['account_det']=$row->description;
    }

    $sql="  SELECT t_account_trans.bc, SUM(dr_amount-cr_amount) AS op, SUM(dr_amount) AS dr, SUM(cr_amount) AS cr , B.`name` AS `bc_name`
            FROM t_account_trans
            JOIN m_branches B ON t_account_trans.`bc` = B.`bc`
            WHERE $op_bal_time_range $q_acc $bc_for_op_bal_query
            GROUP BY t_account_trans.bc ";
    
    //$r_detail['op']=$this->db->query($sql)->first_row()->op;
    $r_detail['op_bal']=$this->db->query($sql)->result();


    
    if ($this->sd['isAdmin'] == 1){
        // $qqq = ' concat(ta.ddate, "" ,ta.time , " - ", ta.description ) as `description` ';
        $qqq = ' ta.description ';
    }else{
        $qqq = ' ta.description ';
    }


    /*$sqll="SELECT ta.*,td.`description` AS det, td.`code` AS t_code, mb.`name` AS branch_name , CONCAT(ta.description,' , Class BC : ',mb2.`name` ) AS `description`
    FROM t_account_trans ta 
    LEFT JOIN t_trans_code td ON td.`code`= ta.`trans_code` 
    LEFT JOIN m_branches mb ON mb.`bc`=ta.`bc`
    LEFT JOIN m_branches mb2 ON mb2.bc =ta.`v_bc`
    WHERE $bcC  ta.ddate!='' $billtype $date_time_range ";    */

    $sqll="SELECT ta.*,td.`description` AS det, td.`code` AS t_code, mb.`name` AS branch_name , 

    CONCAT( IFNULL(ta.description,''),' ',  IFNULL(CONCAT('- Chq no:',cis.`cheque_no`),'') , '-' , IFNULL(ma2.`description`,'') ,
    IFNULL(CONCAT(' : Class BC : ',mb2.`name`),' '), 
    IFNULL(CONCAT(ft.ref_no,' (', ft.to_acc,')', ma.`description` , ' : (' , ft.`from_acc`,')',ma3.`description` ) , '' ), IFNULL(CONCAT(' | Cash Supplier: ', vgs.`payee_info`),'') , if(ta.reconcile=1,' - RECONCILED*','') ) AS `description`
    
    FROM t_account_trans ta 
    LEFT JOIN t_trans_code td ON td.`code`= ta.`trans_code` 
    LEFT JOIN m_branches mb ON mb.`bc`=ta.`bc`
    LEFT JOIN m_branches mb2 ON mb2.bc =ta.`v_bc`
    LEFT JOIN `t_fund_transfer_sum` ft ON ta.trans_code = 9 AND ta.trans_no = ft.no
    LEFT JOIN m_account ma ON ft.`to_acc` = ma.`code`
    LEFT JOIN m_account ma3 ON ft.`from_acc` = ma3.`code`
    
    LEFT JOIN t_cheque_issued cis ON ta.`trans_code` = cis.`trans_code` AND ta.`trans_no` = cis.`trans_no` AND ta.`bc` = cis.`bc`
    LEFT JOIN m_account ma2 ON cis.`account` = ma2.`code`
    LEFT JOIN t_voucher_gl_sum vgs ON ta.`bc` = vgs.`bc` AND ta.`trans_code` = 48 AND ta.`trans_no` = vgs.`nno` AND vgs.type = 'cash'

    WHERE $bcC  ta.ddate!='' $billtype $date_time_range ";    
    

    if ($bc_acc_code != ''){
        $sqll.=" AND ta.acc_code='$bc_acc_code'";
    }
    
    
    $sqll.=" ORDER BY ta.bc, ta.ddate, ta.auto_no ";

/*    
        echo $sqll;
        exit;*/


    $r_detail['all_acc_det']=$this->db->query($sqll)->result();

    $cl=$this->sd['cl'];
    $bc=$this->sd['branch'];
    $r_detail['acc_code']=$_POST['acc_code'];
    $r_detail['page']='A4';
    $r_detail['orientation']='P';  
    $r_detail['dfrom']=$fd;
    $r_detail['dto']=$td;

    $r_detail['tf']=$ftime;
    $r_detail['tt']=$ttime;

    



    if($this->db->query($sqll)->num_rows()>0){
     
     if ($_POST['rtype'] == 0){
        $this->load->view('r_account_report_new_pdf',$r_detail);
    }else{
        $this->load->view('r_account_report_new_excel',$r_detail);
    }

  }else{
    echo "<script>alert('No Data');window.close()</script>";
  }

    }



}   
?>