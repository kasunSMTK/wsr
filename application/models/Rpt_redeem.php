<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class rpt_redeem extends CI_Model {

    

    private $sd;    

    

    function __construct(){

        parent::__construct();      

        $this->sd = $this->session->all_userdata();     

    }

    

    public function base_details(){     

        $a['max_no'] = 1;        

        return $a;

    }   



    public function PDF_report($_POST_,$r_type = "pdf"){



        ini_set('max_execution_time', 600 );



        if (isset($_POST['bc_n'])){

        if ( $_POST['bc_n'] == "" ){        

            $bc = "";

        }        

    }else{



        if ( !is_array($_POST['bc_arry']) ){

            

            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);

            

            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                

                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";

            }



            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];

        }



        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            

            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";

        }



        $bc = implode(',', $bc_ar);



        if ($bc == ""){

            $bc   = "";

        }else{

            $bc   = "   L.bc IN ($bc) AND ";

        }

    }



    // r_billtype filteration 

    $billtype = "";

    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){

        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";

    }



        $fd = $_POST_['from_date'];

        $td = $_POST_['to_date'];



        $ftime = $_POST['ftime'];

        $ttime = $_POST['ttime'];



        $Qtime = "";

        

        if ( $this->sd['isAdmin'] == 1 ){

            if (!$ftime == ""){

                $Qtime = " AND TIME(LT.`action_date`) BETWEEN '$ftime' AND '$ttime'";

            }

        }


        $qry = "SELECT 

            L.`loanno`,
            BT.`billtype`,
            L.`billno`,
            L.`ddate` AS `pawn_date`,
            LT.`ddate`,
            C.`cusname`,
            C.`address`,
            C.`nicno`,
            C.`mobile`,
            L.`totalweight`,
            L.`fmintrest` AS `pawn_int`,
            L.billtypeno,
            LTT.amount AS `capital_amount`,
            L.`stamp_fee`,
            /* IFNULL(ACC.cr_amount,0) AS postage, */
            (LT.`redeem_int` + LT.discount) AS `redeem_int`,
            LT.discount AS `redeem_discount`,
            /* IFNULL(ACC.cr_amount,0) */
            (LTT.amount + L.`stamp_fee` + (LT.`redeem_int` + LT.discount)) - LT.discount AS `redeem_amount`, B.name AS `bc_name`,
            TIME(LT.`action_date`) as `tm`,
            SUM(IF ( LT.transecode = 'RF',LT.amount,0 )) AS `refund_amount`

            FROM `t_loantranse_re_fo` LT 
            JOIN `t_loan_re_fo` L ON LT.`billno` = L.`billno` AND LT.`bc` = L.`bc` 
            JOIN `m_customer` C ON L.`cus_serno` = C.`serno` 
            JOIN `m_bill_type` BT ON L.`billtype` = BT.`billtype`  AND L.`bc` = BT.`bc` 
            /* LEFT JOIN ( SELECT cr_amount,billno FROM t_account_trans WHERE entry_code = 'DF' GROUP BY billno  ) ACC ON LT.billno = ACC.billno */ 
            JOIN m_branches B ON L.bc = B.bc
              JOIN redeem_capital LTT ON L.`billno` = LTT.`billno` AND L.`bc` = LTT.`bc`
            WHERE $bc LT.`transecode` IN ('R','RF') AND LT.ddate BETWEEN '$fd' AND '$td' $billtype $Qtime
            GROUP BY L.`loanno` ORDER BY L.bc,L.loanno,L.ddate";

        $Q = $this->db->query($qry);

        if($Q->num_rows() > 0){

            $r_data['list'] = $Q->result();

            $r_data['fd'] = $fd;

            $r_data['td'] = $td;


            if ($r_type == "pdf"){                            

                $this->load->view($_POST['by'].'_'.'pdf',$r_data);

            }else{

                $this->load->view($_POST['by'].'_'.'excel',$r_data);

            }



        }else{

            echo "<script>location='default_pdf_error'</script>";

        }



    }

    



    public function Excel_report()

    {

        $this->PDF_report($_POST,"Excel");

    }



}