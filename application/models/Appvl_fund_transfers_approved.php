<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class appvl_fund_transfers_approved extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;
		return $a;
    }


    public function approve_cheque_withdraw_request(){
	    
	    $_POST['hid'] = 0;
	    $_POST['date'] = $this->sd['current_date'];

	    $_POST['ln'] = "";
	    $_POST['bt'] = "";
	    $_POST['bn'] = "";

	    $no = $this->max_no = $_POST['no'];    
	    
	    $this->db->trans_begin();    

	        $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 2 WHERE `no` = '$no' LIMIT 1 ");

	    if ($this->db->trans_status() === FALSE){
	        
	        $this->db->trans_rollback();
	        $a['s'] = "error";

	    }else{

	        $this->db->trans_commit();        
	        $a['s'] = 1;

	    }

	    echo json_encode($a);

	}


	public function reject_cheque_withdraw_request(){
	    
	    $no = $_POST['no'];    
	    
	    $this->db->trans_begin();    

	        $this->db->query("UPDATE `t_fund_transfer_sum` 			SET `status` = 3 WHERE `no` = '$no' LIMIT 1 ");
	        $this->db->query("UPDATE `t_fnd_trnsfr_rqsts_chq_det` 	SET `status` = 3 WHERE `no` = '$no' LIMIT 1 ");

	    if ($this->db->trans_status() === FALSE){
	        
	        $this->db->trans_rollback();
	        $a['s'] = "error";

	    }else{

	        $this->db->trans_commit();        
	        $a['s'] = 1;

	    }

	    echo json_encode($a);

	}

    
}