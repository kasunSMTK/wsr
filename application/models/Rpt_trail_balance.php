<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_trail_balance extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function PDF_report($_POST_){

		$bc = $_POST_['bc'];
		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];

		// if ($bc == ""){
		// 	$QBC = "";			
		// }else{
		// 	$QBC = "L.`bc`='$bc' AND ";
		// }

		if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
            $QBC = "";
        }else{
        	$QBC = "   L.bc IN ($bc) AND ";
            $bc   = "   L.bc IN ($bc) AND ";

        }
    }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }


		$Q  = $this->db->query("SELECT * FROM (

SELECT  L.`ddate` AS `p_date`	 , L.`billno`, C.`cusname` AS `name`, C.`nicno`
	, GROUP_CONCAT( CONCAT( I.itemname , '(',LI.`pure_weight`,')') ) AS `item`
	,LI.`pure_weight`  AS `weight`, L.`requiredamount` AS `p_amount`, '' AS `initial1`, 
	IFNULL(LT_R.`amount`,0) AS `r_amount`, IFNULL(LT_R.`ddate`,'') AS `r_date`, 	'' AS `initial2`,
	IFNULL(C_STK.pw,0) AS `cus_stock`	,  L.`fmintrest` AS `p_interest` 
	, LT_R.`redeem_int` AS `r_interest`  ,L.`cus_serno`
FROM `t_loan` L
	LEFT JOIN `t_loantranse` LT_P 	ON LT_P.`billno` = L.`billno` AND LT_P.`loanno` = L.`loanno` AND LT_P.`transecode` = 'P'
	LEFT JOIN `t_loantranse` LT_R 	ON LT_P.`billno` = L.`billno` AND LT_R.`loanno` = L.`loanno` AND LT_R.`transecode` = 'R'
	JOIN m_customer 	C 	ON L.`cus_serno` = C.`serno` 
	JOIN `t_loanitems`	LI 	ON L.`loanno` = LI.`loanno`
	JOIN r_items 		I 	ON LI.`itemcode` = I.`itemcode` 

	LEFT JOIN (SELECT t.`cus_serno` , SUM( t.`totalweight`) AS PW FROM `t_loan` t WHERE t.`status`='P'  GROUP BY t.`cus_serno`) 	C_STK ON L.`cus_serno` = C_STK.cus_serno 

 WHERE $QBC L.`ddate` <= '$td' $billtype 
 
 GROUP BY L.`billtype` , L.`billno` 

UNION ALL

SELECT  L.`ddate` AS `p_date`	 , L.`billno`, C.`cusname` AS `name`, C.`nicno`
	, GROUP_CONCAT( CONCAT( I.itemname , '(',LI.`pure_weight`,')') ) AS `item`
	,LI.`pure_weight`  AS `weight`, L.`requiredamount` AS `p_amount`, '' AS `initial1`, 
	IFNULL(LT_R.`amount`,0) AS `r_amount`, IFNULL(LT_R.`ddate`,'') AS `r_date`, 	'' AS `initial2`,
	IFNULL(C_STK.pw,0) AS `cus_stock`	,  L.`fmintrest` AS `p_interest` 
	, LT_R.`redeem_int` AS `r_interest`  ,L.`cus_serno`
FROM `t_loan_re_fo` L
	LEFT JOIN `t_loantranse_re_fo` LT_P 	ON LT_P.`billno` = L.`billno` AND LT_P.`loanno` = L.`loanno` AND LT_P.`transecode` = 'P'
	LEFT JOIN `t_loantranse_re_fo` LT_R 	ON LT_P.`billno` = L.`billno` AND LT_R.`loanno` = L.`loanno` AND LT_R.`transecode` = 'R'
	JOIN m_customer 	C 	ON L.`cus_serno` = C.`serno` 
	JOIN `t_loanitems_re_fo`	LI 	ON L.`loanno` = LI.`loanno`
	JOIN r_items 		I 	ON LI.`itemcode` = I.`itemcode` 
 	LEFT JOIN (SELECT t.`cus_serno` , SUM( t.`totalweight`) AS PW FROM `t_loan` t WHERE t.`status`='P'  GROUP BY t.`cus_serno`) 	C_STK ON L.`cus_serno` = C_STK.cus_serno

 WHERE $QBC L.`ddate` <= '$td' $billtype 
 GROUP BY L.`billtype` , L.`billno` 
 
 ) a ORDER BY a.p_date");
		

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;
			$r_data['barnch']= $this->db->select("name")->where("bc",$bc)->get("m_branches")->row()->name;

			$this->load->view($_POST['by'].'_'.'pdf',$r_data);
		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}