<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pawn_ticket extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report($loanno,$fullmonth_int = 0){

        /*var_dump($_POST);
        exit;*/        
        $bc = $this->sd['bc'];        
        $data1 = $this->db->query("SELECT CONCAT(C.`title`,C.`cusname`) AS `cusname`,C.`address`,C.`nicno`,ifnull(C.`mobile`,'Not added') as `mobile`,L.`cus_serno`,L.`bc`,L.`ddate`,L.`billtype`,L.`bt_letter`,L.`billno`,L.`loanno`,L.`action_date`,L.`requiredamount` AS `requiredamount`,L.`fmintrate`,L.`fmintrate2`,L.`finaldate`,L.`period`,L.`stamp_fee`,L.`billtypeno`, L.int_discount , L.int_paid_untill, L.old_o_new_billno, L.increased_amount, L.fmintrest, L.goldvalue, L.`old_bill_redeem_int` , L.`manual_billno` , L.`is_renew`, L.reduced_amount,L.time , L.is_non_gold FROM `t_loan` L JOIN `m_customer` C ON L.cus_serno = C.serno WHERE loanno = '$loanno' AND L.`bc` = '$bc' Limit 1 ");      


        if($data1->num_rows()>0){
            $data2 = $this->db->query("SELECT CONCAT(LI.`bulk_items`, ' ', I.`itemname`) AS `itemcode`,IC.`des` AS `cat_code`,C.`des` AS `con`,LI.goldweight,LI.pure_weight, LI.qty,GR.`goldcatagory` AS `goldtype`,GQ.`code` AS `quality`,LI.goldvalue,LI.value, GR.`printval` AS gold_content FROM `t_loanitems` LI JOIN `r_items` I ON LI.`itemcode` = I.`itemcode` JOIN `r_itemcategory` IC ON LI.`cat_code` = IC.`code` JOIN `r_condition` C ON LI.`con` = C.`code` LEFT JOIN `r_gold_rate` GR ON LI.`goldtype` = GR.`id` JOIN `r_gold_quality` GQ ON LI.`quality` = GQ.`rate` WHERE loanno = '$loanno' AND LI.`bc` = '$bc'");
            
            $data3 = $this->db->query("SELECT CONCAT(LI.`bulk_items`, ' ', I.`itemname`) AS `itemcode`,IC.`des` AS `cat_code`,C.`des` AS `con`,LI.goldweight,LI.pure_weight , LI.qty,LI.goldvalue,LI.value,
                                        LI.`elec_model`,LI.`elec_imei`,LI.`elec_serno`,LI.`elec_description`,
                                        LI.`veh_model`,LI.`veh_engine_no`,LI.`veh_chassis_no`,LI.`veh_description`
                                        FROM `t_loanitems` LI JOIN `r_items` I ON LI.`itemcode` = I.`itemcode` JOIN `r_itemcategory` IC ON LI.`cat_code` = IC.`code` JOIN `r_condition` C ON LI.`con` = C.`code` WHERE loanno = '$loanno' AND LI.`bc` = '$bc'");
            $r_detail['R']  =   $data1->row();
            $r_detail['RR'] =   $data2->result();
            $r_detail['RR3'] =   $data3->result();
            $r_detail['is_reprint'] =   $_POST['is_reprint'];           

            $data3 = $this->db->query("SELECT LT.`ddate`,LT.`transecode`,LT.`amount`,LT.`discount`,LT.`action_date`, LT.`transeno` FROM `t_loantranse` LT WHERE LT.`loanno` = '$loanno' AND LT.bc = '$bc' AND LT.`transecode`  = 'A' LIMIT 1");

            $pbn = $data1->row()->old_o_new_billno;
            $data5 = $this->db->query("SELECT L.`requiredamount` as `old_bill_value` FROM `t_loan_re_fo` L WHERE L.billno = '$pbn' AND L.bc = '$bc' LIMIT 1");
            
            if ($data5->num_rows() > 0){
                $r_detail['old_bill_value'] =   $data5->row()->old_bill_value;
            }else{
                $r_detail['old_bill_value'] = 0;
            }
            // ------ new modification -----------------------2016-06-12-------------------------------------------------------------
                
                if (isset($_POST['is_tfr'])){
                    
                    $tr_no = $_POST['redeem_int_tr_no'];
                    $data4 = $this->db->query("SELECT LT.`ddate`,LT.`transecode`,LT.`amount`,LT.`discount`,LT.`action_date`,LT.`billno` as `old_billno` FROM t_loantranse_re_fo LT WHERE LT.`transeno` = '$tr_no' AND LT.bc = '$bc' AND LT.`transecode` = 'A' LIMIT 1");
                    $r_detail['IIII'] =   $data4->row();
                    $_POST['is_tfr'] = 1;
                }else{
                    $_POST['is_tfr'] = 0;
                }

            // ------End new modification -----------------------2016-06-12----------------------------------------------------------

            $r_detail['III'] =   $data3->row();
            $r_detail['fullmonth_int'] = $fullmonth_int;
            $r_detail['is_tfr'] = $_POST['is_tfr'];
            $r_detail['bc'] = $this->sd['bc'];
            //user_des
            $r_detail['user_des'] = $this->sd['user_des'];
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}