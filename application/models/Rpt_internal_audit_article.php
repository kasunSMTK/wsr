<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_internal_audit_article extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){
        $a['max_no'] = 1;
		return $a;
    }    

    public function Excel_report($_POST_){
        $_POST_ = $_POST;
        $this->PDF_report($_POST_,"excel");
    }

    public function PDF_report($_POST_,$st="pdf"){

        ini_set('max_execution_time', 300); //300 seconds = 5 minutes

        $fd = $_POST_['from_date'];
        $td = $_POST_['to_date'];

        if (isset($_POST['bc'])){ 
            if ( $_POST['bc'] == "" ){
                $BC_Q = "";
                $bc = " Multiple Branches";
            }else{
                $bc     = $_POST['bc'];
                $BC_Q   = " AND  L.`bc` = '$bc' ";    
            }

        }else{            
            $bc     = $this->sd['bc'];
            $BC_Q   = " AND  L.`bc` = '$bc' ";
        }


        if(!empty($_POST['r_billtype'])){
            
            $bill = " AND L.billtype='".$_POST['r_billtype']."'";

        }else{
            $bill ="";
        }     


        if (isset($_POST['chk_au']) && isset($_POST['chk_unau'])){
            $type = '';
        }else{

            if ( isset($_POST['chk_au']) ){
                $type = ' AND NOT L.audit = 0 ';
            }else if( isset($_POST['chk_unau']) ){
                $type = ' AND L.audit = 0 ';
            }else{
                $type = '';
            }

        }  

        

        // inner loop
        $Q  = $this->db->query("SELECT    
                L.billtype,
                L.`ddate`,
                L.`billno`,
                concat(R.`itemname`,' - ',C.des, '(', I.`qty`  ,')') as `itemname`,
                I.`pure_weight`,
                I.`goldweight`,
                G.`goldcatagory`,
                L.`requiredamount`,
                'Audit Comment' AS `audit_comment`,
                B.`name` AS `bc_name`,
                B.bc,
                L.totalweight,
                I.audit_comment,

                I.audit_goldtype,
                if(I.audit_goldweight=0,'',I.audit_goldweight) as `audit_goldweight`,
                if(I.audit_pure_weight=0,'',I.audit_pure_weight) as `audit_pure_weight`,
                if(I.audit_overadvance=0,'',I.audit_overadvance) as `audit_overadvance`
                
                FROM `t_loan` L 
                INNER JOIN `t_loanitems` I ON (I.`loanno` = L.`loanno`) 
                INNER JOIN `r_items` R ON (R.`itemcode` = I.`itemcode`) 
                INNER JOIN `r_gold_rate` G ON (I.`goldtype` = G.`id`) 
                INNER JOIN `r_condition` C ON (I.`con` = C.`code`) 
                LEFT JOIN `m_branches` B ON L.`bc` = B.`bc`

                WHERE L.`ddate` <= '$td'

                $BC_Q $bill $type

                ORDER BY L.`bc`,L.`billtype`,L.billno ");


        // get bc list to an array
        foreach ($Q->result() as $rr) { 
            $bc_arr[] = $rr->bc; 
        }   $bc_arr = array_values( array_flip( array_flip( $bc_arr ) ) );

        // get billno list to an array
        foreach ($Q->result() as $rrr) { 
            $bn_arr[] = $rrr->billno; 
        }   $bn_arr = array_values( array_flip( array_flip( $bn_arr ) ) );


        if($Q->num_rows() > 0){            
            $r_data['list'] = $Q->result();
            $r_data['fd'] = $fd;
            $r_data['td'] = $td;
            $r_data['bc'] = $bc;
            $r_data['bc_arr'] = $bc_arr;
            $r_data['bn_arr'] = $bn_arr;
            $r_data['bc_name'] = $this->db->where("bc",$bc)->limit(1)->get('m_branches')->row()->name;

            if ($st == "pdf"){
                $this->load->view($_POST['by'].'_'.'pdf',$r_data);
            }else{
                $this->load->view($_POST['by'].'_'.'excel',$r_data);
            }

        }else{
            echo "<script>location='default_pdf_error'</script>";
        }

    }
    
}