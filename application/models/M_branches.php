<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_branches extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){		
		$a['aaaS'] = 1;
        //$a['bc_max'] = $this->getBcMax();

        $this->load->model('m_area');
        $a['area'] = $this->m_area->getArea();
        
        $a['acc1']=$this->getAcc('cash_acc');
        $a['acc2']=$this->getAcc('stock_acc');
        $a['acc3']=$this->getAcc('interest_acc');

        $a['load_list'] = $this->load_list();
        $a['default_max_min_cash_balance'] = $this->default_max_min_cash_balance();
        $a['bc_no'] = $this->db->query("SELECT LPAD(IFNULL(MAX(CONVERT(bc_no, SIGNED INTEGER))+1,1),3,0) AS `bc_no` FROM `m_branches`")->row()->bc_no;
		return $a;
    }

    public function default_max_min_cash_balance(){
        $bc = $this->sd['bc'];
        return $this->db->query("SELECT IF (b.`bc_max_cash_bal` = 0, d.`bc_max_cash_bal` ,b.`bc_max_cash_bal`) AS `bc_max_cash_bal`, IF (b.`bc_min_cash_bal` = 0, d.`bc_min_cash_bal` ,b.`bc_min_cash_bal`) AS `bc_min_cash_bal` FROM `m_branches` b JOIN `m_pawning_values` d WHERE bc = '$bc'")->row();
    }
    
    public function save(){


        if (isset($_POST['other_bc_name_allow'])){
            $_POST['other_bc_name_allow'] = 1;
        }else{
            $_POST['other_bc_name_allow'] = 0;
        }

        if (isset($_POST['close_bc'])){
            $_POST['close_bc'] = 1;
        }else{
            $_POST['close_bc'] = 0;
        }

        if ( strtolower($_POST['bc']) == strtolower("all") ){
            $a['s'] = 0; 
            $a['n'] = 0; 
            $a['m'] = "Invalid branch code";
        }else{
            if ($_POST['hid'] == "0"){
                unset($_POST['hid']);
                
                if ($this->db->insert("m_branches",$_POST)) {                
                     $a['s'] = 1;
                    }
                else {
                    $a['s'] = 0; 
                    $a['n'] = $this->db->conn_id->errno; 
                    $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Branch");
                }

            }else{
                $hid = $_POST['hid'];
                unset($_POST['hid']);
                $this->db->where("bc",$hid)->update("m_branches",$_POST);
                $a['s'] = 1;  
            }
        }

        echo json_encode($a);
    }

    public function autocomplete(){               
        $q = $_GET['term'];
        $query = $this->db->query("SELECT * FROM m_branches WHERE (bc like '%$q%' OR name like '%$q%' ) ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->bc . " - ". $r->name ; }    
        echo json_encode($ary);
    }
    
    
    public function load(){
	   echo $this->load_list();
    }
    
    public function delete(){
	   $code = $_POST['code'];

       if ($this->db->query("DELETE FROM`m_branches` WHERE bc = '$code' ")){
             $a['s'] = 1;

        }else {

            $a['s'] = 0; 
            $a['n'] = $this->db->conn_id->errno; 
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Branch");
        }
        echo json_encode($a);
    }

    public function load_list(){
        $Q = $this->db->query("SELECT B.*  , IFNULL( B.`pawn_start_date`, 'not found') AS `pawn_start_date`

FROM `m_branches` B

ORDER BY B.bc ");


        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){

                $T .= "<tr><td class='Des'>".$R->bc . " - " .$R->name. "<br>" . $R->address . "<br><span style='color:#666666;font-size:12px'>Pawning Start Date : ".$R->pawn_start_date."</span></td><td class='ED'><a href=# onClick=setEdit('".$R->bc."')>Edit</a> | <a href=# onClick=setDelete('".$R->bc."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No branches found</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;
        $a['max_no'] = $this->getBcMax();
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $code = $_POST['code'];
        $R = $this->db->query("SELECT b.*, IF (b.`bc_max_cash_bal` = 0, d.`bc_max_cash_bal` ,b.`bc_max_cash_bal`) AS `bc_max_cash_bal_`, IF (b.`bc_min_cash_bal` = 0, d.`bc_min_cash_bal` ,b.`bc_min_cash_bal`) AS `bc_min_cash_bal_` FROM `m_branches` b JOIN `m_pawning_values` d WHERE bc = '$code' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `m_branches` WHERE (bc like '%$k%' OR name like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){

                $T .= "<tr><td class='Des'>".$R->bc . " - " .$R->name. "<br>" . $R->address . "</td><td class='ED'><a href=# onClick=setEdit('".$R->bc."')>Edit</a> | <a href=# onClick=setDelete('".$R->bc."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No branches found</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }

    public function getAcc($n){
        $Q = $this->db->query("SELECT `code`,`description` FROM `m_account` WHERE `is_control_acc`=0");
        if ($Q->num_rows() > 0){
            $T = "<SELECT id='$n' name='$n'  class='input_ui_dropdown' style='width:390px'><option value=''>Select Account</option>";
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->code."'>".$R->code." - ".$R->description."</option>";
            }
            $T .= "</SELECT>";
            return $T;
        }else{
            return $T;
        }
    } 

    public function getBcMax(){
        $max_no = $this->db->query("SELECT IFNULL(MAX(CONVERT(bc, SIGNED INTEGER))+1,1) AS `max_no` FROM `m_branches` ")->row()->max_no;
        if (strlen($max_no) == 1){ $max_no = "00".$max_no; }
        if (strlen($max_no) == 2){ $max_no = "0".$max_no; }
        if (strlen($max_no) == 3){ $max_no = $max_no; }
        return $max_no;
    }

    public function select($name="bc"){

        $Q = $this->db->query("SELECT * FROM `m_branches` ORDER BY bc");

        if ($Q->num_rows() > 0){

            $T = "<SELECT id='bc' name='$name'  class='input_ui_dropdown v_bc'><option value=''>Select Branch</option>";

            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->bc."'>".$R->bc." - ".$R->name."</option>";
            }

            $T .= "</SELECT>";

            return $T;
        }else{
            return $T;
        }

    }

    public function select_own_bc(){

        if ($this->sd['isAdmin'] == 0){
            $bc = " WHERE bc = '".$this->sd['bc']."'";
        }else{
            $bc = "";            
        }

        $Q = $this->db->query("SELECT * FROM `m_branches` $bc ORDER BY bc");


        if ($Q->num_rows() == 1){
            $T = "<SELECT id='bc' name='bc'  class='input_ui_dropdown v_bc input_text_regular_new_pawn' style='border:2px solid Green; padding:3px'>";
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->bc."'>".$R->bc." - ".$R->name."</option>";
            }
            $T .= "</SELECT>";
            return $T;
        }elseif($Q->num_rows() > 1){
            $T = "<SELECT id='bc' name='bc'  class='input_ui_dropdown v_bc input_text_regular_new_pawn' style='border:2px solid Green; padding:3px'><option value=''>Select Branch</option>";

            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->bc."'>".$R->bc." - ".$R->name."</option>";
            }

            $T .= "</SELECT>";
            return $T;
        }else{
            return "Branch Not found";
        }



        /*if ($Q->num_rows() == 1){

            $T = "<SELECT id='bc' name='bc'  class='input_ui_dropdown v_bc'>";
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->bc."'>".$R->bc." - ".$R->name."</option>";
            }
            $T .= "</SELECT>";
            return $T;

        }elseif($Q->num_rows() > 1){

            $T = "<SELECT id='bc' name='bc'  class='input_ui_dropdown v_bc'><option value=''>Select Branch</option>";

            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->bc."'>".$R->bc." - ".$R->name."</option>";
            }

            $T .= "</SELECT>";

            return $T;
        }else{
            return $T;
        } */

    }


    public function bc_dropdown_multi(){

        if ($this->sd['isAdmin'] == 0){
            $bc = " WHERE bc = '".$this->sd['bc']."' /*AND  close_bc = 0*/ ";
        }else{
            $bc = " /*WHERE close_bc = 0*/ ";            
        }

        $Q = $this->db->query("SELECT * FROM `m_branches` $bc  ORDER BY bc");

        $T = '';

        if ($Q->num_rows() == 1){
            
            $R = $Q->first_row();
                $T .= '<li><lable><input class="bc_each" checked="checked" type="checkbox" value="'.$R->bc.'" title="'.$R->bc.'" />'.$R->bc.' - '.$R->name.'</lable><li>';
            

        }else if ($Q->num_rows() > 1){
                $T .= '<li><lable><input type="checkbox" value="" class="all_bc_select" />All Branches</lable><li>';
            foreach ($Q->result() as $R) {
                $T .= '<li><lable><input type="checkbox" title="'.$R->bc.'" class="bc_each" value="'.$R->bc.'" />'.$R->bc.' - '.$R->name.'</lable><li>';
            }

        }else{
            $T .= '<li><input type="checkbox" value="000" />No Branches Found<li>';
        }


        return $T;
    }


    public function is_bc_user(){

        if ($this->sd['isAdmin'] == 0){
            $bc = $this->sd['bc'];
            $t  = '<span class="hida">'.$bc.'</span>';
            $t .= '<input name="bc_arry[]" value="'.$bc.'" title="'.$bc.'" type="hidden">';

            return $t;
            
        }

    }



}