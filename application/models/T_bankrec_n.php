<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_bankrec_n extends CI_Model {

    private $sd;    
    private $max_no;

    function __construct(){
        parent::__construct();
        $this->sd = $this->session->all_userdata(); 
    }

    public function base_details(){
        
        $a['no'] = $this->get_max(); 
        
        return $a;

    }

    public function get_max(){

        return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM t_bank_reconcil_sum")->row()->max_no;

    }

    public function get_acc_list(){
        $q      = $_GET['term'];
        $query  = $this->db->query("    SELECT MA.`code`,MA.`description` 
                                        FROM m_account MA 
                                        WHERE MA.is_bank_acc='1' AND MA.`is_control_acc` = 0 AND (MA.`code` LIKE '%$q%' OR MA.`description` LIKE '%$q%') LIMIT 200 ");
        $ary    = array();    
        
        foreach($query->result() as $R){ 
            $ary[] = $R->code." - ".$R->description; 
        }
        
        echo json_encode($ary);
    }


    public function get_acc_entries(){

        $fd         = $_POST['fd'];
        $td         = $_POST['td'];
        $acc_code   = $_POST['acc_code'];
        $CLRec_date = $this->db->query("SELECT IF('$fd'<=MAX(to_date),MAX(to_date),0) AS `tde` FROM `t_bank_reconcil_sum` WHERE acc_code = '$acc_code'");

        if ($CLRec_date->row()->tde != 0){
            $a['s'] = 2;
            $a['last_rec_date'] = $CLRec_date->row()->tde;
            echo json_encode($a);
            exit;
        }

        $Q = $this->db->query("    SELECT t_account_trans.* , t_trans_code.`description` AS `trans_desc` , ifnull(concat(' - ',V.cheque_no),'') as `cheque_no`,
                                            

    CONCAT( 

    IFNULL(t_account_trans.description,''),' ',  IFNULL(CONCAT('- Chq no:',cis.`cheque_no`),'') , '-' , IFNULL(ma2.`description`,'') ,
    IFNULL(CONCAT(' : Class BC : ',mb2.`name`),' '), 
    IFNULL(CONCAT(ft.ref_no,' (', ft.to_acc,')', ma.`description` , ' : (' , ft.`from_acc`,')',ma3.`description` ) , '' ), 
    IFNULL(CONCAT(' | Cash Supplier: ', vgs.`payee_info`),'') , if(t_account_trans.reconcile=1,' - RECONCILED*','') ,' - ',
    IFNULL(V.`note`,'')


    ) AS `description`




FROM `t_account_trans`

        
        JOIN t_trans_code ON t_account_trans.`trans_code` = t_trans_code.`code`

        LEFT JOIN (     SELECT v.`bc`,v.`nno`,ci.`cheque_no`,v.`ref_no`, v.`note`
                        FROM `t_voucher_gl_sum` v 
                        JOIN `t_cheque_issued` ci ON v.`bc` = ci.bc AND v.`nno` = ci.`trans_no` AND ci.`trans_code` = 47
                        WHERE v.`type` = 'cheque' ) V ON t_account_trans.`bc` = V.bc AND t_account_trans.`trans_no` = V.nno

    

    LEFT JOIN m_branches mb ON mb.`bc`=t_account_trans.`bc`
    LEFT JOIN m_branches mb2 ON mb2.bc =t_account_trans.`v_bc`
    LEFT JOIN `t_fund_transfer_sum` ft ON t_account_trans.trans_code = 9 AND t_account_trans.trans_no = ft.no
    LEFT JOIN m_account ma ON ft.`to_acc` = ma.`code`
    LEFT JOIN m_account ma3 ON ft.`from_acc` = ma3.`code`
    
    LEFT JOIN t_cheque_issued cis ON t_account_trans.`trans_code` = cis.`trans_code` AND t_account_trans.`trans_no` = cis.`trans_no` AND t_account_trans.`bc` = cis.`bc`
    LEFT JOIN m_account ma2 ON cis.`account` = ma2.`code`
    LEFT JOIN t_voucher_gl_sum vgs ON t_account_trans.`bc` = vgs.`bc` AND t_account_trans.`trans_code` = 48 AND t_account_trans.`trans_no` = vgs.`nno` AND vgs.type = 'cash'

    WHERE `reconcile`=0 AND t_account_trans.ddate <= '$td' AND acc_code='$acc_code' ");





        $a['op_bal']= $this->db->query("    SELECT IFNULL(SUM(dr_amount - cr_amount),0) AS `op_bal` FROM t_account_trans WHERE ddate < '$fd' AND acc_code = '".$_POST['acc_code']."' AND reconcile = 1")->row()->op_bal;

        $dr = $cr = $a['count'] = $a['dr'] = $a['cr'] = 0;

        if ($Q->num_rows() > 0){

            foreach ($Q->result() as $r) {
                $dr += $r->dr_amount;
                $cr += $r->cr_amount;
            }

            $a['dr']    = $dr;
            $a['cr']    = $cr;
            $a['det']   = $Q->result();
            $a['count']   = $Q->num_rows();

        }
            

        $a['s'] = 1;

        echo json_encode($a);

    }



    public function save(){
        
        $_POST['bc'] = $this->sd['bc'];
        $_POST['cl'] = $this->sd['cl'];
        $_POST['oc'] = $this->sd['oc'];

        $this->db->trans_begin();        

        if ( $_POST['code_'] == 0 ){
            
            $_POST['no'] = $this->get_max();

            $ar_trans_no = 0;
            
            if (isset($_POST['ar_auto_no'])){
                $ar_auto_no     = $_POST['ar_auto_no'];
                $ar_bc          = $_POST['ar_bc'];
                $ar_trans_date  = $_POST['ar_trans_date'];
                $ar_trans_code  = $_POST['ar_trans_code'];
                $ar_trans_no    = $_POST['ar_trans_no'];
                $ar_description = $_POST['ar_description'];
                $ar_dr          = $_POST['ar_dr'];
                $ar_cr          = $_POST['ar_cr'];
                $ar_chk         = $_POST['ar_chk'];
            }

            $checked_item_count = 0;

            if (isset($ar_chk)){

                foreach ($ar_chk as $checked_item) {
                    
                    if ($checked_item == 1){
                        $checked_item_count++;
                    }

                }

            }

            unset($_POST['code_'],$_POST['ar_auto_no'],$_POST['ar_bc'],$_POST['ar_trans_date'], $_POST['ar_trans_code'], $_POST['ar_trans_no'], $_POST['ar_description'], $_POST['ar_dr'], $_POST['ar_cr'], $_POST['ar_chk']);
            
            $nnn = 0;

            if (isset($ar_chk)){
            
                for($n = 0 ; $n < count($ar_trans_no) ; $n++){

                    $is_reconcil = $ar_chk[$n];

                    $rec_det[] = array(
                        
                        "auto_no" => $ar_auto_no[$n],
                        "no" => $_POST['no'],
                        "cl" => $_POST['cl'],
                        "bc" => $ar_bc[$n],
                        "trans_date" => $ar_trans_date[$n],
                        "trans_code" => $ar_trans_code[$n],
                        "trans_no" => $ar_trans_no[$n],
                        "description" => $ar_description[$n],
                        "dr" => $ar_dr[$n],
                        "cr" => $ar_cr[$n],
                        "is_reconcil" => $is_reconcil
                    );


                    if ($is_reconcil == 1){

                        $qqq = $this->db->query("SELECT auto_no FROM  t_account_trans WHERE   auto_no = '".$ar_auto_no[$n]."' AND
                                                    bc = '".$ar_bc[$n]."' AND 
                                                    acc_code = '".$_POST['acc_code']."' AND 
                                                    trans_code = '".$ar_trans_code[$n]."' AND 
                                                    trans_no = '".$ar_trans_no[$n]."' AND 
                                                    dr_amount = '".$ar_dr[$n]."' AND 
                                                    cr_amount = '".$ar_cr[$n]."'

                                            LIMIT 1");

                        if ($qqq->num_rows() > 0){
                            
                            $nnn++;
                            $this->db->query("  UPDATE t_account_trans 
                                            SET `reconcile` = 1 , `bank_rec_no` = '".$_POST['no']."' 
                                            
                                            WHERE   auto_no = '".$ar_auto_no[$n]."' AND
                                                    bc = '".$ar_bc[$n]."' AND 
                                                    acc_code = '".$_POST['acc_code']."' AND 
                                                    trans_code = '".$ar_trans_code[$n]."' AND 
                                                    trans_no = '".$ar_trans_no[$n]."' AND 
                                                    dr_amount = '".$ar_dr[$n]."' AND 
                                                    cr_amount = '".$ar_cr[$n]."'

                                            LIMIT 1 ");
                        }
                    }

                }

            }
            

            $this->db->insert("t_bank_reconcil_sum",$_POST);
            
            if (isset($rec_det)){
                $this->db->insert_batch("t_bank_reconcil_det",$rec_det);
            }

        }else{
            // if update is available
        }

        if ($checked_item_count != $nnn){
            $this->db->trans_rollback();
            $a['m'] = "Updated entry count not matched with selected entry count. Please count IT department. ";
            $a['s'] = "error";
            $a['s'] = 3;
        }else{
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $a['s'] = "error";
                $a['s'] = 0;
            }else{
                $this->db->trans_commit();
                $a['s'] = 1;
                $a['next_no'] = $this->get_max();
            }
        }

        echo json_encode($a);

    }

    public function load_data(){

        $no = $_POST['no'];
        $Q  = $this->db->query("SELECT t_bank_reconcil_sum.*,m_account.`description` AS `acc_desc` 
                                FROM `t_bank_reconcil_sum` 
                                JOIN m_account ON t_bank_reconcil_sum.`acc_code` = m_account.`code`
                                WHERE `no` = '$no' LIMIT 1 ");

        if ( $Q->num_rows() > 0 ){

            $a['s'] = 1;
            $a['sum'] = $Q->row();
            $a['det'] = $this->db->query("  SELECT t_bank_reconcil_det.*, t_trans_code.`description` AS `trans_desc` 
                                            FROM `t_bank_reconcil_det` 
                                            JOIN t_trans_code ON t_bank_reconcil_det.`trans_code` = t_trans_code.`code`
                                            WHERE `no` = '$no' ")->result();

        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }

    public function PDF_report(){

        $no = $_POST['no'];        
        $q  = $this->db->query("SELECT BRS.* , MA.`description` AS `bank_acc_name` FROM `t_bank_reconcil_sum` BRS

JOIN m_account MA ON BRS.`acc_code` = MA.`code`

WHERE NO = '$no' LIMIT 1 ");

        if ($q->num_rows() > 0){



            $q_rows = $this->db->query("SELECT BRD.* , IFNULL(V.cheque_no,'') AS `cheque_no` , V.ref_no , concat(BRD.description,' - ', ifnull(V.`note`,'')) as `description`
                        FROM `t_bank_reconcil_det` BRD
                        LEFT JOIN (     SELECT v.`bc`,v.`nno`,ci.`cheque_no`,v.`ref_no`,v.`note`
                                FROM `t_voucher_gl_sum` v 
                                JOIN `t_cheque_issued` ci ON v.`bc` = ci.bc AND v.`nno` = ci.`trans_no` AND ci.`trans_code` = 47
                                WHERE v.`type` = 'cheque'
                              ) V ON BRD.`bc` = V.bc AND BRD.`trans_no` = V.nno

                        WHERE BRD.`no` = '$no' AND BRD.`is_reconcil` = 0 ");



            $r_detail['cash_book_bal'] = $this->db->query("SELECT ifnull(SUM(dr_amount - cr_amount),0) AS `acc_bal` FROM t_account_trans WHERE ddate <= '".$q->row()->to_date."' AND acc_code = '".$q->row()->acc_code."'")->row()->acc_bal;
            $r_detail['rec_date'] = $q->row()->date;
            $r_detail['rec_from_date'] = $q->row()->from_date;
            $r_detail['rec_to_date'] = $q->row()->to_date;
            $r_detail['bank_acc_name'] = $q->row()->bank_acc_name;
            $r_detail['add_less_rows'] = $q_rows->result();
            $r_detail['statment_closing_bal'] = $q->row()->statment_closing_bal;
            $r_detail['sum'] = $q->result();




            $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);

        }else{

            echo "No data found";

        }

    }


    public function get_rec_trans_nos(){

        $acc_code = $_POST['acc_code'];

        $q = $this->db->query("SELECT no , DATE_FORMAT(from_date,'%Y-%m') as m FROM t_bank_reconcil_sum WHERE acc_code = '$acc_code' ");

        if ($q->num_rows() > 0){
            $a['nos'] = $q->result();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }
    


}