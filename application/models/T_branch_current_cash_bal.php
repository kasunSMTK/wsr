<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class T_branch_current_cash_bal extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		  parent::__construct();		
		  $this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){
        $a['get_branch_cash_bal'] = $this->get_branch_cash_bal();
		return $a;
    }


    public function get_branch_cash_bal(){

        $Q = $this->db->query("SELECT A.bc,SUM(dr_amount) - SUM(cr_amount) AS bal , B.name FROM t_account_trans A JOIN m_branches B ON A.bc = B.bc WHERE B.`close_bc` = 0 AND acc_code='303003' AND ddate <= CURRENT_DATE GROUP BY A.bc ");

        
        $t  = '<table align="center" class="bc_cash_bal">';
        
        $t .= '<tr>';
        $t .= '<td>Code</td>';
        $t .= '<td>Branch</td>';
        $t .= '<td align="right">Amount</td>';
        $t .= '</tr>';

        $total = 0;


        foreach ($Q->result() as $r) {
            
            $t .= '<tr>';
            $t .= '<td>'.$r->bc.'</td>';
            $t .= '<td>'.$r->name.'</td>';
            $t .= '<td align="right"><input type="text" class="bc_bal_amount" value="'.number_format($r->bal,2).'" readonly="readonly"></td>';
            $t .= '</tr>';
            
            $total += $r->bal;            

        }


            $t .= '<tr style="border-top:1px solid #cccccc;">';
            $t .= '<td></td>';
            $t .= '<td style="font-size:32px">Total</td>';
            $t .= '<td align="right" style="font-size:32px">'.number_format($total,2).'</td>';            
            $t .= '</tr>';


        $t .= '</table>';





        return $t;

    }

}