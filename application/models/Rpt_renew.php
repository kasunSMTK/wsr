<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class rpt_renew extends CI_Model {

    

    private $sd;    

    

    function __construct(){

        parent::__construct();      

        $this->sd = $this->session->all_userdata();     

    }

    

    public function base_details(){     

        $a['max_no'] = 1;        

        return $a;

    }   



    public function PDF_report($_POST_,$r_type = "pdf"){



        ini_set('max_execution_time', 600 );



        if (isset($_POST['bc_n'])){

        if ( $_POST['bc_n'] == "" ){        

            $bc = "";

        }        

    }else{



        if ( !is_array($_POST['bc_arry']) ){

            

            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);

            

            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                

                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";

            }



            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];

        }



        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            

            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";

        }



        $bc = implode(',', $bc_ar);



        if ($bc == ""){

            $bc   = "";

        }else{

            $bc   = "   LT.bc IN ($bc) AND ";

        }

    }



        // r_billtype filteration 

        

        $billtype = "";

    

        if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){

            $billtype = " AND LT.`billtype`='".$_POST['r_billtype']."'";

        }



        $fd = $_POST_['from_date'];

        $td = $_POST_['to_date'];



        $Q = $this->db->query("SELECT LT.`bc`,LT.`billtype`,LT.`billno`,LT.`ddate`, IF(LT.`amount`<0, LT.`amount` *-1, LT.`amount`) AS amount,
                        LT.`transecode`,LT.`transeno`,LT.billtypeno,LT.`action_date`,
                        IF (LT.billtypeno != 0 , 'Original Bill Value' , IF (LT.transecode = 'PP', 'Interest Payment' , IF (LT.transecode = 'P' AND LT.amount <=0 , 'Capital Payment' , 'Capital Increase') ) ) AS `transaction`
                FROM  t_loantranse LT
                WHERE LT.transecode != 'PD' AND LT.model_trans_code = 99 AND $bc LT.`ddate` BETWEEN '$fd' AND '$td'
                ORDER BY LT.bc,LT.billtype, LT.billno, LT.`action_date` ASC, LT.`transeno` ASC  ");

        

        if($Q->num_rows() > 0){



            $r_data['list'] = $Q->result();

            $r_data['fd'] = $fd;

            $r_data['td'] = $td;



            if ($r_type == "pdf"){                            

                $this->load->view($_POST['by'].'_'.'pdf',$r_data);

            }else{

                $this->load->view($_POST['by'].'_'.'excel',$r_data);

            }



        }else{

            echo "<script>location='default_pdf_error'</script>";

        }



    }

    



    public function Excel_report()

    {

        $this->PDF_report($_POST,"Excel");

    }



}