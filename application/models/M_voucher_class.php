<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_voucher_class extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'm_voucher_class';        
    }
    
    public function base_details(){
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){

        $_POST['oc'] = $this->sd['oc'];

        if ($_POST['hid'] == "0"){
            
            unset($_POST['hid']);

            if ( $this->db->where("code",$_POST['code'])->select("code")->get('m_voucher_class')->num_rows() < 1 ){

                if ($this->db->insert("m_voucher_class",$_POST)) {                
                    $a['s'] = 1;                
                }else {
                    $a['s'] = 0; 
                    $a['n'] = $this->db->conn_id->errno; 
                    $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Voucher_Class");
                }

            }else{
                $a['s'] = 2;
            }

        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("code",$hid)->update("m_voucher_class",$_POST);
            $a['s']= 1;            
        }
        
        echo json_encode($a);

    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $code = $_POST['code'];
       
       if ($this->db->query("DELETE FROM`m_voucher_class` WHERE code = '$code' ")) {          
            $a['s'] = 1;
        }else{       
            $a['s'] = 0;            
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
        }

        echo json_encode($a);
    }





    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `m_voucher_class` ORDER BY `code` DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No voucher class added</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;        
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $code = $_POST['code'];
        $R = $this->db->query("SELECT * FROM `m_voucher_class` WHERE code = '$code' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `m_voucher_class` WHERE (code like '%$k%' OR description like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }

    public function getArea(){

        $Q = $this->db->query("SELECT * FROM `m_voucher_class` ORDER BY description");

        if ($Q->num_rows() > 0){

            $T = "<SELECT id='area' name='area'  class='input_ui_dropdown'><option value=''>Select area</option>";

            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->code."'>".$R->code." - ".$R->description."</option>";
            }

            $T .= "</SELECT>";

            return $T;
        }else{
            return $T;
        }

    } 

    public function getBcMax(){
        $max_no = $this->db->query("SELECT IFNULL(MAX(CONVERT(bc, SIGNED INTEGER))+1,1) AS `max_no` FROM `m_voucher_class` ")->row()->max_no;
        if (strlen($max_no) == 1){ $max_no = "00".$max_no; }
        if (strlen($max_no) == 2){ $max_no = "0".$max_no; }
        if (strlen($max_no) == 3){ $max_no = $max_no; }
        return $max_no;
    }


    public function select($select_name = "voucher_class"){

        $Q = $this->db->get('m_voucher_class');
        $T = "<select name='$select_name' class='v_class'>";

        if ($Q->num_rows() > 0){
                $T .= "<option value=''>Select Class</option>";
            foreach ($Q->result() as $R) {                
                $T .= "<option value='".$R->code."' acc_cat='".$R->acc_cat."'>".$R->description."</option>";
            }

        }else{
            $T .= "<option value=''>No Voucher Class Added</option>";
        }
        
        $T .= "</select>";

        return $T;

    }

}