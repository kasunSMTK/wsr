<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_tag extends CI_Model {
    
    private $sd;    
    private $max_no;
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();     
    }
    
    public function base_details(){
        $a['current_date'] = $this->sd['current_date']; 
               
        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        $a['bc_no'] = $this->sd['bc_no'];
        $a['backdate_upto'] = $this->sd['backdate_upto'];
        $a['max_no']                = $this->get_max_no();
        $a['current_date']              = $this->sd['current_date'];
        $this->load->model('m_bank');
        $a['store']                     = $this->select_store();
        $a['employee']                     = $this->select_employee();

        return $a;
    }

    public function get_max_no()
    {
        return $this->db->query("SELECT IFNULL(MAX(nno)+1,1) AS `max_no` FROM `t_tag_sum`")->first_row()->max_no;
    
    }

    public function save()
    {
        $oc=$this->sd['oc'];

        $obj = json_decode($_POST['details']);

        //var_dump($obj);exit;

        $tbl1= sizeof($obj->items);

           for($x =0; $x<$tbl1; $x++){

             $t_data[]= array(
                
                  "itemcode"    =>$obj->items[$x]->item_code,
                  "bc"    => $this->sd['bc'],
                  "billno"    =>$obj->items[$x]->billno, 
                  "tag_no"    =>$obj->items[$x]->tagno,
                  "cat_code"    =>$obj->items[$x]->cat_code,
                  "goldweight"    =>$obj->items[$x]->goldweight,  
                  "pure_weight"    =>$obj->items[$x]->pureweight,
                  "qty"    =>$obj->items[$x]->qty,
                  "goldtype" =>$obj->items[$x]->goldtype,
                  "quality" =>$obj->items[$x]->quality,
                  "value" =>$obj->items[$x]->value,
                  "denci_weight" =>$obj->items[$x]->denci_weight,
                  "elec_model" =>$obj->items[$x]->elec_model,
                  "elec_imei" =>$obj->items[$x]->elec_imei,
                  "elec_serno" =>$obj->items[$x]->elec_serno,
                  "elec_description" =>$obj->items[$x]->elec_description,
                  "veh_model" =>$obj->items[$x]->veh_model,
                  "veh_engine_no" =>$obj->items[$x]->veh_engine_no,
                  "veh_chassis_no" =>$obj->items[$x]->veh_chassis_no,
                  "veh_description" =>$obj->items[$x]->veh_description,
                  "loanno" =>$obj->items[$x]->loanno,
                  "goldvalue" =>$obj->items[$x]->goldvalue,
                  "billtype" =>$obj->items[$x]->billtype

                ); 
           }

             $t_data1[]=array(

                "bc"    =>$this->sd['bc'],
                "nno"    =>$obj->items[0]->nno,
                "ddate"    =>$obj->items[0]->ddate,
                "billno"    =>$obj->items[0]->billno,
                "amount"    =>$obj->items[0]->amount,
                "store"    =>$obj->items[0]->store,
                "officer"    =>$obj->items[0]->employee,
                "note"    =>$obj->items[0]->note,
                "oc"    =>$oc
             );

            if(isset($t_data)){
                if(count($t_data))
              {
                    // var_dump($t_data);
                    // exit();
                    $this->db->insert_batch("t_tag_det",$t_data);
                    $this->db->insert_batch("t_tag_sum",$t_data1);
                    
              }
            }

            // $_POST_['billno']=$_POST['billno'];
            // $_POST_['bc']=$this->db->query("SELECT bc FROM m_branches WHERE bc_no='".$_POST['bc_no']."' ")->row()->bc;
 
    }

     public function LOAD_LOAN(){
        $billtype   = $_POST['billtype'];
        $bc = $this->sd['bc'];
        $billno = $_POST['bc_no'].$_POST['billno'];

        $q = $this->db->query(" SELECT * FROM t_tag_sum WHERE bc = '$bc' and is_cancel = 0 and billno='".$billno."'")->result();
        
        //var_dump(count($q));
        if(count($q)>0){
            $this->LOAD_LOAN_TAG();
        }else{
            $this->LOAD_LOAN_NEW();
        }

        //$q1 = $this->db->query("SELECT * FROM t_loan_fo tl  WHERE tl.`billno`='$billno' and bc = '$bc' ");
     }

    public function LOAD_LOAN_NEW(){

        $billtype   = $_POST['billtype'];
        $bc = $this->sd['bc'];
        $billno = $_POST['bc_no'].$_POST['billno'];

        $q="";
        $qr=$this->db->query("SELECT * FROM t_loan_re_fo tl  WHERE tl.`billno`='$billno' AND billno not in (SELECT billno FROM t_tag_sum WHERE bc = '$bc' and is_cancel = 0) and bc = '$bc' ");

        if ($qr->num_rows() > 0)
        {

        $Q1 = $this->db->query("SELECT 
                                L.ori_pwn_date
                                ,L.ddate as  forfeit_date
                                ,L.is_amt_base
                                ,L.fm_int_paid
                                ,L.time
                                ,L.goldvalue
                                ,L.old_bill_age
                                ,L.is_renew
                                ,L.billno
                                ,L.bc
                                ,c.`nicno`
                                ,L.billtype
                                ,L.loanno
                                ,L.ddate
                                ,L.requiredamount
                                ,L.fmintrate
                                ,L.fmintrate2
                                ,L.period
                                ,L.finaldate
                                ,L.status
                                ,L.old_o_new_billno
                                ,L.cus_serno
                                ,C.`customer_id`
                                ,L.`stamp_fee`
                                ,L.int_cal_changed
                                ,L.`am_allow_frst_int`
                                ,(SELECT IF (COUNT(transecode) > 0, transecode ,L.status) 
                                  FROM t_loantranse_re_fo 
                                  WHERE bc = '$bc' AND `billno` = '$billno' AND transecode NOT IN('P','A','ADV','AS','PD','PP')
                                 ) AS `status`
                                ,L.`int_paid_untill`
                                ,L.fmintrest
                                ,is_weelky_int_cal 
                                ,L.billcode
                                ,L.`nmintrate`
                                ,L.cat_code
                                ,L.is_non_gold 
                                ,L.cat_code
                                FROM `t_loan_re_fo` L 
                                JOIN `m_customer` C ON L.`cus_serno` = C.`serno` 
                                JOIN `m_bill_type` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` 
                                WHERE billno = '$billno' AND L.bc = '$bc' $q 
                                LIMIT 1 ");
        
        $Q2 = $this->db->query("SELECT d.*,i.itemcate, i.`itemname`, c.des AS cat_des , cn.des as `condition`,d.bulk_items as des
                                FROM t_loanitems_re_fo AS d 
                                LEFT JOIN `r_items` AS i ON d.itemcode=i.itemcode
                                LEFT JOIN `r_itemcategory` c ON d.`cat_code` = c.`code`
                                left join  r_condition cn on d.con = cn.code
                                WHERE d.billno ='$billno'");

//var_dump( $Q2->result());exit;
        $a['loan_sum'] = $Q1->row();
        $a['loan_det'] = $Q2->result();
        $a['tagged'] = 0;
        echo json_encode($a);
    }
    else
    {
       echo "0";
    }
    }

    public function LOAD_LOAN_TAG(){

        $billtype   = $_POST['billtype'];
        $bc = $this->sd['bc'];
        $billno = $_POST['bc_no'].$_POST['billno'];

        $q="";
        $qr=$this->db->query("SELECT * FROM t_tag_sum tl  WHERE tl.`billno`='$billno' AND bc = '$bc' ");

        if ($qr->num_rows() > 0)
        {
        $Q1 = $this->db->query("SELECT 
                                L.ori_pwn_date
                                ,L.loanno
                                ,L.goldvalue
                                ,L.billtype
                                ,L.ddate as  forfeit_date
                                ,L.fmintrate
                                ,L.period
                                ,L.requiredamount
                                ,L.fmintrest
                                ,c.`nicno`
                                ,L.cat_code
                                ,L.billno
                                ,L.bc
                                ,L.ddate
                                ,L.cus_serno
                                ,C.`customer_id`
                                ,L.billcode
                                ,L.cat_code
                                FROM `t_loan_re_fo` L 
                                JOIN `m_customer` C ON L.`cus_serno` = C.`serno` 
                                JOIN `m_bill_type` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc`  
                                WHERE billno = '$billno' AND L.bc = '$bc' $q 
                                LIMIT 1 ");
        
        $Q2 = $this->db->query("SELECT d.*,i.itemcate, i.`itemname`, c.des AS cat_des , cn.des as `condition` , t.tag_no
                                FROM t_tag_det AS d 
                                inner join (SELECT cat_code,item,loanno, GROUP_CONCAT(tag_no) as tag_no,bc FROM t_tag
                                            group by loanno ) as t 
                                on d.loanno = t.loanno  and d.itemcode = t.item
                                LEFT JOIN `r_items` AS i ON d.itemcode=i.itemcode
                                LEFT JOIN `r_itemcategory` c ON d.`cat_code` = c.`code`
                                left join  r_condition cn on d.con = cn.code
                                WHERE d.billno ='$billno' and D.bc = '$bc'");

        $a['loan_sum'] = $Q1->row();
        $a['loan_det'] = $Q2->result();
        $a['tagged'] = 1;
        echo json_encode($a);
    }
    else
    {
       echo "0";
    }
    }

    public function get_karatage(){
        $query = $this->db->query(" SELECT id, `printval`, `goldrate` FROM `r_gold_rate` ");
        $s = "<select style='width:100%' id='goldtype".$_POST['ind']."' name ='goldtype".$_POST['ind']."' ind =".$_POST['ind']." >";
        $s .= "<option> -- </option>";
        foreach ($query->result() as $r) {
            $s .= "<option value =".$r->id." >".$r->id.'-'.$r->printval.'-'.$r->goldrate."</option>" ;   
        }
        $s .= "</select>";
        echo $s;
    } 

    public function get_cat(){
        $query = $this->db->query("SELECT * FROM `r_itemcategory` WHERE is_non_gold = 0");
        $s = "<select class= 'catg' style='width:100%' id='c_code_".$_POST['ind']."' name ='c_code_".$_POST['ind']."' ind =".$_POST['ind']." >";
        $s .= "<option> -- </option>";
        foreach ($query->result() as $r) {
            $s .= "<option value =".$r->code." >".$r->code.'-'.$r->des."</option>" ;   
        }
        $s .= "</select>";
        echo $s;
    }

    public function get_item(){
        $query = $this->db->query("SELECT * FROM `r_items` WHERE itemcate = '".$_POST['cat']."'");
        $s = "<select style='width:100%' id='i_code_".$_POST['ind']."' name ='i_code_".$_POST['ind']."' ind =".$_POST['ind']." >";
        $s .= "<option> -- </option>";
        foreach ($query->result() as $r) {
            $s .= "<option value =".$r->itemcode." >".$r->itemcode.'-'.$r->itemname."</option>" ;   
        }
        $s .= "</select>";
        echo $s;
    }

    public function get_bill_doc_fees($billno,$bc){
        
        $Q = $this->db->query(" SELECT D.`letter_no`,D.document_charge 
                                FROM `t_remind_letter_det` D 
                                WHERE D.`billno` = '$billno' AND D.`bc` = '$bc' AND D.`ignore_doc_charge` = 0 ");

        if ($Q->num_rows() > 0){
            return $Q->result();
        }else{
            return "";
        }

    }
    
    public function getSavedRedeem(){
        $bc     = $this->sd['bc'];
        $tr_no  = $_POST['tr_no'];        
        $Q1 = $this->db->query("SELECT L.old_bill_age, L.is_renew, L.`bc`,L.`ddate`,L.`billtype`,L.`billno`,L.`loanno`,L.`requiredamount`,L.`fmintrate`, L.`finaldate`,L.`period` , LT.`ddate` AS `redeemed_date`, LT.`amount` AS `redeemed_amount`, LT.discount, LT.redeem_int, LT.`card_amount`,LT.`advance_amount`,LT.`cash_amount`,L.cus_serno FROM `t_loantranse_re_fo` LT JOIN `t_loan_re_fo` L ON LT.`loanno` = L.`loanno` WHERE LT.bc = '$bc' AND LT.transecode = 'R' AND LT.`transeno` = '$tr_no' LIMIT 1");
        
        if ($Q1->num_rows() > 0){
            
            $a['sum'] = $Q1->row();

            $this->load->model("calculate");
            $a['paid_int'] = $this->calculate->paid_interest($a['sum']->loanno);

            $a['pay_option'] = $this->calculate->get_pay_option($bc, $tr_no, $a['sum']->cus_serno, $a['sum']->card_amount, $a['sum']->advance_amount, $a['sum']->cash_amount ,"R");
            
            if ($a['sum']->cash_amount > 0){ $a['pay_option']['cash_amount'] = $a['sum']->cash_amount; }

            $a['s'] = 1;            
        }else{
            $a['s'] = 0;
        }

       echo json_encode($a);

    }

    public function getRedeemTransNo(){
        $bc = $this->sd['bc'];
        return $this->db->query("SELECT IFNULL(MAX(`transeno`)+1,1) AS `max_no` FROM `t_loantranse_re_fo` WHERE bc = '$bc' AND `transecode` = 'R'")->row()->max_no;
    }

    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 2);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',2);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => date('H:i:s'),
            "trans_code" => 2,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        $this->load->model('account');
        $this->account->set_data($config);

        $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');        
        $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');
        $redeem_interest    = $this->utility->get_default_acc('REDEEM_INTEREST');
        $redeem_discount    = $this->utility->get_default_acc('REDEEM_DISCOUNT');        
        $stamp_fee          = $this->utility->get_default_acc('STAMP_FEE');
        $doc_charges        = $this->utility->get_default_acc('POSTAGE_RECEIVABLE'); // must go to 30211 postage receivable 
        $customer_advance   = $this->utility->get_default_acc('ADVANCE_RECEIVED');
        $pawning_interest   = $this->utility->get_default_acc('PAWNING_INTEREST');

        /*echo "cash_book Dr ".(($_POST['loan_amount']+$_POST['redeem_int']+$_POST['doc_fee_tot'])-$_POST['discount'])."<br>";
        echo "UNREDEEM_ARTICLES Cr ".((($_POST['loan_amount'] - $_POST['stamp_fee'])-$_POST['discount']) + $_POST['customer_advance'])."<br>";
        echo "redeem_interest Cr ".$_POST['redeem_int']."<br>";
        echo "redeem_discount Dr ".$_POST['discount']."<br>";
        echo "stamp_fee Cr ".$_POST['stamp_fee']."<br>";
        echo "doc_charges Cr ".$_POST['doc_fee_tot']."<br>";
        echo "customer_advance Dr ".$_POST['customer_advance']."<br>";
        exit;*/

        
        // Dr

        $this->account->set_value2("Redeem value", 
            ($_POST['loan_amount'] + $_POST['redeem_int']) - $_POST['discount']
        , "dr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R");


        if (floatval($_POST['discount']) > 0){
            $this->account->set_value2("Redeem Discount", $_POST['discount'], "dr", $redeem_discount,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"RD");
        }
        
        if (floatval($_POST['customer_advance']) > 0){
            $this->account->set_value2("Customer advance settle - Redeem", $_POST['customer_advance'], "dr", $customer_advance,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"ADV_R");
        }

        

        // Cr

        $this->account->set_value2("Pawning", ((($_POST['loan_amount'] - $_POST['stamp_fee'])-$_POST['discount']) + $_POST['customer_advance']) - $_POST['doc_fee_tot'] , "cr", $pawn_stock,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R");

        if (floatval($_POST['stamp_fee']) > 0){
            $this->account->set_value2("Stamp Fee", $_POST['stamp_fee'], "cr", $stamp_fee,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"SF","","",$this->sd['bc'],"");
        }

        if (floatval(($_POST['redeem_int'] + $_POST['discount'])) > 0){
            $this->account->set_value2("Redeem Interest", ($_POST['redeem_int'] + $_POST['discount']), "cr", $redeem_interest,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"RI");
        }

        if (floatval($_POST['doc_fee_tot']) > 0){
            $this->account->set_value2("Document Fees", $_POST['doc_fee_tot'], "cr", $doc_charges,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"DF","","",$this->sd['bc'],"");
        }


        if ( floatval($_POST['refundable_int']) > 0 ){

            $this->account->set_value2("Pawning interest refund", $_POST['refundable_int'] , "dr",$pawning_interest  ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R","","",$this->sd['bc'],"");
            $this->account->set_value2("Pawning interest refund", $_POST['refundable_int'] , "cr",$pawn_stock        ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R","","",$this->sd['bc'],"");

        }
        

        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='2'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 2);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }
    
    public function select_store($name="store",$id="store",$class=""){
        $q = $this->db->query(" SELECT `code`,`description` FROM `m_store` ORDER BY description ");
        $t  = "<select name='".$name."' id='".$id."' class='".$class."' style='width:250px;'>";

        if ($q->num_rows() > 0 ){            
                $t .= '<option value="">Select</option>';
            foreach($q->result() as $r){
                $t .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }
        }else{
            $t .= '<option value="">No Store added</option>';
        }
        $t .= '</select>';
        return $t;
    }

    public function select_employee($name="employee",$id="employee",$class=""){
        $q = $this->db->query(" SELECT `code`,`name` FROM `m_employee` ORDER BY name ");
        $t  = "<select name='".$name."' id='".$id."' class='".$class."' style='width:250px;'>";

        if ($q->num_rows() > 0 ){            
                $t .= '<option value="">Select</option>';
            foreach($q->result() as $r){
                $t .= '<option value="'.$r->code.'">'.$r->name.'</option>';
            }
        }else{
            $t .= '<option value="">No Employee added</option>';
        }
        $t .= '</select>';
        return $t;
    } 

}