<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class redeem_ticket extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report(){

        $loanno = $_POST['ln'];
        
        $data1 = $this->db->query("SELECT L.bc, CONCAT(C.`title`,C.`cusname`) AS `cusname`,C.`address`,C.`nicno`,C.`mobile`,L.`cus_serno`,L.`bc`,L.`ddate`,L.`billtype`,L.`billno`,L.`loanno`,L.`action_date`,LT.amount as `requiredamount`,L.`fmintrate`,L.`finaldate`,L.`period`,L.billtypeno,L.`totalweight` 
            FROM `t_loan_re_fo` L 
            JOIN `m_customer` C ON L.cus_serno = C.serno  
            JOIN (SELECT billno,bc,SUM(amount) AS `amount` FROM t_loantranse_re_fo WHERE transecode = 'P' GROUP BY billno) LT ON L.`billno` = LT.`billno` AND L.`bc` = LT.`bc`
            WHERE L.loanno = '$loanno' Limit 1");        

        if($data1->num_rows()>0){

            $data2 = $this->db->query("SELECT  dr_amount, cr_amount, description, entry_code FROM t_account_trans WHERE loanno = '$loanno' AND trans_code = 2 LIMIT 100 ");

            $data3 = $this->db->query("SELECT amount as `redeem_amount`,advance_amount,ddate as `redeem_date` , time(action_date) as `time`,SUM(IF(t_loantranse_re_fo.`transecode` = 'RF',t_loantranse_re_fo.`amount`,0)) AS `refundable_interest` FROM `t_loantranse_re_fo` WHERE transecode in ('R','RF') AND loanno = $loanno LIMIT 1");

            $data4 = $this->db->query("SELECT 
                                        SUM(IF(t_loantranse_re_fo.`transecode`='P',t_loantranse_re_fo.`amount`,0)) AS `paid_capital` , 
                                        SUM(IF(t_loantranse_re_fo.`transecode`='PP',t_loantranse_re_fo.`amount`,0)) AS `paid_interest` 
                                        FROM t_loantranse_re_fo 
                                        WHERE loanno = $loanno AND transecode IN ('P','PP') AND amount < 0 ");

            $oC = $this->db->query("SELECT discription FROM u_users WHERE cCode = '".$this->sd['oc']."' LIMIT 1")->row()->discription;
            
            $r_detail['R']          = $data1->row();
            $r_detail['RR']         = $data2->result();
            $r_detail['RRR']        = $data3->row(); 
            
            $r_detail['paid_interest'] = $data4->row()->paid_interest; 
            $r_detail['paid_capital'] = $data4->row()->paid_capital; 

            $r_detail['is_reprint'] = $_POST['is_reprint'];
            $r_detail['occ']        = $oC." (".$this->sd['oc'].")";

        	
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}