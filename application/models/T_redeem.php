<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_redeem extends CI_Model {
    
    private $sd;    
    private $max_no;
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();     
    }
    
    public function base_details(){ 
        $a['current_date'] = $this->sd['current_date']; 
        $a['max_no'] = $this->getRedeemTransNo();        
        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        $a['bc_no'] = $this->sd['bc_no'];
        $a['backdate_upto'] = $this->sd['backdate_upto'];


        //$this->load->model('calculate');
        //$a['int'] = $this->calculate->calculate_interest('1000', 'AAA', '2018-01-01', '2018-03-11', 3 );

        return $a;
    }
    
    public function save(){

        $a['s'] = 33;
        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this redeem bill and you might required re-login to continue";

        if (isset($this->sd['current_date'])){
            if (!$this->utility->is_valid_date($this->sd['current_date'])){
                echo json_encode($a);
                exit;
            }
        }else{            
            echo json_encode($a);
            exit;
        }   $a['s'] = ''; $a['msg_33'] = '';

        if ( floatval( $_POST['discount'] ) > 0 ){
            $this->load->model("approvals");            
            if (!$this->approvals->isDiscountApproved($_POST['loanno'])){
                $a['m'] = "Invalid click";
                echo json_encode($a);
                exit;
            }            
        }

        $_POST['billno'] = $_POST['bc_no'].$_POST['billno'];

        $tr_no        = $this->getRedeemTransNo(); 
        $this->max_no = $tr_no;        
        $this->load->model("calculate");

        
        $discount   = $_POST['discount'];        
        $redeem_int = $_POST['balance'] - $discount;        
        $cus_serno  = $_POST['hid_cus_serno']; 


        if ( floatval($_POST['customer_advance_paying']) > 0 ){
            $_PST['advance_amount']    =    $_POST['customer_advance_paying'];
            $this->calculate->cr_advance_bal($cus_serno,$_PST['advance_amount'],$tr_no,"ADV_R",$_POST['billno']);
        }        

        if ( floatval($_POST['card_amount']) > 0 ){
            $_PST['card_amount']    = $_POST['card_amount'];
            $this->calculate->add_credit_card_entry($cus_serno,$_PST['card_amount'],$tr_no);
        }        

        if ( floatval($_POST['cash_amount']) > 0 ){
            $_PST['cash_amount']    = $_POST['cash_amount'];
        }


        // Redeem entery

        $_POST['billno']   = $_POST['billno'];
        $_PST['loanno']    = $_POST['loanno']; 
        $_PST['bc']        = $this->sd['bc'];
        $_PST['billtype']  = $_POST['billtype']; 
        $_PST['billno']    = $_POST['billno'];
        $_PST['ddate']     = $_POST['ddate']; 
        $_PST['amount']    = $_POST['amount']; 
        $_PST['transecode']= "R"; 
        $_PST['transeno']  = $tr_no; 
        $_PST['discount']  = $_POST['discount']; 
        $_PST['redeem_int']= $redeem_int;
        $_PST['billcode']= $_POST['billcode'];
        
        
        // Interest entery

        $_PT['loanno']    = $_POST['loanno']; 
        $_PT['bc']        = $this->sd['bc']; 
        $_PT['billtype']  = $_POST['billtype']; 
        $_PT['billno']    = $_POST['billno'];
        $_PT['ddate']     = $_POST['ddate']; 
        $_PT['amount']    = $redeem_int; 
        $_PT['transecode']= "RI"; 
        $_PT['transeno']  = $tr_no; 
        $_PT['discount']  = $_POST['discount'] ; 
        $_PT['is_pawn_int']  = 0 ;
        $_PT['billcode']= $_POST['billcode'];

        // advance settlement entery
        
        $_PTS['loanno']    = $_POST['loanno']; 
        $_PTS['bc']        = $this->sd['bc']; 
        $_PTS['billtype']  = $_POST['billtype']; 
        $_PTS['billno']    = $_POST['billno'];
        $_PTS['ddate']     = $_POST['ddate']; 
        $_PTS['amount']    = floatval($_POST['customer_advance_paying']); 
        $_PTS['transecode']= "AS";
        $_PTS['transeno']  = $tr_no; 
        $_PTS['discount']  = 0 ; 
        $_PTS['is_pawn_int']  = 0 ;
        $_PTS['billcode']= $_POST['billcode'];


        // Interest entery


        unset($_POST['hid_cus_serno'],$_POST['bc_no']);
        
        if (isset($_POST['five_days_int_cal'])){
            $_PT['onefive_day_int'] = 1; 
            $_PT['onefive_days']    = $_POST['no_of_int_cal_days'];
        }else{
            $_PT['onefive_day_int'] = 0; 
            $_PT['onefive_days']    = 0;
        }

        $this->db->trans_begin();
        $Q1 = $this->db->insert("t_loantranse",$_PST);

        if ($redeem_int > 0){ 
            $Q2 = $this->db->insert("t_loantranse",$_PT); 
        }

        if ( floatval($_POST['customer_advance_paying']) > 0 ){
            $Q2_5 = $this->db->insert("t_loantranse",$_PTS);
        }        
        

        $Q3 = $this->db->where( array("billno"=>$_POST['billno'],"bc"=>$this->sd['bc']) )->limit(1)->set("stamp_fee",$_POST['stamp_fee'])->update("t_loan");

        $_POST['redeem_int'] = $redeem_int;
        $_POST['loan_amount'] = ($_POST['amount'] - $redeem_int);
        $_POST['hid'] = 0;
        $_POST['date'] = $_POST['ddate'];

        $_POST['ln'] = $_POST['loanno'];
        $_POST['bt'] = $_POST['billtype'];
        $_POST['bn'] = $_POST['billno'];

        $account_update =   $this->account_update(0);                
                
        if($account_update!=1){
            $a['s'] = 0;
            $a['m'] = "Invalid account entries";
            $this->db->trans_rollback();
            $a['s'] = "error";
            echo json_encode($a);
            exit;
        }else{
            $account_update =   $this->account_update(1);
        }
        
        $this->calculate->move_loan_data_to_RE_FO($_POST['loanno'],"P");

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{
            $this->db->trans_commit();
            $a['a']         = 1;
            $a['max_no']    = $this->getRedeemTransNo();
            $a['no']        = $tr_no;
            $a['doc_fee_r'] = $_POST['doc_fee_tot'];
            $a['ln']        = $_POST['loanno'];
            $this->calculate->update_customer_pawn_details($cus_serno,'R');
            $this->utility->user_activity_log($module='Redeem',$action='insert',$trans_code='2',$trans_no=$_POST['billno'],$note='');
        }        

        echo json_encode($a);
    }

    public function LOAD_LOAN(){

        $time1 = time();
        
        $billtype   = $_POST['billtype'];

        if ($_POST['is_old_billno'] == 0){
            $billno     = $_POST['bc_no'].$_POST['billno'];
        }else{
            $billno     = $_POST['billno'];
        }
        
        $five_days_int_cal = $_POST['five_days_int_cal'];
        $bc = $this->sd['bc'];
        $a['billtype_with_same_bill_number_A'] = "";
        $a['billtype_with_same_bill_number_B'] = "";

        if ($_POST['is_bt_choosed']){
            $q = " AND L.billtype='$billtype' ";
        }else{
            $q = "";
        }

        $Q1 = $this->db->query("SELECT L.ori_pwn_date,L.is_amt_base, L.fm_int_paid, L.time,L.goldvalue, L.old_bill_age, L.is_renew, L.billno, L.bc,nicno,L.billtype,L.loanno,L.ddate,LT.amount AS requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,L.cus_serno,C.`customer_id`,L.`stamp_fee`,L.`int_cal_changed`,L.`am_allow_frst_int`,L.`int_paid_untill`,L.fmintrest,is_weelky_int_cal, L.billcode,L.`nmintrate`,L.cat_code,L.is_non_gold FROM `t_loan` L 
            JOIN (SELECT billno,bc, SUM(amount) AS `amount` FROM t_loantranse WHERE transecode = 'P' GROUP BY billno ) LT ON L.`billno` = LT.`billno` AND L.`bc` = LT.`bc`
            JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `m_bill_type` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE L.billno = '$billno' AND L.bc = '$bc' $q LIMIT 1 ");

        $time2 = time(); //A

        $a['TIME_first-query_A'] = ($time2 - $time1);

        if ($Q1->num_rows() > 0){
            $a['loan_sum'] = $Q1->row();
            $loan_no = $Q1->row()->loanno;            
            
            $this->load->model("calculate");
            $this->load->model("t_new_pawn");
            $this->load->model("m_billtype_det");

            //$a['int'] = $this->calculate->interest($loan_no,"json","", $a['loan_sum'] ,$five_days_int_cal);

            //var_dump($Q1->row()->ddate);
            //exit();
            // ---------------------------------------

            $a['int'] = $this->calculate->calculate_interest(   $req_amount = $Q1->row()->requiredamount, 
                                                                $billtype = $Q1->row()->billtype, 
                                                                $pawn_date = $Q1->row()->ddate, 
                                                                $current_date = $this->sd['current_date'], 
                                                                $first_mon_int_cal_range = '',
                                                                $loanno = $Q1->row()->loanno,
                                                                $five_days_int_cal,
                                                                $int_paid_untill = $Q1->row()->int_paid_untill,
                                                                $is_amount_base_int_cal = $Q1->row()->is_amt_base,
                                                                $fm_int_paid = $Q1->row()->fm_int_paid,
                                                                $Q1->row()->ori_pwn_date,
                                                                $Q1->row()
                                                            );

            $time3 = time(); //B
            $a['TIME_first-query_int'] = ($time3 - $time2);
            
            $a['customer_advance']  = $this->calculate->customer_advance($Q1->row()->customer_id,$billno);

            $time4 = time(); //B
            $a['TIME_first-query_customer_advance'] = ($time4 - $time3);

            $a['cus_info']          = $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;

            $time5 = time(); //B
            $a['TIME_first-query_cus_info'] = ($time5 - $time4);

            $a['rec']               = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);

            $time6 = time(); //B
            $a['TIME_first-query_rec'] = ($time6 - $time5);

            $a['s']                 = 1;


            /*//-- bill number search might be delay, if this table got large amount of data 
                $Q4 = $this->db->query("SELECT L.billtype FROM `t_loan` L WHERE billno = '$billno' AND L.bc = '$bc' ");
                if ($Q4->num_rows() > 0){ $a['billtype_with_same_bill_number_A'] = $Q4->result(); }
            //---------------------------------------------------------------------------------*/
        

        }else{            

            $Q2 = $this->db->query("SELECT L.ori_pwn_date,L.is_amt_base, L.fm_int_paid,L.time,L.goldvalue, L.old_bill_age, L.is_renew,L.billno,L.bc,nicno,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,L.cus_serno,C.`customer_id`,L.`stamp_fee`,L.int_cal_changed,L.`am_allow_frst_int`,  (SELECT IF (COUNT(transecode) > 0, transecode ,L.status) FROM t_loantranse_re_fo WHERE bc = '$bc' AND `billno` = '$billno' AND transecode NOT IN('P','A','ADV','AS','PD','PP')) AS `status`,L.`int_paid_untill`,L.fmintrest,is_weelky_int_cal , L.billcode,L.`nmintrate`,L.cat_code,L.is_non_gold FROM `t_loan_re_fo` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `m_bill_type` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '$billno' AND L.bc = '$bc' $q LIMIT 1 ");
            
            if ($Q2->num_rows()>0){
                
                $a['loan_sum'] = $Q2->row();
                $loan_no = $Q2->row()->loanno;            
            
                $this->load->model("calculate");
                $this->load->model("t_new_pawn");
                $this->load->model("m_billtype_det");

                //$a['int'] = $this->calculate->interest($loan_no,"json","", $a['loan_sum'] ,$five_days_int_cal);
                $a['int'] = $this->calculate->calculate_interest(   $req_amount = $Q2->row()->requiredamount, 
                                                                $billtype = $Q2->row()->billtype, 
                                                                $pawn_date = $Q2->row()->ddate, 
                                                                $current_date = $this->sd['current_date'], 
                                                                $first_mon_int_cal_range = '',
                                                                $loanno = $Q2->row()->loanno,
                                                                $five_days_int_cal,
                                                                $int_paid_untill = $Q2->row()->int_paid_untill,
                                                                $is_amount_base_int_cal = $Q2->row()->is_amt_base,
                                                                $fm_int_paid = $Q2->row()->fm_int_paid,
                                                                $Q2->row()->ori_pwn_date
                                                            );
                $a['customer_advance'] = $this->calculate->customer_advance($Q2->row()->customer_id,$billno);
                $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;
                $a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);            
                $a['s'] = 1;

                /*//-- bill number search might be delay because this query with large amount of data 
                    $Q5 = $this->db->query("SELECT L.billtype FROM `t_loan_re_fo` L WHERE billno = '$billno' AND L.bc = '$bc' ");
                    if ($Q5->num_rows() > 0){ $a['billtype_with_same_bill_number_B'] = $Q5->result(); }
                //---------------------------------------------------------------------------------*/
              

                $Q6 = $this->db->query("SELECT transeno FROM t_loantranse_re_fo WHERE bc = '$bc' AND billno = '$billno' AND transecode = 'R' LIMIT 1");

            if($Q6->num_rows()>0){
                    $a['tr_no'] = intval( $Q6->row()->transeno );
                }else{
                     $a['tr_no'] = "";
                }
               


            }else{
                $a['s'] = 0;
            }

        }


        $a['doc_fees'] = $this->get_bill_doc_fees($billno,$bc);

        $time33 = time();

        $a['spend_time'] = ($time33 - $time1) ;

        echo json_encode($a);
    }    


    public function get_bill_doc_fees($billno,$bc){
        
        $Q = $this->db->query(" SELECT D.`letter_no`,D.document_charge 
                                FROM `t_remind_letter_det` D 
                                WHERE D.`billno` = '$billno' AND D.`bc` = '$bc' AND D.`ignore_doc_charge` = 0 ");

        if ($Q->num_rows() > 0){
            return $Q->result();
        }else{
            return "";
        }

    }
    
    public function getSavedRedeem(){
        $bc     = $this->sd['bc'];
        $tr_no  = $_POST['tr_no'];        
        $Q1 = $this->db->query("SELECT L.old_bill_age, L.is_renew, L.`bc`,L.`ddate`,L.`billtype`,L.`billno`,L.`loanno`,L.`requiredamount`,L.`fmintrate`, L.`finaldate`,L.`period` , LT.`ddate` AS `redeemed_date`, LT.`amount` AS `redeemed_amount`, LT.discount, LT.redeem_int, LT.`card_amount`,LT.`advance_amount`,LT.`cash_amount`,L.cus_serno FROM `t_loantranse_re_fo` LT JOIN `t_loan_re_fo` L ON LT.`loanno` = L.`loanno` WHERE LT.bc = '$bc' AND LT.transecode = 'R' AND LT.`transeno` = '$tr_no' LIMIT 1");
        
        if ($Q1->num_rows() > 0){
            
            $a['sum'] = $Q1->row();

            $this->load->model("calculate");
            $a['paid_int'] = $this->calculate->paid_interest($a['sum']->loanno);

            $a['pay_option'] = $this->calculate->get_pay_option($bc, $tr_no, $a['sum']->cus_serno, $a['sum']->card_amount, $a['sum']->advance_amount, $a['sum']->cash_amount ,"R");
            
            if ($a['sum']->cash_amount > 0){ $a['pay_option']['cash_amount'] = $a['sum']->cash_amount; }

            $a['s'] = 1;            
        }else{
            $a['s'] = 0;
        }

       echo json_encode($a);

    }

    public function getRedeemTransNo(){
        $bc = $this->sd['bc'];
        return $this->db->query("SELECT IFNULL(MAX(`transeno`)+1,1) AS `max_no` FROM `t_loantranse_re_fo` WHERE bc = '$bc' AND `transecode` = 'R'")->row()->max_no;
    }

    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 2);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',2);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => date('H:i:s'),
            "trans_code" => 2,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        $this->load->model('account');
        $this->account->set_data($config);

        $cash_book          = $this->utility->get_default_acc('CASH_IN_HAND');        
        $pawn_stock         = $this->utility->get_default_acc('UNREDEEM_ARTICLES');
        $redeem_interest    = $this->utility->get_default_acc('REDEEM_INTEREST');
        $redeem_discount    = $this->utility->get_default_acc('REDEEM_DISCOUNT');        
        $stamp_fee          = $this->utility->get_default_acc('STAMP_FEE');
        $doc_charges        = $this->utility->get_default_acc('POSTAGE_RECEIVABLE'); // must go to 30211 postage receivable 
        $customer_advance   = $this->utility->get_default_acc('ADVANCE_RECEIVED');
        $pawning_interest   = $this->utility->get_default_acc('PAWNING_INTEREST');

        /*echo "cash_book Dr ".(($_POST['loan_amount']+$_POST['redeem_int']+$_POST['doc_fee_tot'])-$_POST['discount'])."<br>";
        echo "UNREDEEM_ARTICLES Cr ".((($_POST['loan_amount'] - $_POST['stamp_fee'])-$_POST['discount']) + $_POST['customer_advance'])."<br>";
        echo "redeem_interest Cr ".$_POST['redeem_int']."<br>";
        echo "redeem_discount Dr ".$_POST['discount']."<br>";
        echo "stamp_fee Cr ".$_POST['stamp_fee']."<br>";
        echo "doc_charges Cr ".$_POST['doc_fee_tot']."<br>";
        echo "customer_advance Dr ".$_POST['customer_advance']."<br>";
        exit;*/

        
        // Dr

        $this->account->set_value2("Redeem value", 
            ($_POST['loan_amount'] + $_POST['redeem_int']) - $_POST['discount']
        , "dr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R");


        if (floatval($_POST['discount']) > 0){
            $this->account->set_value2("Redeem Discount", $_POST['discount'], "dr", $redeem_discount,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"RD");
        }
        
        if (floatval($_POST['customer_advance']) > 0){
            $this->account->set_value2("Customer advance settle - Redeem", $_POST['customer_advance'], "dr", $customer_advance,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"ADV_R");
        }

        

        // Cr

        $this->account->set_value2("Pawning", ((($_POST['loan_amount'] - $_POST['stamp_fee'])-$_POST['discount']) + $_POST['customer_advance']) - $_POST['doc_fee_tot'] , "cr", $pawn_stock,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R");

        if (floatval($_POST['stamp_fee']) > 0){
            $this->account->set_value2("Stamp Fee", $_POST['stamp_fee'], "cr", $stamp_fee,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"SF","","",$this->sd['bc'],"");
        }

        if (floatval(($_POST['redeem_int'] + $_POST['discount'])) > 0){
            $this->account->set_value2("Redeem Interest", ($_POST['redeem_int'] + $_POST['discount']), "cr", $redeem_interest,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"RI");
        }

        if (floatval($_POST['doc_fee_tot']) > 0){
            $this->account->set_value2("Document Fees", $_POST['doc_fee_tot'], "cr", $doc_charges,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"DF","","",$this->sd['bc'],"");
        }


        if ( floatval($_POST['refundable_int']) > 0 ){

            $this->account->set_value2("Pawning interest refund", $_POST['refundable_int'] , "dr",$pawning_interest  ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R","","",$this->sd['bc'],"");
            $this->account->set_value2("Pawning interest refund", $_POST['refundable_int'] , "cr",$pawn_stock        ,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"R","","",$this->sd['bc'],"");

        }
        

        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='2'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 2);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }

    

}