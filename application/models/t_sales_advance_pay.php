<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class t_sales_advance_pay extends CI_Model
{

    private $sd;
    private $max_no;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['current_date'] = $this->sd['current_date'];

        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        $a['bc_no'] = $this->sd['bc_no'];
        $a['backdate_upto'] = $this->sd['backdate_upto'];
        $a['max_no'] = $this->get_max_no();
        $a['current_date'] = $this->sd['current_date'];
        $this->load->model('m_bank');
        $a['store'] = $this->select_store();
        $a['employee'] = $this->select_employee();

        return $a;
    }

    public function get_max_no()
    {
        return $this->db->query("SELECT IFNULL(MAX(nno)+1,1) AS `max_no` FROM `t_sales_advance_sum` where bc = '" . $this->sd['bc'] . "'")->first_row()->max_no;

    }

    public function save()
    {

        try {
            $this->db->trans_begin();
            // $is_redeem = 0;
            // if (isset($_POST['is_redeem'])) {

            //     $is_redeem = $_POST['is_redeem'] == 'R' ? '1' : '0';
            // }

            $oc = $this->sd['oc'];
            $no = $this->get_max_no();
            $this->max_no = $this->get_max_no();
            $rows = $_POST['hid_rows'];
            $_POST['cus_serno'] = $this->db->query("SELECT serno FROM `m_customer` WHERE `customer_id` ='" . $_POST['cus_serno'] . "'")->row()->serno;
            $sum[] = array(
                "bc" => $this->sd['bc'],
                "nno" => $no,
                "ddate" => $_POST['date'],
                "cus_serno" => $_POST['cus_serno'],
                "amount" => $_POST['total_amount'],
                "store" => $_POST['store'],
                "employee" => $_POST['employee'],
                "note" => $_POST['note'],
                "total_discount" => $_POST['discount_amount'],
                "net_amount" => $_POST['net_amount'],
                "payment_amount" => $_POST['payment_amount'],
                "balance_amount" => $_POST['balance_amount'],
                "oc" => $oc,
            );

            $cus[] = array(
                "bc" => $this->sd['bc'],
                "ddate" => $_POST['date'],
                "cus_code" => $_POST['cus_serno'],
                "trans_code" => 106,
                "trans_no" => $this->max_no,
                "sub_trans_no" => $this->max_no,
                "description" => 'Advance Payment',
                "dr" => '0.0',
                "cr" => $_POST['payment_amount'],
                "oc" => $oc,
            );

            for ($i = 0; $i < $rows; $i++) {

                $result = explode('-', $_POST['item_tag_' . $i]);

                $det[] = array(
                    "bc" => $this->sd['bc'],
                    "nno" => $no,
                    "tag_no" => $_POST['tag_no_' . $i],
                    "amount" => $_POST['amount_' . $i],
                    "billno" => $_POST['billno_' . $i],

                );
            }

            if (isset($sum)) {
                if (count($sum)) {
                    //update t_tag table is_sold field
                    foreach ($det as $key => $v) {
                        $data = array('is_reserved' => '1', 'reserved_note' => $_POST['note']);
                        $this->db->where('tag_no', $det[$key]['tag_no']);
                        $this->db->update('t_tag', $data);
                    }

                    $this->db->insert_batch("t_sales_advance_sum", $sum);

                    $this->db->insert_batch("t_sales_cus_settlement", $cus);

                    $this->db->insert_batch("t_sales_advance_det", $det);

                    $this->account_update(1);

                    $this->db->trans_commit();
                    echo $no;
                } else {
                    echo 0;
                    $this->db->trans_rollBack();
                }
            }
        } catch (Exception $e) {
            echo 0;
            $this->db->trans_rollBack();
        }
    }

    public function LOAD_LOAN()
    {
        $billtype = $_POST['billtype'];
        $bc = $this->sd['bc'];
        $billno = $_POST['bc_no'] . $_POST['billno'];

        $q = $this->db->query(" SELECT * FROM t_tag_sum WHERE bc = '$bc' and is_cancel = 0 and billno='" . $billno . "'")->result();
        $this->LOAD_LOAN_TAG();
    }

    public function LOAD_LOAN_TAG()
    {

        $billtype = $_POST['billtype'];
        $bc = $this->sd['bc'];
        $billno = $_POST['bc_no'] . $_POST['billno'];

        $q = "";
        $qr = $this->db->query("SELECT * FROM t_tag_sum tl  WHERE tl.`billno`='$billno' AND bc = '$bc' ");

        if ($qr->num_rows() > 0) {
            $Q1 = $this->db->query("SELECT
                                L.ori_pwn_date
                                ,L.loanno
                                ,L.goldvalue
                                ,L.billtype
                                ,L.ddate as  forfeit_date
                                ,L.fmintrate
                                ,L.period
                                ,L.requiredamount
                                ,L.fmintrest
                                ,c.`nicno`
                                ,L.cat_code
                                ,L.billno
                                ,L.bc
                                ,L.ddate
                                ,L.cus_serno
                                ,C.`customer_id`
                                ,L.billcode
                                ,L.cat_code
                                FROM `t_loan_re_fo` L
                                JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
                                JOIN `m_bill_type` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc`
                                WHERE billno = '$billno' AND L.bc = '$bc' $q
                                LIMIT 1 ");

            $Q2 = $this->db->query("SELECT d.*,i.itemcate, i.`itemname`, c.des AS cat_des , cn.des as `condition` , t.tag_no
                                FROM t_tag_det AS d
                                inner join (SELECT cat_code,item,loanno, GROUP_CONCAT(tag_no) as tag_no,bc FROM t_tag
                                            group by loanno ) as t
                                on d.loanno = t.loanno  and d.itemcode = t.item
                                LEFT JOIN `r_items` AS i ON d.itemcode=i.itemcode
                                LEFT JOIN `r_itemcategory` c ON d.`cat_code` = c.`code`
                                left join  r_condition cn on d.con = cn.code
                                WHERE d.billno ='$billno' and D.bc = '$bc'");

            $a['loan_sum'] = $Q1->row();
            $a['loan_det'] = $Q2->result();
            echo json_encode($a);
        } else {
            echo "0";
        }
    }

    public function get_cat()
    {
        $query = $this->db->query("SELECT * FROM `r_itemcategory` WHERE is_non_gold = 0");
        $s = "<select class= 'catg' style='width:100%' id='c_code_" . $_POST['ind'] . "' name ='c_code_" . $_POST['ind'] . "' ind =" . $_POST['ind'] . " >";
        $s .= "<option> -- </option>";
        foreach ($query->result() as $r) {
            $s .= "<option value =" . $r->code . " >" . $r->code . '-' . $r->des . "</option>";
        }
        $s .= "</select>";
        echo $s;
    }

    public function get_item()
    {
        $query = $this->db->query("SELECT * FROM `r_items` WHERE itemcate = '" . $_POST['cat'] . "'");
        $s = "<select style='width:100%' id='i_code_" . $_POST['ind'] . "' name ='i_code_" . $_POST['ind'] . "' ind =" . $_POST['ind'] . " >";
        $s .= "<option> -- </option>";
        foreach ($query->result() as $r) {
            $s .= "<option value =" . $r->itemcode . " >" . $r->itemcode . '-' . $r->itemname . "</option>";
        }
        $s .= "</select>";
        echo $s;
    }

    public function account_update($condition)
    {

        $config = array(
            "ddate" => $_POST['date'],
            "time" => date("h:i:sa"),
            "trans_code" => 104,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => '',
        );

        $this->load->model('account');
        $this->account->set_data($config);

        $cash_book = $this->utility->get_default_acc('CASH_IN_HAND');
        $sales_stock = $this->utility->get_default_acc('SALES_ADVANCE');

        if (floatval($_POST['payment_amount']) > 0) {
            $this->account->set_value2("Sales Advance Payment", $_POST['payment_amount'], "dr", $cash_book, $condition, "", 0, "", 0, "");
        }

        if (floatval($_POST['payment_amount']) > 0) {
            $this->account->set_value2("Sales Advance Payment", $_POST['payment_amount'], "cr", $sales_stock, $condition, "", 0, "", 0, "");
        }

    }

    public function select_store($name = "store", $id = "store", $class = "")
    {
        $q = $this->db->query(" SELECT `code`,`description` FROM `m_store` ORDER BY description ");
        $t = "<select name='" . $name . "' id='" . $id . "' class='" . $class . "' style='width:250px;'>";

        if ($q->num_rows() > 0) {
            $t .= '<option value="">--</option>';
            foreach ($q->result() as $r) {
                $t .= '<option value="' . $r->code . '">' . $r->description . '</option>';
            }
        } else {
            $t .= '<option value="">No Store added</option>';
        }
        $t .= '</select>';
        return $t;
    }

    public function select_employee($name = "employee", $id = "employee", $class = "")
    {
        $q = $this->db->query(" SELECT `code`,`name` FROM `m_employee` ORDER BY name ");
        $t = "<select name='" . $name . "' id='" . $id . "' class='" . $class . "' style='width:250px;'>";

        if ($q->num_rows() > 0) {
            $t .= '<option value="">--</option>';
            foreach ($q->result() as $r) {
                $t .= '<option value="' . $r->code . '">' . $r->name . '</option>';
            }
        } else {
            $t .= '<option value="">No Employee added</option>';
        }
        $t .= '</select>';
        return $t;
    }

    public function getrecord()
    {
        $this->db->select(array(
            's.bc',
            's.nno',
            's.ddate',
            's.cus_serno',
            's.store',
            's.note',
            's.amount as total_amount',
            's.total_discount',
            's.net_amount',
            's.employee',
            'c.customer_id',
            'c.nicno',
            'c.cusname',
            'payment_amount',
            'balance_amount',

        ));

        $this->db->from('t_sales_advance_sum as s');
        $this->db->join('m_customer c', 'c.serno=s.cus_serno');
        $this->db->where('s.bc', $this->sd['bc']);
        $this->db->where('s.nno', $_POST['id']);
        $query = $this->db->get();

        $x = 0;
        if ($query->num_rows() > 0) {
            $a['sum'] = $query->result();

        } else {
            // $x=2;
        }

        $this->db->select(array(

            'd.bc',
            'd.nno',
            'd.tag_no',
            'd.amount',
            't.cat_code',
            't.loanno',
            't.ser_no',
            'c.code as category_code',
            'c.des as category_dec',
            'c.is_bulk',

        ));

        $this->db->from('t_sales_advance_det as d');
        $this->db->join('t_tag t', 't.tag_no=d.tag_no');
        $this->db->join('r_itemcategory c', 'c.code=t.cat_code');
        $this->db->where('d.nno', $_POST['id']);
        $this->db->where('d.bc', $this->sd['bc']);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $a['det'] = $query->result();
        }

        if (!$query->num_rows() > 0) {
            $x = 2;
        }

        if ($x == 0) {

            echo json_encode($a);
        } else {
            echo json_encode($x);
        }
    }

    public function delete_record()
    {

        if (isset($_POST['no']) && $_POST['no'] != "") {
            //update t_sales_sum table is_cancel field

            $data = array('is_cancel' => '1');
            $this->db->where('nno', $_POST['no']);
            $this->db->where('bc', $this->sd['bc']);
            $this->db->update('t_sales_sum', $data);

            echo 1;
        } else {
            echo 2;
        }
    }
}
