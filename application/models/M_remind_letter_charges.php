<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_remind_letter_charges extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){

        if ($_POST['hid'] == "0"){
            unset($_POST['hid']);
            
            if ($this->db->insert("m_remind_letter_charges",$_POST)) {                
                 $a['s'] = 1;                
            }else{
                $a['s'] = 0;                 
                
            }

        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("letter_no",$hid)->update("m_remind_letter_charges",$_POST);
            $a['s']= 1;            
        }
        
        echo json_encode($a);

    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $code = $_POST['code'];
       
       if ($this->db->query("DELETE FROM `m_remind_letter_charges` WHERE code = '$code' ")) {          
            $a['s'] = 1;
        }else{       
            $a['s'] = 0;            
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
        }

        echo json_encode($a);
    }

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `m_remind_letter_charges` ORDER BY `letter_no` ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->letter_no ."</td><td class='D'>-</td><td class='Des'>" .$R->amount. "</td><td class='ED'><a href=# onClick=setEdit('".$R->letter_no."')>Edit</a> | <a href=# onClick=setDelete('".$R->letter_no."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No values added</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;        
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $letter_no  = $_POST['code'];
        $a['R']     = $this->db->query("SELECT * FROM `m_remind_letter_charges` WHERE letter_no = '$letter_no' LIMIT 1 ")->row();
        echo json_encode($a);
    }
    
    

}