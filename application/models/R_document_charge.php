<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_document_charge extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){		

        $this->load->model("m_branches");
        $a['bc_dropdown'] = $this->m_branches->select();
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){
        $_POST['oc'] = $this->sd['oc'];

        if ($_POST['hid'] == 0){
            unset($_POST['hid']);
        	$Q = $this->db->insert("r_document_charge",$_POST);
        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("id",$hid)->update("r_document_charge",$_POST);
        }

        if ($Q){
            echo 1;
        }else{
            echo 0;
        }
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $id = $_POST['id'];

       if ($this->db->query("DELETE FROM`r_document_charge` WHERE id = '$id' ")){
            echo 1;
       }else{
            echo 0;
       }
    }

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `r_document_charge` ORDER BY `action_date` DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->bc . " </td><td class='D'>-</td><td class='Des'> (" .$R->from_val. " - ". $R->to_val .") = ". $R->amount . "</td><td class='ED'><a href=# onClick=setEdit('".$R->id."')>Edit</a> | <a href=# onClick=setDelete('".$R->id."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
        $T .= "<table>";    
        return $T;
    }
    
    public function set_edit(){
        $id = $_POST['id'];
        $R = $this->db->query("SELECT * FROM `r_document_charge` WHERE id = '$id' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `r_document_charge` WHERE nicno like '%$k%'");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->bc . " </td><td class='D'>-</td><td class='Des'> (" .$R->from_val. " - ". $R->to_val .") = ". $R->amount . "</td><td class='ED'><a href=# onClick=setEdit('".$R->id."')>Edit</a> | <a href=# onClick=setDelete('".$R->id."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
        $T .= "<table>";  
    
        echo $T;
            
    }

}