<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_ntranse_periodical extends CI_Model {

private $sd;    

function __construct(){
    parent::__construct();		
    $this->sd = $this->session->all_userdata();		
}

public function base_details(){		
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_,$r_type="pdf"){

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        if ( !is_array($_POST['bc_arry']) ){
            
            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
            
            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
            }

            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
        }

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = "   L.bc IN ($bc) AND ";
        }
    }
    
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    $tf = $_POST_['h_tf'];
    $tt = $_POST_['h_tt'];

    $bill_type  = $_POST_['r_billtype'];

    if ($bill_type == ""){
        $bt = '';
    }else{
        $bt = " L.`billtype` = '$bill_type' AND ";
    }

   /* $sql = $this->db->query("SELECT B.name as `bc_name`,a.bc, billtype, SUM(`no_of_pawn`) as `no_of_pawn`, SUM(`no_of_redeem`) as `no_of_redeem` FROM (

SELECT L.`bc`,L.`billtype`, 1 AS `no_of_pawn`, 0 AS `no_of_redeem` 
FROM t_loan L 
WHERE $bc $bt L.`status` = 'P' AND L.`action_date` BETWEEN '$fd' AND '$td'

UNION ALL

SELECT L.`bc`,L.`billtype`, 1 AS `no_of_pawn`, 0 AS `no_of_redeem` 
FROM t_loan_re_fo L 
WHERE $bc $bt L.`status` = 'P' AND L.`action_date` BETWEEN '$fd' AND '$td'

UNION ALL

SELECT L.`bc`,L.`billtype`, 0 AS `no_of_pawn`, 1 AS `no_of_redeem` 
FROM t_loantranse_re_fo L 
WHERE $bc $bt L.`transecode` = 'R' AND L.`action_date` BETWEEN '$fd' AND '$td'

) a JOIN m_branches B ON a.bc = B.`bc`

GROUP BY a.bc,A.billtype ORDER BY a.bc,a.billtype ");*/


    $sql = $this->db->query("SELECT B.name AS `bc_name`,a.bc, billtype, SUM(`no_of_pawn`) AS `no_of_pawn`, SUM(`no_of_redeem`) AS `no_of_redeem` FROM ( 

    SELECT L.`loanno`,L.`ddate`, L.`bc`,L.`billtype`, 1 AS `no_of_pawn`, 0 AS `no_of_redeem` 
    FROM `t_loan` L
    WHERE $bc L.ddate BETWEEN '$fd' AND '$td' AND L.`status` = 'P' AND TIME(L.`action_date`) BETWEEN '$tf' AND '$tt' 
    GROUP BY L.`bc`,L.`billtype`,L.`billno` 

    UNION ALL 

    SELECT L.`loanno`,L.`ddate`,L.`bc`,L.`billtype`, 1 AS `no_of_pawn`, 0 AS `no_of_redeem` 
    FROM `t_loan_re_fo` L 
    WHERE $bc L.ddate BETWEEN '$fd' AND '$td' AND L.`status` IN ('P','AM','R','RN','L','PO') AND TIME(L.`action_date`) BETWEEN '$tf' AND '$tt' 
    GROUP BY L.`bc`,L.`billtype`,L.`billno` 

    UNION ALL 

    SELECT '','', L.`bc`,L.`billtype`, 0 AS `no_of_pawn`, 1 AS `no_of_redeem` 
    FROM `t_loantranse_re_fo` LT 
    JOIN `t_loan_re_fo` L ON LT.`loanno` = L.`loanno` AND LT.`bc` = L.`bc` 
    LEFT JOIN ( SELECT cr_amount,billno FROM t_account_trans WHERE entry_code = 'DF' GROUP BY billno ) ACC ON LT.billno = ACC.billno
    WHERE $bc LT.`transecode` = 'R' AND LT.ddate BETWEEN '$fd' AND '$td' AND TIME(LT.`action_date`) BETWEEN '$tf' AND '$tt'
    GROUP BY L.`loanno`

 ) a JOIN m_branches B ON a.bc = B.`bc` GROUP BY a.bc,a.billtype ORDER BY a.bc,a.billtype ");

    
    if( $sql->num_rows() > 0 ){

        /*$r_data['list'] = $sql->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;
        $r_data['d_range'] = $data_range;
        $r_data['b_type'] = $data_type;*/

        $t  = '<table border="01" cellspacing="0" cellpadding="0" borderColor="#cccccc">';

        $t .= '<tr>';        
        $t .= '<td colspan="3"><b>Number of transactions within a period</b></td>';        
        $t .= '</tr>';

        $t .= '<tr>';        
        $t .= '<td colspan="3"><b>From '.$fd.' to '.$td.'</b></td>';        
        $t .= '</tr>';
        
        $s  = '';
        $tp = $tr = 0;
        $first_row_passed = false;

        $make_excel = 1;

        foreach ($sql->result() as $r) {

            if ($s != $r->bc){

                if ($first_row_passed){

                    $t .= '<tr>';        
                    $t .= '<td>Total</td>';
                    $t .= '<td><b>'.$tp.'</b></td>';
                    $t .= '<td><b>'.$tr.'</b></td>';
                    $t .= '</tr>';

                    $tp = $tr = 0;
                }

                $first_row_passed = true;

                $t .= '<tr>';        
                $t .= '<td colspan="3" height="40"><b>'.$r->bc_name.'</b></td>';        
                $t .= '</tr>';
                $s  = $r->bc;
                
                $t .= '<tr>';        
                $t .= '<td>Bill Type</td>';
                $t .= '<td>No of Pawning</td>';
                $t .= '<td>No of Redeemed</td>';
                $t .= '</tr>';


            }

            $t .= '<tr>';        
            $t .= '<td>'.$r->billtype.'</td>';
            $t .= '<td>'.$r->no_of_pawn.'</td>';
            $t .= '<td>'.$r->no_of_redeem.'</td>';
            $t .= '</tr>';

            $tp += $r->no_of_pawn;
            $tr += $r->no_of_redeem;

        }

        $t .= '<tr>';        
        $t .= '<td>Total</td>';
        $t .= '<td><b>'.$tp.'</b></td>';
        $t .= '<td><b>'.$tr.'</b></td>';
        $t .= '</tr>';

        $t .= '</table>';




        if ($r_type == "pdf"){
            echo "No pdf file for this report, Please use excel format";
        }else{

            if (!$make_excel){
                echo $t;
            }else{
                header('Content-type: application/excel');
                $filename = 'periodical_transactions.xls';
                header('Content-Disposition: attachment; filename='.$filename);
                $data = $t;
                echo $data;
            }

        }
      
        

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

public function Excel_report(){
    $this->PDF_report($_POST,"Excel");
}

}