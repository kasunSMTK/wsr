<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_ftv_cash_collection extends CI_Model {

  private $sd;    

  function __construct(){
    parent::__construct();        
    $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
  }   

  public function PDF_report($_POST_){

  
     $bc=$this->sd['bc'];
   
   

    $Q = $this->db->query("SELECT
                              f.no,
                              f.ref_no,
                              f.date_time,
                              f.requ_initi_bc,
                              bb.name AS ini_branch,
                              f.from_bc,
                              b.name AS from_branch,
                              f.to_bc,
                              t.name AS to_branch,
                              f.transfer_type,
                              f.amount,
                              f.comment,
                              f.to_acc,
                              m.description
                            FROM
                              t_fund_transfer_sum f 
                            LEFT JOIN m_branches b ON b.bc=f.from_bc 
                            LEFT JOIN m_branches t ON t.bc=f.to_bc 
                            LEFT JOIN m_branches bb ON bb.bc = f.requ_initi_bc
                            LEFT JOIN m_account m ON m.code =f.to_acc 
                            WHERE f.no='".$_POST['inv_no']."'");

    
    if($Q->num_rows() > 0){

      $r_data['list'] = $Q->result();
      /*$r_data['fd'] = $fd;
      $r_data['td'] = $td;*/

      $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
      echo "<script>location='default_pdf_error'</script>";
    }

  }

}