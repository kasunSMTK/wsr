<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class rpt_sales_adavance_details extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    public function Excel_report()
    {

        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST, 'XL');
    }

    public function PDF_report($a, $rt = 'PDF')
    {

        if ($_POST['bc_arry'] === null) {
            $BC = " ";
        } else {

            for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                $bc_ar[] = "'" . str_replace(",", "", $_POST['bc_arry'][$n]) . "'";
            }

            $bc = implode(',', $bc_ar);
            $BC = " AND s.bc IN ($bc)  ";
        }

        // var_dump($bc,$BC);
        // exit();
        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];

        $q = $this->db->query("SELECT
                s.bc,
                s.`nno` AS nno,
                s.ddate, 
                s.`amount` AS amount,
                s.`payment_amount` AS payment_amount,
                s.`balance_amount` AS balance_amount,
                CONCAT(
                  d.`tag_no`,
                  '- ',
                  c.`des`,
                  ' / ',
                  i.`itemname`
                )  AS `description`,
                CONCAT(m.`customer_id`,' - ',m.`cusname`) AS customer

              FROM
                `t_sales_advance_det` d
                JOIN `t_sales_advance_sum` s
                  ON s.bc = d.bc
                  AND s.nno = d.`nno`
                JOIN t_tag t
                  ON t.`tag_no` = d.`tag_no`
                JOIN r_items i
                  ON i.`itemcode` = t.`item`
                JOIN `r_itemcategory` c
                  ON c.`code` = i.`itemcate`
                  JOIN `m_customer` m ON m.`serno` = s.`cus_serno`
            WHERE d.is_ret='0' AND s.`ddate`  BETWEEN '" . $fd . "' AND '" . $td . "' $BC");

        if ($q->num_rows() > 0) {

            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            if ($rt == "PDF") {
                $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
            } else {
                echo "<script>alert('Excel Report Not Found');close();</script>";
            }

        } else {
            echo "<script>alert('No data found');close();</script>";
        }

    }

}
