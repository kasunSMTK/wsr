<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_mark_bills_list extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    $type = $_POST['mark_bill_type'];

    $Q1 = "";

   if ($type != "All"){
      $Q1 = " LM.`type` = '$type' AND ";
    }

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = " AND  LR.bc IN ($bc)  ";
        }
    }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND LR.`billtype`='".$_POST['r_billtype']."'";
    }


    $sql = "  SELECT LM.`type`,LM.`no`,LR.`billtype`,LR.`ddate`,LR.`billno`,C.`cusname`, C.`address`,LR.`requiredamount`
            
              FROM `t_loan_re_fo` LR 

              INNER JOIN `t_loan_mark` LM ON (LR.`loanno` = LM.`loan_no`) 
              INNER JOIN `m_customer` C   ON (LR.`cus_serno` = C.`serno`) 
              WHERE $Q1 LM.`ddate` BETWEEN '$fd' AND '$td' $bc $billtype
              ORDER BY LM.type, LM.no ";

    $query = $this->db->query($sql);
    
    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}