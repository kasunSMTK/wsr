<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class rpt_sales_summery extends CI_Model
{

    private $sd;
    private $mtb;

    public function __construct()
    {
        parent::__construct();
        $this->sd = $this->session->all_userdata();
    }

    public function base_details()
    {
        $a['report'] = '';
        return $a;
    }

    public function Excel_report()
    {

        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST, 'XL');
    }

    public function PDF_report($a, $rt = 'PDF')
    {

        if ($_POST['bc_arry'] === null) {
            $BC = " ";
        } else {

            for ($n = 0; $n < count($_POST['bc_arry']); $n++) {
                $bc_ar[] = "'" . str_replace(",", "", $_POST['bc_arry'][$n]) . "'";
            }

            $bc = implode(',', $bc_ar);
            $BC = " AND s.bc IN ($bc)  ";
        }

        // var_dump($bc,$BC);
        // exit();
        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];

        $q = $this->db->query("SELECT
        s.bc,
        s.nno,
        s.cus_serno,
        c.`cusname`,
        s.store,
        st.`description` AS store_name,
        s.employee,
        e.`name` AS emp_name,
        s.note,
        s.`amount` - IFNULL(SUM(r.amount),0) AS total_amount,
        s.total_discount,
        s.`net_amount` - IFNULL(SUM(r.net_amount),0) AS net_amount
      FROM
        t_sales_sum s
        JOIN `t_sales_det` d ON d.`nno` = s.`nno` AND d.`bc`= s.`bc`
        JOIN m_customer c
          ON c.`serno` = s.`cus_serno`
        JOIN m_store st
          ON st.`code` = s.`store`
        JOIN m_employee e
          ON e.`code` = s.`employee`
          LEFT JOIN `t_sales_ret_sum` r ON r.`sales_sum` = s.`nno` AND s.`bc`= r.`bc`
      WHERE d.is_ret = '0' and s.ddate BETWEEN '" . $fd . "'
        AND '" . $td . "' $BC
        GROUP BY s.nno ");

        if ($q->num_rows() > 0) {

            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            if ($rt == "PDF") {
                $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
            } else {
                echo "<script>alert('Excel Report Not Found');close();</script>";
            }

        } else {
            echo "<script>alert('No data found');close();</script>";
        }

    }

}
