<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_customer_hitory extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){
    $a=false;
    $cus_no=$_POST_['txt_rpt_cus_info'];

    if($a==true){
      $sql_no = "SELECT customer_no,nicno FROM m_customer WHERE customer_no='$cus_no' LIMIT 1";
      $query1 = $this->db->query($sql_no);

      if($query1>0){
       
        $customer= $query1->row()->customer_no;  
      }else{
         $a=false;
      }
    }else if($a==false){
       $sql_no = "SELECT serno,nicno FROM m_customer WHERE nicno='$cus_no' LIMIT 1";
      $query1 = $this->db->query($sql_no);

      if($query1>0){
        $customer= $query1->row()->serno;  
      }else{

      }
    }
       // $sql_no = "SELECT customer_no,nicno FROM m_customer WHERE customer_no LIKE '%$cus_no%'  OR nicno LIKE '%$cus_no%' LIMIT 1";
      
    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = "   l.bc IN ($bc) AND ";
        }
    }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND l.`billtype`='".$_POST['r_billtype']."'";
    }
   
    
       
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

   
    $sql="SELECT 
                a.stus,
                a.cus_serno,
                m.cusname,
                a.bc,
                b.name,
                a.billno,
                a.pwningdate,
                a.finaldate,
                a.redemdate,
                a.requiredamount,
                a.fofeited,
                a.rdm_amt,
                (a.requiredamount - a.rdm_amt) AS bal,
                a.rdm_int
          FROM (SELECT 
                    'P'AS stus,
                    l.cus_serno,
                    l.bc,
                    l.billno,
                    l.ddate AS pwningdate,
                    l.finaldate,
                    '' AS redemdate,
                    IFNULL(l.requiredamount,0) AS requiredamount,
                    IFNULL(a.FCA,0) AS fofeited,
                    '0' AS rdm_amt,
                    '0' as rdm_int
                FROM t_loan l
                LEFT JOIN (SELECT ls.bc,ls.billno,IFNULL(ls.amount,0) AS FCA 
                           FROM t_loantranse ls  
                           WHERE ls.transecode='F') a ON a.bc=l.bc AND a.billno=l.billno AND l.cus_serno='$customer'
                WHERE $bc  l.cus_serno='$customer' $billtype
                UNION ALL
                
                SELECT
                  'R'AS stus,
                  l.cus_serno,
                  l.bc,
                  l.billno,
                  l.ddate  AS pwningdate,
                  l.finaldate,
                  l.ddate AS redemdate,
                  IFNULL(l.requiredamount,0)  AS requiredamount,
                  IFNULL(a.FCA,0) AS fofeited,
                  IFNULL(l.requiredamount,0) AS rdm_amt,
                  IFNULL(ri.amount,0) AS rdm_int
                FROM t_loan_re_fo l
                LEFT JOIN (SELECT ls.bc,ls.billno,IFNULL(ls.amount,0) AS FCA 
                           FROM t_loantranse_re_fo ls  
                           WHERE ls.transecode='F' ) a ON a.bc=l.bc AND a.billno=l.billno 
                LEFT JOIN (SELECT ls.bc,ls.billno,ls.ddate AS pwningdate 
                           FROM t_loantranse_re_fo ls  
                           WHERE ls.transecode='P') b ON b.bc=l.bc AND b.billno=l.billno 
                left join (SELECT ls.bc,ls.billno, ls.`amount`  FROM t_loantranse_re_fo ls  
                             WHERE ls.`transecode`='RI') as ri on l.bc = ri.bc and l.billno = ri.billno                                      
                WHERE $bc l.cus_serno='$customer'  $billtype 
          ) a
          LEFT JOIN m_customer m ON  m.customer_no=a.cus_serno
          LEFT JOIN m_branches b ON b.bc=a.bc  
          WHERE a.cus_serno='$customer'
               
                ";
     

    if($_POST['bc']!=""){

        //$sql.="  AND  a.bc='".$_POST['bc']."'";
    }

    $sql.="GROUP BY a.billno";

    $query = $this->db->query($sql);


    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}