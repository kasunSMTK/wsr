<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_br_man_period_setup extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){
        $a['data'] = $this->get_bc_man_list();
		return $a;
    }

    public function get_bc_man_list(){

        $Q = $this->db->query("SELECT A.*, B.`name` AS bc_name , U.`display_name`,U.`loginName`, U.`disable_login` FROM `t_over_adv_app_managers`  A JOIN m_branches B ON A.`bc` = B.`bc` JOIN u_users U ON A.`manager_cCode` = U.`cCode` WHERE A.bc  IN (SELECT bc FROM u_users WHERE bc_manager = 1 GROUP BY bc HAVING COUNT(cCode) > 1) ORDER BY A.bc ");

        $T = '';

        $T .= "<table border='1' class='tbl_bc_man_p' align='center'>";
        
        $T .= "<tr>";
        $T .= "<td><b>Branch</b></td>";
        $T .= "<td><b>User</b></td>";
        $T .= "<td><b>From Date</b></td>";
        $T .= "<td><b>To Date</b></td>";
        $T .= "<td><b>Action</b></td>";
        $T .= "</tr>";        

        if ($Q->num_rows() > 0){

            foreach ($Q->result() as $r) {
                
                $T .= "<tr>";
                $T .= "<td>".$r->bc_name."</td>";

                if ($r->disable_login == 1){
                    $d = ' <div style="color:red;float:right">Disabled</div>';
                }else{
                    $d = '';
                }

                if ($r->date_set == 1){
                    $u = "<br><span style='font-size:11px;color:green'>Date period updated for this user</span> ";
                }else{
                    $u = '';
                }
                

                $T .= "<td>".$r->display_name." (".($r->loginName).")"." $d $u</td>";
                $T .= "<td><input type='text' class='ddd fd' value='".$r->from_date."'></td>";
                $T .= "<td><input type='text' class='ddd td' value='".$r->to_date."'></td>";
                $T .= '<td><input type="button" value="Set" class="btn_regular btnSet" bc="'.$r->bc.'" ccode="'.$r->manager_cCode.'" ></td>'; 
                $T .= "</tr>";

            }

        }


        $T .= "</table>";

        return $T;

    }


    public function set_period(){

        $bc = $_POST['bc'];
        $cc = $_POST['cc'];
        $fd = $_POST['fd'];
        $td = $_POST['td'];

        if ( $fd > $td ){
            $a['s'] = 2;
            echo json_encode($a);
            exit;
        }



        $q = $this->db->query(" UPDATE t_over_adv_app_managers
                                SET date_set = 1, no_manager_changed = 1 ,  from_date = '$fd' , to_date = '$td'
                                WHERE bc = '$bc' AND manager_cCode = '$cc'
                                LIMIT 1 ");

        if ($q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }


    
}