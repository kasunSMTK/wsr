<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_message extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();
        $this->mtb = 'tblusers';        
    } 

    public function base_details(){        
        $a['old_messages'] = $this->get_old_messages();
        return $a;
    }

    public function send_message(){

        $Q = $this->db->insert('msg',array('msg'=>$_POST['msg'] , 'hg_priority' => $_POST['hp'] ));
        if ($Q){ $a['s'] = 1; $a['data'] = $this->get_old_messages(); }else{$a['s'] = 0; } 
        echo json_encode($a);
    }

    public function get_old_messages(){
        $Q = $this->db->query("SELECT * FROM `msg` ORDER BY id DESC");

        if ($Q->num_rows() > 0){
            
            $t = "";
            
            foreach ($Q->result() as $R) {


                if ( $R->imagename != "" ){
                    $fn = "<br><br><img width='600' src='./images/msg_img/".$R->imagename."' >";
                }else{
                    $fn = "";
                }


                $t .= "<div class='div_msg_list'> 

                    <div style='float:right'>Sent Date and Time : ".$this->calculate_time_span($R->datetime)."</div>

                     ".nl2br($R->msg). " $fn

                     
                     <br><br><div><a class='delete_msg' id='".$R->id."'>Delete</a></div>
                     </div>";
            }

        }else{
            $t = "<br>No messages found";
        }
        return $t;
    }

    public function new_messages(){

        $Q = $this->db->where("hg_priority",0)->order_by('id','desc')->limit(2)->get('msg');

        if ($Q->num_rows() > 0){
            
            $M = "";

            foreach ($Q->result() as $R) {
                
                if ( $R->imagename != "" ){
                    $fn = "<br><br><a target='_blank' href='./images/msg_img/".$R->imagename."'><img width='600' src='./images/msg_img/".$R->imagename."' ></a>";
                }else{
                    $fn = "";
                }

                $M .= "<div class='msg_c'>". nl2br($R->msg)." $fn <br><br><div style='color:green;font-size:11px'> ".$this->calculate_time_span($R->datetime)."</div> </div>";
            }
            
            return $M;

        }else{
            return "No messages found";
        }
    }    

    public function delete_message(){
        $id = $_POST['id'];

        $Q = $this->db->where('id',$id)->limit(1)->delete('msg');

        if ($Q){
            $a['s']     = 1;
            $a['data']  = $this->get_old_messages();
        }else{
            $a['s']     = 0;
        }

        echo json_encode($a);
    }



    public function save(){        

        if (isset($_FILES["userfile"]['name'])){            
                    
            $config['upload_path']          = './images/msg_img';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 4096;
            /*$config['max_width']            = 1024;
            $config['max_height']           = 768;*/
            $config['file_name']            = $file_name = strtotime( date('Y-m-d h:i:s') )."_".$_FILES["userfile"]['name'];

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userfile')){            
                
                $a['s'] = 3;
                echo json_encode($a);
                exit;

            }

        }else{            

            $file_name = '';

        }

        $Q = $this->db->insert('msg',array('msg'=>$_POST['msg'] , 'hg_priority' => $_POST['hp_msg'] , 'imagename' => $file_name ));
        
        if ($Q){
            $a['s'] = 1; 
            $a['data'] = $this->get_old_messages(); 
        }else{
            $a['s'] = 0; 
        }

        echo json_encode($a);

    }



    public function calculate_time_span($date){
        
/*        $seconds  = strtotime(date('Y-m-d H:i:s')) - strtotime($date);

            $months = floor($seconds / (3600*24*30));
            $day = floor($seconds / (3600*24));
            $hours = floor($seconds / 3600);
            $mins = floor(($seconds - ($hours*3600)) / 60);
            $secs = floor($seconds % 60);

            if($seconds < 60)
                $time = $secs." seconds ago";
            else if($seconds < 60*60 )
                $time = $mins." min ago";
            else if($seconds < 24*60*60)
                $time = $hours." hours ago";
            else if($seconds < 24*60*60)
                $time = $day." day ago";
            else
                $time = $months." month ago";*/

            return $this->db->query("SELECT CONCAT(
FLOOR(HOUR(TIMEDIFF(NOW(), '$date')) / 24), ' days ',
MOD(HOUR(TIMEDIFF(NOW(), '$date')), 24), ' hours ',
MINUTE(TIMEDIFF(NOW(), '$date')), ' minutes') as `tm`")->row()->tm;
    }



    public function set_loder_msgs(){

        $nfvm = $_POST['no_of_visible_msgs'];

        $Q = $this->db->where("hg_priority",0)->order_by('id','desc')->limit(1,$nfvm)->get('msg');

        $a['d'] = $Q->result();

        echo json_encode($a);

    }



}