<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class t_salary_general extends CI_Model
{
  
  private $sd;
  private $mtb;
  
  private $mod = '003';
  
  function __construct()
  {
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);
    //$this->mtb = $this->tables->tb['t_voucher_gl_sum'];
    $this->load->model('user_permissions');
    $this->max_no = $this->utility->get_max_no_sub2("t_voucher_gl_sum", "nno");
  }
  
  public function base_details(){

    $this->load->model('m_branches');
    $a['grid_bc'] =  $this->m_branches->select("v_bc[]");
    
    $this->load->model('m_voucher_class');
    $a['voucher_class_dropdown'] = $this->m_voucher_class->select("voucher_class[]");

    $a['isHO'] = $this->sd['isHO'];

    $a['app_pending_vou'] = $this->app_pending_vou();

    return $a;
  }
  
  
   public function app_pending_vou(){

    $bc = $this->sd['bc'];
      $q  = $this->db->query("SELECT VS.`ddate`,VS.`bc`,VS.`nno`,VS.`type`,VS.`paid_acc`,MA.`description`
                    FROM `t_voucher_gl_sum` VS 
                    JOIN m_account MA ON VS.`paid_acc` = MA.`code`
                    WHERE VS.`bc` = '$bc' AND VS.`is_approved` = 0");

      $t = '';

      if ($q->num_rows() > 0){
        $t .= "<option value=''>Select</option>";       
        foreach ($q->result() as $r) {
          $t .= "<option nno='".$r->nno."' paid_acc='".$r->paid_acc."' type='".$r->type."'>".$r->nno." - ".$r->ddate." - ".$r->type." - (".$r->paid_acc.") ".$r->description."  </option>";
        }
      }else{
        $t .= "<option>No vouchers for approval </option>";       
      }

      return $t;

   }
  
  public function data_table()
  {
    $this->load->library('table');
    $this->load->library('useclass');
    
    $this->table->set_template($this->useclass->grid_style());
    
    $code        = array(
      "data" => "Code",
      "style" => "width: 50px;"
    );
    $description = array(
      "data" => "Description",
      "style" => ""
    );
    $action      = array(
      "data" => "Action",
      "style" => "width: 60px;"
    );
    
    $this->table->set_heading($code, $description, $action);
    
    
    return $this->table->generate();
  }
  
  public function get_data_table()
  {
    echo $this->data_table();
  }
  
  
  
public function validation(){
    
    $status         = 1;
    
    $account_update = $this->account_update(0);
    
    if ($account_update != 1){
      return "Invalid account entries";
    }
    
    return $status;

}
  
  
  
  
  public function save(){
    /*$this->db->trans_begin();
    error_reporting(E_ALL);
    
    function exceptionThrower($type, $errMsg, $errFile, $errLine){
      throw new Exception($errMsg);
    }
    
    set_error_handler('exceptionThrower');
    try {*/
      
      if ($_POST['hid'] == "0" || $_POST['hid'] == ""){
           $this->max_no = $this->get_max_no($_POST['from_acc'],'gl_vouchers');
      }else{
           $this->max_no = $_POST['hid'];
      }

      if ($_POST['h_load_vou_bc'] != ""){
        $bc_v = $_POST['h_load_vou_bc'];
      }else{
        $bc_v = $this->sd['branch'];
      }
      
      $validation_status = $this->validation();
      
      if ($validation_status == 1) {
        
        $_POST['groups']=0;        

        if ($_POST['type'] == "cash" ){
            $_POST['paid_acc'] = $_POST['cash_acc'] = $_POST['from_acc'];
            $_POST['cheque_acc'] = "";
        }

        if ($_POST['type'] == "cheque" ){
            $_POST['paid_acc'] = $_POST['cheque_acc'] = $_POST['from_acc'];
            $_POST['cash_acc'] = "";
        }

        if ($_POST['type'] == "cash"){
            $cash_total_amount = $_POST['total_amount'];
            $chqu_total_amount = "";
        }

        if ($_POST['type'] == "cheque"){
            $chqu_total_amount = $_POST['total_amount'];
            $cash_total_amount = "";
        }
            

        $sum = array(
          "cl" => $this->sd['cl'],
          "bc" => $this->sd['branch'],
          "nno" => $this->max_no,
          "type" => $_POST['type'],
          "sub_no" => 0,
          "ref_no" => $_POST['ref_no'],
          "ddate" => $_POST['date'],
          "paid_acc" => $_POST['paid_acc'],
          "note" => $_POST['voucher_desc'],
          "payee_desc" => $_POST['payee_desc'],
          "cash_amount" => $cash_total_amount,
          "cash_acc" => $_POST['cash_acc'],
          "cheque_amount" => $chqu_total_amount,
          "cheque_acc" => $_POST['cheque_acc'],
          "oc" => $this->sd['oc'],
          "vocuher_type" => "gl_vouchers"
        );

       // var_dump($sum);
        
        $sum_update= array(
          
          "type" => $_POST['type'],
          "sub_no" => 0,
          "ref_no" => $_POST['ref_no'],
          "ddate" => $_POST['date'],
          "paid_acc" => $_POST['paid_acc'],
          "note" => $_POST['voucher_desc'],
          "payee_desc" => $_POST['payee_desc'],          
          "category_id" => "",
          "group_id" => "",
          "cash_amount" => $_POST['total_amount'],
          "cash_acc" => $_POST['cash_acc'],
          "cheque_amount" => $_POST['total_amount'],
          "cheque_acc" => $_POST['cheque_acc'],
          "oc" => $this->sd['oc']
          
        );
        
        
        for ($i = 0; $i < count($_POST['to_acc']); $i++) {
          if ($_POST['to_acc'][$i] != "" && $_POST['to_amount'][$i] != "" && $_POST['v_bc'][$i] != ""){
            
            if (isset($_POST['v_bc'][$i]) && isset($_POST['voucher_class'][$i])){             

              

              $det[] = array(
                "bc" => $bc_v,
                "cl" => $this->sd['cl'],
                "nno" => $this->max_no,
                "acc_code" => $_POST['to_acc'][$i],
                "amount" => $_POST['to_amount'][$i],
                "ref_no" => $_POST['ref_no'],
                "type" => $_POST['type'],
                "sub_no" => 0,
                "v_bc" => $_POST['v_bc'][$i],
                "v_class" => $_POST['voucher_class'][$i],
                "paid_acc" => $_POST['paid_acc']              
              );

           }

          }          
          
        }

        $_POSTX_APPROVAL = array(
            
          'no'=>$this->max_vou_app(),
          'bc'=>$this->sd['branch'],
          'date'=>$_POST['date'],
          'ref_no'=>$_POST['ref_no'],
          'request_from'=>"",
          'method'=>$_POST['type'],
          'amount'=>$_POST['total_amount'],
          'approved_amount'=>"",
          'status'=>"W",
          'comment'=>"",
          'request_by'=>$this->sd['oc'],
          'request_datetime'=>date('Y-m-d h:i:s'),
          'approved_by'=>"",
          'approved_datetime'=>"",
          'paid_acc' => $_POST['paid_acc'],
          'nno' => $this->max_no,
          'is_HO_cashier' => $this->sd['is_HO_cashier'],
          'vocuher_type'  => "gl_vouchers"
      );
        
    
    if ($_POST['hid'] == "0" || $_POST['hid'] == ""){
          
          if ($this->user_permissions->is_add('t_voucher_general')) {            
            
            $this->db->insert("t_voucher_gl_sum", $sum);
            $this->db->insert('t_gen_vou_app_rqsts_sum',$_POSTX_APPROVAL);    
            
            if (count($det)) {
              $this->db->insert_batch("t_voucher_gl_det", $det);
            }            

            if ($_POST['type'] == "cheque"){

          $cheque_no = $_POST['cheque_no'];
          
          $cheque_issue = array(
            "cl" => $this->sd['cl'],
            "bc" => $this->sd['branch'],
            "ddate" => $_POST['date'],
            "trans_code" => 48,
            "trans_no" => $this->max_no,
            "bank" => $_POST['from_acc'],
            "branch" => "",
            "account" => $_POST['from_acc'],
            "cheque_no" => $_POST['cheque_no'],
            "amount" => $_POST['total_amount'],
            "bank_date" => $_POST['bank_date'],
            "status" => 0,
            "oc" => $this->sd['oc'],      
            "issued_to_acc" => $_POST['from_acc']
          );

          $this->db->insert('t_cheque_issued',$cheque_issue);
      
        }
            
            //$this->account_update(1);
            echo $this->db->trans_commit();
            
          } else {
            echo "No permission to save records";
            $this->db->trans_commit();
          }
        

    }else{
         
         if ($this->user_permissions->is_edit('t_voucher_general')) {
            
            $this->db->where('nno', $_POST['hid']);
            $this->db->where("type", $_POST['type']);
            $this->db->where("paid_acc", $_POST['paid_acc']);
            $this->db->where("cl", $this->sd['cl']);
            $this->db->where("bc", $bc_v);
            $this->db->delete("t_voucher_gl_det");
            
            $this->db->where('nno', $_POST['hid']);
            $this->db->where("type", $_POST['type']);
            $this->db->where("paid_acc", $_POST['paid_acc']);
            $this->db->where("cl", $this->sd['cl']);
            $this->db->where("bc", $bc_v);
            $this->db->update("t_voucher_gl_sum", $sum_update);
            
            if (count($det)) {
              $this->db->insert_batch("t_voucher_gl_det", $det);

              $this->db->where(array(                
                "no" => $_POST['h_load_vou']
                ))->update('t_gen_vou_app_rqsts_sum', array("amount" => $_POST['total_amount']));
            }   


            if ($_POST['type'] == "cheque"){

                 $cheque_no = $_POST['cheque_no'];

                $cheque_issue = array(            
                  "trans_code" => 48,
                  "trans_no" => $this->max_no,
                  "bank" => $_POST['from_acc'],           
                  "account" => $_POST['from_acc'],
                  "cheque_no" => $_POST['cheque_no'],
                  "amount" => $_POST['total_amount'],
                  "bank_date" => $_POST['bank_date'],           
                  "oc" => $this->sd['oc'],      
                  "issued_to_acc" => $_POST['paid_acc']
                );


          
               $this->db->where('trans_no', $this->max_no);
               $this->db->where("trans_code",48);
               $this->db->where("issued_to_acc", $_POST['paid_acc']);         
               $this->db->update('t_cheque_issued',$cheque_issue);
      
            }

            //$this->account_update(1);
            echo $this->db->trans_commit()+1;
          
          } else {
            echo "No permission to save records";
            $this->db->trans_commit();
          }
        

        }
        
      } else {
        echo $validation_status;
        $this->db->trans_commit();
      }
      
    /*}
    catch (Exception $e) {
      $this->db->trans_rollback();
      echo $e->getMessage()."Operation fail please contact admin";
    }*/
    
  }
  
  
   
  
public function account_update($condition){

    if ($_POST['h_load_vou_bc'] != ""){
      $bc_v = $_POST['h_load_vou_bc'];
    }else{
      $bc_v = $this->sd['branch'];
    }


    $this->db->where("trans_no", $this->max_no);
    $this->db->where("trans_code", 48);
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $bc_v);
    $this->db->delete("t_check_double_entry");
    
    if ($condition == 1) {
      if ($_POST['hid'] != "0" || $_POST['hid'] != "") {
        $this->db->where('cl', $this->sd['cl']);
        $this->db->where('bc', $bc_v);
        $this->db->where('trans_code', 48);
        $this->db->where('trans_no', $this->max_no);
        $this->db->where('entry_code',$_POST['from_acc']);
        $this->db->delete('t_account_trans');
      }
    }
    
    $config = array(
      "ddate" => $_POST['date'],
      "trans_code" => 48,
      "trans_no" => $this->max_no,
      "op_acc" => 0,
      "reconcile" => 0,
      "cheque_no" => 0,
      "narration" => "",
      "ref_no" => $_POST['ref_no']
    );
    
    $des = "GENARAL VOUCHER - " . $_POST['from_acc'];
    $this->load->model('account');
    $this->account->set_data($config);
    
    for ($i = 0; $i < count($_POST['to_acc']); $i++) {            

      if ($_POST['to_acc'][$i] != "" && $_POST['to_amount'][$i] != "" && $_POST['v_bc'][$i] != ""){

        $dess = "GENARAL VOUCHER - " . $_POST['to_acc'][$i];        
        $this->account->set_value4($dess,$_POST['to_amount'][$i],"dr", $_POST['to_acc'][$i],$condition,"","",$_POST['from_acc']);
      
      }
    }    
    
    if ($_POST['type'] == "cash" || $_POST['type'] == "cheque"){      
      $acc_code = $_POST['from_acc'];
      $this->account->set_value4($des, $_POST['total_amount'], "cr", $acc_code, $condition,"","",$_POST['from_acc']);
    }    
    
    if ($condition == 0) {
      
      $query = $this->db->query("
           SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok 
           FROM `t_check_double_entry` t
           LEFT JOIN `m_account` a ON t.`acc_code` = a.`code`
           WHERE  t.`cl`='".$this->sd['cl']."'  AND t.`bc`='".$bc_v."' AND t.`trans_code`='48'  AND t.`trans_no` ='" . $this->max_no . "' AND 
           
           t.entry_code = '".$_POST['from_acc']."' AND

           a.`is_control_acc`='0'");
      
      if ($query->row()->ok == "0") {
        
        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 48);
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $bc_v);
        $this->db->delete("t_check_double_entry");
        
        return "0";

      } else {
        
        return "1";

      }


    }
  }






  public function account_cancel($condition){
        
    $q1 = $this->db->where(array("nno" => $_POST['hid']))->get('t_voucher_gl_sum')->row();

    $config = array(
      "ddate" => $q1->ddate,
      "trans_code" => 48,
      "trans_no" => $_POST['hid'],
      "op_acc" => 0,
      "reconcile" => 0,
      "cheque_no" => 0,
      "narration" => "",
      "ref_no" => $q1->ref_no
    );
    
    $des = "GENARAL VOUCHER - Cancelled ";
    $this->load->model('account');
    $this->account->set_data($config);
    
    $q = $this->db->where(array("trans_code" => 48,"trans_no" => $_POST['hid']))->get('t_account_trans');

    foreach($q->result() as $r){        
        
        if ($r->dr_amount !=  0){
            $dess = "GENARAL VOUCHER - Cancelled ".$r->acc_code;
            $this->account->set_value4($dess,$r->dr_amount,"cr",$r->acc_code, $condition, $_POST['hid']);
        }

        if ($r->cr_amount !=  0){
            $dess = "GENARAL VOUCHER - Cancelled ".$r->acc_code;
            $this->account->set_value4($dess,$r->cr_amount,"dr",$r->acc_code, $condition, $_POST['hid']);
        }

    }
    
    /*if (isset($_POST['cash']) && !empty($_POST['cash']) && $_POST['cash'] > 0) {      
      $acc_code = $_POST['cash_acc'];
      $this->account->set_value4($des, $_POST['cash'], "cr", $acc_code, $condition,$_POST['id']);
    }
    
    if (isset($_POST['cheque_recieve']) && !empty($_POST['cheque_recieve']) && $_POST['cheque_recieve'] > 0) {
      $acc_code = $this->utility->get_default_acc('CHEQUE_IN_HAND');
      $this->account->set_value4($des, $_POST['cheque_recieve'], "cr", $acc_code, $condition,$_POST['id']);
    }*/
    
    /*if (isset($_POST['credit_card']) && !empty($_POST['credit_card']) && $_POST['credit_card'] > 0) {
      $acc_code = $this->utility->get_default_acc('CREDIT_CARD_IN_HAND');
      $this->account->set_value4($des, $_POST['credit_card'], "cr", $acc_code, $condition,$_POST['id']);
    }*/

    /*if(isset($_POST['credit_card']) && !empty($_POST['credit_card']) && $_POST['credit_card']>0){
      for($x = 0; $x<25; $x++){
        if(isset($_POST['type1_'.$x]) && isset($_POST['amount1_'.$x]) && isset($_POST['bank1_'.$x]) && isset($_POST['no1_'.$x])){
          if(!empty($_POST['type1_'.$x]) && !empty($_POST['amount1_'.$x]) && !empty($_POST['bank1_'.$x]) && !empty($_POST['no1_'.$x])){
            $acc_code = $_POST['acc1_'.$x];
            $this->account->set_value4($des, $_POST['amount1_'.$x], "cr", $acc_code,$condition,$_POST['id']);    
          }
        }
      }  
    }
    
    if (isset($_POST['cheque_issue']) && !empty($_POST['cheque_issue'])) {
      for ($i = 0; $i < 10; $i++) {
        if ($_POST['bank7_' . $i] != "" && $_POST['bank7_' . $i] != "0") {
          $acc_code = $this->utility->get_default_acc('ISSUE_CHEQUES');
          $this->account->set_value4("CHEQUE ACC -'".$_POST['bank7_'.$i]."' ", $_POST['amount7_'.$i], "cr", $acc_code, $condition,$_POST['id']);
        }
      }      
    }
    
    
    if ($condition == 0) {
      $query = $this->db->query("
           SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok 
           FROM `t_check_double_entry` t
           LEFT JOIN `m_account` a ON t.`acc_code` = a.`code`
           WHERE  t.`cl`='" . $this->sd['cl'] . "'  AND t.`bc`='" . $this->sd['branch'] . "'  AND t.`trans_code`='48'  AND t.`trans_no` ='" . $this->max_no . "' AND 
           a.`is_control_acc`='0'");
      
      if ($query->row()->ok == "0") {
        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 48);
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $this->sd['branch']);
        $this->db->delete("t_check_double_entry");
        return "0";
      } else {
        return "1";
      }
    }*/
  }
  
  
  
  
  
  public function check_code()
  {
    $this->db->where('code', $_POST['code']);
    $this->db->limit(1);
    
    echo $this->db->get($this->mtb)->num_rows;
  }
  
  
  
  public function delete()
  {

    
    $this->db->trans_begin();
    /*error_reporting(E_ALL);
    
    function exceptionThrower($type, $errMsg, $errFile, $errLine)
    {
      throw new Exception($errMsg);
    }
    
    set_error_handler('exceptionThrower');
    try { */
      

      if ($this->user_permissions->is_delete('t_voucher_general')) {        

        $this->db->where("sub_no", $_POST['code']);
        $this->db->where("type", $_POST['type2']);
        $this->db->where("bc", $this->sd['branch']);
        $this->db->where("cl", $this->sd['cl']);
        $this->db->limit(1);
        $this->db->update("t_voucher_gl_sum", array("is_cancel" => 1));
        
        // account reverse entry here

        $this->account_cancel(1);

        $update_chq_book = array('status' => 2);

        $this->db->where('cl', $this->sd['cl']);
        $this->db->where("bc", $this->sd['branch']);
        $this->db->where("trans_code", 48);
        $this->db->where("trans_no", $_POST['hid']);
        $this->db->update("t_cheque_book_det",$update_chq_book);
        
        $this->utility->update_chq_book_status();
        
        echo $this->db->trans_commit();
      



      } else {
        echo "No permission to delete records";
        $this->db->trans_commit();
      }
    
    /*}
    catch (Exception $e) {
      $this->db->trans_rollback();
      echo $e->getMessage() . "Operation fail please contact admin";
    }*/
  }
  
  public function select()
  {
    $query = $this->db->get($this->mtb);
    
    $s = "<select name='sales_ref' id='sales_ref'>";
    $s .= "<option value='0'>---</option>";
    foreach ($query->result() as $r) {
      $s .= "<option title='" . $r->name . "' value='" . $r->code . "'>" . $r->code . " | " . $r->name . "</option>";
    }
    $s .= "</select>";
    
    return $s;
  }

    public function get_max_no($paid_acc,$vocuher_type){
        return $this->db->query("SELECT IFNULL(MAX(nno)+1,1) AS `max_no` FROM `t_voucher_gl_sum` V WHERE paid_acc = '$paid_acc' AND `vocuher_type` = '$vocuher_type' ")->row()->max_no;
    }
  
  public function get_max_no_type()
  {
    
    $table_name = $_POST['table'];
    $field_name = $_POST['nno'];
    $type       = $_POST['type'];
    
    if (isset($_POST['hid'])) {
      if ($_POST['hid'] == "0" || $_POST['hid'] == "") {
        $this->db->select_max($field_name);
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $this->sd['branch']);
        $this->db->where("type", $type);
        echo $this->db->get($table_name)->first_row()->$field_name + 1;
      } else {
        echo $_POST['hid'];
      }
    } else {
      $this->db->select_max($field_name);
      $this->db->where("cl", $this->sd['cl']);
      $this->db->where("bc", $this->sd['branch']);
      $this->db->where("type", $type);
      echo $this->db->get($table_name)->first_row()->$field_name + 1;
    }
  }
  
  public function get_data()
  {
    
    
    $sql      = "SELECT 
          a.`code` AS paid_acc_code,
          a.`description` AS paid_acc_des,
          v.`note`,
          v.`sub_no` as nno,
          v.`nno` as hid_nno,
          v.`ref_no` as ref,
          c.`code` AS cat_code,
          c.`description` AS cat_des,
          g.`code` AS groups_code,
          g.`name`,
          v.`type`,
          v.is_cancel,
          v.`ddate`,
          v.`cash_amount`,
          v.`cheque_amount`,
          (v.`cash_amount` + v.cheque_amount) AS tot ,
          v.is_approved
        FROM
          t_voucher_gl_sum v 
        LEFT JOIN m_account a ON a.code = v.paid_acc 
        LEFT JOIN r_sales_category c ON c.code = v.category_id 
        LEFT JOIN r_groups g ON g.code = v.`group_id` 
        WHERE 
          v.type = '" . $_POST['type'] . "'
          AND v.sub_no = '" . $_POST['id'] . "' 
          AND v.bc = '" . $this->sd['branch'] . "' 
          AND v.cl = '" . $this->sd['cl'] . "' 
        LIMIT 1";
        
    $query    = $this->db->query($sql);
    $a['sum'] = $query->first_row();
    
    
    $n_no = 0;
    $n_no = $query->row()->hid_nno;
    
    $sql_cheque  = "SELECT * FROM opt_issue_cheque_det 
                      WHERE cl='" . $this->sd['cl'] . "' AND bc='" . $this->sd['branch'] . "'
                      AND trans_code='48' AND trans_no='$n_no'";
    $queryy      = $this->db->query($sql_cheque);
    $a['cheque'] = $queryy->result();
    
    
    
    $sql      = "SELECT 
            d.`acc_code`,
            a.`description`,
            d.`amount`,
            d.`ref_no`,
            d.v_bc,
            d.v_class
          FROM t_voucher_gl_sum 
          
          JOIN t_voucher_gl_det d
            ON t_voucher_gl_sum.`sub_no` = d.`sub_no` 
              AND t_voucher_gl_sum.`type` = d.`type`
              AND t_voucher_gl_sum.`cl` = d.`cl` 
              AND t_voucher_gl_sum.`bc` = d.`bc`  
           JOIN m_account a ON a.code = d.`acc_code`
        WHERE t_voucher_gl_sum.`type` = '" . $_POST['type'] . "' 
          AND t_voucher_gl_sum.`sub_no` ='" . $_POST['id'] . "'  AND t_voucher_gl_sum.bc='" . $this->sd['branch'] . "' AND t_voucher_gl_sum.cl='" . $this->sd['cl'] . "'
          GROUP BY d.`acc_code` ,d.`v_bc`,d.`v_class` ";
    $query    = $this->db->query($sql);
    $a['det'] = $query->result();
    
    
    echo json_encode($a);
  }
  
  
  
  public function PDF_report(){

    if (isset($_POST['h_v_bc'])){
      if ($_POST['h_v_bc'] != ""){
        $bc = $_POST['h_v_bc'];
      }else{
        $bc = $this->sd['branch'];
      }
    }else{
      $bc = $this->sd['branch'];
    }
    
    $this->db->select(array('name'));
    $r_detail['company'] = $this->db->get('m_company')->result();
    $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $bc);
    $r_detail['branch'] = $this->db->get('m_branches')->result();
    
    $invoice_number      = $this->utility->invoice_format($_POST['r_nno']);
    
    $session_array       = array(      
      $invoice_number
    );

    $r_detail['session'] = $session_array;
    
    $this->db->where("code", $_POST['sales_type']);
    $query = $this->db->get('t_trans_code');
    
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $r_detail['r_type'] = $row->description;
      }
    }
 
    
    $r_detail['type']         = $_POST['type'];
    $r_detail['dt']           = $_POST['dt'];
    $r_detail['qno']          = $_POST['qno'];
    $r_detail['voucher_type'] = $_POST['voucher_type'];
    
    $r_detail['voucher_no']  = $_POST['qno'];
    $r_detail['category_id'] = $_POST['category_id'];
    $r_detail['cat_des']     = $_POST['cat_des'];
    $r_detail['group_id']    = $_POST['group_id'];
    $r_detail['group_des']   = $_POST['group_des'];
    $r_detail['ddate']       = $_POST['r_ddate'];
    $r_detail['tot']         = $_POST['tot'];
    
    
    
    $r_detail['num'] = $_POST['tot'];
    
    $num = $_POST['tot'];
    
    $this->utility->num_in_letter($num);
    
//-------------------------------------------------

    $r_detail['rec'] = convertNum($num);
    ;
    
    $r_detail['page']        = $_POST['page'];
    $r_detail['header']      = $_POST['header'];
    $r_detail['orientation'] = $_POST['orientation'];
    
    $r_detail['acc_code'] = $_POST['acc_code'];
    $r_detail['acc_des']  = $_POST['acc_des'];
    $r_detail['vou_des']  = $_POST['vou_des'];
    
     $sql=" SELECT VD.acc_code, VD.amount, MA.description 
            FROM t_voucher_gl_det VD
            JOIN m_account MA ON VD.acc_code = MA.code 
            WHERE VD.cl='".$this->sd['cl']."' AND VD.bc='".$bc."' AND VD.`type`= '".$_POST['voucher_type']."'  AND VD.`paid_acc` = '".$_POST['r_paid_acc']."' AND VD.`nno` = ".$_POST['r_nno']." ";

    
    $r_detail['dets'] = $this->db->query($sql)->result(); 

    $sql="SELECT * FROM t_voucher_gl_sum 
          WHERE cl='".$this->sd['cl']."' AND bc='".$bc."' 
          AND `nno` = ".$_POST['r_nno']." AND `type`= '".$_POST['voucher_type']."'
          AND paid_acc = '".$_POST['r_paid_acc']."' ";
    
    $r_detail['sum'] = $this->db->query($sql)->result(); 
    
  
    $sql="SELECT * FROM t_cheque_issued WHERE trans_code='48' 
          AND cl='".$this->sd['cl']."' AND bc='".$bc."'
          AND account = '".$_POST['r_paid_acc']."' AND trans_no = '".$_POST['r_nno']."'";
    
    $r_detail['cheque'] = $this->db->query($sql)->result();       



    // $this->db->select(array(
    //   'name'
    // ));
    // $this->db->where("code", $_POST['salesp_id']);
    // $query = $this->db->get('m_employee');
    
    //foreach ($query->result() as $row) {
      //$r_detail['employee'] = $row->name;
   // }
    
    $this->db->select(array(
      'loginName'
    ));
    $this->db->where('cCode', $this->sd['oc']);
    $r_detail['user'] = $this->db->get('u_users')->result();
    
    
    
    $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
  }

  public function get_default_acc(){
    $a = $this->utility->get_acc('cash_acc',$this->sd['bc']);

    $sql = "SELECT * FROM m_account WHERE CODE  = '$a'";
    $query = $this->db->query($sql);
   
     if($query->num_rows()>0){
       $b = $query->result();
     }else{
       $b =2;
     }
    echo json_encode($b);
  }




  //--------------------------- 2016-10-22- ------------------------------------

  
    public function set_paying_acc(){
        if ($_POST['type'] == "cash"){  $a['acc_drop_down'] = $this->get_cash_acc($this->sd['bc']);}
        if ($_POST['type'] == "cheque"){$a['acc_drop_down'] = $this->get_bank_acc(); }
        echo json_encode($a);
    }


    public function get_cash_acc($bc = ""){

        if ($bc == ""){
            $bc = $_POST['bc'];
        }else{
            $bc = $bc;
        }

        $Q = $this->db->query("SELECT B.`cash_acc`,MA.description FROM `m_branches` B

            left join m_account MA on B.cash_acc = MA.code

            WHERE bc = '$bc' LIMIT 1");

        $T  = "<select name='from_acc' id='from_acc'>";
        
        if ($Q->num_rows() >0){
                $T .= "<option value=''>Select</option>";
            foreach ($Q->result() as $R) { 
                $T .= "<option value='".$R->cash_acc."'>".$R->cash_acc." - ".$R->description." </option>";
            }
        }else{
            $T .= "<option value=''>Cash account not added</option>";
        }
        
        $T .= "</select>";

        return $T;
    }


    function set_acc_bal_n_voucher_no(){
        
        $acc_code = $_POST['acc_code'];

$a['data'] = $this->db->query("SELECT IFNULL(MAX(nno)+1,1) AS `max_no`,
0 AS `cash_bal`,
0 AS `chqu_bal`

FROM `t_voucher_gl_sum` V 

WHERE paid_acc = '$acc_code' AND `vocuher_type` = 'gl_vouchers' ")->row();


        echo json_encode($a);
    }

    public function get_acc_list(){
        $q = $_GET['term'];
        $query = $this->db->query("SELECT MA.`code`,MA.`description` FROM m_account MA WHERE MA.`is_control_acc` = 0 AND (MA.`code` LIKE '%$q%' OR MA.`description` LIKE '%$q%') LIMIT 200 ");                
        $ary = array();    
        
        foreach($query->result() as $R){ 
            $ary[] = $R->code." - ".$R->description; 
        }
        
        echo json_encode($ary);
    }


    public function load_vou(){
        
        $acc_code = $_POST['acc_code'];
        $nno      = $_POST['nno'];

        $Q = $this->db->query("   SELECT VS.*, S.`status` AS `st` FROM `t_voucher_gl_sum` VS 
                                  JOIN `t_gen_vou_app_rqsts_sum` S ON VS.`nno` = S.`nno` AND VS.`paid_acc` = S.`paid_acc`
                                  WHERE VS.`vocuher_type` = 'salary_vouchers' AND VS.`nno`='$nno' AND VS.`paid_acc`='$acc_code' 
                                  LIMIT 1 ");

        if ($Q->num_rows() > 0){
            $a['s']   = 1;
            $a['sum'] = $Q->row();
            $a['det'] = $this->db->query("
            SELECT 
              VS.`nno`,
              VS.`cash_amount` as `amount`,
              VS.`bc`,
              B.`name` AS `bc_name`,
              VS.paid_acc,
              VS.`note` as `payee`,
              AC.`description` AS `paid_acc_desc`
            FROM `t_voucher_gl_sum` VS 

            LEFT  JOIN `m_account` AC ON VS.`paid_acc` = AC.`code`
                  JOIN `m_branches` B ON VS.`bc` = B.`bc`

            WHERE VS.`vocuher_type` = 'salary_vouchers' AND VS.`nno` = '$nno' AND VS.`paid_acc` = '$acc_code' ")->result();

          if ($Q->row()->type == "cheque"){
            
            $a['chq'] = $this->db->where(array('trans_code' => 48,'trans_no' => $nno,'issued_to_acc' => $acc_code))->get('t_cheque_issued')->row();

          }else{
            $a['chq'] = 0;
          }


          $this->load->model('user_permissions');        
          
          $a['l1_auth'] = intval($this->user_permissions->is_auth('level_1_authorization'));
          $a['l2_auth'] = intval($this->user_permissions->is_auth('level_2_authorization'));
        
        }else{
            $a['s']   = 0;
        }

        echo json_encode($a);
    }


public function get_bank_acc(){
    
    $Q = $this->db->query("SELECT * FROM m_account MA WHERE MA.`is_control_acc` = 0 AND MA.`is_bank_acc` = 1 ");

    $T  = "<select name='from_acc' id='from_acc'>";
    
    if ($Q->num_rows() >0){
            $T .= "<option value=''>Select</option>";
        foreach ($Q->result() as $R) { 
            $T .= "<option value='".$R->code."'>".$R->code." - ".$R->description." </option>";
        }
    }else{
        $T .= "<option value=''>Bank accounts not added</option>";
    }
    
    $T .= "</select>";

    return $T;

}

private function max_vou_app(){

  return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM `t_gen_vou_app_rqsts_sum`")->row()->max_no;

}



















}