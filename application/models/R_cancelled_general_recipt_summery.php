
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_cancelled_general_recipt_summery extends CI_Model{
    
  private $sd;
  private $mtb;
  
  private $mod = '003';
  
  function __construct(){
    parent::__construct();

    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);
    $this->mtb = $this->tables->tb['t_privilege_card'];
    $this->m_customer = $this->tables->tb['m_customer'];
    $this->t_sales_sum=$this->tables->tb['t_sales_sum'];
    $this->t_previlliage_trans=$this->tables->tb['t_previlliage_trans'];
    $this->t_privilege_card=$this->tables->tb['t_privilege_card']; 
  }
  
  public function base_details(){
      
    $a['table_data'] = $this->data_table();

    return $a;
  }
  

  public function PDF_report(){
        
    $this->db->select(array('name'));
    $r_detail['company'] = $this->db->get('m_company')->result();
    $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $this->sd['branch']);
    $r_detail['branch'] = $this->db->get('m_branches')->result();

    $cl=$this->sd['cl'];
    $bc=$this->sd['branch'];

    $r_detail['page']='A4';
    $r_detail['orientation']='L'; 

    $cluster=$this->sd['cl'];
    $branch=$_POST['branch'];
    $acc=$_POST['acc_code'];
    $no_range_frm=$_POST['t_range_from'];
    $no_range_to=$_POST['t_range_to'];

    $r_detail['acc']=$_POST['acc_code'];
    $r_detail['t_no_from']=$_POST['t_range_from'];
    $r_detail['t_no_to']=$_POST['t_range_to'];

    $r_detail['cluster1']=$cluster;
    $r_detail['branch1']=$branch;
    $r_detail['acc']=$_POST['acc_code'];
    $r_detail['acc_des']=$_POST['acc_code_des'];
    
   

    $date_range=$_POST['from'];
    $acc_code=$_POST['acc_code'];
    $no_range=$_POST['t_range_from'];

     if(isset($_POST['chknumRange']))
    {
      $checkR=1;
    }else{
      $checkR=0;
    }

    if(isset($_POST['chkdate1']))
    {
      $checkD=1;
    }else{
      $checkD=0;
    }

    if($checkD==1)
    {

      $r_detail['dfrom']=$_POST['from'];
      $r_detail['dto']=$_POST['to'];

      $date1="AND ddate between'".$_POST['from']."' AND '".$_POST['to']."'";
    }
    else
    {
      $date1="";

      $r_detail['dfrom']="";
      $r_detail['dto']="";
    }

    
   
    if($acc_code!=""){
      $acc1="AND pettycash_account='".$_POST['acc_code']."'";
    }
    else
      $acc1="";

    if($checkR==1){

      $r_detail['t_no_from']=$_POST['t_range_from'];
      $r_detail['t_no_to']=$_POST['t_range_to'];

      $num_range="AND nno between'".$_POST['t_range_from']."' AND '".$_POST['t_range_to']."'";
      }
    else
    {
      $num_range="";
      $r_detail['t_no_from']="";
      $r_detail['t_no_to']="";
    }
    
    if(!empty($cluster)) 
    {
       $cluster1= "AND cl ='".$_POST['cluster']."'";
    }
    else
     $cluster1="";

    if(!empty($branch)) 
    {
       $branch1= " AND bc ='".$_POST['branch']."'";
    }
    else
     $branch1="";

  
    
    $sql="SELECT 
            (SELECT COUNT(*) FROM t_receipt_gl_sum WHERE `is_cancel` = '1' AND DATE BETWEEN '2017-03-11' AND '2017-03-11' AND cl = '' AND bc = 'KN') AS counts,
  RGS.`nno`,
  RGS.`date`,
  RGS.`type`,
  RGS.`receipt_desc` AS `note`,
  RGS.`paid_to_acc` AS `paid_acc`,
  RGS.`cheque_amount`,
  RGS.`cash_amount`,
  MA.`description`,
  '' AS cl,
  RGS.bc AS bc,
  MBC.`name` AS bc_name,
  '' AS cl_description  
            FROM `t_receipt_gl_sum` AS RGS
            
            INNER JOIN m_account AS MA ON MA.code = RGS.`paid_to_acc`
            INNER JOIN `m_branches` MBC ON RGS.bc = MBC.bc             

            WHERE `is_cancel` = '1'";

            if($checkD==1)
        {
           $sql.=" AND RGS.date between '".$_POST['from']."' AND '".$_POST['to']."'";
        }
        if(!empty($_POST['acc_code']))
        {
           $sql.=" AND MA.`code` = '$acc'";
        }
        
        
       if(!empty($branch))
        {
            $sql.=" AND  RGS.bc = '$branch'";
        }
         
        if(!empty($_POST['t_range_from']) && empty($_POST['t_range_to']))
        {
           $sql.=" AND  RGS.nno >= '$no_range_frm'";
        }
        
         if(!empty($_POST['t_range_to']) && empty($_POST['t_range_from']))
        {
           $sql.=" AND RGS.nno <= '$no_range_to'";
        }

        if(!empty($_POST['t_range_to']) && !empty($_POST['t_range_to']))
        {
           $sql.=" AND  RGS.nno BETWEEN  '$no_range_frm' AND '$no_range_to'";
        }
         $sql.=" ORDER BY RGS.bc,RGS.nno ASC";
            
        $query=$this->db->query($sql);


       
    $r_detail['sum']=$this->db->query($sql)->result(); 
    

    if($this->db->query($sql)->num_rows()>0)
    {
        $this->load->view($_POST['by'].'_'.'pdf',$r_detail);
    }
    else
    {
        echo "<script>alert('No Data');window.close();</script>";
    }
  }
}