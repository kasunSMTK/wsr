<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_balance_sheet extends CI_Model {
    
    private $tb_items;
    private $tb_storse;
    private $tb_department;
    private $sd;
    private $w = 297;
    private $h = 210;
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
        $this->load->library('useclass');
        
        $this->sd = $this->session->all_userdata();
		$this->load->database($this->sd['db'], true);
	
        $this->tb_items = $this->tables->tb['m_items'];
        $this->tb_storse = $this->tables->tb['m_stores'];
    }
    
    public function base_details()
    {
    	$this->load->model('m_stores');
    	$a['store_list']=$this->m_stores->select3();
    	$this->load->model('m_branch');
    	$a['branch']=$this->get_branch_name();
    	return $a;
	}


	public function get_branch_name()
	{
		$this->db->select('name');
		$this->db->where('bc',$this->sd['branch']);
		return $this->db->get('m_branch')->row()->name;
	}



	public function PDF_report(){

		ini_set("max_execution_time",600);
		
		$fd = $_POST['from'];
		$td = $_POST['to'];


		// asset

		$sql2="SELECT m_account.code, m_account.description, IFNULL(SUM( t_account_trans.dr_Amount -t_account_trans.cr_Amount),0) AS bal ,m_account_type.report , m_account_type.rtype ,m_account_type.heading FROM 	m_account INNER JOIN m_account_type ON (m_account.type = m_account_type.code) INNER JOIN t_account_trans ON (t_account_trans.Acc_Code = m_account.code) WHERE  m_account_type.report ='1'     AND m_account_type.rtype = '3' AND (t_account_trans.dDate <= '$td') AND (m_account.control_Acc IS NULL OR m_account.control_Acc ='') GROUP BY m_account_type.heading , m_account.code ORDER BY m_account_type.`heading`,m_account.code";


		// liabilities

		$sql1 = "SELECT m_account.code, m_account.description, IFNULL(SUM( t_account_trans.cr_Amount -t_account_trans.dr_Amount),0) AS bal ,m_account_type.report , m_account_type.rtype ,m_account_type.heading FROM 	m_account INNER JOIN m_account_type ON (m_account.type = m_account_type.code) INNER JOIN t_account_trans ON (t_account_trans.Acc_Code = m_account.code) WHERE  m_account_type.report ='1'     AND m_account_type.rtype <> '3' AND (t_account_trans.dDate <= '$td') AND (m_account.control_Acc IS NULL OR m_account.control_Acc ='') GROUP BY m_account_type.heading, m_account.code ORDER BY m_account_type.heading, m_account_type.`heading`,m_account.code";

		// profit of the period

		$sql3 = "SELECT  IFNULL(SUM(t_account_trans.cr_Amount - t_account_trans.dr_Amount ),0) AS bal FROM 	m_account INNER JOIN m_account_type ON (m_account.code = m_account_type.code) INNER JOIN t_account_trans ON (t_account_trans.Acc_Code = m_account.code) WHERE  m_account_type.report ='2'     AND (t_account_trans.dDate BETWEEN '$fd' AND '$td') AND (m_account.control_Acc IS NULL OR m_account.control_Acc ='')";


		//

		$sql4 = "SELECT ((SUM(`income`) - SUM(`exp`)) + SUM(othr_incom)) AS `profit_or_loss`  FROM (

			SELECT SUM(IFNULL(C.bal,0)) AS `income` , 0 AS `exp` , 0 AS `othr_incom`
			FROM m_branches A 
			CROSS JOIN m_account_type B 
			LEFT JOIN ( 	SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate < '$fd' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code 
			WHERE B.`rtype` = 1 AND B.code IN ('10101','10102','10103')  

			UNION ALL

			SELECT 0 AS `income`, SUM(IFNULL(C.bal,0)) AS `exp` , 0 AS `othr_incom`
			FROM m_branches A 
			CROSS JOIN m_account_type B 
			LEFT JOIN ( 	SELECT bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal` FROM t_account_trans WHERE ddate < '$fd' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code 
			WHERE B.`rtype` = 2  

			UNION ALL

			SELECT 0 AS `income`,0 AS `exp`,SUM(IFNULL(C.bal,0)) AS `othr_incom` 
			FROM m_branches A 
			CROSS JOIN m_account_type B 
			LEFT JOIN ( 	SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate < '$fd' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code 
			WHERE B.`rtype` = 1 AND NOT B.code IN ('10101','10102','10103') 
			 
			) a ";

		$r_detail['asset'] = $this->db->query($sql2)->result();	
		$r_detail['liabilities'] = $this->db->query($sql1)->result();

		if ($this->db->query($sql4)->num_rows() > 0){
			$r_detail['profit0']= floatval($this->db->query($sql4)->row()->profit_or_loss);
		}else{
			$r_detail['profit0'] = 0;
		}

		$r_detail['profit'] = $this->db->query($sql3)->result();

		if($this->db->query($sql2)->num_rows()>0){
			
			$r_detail['fd'] = $fd;
			$r_detail['td'] = $td;

			$this->load->view($_POST['by'].'_'.'pdf',$r_detail);
		}else{
			echo "<script>alert('No Data');window.close();</script>";
		}


	}

















































































































	/*public function PDF_report()
	{
		$this->db->select(array('name'));
        $r_detail['company'] = $this->db->get('m_company')->result();
        $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $this->sd['branch']);
        $r_detail['branch'] = $this->db->get('m_branches')->result();


		$cl=$this->sd['cl'];
		$bc=$this->sd['branch'];

		$r_detail['store_code']=$_POST['stores'];	
		$r_detail['type']=$_POST['type'];        
		$r_detail['dd']=$_POST['dd'];
		$r_detail['qno']=$_POST['qno'];

		$r_detail['page']=$_POST['page'];
		$r_detail['header']=$_POST['header'];
		$r_detail['orientation']=$_POST['orientation'];
		$r_detail['dfrom']=$_POST['from'];
        $r_detail['dto']=$_POST['to'];
        $r_detail['trans_code']=$_POST['t_type'];
        $r_detail['trans_code_des']=$_POST['t_type_des'];
        $r_detail['trans_no_from']=$_POST['t_range_from'];
        $r_detail['trans_no_to']=$_POST['t_range_to'];
        $cluster=$_POST['cluster'];
		$branch=$_POST['branch'];

		$sql="SELECT IFNULL( SUM( t.`Dr_Amount`) -SUM(t.`Cr_Amount`  ),0) AS bal  
			FROM `t_account_trans` t
			LEFT JOIN `m_account` m ON t.`Acc_Code` =m.`code`
			LEFT JOIN `m_account_type` a ON m.`code` = a.`code`
			WHERE m.`control_Acc`=''
			AND a.report=1 
			AND t.ddate<= '".$_POST['to']."'";
		
		if(!empty($cluster))
		{
            $sql.=" AND t.cl = '$cluster'";
        }
        if(!empty($branch))
        {
            $sql.=" AND t.bc = '$branch'";
        }  
		
		$r_detail['bal_sheet1']=$this->db->query($sql)->result();	

		$sql2="SELECT  m_account.code,
				       m_account.description,
				       IFNULL(SUM(t_account_trans.dr_Amount-t_account_trans.cr_Amount),0) AS balance
				FROM	m_account
				INNER JOIN t_account_trans ON (t_account_trans.Acc_Code=m_account.code)
				INNER JOIN m_account_type ON (m_account.code = m_account_type.code)
				WHERE (  m_account_type.report =1 
					 AND  m_account_type.rtype <> 'Assets'
					 AND  (m_account.control_Acc='' OR  m_account.control_Acc IS NULL) 
				     AND t_account_trans.ddate<= '2015-01-15' 
				     AND m_account.code='10101' ";

		if(!empty($cluster))
		{
            $sql2.=" AND t_account_trans.cl = '$cluster'";
        }
        if(!empty($branch))
        {
            $sql2.=" AND t_account_trans.bc = '$branch'";
        }  		

        $sql2.=" ) GROUP BY m_account.code ORDER BY m_account_type.`heading`,m_account.code";

		$r_detail['bal_sheet2']=$this->db->query($sql2)->result();			

		if($this->db->query($sql)->num_rows()>0)
		{
			$this->load->view($_POST['by'].'_'.'pdf',$r_detail);
		}
		else
		{
			echo "<script>alert('No Data');window.close();</script>";
		}
	}*/















}	
?>