<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_daily_cash_report_all_branch extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
		return $a;
    }   

    public function PDF_report($_POST_){

        $fd = $_POST_['from_date'];
        $td = $_POST_['to_date'];

        // if (isset($_POST['bc'])){

        //     if ( $_POST['bc'] == "" ){
        //         $BC_Q = "";    
        //     }else{
        //         $bc     = $_POST['bc'];
        //         $BC_Q   = "T.`bc` = '$bc' AND";    
        //     }

        // }else{            
        //     $bc     = $this->sd['bc'];
        //     $BC_Q   = "T.`bc` = '$bc' AND";
        // }


        if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";$BC_Q = "";  
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $BC_Q   = "   T.bc IN ($bc) AND ";
            $bc   = "   L.bc IN ($bc) AND ";

        }
    }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND T.`billtype`='".$_POST['r_billtype']."'";
    }

        

        // inner loop
        $Q  = $this->db->query("SELECT T.bc, T.`description`, T.`entry_code`, T.`billno`, T.`billtype`, T.`dr_amount` AS `receipts`, T.`cr_amount` AS `payments`, T.`action_date` FROM `t_account_trans` T 

            LEFT JOIN `m_branches` B ON T.`acc_code` = B.`cash_acc` AND T.`bc` = B.`bc` 

            WHERE $BC_Q T.`ddate` = '$td' $billtype
            ORDER BY T.bc ");

        $nl = 0;        

        foreach ($Q->result() as $rr) { 
            $bc_arr[] = $rr->bc; 
        }
        
        $bc_arr = array_values( array_flip( array_flip( $bc_arr ) ) );


        if($Q->num_rows() > 0){

            $r_data['bc_list'] = $bc_arr;
            $r_data['list'] = $Q->result();
            $r_data['fd'] = $fd;
            $r_data['td'] = $td;
            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{
            echo "<script>location='default_pdf_error'</script>";
        }

    }
    
}