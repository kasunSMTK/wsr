<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Controller {

	private $sd;

	function __construct(){
		parent::__construct();		
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->database();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function index(){

		$R = $this->db->query("SELECT `Name` FROM `m_company`")->row();
		$company_name = ucwords(strtolower($R->Name));		

		$user_branch = isset($this->sd['user_branch']) ? $this->sd['user_branch'] : "";
		
		if (isset($this->sd['bc_no'])){
			$bc_no 		= $this->sd['bc_no'];
		}else{
			$bc_no		= "";
		}

		$company_nameT= $company_name;
		$company_name = str_replace("pvt", "Pvt", $company_name) ." <span class='header_bc_title'>$user_branch (<span style='color:#000000'>$bc_no</span>)</span>";
			
		if (isset($this->sd['is_login']) == true){
			
			if (isset($_GET['action'])){
				$page = $_GET['action'];			
				$data['page_title'] = $company_nameT . " - ". ucwords(strtolower(str_replace("_", " ", $_GET['action'] ))) ;
				$data['js_file_name'] = $_GET['action'];
				$data['company_name'] = $company_name;
				$data['is_login_view'] = false;
				$data['isAdmin'] = $this->sd['isAdmin'];
				$data['oc'] = $this->sd['oc'];
				$data['isHO'] = $this->sd['isHO'];

			}else{
				$page = "index";			
				$data['page_title'] = $company_nameT . " - " . ucwords(strtolower(str_replace("_", " ", $R->Name ))) ;
				$data['js_file_name'] = "index";
				$data['company_name'] = $company_name;
				$data['is_login_view'] = false;				
				$data['isAdmin'] = $this->sd['isAdmin'];
				$data['oc'] = $this->sd['oc'];
				$data['isHO'] = $this->sd['isHO'];
			}

			$this->load->model('user_permissions');			

			$data['menu_item_visibility'] = $this->user_permissions->check_menu_item_visibility();
			$user_role_status 			  = $this->user_permissions->user_role_status();
			

			$data['hp_msg'] = $this->get_hp_msg();
			$data['bk_opt'] = $this->get_back_date_option_status();
			$data['ud_opt'] = $this->get_receipt_update_status();
			$data['User'] 	= $this->sd['user_des'];

			if ($user_role_status){
				
				if ($this->user_permissions->is_view($page) > 0){					

					$this->load->view("header",$data);
					$this->load->model($page);			
					$this->load->view($page,$this->{$page}->base_details());

				}else{
										
					$this->login_UI($company_nameT,$company_name);

				}

			}else{
				$this->load->view("header",$data);
				$this->load->view('user_role_status','User role not added');
			}
		
		}else{

			if (isset($_GET['action'])){				
				$this->login_UI($company_nameT,$company_name);
			}else{
				$this->login_UI($company_nameT,$company_name);
			}

		}
	
	}

	private function login_UI($company_nameT,$company_name){
		
		$this->load->model('login_ui');			
		
		$data['page_title'] = $company_nameT . " - Login";
		$data['base_details'] = $this->login_ui->base_details();
		$data['company_name'] = $company_name . " - Login";
		$data['is_login_view'] = true;
		$data['js_file_name'] = "login_ui";
		



		// $data['reg_required'] = isset($this->sd['reg_required']) ? $this->sd['reg_required'] : false;
		// remove below line and S_users.php line 80, to enable pc reg option
		$data['reg_required'] = false;

		$this->load->view("header_blank",$data);			
		$this->load->view('login_ui', $data );
	}

	public function get_hp_msg(){

		$Q = $this->db->query("SELECT * FROM `msg` M WHERE M.`hg_priority` = 1 AND id = ( SELECT MAX(id) FROM `msg` )");

		if ($Q->num_rows() > 0){
			return "<div class='tmp_msg_header'>".$Q->row()->msg."</div>";
		}else{
			return "";
		}

	}

	public function get_back_date_option_status(){

		$BC = $this->sd['bc'];

		$dd = '"';
		$d  = "'";

		$Q = $this->db->query("SELECT IF (ISNULL(MAX(backdate_upto)),1,0) AS `no_data`,MIN(backdate_upto) AS `backdate_upto`  FROM `m_branch_backdate` WHERE `status` = 'active' AND REPLACE(bc,$dd$d$dd,$dd$dd) = '$BC' AND LEFT(`valid_upto`,10) >= '".$this->sd['current_date']."' ");

		if ($Q->row()->no_data == 0){
			
			if ( $this->sd['current_date'] <> date('Y-m-d') ){
				return "<div class='bc_back_date_opt bkdt_opt_red'>CAUTION : You changed the current date to a different  date</div>";
			}else{
				return "<div class='bc_back_date_opt'>Back date option enable for your branch.</div>";
			}
			

		}else{
			if ( $this->sd['current_date'] <> date('Y-m-d') ){
				return "<div class='bc_back_date_opt bkdt_opt_red'>CAUTION : You changed the current date to a different date</div>";
			}else{
				return '';
			}
		}

	}


	public function get_receipt_update_status(){

		$BC = $this->sd['bc'];

		$dd = '"';
		$d  = "'";

		$Q = $this->db->query("SELECT IF (ISNULL(MAX(backdate_upto)),0,1) AS `edit_status`,bc,valid_upto  FROM `m_branch_receipt_edit_control` WHERE `status` = 'active' AND REPLACE(bc,$dd$d$dd,$dd$dd) = '$BC' AND LEFT(`valid_upto`,10) >= '".$this->sd['current_date']."' ");

		if ($Q->row()->edit_status == 1){		
			
			return "<div class='bc_back_date_opt' style='background-color: #C70039;color:#ffffff'>Receipt update option enable for your branch.</div>";			

		}else{
			
			return '';
			
		}

	}


	public function get_data(){
		$this->load->model($this->uri->segment(3));
		$this->{$this->uri->segment(3)}->load();
    }

    public function auth_check(){

    	$com_id = $_POST['com_id'];

    	$a['auth_stat'] = 0;

    	if ($com_id == ''){
    		$a['COM_ID'] = $this->db->query("SELECT IFNULL(MAX(com_id)+1,1) AS `com_id` FROM `com_auth`")->first_row()->com_id;
    		$x = 1;
    	}else{
    		$a['COM_ID'] = $com_id;
    		$x = 0;
    	}
    	
    	$a['LOGIN_SESSION_ID'] = session_id();
    	
    	$AUTH_CODE 				= substr(md5(date("Y-m-d H:i:s")), 2, 20);
    	$AUTH_HO_CODE 			= substr($AUTH_CODE, 10,10);
    	$a['AUTH_YOUR_CODE'] 	= substr($AUTH_CODE, 0,10);

    	$data['com_id'] 		= $a['COM_ID'];
    	$data['session_id']		= $a['LOGIN_SESSION_ID'];
    	$data['auth_code'] 		= $AUTH_CODE;
    	

    	if ($x == 1){
    		$Q = $this->db->insert("com_auth",$data);
    	}else{

    		$Q3 = $this->db->select(array('auth_code','isAuth'))->where('com_id',$com_id)->get('com_auth');
    		

    		if ($Q3->num_rows() > 0){
    			
    			$a['auth_stat'] = intval( $Q3->first_row()->isAuth );
    			
    			$Q = $this->db->where("com_id",$com_id)->set(array(	'session_id'=>$data['session_id'], 'auth_code' =>$AUTH_CODE  	) )->limit(1)->update('com_auth');

    		}else{
    			$Q = $this->db->insert("com_auth",$data);
    		}

    	}


    	echo json_encode($a);

    }

    public function reg_com(){

    	$com_id 	= $_POST['com_id'];    	
    	$reg_code 	= $_POST['reg_code'];

    	$Q = $this->db->query("SELECT * FROM com_auth WHERE com_id = '$com_id' AND RIGHT(auth_code,10) = '$reg_code' ");

    	if ( $Q->num_rows() > 0 ){
    		$Q = $this->db->where("com_id",$com_id)->set(array('bc'=>$Q->row()->bc,'isAuth'=>1))->limit(1)->update('com_auth');
    		$a['s'] = 1;
    		$this->session->set_userdata(array("reg_required"=>false));
    	}else{
    		$a['s'] = 0;
    	}

    	echo json_encode($a);

    }

	public function login(){
		$this->load->model('s_users');	
		echo $this->s_users->authenticate();		
	}

	public function logout(){
		$sd = array("is_login"=>false);		
	    $this->session->set_userdata($sd);
		$this->session->unset_userdata("oc");
		$this->session->unset_userdata("bc");
		$this->session->unset_userdata("db");
		$this->session->unset_userdata("is_login");		
		redirect("");
    }

    public function save(){		
		
		$mod = $this->uri->segment(3);	
		$this->load->model($mod);
		$this->{$mod}->save();	
		
	}

	public function delete(){		
		
		$mod = $this->uri->segment(3);	
		$this->load->model($mod);
		$this->{$mod}->delete();	
		
	}

	public function load(){		
		
		$mod = $this->uri->segment(3);	
		$this->load->model($mod);
		$this->{$mod}->load();	
		
	}

	public function load_data(){
		$mod = $this->uri->segment(3);
		$mod2 = $this->uri->segment(4);
		$this->load->model($mod);
		
		if(isset($_GET['echo'])){
		    if($_GET['echo'] == "true"){
			echo json_encode($this->{$mod}->{$mod2}());
		    }else{
			$this->{$mod}->{$mod2}();
		    }
		}else{
		    $this->{$mod}->{$mod2}();
		}
    }


}


