<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

class Reports extends CI_Controller {

    private $session_data;

    function __construct() {

        parent::__construct();



        $this->session_data = $this->session->all_userdata();

        $this->load->library('pdf');
    }

    function generate() {
        $this->load->model($_POST['by']);
        $this->{$_POST['by']}->PDF_report($_POST['r_no'],$_POST['fullmonth_int']);
    }

    // 2016-10-18

    /*function generate() {
        $this->load->model($_POST['by']);
        
        if (isset($_POST['r_no'])){
            $this->{$_POST['by']}->PDF_report($_POST['r_no'],$_POST['fullmonth_int']);
        }else{
            $this->refresh_hit_message();
        }
    }*/



    function generate_i() {        
        $this->load->model($_POST['by_i']);
        if (isset($_POST['r_no_i'])){
            $this->{$_POST['by_i']}->PDF_report($_POST['r_no_i'],$_POST['fullmonth_int']);
        }else{
            $this->refresh_hit_message();
        }
    }

    function generate_r() {
        if ( isset($_POST['f_hour']) ){
            if ( $_POST['f_hour'] != "" || $_POST['f_minute'] != "" ){            
                $_POST['ftime'] = str_pad($_POST['f_hour'], 2, "0", STR_PAD_LEFT).":".str_pad($_POST['f_minute'], 2, "0", STR_PAD_LEFT).":00";
                $_POST['ttime'] = str_pad($_POST['t_hour'], 2, "0", STR_PAD_LEFT).":".str_pad($_POST['t_minute'], 2, "0", STR_PAD_LEFT).":00";
            }else{
                $_POST['ftime'] = "";
                $_POST['ttime'] = "";    
            }
        }else{
            $_POST['ftime'] = "";
            $_POST['ttime'] = "";
        }
        if($_POST['by']==""){
            $_POST['by']="rpt_pawnings";
        }
        $this->load->model($_POST['by']);
        $meHv= new  $_POST['by'];
        if(method_exists($meHv, 'PDF_report')){
        $this->{$_POST['by']}->PDF_report($_POST);
        }
        else{
            $this->default_pdf_error();            
        }
    }

    function default_pdf_error() {
         $this->load->view('default_pdf_error_pdf');
    }

    function refresh_hit_message(){
        echo "<script> var counter = 15; var interval = setInterval(function() {counter--; document.getElementById('show_count').innerHTML = counter + ' seconds'; if (counter == 0) {clearInterval(interval); close(); } }, 1000); </script> <span style='font-size:24px'>Please regenerate report using print button. This page will close in <span id='show_count'>15 seconds</span></span> "; 
    }

    function generate_fund_tr_voucher() {        
        $this->load->model($_POST['by']);        
        $this->{$_POST['by']}->PDF_report($_POST);        

    }



}
