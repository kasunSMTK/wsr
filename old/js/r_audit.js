$(document).ready(function(){

	$("#btnGenerateEx").click(function(){

		var fd = $('#from_date').val().replace('-','/');
		var td = $('#to_date').val().replace('-','/');

		if(fd <= td){

			if ($('input[type=radio]').is(":checked")){
				$("#from_date_ex").val($('#from_date').val());
				$("#to_date_ex").val($('#to_date').val());
		        $("#by_ex").val($("input[type='radio'][name='by']:checked").val());		

		        $("#chk_au_ex").val(	$(".chk_au").is(":checked")	);
		        $("#chk_unau_ex").val(	$(".chk_unau").is(":checked") );

		        $("#by_bc").val($("#bc :selected").val());

				$("#print_excel_form").attr("action","index.php/excelReports/generate").submit();
			}else{
				showActionProgress("");			
				showMessage("e","Please select an option to view report");
			}

		}else{
			showActionProgress("");			
			showMessage("e","Invalid date selection");
		}

    });


	$('#bc option[value=""]').remove();
	$('#bc').prepend("<option value='' selected='selected'>All Branches</option>").css("font-size",'14px');

	$('#bc').change(function(){
		$(this).parent().find('input[type="radio"]').attr("checked",true);
	});

	$("#btnGenerate").click(function(){

		var fd = $('#from_date').val().replace('-','/');
		var td = $('#to_date').val().replace('-','/');

		if(fd <= td){

			if ($('input[type=radio]').is(":checked")){
				$("#print_report_pdf").submit();
			}else{
				showActionProgress("");			
				showMessage("e","Please select an option to view report");
			}

		}else{
			showActionProgress("");			
			showMessage("e","Invalid date selection");
		}

	});

});