$.fn.center = function(){

    var h = $(this).height();
    var w = $(this).width();
    
    h = (h - $(window).height())/2;
    w = (w - $(window).width())/2;
    
    if(h<0){ h = h*-1; }
    if(w<0){ w = w*-1; }
    
    $(this).css("top", h);
    $(this).css("left", w);
    $(this).css("display", "block");
}   

var ove = true; var d = false; var to; var fcid, scid; var banks; var tab_id = 0;
var time_out;


$(document).on("click",".update_notifi_action",function(){
    
    var notifi_id   = $(this).attr("notifi_id");
    var redirect_to = $(this).attr("redirect_to");
    
    $.post('index.php/main/load_data/notifications/update_notifi_action',{
        notifi_id: notifi_id
    }, function(D) {        

        if (D.s == 1){

            history.pushState({},null,redirect_to);
            location.reload();

        }else{
            alert("Notification loading error, please re try");
        }

    },"json");

});


$(document).ready(function(){
    // $(document).bind("contextmenu",function(e) {
    //  e.preventDefault();


    $(document).keypress(function(e){
        if(e.keyCode ==119){
            //$("#btnSave,#btnSave1,#btnSave2").click();
        }
        if(e.keyCode ==123){
            $("#btnReset,#btnResett").click();
        }
        if(e.keyCode ==113){
            $("#print_pdff").submit();
        }
        if(e.keyCode ==27){
            $("#pop_close").click();
            $("#pop_close6").click();
            $("#pop_close2").click();
            $("#pop_close3").click();
            $("#pop_close4").click();
            $("#pop_close7").click();
            $("#pop_close10").click();
            $("#pop_close11").click();
            $("#pop_close12").click();
            $("#pop_close13").click();
            $("#pop_close14").click();
            $("#pop_close15").click();
            $("#pop_close_find").click();
        }
    });

    $("#approve4").css('display','none');
    $("#light").css("display","none");
    $("#fade").css("display","none");



    $("#close_msg_box").click(function(){
        $("#msg_box").slideUp();
    });

    $("#close_det_box").click(function(){
        $("#det_box").slideUp();
    });
    

    $("#pop_close").click(function(){
        $("#serch_pop").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close10").click(function(){
        $("#serch_pop10").css("display", "none");
        $("#blocker").css("display", "none");
        $("#blocker4").css("display", "none");
    });

    $("#pop_close11").click(function(){
        $("#serch_pop11").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close12").click(function(){
        $("#serch_pop12").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close13").click(function(){
        $("#serch_pop13").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close14").click(function(){
        $("#serch_pop14").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close15").click(function(){
        $("#serch_pop15").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close_find").click(function(){
        $("#serch_pop_find").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close6").click(function(){
        $("#serch_pop6").css("display", "none");
        $("#blocker").css("display", "none");
    });

    $("#pop_close7").click(function(){
        $("#serch_pop7").css("display", "none");
        $("#blocker").css("display", "none");
        $("#blocker2").css("display", "none");
    });

    $("#pop_close2").click(function(){
        $("#serch_pop2").css("display", "none");
        $("#blocker").css("display", "none");
        $("#blocker2").css("display", "none");
    });

    $("#pop_close3").click(function(){
        $("#serch_pop3").css("display", "none");
        $("#blocker3").css("display", "none");
    });
    
    $("#pop_close4").click(function(){
        $("#serch_pop4").css("display", "none");
        $("#blocker4").css("display", "none");
        $("#blocker").css("display", "none");
        $("#pop_search4").val("");
    });
    

    $(".SpRemove").keydown(function (e) {
        if (e.keyCode == 32) {
            return false;
        }
    });


    $(".NumOnly").keypress(function (event) {
      if (((event.which < 48 || event.which > 57) &&
          (event.which != 0 && event.which != 8))) {
        event.preventDefault();
}
});

    $(".ltnumOnly").keypress(function (event) {
      if (event.which  > 31 && (event.which  < 48 || event.which  > 57)&&
        (event.which < 97 || event.which > 122)) {
        event.preventDefault();
}
});

    $(".amount").keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
            ((event.which < 48 || event.which > 57) &&
                (event.which != 0 && event.which != 8))) {
            event.preventDefault();
    }

    var text = $(this).val();
    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 2) &&
        (event.which != 0 && event.which != 8) &&
        ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
}

});

    $(".amount").blur(function(){
        var text = $(this).val();        
        CheckMxLngth($(this),2);
        if (addZroToEnd(text,2)!="") {$(this).val(addZroToEnd(text,2));}; 
        CheckMxLngth($(this),2);        
    });

    $(".FloatVal").keypress(function (event) {

        if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
            ((event.which < 48 || event.which > 57) &&
                (event.which != 0 && event.which != 8))) {
            event.preventDefault();
    }

    var text = $(this).val();
    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 2) &&
        (event.which != 0 && event.which != 8) &&
        ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
}

});


    $(".FloatVal").blur(function(){
        var text = $(this).val();
        CheckMxLngth($(this),2);
        if (addZroToEnd(text,2)!="") {$(this).val(addZroToEnd(text,2));}; 
        CheckMxLngth($(this),2);
    });


    $(".FloatVal3").keypress(function (event) {

        if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
            ((event.which < 48 || event.which > 57) &&
                (event.which != 0 && event.which != 8))) {
            event.preventDefault();
    }

    var text = $(this).val();
    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 3) &&
        (event.which != 0 && event.which != 8) &&
        ($(this)[0].selectionStart >= text.length - 3)) {
        event.preventDefault();
}

});

    $(".IntVal").keypress(function (event) {



        if (
            ((event.which < 48 || event.which > 57) &&
                (event.which != 0 && event.which != 8))) {
            event.preventDefault();
    }

    var text = $(this).val();

    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 0 ) &&
        (event.which != 0 && event.which != 8) &&
        ($(this)[0].selectionStart >= text.length - 0)) {
        event.preventDefault();
}

});


    $(".FloatVal3").blur(function(){
        var text = $(this).val();
        CheckMxLngth($(this),3);
        if (addZroToEnd(text,3)!="") {$(this).val(addZroToEnd(text,3));}; 
        CheckMxLngth($(this),3);
    });

// PDF Iframe
$("#Ifclose, #repDiv").click(function(){
    closePdf();
})






});


function CheckMxLngth(ThClas,FltLnth){

    FltLnth=parseInt(FltLnth); 

    var adDot="";
    var text = ThClas.val(); 
    var mxLength=ThClas.attr("maxlength")-FltLnth;

    if (FltLnth!=0) {adDot="."+Array(FltLnth+1).join("9")};

    if(!(isNaN(mxLength))){

        var ThLength=text.split(".");
        var ThCnMax=Array(mxLength).join("9")+adDot;
        if(!(ThLength[0].length<mxLength)){
            // ThClas.val(ThCnMax);  
            ThClas.val("");  
            showActionProgress("Loding...");              
            showMessage("e","Error, Max is "+ThCnMax); 

        }
        else{
            closeActionProgress();        
        }
    }

}

function addZroToEnd(text,Zroz){

    text=parseFloat(text);    
    Zroz=parseInt(Zroz); 
    var retVal="";    
    if(text!="" && !isNaN(text)){    
        retVal=text.toFixed(Zroz);
    }



    // var retVal="";
    // // var ptn0=/^\d+(?:\.\d{0})$/;
    // // var ptn1=/^\d+(?:\.\d{1})$/;
    // var ptn2=/^\d{0}(\.\d{0})$/;
    // var ptn5="^\\d+(?:\\.\\d{"+Zroz+"})$";
    // var ptn5=new RegExp(ptn5);
    // var ptn3=/^\d*$/;      
    // var ptn4=/^\.\d*$/;  

    // if(text!="" && !isNaN(text)){
    //     if(!(ptn5.test(text))){
    //         if((ptn3.test(text))){
    //             retVal=(text+'.');
    //         }
    //         else if((ptn4.test(text)) || (ptn2.test(text))){
    //             retVal=('0'+text);
    //         }
    //         else
    //         {
    //             var ThTxt=text.split(".");
    //             retVal=ThTxt[0]+"."+pad(ThTxt[1],Zroz);
    //         }

    //     var ThTxt=retVal.split(".");
    //     padVal=pad(ThTxt[1],Zroz);
    //     retVal=parseInt(ThTxt[0])+"."+padVal;         

    //     }
    //     else {
    //         /*** If have Length Problum Delete this 2 lines*/            
    //         var ThTxt=text.split(".");
    //         retVal=parseInt(ThTxt[0])+"."+ThTxt[1];  
    //     }

    // }
    // else{

    //     //retVal=(text+'0.00');
    // }

    return retVal;
}

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad(str+"0", max) : str;
}


function set_cid(id){
    id = id.split('_');
    fcid = id[0];
    scid = id[1];
}


function set_t_body(){
    var x = $(window).width()-50;
    $("#t_body").css("width", x);
    $("#t_body").css("top", 130);
    $("#t_body").css("left", 25);
}
function delay(name){
    clearTimeout(time_out);
    time_out=setTimeout(function() {
        name();
    }, 1000);
}


function m_round(val){
    var number = Math.round(val * Math.pow(10, 2)) / Math.pow(10, 2);
    return number.toFixed(2);
}


function loding(){
    if($("#blanket").css("display")=="none"){
       to = setTimeout("error_maz()", 10000);
       d = true;
       $("#blanket").css("display", "block");
   }else{
       clearTimeout(to);
       d = false;
       $("#blanket").css("display", "none");
   }
}

function center(div){
    var h = $(div).height();
    var w = $(div).width();
    
    h = (h - $(window).height())/2;
    w = (w - $(window).width())/2;
    
    if(h<0){ h = h*-1; }
    if(w<0){ w = w*-1; }
    
    $(div).css("top", h);
    $(div).css("left", w);
    $(div).css("display", "block");
}






function sucess_msg(){
    alert("Save Completed");
    location.href="";
}

function delete_msg(){
    alert("Record Deleted Successfully");
    location.href="";
}

// PDF Iframe
function showPdf(){
    $("#repDiv").show();    
    $(".msg_pop_up_bg").fadeIn(300);

}

function closePdf(){
    $("#repDiv").hide();
    $(".msg_pop_up_bg").fadeOut(300); 
}


function make_round(a){
    //return Math.round(a);

    return parseInt(a / 5) * 5 + 5 ;

    

    
}