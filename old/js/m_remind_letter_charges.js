$(document).ready(function(){

	load_list();

	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_areaname").val());
	});




});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Area saving success");
					clear_form();
					load_list();					
/*				}else if (D == 2){					
					showMessage("e","Error, Duplicate Code");*/
				}else{					
					showMessage("e",D.m);
				}
			}

		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_area",{
			code : code
		},function(D){

			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_remind_letter_charges",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#bc").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	if ($("#code").val() == ""){showMessage("e","Enter area code");	return false; }
	if ($("#description").val() == ""){showMessage("e","Enter area name");	return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_remind_letter_charges/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.R.letter_no);
		$("#btnSave").val("Edit");
		$("#letter_no").val(D.R.letter_no);		
		$("#amount").val(D.R.amount);				
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_area/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}