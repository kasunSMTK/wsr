$(document).ready(function(){
   	
   	$("#start_backup").click(function(event) {   		
		start_backup();
   	});

});

function start_backup(){
	
	$("#start_backup").removeClass('btn_regular').addClass('btn_regular_disable').attr('disabled',true);
	$("#progress_txt").css('visibility','visible').html('Please wait...');



	$.post("index.php/backup/backup_db",{

	},function(D){

		if (D.s == 1){
			$("#start_backup").removeClass('btn_regular_disable').addClass('btn_regular').attr('disabled',false);;			
			$("#progress_txt").html(D.dl);
		}

	},"json");
}