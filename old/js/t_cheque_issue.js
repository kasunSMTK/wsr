$(document).ready(function(){

    $("#btnViewIssued").click(function(){

        $("#print_bc_issued_cheques").submit();

    });

    $("#no_of_cheques").blur(function(event) {
        var x = $(this).val();
        if (x == "" || isNaN(x) ){x = 1; }
        x = parseInt(x);
        $(this).val(x);
    });
    
    $("#btnAddtoGrid").click(function(event) {                
        add_to_grid();
    });

    $("#bank_name").autocomplete({
        source: "index.php/main/load_data/t_cheque_issue/auto_com",
        select: function (event, ui) {
            b = ui.item.value;
            b = b.split(" - ");
            $("#bank_acc").val(b[0]);
            $("#i_bc_name").focus();
        }
    });


    $("#i_bc_name").autocomplete({
        source: "index.php/main/load_data/t_cheque_issue/i_bc_auto_com",
        select: function (event, ui) {
            b = ui.item.value;
            b = b.split(" - ");
            $("#issue_bc").val(b[0]);
            $("#memo").focus();
        }
    });

    $("#r_i_bc_name").autocomplete({
        source: "index.php/main/load_data/t_cheque_issue/i_bc_auto_com",
        select: function (event, ui) {
            b = ui.item.value;
            b = b.split(" - ");
            $("#r_issue_bc").val(b[0]);
            $("#fd").focus();
        }
    });    


    $("#memo").keypress(function(event) {
        if (event.keyCode == 13){
            $("#ref_no").focus();
        }
    });

    $("#ref_no").keypress(function(event) {
        if (event.keyCode == 13){
            $("#cheque_no").focus();
        }
    });

    $("#cheque_no").keypress(function(event) {
        if (event.keyCode == 13){
            $("#c_amount").focus();
        }
    });

    $("#c_amount").keypress(function(event) {
        if (event.keyCode == 13){
            $("#btnAddtoGrid").click();
        }
    });


    $("#btnSave").click(function(event) {
        save();
    });

    $("#no").keypress(function(event) {
        if (event.keyCode ==13){
            load_data($(this).val());
        }
    });
    

});


$(document).on('click', '.remove_cheque_row', function(event) {
    $(this).parent().parent().remove();    
});

function load_data(no){

    showActionProgress("Please wait...");

    if (no != ""){
        
        $.post("index.php/main/get_data/t_cheque_issue",{
            no : no
        },function(D){

            closeActionProgress();

            if (D.s == 1 ){

                $('#hid').val(D.sum.no)
                $('#btnSave').val("Update");
                $("#no").val(D.sum.no);
                $("#date").val(D.sum.date);
                $("#ref_no").val(D.sum.ref_no);                
                $("#bank_acc").val(D.sum.bank_acc);
                $("#bank_name").val(D.sum.bank_acc + "-" + D.sum.bank_name);               
                $("#issue_bc").val(D.sum.issue_bc);
                $("#i_bc_name").val(D.sum.issue_bc + "-" + D.sum.i_bc_name);                
                $("#memo").val(D.sum.memo);
                $("#employee").val(D.sum.employee);

                for(n = 0 ; n < D.det.length ; n++){
                    cheque_no = D.det[n].chq_no;
                    c_amount  = D.det[n].amount;
                    var T = '<div><div class="text_box_holder_commen" style="width:40%;float: left;border-top: none;background-color:#f1f1f1"> <input type="text"  name="cheque_no[]" value="'+cheque_no+'" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:40%;float: left;border-left: none;border-top: none;background-color:#f1f1f1"> <input type="text" name="c_amount[]" value="'+c_amount+'" class="amount" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:20%;float: left;border-left: none;height: 41px;;border-top: none;"> <a href="#" class="remove_cheque_row">Remove</a> </div></div>';
                    $(".cheque_grid").prepend(T);
                }

            }else{
                showMessage("e","Data not found");        
            }

        },"json");

    }else{
        
        showMessage("e","Select number");
    }

}

function validate_form(){
    return true;
}

function add_to_grid(){
    var cheque_no = $("#cheque_no").val();
    var c_amount  = $("#c_amount").val();    
    var n         = $("#no_of_cheques").val();    

    if (cheque_no == ""){ $("#cheque_no").focus(); return; }
    if (c_amount == ""){  $("#c_amount").focus(); return; }
    if (n == ""){         $("#no_of_cheques").focus(); return; }

    var T = "";


    for (no = 0 ; no < n ; no++ ){
        chq_no = cheque_no++;
        T += '<div><div class="text_box_holder_commen" style="width:40%;float: left;border-top: none;background-color:#f1f1f1"> <input type="text"  name="cheque_no[]" value="'+chq_no+'" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:40%;float: left;border-left: none;border-top: none;background-color:#f1f1f1"> <input type="text" name="c_amount[]" value="'+c_amount+'" class="amount" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:20%;float: left;border-left: none;height: 41px;;border-top: none;"> <a href="#" class="remove_cheque_row">Remove</a> </div></div>';
    }
    

    
    $(".cheque_grid").prepend(T);

    $("#cheque_no").val("").focus();
    $("#c_amount").val("");    

}

function save(){
    showActionProgress("Saving...");
    if (validate_form()){
        var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: "json",
            success: function (D) {

                closeActionProgress();

                if (D.s == 1){
                    showMessage("s","success");
                    resetFormData();
                    $('#no').val(D.nmax);
                }else{                  
                    showMessage("e","errror");
                }
            }
        });
    }
}

function resetFormData(){

    var d = $('#date').val();
    $('input').val("");
    $('#date').val(d);
    $('#btnAddtoGrid').val('Add');
    $('#btnSave').val('Save');
    $(".cheque_grid").html('');

}