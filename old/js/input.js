var notify_menu_droped = false;
var blinker_listn = "";
var blink_once = false;
var last_notification_count = 0;

$(document).click(function(){

	if ( notify_menu_droped ){
		setTimeout('close_notify()',250);		
	}

});

$(document).ready(function(){

	set_notification_count();	
	
	$(".notify,.notify_count").click(function(event) {
		set_notify();
	});

	$(window).resize(function(event) {		
		if ( notify_menu_droped ){
			set_notify();
		}
	});


	$(".load_more_msgs").click(function(){		

		$.post("index.php/main/load_data/t_message/set_loder_msgs",{
			no_of_visible_msgs : $(".no_of_visible_msgs").val()
		},function(D){

			var img_html = '';

			for(n = 0 ; n < D.d.length ; n++){
				img_html += D.d[n].msg;
			}

			$(".dashborad_msg").append(img_html);

			$(".no_of_visible_msgs").val( parseInt($(".no_of_visible_msgs").val())+1 );

		},"json");

	});


});

function set_notify(){

	$("body").css("overflow", "hidden");
		
	var offset = $('.notify').offset();
	var h 	   = parseInt( $('.notify').css('height') );
	var w 	   = parseInt( $('.notify').css('width') );
	var nh 	   = parseInt($(".notifications").css("width"));	
	
	$(".notify").css({
		"background-color" : "#ffffff",
		"border":"1px solid #f1f1f1",
		"border-bottom":"1px solid #ffffff",
		"z-index" : 499,
		"box-shadow": "3px 1px 6px #f1f1f1",
		"color" : "#000000"
	});	



	$(".notifications").css({			
		"top" : (offset.top + h - 1 ) + "px",
		"right": 0,
		"height" : (window.innerHeight)-42,
		"visibility": "visible",
		"background-color": "#ffffff",
		"border":"1px solid #eaeaea",
		"z-index" : 500,
		"border-top" : "none",
		"position" : "fixed"
	});

	if (!notify_menu_droped){
		get_notifications();
	}

}


function close_notify(){

	$("body").css("overflow", "auto");
	
	$(".notify").css({
		"background-color" : "#f7f7f7",
		"box-shadow": "none",
		"border":"1px solid #f7f7f7",
		"border-bottom": "1px solid #eaeaea",
		"color" : "Green"
	});

	$(".notifications").css({					
		"visibility": "hidden"
	});	

	notify_menu_droped = false;

}


function get_notifications(){

	$(".notifications_title").html("<div style='border-bottom: 1px solid #cccccc; padding:10px; padding-top: 20px; padding-bottom: 20px; width: 398px;'>Please wait...</div>");
	
	$.post("index.php/main/load_data/notifications/notifications_titles",{
		st : 'get_notifications'
	},function(D){		
		
		//alert(D.notifications_titles);

		$(".notifications_title").html(D.notifications_titles);		
		
		notify_menu_droped = true;

		if (D.notifi_count > 0){
			$(".notifi_indicator").html(D.notifi_count).css({
				"display":"block",
				"background-color":"red",
    			"color":"#ffffff"
			});
		}else{
			$(".notifi_indicator").html('').css("display","none");
		}

		blinker_stop();

	},"json");

}


function set_notification_count(){
	
	$.post("index.php/main/load_data/notifications/notifications_titles",{
		st : 'set_notification_count'
	},function(D){

		if (D.notifi_count > 0){
			
			$(".notifi_indicator").html(D.notifi_count).css({
				"display":"block",
				"background-color":"red",
    			"color":"#ffffff"
			});


			if (typeof $.cookie('last_notification_count') === 'undefined'){			
				$.cookie("last_notification_count", 0);
			}


			if ($.cookie("last_notification_count") != D.notifi_count){

				
				if ( $.cookie("last_notification_count") < D.notifi_count ){
					
					document.getElementById('audiotag1').play();					
					
					if (!blink_once){
						$(".notifi_indicator").addClass("blinker");
						blink_once = true;
					}
				}


			}else{
				blink_once = false;
			}

		}else{
			$(".notifi_indicator").html('').css("display","none");
		}

		blinker_listn = setTimeout("blinker_stop()",5000);

		setTimeout("set_notification_count()",10000);

		//last_notification_count = D.notifi_count;

		if (typeof $.cookie('token') === 'undefined'){			
			$.cookie("last_notification_count", D.notifi_count);
		} else {			
			$.removeCookie("last_notification_count");
			$.cookie("last_notification_count", D.notifi_count);
		}



	},"json");
}

function blinker_stop(){	
	$(".notifi_indicator").removeClass("blinker");
}



