
$(document).ready(function(){
     
     $(".amount").blur(function(e){
        cal_amount();
     });

     if($("#hid").val()== 0){
        load_def_acc();
    }

    $("#btnCancel").click(function(){

        if (confirm("Confirm entry cancel")){
            
            showActionProgress("Please wait...");

            $.post("index.php/main/load_data/t_day_cach_bal/cancel_entry",{
                no : $("#nno").val()
            },function(D){        
                
                closeActionProgress();

                if (D.s == 1){
                    alert("Entry canceled");
                    location.href = '';
                }else{
                    alert("Error");
                }

            },"json");

        }

    });


     $("#nno").keypress(function(e){

        if (e.keyCode == 13){

            showActionProgress("Please wait...");

            $.post("index.php/main/load_data/t_day_cach_bal/load_receipt",{
                nno : $("#nno").val()
            },function(D){        
                
                closeActionProgress();

                if (D.s == 1){
                    $('#nno').attr("readonly","readonly");
                    $("#hid").val(D.sum.nno);                    
                    $("#date").val(D.sum.ddate);
                    $("#time").val(D.sum.time);

                    $("#sys_cash").val(D.sum.system_cash);
                    $("#manual_cash").val(D.sum.maunal_cash);
                    $("#dif_cash").val(D.sum.difference);

                     //--------pdf report details-------------
                    $("#qno").val(D.sum.nno);  
                   
                    
                    $("#ddate").val(D.sum.ddate);
                    $("#acc_code").val(D.sum.acc);
                    $("#acc_des").val(D.sum.acc_desc);
                    $("#org_print").val("");
                   


                    if (D.sum.is_cencel == 1){ 
                        $(".div_vou_hin").html("This entry was canceled").css({
                            "background-color":"red",
                            "color":"#ffffff",
                            "padding" : "12px",
                            "text-align" : "center",
                            "font-size" : "19px",
                            "font-family":"bitter"
                        });

                        $("#btnSave").prop("disabled",true).removeClass('btn_regular').addClass('btn_regular_disable');
                    }else{
                        $(".div_vou_hin").html("");
                        $("#btnSave,#btnCancel").prop("disabled",false).removeClass('btn_regular_disable').addClass('btn_regular');
                        $("#btnSave").val("Update");
                    }

                  
                    $("#org_print").val("");

                    var T = '';
                  

                    $(".voucher_acc_list_tbody").html("");


                        acc         = D.sum.acc;
                        acc_desc    = acc + " - " + D.sum.acc_desc;
  

                        T += '<tr>'; 

                        T += '<td><input type="text" id="to_acc_desc" class="" value="'+acc_desc+'" readonly></span><input type="hidden" name="to_acc" id="to_acc" value="'+acc+'"></td>';

                     
                        T += '</tr>';

                 
                    $(".voucher_acc_list_tbody").html("");
                    $(".voucher_acc_list_tbody").html(T);


                }else{
                    alert("Data not found");
                }
                

            },"json");

        }

    });

    $("#btnSave").click(function(){        
        save();
    });

    

});


function cal_amount(){
    var diff=parseFloat(0);
    var sys_csh=parseFloat($("#sys_cash").val());
    var man_csh=parseFloat($("#manual_cash").val());

  
    if((!isNaN(sys_csh)) && (!isNaN(man_csh))){

        if(sys_csh==man_csh){
             diff=0;
        }else {
            diff=sys_csh - man_csh
        }

        $("#dif_cash").val(m_round(diff));
    }
}



//=================================functions==============================================

function save(){

   // if (validate_voucher_save()){

        showActionProgress("Saving...");
        $(".receipt_update_msg").html("");

        var frm = $('#form_'); 

        $.ajax({
            type: frm.attr('method'), 
            url: frm.attr('action'), 
            data: frm.serialize(), 
            dataType: "json", 
            success: function (D) {
                closeActionProgress(); 
                
                if (D.s == 1 ){
                    alert("Save/Update success");                                 
                    location.href = "";
                }else if (D.s == 2 ){                    
                    $(".receipt_update_msg").html(D.update_det);                    
                }else if (D.s == 3 ){                    
                    alert("Unable to save daily cash balance update. Cash difference exceed max/min allowed amount. \n\nPlease contact IT department");
                }else{
                    showMessage("e",D); 
                }
                 
            } 
        });
}


function validate_list_items(){
    
    if ($("#to_acc").val() == ""){
        alert("incorrect account selection");
        $("#to_acc").focus();
        return false;
    }

    if ($("#to_amount").val() == ""){
        alert("Invalid amount");
        $("#to_amount").focus();
        return false;
    } 

    if ($(".v_bc").val() == ""){
        alert("Select reference branch");
        $(".v_bc").focus();
        return false;
    }

    if ($(".v_class").val() == ""){
        alert("Select reference class");
        $(".v_class").focus();
        return false;
    }

    return true;
}

/*function reset_voucher_items_to_list(){
    $("#to_acc_desc").val("");
    $("#to_acc").val("");
    $("#to_amount").val("");            
    $(".v_bc").val("");    
    $(".v_class").val("");
}*/

$(document).ready(function(){

    var voucher_loaded = false;

   // check_for_voucher_view();

    $("select").css("border","1px solid Green");
    $(".v_bc,.v_class").css("border","none");

    

    $("#btnSave2").click(function(){
        $("#btnSave").attr("disabled", false);
    });

    
    
    $("#btnReset").click(function(){
        if (confirm("Confirm reset this form")){
            location.href="index.php?action=t_day_cach_bal";
        }
    });


    $("#id").keypress(function(e){
        if(e.keyCode == 13){
            $(this).blur();
            load_data($(this).val());
        }
    });

   /* $("#btnDelete").click(function(){
        set_delete();   
    });*/

    $("#btnPrint").click(function(){
        if($("#hid").val()=="0"){
          set_msg("Please load data before print","error");
          return false;
        }else{
          $("#print_pdf").submit();
        }
    });

    /*$("#btnShow").click(function(){
        $("#print_vou_list_pdf").submit();        
    });*/

});


//--------------load def_acc-----------------------------------------------------------

    function load_def_acc(){
    $.post("index.php/main/load_data/t_voucher_general/load_def_acc/", {
    }, function(D){
         //alert(r);
         if(D!=2){

             var T = "";

                    T += $(".voucher_acc_list_tbody").html();

                    $(".voucher_acc_list_tbody").html("");

                    for (n = 0 ; n < D.det.length ; n++){

                       //alert(D.det.toSource())

                        acc         = D.det[n].acc_code;
                        acc_desc    = acc + " - " + D.det[n].acc_desc;
                       

                        T += '<tr>'; 

                        T += '<td><input type="text" id="to_acc_desc" class="" value="'+acc_desc+'" readonly></span><input type="hidden" name="to_acc[]" id="to_acc" value="'+acc+'"></td>';
                   
                        T += '</tr>';

                    }

                    

                    $(".voucher_acc_list_tbody").html(T);
  
         }

        }, "json");
}
//-------------------------------------------------------------------------------------


function save_(){

    $("#qno").val($("#id").val());
    $("#voucher_type").val($("#type_hid").val());
    $("#ddate").val($("#date").val());
    $("#acc_code").val($("#cash_acc").val());
    $("#acc_des").val($("#cash_acc_des").val());
    $("#tot").val($("#net").val());
    $('#form_').attr('action',$('#form_id').val()+"t_br_receipt_general");

    var frm = $('#form_');
    console.log($('#form_id').val());
    loding();
    $.ajax({
    type: frm.attr('method'),
    url: frm.attr('action'),
    data: frm.serialize(),
    success: function (pid){
            

            if(pid == 1){
                
                $("#btnSave").attr("disabled",true);
                $("#showPayments").attr("disabled",true);
                
                loding();

                alert("Voucher sent for approval");
                location.href="";

              
            }else if(pid == 2){
                alert("No permission to add data.");
            }else if(pid == 3){
                alert("No permission to edit data.");
            }else{
                alert("Error : \n"+pid);
            }
            
        }
    });
}


function validate(){
    // if($("#code").val() === $("#code").attr('title') || $("#code").val() == ""){
    //     alert("Please enter code.");
    //     $("#code").focus();
    //     return false;
    // }else 
    if($("#description").val() === $("#description").attr('title') || $("#description").val() == ""){
        set_msg("Please enter description.");
        $("#description").focus();
        return false;
    }else if($("#description").val() === $("#code").val()){
        set_msg("Please enter deferent values for description & code.");
        $("#des").focus();
        return false;
    }else if($("#sales_category").val()=="0"){
        set_msg("Please select category");
        return false;        
    }else{
        return true;
    }
}
    
function set_delete(){

    var code = $("#id").val();
    var type = $("#type_hid").val();

    if(confirm("Are you sure do you want cancel "+code+"?")){
        loding();
        $.post("index.php/main/delete/t_br_receipt_general", {
            code : code,
            type2 :type,
             hid :$("#hid_nno").val() 
        }, function(res){
            if(res == 1){
               alert("Voucher cancel success");
               location.href = "";
            }else if(res == 2){
                alert("No permission to delete data.");
            }else{
                alert("Item deleting fail.");
            }
            loding();
        }, "text");
    }
}

function is_edit($mod)
{
    $.post("index.php/main/is_edit/user_permissions/is_edit", {
        module : $mod
        
    }, function(r){
       if(r==1)
           {
             $("#btnSave").removeAttr("disabled", "disabled");
           }
       else{
             $("#btnSave").attr("disabled", "disabled");
       }
       
    }, "json");

}


function empty_grid(){
    for(var i=0; i<25; i++){
        $("#h_"+i).val("");
        $("#0_"+i).val("");
        $("#n_"+i).val("");
        $("#1_"+i).val("");
        $("#2_"+i).val("");
    }
}


//--------------load def_acc-----------------------------------------------------------

    function load_def_acc(){
    $.post("index.php/main/load_data/t_day_cach_bal/load_def_acc/", {
    }, function(D){
      
         if(D!=2){

             var T = "";

                    T += $(".voucher_acc_list_tbody").html();

                    $(".voucher_acc_list_tbody").html("");

                    for (n = 0 ; n < D.det.length ; n++){

                      

                        acc         = D.det[n].acc_code;
                        acc_desc    = acc + " - " + D.det[n].acc_desc;

                        T += '<tr>'; 

                        T += '<td style="border:none"><input type="text" id="to_acc_desc" style="border:none" value="'+acc_desc+'" readonly></span><input type="hidden" name="to_acc" id="to_acc" value="'+acc+'"></td>';
                       
                        T += '</tr>';

                    }
 
                    $(".voucher_acc_list_tbody").html(T);
  
         }

        }, "json");
}
//-------------------------------------------------------------------------------------