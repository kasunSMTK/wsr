$(document).on("click",".edit_dc",function(){

    var d = $(this).attr("c_date");

    showActionProgress("Please wait...");

    $.post("index.php/main/load_data/t_daily_cash_bal/set_edit",{
        d : d
    },function(D){
        closeActionProgress();

        $("#code").val(1);
        $("#date").val(D.d.date);
        $("#amount").val(D.d.amount);
        $("#btnSave").val("Update");

    },"json");


});

$(document).on("click",".remove_dc",function(){

    var d = $(this).attr("c_date");

    showActionProgress("Please wait...");

    if (confirm("Do you want remove this cash entry")){
        $.post("index.php/main/delete/t_daily_cash_bal",{
            d : d
        },function(D){
            closeActionProgress();
            load();
        },"json");
    }

});

$(document).ready(function(){

    load();

    $("#btnSave").click(function(){

        if ($("#amount").val() == "" || $("#amount").val() == 0){
            showMessage("s","Invalid amount"); 
            return;
        }

        save();
    });


});

function save(){

    showActionProgress("Saving...");            
    var frm = $('#form_'); 

    $.ajax({
        type: frm.attr('method'), 
        url: frm.attr('action'), 
        data: frm.serialize(), 
        dataType: "json", 
        success: function (D) {
            closeActionProgress(); 
            
            if (D.s == 1){
                showMessage("s","Save success"); 
                load();
                resetFormData(); 
            }else if(D.s == 2){
                showMessage("s","A cash balance entery already added for this date"); 
                resetFormData(); 
            }else{
                showMessage("e","Error, save failed");
            } 
        } 
    });

}

function resetFormData(){
    var d = $("#date").val();
    $("input:text").val(""); 
    $("#date").val(d);
    $("#btnSave").val("Save").attr("disabled",false).attr("class","btn_regular");
}

function load(){

    showActionProgress("Please wait...");

        $.post("index.php/main/get_data/t_daily_cash_bal",{
            
        },function(D){
            closeActionProgress();

            T = "<table class='tbl_daily_cash' width='100%'>";

            T += "<tr>";
            T += "<td>Date</td>";
            T += "<td align='right'>Amount</td>";
            T += "<td width='100' align='right'>Action</td>";
            T += "</tr>";

            if (D.s == 1){
                
                for(n = 0 ; n < D.d.length ; n++ ){

                    T += "<tr>";
                    T += "<td>"+D.d[n].date+"</td>";
                    T += "<td align='right'>"+D.d[n].amount+"</td>";
                    T += "<td align='right'><a c_date='"+D.d[n].date+"' class='edit_dc'>Edit</a> | <a c_date='"+D.d[n].date+"' class='remove_dc'>Delete</a></td>";
                    T += "</tr>";
                }

            }else{
                T += "<tr>";
                T += "<td colspan='3'>No data found</td>";                
                T += "</tr>";                
            }

            T += "</table>";

            $(".daily_cash_bal").html(T);

        },"json");

}