var redeem_trans_no = "";
var last_max_no = "";
var current_date = "";
var pp_data = "";
var paying_amount_PP = 0;
var balance_amount_PP = 0;
var cgv_PP = 0;
var op_toggle = false;
var pblamt = 0;
var pblamt_constant = 0;



$(document).ready(function(){	

	$("#billno").focus();

	$("#paying_amount").click(function(){
		$("#btnPaymentOptions").click();
	});

	function set_val_1(obj){		

		if ( parseFloat(obj.val()) > parseFloat($("#customer_advance").val()) ){
			obj.val(parseFloat($("#customer_advance").val())); 
		} 

		discount = 0;
		
		var pa = (pblamt - obj.val());

	}

	function set_val_2(obj){

		// call total cal function here
	}

	$("#customer_advance_paying").change(function(){set_val_1($(this)); calPayAmount(); });
	
	$("#customer_advance_paying").keyup(function(){
		set_val_1($(this)); calPayAmount();
	});
	

	$("#card_amount").change(function(){set_val_2($(this));calPayAmount(); });
	$("#card_amount").keyup(function(){set_val_2($(this));calPayAmount(); });
	

	$("#cash_amount").change(function(){calPayAmount(); });
	$("#cash_amount").keyup(function(){calPayAmount(); });

	$(".chk_pay_opt_apply").click(function(){
		
		if ($(this).is(":checked")){			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "80px"},'fast');			
			$(this).parent().parent().css("background","#f9f9f9");
		}else{			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "0px"},'fast');
			$(this).parent().parent().css("background","#ffffff");
		}
	});

	$(".pay-close").click(function() {
		$("#btnPaymentOptions").click();		
	});


	$("#btnPaymentOptions").click(function() {

		if (!op_toggle){
			$(".op_holder").animate({height: "400px"},'slow');
			op_toggle = true;
			 $("html, body").animate({ scrollTop: $(document).height() }, 'slow');
		}else{
			$(".op_holder").animate({height: "0px"},'slow');
			op_toggle = false;
			  $("html, body").animate({ scrollTop: 0 }, "slow");
		}

	});


	$("#bill_code_desc").focus();

	

	$(document).on('keyup', '#txt_tfr_int_discount', function(){
		
		if ($(this).val() > 0){
			$("#btnProceedRenew").prop("disabled",true).attr("class","btn_regular_disable");
			$("#btn_tfr_int_discount_request").prop("disabled",false).attr("class","btn_regular");
		}else{
			$("#btnProceedRenew").prop("disabled",false).attr("class","btn_regular");
			$("#btn_tfr_int_discount_request").prop("disabled",true).attr("class","btn_regular_disable");
		}

	});

	$(document).on('click', '#btn_tfr_int_discount_request', function(){

	 	var i = parseFloat($("#new_pawn_int").val());
	 	var d = parseFloat($("#txt_tfr_int_discount").val());

	 	if (i <	d){
	 		alert('Invalid discount amount');
	 		$("#txt_tfr_int_discount").focus().select();
	 		return;
	 	}
		
		$("#txt_tfr_int_discount").prop("disabled",true);

		$("#btn_tfr_int_discount_request").prop("disabled",true).val("Waiting for response...").removeClass("btn_regular").addClass("btn_regular_disable");
		
		$.post("index.php/main/load_data/t_amendments/discount_approval",{		
			
			dis_amount :$("#txt_tfr_int_discount").val()			
			
		},function(D){		
						
			if (D.s == 1){				
				
				call_dis_app_response_monitor(D.app_no);

			}

		},"json");	


	});
		

	$(document).on('click', '#btnProceedRenew', function(){
		ProceedRenew();
	});

	$(document).on('click', '#btnPayOnlyInt', function(){
		pay_only_interest();
	});

	$(document).on('click', '#btnPPcancel', function(){
		resetFormData();
		closePartPayMessage();
		$("#bill_code_desc").focus();
	});

	$(document).on('click', '.show_pawn_articles_div', function(){		
		$(this).hide();
		$("#div_itm_contain").animate({height: $("#div_show_items").css("height") });		
	});	

	$(".cal_5").click(function(){
		if ($("#billtype").val() == ""){
			$("#bill_code_desc").focus();
			return;
		}
		if ($("#billno").val() == "" ){			
			$("#billno").focus();
			return;
		} 

		LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());	
	});
	
	$( "#bill_code_desc" ).autocomplete({
		source: "index.php/main/load_data/m_billtype_det/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#billtype").val(bt[0]);				
				$("#billno").val("").focus();
		}
	});	

	$("#billno").keypress(function(e){
		if (e.keyCode == 13){
			LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());
		}
	});	

	$("#btnSave").click(function(){
		save();
	});

	$("#btnPrint").click(function(e){
		$("#print_pdf").submit();
	});

	$("#paying_amount").keypress(function(e){
		if (e.keyCode == 13){
			$("#btnSave").focus();
		}
	});

	$("#btnReset").click(function(){
		redeem_trans_no = $("#no").val();
		current_date = $("#ddate").val();
		resetFormData();
		$("#no").val(redeem_trans_no);
		$("#btnSave").attr("disabled",false).attr("class","btn_regular");
		$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
		$(".pawn_msg").html("").fadeOut();
		$("#bill_code_desc").focus();
		$("#no").val(last_max_no);
		$("#btnPrint").attr("disabled",true).attr("class","btn_regular_disable");		
		$("#pp_list").html("");
		$("#paying_amount").attr("disabled",false)
	});

	$("#no").keypress(function(e){
		if (e.keyCode == 13){
			getSavedPartPay($(this).val());
		}
	});

	$("#manual_bill_no").keypress(function(e){

		if (e.keyCode == 13){

			showActionProgress("Please wait...");

			$.post("index.php/main/load_data/t_new_pawn/get_system_billno",{
				manual_bill_no : $("#manual_bill_no").val(),
				cut_bc_no : 1
			},function(D){
				
				closeActionProgress();

				if (D.s == 1){

					$("#billno").val(D.billno);
					LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());	

				}else{
					alert("Invalid manual bill number ");
				}

			},"json");

		}

	});


});

function call_dis_app_response_monitor(app_no){
	
	$.post("index.php/main/load_data/t_amendments/call_dis_app_response_monitor",{		
		app_no : app_no	
	},function(D){		
	
		if ( D.s == 0 ){
			location.href = '';
		}else{

			if ( D.r.record_status == 'W' ){
				
				setTimeout("call_dis_app_response_monitor('"+app_no+"')",5000);

			}else if ( D.r.record_status == "A" ){
				
				$("#btn_tfr_int_discount_request").val("APPROVED").removeClass("btn_regular_disable").addClass("btn_regular");

				$('#btnProceedRenew').prop("disabled",false).removeClass("btn_regular_disable").addClass("btn_regular");

				// set hidden var if ness

				$('#btnProceedRenew').click();

			}else if ( D.r.record_status == 'R' ){

				$("#txt_tfr_int_discount").prop("disabled",false);
				$("#txt_tfr_int_discount").val("");

				$("#btn_tfr_int_discount_request").val("Request Approval").removeClass("btn_regular_disable").addClass("btn_regular");

			}

		}


	},"json");	

}


function enable_pay_option_inputs(){
	$("#customer_advance_paying,#card_amount,#cash_amount").removeAttr("readonly");
}

function disable_pay_option_inputs(){
	$("#customer_advance_paying,#card_amount,#cash_amount").val("");
	$("#customer_advance_paying,#card_amount,#cash_amount").attr("readonly","readonly");
}


function calPayAmount(){

	if ($('#billtype').val() == ""){
		return;
	}
	
	discount = 0;
	pblamt = pblamt_constant;
	pblamt -= discount;
	var pa = pblamt;

	var customer_advance = isNaN(parseFloat($("#customer_advance").val())) ? 0 : parseFloat($("#customer_advance").val());
	
	var customer_advance_paying = isNaN(parseFloat($("#customer_advance_paying").val())) ? 0 : parseFloat($("#customer_advance_paying").val());	
	var card_amount = isNaN(parseFloat($("#card_amount").val())) ? 0 : parseFloat($("#card_amount").val());
	var cash_amount = isNaN(parseFloat($("#cash_amount").val())) ? 0 : parseFloat($("#cash_amount").val());
	var n = 0;
		
		n += customer_advance_paying;
		n += card_amount;
		n += cash_amount;

	$("#paying_amount").val( n.toFixed(2) );
}


function ProceedRenew(){
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/t_amendments/proceed_to_renew_loan",{		
		data : pp_data,
		paying_amount : paying_amount_PP,
		balance_amount : balance_amount_PP,
		current_gold_value : cgv_PP,
		nla : $("#nla").val(),
		int_discount : $("#txt_tfr_int_discount").val(),
		new_pawn_int : $("#new_pawn_int").val(),
		time : $("#time").val()
	},function(D){		
		
		closeActionProgress();

		if(D.s == 33){
			alert(D.msg_33);
			location.href = '';
		}
	
		if (D.s == 1){

			showMessage("s","Part payment sucess");
			current_date = $("#ddate").val();
			resetFormData();
			$("#no").val(D.max_no);
			$("#bill_code_desc").focus();
		
			/*$(".r_no2").val(D.new_loanno); $(".by2").val("pawn_ticket_first_int"); $("#new_loan_first_int_print_pdf").submit();*/









			if (D.is_tfr == 1){				
				
				$(".by1").val("pawn_ticket");
				$(".r_no1").val(D.new_loanno);				
				$(".r_no2").val(D.int_tr_no);				
				$("#is_tfr").val(1);
				
				$("#print_pdf").submit();				

			}else{
				
				$(".by1").val("pawn_ticket");
				$(".r_no1").val(D.new_loanno);
				$("#print_pdf").submit();
				
			}








			$("#ddate").val(current_date);

			closePartPayMessage();
		}

	},"json");
}




function pay_only_interest(){
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/t_amendments/pay_only_interest",{		
		data : pp_data,
		paying_amount : paying_amount_PP,
		balance_amount : balance_amount_PP
	},function(D){		
		closeActionProgress();
		
		if (D.s == 1){
			showMessage("s","Part payment sucess");
			current_date = $("#ddate").val();
			resetFormData();
			$("#no").val(D.max_no);
			$("#bill_code_desc").focus();
			
			$("#r_no").val(D.no + "~~~" + D.loanno);
			
			$("#by").val("partpay_ticket");				
			$("#print_pdf").submit();
			$("#ddate").val(current_date);

			closePartPayMessage();
		}

	},"json");	
}



function getSavedPartPay(tr_no){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_amendments/getSavedPartPay",{
		tr_no : tr_no
	},function(D){

		closeActionProgress();

		if(D.s == 1){
			
			$("#bill_code_desc").val(D.sum.billtype);
			$("#billtype").val(D.sum.billtype);
			$("#billno").val(D.sum.billno);
			$("#ddate").val(D.sum.pp_paid_date);
			$("#dDate").val(D.sum.pawn_date);

			$("#period").val(D.sum.period);
			$("#finaldate").val(D.sum.finaldate);						
			$("#loan_amount").val(D.sum.requiredamount);
			
			$("#fmintrate").val(D.sum.fmintrate);
			$("#loan_amount").val(D.sum.requiredamount);			

			$("#no_of_days").val(D.int.no_of_days);
			$("#interest").val(D.int.total_int);
			$("#paid_interest").val(D.int.paid_int);

			$("#balance").val(D.int.int_balance);
			$("#paying_amount").val(D.sum.amount).attr("disabled",true);

			if (D.pay_option.advance_amount != undefined){$("#customer_advance_paying").val(D.pay_option.advance_amount.cr_amount) }
			if (D.pay_option.card_details != undefined){$("#card_number").val(D.pay_option.card_details.card_no); $("#card_amount").val(D.pay_option.card_details.card_amount); }
			if (D.pay_option.cash_amount != undefined){$("#cash_amount").val(D.pay_option.cash_amount); }

			$(".op_holder").animate({height: "400px"},'slow');
			op_toggle = true;
			$("html, body").animate({ scrollTop: $(document).height() }, 'slow');


			$('.pay-penal-sub').animate({height: "80px"},'fast');			
			$('.pay-opt-penal').css("background","#f9f9f9");
			

			$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
			$("#discount").attr("disabled",true);
			$("#btnReset").attr("disabled",false).attr("class","btn_regular");
			$("#billno,#bill_code_desc").attr("readonly","readonly");

			$("#r_no").val( $("#no").val() + "~~~" + D.sum.loanno );
			$("#by").val("partpay_ticket");				

			$("#btnPrint").attr("disabled",false).attr("class","btn_regular");

		}else{			
			showMessage("e","Invalid loan number");
			$("#btnReset").click();
		}		
		
	},"json");
}

function resetFormData(){
	setCurrent_date();	
	op_toggle = true;
	$("#btnPaymentOptions").click();
	disable_pay_option_inputs();
	ddate = $("#ddate").val();	
	bc_no = $("#bc_no").val();

	$("input:text").val("");

	$("#bc_no").val(bc_no);
	$("#ddate").val(ddate);	
	$("input:hidden").val("");	
	$("#cus_info_div").css("height","0").html("");
	$("#billtype_info_div").html("");
	$("#pp_list").html("");
	$("#billno,#bill_code_desc").removeAttr("readonly");
	$("#billno").focus();
}

function setCurrent_date(){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/utility/setCurrent_date",{
	},function(D){		
		closeActionProgress();
		$("#ddate").val(D.current_date);		
	},"json");	
}

function save(){

	showActionProgress("Please wait...");

	if (validate_form()){

		$.post("index.php/main/save/t_amendments",{

			loanno 		: $("#hid_loan_no").val(),
			customer_id : $("#hid_customer_id").val(),
			billtype 	: $("#billtype").val(),
			billno 		: $("#billno").val(),
			ddate 		: $("#ddate").val(),
			amount 		: $("#paying_amount").val(),
			loan_amount : $("#loan_amount").val(),
			five_days_int_cal : $(".cal_5").is(":checked") ? 1 : 0,

			customer_advance : $("#customer_advance").val(),
			customer_advance_paying : $("#customer_advance_paying").val(),
			card_number : $("#card_number").val(),
			card_amount : $("#card_amount").val(),
			cash_amount : $("#cash_amount").val(),
			hid_customer_serno : $("#hid_customer_serno").val(),
			pDate 		: $("#pDate").val(),
			is_weelky_int_cal : $("#is_weelky_int_cal").val(),
			int_bal 	: $("#balance").val(),
			bc_no 		: $("#bc_no").val()

		},function(D){

			closeActionProgress();

			if(D.s == 33){
				alert(D.msg_33);
				location.href = '';
			}

			if (D.s == 1){
				showMessage("s","Part payment sucess");
				current_date = $("#ddate").val();
				resetFormData();
				$("#no").val(D.max_no);
				$("#bill_code_desc").focus();
				
				$("#r_no").val(D.no + "~~~" + D.loanno + "~~~from_master_table");
				
				$("#by").val("partpay_ticket");				
				$("#print_pdf").submit();
				$("#ddate").val(current_date);				
			}

			if (D.a == 1){

				showpartPayMessage(D.msg_html);
				$("#nla").val(D.new_loan_amount);
				$("#cgv").val(D.cgv);
				$("#lgv").val(D.lgv);
				pp_data = D.data;
				paying_amount_PP = D.paying_amount;
				balance_amount_PP = D.balance_amount;	
				cgv_PP = D.cgv;

				var T = "<table class='tbl_item_details'>";

					T += "<tr>";

						T += "<td>Category</td>";
						T += "<td>Item</td>";
						T += "<td>Condition</td>";
						T += "<td>Gold Type</td>";
						T += "<td>Quality</td>";
						T += "<td>Qty</td>";
						T += "<td>T.Weight</td>";
						T += "<td>P.Weight</td>";
						T += "<td>Gold Value</td>";
						T += "<td>Value</td>";						

					T += "</tr>";


				for (no = 0 ; no < D.articles.length ; no++){

					T += "<tr>";

					T += "<td>"+D.articles[no].cat_code+"</td>";
					T += "<td>"+D.articles[no].itemcode+"</td>";
					T += "<td>"+D.articles[no].con+"</td>";
					T += "<td>"+D.articles[no].goldtype+"</td>";
					T += "<td>"+D.articles[no].quality+"</td>";
					T += "<td>"+D.articles[no].qty+"</td>";
					T += "<td>"+D.articles[no].goldweight+"</td>";
					T += "<td>"+D.articles[no].pure_weight+"</td>";
					T += "<td>"+D.articles[no].goldvalue+"</td>";
					T += "<td>"+D.articles[no].value+"</td>";					

					T += "</tr>";

				}


				T += "</table>";

				$("#div_show_items").html("<br>"+T);


				//------------- New Modification ---------------------- 2016-06-08-----------

				var html_optional = "";

				if (D.bill_status == "REDEEM_AND_ISSUE_NEW"){

					var int_for_new_loan = (D.int_for_new_loan);

					html_optional += "<span style='color:red;font-size:18px'>This bill will be redeemed and issue a new pawn ticket with extra required amount</span><br>";
					html_optional += "<br>";					
					
					html_optional += '<div style="width:250px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn" style="width:186px">Interest for new advance amount</span> <input type="text" value="'+int_for_new_loan.toFixed(2)+'" readonly="readonly" id="new_pawn_int" class="input_text_large_s" style="width:200px">';

					html_optional += '<div style="width:250px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn" style="width:165px">Interest Discount for New Bill</span><br>';

					html_optional += '<div style="border:1px solid #ffffff;width:380px;text-align:left"> <input type="text" id="txt_tfr_int_discount" name="int_discount" class="input_text_large_s amount" style="width:200px"> <input type="button" id="btn_tfr_int_discount_request" value="Request Approval" class="btn_regular_disable" style="width:150px" disabled="disabled"> &nbsp; <a>X</a> </div>';

					html_optional += '<div style="width:232px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn" style="width:150px">Extra Required Amount</span> <input type="text" value="'+D.paying_amount.toFixed(2)+'" readonly="readonly" id="" class="input_text_large_s" style="width:200px;color:red">';
				}
				
				$(".optional_content_show").html(html_optional);

			}

		},"json");
	}

}

function LOAD_LOAN(billtype,billno,dDate){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_amendments/LOAD_LOAN",{
		billtype : billtype,
		billno : billno,
		dDate : dDate,
		five_days_int_cal : $(".cal_5").is(":checked") ? 1 : 0,
		bc_no : $("#bc_no").val()
	},function(D){

		closeActionProgress();

		if (D.s == 1){		

			if ( D.loan_sum.tfr_res == 1 ){

				$(".pawn_msg").html("TFR Transaction restricted for this bill, Please concat head office").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}


			if (D.loan_sum.status == "R"){
				$(".pawn_msg").html("This bill already redeemed").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if (D.loan_sum.status == "F"){
				$(".pawn_msg").html("This bill already forfeited").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if( D.loan_sum.status == "RN" ){
				$(".pawn_msg").html("This bill renewed to bill number " + D.loan_sum.new_bill_no ).fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if( D.loan_sum.status == "AM" ){
				$(".pawn_msg").html("This bill renewed to bill number " + D.loan_sum.new_bill_no ).fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else{
				$(".pawn_msg").html("").fadeOut();
				$("#btnSave").attr("disabled",false).attr("class","btn_regular");
				$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
			}

			$("#hid_loan_no").val(D.loan_sum.loanno);
			$("#hid_customer_id").val(D.loan_sum.customer_id);
			$("#hid_customer_serno").val(D.loan_sum.cus_serno);

			$("#billtype,#bill_code_desc").val(D.loan_sum.billtype);
			$("#pDate").val(D.loan_sum.ddate);
			$("#time").val(D.loan_sum.time);
			$("#period").val(D.loan_sum.period);
			$("#finaldate").val(D.loan_sum.finaldate);			
			$("#loan_amount").val(D.loan_sum.requiredamount);			
			$("#fmintrate").val(D.loan_sum.fmintrate);
			$("#is_weelky_int_cal").val(D.loan_sum.is_weelky_int_cal);

			// calculations from php
			if (D.loan_sum.is_renew == 1){
				$("#no_of_days").val(D.int.no_of_days + "     +" + D.loan_sum.old_bill_age );
			}else{
				$("#no_of_days").val(D.int.no_of_days);
			}

			$("#interest").val(D.int.total_int);
			$("#paid_interest").val(D.int.paid_int);
			$("#customer_advance").val(D.customer_advance.balance);
			$("#balance").val(D.int.int_balance.toFixed(2));
			$("#goldvalue").val( D.loan_sum.goldvalue );

			$("#max_payable_extra_amount").val( parseFloat(D.loan_sum.goldvalue - D.loan_sum.requiredamount).toFixed(2) );

			op_toggle = true;
			$("#btnPaymentOptions").click();
			disable_pay_option_inputs();
			enable_pay_option_inputs();

			// partpay
			var T = "";

			if (D.partpay_histomer.length > 0){				

				var num2 = D.partpay_histomer.length;

				for(num = 0 ; num < D.partpay_histomer.length ; num++){
					T += '<div style="border:0px solid red; height:24px;"><div style="width:50px;float:left; padding:2px;" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">'+(num2--)+'</span></div><div style="width:175px;float:left; padding:2px" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn" style="width:150px">'+D.partpay_histomer[num].action_date+'</span></div><div style="width:145px;float:left; padding:2px;text-align:right" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">'+D.partpay_histomer[num].discount+'</span></div><div style="width:145px;float:left; padding:2px;text-align:right" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">'+D.partpay_histomer[num].amount+'</span></div><div style="width:100px;float:left; padding:2px" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">'+D.partpay_histomer[num].bc+'</span></div></div>';
				}
			}else{
				T += '<div style="border-bottom:1px dotted #ccc;height:33px;border-bootm:1px solid #eaeaea;background-color:#fff"><div style="width:615px;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn" style="width:100%">No advance payments found</span></div></div>';
			}			

			$("#pp_list").html("").html(T);
			$("#paying_amount").focus();
			$("#cus_info_div").css({height: "42px"}).html(D.cus_info).animate({height: "398px"});
			$("#billtype_info_div").html(D.rec);

		}else{
			showMessage("e","Invalid bill number");
			$(".pawn_msg").html("").fadeOut();
			$("#cus_info_div").html("");
			$("#btnSave").attr("disabled",false).attr("class","btn_regular");
			$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
			$("#btnReset").click();
		}
		
		
	},"json");
}

function getPayableAmount(){
	if (!validateNumber($("#discount").val()) || $("#discount").val() == "" ){				
		discount = 0;
		$("#discount").val("")
	}else{
		discount = parseFloat( $("#discount").val() );
	}
	var paying_amount  = parseFloat( $("#loan_amount").val() ) + parseFloat( $("#balance").val() ) - discount;
	return paying_amount.toFixed(2);
}

function validate_form(){

	if ( rmsps($("#bill_code_desc").val()) ==  "" || rmsps($("#billtype").val()) ==  "" ){ showMessage("e","Enter bill number"); $("#bill_code_desc").val("").focus(); return false; }
	if (!validateAmount($("#paying_amount").val() ," paying amount")){ $("#paying_amount").val("").focus(); return false; }

	var loan_amount 	= parseFloat($("#loan_amount").val());
	var balance     	= parseFloat($("#balance").val());
	var customer_advance= parseFloat($("#customer_advance").val());
	var paying_amount 	= parseFloat( $("#paying_amount").val() );
	var max_payable_extra_amount = parseFloat( $("#max_payable_extra_amount").val() );


	if (customer_advance > 0){
		if ((paying_amount+customer_advance) > (loan_amount+balance)){
			showMessage("e","Please use redeem window to redeem this pawn");			
			return;	
		}		
	}else{
		//if (paying_amount > (loan_amount+balance) || paying_amount <= 0){
		if (paying_amount <= 0){
			showMessage("e","Invalid required amount");
			$("#paying_amount").val("").focus();
			return;
		}
	}

	//alert(paying_amount);
	//alert(max_payable_extra_amount);

	if ( (paying_amount+balance) > max_payable_extra_amount ){

		showMessage("e","Invalid paying amount");
		$("#paying_amount").focus().select();
		return;

	}


	return true;	
}

function setEdit(recid){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_new_pawn/set_edit",{
		recid : recid
	},function(D){		
		
		closeActionProgress();
	},"json");	

}
