$(document).ready(function(){
    
    $("#btnChangePw").click(function(){   
        ChangePw();
    }); 

    $("#btnChangeDisplayName").click(function(){
        
        if ($("#display_name").val() != ""){
            ChangeDN();
        }else{

            if ($("#display_name").val() != "no_name_added"){
                alert("Please enter display name");
            }else{
                alert("Invalid display name");
            }
        }

    });

    $("#display_name").click(function(){
        $(this).select();
    });


});


function ChangeDN(){

    showActionProgress("Please wait...");
    
    $.post("index.php/main/load_data/my_account/ChangeDN",{
        n : $("#display_name").val()
    },function(D){              
        closeActionProgress();

        if (D.s == 1){
            $(".dis_name_msg").remove();
            alert("Display name update success");
        }else{
            alert("update error");
        }

    },"json");

}

function ChangePw(){    
    showActionProgress("Saving...");

    if (validate_ChangePw()){
        var frm = $('#frmChangePw');
        $.ajax({
            type: frm.attr('method'),
            url: "index.php/main/load_data/my_account/ChangePw",
            data: frm.serialize(),
            dataType: "json",
            success: function (D) {
                if (D.s == 1){
                    showMessage("s","Password change success");
                    clear_form();
                }else{                  
                    showMessage("e",D.m);
                }
            }
        });

    }

}

function validate_ChangePw(){
// alert($("#OlduserPassword").val());
    if ($("#OlduserPassword").val() == ""){showMessage("e","Enter Current Password"); return false; }
    if ($("#ruserPassword").val() == ""){showMessage("e","Enter Password"); return false; }
    if ($("#userPassword").val() == ""){showMessage("e","Enter Confirm Password"); return false; }
    if ($("#userPassword").val()  != $("#ruserPassword").val()){showMessage("e","Confirm Password Not Match"); return false; }

    return true;    
}

function clear_form(){

    $("#OlduserPassword").val("");
    $("#ruserPassword").val("");
    $("#userPassword").val("");
}

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Area saving success");
					clear_form();
					load_list();					
/*				}else if (D == 2){					
					showMessage("e","Error, Duplicate Code");*/
				}else{					
					showMessage("e",D.m);
				}
			}

		});

	}

}

