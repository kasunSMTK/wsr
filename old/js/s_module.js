$(document).ready(function(){

    $("#module_name").focus();

    $(".gmod").click(function(){
        $(".gmod").css({"background-color":"#ffffff","color":"#000000"});
        $(this).css({"background-color":"Green","color":"#ffffff"});
    });

    $(".group").click(function(event) {
        
        $(".gxt").css("display","none");

        var c =    $(this).attr('class')  ;
            c = c.split(' group');
            c = c[0]+"_H";

            if ($("."+c).css("display") == "none"){
                $("."+c).fadeIn();
            }else{
                $("."+c).fadeOut();
            }

    });


    //$("#m_code").focus();
    $("#btnSave").val("Save");
    
    $(".update_m_module").click(function(){
        set_update($(this));
    });

    $("#btnSave").click(function(event) {
        save();
    });

    $(".remove_m_module").click(function(){
        
        if (confirm("Do you want remove this module?")){

            $.post("index.php/main/delete/s_module", {
                code : $(this).attr("code")
            }, function(D){

                if (D.s == 1){
                    alert("Delete completed");
                    location.href = "";
                }else{
                    alert("Delete ERROR");
                }

            },"json");       
        }

    });

});

    
function set_update(OBJ){
    $("#m_code,#code_").val(OBJ.attr("code"));
    $("#module_name").val(OBJ.attr("desc"));
    $("#mod_group_name").val(OBJ.attr("group"));
    $("#btnSave").val("Update");
}


function load_data(){
    empty_grid();

    var r = "";

    $.post("index.php/main/get_data/s_add_role/", {

        id : $("#user option:selected").val()

    }, function(r){       

        var i = 0;

        $("#tgrid tr").each(function(){    

            $(this).css({
                'background-color': '#ffffff',
                'color': '#000000'
            });

        });

        $("#tgrid tr").each(function(){    
            
            
            $("#check"+r.d[i].role_id).prop("checked", true);
            
            $("#check"+r.d[i].role_id).parent().parent().css({
                'background-color': '#eaeaea',
                'color': '#000000'
            });
            
            //$("#date_from"+r.d[i].role_id).val(r.d[i].date_to);
            //$("#date_to"+r.d[i].role_id).val(r.d[i].date_from);            
            

            i++;
        
        });

    }, "json");

    

}





function get_data(){

        var x = y = new Array(); var a = 0;

        $("#tgrid tr").each(function(){

            y = new Array();

            $(this).find("td").each(function(i){

                if(i==0){

                    y[0] = $(this).html();

                }else if(i == 2){

                    y[1] = $(this).children().attr("checked")? 1:0;

                }else if(i == 3){

                    y[2] = $(this).children().val();

                }else if(i == 4){

                    y[3] = $(this).children().val();

                }

            });

            if(y[1] == 1) { x[a++] = y; }

        });

        

        return x;

    }



function save(){
    
    var frm = $("#form_");

    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        dataType: "json",
        success: function (D) {

            if (D.s == 1){
                //alert("Module save compelete");
                location.href = "";
            }else if (D.s == 2){
                //alert("Module update compelete");
                location.href = "";
            }else{
                if (D.q1 == 1){alert("Module code already exist"); } 
                if (D.q2 == 1){alert("Module name already exist"); } 
            }
                
        }
    });

}



function check_code(){

    var code = $("#code").val();

    $.post("index.php/main/load_data/s_module/check_code", {

        code : code

    }, function(res){

        if(res == 1){

            if(confirm("This code ("+code+") already added. \n\n Do you need edit it?")){

                set_edit(code);

            }else{

                $("#code").val('');

            }

        }

    }, "text");

}



function validate(){

    if($("#user option:selected").attr('value') == "0"){

        alert("Please select user.");

        return false;

    }else{

        return true;

    }

}

    

function set_delete(code){

    if(confirm("Are you sure delete "+code+"?")){

        loding();

        $.post("index.php/main/delete/s_module", {

            code : code

        }, function(res){

            if(res == 1){

                location.reload();

            }else{

                alert("Item deleting fail.");
                 //pop_note("You don't have permission for add records.", 'r');

            }

            loding();

        }, "text");

    }

}

    

function set_edit(code){

    loding();

    $.post("index.php/main/get_data/s_module", {

        code : code

    }, function(res){

        $("#code_").val(res.m_code);

        $("#code").val(res.m_code);

        $("#code").attr("readonly", true);

        $("#des").val(res.m_description);

        

        loding(); input_active();

    }, "json");

}



function empty_grid(){

    

    $("#tgrid tr").each(function(){

            $(this).find("td").each(function(i){

                

                if(i == 2){

                    $(this).children().removeAttr("checked");

                }else if(i == 3){

                    $(this).children().val("0000-00-00");

                }else if(i == 4){

                    $(this).children().val("0000-00-00");

                }

            });

        });

}