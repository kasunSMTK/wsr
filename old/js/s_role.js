$(document).ready(function(){



    $("#btnAddNewRole").click(function(){


        if ($(this).attr("value") == "Reset"){
            location.href = "";
            return;
        }

        $("#code").attr({
            "id":"code_temp",
            "name" : "code_temp"
        }).hide();

        var t = 'Role Code :   <input type="text" name="code" id="code" class="input_text_regular" style="width: 150px"/>';
        $(".role_input_A").val("").prepend("Role Title");
        $(".role_input_A").val("").prepend(t);
        $("#des").val("").attr("type","text").attr("class","input_text_regular");

        $("#code").focus();

        $(this).attr("value","Reset");

    });






 //$("#tgrid").tableScroll({height:280});

 $("#code").change(function(){

    $("#des").val($("#code :selected").text());

    load_data($(this));


 });


 $("#btnSave").click(function(){
    
    if (form_validate()){
        save();
    }

 });

$("#all_view").change(function(){  
    
    if ($(this).is(":checked")){
        $(".ob_a").prop("checked",true);
    }else{
        $(".ob_a").prop("checked",false);
    }

 });





$("#btnReset").click(function()

{

    

   $(".ob_a").removeAttr("checked");

   $(".ob_b").removeAttr("checked");

   $(".ob_c").removeAttr("checked");

   $(".ob_d").removeAttr("checked");

   $(".ob_e").removeAttr("checked");

   $(".ob_f").removeAttr("checked");

   $(".ob_g").removeAttr("checked");

   $(".ob_h").removeAttr("checked");

   $(".ob_i").removeAttr("checked");

   $(".ob_j").removeAttr("checked");

    

    

    

});





 $("#all_add").change(function(){

    if ($(this).is(":checked")){
        $(".ob_b").prop("checked",true);
    }else{
        $(".ob_b").prop("checked",false);
    }

});



$("#all_edit").change(function(){

    if ($(this).is(":checked")){
        $(".ob_e").prop("checked",true);
    }else{
        $(".ob_e").prop("checked",false);
    }

});



$("#all_delete").change(function(){

    if ($(this).is(":checked")){
        $(".ob_d").prop("checked",true);
    }else{
        $(".ob_d").prop("checked",false);
    }

});



$("#all_print").change(function(){

    if ($(this).is(":checked")){
        $(".ob_f").prop("checked",true);
    }else{
        $(".ob_f").prop("checked",false);
    }

});



$("#all_r_print").change(function(){

    if ($(this).is(":checked")){
        $(".ob_g").prop("checked",true);
    }else{
        $(".ob_g").prop("checked",false);
    }

});



$("#all_back_date").change(function(){

    if ($(this).is(":checked")){
        $(".ob_h").prop("checked",true);
    }else{
        $(".ob_h").prop("checked",false);
    }

});

$("#all_authoriser").change(function(){

    if ($(this).is(":checked")){
        $(".ob_i").prop("checked",true);
    }else{
        $(".ob_i").prop("checked",false);
    }

});

$("#all_accept").change(function(){

    if ($(this).is(":checked")){
        $(".ob_j").prop("checked",true);
    }else{
        $(".ob_j").prop("checked",false);
    }

});

$( "#code" ).autocomplete({
        source: "index.php/main/load_data/s_role/auto_com",
        select: function (event, ui) {
                var bt = ui.item.value;
                bt = bt.split(" - ");
                /*$(this).parent().append('<div><div style="padding: 6px;padding-top:10px" class="bulk_item_row"><input type="hidden" class="bulk_item_codes" value="'+bt[0]+'"><span class="bulk_item_names">'+bt[1]+'</span> <div class="bulk_list_item_remove" style="float:right;font-size:9px;cursor: pointer">Remove</div></div></div>');
                $( "#bulk_items_add" ).val("").focus();*/
                
                event.preventDefault();
                    
                set_cus_values($(this));


        }
    });


/*$("#code").autocomplete('index.php/main/load_data/s_role/auto_com', {

        width: 350,
        multiple: false,
        matchContains: true,
        formatItem: formatItems,
        formatResult: formatItemsResult

});*/



    $("#code").blur(function(){

        set_cus_values($(this));

    });

    

    $("#code").keypress(function(e){

    if(e.keyCode == 13){

        set_cus_values($(this));

    }

    });



});//end of document .ready



function set_cus_values(f){



    set_cid(f.attr("id"));

    var v = f.val();

    v = v.split(" - ");

    

   // alert(v.length);

    

    

 if(v.length == 4){

    $("#code_").val(v[0]);

    f.val(v[1]);

    $("#des").val(v[2]);

    $("#bc").val(v[3]);

        load_data(v[0]);

        

 }

//    }else{

  

//      $.post("index.php/main/load_data/s_role/check_role", {

//          code : f.val()

//      }, function(r){

//          if(r > 0){

//              alert("Code alredy added.");

//              f.val('');

//              f.focus();

//          }

//      }, "json");

//      $("#code_").val(0);

//  }

    input_active(v[0]);

}



function formatItems(row){

    return "<strong> " +row[1] + "</strong> | <strong> " +row[2] + "</strong>";

}



function formatItemsResult(row){

    return row[0]+"~"+row[1]+"~"+row[2]+"~"+row[3];

}



function load_data(id){

    id = $("#code :selected").attr("id_det");



    empty_grid();

    $.post("index.php/main/get_data/s_role/", {

        id : id

    }, function(r){

        $("#tgrid tr").each(function(){


            for(var i=0; i<r.length; i++){

                if ($(this).children().eq(0).children().val() != undefined){                        

                    if($(this).children().eq(0).children().val() == r[i].module_id){                        

                        if(r[i].is_view == 1){ $(this).children().eq(2).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_add == 1){ $(this).children().eq(3).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_edit == 1){ $(this).children().eq(4).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_delete == 1){ $(this).children().eq(5).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_print == 1){ $(this).children().eq(6).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_re_print == 1){ $(this).children().eq(7).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_back_date == 1){ $(this).children().eq(8).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_authoriser == 1){ $(this).children().eq(9).find('input[type="checkbox"]').prop("checked","checked"); }
                        if(r[i].is_approve == 1){ $(this).children().eq(10).find('input[type="checkbox"]').prop("checked","checked"); }

                    }

                }

            }

        });

    

        $("#code_").val(id);

    

    }, "json");

}



function empty_grid(){

    $(":checkbox").removeAttr('checked');

}



function validate(){

    if($("#code").val() == $("#code").attr("title")){

        alert("Please enter code");

        $("#code").focus();

        return false;

    }else if($("#des").val() == $("#des").attr("title")){

         alert("Please enter description");

        $("#des").focus();

        return false;

    }else if($("#bc").val() == "0"){

         alert("Please select branch");

        return false;

    }else{

        return true;

    }

}



function save(){



    //$("#_form").submit();
    var frm = $("#_form");
    $.ajax({
    type: frm.attr('method'),
    url: frm.attr('action'),
    data: frm.serialize(),
    success: function (pid) {
        
        if(pid == 1){
            // input_reset(); pop_note('Saving Success.', 'g');
            // $("#nic_no").removeAttr("readonly"); $("#btnSave").val("Save");
            // $("#nic_no").focus(); $("#img").html(imag_up);
            // reset_map(); img_uploads(); $("#code_").val(0);
            alert("Role data added");
            location.href="";
        }else if(pid == 3){
            //alert("You don't have permission for add records");
            pop_note("You don't have permission for add records.", 'r');
        }else{
            alert("Error : "+pid);
            //pop_note("You don't have permission for save records.", 'r');
        }
    }
    });

}

function form_validate(){


    if ( $("#code").val() == "" ){
        alert("Please enter role code");
        $("#code").focus();
        return;
    }


    if ( $("#des").val() == "" ){
        alert("Please enter role title");
        $("#des").focus();
        return;
    }


    return true;

}