$(document).ready(function(){

	load_list();

	$('#inactive').prop('checked', false);

	$("#bc").removeAttr("Class").css("width","200px");
	$("#bc").prepend("<option value='all'>All Branch</option>").val("all");

	$("#bc").change(function(){
		clear_form();		
		load_list($(this).val())
	});
	

	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});

	$("#btnReset").click(function(){		
		clear_form();		
	});

	$("#btnSearch").click(function(){
		search($("#Search_billtype").val());
	});	

	$("#no").keypress(function(e){
		if (e.keyCode == 13){
			setUpdate($(this).val());
		}
	});

	$(".chkbc_select").click(function(){
		if ( $(this).is(":checked") ){
			$(".chkbc_").prop("checked",true);
			$(".chk_main_lable").html("Deselect all");
		}else{
			$(".chkbc_").prop("checked",false);
			$(".chk_main_lable").html("Select all");
		}
	});


	$(".rd").click(function(){
		
		if ( $(this).val() == "G" || $(this).val() == "L" ){
			$("#percentage_to").val("").attr("readonly",true);
		}

		if ( $(this).val() == "B"){
			$("#percentage_to").attr("readonly",false);
		}

	});



	$(".btnAddaRow").click(function(){
		add_rate_row();		
	});

	$(document).on('click', '.btnremoveRow', function(){	
		if (confirm("Do you want remove this interest rate row?")){
			$(this).parent().parent().remove();
		}
	});



});


function setUpdate(billtype){

	showActionProgress("Loading...");
	$.post("index.php/main/load_data/m_billtype_det/setUpdate",{
		billtype : billtype
	},function(D){				
		closeActionProgress();

		$("#hid").val(D.sum[0].no);
		$("#billtype").val(D.sum[0].billtype);
		$("#billtype_in_no").val(D.sum[0].billtype_in_no);

		if ( D.sum[0].is_gold == 1 ){
			$("input[name=is_gold][value=" + 1 + "]").prop('checked', true);
		}else{
			$("input[name=is_gold][value=" + 0 + "]").prop('checked', true);
		}

		$("#amount_from").val(D.sum[0].amount_from);
		$("#amount_to").val(D.sum[0].amount_to);		
		$("#first_month_int").val(D.sum[0].first_month_int);
		$("#next_months_int").val(D.sum[0].next_months_int);
		$("#gp1").val(D.sum[0].gp1);
		$("#gp1_int_rate").val(D.sum[0].gp1_int_rate);

		$("#gp2").val(D.sum[0].gp2);
		$("#gp2_int_rate").val(D.sum[0].gp2_int_rate);

		$("#next_months_int_gp2").val(D.sum[0].next_months_int_gp2);
		$("#period").val(D.sum[0].period);		

		var n = 0;
		var bc_array = Array();

		for(nno=0 ; nno < D.sum.length ; nno++){
			bc_array[nno] = D.sum[nno].bc;
		}

		$(".chkbc_").each(function(){
			if( $.inArray($(this).val(), bc_array) !== -1 ) {		
				$(this).prop("disabled",false).prop("checked",true);
			}			
		});		

		$("#r1").val( D.sum[0].r1 );
		$("#r2").val( D.sum[0].r2 );
		$("#r3").val( D.sum[0].r3 );


	},"json");

}

function save(){	

	//showActionProgress("Saving...");

	if (validate_form()){
		
		var frm = $('#form_');
		
		if (	$("#lt_opt").is(":checked")	){ opt = "L"; }
		if (	$("#gt_opt").is(":checked")	){ opt = "G"; }
		if (	$("#bt_opt").is(":checked")	){ opt = "B"; }

		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){

					alert("Save success");
					location.href = '';
					
				}else if(D.s == 2){		

					alert("Please select branches");

				}else{

				}
			}
		});

	}

}

function setDelete(billtype){

	if (confirm("Do you want delete this bill type?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_billtype_det",{
			billtype : billtype
		},function(D){		
			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				clear_form();
				load_list();
			}else{
				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(bc=""){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_billtype_det",{
		s : "load_list",
		bc : bc
	},function(D){		
		$(".list_div").html(D.T);
		$("#code").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	
	if ($("#billtype").val() == ""){alert("Please enter bill type"); $("#billtype").focus(); return false; }		
	if ($("#billtype_in_no").val() == ""){alert("Please enter bill number"); $("#billtype_in_no").focus(); return false; }		
	
	return true;	
}

function clear_form(){

	var tmp_no = $("#no").val();
	$("input[type=text]").val("");	
	$("#no").val(tmp_no);
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$('#inactive').prop('checked', false);
	$('#tax').prop('checked', false);
	$("input[type='checkbox'][name='weekly_cal']").prop("checked",false);
	$("input[type='checkbox'][name='is_old_bill']").prop("checked",false);
	$(".chkbc_").parent().parent().show();
	$(".chkbc_").prop("disabled",false).prop("checked",false);
}

function setEdit(billtype){


	$("#btnSave").attr("value","Update");
	$("#btnReset").attr("class","btn_regular");
	setUpdate(billtype);

	/*showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_billtype_det/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.code);
		$("#btnSave").val("Edit");
		$("#area").val(D.area);		
		$("#code").val(D.code);
		$("#description").val(D.description);
		$("#amount_from").val(D.amount_from);
		$("#amount_to").val(D.amount_to);
		$("#period").val(D.period);		
		$("#int_rate").val(D.int_rate);

		if (D.inactive == "1"){ $('#inactive').prop('checked', true); }else{ $('#inactive').prop('checked', false); }
		if (D.tax == "1"){ $('#tax').prop('checked', true); }else{ $('#tax').prop('checked', false); }

		closeActionProgress();
	},"json");	*/

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_billtype_det/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function add_rate_row(){
	var T = '<tr> <td><input id = "" name="day_from[]" 	type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;margin-bottom:5px;"></td> <td><input id = "" name="day_to[]" 	type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td> <td><input id = "" name="rate[]"		type="text"  class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td><td><a class="btnremoveRow">remove</a></td></tr>'; 
	$(".clz_billt tbody").append(T);
}


