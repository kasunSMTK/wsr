
$(document).bind('keydown', function(e) {
  if(e.ctrlKey && (e.which == 83)) {
    e.preventDefault();
   save();
    return false;
  }
});


$(document).on("click",".chk_all",function(){


    var check_any = false;

    $(".chk_bnk_rec").each(function(){
        if ($(this).is(":checked")){
            check_any = true;
        }
    });


    if ( $(this).is(":checked") ){
        if ( !check_any ){
            $(".chk_bnk_rec").click();
        }
    }else{

        rec_dr_tot = 0;
        rec_cr_tot = 0;

        $(".chk_bnk_rec").prop("checked",false);
        $(".rec_f_dr_tot,.rec_f_cr_tot").html("0.00");


    }

    var op_bal = parseFloat($("#op_bal").val());
    var rec_dr_amount = parseFloat($(".rec_f_dr_tot").html());
    var rec_cr_amount = parseFloat($(".rec_f_cr_tot").html());   

    $("#closing_bal").val(  ((op_bal+rec_dr_amount) - rec_cr_amount).toFixed(2)  );

    set_bal_diff();

});


$(document).ready(function(){

    $("#btnSave").click(function(){
        save();
    });


    $("#statment_closing_bal").keyup(function(){
        set_bal_diff();
    });


    $("#statment_closing_bal").change(function(){
        set_bal_diff();
    });

    $("#no").keypress(function(e){

        if (e.keyCode == 13){
            load_data( $(this).val() );
        }

    });


    $("#btnPrint").click(function(){

        $("#rec_no").val($("#code_").val());        
        $("#print_rec_report").submit();

    });   


});


function load_data(no){

    showActionProgress("Please wait...");

    $("#btnSave").attr("disabled",true).removeClass('btn_regular').addClass('btn_regular_disable');

    $.post("index.php/main/load_data/t_bankrec_n/load_data",{
        no : no
    },function(D){

        closeActionProgress(); 

        $("#btnPrint,#btnReset").attr("disabled",false).removeClass('btn_regular_disable').addClass('btn_regular');

        if (D.s == 1){

            var T = '';

            $("#from_date").val(D.sum.from_date);
            $("#to_date").val(D.sum.to_date);
            $("#no").val(D.sum.no);
            $("#code_").val(D.sum.no);
            $("#account_desc").val(D.sum.acc_code+" - "+D.sum.acc_desc);
            $("#acc_code").val(D.sum.acc_code);
            $("#date").val(D.sum.date);
            $("#op_bal").val(D.sum.op_bal);

            if (D.det.length > 0){

                var checked_count = 0;

                for(n = 0 ; n < D.det.length ; n++){

                    var Tx = Tr_style = '';
                    
                    if (D.det[n].is_reconcil == 1){
                        Tx += '<td align=center><input type="checkbox" class="chk_bnk_rec" checked="checked" disabled="disabled"></td>';
                        Tr_style =  'style="background-color:#eaeaea;color:green"';
                    }else{
                        Tx += '<td align=center><input type="checkbox" class="chk_bnk_rec" disabled="disabled"></td>';
                        Tr_style =  'style="background-color:#ffffff;color:000000"';
                    }

                    T += '<tr class="tr_bnk_rec" '+Tr_style+'>';
                    T += '<td>'+D.det[n].trans_date+' </td>';
                    T += '<td>'+D.det[n].trans_desc+' </td>';
                    T += '<td align=center>'+D.det[n].trans_no+' </td>';
                    T += '<td>'+D.det[n].description+' </td>';
                    T += '<td align=right class="row_dr">'+D.det[n].dr+' </td>';
                    T += '<td align=right class="row_cr">'+D.det[n].cr+' </td>';
                    T += Tx;
                    T += '</tr>';  

                    checked_count++;              

                } 

                $(".f_dr_tot").html(D.sum.total_dr);
                $(".f_cr_tot").html(D.sum.total_cr);

                $(".rec_f_dr_tot").html(D.sum.reconcil_dr);
                $(".rec_f_cr_tot").html(D.sum.reconcil_cr);

                $(".bal_f_dr_tot").html( (parseFloat(D.sum.total_dr) - parseFloat(D.sum.reconcil_dr)).toFixed(2) );
                $(".bal_f_cr_tot").html( (parseFloat(D.sum.total_cr) - parseFloat(D.sum.reconcil_cr)).toFixed(2) );

                $("#closing_bal").val(D.sum.closing_bal);
                $("#statment_closing_bal").val(D.sum.statment_closing_bal);

                set_bal_diff();

                $(".tblbankrec tbody").html(T);

            }



        }else{            
            alert("No data found for this number "+ no);
        }


    },"json");


}

function set_bal_diff(){

    if (isNaN( parseFloat( $("#statment_closing_bal").val() ) )){
        var statment_closing_bal = 0;
    }else{
        var statment_closing_bal = parseFloat( $("#statment_closing_bal").val() );
    }

    var closing_bal = parseFloat($("#closing_bal").val());

    $("#bal_diff").val( (closing_bal - statment_closing_bal).toFixed(2) );

}


$(document).on("keypress","#account_desc",function(){
    $( "#account_desc" ).autocomplete({
        source: "index.php/main/load_data/t_bankrec_n/get_acc_list",
        select: function (event, ui) {                  
            var d = ui.item.value;
            d = d.split(" - ");      

            $(this).parent().find( "#account_desc" ).val(d[0] + " " +d[1]);
            $(this).parent().find( "#acc_code").val(d[0]);     

            $("#btnLoad").focus();

        },
        delay : 500
    });
});

$(document).on("click","#btnLoad",function(){

    if(load_data_validate()){

        showActionProgress("Please wait...");

        $(".tblbankrec tbody").html('<tr> <td colspan="7">No data</td> </tr>');

        $(".f_dr_tot").html(0);
        $(".f_cr_tot").html(0);

        $("#total_dr").val(0);
        $("#total_cr").val(0);

        $.post("index.php/main/load_data/t_bankrec_n/get_acc_entries",{
            fd : $("#from_date").val(),
            td : $("#to_date").val(),
            acc_code : $("#acc_code").val()
        },function(D){

            closeActionProgress(); 

            if (D.s == 1){

                var T = '';  

                var checked_count  = 0;

                rec_dr_tot = 0;
                rec_cr_tot = 0;

                $(".rec_f_dr_tot").html("0.00");
                $(".rec_f_cr_tot").html("0.00");
                $(".bal_f_dr_tot").html("0.00");
                $(".bal_f_cr_tot").html("0.00");

                $("#op_bal").val(D.op_bal);
                $("#closing_bal").val(D.op_bal);

                for(n = 0 ; n < D.det.length ; n++){
                    
                    T += '<tr class="tr_bnk_rec"><input type="hidden" name="ar_bc[]" value="'+D.det[n].bc+'">';
                    T += '<td>'+D.det[n].ddate+' <input type="hidden" name="ar_trans_date[]" value="'+D.det[n].ddate+'"> </td>';
                    T += '<td>'+D.det[n].trans_desc+' <input type="hidden" name="ar_trans_code[]" value="'+D.det[n].trans_code+'"> </td>';
                    T += '<td align=center>'+D.det[n].trans_no+' <input type="hidden" name="ar_trans_no[]" value="'+D.det[n].trans_no+'"></td>';
                    T += '<td>'+D.det[n].description+" " +D.det[n].cheque_no+' - '+D.det[n].note+' <input type="hidden" name="ar_description[]" value="'+D.det[n].description+'"></td>';
                    T += '<td align=right class="row_dr">'+D.det[n].dr_amount+' <input type="hidden" name="ar_dr[]" value="'+D.det[n].dr_amount+'"></td>';
                    T += '<td align=right class="row_cr">'+D.det[n].cr_amount+' <input type="hidden" name="ar_cr[]" value="'+D.det[n].cr_amount+'"></td>';
                    T += '<td align=center><input type="checkbox" class="chk_bnk_rec"><input type="hidden" name="ar_auto_no[]" value="'+D.det[n].auto_no+'"></td>';

                    T += '</tr>';  

                    checked_count++;              

                }           
                

                //$(".row_count").html(checked_count);
                $(".f_dr_tot").html(D.dr.toFixed(2));
                $(".f_cr_tot").html(D.cr.toFixed(2));

                $("#total_dr").val(D.dr.toFixed(2));
                $("#total_cr").val(D.cr.toFixed(2));

                $("#bal_diff").val('');

                $("#grid_row_count").val(D.count);



            }else if( D.s == 2 ){

                alert("A reconciliation exist within selected date range. (Last date "+D.last_rec_date+" )");
                return;

            }else{
                T = '<td colspan="7">No data</td>';
            }


            $(".tblbankrec tbody").html(T);


        },"json");

    }

});

var rec_dr_tot = 0;
var rec_cr_tot = 0;

$(document).on("click",".chk_bnk_rec",function(){

    var ch = false;

    $(".chk_bnk_rec").each(function(){
        if ( $(this).is(":checked") ){
            ch = true;
        }
    });

    if (ch){
        $(".chk_all").prop("checked",1);
    }else{
        $(".chk_all").prop("checked",0);
    }



    if( $(this).is(":checked") ){
        $(this).parent().parent().css({"background-color":"#eaeaea","color":"green"});    
        rec_dr_tot += parseFloat($(this).parent().parent().find(".row_dr").html());
        rec_cr_tot += parseFloat($(this).parent().parent().find(".row_cr").html());
    }else{
        $(this).parent().parent().css({"background-color":"#ffffff","color":"#000000"});    
        rec_dr_tot -= parseFloat($(this).parent().parent().find(".row_dr").html());
        rec_cr_tot -= parseFloat($(this).parent().parent().find(".row_cr").html());
    }    

    // reconcile total section
    $(".rec_f_dr_tot").html(rec_dr_tot.toFixed(2));
    $(".rec_f_cr_tot").html(rec_cr_tot.toFixed(2));    

    $("#reconcil_dr").val(rec_dr_tot.toFixed(2));
    $("#reconcil_cr").val(rec_cr_tot.toFixed(2));        

    // end reconcile total section


    // calculated total section
    var f_dr_tot = parseFloat($(".f_dr_tot").html());
    var rec_f_dr_tot = parseFloat($(".rec_f_dr_tot").html());    
    $(".bal_f_dr_tot").html( (f_dr_tot - rec_f_dr_tot).toFixed(2) );

    var f_cr_tot = parseFloat($(".f_cr_tot").html());
    var rec_f_cr_tot = parseFloat($(".rec_f_cr_tot").html());    
    $(".bal_f_cr_tot").html( (f_cr_tot - rec_f_cr_tot).toFixed(2) );
    // end calculated total section


    // closing balance

    var op_bal = parseFloat($("#op_bal").val());
    var rec_dr_amount = parseFloat($(".rec_f_dr_tot").html());
    var rec_cr_amount = parseFloat($(".rec_f_cr_tot").html());   

    $("#closing_bal").val(  ((op_bal+rec_dr_amount) - rec_cr_amount).toFixed(2)  );

    // end closing balance

    set_bal_diff();

});

$(document).on("mouseover",".tr_bnk_rec",function(){
    $(this).css({"background-color":"green","color":"#ffffff"});
});

$(document).on("mouseout",".tr_bnk_rec",function(){

    if ( $(this).find(".chk_bnk_rec").is(":checked") ){
        $(this).css({"background-color":"#eaeaea","color":"green"});
    }else{
        $(this).css({"background-color":"#ffffff","color":"#000000"});
    }

});

function validate_voucher_save(){

    var grid_row_count = parseInt($("#grid_row_count").val());    

    if (grid_row_count == 0){
        alert("No data to save");
        return false;
    }


    var chk_checked = false;

    $(".chk_bnk_rec").each(function(){
        if ( $(this).is(":checked") ){
            chk_checked = true;
        }
    });

    if (!chk_checked){
        alert("No any entry marked.");
        return false;
    }

    return true;
}

function load_data_validate(){

    var fd = $('#from_date').val().replace('-','/');
    var td = $('#to_date').val().replace('-','/');

    if(fd > td){            
        alert("Invalid date selection");
        return false;
    }

    if ($("#acc_code").val() == ''){
        alert("Please select account");
        return false;
    }

    return true;

}







































function save(){

    if (validate_voucher_save()){

        //showActionProgress("Saving...");       

        var frm = $('#form_');
        
        $(".ar_chk_cl").remove();
        $(".chk_bnk_rec").each(function(){
            if ( $(this).is(":checked") ){
                frm.append('<input type="hidden" name="ar_chk[]" value="1" class="ar_chk_cl"  />');                
            }else{
                frm.append('<input type="hidden" name="ar_chk[]" value="0" class="ar_chk_cl"  />');
            }
        });

        $.ajax({
            type: frm.attr('method'), 
            url: frm.attr('action'), 
            data: frm.serialize(), 
            dataType: "json", 
            success: function (D) {
                //closeActionProgress(); 
                
                if (D.s == 1 ){
                    
                    $("#no").val(D.next_no);

                    alert("Save success");

                    location.href = '';

                }else{
                    
                }
                 
            } 
        });

    }

}

