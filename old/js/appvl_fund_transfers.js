var page_name = getURL_pagename_var();
var Tm

$(document).ready(function(){
    
    getApprovalRequests('fund_transfers');

});

$(document).on("click",".approve_chq_wdr_rqu",function(){

    var no = $(this).attr("no");    
    var bc = $(this).attr("bc");

    showActionProgress("Please wait ...");

    $(this).prop("disabled",true);

    $.post('index.php/main/load_data/appvl_fund_transfers/approve_cheque_withdraw_request',{
        no: no,
        bc : bc
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            showMessage("s","Update success");                
            window.location.hash = "#tabs-3";
            location.reload();
        }else if(D.s == 2){
            alert("Unable to proceed. This request was canceled or removed.");            
            location.reload();
        }else{
            showMessage("e","Update failed");
        }
        

    },"json");

});


$(document).on("click",".approve_BtoB_rqu",function(){

    var no = $(this).attr("no");    
    var bc = $(this).attr("bc");

    showActionProgress("Please wait ...");

    $(this).prop("disabled",true);

    $.post('index.php/main/load_data/appvl_fund_transfers/approve_BtoB_rqu',{
        no: no,
        bc : bc
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            showMessage("s","Update success");                
            window.location.hash = "#tabs-3";
            location.reload();
        }else if(D.s == 2){
            alert("Unable to proceed. This request was canceled or removed.");            
            location.reload();
        }else{
            showMessage("e","Update failed");
        }
        

    },"json");

});


$(document).on("click",".reject_chq_wdr_rqu",function(){


    var tr_type = $(this).attr("tr_type");

    if (tr_type == 3){ var c_msg = 'Confirm cheque withdrawal request reject'; }
    if (tr_type == 5){ var c_msg = 'Confirm bank to bank transfer request reject'; }


    if (confirm(c_msg)){

        var no = $(this).attr("no");   
        var bc = $(this).attr("bc"); 

        showActionProgress("Please wait ...");

        $(this).prop("disabled",true);

        $.post('index.php/main/load_data/appvl_fund_transfers/reject_cheque_withdraw_request',{
            no: no,
            bc : bc
        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                showMessage("s","Request rejected");                
                window.location.hash = "#tabs-3";
                location.reload();
            }else{
                showMessage("e","Reject failed");
            }

        },"json");

    }

});



function getApprovalRequests(page_name){

    $.ajax({
        method: "POST",
        url: "index.php/main/load_data/approvals/getApprovalRequests",              
        data : {page_name : page_name},
        dataType: "json"
    }).done(function( D ) {

        if (D.data != 0){
            $(".discount_requests").html(D.data);
        }else{
            $(".discount_requests").html("<tr><td colspan='10' align='center'>No requests found</td></tr>");
        }

        Tm = setTimeout("getApprovalRequests('"+page_name+"')",10000);

    });

}