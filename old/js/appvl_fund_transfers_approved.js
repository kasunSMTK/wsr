var page_name = getURL_pagename_var();
var Tm

$(document).ready(function(){
    
    getApprovalRequests('fund_transfers_approved');

});



$(document).on("click",".bc_name_cls",function(){



    $(".all_rows").css({"display":"none"});

    $("."+$(this).attr("cn")).css("display","table-row");    
    

    $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 300);   

});


$(document).on("click",".approve_chq_wdr_rqu",function(){

    var no = $(this).attr("no");    

    showActionProgress("Please wait ...");

    $(this).prop("disabled",true);

    $.post('index.php/main/load_data/appvl_fund_transfers/approve_cheque_withdraw_request',{
        no: no
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            showMessage("s","Update success");                
            window.location.hash = "#tabs-3";
            location.reload();
        }else{
            showMessage("e","Update failed");
        }

    },"json");

});

$(document).on("click",".reject_chq_wdr_rqu",function(){

    if (confirm('Confirm cheque withdrawal request ')){

        var no = $(this).attr("no");    

        showActionProgress("Please wait ...");

        $(this).prop("disabled",true);

        $.post('index.php/main/load_data/appvl_fund_transfers/reject_cheque_withdraw_request',{
            no: no
        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                showMessage("s","Request rejected");                
                window.location.hash = "#tabs-3";
                location.reload();
            }else{
                showMessage("e","Reject failed");
            }

        },"json");

    }

});



function getApprovalRequests(page_name){

    $(".discount_requests").html("<tr><td colspan='8' align='center'>Please wait...</td></tr>");

    $.ajax({
        method: "POST",
        url: "index.php/main/load_data/approvals/getApprovalRequests",              
        data : {page_name : page_name},
        dataType: "json"
    }).done(function( D ) {

        if (D.data != 0){
            $(".discount_requests").html(D.data);
        }else{
            $(".discount_requests").html("<tr><td colspan='8' align='center'>No requests found</td></tr>");
        }

        //Tm = setTimeout("getApprovalRequests('"+page_name+"')",10000);

    });

}


$(document).on("click","#btnfundtrfApproveCancel",function(){

    var no = $(this).attr('no');

    if (confirm("Confirm approval cancel")){

        showActionProgress("Please wait...");
        
        $.post("index.php/main/load_data/approvals/cancel_chq_withdraw_approval",{
            no : no
        },function(D){              

            window.location.href += "";
            location.reload();                
            
        },"json");

    }

});