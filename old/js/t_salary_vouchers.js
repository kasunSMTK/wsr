$(document).on('click', '.remove_chq_row', function(event) {

});

$(document).ready(function(){


	$("#btnSendtoApproval").click(function(){
		save();
	});


	$("#btnImport").click(function(){

		showActionProgress("Please wait...");

		$.post("index.php/main/load_data/t_salary_vouchers/import_salary_data",{
			y : $("#year :selected").val(),
			m : $("#month :selected").val(),
			bc : $("#bc :selected").val()
		},function(D){
			closeActionProgress();

			var T = "";


			if (D.data != 0){

				for(n = 0 ; n < D.data.length ; n++){

					T += "<tr>";
					T += "<td>"+(n+1)+"</td>";					
					
					T += "<td>"+D.data[n].emp_no+"					 <input type='hidden' name='emp_no[]'  value='"+D.data[n].emp_no+"'></td>";
					T += "<td>"+D.data[n].nName+"					 <input type='hidden' name='nName[]'   value='"+D.data[n].nName+"'></td>";
					T += "<td align='right'>"+D.data[n].net_salary+" <input type='hidden' name='net_sal[]' value='"+D.data[n].net_salary+"'><input type='hidden' name='bc[]' value='"+D.data[n].bc+"'><td>";
					
					T += "<td>Action</td>";
					T += "</tr>";

				}

			}else{
				
				T +="<tr><td colspan='5' align='center'>No data found</td></tr>";

			}

			T += "";

			$(".tbl_sv tfoot").html(T);

			$("#btnSendtoApproval").removeClass("btn_regular_disable").addClass("btn_regular").attr("disabled",false);

			
		},"json");
	
	});
	
});

function save(){	

	showActionProgress("Please wait...");

	if (validate_form()){
		
		var frm = $('#form_');
		
		$.ajax({

			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				closeActionProgress();
				
			}
		});

	}

}

function validate_form(){
	return true;
}