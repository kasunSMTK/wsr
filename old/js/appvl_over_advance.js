var page_name = getURL_pagename_var();
var Tm


$(document).on("click",".close_item_app_popup",function(){		
	$(".article_view_inner,.article_view_outter").css("display","none");
});

$(document).ready(function(){

	getApprovalRequests('over_advance');



	$(".approval_tab").click(function(event) {		
		$(".approval_tab_selected").css("border-bottom","1px solid #ccc");
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".approval_tab_selected").click(function(event) {				
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".input_text_app_search").keyup(function () {
		
		var rows = $(".discount_requests .request_row").hide();
		
		if (this.value.length) {
		    var data = this.value.split(" ");
		    $.each(data, function (i, v) {
		        rows.filter(":contains('" + v + "')").show();
		    });
		} else rows.show();
	});

	
	$(document).on( "keypress", ".input_approval", function(){
		clearTimeout(Tm);
		Tm = setTimeout("getApprovalRequests('"+page_name+"')",10000);
	});

	$(document).on( "click", ".input_approval", function() { 		
		$(this).select();
		clearTimeout(Tm);
		Tm = setTimeout("getApprovalRequests('"+page_name+"')",10000);
	});

	$('.app_requ_link').click(function(){
		
		clearTimeout(Tm);

		var x =  $(this).attr("href");
		x = x.split("#l=");		
		page_name = x[1];
		getApprovalRequests(x[1]);

	});

	$("#btnCancelForward_bc_request").click(function(event) {
		$(".bc_cash_balance_grid").html("");
		$("#f_bc").html('Loading branches...');
		$("#bc_cash_balance").html('0.00');
		
		$(".app_popup_bg,.app_popup").css("visibility","hidden");
	});

	
	$("#btnForward_bc_request").click(function(event) {
		forward_request_to_selected_bc();		
	});

});

function appr(loanno, approved_amount_id,row_id,requested_amount,comment_txt_id){
	var approved_amount = $("#" + approved_amount_id ).val();
	var comment 		= $("#" + comment_txt_id ).val();
	
	if (validateAmount_no_deci(approved_amount  , "") && parseFloat(approved_amount) <= parseFloat(requested_amount)  ){
		makeAprove(loanno,approved_amount,row_id,requested_amount,comment);
	}else{
		alert("Invalid amount")
	}
}

function appr_vou(no,S,paid_acc){

	clearTimeout(Tm);
	
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/approvals/appr_vou",{
		no : no,		
		record_status : S,
		paid_acc : paid_acc
	},function(D){				

		window.location.href += "#l=general_voucher";
		location.reload();
		
		//window.location.hash = "";
		//window.location.href = window.location.href;
		
	},"json");
	
}

function makeAprove(loanno,approved_amount,row_id,requested_amount,comment){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
		loanno : loanno,
		approved_amount : approved_amount,
		record_status : "A",
		comment : comment
	},function(D){
		$("#"+row_id).remove();
		closeActionProgress();
	},"json");
}

function rejt(loanno, row_id){
	if(confirm("Do you want reject this request? ")){
		showActionProgress("Loading...");
		$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
			loanno : loanno,		
			record_status : "R"
		},function(D){
			$("#"+row_id).remove();
			closeActionProgress();
		},"json");
	}
}


function getApprovalRequests(page_name){

	$.ajax({
		method: "POST",
		url: "index.php/main/load_data/approvals/getApprovalRequests",				
		data : {page_name : page_name},
		dataType: "json"
	}).done(function( D ) {

		if (page_name == "fund_transfers" || page_name == "general_voucher"){			
			$(".request_row").hide();
			$(".fund_transfer_title_row").show();			
		}else{			
			$(".request_row").show();
			$(".fund_transfer_title_row").hide();			
		}
	
		var old_loans = Array();
		var old_loans_II = Array();
		var new_result = Array();
		var num1 = 0;
		
		$(".loan_no_list").each(function(){ old_loans[num1] = $(this).html(); num1++; });		

		if (D.discount != 0){

			$(".div_no_request").remove();

			for (num = 0 ; num < D.discount.length ; num++){ new_result[num] = D.discount[num].loanno; }
			var removeList = old_loans.filter(function(obj) { return new_result.indexOf(obj) == -1; });		
			
			$(".loan_no_list").each(function(){ 
				if(jQuery.inArray($(this).html(), removeList) !== -1){
					$(this).parent().html("").css({"height":"30px","border-bottom":"1px solid #ffffff"}).slideUp(500);
				}
			});

			var T = "";
			var clz = "";
			var tno = "";

			$(".loan_no_list").each(function(){ old_loans_II[num1] = $(this).html(); num1++; });
			
			for (num = 0 ; num < D.discount.length ; num++){
				if(jQuery.inArray(D.discount[num].loanno, old_loans_II) == -1){		
					txt_id = "txt_"+num;
					row_id = "row_"+num;

					if (D.discount[num].request_type == "EL"){clz = "request_type_bbl_red"; }
					if (D.discount[num].request_type == "DI"){clz = "request_type_bbl_green"; }
					if (D.discount[num].request_type == "CN"){clz = "request_type_bbl_gray"; }

					$(".discount_requests").html("");
					
					T += '<div id="'+row_id+'" class="request_row"><div class="RR_title_content loan_no_list '+clz+'" style="text-align:center;width:40px">'+D.discount[num].auto_no+'</div><div class="RR_title_content" style="width:75px;text-align:right">'+D.discount[num].payable_amount+'</div><div class="RR_title_content" style="width:75px;text-align:right">'+D.discount[num].goldvalue+'</div><div class="RR_title_content" style="width:70px;padding-left:10px">'+D.discount[num].requested_by+'</div><div class="RR_title_content" style="width:70px;padding-left:10px">'+D.discount[num].bc+'</div><div class="RR_title_content" style="width:140px">'+D.discount[num].requested_datetime+'</div><div class="RR_title_content" style="width:120px;text-align:right;margin:-12px 5px 0px 4px ;padding:10px; border-left:2px solid Red;border-right:2px solid Red">'+D.discount[num].requested_amount+'</div> <div class="RR_title_content" style="width:250px;padding-right:10px"><input type="text" style="width:100%" class="input_approval amount" id="comment_'+txt_id+'"></div> <div class="RR_title_content" style="width:120px"><input type="text" class="input_approval amount" id="'+txt_id+'"></div><div class="RR_title_content" style="width:60px">'+D.user+'</div><div class="RR_title_content" style="width:140px"><a class="link_app" onClick=view_articles_app_window("'+D.discount[num].loanno+'")>Articles</a> | <a class="link_app" onClick=appr("'+D.discount[num].loanno+'","'+txt_id+'","'+row_id+'","'+D.discount[num].requested_amount+'","comment_'+txt_id+'")>Approve</a> | <a class="link_app" onClick=rejt("'+D.discount[num].loanno+'","'+row_id+'")>Reject</a></div></div>';
					
				}
			}

			$(".discount_requests").append(T);
		}else{
			$(".discount_requests").html("<div class='div_no_request'>No requests found</div>");
		}

		Tm = setTimeout("getApprovalRequests('"+page_name+"')",25000);

	});

}

function view_articles_app_window(no=''){

	showActionProgress("Please wait...");

	$(".item_holder_app").html("");	
	
	$.post("index.php/main/load_data/approvals/view_articles_app_window",{		
		no : no
	},function(D){		
		
		closeActionProgress();

		$(".article_view_inner,.article_view_outter").css("display","block");

		var ti = '<div style="padding:10px;padding-right:23px;text-align:right;font-size:12px"><div style="float:left;font-size:24px">'+no+'</div><div class="close_item_app_popup">Close</div></div><br>';

		ti += '<div id="pn1" style="border-bottom:1px dotted #ccc;height:47px"><div style="width:135px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Category" style="width:120px" id="cat_code_desc" class="input_text_regular_new_pawn catC" readonly="readonly"></div><div style="width:175px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Item" style="width:165px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn itemC"></div><div style="width:140px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Condition" style="width:130px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn conC"></div><div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Karatage" style="width:97px" id="gold_type_desc" readonly="readonly" class="input_text_regular_new_pawn gtypeC"></div><div style="width:60px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Quality" style="width:47px" id="gold_qlty" readonly="readonly" class="input_text_regular_new_pawn gquliC"></div><div style="width:50px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Qty" style="width:37px" id="qty" readonly="readonly" class="input_text_regular_new_pawn qtyC"></div><div style="width:76px;float:left" class="text_box_holder_new_pawn"><input type="text" value="0" style="width:63px" id="t_weigth" readonly="readonly" class="input_text_regular_new_pawn t_weigthC"></div><div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="0" style="width:60px" id="p_weigth" readonly="readonly" class="input_text_regular_new_pawn p_weigthC"></div><div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Densi Read" style="width:60px" id="d_weigth" readonly="readonly" class="input_text_regular_new_pawn d_weigthC"></div><div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="0" style="width:100px; text-align:right" id="value" readonly="readonly" class="input_text_regular_new_pawn valueC"></div></div>';

		$(".item_holder_app").html(ti);
		$(".item_holder_app").append(D.list);


		var t_weigthC = 0;
		$(".t_weigthC").each(function(){
			t_weigthC += parseFloat( $(this).val() );
		});

		var p_weigth = 0;
		$(".p_weigthC").each(function(){
			p_weigth += parseFloat( $(this).val() );
		});

		var valueC = 0;
		$(".valueC").each(function(){
			valueC += parseFloat( $(this).val() );
		});

		
		ti = '<div id="pn1" style="border-bottom:1px dotted #ccc;height:47px"><div style="width:135px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:120px" id="cat_code_desc" class="input_text_regular_new_pawn catC" readonly="readonly"></div><div style="width:175px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:165px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn itemC"></div><div style="width:140px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:130px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn conC"></div><div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:97px" id="gold_type_desc" readonly="readonly" class="input_text_regular_new_pawn gtypeC"></div><div style="width:60px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:47px" id="gold_qlty" readonly="readonly" class="input_text_regular_new_pawn gquliC"></div><div style="width:50px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Total" style="width:37px" id="qty" readonly="readonly" class="input_text_regular_new_pawn qtyC"></div><div style="width:76px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'+t_weigthC+'" style="width:63px" id="t_weigth" readonly="readonly" class="input_text_regular_new_pawn t_weigthC"></div><div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'+p_weigth+'" style="width:60px" id="p_weigth" readonly="readonly" class="input_text_regular_new_pawn p_weigthC"></div><div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:60px" id="d_weigth" readonly="readonly" class="input_text_regular_new_pawn d_weigthC"></div><div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="'+valueC+'" style="width:100px; text-align:right" id="value" readonly="readonly" class="input_text_regular_new_pawn valueC"></div></div>';
		$(".item_holder_app").append(ti);


		$('.item_holder_app').find('a').each(function(){
			$(this).parent().remove();
		});
		
		$('.item_holder_app').find('input[type="hidden"]').each(function(){
			$(this).remove();
		});

		$('.item_holder_app').find('input').each(function(){
						
		});


	},"json");	

}

function appr_ft(no,row_id,S,approved_amount,chq_amount,r_bc = ""){
	
	var approved_amount = approved_amount;			
	showActionProgress("Please wait...");	
	
	$.post("index.php/main/load_data/approvals/makeAproveOrRejectFT",{
		
		no : no,
		approved_amount : approved_amount,
		chq_amount : chq_amount,
		status : S,
		r_bc : r_bc

	},function(D){
		
		//$("#"+row_id).remove();
		closeActionProgress();

	},"json");	

}

function appr_ft_popup(no,row_id,S,approved_amount,chq_amount,d){

	$(".requ_amt").val("");
	$(".bc_cash_balance_grid").html("<span style='font-size:20px;font-family:bitter;color:Green'>Calculating branch cash balances, Please wait...</span>");
	$(".app_popup_bg,.app_popup").css("visibility","visible");

	$.post("index.php/main/load_data/approvals/get_bc_list",{
			
	},function(D){	

		$(".bc_cash_balance_grid").html(D.bc_cash_balance_grid);
		$(".requ_amt").html(approved_amount);
		
		var d = "<select id='ho_selected_bc'>";	
		for (n_bc = 0 ; n_bc < D.bc_list.length ; n_bc++){d += "<option value='"+D.bc_list[n_bc].bc+"'>"+D.bc_list[n_bc].name+"</option>"; }	d += "</select>";
		
		$("#aa").val(no);
		$("#bb").val(row_id);
		$("#cc").val(S);
		$("#dd").val(approved_amount);
		//$("#dd").val(chq_amount);
		//$("#f_bc").html(d);

	},"json");

}

function forward_request_to_selected_bc(){

	var checked = false;
	var checked_bc = "";
	var bc_cash_balance = 0;

	$(".cash_bal_option").each(function(index, el) {
		if ($(this).is(":checked")){
			checked = true;
			checked_bc = $(this).val();
			bc_cash_balance = parseFloat($(this).parent().find('input[type="hidden"]').val());
		}
	});

	if (!checked){
		showActionProgress("");	
		showMessage("e","Please select a branch");
		return;
	}

	var requested_amount = parseFloat($(".requ_amt").html());

	if (requested_amount > bc_cash_balance){
		showActionProgress("");	
		showMessage("e","Unable to pay requested amount with this branch cash balance");
		return;	
	}

	no = $("#aa").val();
	row_id = $("#bb").val();
	S = $("#cc").val();
	approved_amount = $("#dd").val();
	chq_amount = $("#dd").val();
	
	d = checked_bc; // $("#ho_selected_bc :selected").val();

	appr_ft(no,row_id,S,approved_amount,chq_amount,d);
	$("#btnCancelForward_bc_request").click();
}

function appr_ft_popup_ant(){
	alert("Update action not taken by receiving branch");
}