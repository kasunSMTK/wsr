var page_name = getURL_pagename_var();
var Tm;

$(document).on("click",".close_je_view",function(){
	$(".je_view_popup_bg,.je_view_popup").css("display",'none');	
	$(".je_view_popup").html("");
});



$(document).on("click",".view_je",function(){

	$(".je_view_popup_bg,.je_view_popup").css("display",'table');

	showActionProgress("Please wait...");
	
	$.post("index.php/main/load_data/appvl_je/view",{		
		no : $(this).attr('no'),
		bc : $(this).attr('bc')
	},function(D){		
		closeActionProgress();

		if (D.s == 1){
			
			$(".je_view_popup").html(D.data);

		}else{
			alert('error');
		}

	},"json");
	
});

$(document).on("click",".reject_je",function(){	

	if (confirm("Do you want cancel this approval")){
		showActionProgress("Please wait...");
		
		$.post("index.php/main/load_data/appvl_je_approved/makeAppCancel",{
			no : $(this).attr('no'),
			bc : $(this).attr('bc')
		},function(D){		
			closeActionProgress();

			if (D.s == 1){
				alert("Journal entry approval canceled");
				location.href = '';
			}else{
				alert('error');
			}

		},"json");
	}
	
});






$(document).ready(function(){
	getApprovalRequests('je_approval_approved');
});

function getApprovalRequests(page_name){

	$.ajax({
		method: "POST",
		url: "index.php/main/load_data/appvl_je_approved/getApprovalRequests",				
		data : {page_name : page_name},
		dataType: "json"
	}).done(function( D ) {

		var t = '';

		if (D.s == 1){

			for (n = 0 ; n < D.det.length ; n++){

				t +="<tr>";
				t +="<td>"+(n+1)+"</td>";
				t +="<td>"+D.det[n].bc+"</td>";
				t +="<td>"+D.det[n].no+"</td>";
				t +="<td>"+D.det[n].date+"</td>";
				t +="<td>"+D.det[n].journal_type+"</td>";
				t +="<td>"+D.det[n].description+"</td>";
				t +="<td>"+D.det[n].narration+"</td>";
				t +="<td>"+D.det[n].oc+"</td>";
				t +="<td>"+D.det[n].action_date+"</td>";
				t +="<td align='right'>"+D.det[n].total+"</td>";
				t +="<td>"+D.det[n].time+"</td>";
				t +="<td width='155'><a class='view_je' bc='"+D.det[n].bc_+"' no='"+D.det[n].no+"'>View</a><input type='button' value='Cancel Approval' class='btn_regular reject_je' bc='"+D.det[n].bc_+"' no='"+D.det[n].no+"' style='width:120px;background-color:red;color:#fff'></td>";
				t +="</tr>";

			}

		}else{
			t += "<tr><td align='center' colspan='11'>No data found</td></tr>";
		}


		setTimeout('getApprovalRequests()',25000);

		$(".discount_requests_a").html(t);


	});

}

