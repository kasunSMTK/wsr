

$(document).on("change","#bc",function(){
    set_bc_users( $("#bc :selected").val() );
});

$(document).ready(function(){
    
    $('select').css({
        padding: '4px',
        'font-size': '18px',
        'text-transform': 'capitalize',
        'width' : '100%',
        'border' : 'none'
    });

    $(":checkbox").removeAttr('checked');
    
    $("#user").change(function(){
        load_data();
    });

    $("#btnSave").click(function(event) {
        save();
    });

});


function load_data(){
    empty_grid();

    var r = "";

    $.post("index.php/main/get_data/s_add_role/", {

        id : $("#user option:selected").val()

    }, function(r){       

        var i = 0;

        $("#tgrid tr").each(function(){    

            $(this).css({
                'background-color': '#ffffff',
                'color': '#000000'
            });

        });

        $("#tgrid tr").each(function(){    
            
            
            $("#check"+r.d[i].role_id).prop("checked", true);
            
            $("#check"+r.d[i].role_id).parent().parent().css({
                'background-color': '#eaeaea',
                'color': '#000000'
            });
            
            //$("#date_from"+r.d[i].role_id).val(r.d[i].date_to);
            //$("#date_to"+r.d[i].role_id).val(r.d[i].date_from);            
            

            i++;
        
        });

    }, "json");

    

}





function get_data(){

        var x = y = new Array(); var a = 0;

        $("#tgrid tr").each(function(){

            y = new Array();

            $(this).find("td").each(function(i){

                if(i==0){

                    y[0] = $(this).html();

                }else if(i == 2){

                    y[1] = $(this).children().attr("checked")? 1:0;

                }else if(i == 3){

                    y[2] = $(this).children().val();

                }else if(i == 4){

                    y[3] = $(this).children().val();

                }

            });

            if(y[1] == 1) { x[a++] = y; }

        });

        

        return x;

    }



function save(){
    //$("#form_").submit();
    var frm = $("#form_");
    $.ajax({
    type: frm.attr('method'),
    url: frm.attr('action'),
    data: frm.serialize(),
    dataType: "json",
    success: function (pid) {
            if(pid == 1){
                // input_reset(); pop_note('Saving Success.', 'g');
                // $("#nic_no").removeAttr("readonly"); $("#btnSave").val("Save");
                // $("#nic_no").focus(); $("#img").html(imag_up);
                // reset_map(); img_uploads(); $("#code_").val(0);
                 location.href="";

            }else if(pid == 2){
                //alert("You don't have permission for add records");
                pop_note("You don't have permission for save records.", 'r');
            }else if(pid == 3){
                //alert("You don't have permission for edit records");
                pop_note("You don't have permission for edit records.", 'r');
            }else{
                alert("Error : "+pid);
            }
    }
    });
}



function check_code(){

    var code = $("#code").val();

    $.post("index.php/main/load_data/s_module/check_code", {

        code : code

    }, function(res){

        if(res == 1){

            if(confirm("This code ("+code+") already added. \n\n Do you need edit it?")){

                set_edit(code);

            }else{

                $("#code").val('');

            }

        }

    }, "text");

}



function validate(){

    if($("#user option:selected").attr('value') == "0"){

        alert("Please select user.");

        return false;

    }else{

        return true;

    }

}

    

function set_delete(code){

    if(confirm("Are you sure delete "+code+"?")){

        loding();

        $.post("index.php/main/delete/s_module", {

            code : code

        }, function(res){

            if(res == 1){

                location.reload();

            }else{

                alert("Item deleting fail.");
                 //pop_note("You don't have permission for add records.", 'r');

            }

            loding();

        }, "text");

    }

}

    

function set_edit(code){

    loding();

    $.post("index.php/main/get_data/s_module", {

        code : code

    }, function(res){

        $("#code_").val(res.m_code);

        $("#code").val(res.m_code);

        $("#code").attr("readonly", true);

        $("#des").val(res.m_description);

        

        loding(); input_active();

    }, "json");

}



function empty_grid(){

    

    $("#tgrid tr").each(function(){

            $(this).find("td").each(function(i){

                

                if(i == 2){

                    $(this).children().removeAttr("checked");

                }else if(i == 3){

                    $(this).children().val("0000-00-00");

                }else if(i == 4){

                    $(this).children().val("0000-00-00");

                }

            });

        });

}


function set_bc_users(bc){    

    $.post("index.php/main/load_data/S_add_role/set_bc_users", {
        bc : bc
    }, function(D){

        $("#user").html(D.dd);

    }, "json");

}