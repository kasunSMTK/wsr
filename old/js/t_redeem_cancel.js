$(document).ready(function(){

    $("#btnGetBillno").click(function(){ 

        if ($("#billno").val() == ""){
            alert("Please enter bill number");
            return;
        }

        getRedeemedBill( $("#billno").val() );
        
    });    

    $("#btnCancelRedeem").click(function(){ 

        if( $("#billno").val() == "" ){
            alert("Please enter redeem bill number");
            return;            
        }
        
        if ( $("#redeen_cancel_reason").val() == "" ){
            alert("Please enter reason for cancel this redeem");
            return;
        }

        
        save($("#billno").val());

    }); 

});

function set_info_line_height(){

    $(".div_cus_info").find('div').append("<br><br>");

    $(".div_cus_info div").find('span').each(function(){

        if ( $(this).html() == "" ){
            $(this).parent().remove();
        }

    });

}


function save(billno){

    showActionProgress("Please wait...");

    $("#btnCancelRedeem").prop("disabled",true);

    $.post("index.php/main/load_data/t_redeem_cancel/save",{
        
        billno      : billno,
        cus_serno   : $("#cus_serno").val(),
        bc          : $("#bc").val(),
        r_c_r       : $("#redeen_cancel_reason").val(),
        cus_id      : $("#cus_id").val()

    },function(D){  
        
        if (D.s == 1){
            alert("Redeem canceled");
            $("#btnCancelRedeem").prop("disabled",false);
            location.href = '';
        }else{
            alert("Error");
        }

        closeActionProgress();        

    },"json");
    

}

function getRedeemedBill(billno){
    
    showActionProgress("Please wait...");

    $.post("index.php/main/load_data/t_redeem_cancel/getRedeemedBill",{
        billno : billno
    },function(D){  
        
        if (D.s == 1){         
            
            $(".div_cus_info").html(D.cus_info);

            var d  = "<div><h3>Advance Amount</h3><span>" + D.loan_sum.requiredamount + "</span></div>";
                d += "<div><h3>Pawn Date</h3><span>" + D.loan_sum.ddate + "</span></div>";
                d += "<div><h3>Bill Type</h3><span>" + D.loan_sum.billtype + "</span></div>";
                d += "<div><h3>Branch</h3><span> " + D.loan_sum.bc + "</span></div>";

            $(".div_cus_info").append(d);

            $("#cus_serno").val(D.loan_sum.cus_serno);
            $("#bc").val(D.loan_sum.bc);
            $(".btn_rmd_holder").css("display","block");
            $("#cus_id").val(D.cus_id);

        }

        closeActionProgress();

        set_info_line_height();

    },"json");    

}

function validate_form(){    
    return true;
}