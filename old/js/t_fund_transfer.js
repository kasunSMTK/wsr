$(document).on('click', '.chk_chq_list', function(event) {
       
    if ($(this).is(":checked")){            
        $(this).parent().parent().css({
            "background-color":"Green",
            "color":"#FFFFFF"
        });
    }else{
        $(this).parent().parent().css({
            "background-color":"#FFFFFF",
            "color":"#000000"
        });
    }

});


$(document).on("change",".from_bc",function(){
    $(this).parent().find(".fb").val($(this).find(":selected").attr("bc"));
    $(this).parent().find(".fa").val($(this).find(":selected").attr("cash_acc"));
});

$(document).on("change","#from_acc",function(){   
    $('.hid_selected_bnk_acc').val(  $(this).find(" :selected").val() );
    $('.hid_bnk_acc_desc').val(  $(this).find(" :selected").text() );
});

function get_fund_transfer_data(){

    showActionProgress("Please wait...");

    $.post('index.php/main/load_data/t_fund_transfer/get_fund_transfer_data',{
        no: $("#transfer_no").val()
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){


            $(".tt").html(D.d.transfer_type);
            $(".ff").html(D.d.from_bc);
            $(".tb").html(D.d.to_bc);
            $(".aa").html(D.d.amount);
            

            if (D.d.status == 1){ var stat = "Waiting/Request sent"; }
            if (D.d.status == 2){ var stat = "Accept transfer request"; }
            if (D.d.status == 3){ var stat = "Reject transfer request"; }
            if (D.d.status == 4){ var stat = "Voucher printed"; }
            if (D.d.status == 5){ var stat = "Request forwarded"; }
            if (D.d.status == 6){ var stat = "Canceled by head office"; }
            if (D.d.status == 10){ var stat = "Transaction completed"; }

            $(".ss").html(stat);


            $(".cc").html(D.d.comment);

            $(".div_fntr_det").css("display","block");

            
        }else{
            showMessage("e","Error");
        }

    },"json");

}

function cancel_fund_transfer(no){

    showActionProgress("Please wait...");

    $.post('index.php/main/load_data/t_fund_transfer/cancel_fund_transfer',{
        no: no
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            alert("Cancel success");                
            window.location.hash = "#tabs-4";
            location.reload();
        }else{
            showMessage("e","Cancel failed");
        }

    },"json");

}


$(document).ready(function(){

    $( "#tabs" ).tabs();

    $("a").click(function() {
        
        $('html, body').animate({scrollTop: '0px'}, 1);
       // return false;

    });


    $(".btn_cancel_fund_transfer").click(function(){
        
        if (isNaN($("#transfer_no").val()) || $("#transfer_no").val() == ""  ){
            alert("Invalid fund transfer number");
            return false;
        }
        
        if ( confirm("Confirm fund transfer cancel") ){
            cancel_fund_transfer( $("#transfer_no").val() );
        }

    });


    $(".btn_get_fund_transfer_data").click(function(){
        
        if (isNaN($("#transfer_no").val()) || $("#transfer_no").val() == ""  ){
            alert("Invalid fund transfer number");
            return false;
        }        
        
        get_fund_transfer_data( $("#transfer_no").val() );        

    });

    


    $(".tt_opt").click(function(){

        if ( $(this).val() == "bank_deposit" ){

            showActionProgress("Loading bank accounts...");

            $.post('index.php/main/load_data/t_fund_transfer/get_bank_acc_dropdown',{
                
            }, function(D) {
                
                closeActionProgress();
                
                $(".div_bnk_dp_set").html(D.bnk_dd);
                $(".div_bnk_dp_set").append('<input type="hidden" name="to_acc"         class="hid_selected_bnk_acc">');
                $(".div_bnk_dp_set").append('<input type="hidden" name="bnk_acc_desc"   class="hid_bnk_acc_desc">');

            },"json");

        }else{
            var ca = $('.hid_ho_cash_acc').val();
            $(".div_bnk_dp_set").html('<input type="text" style="width:100%" class="class_to_acc input_text_regular_ftr" readonly="readonly" name="to_acc" value="'+ca+'">');

        }

    });



    $(".ftr_forward_request").click(function(){

        if ( $(this).parent().find(".from_bc :selected").val() == "Select Branch" ){
            alert("Please select branch to forward");
            return;
        }

        var no          = $(this).attr("no");
        var bc          = $(this).attr("bc");
        var from_bc     = $(this).parent().find('.fb').val();
        var from_acc    = $(this).parent().find('.fa').val();

        forward_request(no,from_bc,from_acc,bc);

    });

    $(".ftr_accept_request").click(function(){
        var tfr_no = $(this).attr("no");
        var bc = $(this).attr("bc");
        accept_request(tfr_no,bc);
    });

    $(".ftr_reject_request").click(function(){
        var tfr_no = $(this).attr("no");
        var bc = $(this).attr("bc");
        reject_request(tfr_no,bc);
    });

    $(".send_chq_wdr_rqu").click(function(){
        send_cheque_withdraw_request($(this));
    });

    $(".cancel_tfr_reque").click(function(){
        cancel_tfr_request($(this));
    });

    $(".cancel_tfr_reque_AAA").click(function(){
        cancel_tfr_request_AAA($(this));
    });

    $(".cancel_tfr_reque_BBB").click(function(){
        cancel_tfr_request_BBB($(this));
    });

    $(".cancel_tfr_reque_CCC").click(function(){
        cancel_tfr_request_CCC($(this));
    });


    $(".cancel_tfr_reque_DDD").click(function(){
        cancel_tfr_request_DDD($(this));
    });

    $(".cancel_tfr_reque_EEE").click(function(){
        cancel_tfr_request_EEE($(this));
    });


    $(".ftr_print_vocuher").click(function(event) {
        
        var tab_no = 2;
        var tfr_no = $(this).attr("no");
        var is_v_re_p = $(this).attr("is_v_re_p"); 
        var bc  = $(this).attr("bc"); 
        var tt  = $(this).attr("transfer_type");
        var fb  = $(this).attr("from_bc");

        if ( $(this).attr("tab_no") != undefined  ){
        	tab_no = $(this).attr("tab_no");
        }
                    
        $.ajax({
            type:'POST',
            url: 'index.php/main/load_data/t_fund_transfer/save_and_print_ftr_voucher',
            data: {no : tfr_no,is_v_re_p : is_v_re_p , bc : bc , tt : tt , fb : fb },
            dataType: "json",
            success: function (D) {

                closeActionProgress();

                if (D.s == 1){                    

                    $("#v_no").val(D.v_no);
                    $("#is_v_re_p").val(is_v_re_p);                    
                    $("#print_fund_trans_voucher_pdf").submit();                                        
                    
                    window.location.hash = "#tabs-"+tab_no;
                    location.reload();

                }else if(D.s == 2){
                    alert("Unable to proceed. This request was canceled or removed.");
                    window.location.hash = "#tabs-"+tab_no;
                    location.reload();
                }else{                  
                    showMessage("e","errror");
                }
            }
        });

    });


    $(".ftr_complete_transaction").click(function(){

        var tab_no = 2;
        var no = $(this).attr("no");

        if ( $(this).attr("tab_no") != undefined  ){
            tab_no = $(this).attr("tab_no");
        }

        showActionProgress("Updating...");

        $(this).prop("disabled",true);

        $.post('index.php/main/load_data/t_fund_transfer/update_incomming_request',{
            no: no
        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                showMessage("s","Update success");                
                window.location.hash = "#tabs-3";
                location.reload();

            }else if(D.s == 14){
                
                alert("Unable to proceed. This request was canceled or removed.");
                window.location.hash = "#tabs-"+tab_no;
                location.reload();
                
            }else{
                showMessage("e","Update failed");
            }

        },"json");

    });



    $(".cancel_tfr_transaction").click(function(){


        if (confirm("Confirm completed transaction cancel")){

            var no = $(this).attr("no");

            showActionProgress("Canceling transaction...");

            $(this).prop("disabled",true);

            $.post('index.php/main/load_data/t_fund_transfer/cancel_completed_transaction',{
                no: no
            }, function(D) {
                
                closeActionProgress();
                
                if (D.s == "1" ){
                    showMessage("s","Transaction cancel success");                
                    window.location.hash = "#tabs-2";
                    location.reload();
                }else{
                    showMessage("e","Transaction cancel failed");
                }

            },"json");

        }

    });


    $(".cheque_withdrawal_complete_transaction").click(function(){

        var no = $(this).attr("no");

        showActionProgress("Updating...");

        $(this).prop("disabled",true);

        $.post('index.php/main/load_data/t_fund_transfer/update_cheque_withdrawal_request',{
            no: no
        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                showMessage("s","Update success");                
                window.location.hash = "#tabs-2";
                location.reload();
            }else{
                showMessage("e","Update failed");
            }

        },"json");

    });

    $(".tab_title_").click(function(){ 
        $(".tab_content_,.tab_content_cheque_").animate({"height":"0px"});
        $(this).parent().find(".tab_content_").animate({"height":"200px"});
    });

    $(".tab_title_cheque").click(function(){                
        $(".tab_content_,.tab_content_cheque_").animate({"height":"0px"});
        $(this).parent().find(".tab_content_cheque_").animate({"height": ((parseInt($("#chq_num_rows").val()) * 66) + 20 + 44)+"px"});
    });



    $(".tt_opt").click(function(){

        var v = $(this).attr("val");

        if (v == "cash"){

            $(".class_to_acc").val( $("#hid_ho_cash_acc").val() ).attr("readonly","readonly");
            $(".clx_acc_title").html("(Head Office Cash Account)");

        }else{
        
            $(".class_to_acc").val("").removeAttr("readonly").focus();            
            $(".clx_acc_title").html("(Head Office Bank Account)");
            
        }

    });

    $(".btnSendRequest").click(function(){

        var a = false;

        $(this).parent().parent().find(".tt_opt").each(function(){
            if ( $(this).is(":checked") ){
                a = true;
            }
        });

        if (!a){
            alert("Please select transfer type");
            return;
        }

        if ( $(this).parent().parent().find(".from_bc :selected").val() == "Select Branch" ){
            alert("Please select request from branch");
            return;
        }

        if ( $(this).parent().parent().find(".from_bc :selected").val() != "Select Branch" ){

            if ($("#from_acc :selected").val() == "" || $("#from_acc :selected").val() == "undefined"){
                alert("Please select deposit bank account");
                return;
            }

        }

        if ($(this).parent().parent().find(".amount").val() == ""){
            alert("Please enter transfer amount");
            return;
        }

        save( $(this).attr("form_id") );

    });

    $(".vc").click(function(event) {        
        
        var tfr_no = $(this).attr("tfr_no");
        var is_v_re_p = $(this).attr("is_v_re_p");
        var bc  = $(this).attr("bc"); 
        var tt  = $(this).attr("transfer_type");
        var fb  = $(this).attr("from_bc");
                    
        $.ajax({
            type:'POST',
            url: 'index.php/main/load_data/t_fund_transfer/save_and_print_ftr_voucher',
            data: {no : tfr_no,is_v_re_p : is_v_re_p , bc : bc , tt : tt , fb : fb },
            dataType: "json",
            success: function (D) {

                closeActionProgress();

                if (D.s == 1){                    

                    $("#v_no").val(D.v_no);
                    $("#is_v_re_p").val(is_v_re_p);                    
                    $("#print_fund_trans_voucher_pdf").submit();                                        
                    
                    window.location.hash = "#tabs-2";
                    location.reload();

                }else{                  
                    showMessage("e","errror");
                }
            }
        });

    });




    
    $("#from_branch_name").click(function(){
        $("#from_branch_name,#from_bc").val("");
    });

    $("#bnk_acc_name").click(function(){
        $("#bnk_acc_name,#bank_acc").val("");
    });

    
    $("input[type=radio]").click(function(event) {
        if ($(this).val() =="branch"){
            $("input[type=radio][value='cash']").click();
            $("input[type=radio][value='cheque']").attr({ disabled: true });
            //$("#from_branch_name").focus();
            $("#from_bc").val(1);
            $("#from_branch_name").focus();
            
        }else{
            $("input[type=radio][value='cheque']").attr({ disabled: false });
        }

        if ($(this).val() =="cheque"){
            $("#cash_amount").val("");
            $("input[type=radio][value='headoffice']").attr({checkbox: true});
        }
    });

    $("#from_branch_name").focus(function(event) {
        $("input[type=radio][value='branch']").attr({
            'checked': true
        });
    });



    $(".x").click(function(event) {
        window.location.hash = $(this).attr("href");
    });

    $(".ho_bc_recevied_confirm").click(function(event) {
        update_acc('ho_bc_recevied_confirm',$(this).attr("no"),$(this).attr('rm'));
    });

    $(".bc_bc_recevied_confirm").click(function(event) {        
        update_acc('bc_bc_recevied_confirm',$(this).attr("no"));
    });


    $(".view_incomming_requ").click(function(event) {
        
        var cn = $(this).attr("div_class");

        if ($("."+cn).css("height") == "0px"){
            $("."+cn).animate({"height":"100px"});
        }else{
            $("."+cn).animate({"height":"0px"});
        }

    });

    
    $(".update_incomming_requ").click(function(event) {
        
        showActionProgress("Updating...");

        $.post('index.php/main/load_data/t_fund_transfer/update_incomming_request',{
            no: $(this).attr("no"),
            status : $(this).attr("status"),
            amount : $(this).attr("cash_amount"),
            receiving_bc : $(this).attr("receiving_bc")
        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                showMessage("s","Update success");                
                window.location.hash = "#tabs-3";
                location.reload();
            }else{
                showMessage("e","Update failed");
            }

        },"json");


    });

 
    $("#from_branch_name").autocomplete({
        source: "index.php/main/load_data/t_fund_transfer/i_bc_auto_com",
        select: function (event, ui) {
            b = ui.item.value;
            b = b.split(" - ");
            $("#from_bc").val(b[0]);            
            $("#bnk_acc_name").focus();
        }
    });

    $("#bnk_acc_name").autocomplete({
        source: "index.php/main/load_data/t_fund_transfer/bnk_acc_name",
        select: function (event, ui) {
            b = ui.item.value;
            b = b.split(" - ");
            $("#bank_acc").val(b[0]);            
            $("#cash_amount").focus();
        }
    });

    $("#no").keypress(function(event) {
        if (event.keyCode ==13){
            
        }
    });

    $("input[type=radio]").click(function(event) {
        if ($(this).val() =="cash"){
            $(".chk_chq_list").prop("checked",false);
            
            $(".chk_chq_list").parent().parent().css({"background-color":"#FFFFFF","color":"#000000"});

            $("#div_grid").addClass("cash_cheque_grid_disabled");
        }

        if ($(this).val() =="cheque"){
            $("#div_grid").removeClass("cash_cheque_grid_disabled");
        }
    });


        $("#btnGenerate").click(function(){

      /*  var fd = $('#from_date').val().replace('-','/');
        var td = $('#to_date').val().replace('-','/');*/

       /* if(fd > td){            
            alert("Invalid date selection");
            return;
        }*/
        
        if (!$('input[type=radio]').is(":checked")){                    
            alert("Please select an option to view report");
            return;
        }
        
        $("#print_report_pdf").submit();

    });
    

});

function forward_request(no,from_bc,from_acc,bc){

    showActionProgress("Please wait...");

    $(".ftr_forward_request").prop("disabled",true);

    $.post('index.php/main/load_data/t_fund_transfer/forward_request',{        
        no : no,
        bc : bc,
        from_bc : from_bc,
        from_acc : from_acc
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            alert("Request forwarded");                
            window.location.hash = "#tabs-3";
            location.reload();
        }else if(D.s == 2){

            alert("Unable to proceed. This request was canceled or removed.");
            window.location.hash = "#tabs-3";
            location.reload();

        }else{
            showMessage("e","Request forwarding failed");
        }
        

    },"json");

}

function accept_request(no,bc){

    showActionProgress("Please wait...");

    $(".ftr_accept_request").prop("disabled",true);

    $.post('index.php/main/load_data/t_fund_transfer/accept_request',{        
        no : no,
        bc : bc
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            alert("Request accepted");                
            window.location.hash = "#tabs-3";
            location.reload();
        }else if(D.s == 2){
            alert("Unable to proceed. This request was canceled or removed.");
            window.location.hash = "#tabs-3";
            location.reload();
        }else{
            showMessage("e","Request accept failed");
        }

    },"json");
}

function reject_request(no,bc){

    showActionProgress("Please wait...");

    $(".ftr_reject_request").prop("disabled",true);

    $.post('index.php/main/load_data/t_fund_transfer/reject_request',{        
        no : no,
        bc : bc
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            alert("Request rejected");                
            window.location.hash = "#tabs-3";
            location.reload();
        }else{
            showMessage("e","Request reject failed");
        }

    },"json");
}

function load_data(no){

    showActionProgress("Please wait...");

    if (no != ""){
        
        $.post("index.php/main/get_data/t_cheque_issue",{
            no : no
        },function(D){

            closeActionProgress();

            if (D.s == 1 ){

                $('#hid').val(D.sum.no)
                $('#btnSave').val("Update");
                $("#no").val(D.sum.no);
                $("#date").val(D.sum.date);
                $("#ref_no").val(D.sum.ref_no);                
                $("#bank_acc").val(D.sum.bank_acc);
                $("#bank_name").val(D.sum.bank_acc + "-" + D.sum.bank_name);               
                $("#issue_bc").val(D.sum.issue_bc);
                $("#i_bc_name").val(D.sum.issue_bc + "-" + D.sum.i_bc_name);                
                $("#memo").val(D.sum.memo);
                $("#employee").val(D.sum.employee);

                for(n = 0 ; n < D.det.length ; n++){
                    cheque_no = D.det[n].chq_no;
                    c_amount  = D.det[n].amount;
                    var T = '<div><div class="text_box_holder_commen" style="width:40%;float: left;border-top: none;background-color:#f1f1f1"> <input type="text"  name="cheque_no[]" value="'+cheque_no+'" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:40%;float: left;border-left: none;border-top: none;background-color:#f1f1f1"> <input type="text" name="c_amount[]" value="'+c_amount+'" class="amount" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:20%;float: left;border-left: none;height: 41px;;border-top: none;"> <a href="#" class="remove_cheque_row">Remove</a> </div></div>';
                    $(".cheque_grid").prepend(T);
                }

            }else{
                showMessage("e","Data not found");        
            }

        },"json");

    }else{
        
        showMessage("e","Select number");
    }

}

function validate_form(){

    showActionProgress("");    

    if (!$('input[type=radio][name="request_from"]').is(':checked')){        
        showMessage("e","Please select request destination");
        return false;
    }

    if ($("input[type=radio][value='branch']").is(":checked")){
        
        if ($("#from_bc").val() == ""){
            showMessage("e","Please select branch");
            $("#from_branch_name").val("").focus();
            return false;
        }

        if ($("#cash_amount").val() == "" || $("#cash_amount").val() < 1){
            showMessage("e","Invalid cash amount");
            $("#cash_amount").val("").focus();
            return false;   
        }

    }


    if ($("input[type=radio][value='headoffice']").is(":checked")){

        if ($("input[type=radio][value='cash']").is(":checked")){
            if ($("#cash_amount").val() == "" || $("#cash_amount").val() < 1){
                showMessage("e","Invalid cash amount");
                $("#cash_amount").val("").focus();
                return false;   
            }
        }
        
        if ($("input[type=radio][value='cheque']").is(":checked")){           
                
            var x = 0;

            $(".chk_chq_list").each(function(index, el) {                    
                
                if ( $(this).is(':checked') ){
                    x = 1;
                }

            });

            if (x == 0){
                showMessage("e","Please check one or more cheques from the list");            
                return false;
            }
            
        }

    }



    return true;
}

function add_to_grid(){
    var cheque_no = $("#cheque_no").val();
    var c_amount  = $("#c_amount").val();

    if (cheque_no == ""){ $("#cheque_no").focus(); return; }
    if (c_amount == ""){ $("#c_amount").focus(); return; }

    var T = '<div><div class="text_box_holder_commen" style="width:40%;float: left;border-top: none;background-color:#f1f1f1"> <input type="text"  name="cheque_no[]" value="'+cheque_no+'" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:40%;float: left;border-left: none;border-top: none;background-color:#f1f1f1"> <input type="text" name="c_amount[]" value="'+c_amount+'" class="amount" style="width:305px"> </div> <div class="text_box_holder_commen" style="width:20%;float: left;border-left: none;height: 41px;;border-top: none;"> <a href="#" class="remove_cheque_row">Remove</a> </div></div>';
    
    $(".cheque_grid").prepend(T);

    $("#cheque_no").val("").focus();
    $("#c_amount").val("");    

}

function remove_rows(O){
    for (n = 0 ; n < O.length ; n++){
        $("input:checkbox[value="+O[n]+"]").parent().parent().remove();
    }
}

function resetFormData(){

    var d = $('#date').val();
    
    $('input:text').val("");
    
    $('#date').val(d);
    $('#btnAddtoGrid').val('Add');
    $('#btnRequest').val('Send');
    $(".cheque_grid").html('');
    $("input[type=radio]").removeAttr("checked");
    $("#div_grid").addClass("cash_cheque_grid_disabled");
 
}

function update_acc(a,no,receiving_method = ''){

    showActionProgress("Please wait...");

    $.post('index.php/main/load_data/t_fund_transfer/'+a,{
        no: no,
        receiving_method : receiving_method
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            showMessage("s","Update success");            
            window.location.hash = "#tabs-2";
            location.reload();
        }else{
            showMessage("e","Update failed");
        }

    },"json");
}

function save(form_id){

    showActionProgress("Sending request...");

    $(".btnSendRequest").prop("disabled",true);

    var frm = $('#'+form_id); 

    $.ajax({
        type: frm.attr('method'), 
        url: frm.attr('action'), 
        data: frm.serialize(), 
        dataType: "json", 
        success: function (D) {            
            
            closeActionProgress();             

            if (D.s == "1" ){
                showMessage("s","Request sent");            
                window.location.hash = "#tabs-1";
                location.reload();
            }else{
                showMessage("e","Request failed");
            }

        } 
    });
}


function send_cheque_withdraw_request(OBJ){

    showActionProgress("Sending request...");

    OBJ.prop("disabled",true);

    $.post('index.php/main/load_data/t_fund_transfer/send_cheque_withdraw_request',{
        
        bank_code : OBJ.parent().parent().find(".bank_code").val(),
        chq_no :  OBJ.parent().parent().find(".chq_no").val(),
        amount : OBJ.parent().parent().find(".amount").val(),
        tt_opt : $("#tt_opt").val(),
        comment : OBJ.parent().parent().find(".chqu_wrd_comment").val(),
        from_bc : $("#from_bc").val()
    }, function(D) {
        
        closeActionProgress();
        
        if (D.s == "1" ){
            alert("Request sent");                
            window.location.hash = "#tabs-1";
            location.reload();
        }else{
            showMessage("e","Sending failed");
        }

    },"json");

}



function cancel_tfr_request(OBJ){

    if (confirm("Confirm cancel fund request")){

        showActionProgress("Please wait...");

        OBJ.prop("disabled",true);

        var tab_no = 2;

        if ( $(".cancel_tfr_reque").attr("tab_no") != undefined  ){
            tab_no = $(".cancel_tfr_reque").attr("tab_no");
        }

        $.post('index.php/main/load_data/t_fund_transfer/cancel_tfr_request',{
            
            no : OBJ.attr('no')

        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                alert("Request canceled");                
                window.location.hash = "#tabs-"+tab_no;
                location.reload();
            }else{
                showMessage("e","Cancel failed");
            }

        },"json");

    }

}


function cancel_tfr_request_AAA(OBJ){

    if (confirm("Confirm cancel fund request")){

        showActionProgress("Please wait...");

        OBJ.prop("disabled",true);

        var tab_no = 2;

        if ( $(".cancel_tfr_reque_AAA").attr("tab_no") != undefined  ){
            tab_no = $(".cancel_tfr_reque_AAA").attr("tab_no");
        }

        $.post('index.php/main/load_data/t_fund_transfer/cancel_tfr_request_AAA',{
            
            no : OBJ.attr('no')

        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                alert("Request canceled");                
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else if(D.s == "11"){

                alert("Unable to cancel, this request already accepted by branch ");
                window.location.hash = "#tabs-2";
                location.reload();

            }else{
                showMessage("e","Cancel failed");
            }

        },"json");

    }

}

function cancel_tfr_request_BBB(OBJ){

    if (confirm("Confirm cancel fund request")){

        showActionProgress("Please wait...");

        OBJ.prop("disabled",true);

        var tab_no = 2;

        if ( $(".cancel_tfr_reque_BBB").attr("tab_no") != undefined  ){
            tab_no = $(".cancel_tfr_reque_BBB").attr("tab_no");
        }

        $.post('index.php/main/load_data/t_fund_transfer/cancel_tfr_request_BBB',{
            
            no : OBJ.attr('no')

        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                alert("Request canceled");                
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else if(D.s == "12"){

                alert("Unable to cancel, voucher already printed by requested branch ");
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else{
                showMessage("e","Cancel failed");
            }

        },"json");

    }

}


function cancel_tfr_request_CCC(OBJ){

    if (confirm("Confirm cancel fund request")){

        showActionProgress("Please wait...");

        OBJ.prop("disabled",true);

        var tab_no = 2;

        if ( $(".cancel_tfr_reque_CCC").attr("tab_no") != undefined  ){
            tab_no = $(".cancel_tfr_reque_CCC").attr("tab_no");
        }

        $.post('index.php/main/load_data/t_fund_transfer/cancel_tfr_request_CCC',{
            
            no : OBJ.attr('no')

        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                alert("Request canceled");                
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else if(D.s == "13"){

                alert("Unable to cancel, transaction already completed ");
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else{
                showMessage("e","Cancel failed");
            }

        },"json");

    }

}


function cancel_tfr_request_DDD(OBJ){

    if (confirm("Confirm cancel fund request")){

        showActionProgress("Please wait...");

        OBJ.prop("disabled",true);

        var tab_no = 2;

        if ( $(".cancel_tfr_reque_DDD").attr("tab_no") != undefined  ){
            tab_no = $(".cancel_tfr_reque_DDD").attr("tab_no");
        }

        $.post('index.php/main/load_data/t_fund_transfer/cancel_tfr_request_DDD',{
            
            no : OBJ.attr('no')

        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                alert("Request canceled");                
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else if(D.s == "15"){

                alert("Unable to cancel, already accepted ");
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else if(D.s == "16"){

                alert("Unable to cancel, vocuher for this transaction is already printed");
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else if(D.s == "17"){

                alert("Unable to cancel, transaction already completed");
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else{
                showMessage("e","Cancel failed");
            }

        },"json");

    }

}


function cancel_tfr_request_EEE(OBJ){

    if (confirm("Confirm cancel fund request")){

        showActionProgress("Please wait...");

        OBJ.prop("disabled",true);

        var tab_no = 2;

        if ( $(".cancel_tfr_reque_EEE").attr("tab_no") != undefined  ){
            tab_no = $(".cancel_tfr_reque_EEE").attr("tab_no");
        }

        $.post('index.php/main/load_data/t_fund_transfer/cancel_tfr_request_EEE',{
            
            no : OBJ.attr('no')

        }, function(D) {
            
            closeActionProgress();
            
            if (D.s == "1" ){
                alert("Request canceled");                
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else if(D.s == "15"){

                alert("Unable to cancel, already accepted ");
                window.location.hash = "#tabs-"+tab_no;
                location.reload();

            }else{
                showMessage("e","Cancel failed");
            }

        },"json");

    }

}


