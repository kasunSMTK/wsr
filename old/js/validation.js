var count = 600;

$(document).ready(function(){

   $('.tooltip').tooltipster({
		
		animation: 'fade',
		delay: 100,
		theme: 'tooltipster-punk',
		trigger: 'hover'
   	
   });


	// Left slider	-- to be complete
	// $(".left_hover_slider").on("mouseover", function () {$('.left_hover_slider_content').css("visibility","visible").stop().animate({width:115 },{complete: function(){$(".left_icons").css("visibility","visible"); } }); /*$(".left_hover_slider_content").stop().css("visibility","visible").animate({width:115, complete: function(){alert('end ani'); } }, "fast");*/ }); $(".left_hover_slider").on("mouseout", function () {$(".left_icons").css("visibility","hidden"); $(".left_hover_slider_content").stop().animate({width:0}, "fast"); }); $(".left_hover_slider_content").on("mouseover", function () {$(this).stop(); }); $(".left_hover_slider_content").on("mouseout", function () {$(this).stop().animate({width:0}, "fast"); });
	
	// NIC validate
	$(".nic_feild").attr("maxlength","12");

	// Contact number validate
	$(".contct_no_feild").attr("maxlength","10");

	$(".slider_tab,.ocler").click(function(){

		if (!$(this).parent().parent().find(".div_new_pawn_info_holder").html() == ""){

			if ($(this).parent().parent().find(".div_new_pawn_info_holder").css("height") == "398px"){
				
				$(this).parent().parent().find(".div_new_pawn_info_holder").css("overflow","hidden").animate({"height":"0px"});
				$(this).parent().parent().find(".div_new_pawn_info_holder").css("height","1px");

			}else{
				$(this).parent().parent().find(".div_new_pawn_info_holder").animate({"height":"398px"});
			}

		}

	});


});


function validateNIC(nic,gm){


	if(nic.length <= 10){	
		var filter = /^[0-9]{9}[v|V|x|X]$/;
		
		if(filter.test(nic) && validateNICGender(nic,gm)){		
			return true;
		}else{				
			showMessage("e","Invalid NIC number A");		
			return false;
		}

	}else{

		var filter = /^[0-9]{12}$/;
		
		if(filter.test(nic)){		
			return true;
		}else{				
			showMessage("e","Invalid NIC number B");		
			return false;
		}

	}



}

function validateNICGender(nic,gm){
	var nic = parseInt(nic.substring(2,3));	
	
	if (gm != ""){
		if (gm.toLowerCase() == "m"){
			if (nic <  5){
				return true;
			}else{
				return false;
			}
		}

		if (gm.toLowerCase() == "f"){
			if (nic >=  5){
				return true;
			}else{
				return false;
			}
		}
	}else{
		return true;
	}

}

function validatePassport(txt){
	
	var filter = /^[n|N][0-9]{7}$/;
	
	if(filter.test(txt)){		
		return true;
	}else{				
		showMessage("e","Invalid passport number");		
		return false;
	}
	
}

function validateDrvingLicense(txt){
	
	/*var filter = /^[a-z|A-Z][0-9]{7}$/;
	
	if(filter.test(txt)){		
		return true;
	}else{				
		showMessage("e","Invalid drving license number");		
		return false;
	}*/

	return true;
} 

function validateGSCertifi(txt){
	
	/*var filter = /^[a-z|A-Z][0-9]{7}$/;
	
	if(filter.test(txt)){		
		return true;
	}else{				
		showMessage("e","Invalid drving license number");		
		return false;
	}*/

	return true;
} 


function validateNumber(no){

	if (isNaN(no)){
		return false;
	}else{
		return true;
	}

}

function validatePhoneno(phoneno){	
	var filter = /^[0-9]{10}$/;
	
	if(filter.test(phoneno)){		
		return true;
	}else{		
		showMessage("e","Invalid phone number");
		return false;
	}
}

function validateMobileno(phoneno){	
	var filter = /^[0-9]{10}$/;
	
	if(filter.test(phoneno)){		
		return true;
	}else{		
		showMessage("e","Invalid mobile number");
		return false;
	}
}

function validateFAXno(FAXno){	
	var filter = /^[0-9]{10}$/;
	
	if(filter.test(FAXno)){		
		return true;
	}else{		
		showMessage("e","Invalid FAX number");
		return false;
	}
} 


function validateEmail(email) {
  	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(regex.test(email)){		
		return true;
	}else{		
		showMessage("e","Invalid Email address");
		return false;
	}
}

function validateAmount(str,txt){
   	var num = str;
	var ifMatch = (num.match(/^[0-9]+\.[0-9]{2}$|[0-9]+\.[0-9]{2}[^0-9]/)) ? true : false;
	if (ifMatch){
		return true;
	}else{
		showMessage("e","Invalid "+txt+" ");
		return false;
	}
}

function validateAmount_no_deci(str,txt){
   	var num = str;
	var ifMatch = (num.match(/(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/)) ? true : false;
	if (ifMatch){		
		return true;
	}else{		
		showMessage("e","Invalid "+txt+" ");
		return false;
	}
}

function validateFloat(val,txt){	
	if (!val.match(/^[0-9]+\.[0-9]{2}$|[0-9]+\.[0-9]{2}[^0-9]/)) {
		showMessage("e","Invalid "+txt+" ");
	    return false;
	} else {
	    return true;
	}
}

function validateDate(date,txt) {
    var text = date;
	var comp = text.split('-');
	var m = parseInt(comp[1], 10);
	var d = parseInt(comp[2], 10);
	var y = parseInt(comp[0], 10);
	var date = new Date(y,m-1,d);
	
	if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
	  return true;
	} else {
	  	showMessage("e","Invalid "+txt+" ");
	    return false;
	}
}

function sucess_msg(){
	alert("Save success");
	location.href = "";
}

function delete_msg(){
	alert("Delete success");
	location.href = "";	
}

var DAP_TEXT = "";
function showApprovalWatingMsgBox(msg){

	DAP_TEXT = msg;

	if (msg == ""){
		$(".msg_pop_up_bg").fadeOut(0).fadeIn(300);
		$(".msg_pop_up_sucess").css("background-color","#ffffff").css("border-left","3px solid Green").fadeIn(300).html('<img src="img/loading.gif"> &nbsp; &nbsp; Request sent. Please wait for response...<br><br><div style="padding-left:35px">Your reference number <br></div><div class="request_id">REFERENCE NUMNER</div><div class="dd">10:00</div><br><br><br><br> <a href="#" class="open_new_pawn_window"><div style="padding-left:35px">Open New Pawning Window</div></a>');

	}else{
		$(".msg_pop_up_bg").fadeOut(0).fadeIn(300);
		$(".msg_pop_up_sucess").css("background-color","#ffffff").css("border-left","3px solid Red").fadeIn(300).html(msg);
	}
}

function hideApprovalWatingMsgBox(){
	$(".msg_pop_up_sucess").css("color","#000000").fadeOut(300).html(DAP_TEXT);
	$(".msg_pop_up_bg").fadeOut(300);
	DAP_TEXT = "";
}

function showMessage(s,msg){	

	var ch = $(window).height();
	var sh = $(document).height();

	$(".msg_pop_up_sucess").css("top",sh - (( ch/2 )+25)   );

	if (s == "s"){		
		$(".msg_pop_up_bg").fadeOut(0).fadeIn(300);
		$(".msg_pop_up_sucess").css("background-color","#ffffff").css("border-left","3px solid Green").fadeIn(300).html(msg);
		setTimeout("closeMsg('"+msg+"')",2000);		
	}

	if (s == "e"){		
		$(".msg_pop_up_bg").css("visibility","visible");
		$(".msg_pop_up_sucess").css("background-color","#ffffff").css("border-left","3px solid Red").css("color","red").css("visibility","visible").html(msg);
		setTimeout("closeMsg('"+msg+"')",2000);		
	}

}

function showpartPayMessage(msg_html){	
	$(".msg_pop_up_bg_PP").fadeOut(0).fadeIn(300);
	$(".msg_pop_up_sucess_PP").css("background-color","#ffffff").css("border-left","3px solid Green").fadeIn(300).html(msg_html);		
}

function closePartPayMessage(msg){
	pp_data = "";
	paying_amount_PP = 0;
	balance_amount_PP = 0;
	
	$("#nla,#cgv").val("");
	$(".msg_pop_up_sucess_PP").css("color","#000000").fadeOut(300);
	$(".msg_pop_up_bg_PP").fadeOut(300);	
}



function closeMsg(msg){
	$(".msg_pop_up_sucess").css("color","#000000").fadeOut(300).html(msg);
	$(".msg_pop_up_bg").fadeOut(300);	
}

var AP_text;

function showActionProgress(text){

	AP_text = text;
	//$("body").css("overflow", "hidden");
	$(".msg_pop_up_bg").css("color","#000000").fadeOut(0).fadeIn(300);
	$(".msg_pop_up_sucess").css("background-color","#ffffff").css("border-left","3px solid Green").fadeOut(0).fadeIn(300).html(text);
	
}

function closeActionProgress(){
	$(".msg_pop_up_sucess").fadeOut(200).html(AP_text);
	$(".msg_pop_up_bg").fadeOut(200);		
	$(".msg_pop_up_sucess").html(AP_text);
	$("body").css("overflow", "auto");
}

function loding(){
	
}

function set_msg(msg){
	$(".msg_pop_up_bg").fadeOut(0).fadeIn(300);
	$(".msg_pop_up_sucess").css("background-color","#ffffff").fadeIn(300).html(msg);
	 setTimeout("closeMsg('"+msg+"')",2000);
	//setTimeout("closeMsg(msg)",2000);		
}

// function input_active(){

// }

function rmsps(v){
	return v.replace(/ /g,'');
}

function input_active(){
	
}

function openTab(){
	slider_tab
}

function change_current_date(new_date){
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/utility/change_current_date",{
		new_date : new_date
	},function(D){
		closeActionProgress();
		date_change_response_call();



		if (D.show_cuation_msg == 1){
			
			if ( $(".bc_back_date_opt").html() == "undefined" ){
				$('.company_name').after("<div class='bc_back_date_opt bkdt_opt_red'>CAUTION : You changed the current date to a different date</div>");
				$(".bc_back_date_opt").css({
					"overflow":"hidden",
					"height" : "0px"
				});
				
				$(".bc_back_date_opt").animate({"height":"40px"});
			}else{
				$(".bc_back_date_opt").remove();
				$('.company_name').after("<div class='bc_back_date_opt bkdt_opt_red'>CAUTION : You changed the current date to a different date</div>");
				
				$(".bc_back_date_opt").css({
					"overflow":"hidden",
					"height" : "0px"
				});
				
				$(".bc_back_date_opt").animate({"height":"40px"});

			}

		}else{			
			$(".bc_back_date_opt").remove();
		}

	},"json");
}

function timer() {
    count = count - 1;
    if (count == -1) {
    	count = 600;
        clearInterval(counter);
        clearInterval(disable_monitor);
        hideApprovalWatingMsgBox();			
		showApprovalWatingMsgBox("Request timeout");
		setTimeout("holdAndCloseMSG()",3000);
        return;
    }

    var seconds = count % 60;
    var minutes = Math.floor(count / 60);
    var hours = Math.floor(minutes / 60);
    minutes %= 60;
    hours %= 60;

    //document.getElementById("dd").innerHTML = hours + "hours " + minutes + "minutes and" + seconds + " seconds left on this Sale!"; // watch for spelling
	$(".dd").html(minutes + ":" + seconds);

}

function enableApprovalMonitor(loanno,request_type='Other'){
	$.ajax({
		method: "POST",
		url: "index.php/main/load_data/approvals/get_approval_response",		
		data: { loanno : loanno,request_type : request_type},
		dataType: "json"
	}).done(function( D ) {

		$(".request_id").html(D.request_id)
		
		if (D.approval_status == 0){
			disable_monitor = setTimeout("enableApprovalMonitor('"+loanno+"','"+request_type+"')",10000);
		}

		if (D.approval_status == "A"){			
			
			hideApprovalWatingMsgBox();			

			if (D.page == "fund_transfers"){
				showApprovalWatingMsgBox("Fund transfer approved");
				// if report or print need do it here
				setTimeout('location.href=""',3000);
				return;
			}
			
			if ( $("#approval_id").val() != undefined ){
				$("#approval_id").val(D.approval_id);
			}

			if (D.request_type == "CN"){
				$("#is_cancel_approved").val(1);
				$("#btnCancel").attr("disabled",false).attr("class","btn_regular").click();				
			}else{
				$("#is_discount_approved").val(1);
				$("#discount").val(D.approved_amount);
				$("#extra_approved_amount").val(D.approved_amount);					
				$("#btnSave").attr("disabled",false).attr("class","btn_regular").click();
			}			

			
		}

		if (D.approval_status == "R"){			
			
			hideApprovalWatingMsgBox();			

			if (D.page == "fund_transfers"){
				showApprovalWatingMsgBox("Request rejected");
				// if report or print need do it here
				setTimeout('location.href=""',3000);
				return;
			}else{
				showApprovalWatingMsgBox("Request rejected");
				setTimeout("holdAndCloseMSG()",3000);
				clearTimeout(counter);
				count = 600;
			}

		}

	});	

}

function holdAndCloseMSG(){
	$("#btnReset").attr("disabled",false);
	$("#btnSave").attr("disabled",false).attr("class","btn_regular");
	$("#btnReset").attr("disabled",true);
	hideApprovalWatingMsgBox();
}


function getURL_pagename_var() {
      var half = window.location.href;      
      half = half.split("#");      
      if (half[1] == undefined){
      	half = "all_requests";
      	return half;
      }else{
      	half = half[1];
      	half = half.split("l=");	
      	return half[1];
      }      
  }

