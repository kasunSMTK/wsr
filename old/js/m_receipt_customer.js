$(document).ready(function(){

	load_list();
	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_areaname").val());
	});




});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Vendor saving success");
					clear_form();
					load_list();					
/*				}else if (D == 2){					
					showMessage("e","Error, Duplicate Code");*/
				}else{					
					showMessage("e",D.m);
				}
			}

		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_receipt_customer",{
			code : code
		},function(D){

			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_receipt_customer",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#bc").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	if ($("#name").val() == ""){showMessage("e","Enter vendor name");	return false; }
	//if ($("#nic").val() == ""){showMessage("e","Enter vendor NIC");	return false; }
	if ($("#description").val() == ""){showMessage("e","Enter vendor description");	return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$("#is_cash_customer").prop("checked",false);
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_receipt_customer/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.R.code);
		$("#btnSave").val("Edit");
		$("#code").val(D.R.code);		
		$("#name").val(D.R.name);		
		$("#nic").val(D.R.nic);		
		$("#description").val(D.R.description);		

		if (D.R.is_cash_customer == 1){ 
			$("#is_cash_customer").prop("checked",true);
		}else{
			$("#is_cash_customer").prop("checked",false);
		}

		var t = '';


		if ( D.bc_list.length > 0 ){
			
			t += 'Branches Applied for Zone <br><br>';

			t += '<ul class="a">';

			for( n = 0 ; n < D.bc_list.length ; n++ ){
				t += '<li>' + D.bc_list[n].bc + ' - ' + D.bc_list[n].name + "</li>";
			}

			t += '</ul>';

		}else{
			t += 'No bc added foir this vendor';
		}
 

		$(".zone_bc_list").html(t);


		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_receipt_customer/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}