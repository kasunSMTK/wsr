


$(document).on("click",".bc_bkdate_edit",function(){

	var no = $(this).attr("no");

	$("#no").val(no);
	setEdit();

});


$(document).ready(function(){	

	view_list();	

	$("#btnSave").click(function(){
		save();
	});

	$("#no").keypress(function(e){

		if (e.keyCode == 13){
			setEdit();
		}

	});

	$("#btnEnd").click(function(){

		if (confirm("Do you want end this back date option?")){
			showActionProgress("Please wait...");
			$.post("index.php/main/load_data/m_branch_receipt_update_control/end",{
				no : $("#no").val()
			},function(D){

				if (D.s == 1){					
					closeActionProgress();
					alert("Edit option disabled");
					location.href = "";
				}else{
					alert("Error");
				}

			},"json");
		}

	});




});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {
				// closeActionProgress();

				if ($("#hid").val() == 0){
					alert("Save success");
				}else{
					alert("Update success");
				}

				location.href = '';

			}

		});

	}else{
		closeActionProgress();
	}
}

function setDelete(code){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_branch_receipt_update_control",{
			code : code
		},function(D){

			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}


function setEdit(){
	
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/m_branch_receipt_update_control/setEdit",{
		no : $("#no").val()
	},function(D){

		if (D.s == 1){


			if (D.sum.status == 'active'){
				$("#btnSave").val("Update");
				$("#btnEnd,#btnSave").prop("disabled",false).removeClass("btn_regular_disable").addClass("btn_regular");
			}else{
				$("#btnEnd,#btnSave").prop("disabled",true).addClass("btn_regular_disable");
			}


			var T = '';

			$("#hid").val(D.sum.no);
			$("#date").val(D.sum.date);		

			for ( n = 0 ; n < D.det.length ; n++){				
				T += '<span title="'+D.det[n]+',">'+D.det[n]+',</span>';
				T += '<input type="hidden" title="'+D.det[n]+'," value="'+D.det[n]+'," name="bc_arry[]">';				
			}

			$(".multiSel").html(T);


			$(".bc_each").prop("checked",false);
			$(".bc_each").each(function(){
				for ( n = 0 ; n < D.det.length ; n++){				
					if ($(this).val() == D.det[n]){
						$(this).prop("checked",true);
					}
				}
			});



			$("#backdate_upto").val(D.sum.backdate_upto);
			$("#valid_upto").val(D.sum.valid_upto);
			$("#desc").val(D.sum.desc);

			closeActionProgress();
			
		}else{
			showMessage("e",D.m);
		}

	},"json");
	
}



function load_list(){
	
	showActionProgress("Please wait...");
	
	$.post("index.php/main/load_data/m_branch_receipt_update_control/load_list",{
		no : $("#no").val()
	},function(D){

		if (D.s == 1){

			var T = '';

			$("#hid").val(D.sum.no);
			$("#date").val(D.sum.date);		

			for ( n = 0 ; n < D.det.length ; n++){				
				T += '<span title="'+D.det[n]+',">'+D.det[n]+',</span>';
				T += '<input type="hidden" title="'+D.det[n]+'," value="'+D.det[n]+'," name="bc_arry[]">';				
			}

			$(".multiSel").html(T);

			$(".bc_each").prop("checked",false);
			$(".bc_each").each(function(){
				for ( n = 0 ; n < D.det.length ; n++){				
					if ($(this).val() == D.det[n]){
						$(this).prop("checked",true);
					}
				}
			});

			$("#backdate_upto").val(D.sum.backdate_upto);
			$("#valid_upto").val(D.sum.valid_upto);

			closeActionProgress();
			
		}else{
			showMessage("e",D.m);
		}

	},"json");
	
}


function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
}


function validate_form(){


	if ($(".multiSel").html() == ""){
		alert("Please select branch");
		return false;
	}


	if ($("#backdate_upto").val() == ""){
		alert("Please select back date upto date");
		return false;
	}

	return true;
}

function view_list(){

	showActionProgress("Please wait...");
	
	$.post("index.php/main/load_data/m_branch_receipt_update_control/view_list",{
		no : $("#no").val()
	},function(D){		

		closeActionProgress();


		if ( D.s == 1 ){

		var T  = '<table cellpadding="0" cellspacing="0" border="0" class="tbl_master_n" align="center" width="100%">';
			T += '<tr>';
			T += '<td width="80">No</td>';
			T += '<td width="80">Status</td>';
			T += '<td width="80">Date</td>';
			T += '<td>Branch</td>';
			T += '<td>Reason</td>';
			//T += '<td width="120">No of Days Back</td>';
			T += '<td width="80">Valid Upto</td>';
			T += '<td width="80">Action</td>';
			T += '</tr>';

			for (n = 0 ; n < D.det.length ; n++){
				
				T += '<tr>';
				T += '<td>'+D.det[n].no+'</td>';

				if(D.det[n].exprd == "1"){
					T += '<td style="background-color:red;color:#ffffff">Expired</td>';
				}else if (D.det[n].status == "active"){
					T += '<td style="background-color:green;color:#ffffff">Active</td>';
				}else{
					T += '<td style="background-color:#999999;color:#ffffff">Ended</td>';
				}

				T += '<td>'+D.det[n].date+'</td>';
				T += '<td>'+D.det[n].bc+'</td>';
				T += '<td>'+D.det[n].desc+'</td>';
				//T += '<td align="center">'+D.det[n].backdate_upto+'</td>';
				T += '<td>'+D.det[n].valid_upto+'</td>';
				T += '<td><a href="#" class="bc_bkdate_edit" no="'+D.det[n].no+'">Edit</a></td>';					
				T += '</tr>';

			}

			T += '</table>';

		}else{
			var T = "No data found";
		}		
		

		$(".backdate_list").html(T);

	},"json");

}