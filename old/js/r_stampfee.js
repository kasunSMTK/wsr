$(document).ready(function(){

	load_list();
	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_nicNo").val());
	});

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				closeActionProgress();

				if (D == 1){
					showMessage("s","Customer saving success");
					clear_form();
					load_list();
				}else{					
					showMessage("e","Error, save failed");
				}
			}
		});

	}

}

function setDelete(recid){

	if (confirm("Do you want delete this doc fee?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/r_stampfee",{
			recid : recid
		},function(D){		
			$(".list_div").html(D);
			closeActionProgress();
			load_list();
		},"text");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/r_stampfee",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function validate_form(){
	if ($("#bc").val() == ""){ showMessage("e","Select branch"); return false; }
	if (!validateAmount($("#fromvalue").val() ," from amount")){ return false; }
	if (!validateAmount($("#tovalue").val() ," to amount")){ return false; }
	if (!validateAmount($("#stampfee").val() ," stamp fee amount")){ return false; }
	if (!validateDate($("#stampdate").val() ," stamp date")){ return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#bc').val("");
	$("#hid").val(0);
}

function setEdit(recid){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/r_stampfee/set_edit",{
		recid : recid
	},function(D){		
		$("#hid").val(D.recid);
		$("#btnSave").val("Edit");
		
		$("#bc").val(D.bc);
		$("#fromvalue").val(D.fromvalue);
		$("#tovalue").val(D.tovalue);
		$("#stampfee").val(D.stampfee);
		$("#stampdate").val(D.stampdate);

		
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/r_stampfee/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}