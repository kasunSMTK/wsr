$(document).ready(function(){
            
});

function save(){    

    showActionProgress("Saving...");

    if (validate_form()){
        var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: "json",
            success: function (D) {
                if (D.s == 1){
                    clear_form();                    
                    showMessage("s","Update success");
                    load_list();                    
                }else{                  
                    showMessage("e",D.m);
                }
            }
        });

    }

}

function validate_form(){    
    return true;    
}

