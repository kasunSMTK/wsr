var page_name = getURL_pagename_var();
var Tm

$(document).ready(function(){

	getApprovalRequests(page_name);

	$(".approval_tab").click(function(event) {		
		$(".approval_tab_selected").css("border-bottom","1px solid #ccc");
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".approval_tab_selected").click(function(event) {				
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".input_text_app_search").keyup(function () {
		
		var rows = $(".discount_requests .request_row").hide();
		
		if (this.value.length) {
		    var data = this.value.split(" ");
		    $.each(data, function (i, v) {
		        rows.filter(":contains('" + v + "')").show();
		    });
		} else rows.show();
	});

	$(document).on( "click", ".input_approval", function() { $(this).select();});

	$('.app_requ_link').click(function(){
		
		clearTimeout(Tm);

		var x =  $(this).attr("href");
		x = x.split("#l=");		
		page_name = x[1];
		getApprovalRequests(x[1]);

	});

	$("#btnCancelForward_bc_request").click(function(event) {
		$(".bc_cash_balance_grid").html("");
		$("#f_bc").html('Loading branches...');
		$("#bc_cash_balance").html('0.00');
		
		$(".app_popup_bg,.app_popup").css("visibility","hidden");
	});

	
	$("#btnForward_bc_request").click(function(event) {
		forward_request_to_selected_bc();		
	});

});

function appr(loanno, approved_amount_id,row_id,requested_amount){
	var approved_amount = $("#" + approved_amount_id ).val();
	
	if (validateAmount_no_deci(approved_amount  , "") && parseFloat(approved_amount) <= parseFloat(requested_amount)  ){
		makeAprove(loanno,approved_amount,row_id,requested_amount);
	}else{
		alert("Invalid amount")
	}
}

function appr_vou(no,S,paid_acc){

	clearTimeout(Tm);
	
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/approvals/appr_vou",{
		no : no,		
		record_status : S,
		paid_acc : paid_acc
	},function(D){				

		window.location.href += "#l=general_voucher";
		location.reload();
		
		//window.location.hash = "";
		//window.location.href = window.location.href;
		
	},"json");
	
}

function makeAprove(loanno,approved_amount,row_id,requested_amount){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
		loanno : loanno,
		approved_amount : approved_amount,
		record_status : "A"
	},function(D){
		$("#"+row_id).remove();
		closeActionProgress();
	},"json");
}

function rejt(loanno, row_id){
	if(confirm("Do you want reject this request? ")){
		showActionProgress("Loading...");
		$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
			loanno : loanno,		
			record_status : "R"
		},function(D){
			$("#"+row_id).remove();
			closeActionProgress();
		},"json");
	}
}


function getApprovalRequests(page_name){

	$.ajax({
		method: "POST",
		url: "index.php/main/load_data/approvals/getApprovalRequests",				
		data : {page_name : page_name},
		dataType: "json"
	}).done(function( D ) {

		if (page_name == "fund_transfers" || page_name == "general_voucher"){			
			$(".request_row").hide();
			$(".fund_transfer_title_row").show();			
		}else{			
			$(".request_row").show();
			$(".fund_transfer_title_row").hide();			
		}

		//*************************************************************************/		

		if (page_name == "general_voucher"){			

			var T = "";

			if (D.discount != "0"){

				for (num = 0 ; num < D.discount.length ; num++){					
					row_id = "row_"+num;
					T += '<div style="display: block;border-bottom: 1px solid #eaeaea; padding: 10px; height: 30px;" class="fund_transfer_title_row"> <div style="width: 50px;" class="RR_title_content">'+(num+1)+'</div> <div style="width:120px;" class="RR_title_content">'+D.discount[num].bc+'</div> <div style="width:150px;" class="RR_title_content">'+D.discount[num].date+'</div> <div style="width:120px;" class="RR_title_content">HO</div> <div style="width:120px;" class="RR_title_content">&nbsp;</div> <div style="width:120px;" class="RR_title_content">'+D.discount[num].receiving_method+'</div> <div style="width:110px;text-align: right;margin-right:20px" class="RR_title_content">'+D.discount[num].amount+'</div> <div style="width:120px;" class="RR_title_content">'+D.discount[num].request_by+'</div> <div style="width:150px;" class="RR_title_content">'+D.discount[num].request_datetime+'</div> <div style="width:200px;" class="RR_title_content"><a target = "self" href="?action=t_voucher_general&no='+D.discount[num].nno+'&status=A&paid_acc='+D.discount[num].paid_acc+'&v_app_no='+D.discount[num].no+'&bc='+D.discount[num].bc+'" class="link_app">View</a> | <a class="link_app" onClick=appr_vou("'+D.discount[num].nno+'","A","'+D.discount[num].paid_acc+'")>Approve</a> | <a class="link_app" onClick=appr_vou("'+D.discount[num].no+'","R","")>Reject</a></div> </div>';



				}

			}else{ 

				$(".discount_requests").html("<div class='div_no_request'>No requests found</div>");

			}

			$(".discount_requests").html(T);
			
			Tm = setTimeout("getApprovalRequests('"+page_name+"')",10000);

			return;

		}			

		//*************************************************************************/

	
		var old_loans = Array();
		var old_loans_II = Array();
		var new_result = Array();
		var num1 = 0;
		
		$(".loan_no_list").each(function(){ old_loans[num1] = $(this).html(); num1++; });		

		if (D.discount != 0){

			$(".div_no_request").remove();

			for (num = 0 ; num < D.discount.length ; num++){ new_result[num] = D.discount[num].loanno; }
			var removeList = old_loans.filter(function(obj) { return new_result.indexOf(obj) == -1; });		
			
			$(".loan_no_list").each(function(){ 
				if(jQuery.inArray($(this).html(), removeList) !== -1){
					$(this).parent().html("").css({"height":"30px","border-bottom":"1px solid #ffffff"}).slideUp(500);
				}
			});

			var T = "";
			var clz = "";
			var tno = "";

			$(".loan_no_list").each(function(){ old_loans_II[num1] = $(this).html(); num1++; });
			
			for (num = 0 ; num < D.discount.length ; num++){
				if(jQuery.inArray(D.discount[num].loanno, old_loans_II) == -1){		
					txt_id = "txt_"+num;
					row_id = "row_"+num;

					if (D.discount[num].request_type == "EL"){clz = "request_type_bbl_red"; }
					if (D.discount[num].request_type == "DI"){clz = "request_type_bbl_green"; }
					if (D.discount[num].request_type == "CN"){clz = "request_type_bbl_gray"; }

					if (page_name == "fund_transfers"){				
						
						// if (D.discount[num].status == "F"){var ST = ""; var action_text = "<span style='background-color:red;color:#ffffff'>Approved</span>"; }else{var ST = "A"; var action_text = "Approve"; if (D.discount[num].request_from == "BRANCH"){ST = "F"; action_text = "Forward"; } }
						if (D.discount[num].no != tno){

							var ST = action_text = from_bc = fn_name = "";

							if(D.discount[num].status == 'F'){
								ST = "";
								action_text = "APPROVED";
								from_bc = D.discount[num].from_bc;
								fn_name = "appr_ft_popup_ant";
							
							}else if(D.discount[num].request_from == "BRANCH"){ 
								
								ST = "F";
								action_text = "Forward";
								
								from_bc = "HO choose Bc";
								fn_name = "appr_ft_popup";

							}else{
								
								ST = "A";
								action_text = "Approve";
								
								from_bc = D.discount[num].from_bc;
								fn_name = "appr_ft";								

							}



							if (D.discount[num].receiving_method == "CASH"){
								rm = D.discount[num].receiving_method;
							}else{
								rm = "<span style='font-size:9px'>CHQ_NO: "+D.discount[num].cheque_no +"<br>"+D.discount[num].bank+"</span>";
							}


							T += '<div id="'+row_id+'" class="request_row"><div class="RR_title_content"  style="width: 50px;">'+D.discount[num].no+'</div> <div class="RR_title_content"  style="width:120px;">'+D.discount[num].bc+'</div> <div class="RR_title_content"  style="width:150px;">'+D.discount[num].date+'</div> <div class="RR_title_content"  style="width:120px;">'+D.discount[num].request_from+'</div> <div class="RR_title_content"  style="width:120px;">'+from_bc+'&nbsp;</div> <div class="RR_title_content"  style="width:120px;">'+rm+'</div> <div class="RR_title_content"  style="width:110px;text-align: right;margin-right:20px">'+D.discount[num].amount+'</div> <div class="RR_title_content"  style="width:120px;">'+D.discount[num].request_by+'</div> <div class="RR_title_content"  style="width:150px;">'+D.discount[num].request_datetime+'</div> <div class="RR_title_content" style="width:100px"><a class="link_app" onClick='+fn_name+'("'+D.discount[num].no+'","'+row_id+'","'+ST+'","'+D.discount[num].amount+'","'+D.discount[num].tot_chq_amount+'")>'+action_text+'</a>';

								if (action_text != "APPROVED"){
									T += ' | <a class="link_app" onClick=appr_ft("'+D.discount[num].no+'","'+row_id+'","R","0","0")>Reject</a>';
								}

								T += '</div></div>';

							tno = 	D.discount[num].no;

						}else{							
							T += '<div id="'+row_id+'" class="request_row" style="background-color:#f9f9f9 "><div class="RR_title_content"  style="width: 50px;"><img src="img/down_right.png" width="12"></div> <div class="RR_title_content"  style="width:120px;">-</div> <div class="RR_title_content"  style="width:150px;">-</div> <div class="RR_title_content"  style="width:120px;">-</div> <div class="RR_title_content"  style="width:120px;">-</div> <div class="RR_title_content"  style="width:120px;font-size:9px;">CHQ_NO: '+D.discount[num].cheque_no+' <br>'+D.discount[num].bank+' </div> <div class="RR_title_content"  style="width:110px;text-align: right;margin-right:20px">'+D.discount[num].amount+'</div> <div class="RR_title_content"  style="width:120px;">-</div> <div class="RR_title_content"  style="width:150px;">-</div> <div class="RR_title_content" style="width:100px"> <input type="checkbox"> </div></div>';
						}




					}else{
						$(".discount_requests").html("");
						T += '<div id="'+row_id+'" class="request_row"><div class="RR_title_content loan_no_list '+clz+'" style="text-align:center">'+D.discount[num].loanno+'</div><div class="RR_title_content" style="width:100px;text-align:right">'+D.discount[num].int_balance+'</div><div class="RR_title_content" style="width:100px;text-align:right">'+D.discount[num].payable_amount+'</div><div class="RR_title_content" style="width:100px;text-align:right">'+D.discount[num].goldvalue+'</div><div class="RR_title_content" style="width:120px;padding-left:20px">'+D.discount[num].requested_by+'</div><div class="RR_title_content" style="width:120px;padding-left:20px">'+D.discount[num].bc+'</div><div class="RR_title_content" style="width:140px">'+D.discount[num].requested_datetime+'</div><div class="RR_title_content" style="width:120px;text-align:right;margin:-12px 5px 0px 4px ;padding:10px; border-left:2px solid Red;border-right:2px solid Red">'+D.discount[num].requested_amount+'</div><div class="RR_title_content" style="width:120px"><input type="text" class="input_approval amount" id="'+txt_id+'"></div><div class="RR_title_content" style="width:140px">'+D.user+'</div><div class="RR_title_content" style="width:140px"><a class="link_app" onClick=appr("'+D.discount[num].loanno+'","'+txt_id+'","'+row_id+'","'+D.discount[num].requested_amount+'")>Approve</a> | <a class="link_app" onClick=rejt("'+D.discount[num].loanno+'","'+row_id+'")>Reject</a></div></div>';
					}
				}
			}

			$(".discount_requests").append(T);
		}else{
			$(".discount_requests").html("<div class='div_no_request'>No requests found</div>");
		}

		Tm = setTimeout("getApprovalRequests('"+page_name+"')",10000);

	});

}

function appr_ft(no,row_id,S,approved_amount,chq_amount,r_bc = ""){
	
	var approved_amount = approved_amount;			
	showActionProgress("Please wait...");	
	
	$.post("index.php/main/load_data/approvals/makeAproveOrRejectFT",{
		
		no : no,
		approved_amount : approved_amount,
		chq_amount : chq_amount,
		status : S,
		r_bc : r_bc

	},function(D){
		
		//$("#"+row_id).remove();
		closeActionProgress();

	},"json");	

}

function appr_ft_popup(no,row_id,S,approved_amount,chq_amount,d){

	$(".requ_amt").val("");
	$(".bc_cash_balance_grid").html("<span style='font-size:20px;font-family:bitter;color:Green'>Calculating branch cash balances, Please wait...</span>");
	$(".app_popup_bg,.app_popup").css("visibility","visible");

	$.post("index.php/main/load_data/approvals/get_bc_list",{
			
	},function(D){	

		$(".bc_cash_balance_grid").html(D.bc_cash_balance_grid);
		$(".requ_amt").html(approved_amount);
		
		var d = "<select id='ho_selected_bc'>";	
		for (n_bc = 0 ; n_bc < D.bc_list.length ; n_bc++){d += "<option value='"+D.bc_list[n_bc].bc+"'>"+D.bc_list[n_bc].name+"</option>"; }	d += "</select>";
		
		$("#aa").val(no);
		$("#bb").val(row_id);
		$("#cc").val(S);
		$("#dd").val(approved_amount);
		//$("#dd").val(chq_amount);
		//$("#f_bc").html(d);

	},"json");

}

function forward_request_to_selected_bc(){

	var checked = false;
	var checked_bc = "";
	var bc_cash_balance = 0;

	$(".cash_bal_option").each(function(index, el) {
		if ($(this).is(":checked")){
			checked = true;
			checked_bc = $(this).val();
			bc_cash_balance = parseFloat($(this).parent().find('input[type="hidden"]').val());
		}
	});

	if (!checked){
		showActionProgress("");	
		showMessage("e","Please select a branch");
		return;
	}

	var requested_amount = parseFloat($(".requ_amt").html());

	if (requested_amount > bc_cash_balance){
		showActionProgress("");	
		showMessage("e","Unable to pay requested amount with this branch cash balance");
		return;	
	}

	no = $("#aa").val();
	row_id = $("#bb").val();
	S = $("#cc").val();
	approved_amount = $("#dd").val();
	chq_amount = $("#dd").val();
	
	d = checked_bc; // $("#ho_selected_bc :selected").val();

	appr_ft(no,row_id,S,approved_amount,chq_amount,d);
	$("#btnCancelForward_bc_request").click();
}

function appr_ft_popup_ant(){
	alert("Update action not taken by receiving branch");
}