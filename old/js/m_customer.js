$(document).ready(function(){

	load_list();

	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_nicNo").val());
	});

	$("#isblackListed").click(function(){

		if ($(this).is(":checked")){
			$(".blkreason").css("display","block");
		}else{
			$(".blkreason").css("display","none");
		}

	});
	

});

$(document).on('click', '.blacklist_chk', function(event) {	
	if ($("#isblackListed").is(':checked')){
		$(this).css({
			'background-color' : 'red',
			'color' : '#ffffff'			
		});

		
	}else{
		$(this).css({
			'background-color' : '#ffffff',
			'color' : '#000000'
		});
		
	}	
}); 

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Customer saving success");
					clear_form();
					load_list();					
				}else{					
					showMessage("e",D.m);
				}
			}
		});

	}

}

function setDelete(id){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_customer",{
			customer_id : id
		},function(D){		
			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_customer",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#customer_id").val(D.customer.customer_id);
		$("#customer_no").val(D.customer.customer_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	
	if (!$('input:radio[name=title]').is(":checked")){ showMessage("e","Select title"); return false;}

	gm = $('input:radio[name=title]:checked').val();
	if (gm == "Mr. "){ gm = "m"; }else{ gm = "f"; }
	
	if (!validateNIC($("#nicno").val(),gm)){ return false;	}
	
	
	if ($("#mobile").val() != ""){
		if (!validateMobileno(  $("#mobile").val())){ return false;}	
	}

	if ($("#telNo").val() != ""){ if (!validatePhoneno(  $("#telNo").val())){ return false;} }
	return true;	
}

function clear_form(){
	$("input:text").val("");
	$("input:hidden").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$("#isblackListed").prop('checked',false);
	$(".blacklist_chk").css({'background-color' : '#ffffff', 'color' : '#000000'}); 

	$(".blkreason").css("display","none");
}

function setEdit(customer_id){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_customer/set_edit",{
		customer_id : customer_id
	},function(D){		
		$("#hid").val(D.sum.customer_id);
		$("#btnSave").val("Edit");
		
		$("#customer_id").val(D.sum.customer_id);
		$("#customer_no").val(D.sum.customer_no);

		$("#nicno").val(D.sum.nicno);
		$('input[type=radio][value="'+D.sum.title+'"]').attr('checked','checked');
		$("#cusname").val(D.sum.cusname);
		$("#other_name").val(D.sum.other_name);
		$("#address").val(D.sum.address);
		$("#address2").val(D.sum.address2);
		$("#mobile").val(D.sum.mobile);		
		$("#telNo").val(D.sum.telNo);		
		$("#bc").val(D.sum.bc);


		if (D.sum.max_allowed_pawn == 0 || D.sum.max_allowed_pawn == ""){
			$("#max_allowed_pawn").val(D.default_pawn_limit);
		}else{
			$("#max_allowed_pawn").val(D.sum.max_allowed_pawn);
		}


		if (D.sum.isblackListed == 1){			
			$("#isblackListed").prop('checked',true);
			$(".blacklist_chk").css({'background-color' : 'red','color' : '#ffffff'});

			$(".blkreason").html(D.sum.black_list_reason).css("display","block");

		}else{
			$("#isblackListed").prop('checked',false);
			$(".blacklist_chk").css({'background-color' : '#ffffff', 'color' : '#000000'});
			$(".blkreason").html('').css("display","none");
		}

		closeActionProgress();

	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_customer/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}