$(document).ready(function(){

	load_list();
	$("#code").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_code").val());
	});

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {
				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Branch saving success");
					clear_form();
					load_list();					
				}else{					
					showMessage("e",D.m);
				}
			}
		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this doc fee?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/r_gold_quality",{
			code : code
		},function(D){		
			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/r_gold_quality",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function validate_form(){
	if ($("#code").val() == ""){ showMessage("e","Enter code"); return false; }
	if (!validateFloat($("#Rate").val() ," rate")){ return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/r_gold_quality/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.code);
		$("#btnSave").val("Edit");		
		$("#code").val(D.code);
		$("#Rate").val(D.rate);		
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/r_gold_quality/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}