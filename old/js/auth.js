
$(document).ready(function(){

    check_com_auth();

    $("#btnRegCom").click(function(){

        if ( $("#reg_code").val() != "" ){
            reg_com();
        }else{
            alert("Please enter regiter code");
        }

    });

});


function check_com_auth(){
    
    // no cookie found
    // get a next COM_ID from database and create a cookie
    // get session id and auth_code as well from php

    if (typeof $.cookie('COM_ID') === 'undefined'){
        var com_id = '';
    }else{
        var com_id = $.cookie('COM_ID');
    }

    $.post("index.php/main/auth_check/",{
        com_id : com_id
    },function(D){

        if ( D.auth_stat == 1 ){
            window.location = "index.php";
        }else{
            $.cookie('COM_ID',D.COM_ID , { expires: 365 } );
            $("#auth_your_code").val( D.AUTH_YOUR_CODE );
        }

    }, "json");

}



function reg_com(){

    $("#btnRegCom").prop("disabled",true);

    var reg_code = $("#reg_code").val();

    $.post("index.php/main/reg_com/",{
        com_id : $.cookie('COM_ID'),
        reg_code : reg_code
    },function(D){

        if ( D.s == 1 ){

            window.location = "index.php";

        }else{

            alert("Invalid register code");

        }

        $("#btnRegCom").prop("disabled",false);

    }, "json");

}