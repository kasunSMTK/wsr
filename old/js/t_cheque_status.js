
$(document).on("keyup",".chq_stat_cchu_no_search",function(){
	
	var q = $(this);

	$("."+$(this).attr("cl")).each(function(){

		_this = q;

		if($(this).html().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1){
           $(this).parent().hide();
		}else{
           $(this).parent().show();
		}

	});

});



$(document).on("click",".row_bc",function(){

	$(".all_rows").css("display","none");
	$("."+$(this).attr("cl")).css("display","table-row");


	$(".chq_stat_cchu_no_search").css("display","none").val('');

	$(this).find(".chq_stat_cchu_no_search").css("display",'block').val('');


	$('html, body').animate({
        scrollTop: $(this).offset().top
    }, 300);   


});



$(document).ready(function(){

	$("#print_chq_stat").click(function(){
		
		$("#tr_type").val("r_print_cheque");
		$("#fd_r").val($("#fd").val());
		$("#td_r").val($("#td").val());

		var tx = Array();
		var n = 0;
		
		$(".print_bc").each(function(){

			if ($(this).is(":checked")){
				tx[n] = $(this).val();
				n++;
			}

		});		

		$("#bc_r").val(tx);

		$("#print_excel").submit();

	});

});

function get_chq_details(){

	showActionProgress("Loading...");	

	$.post("index.php/main/load_data/t_cheque_status/chq_stat",{
		

		fd : $("#fd").val(),
		td : $("#td").val(),
		json : 1

	},function(D){

		$(".chq_stat").html(D.data);

		closeActionProgress();
	},"json");

}

function makeAprove(loanno,approved_amount,row_id,requested_amount){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
		loanno : loanno,
		approved_amount : approved_amount,
		record_status : "A"
	},function(D){
		$("#"+row_id).remove();
		closeActionProgress();
	},"json");
}