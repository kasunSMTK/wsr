$(document).ready(function(){

	load_list();
	$("#code").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_code").val());
	});

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				closeActionProgress();

				if (D.s == 1){
					showMessage("s","Customer saving success");
					clear_form();

					$(".tbl_gold_rate_changed_log_det").html(D.history_log);

					load_list();
				}else{					
					showMessage("e","Error, save failed");
				}
			}
		});

	}

}

function setDelete(id){

	if (confirm("Do you want delete this doc fee?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/r_gold_rate",{
			id : id
		},function(D){		
			$(".list_div").html(D);
			closeActionProgress();
			load_list();
		},"text");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/r_gold_rate",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function validate_form(){
	if ($("#goldcatagory").val() == ""){ showMessage("e","Enter gold category"); return false; }
	if (!validateAmount($("#goldrate").val() ," Pawning rate (must be in currency)")){ return false; }
	if (!validateAmount($("#assetrate").val() ," Asset rate (must be in currency)")){ return false; }	
	if (!validateAmount($("#marketrate").val() ," Market rate (must be in currency)")){ return false; }
	if ($("#printval").val() == ""){ showMessage("e","Enter print value"); return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
}

function setEdit(id){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/r_gold_rate/set_edit",{
		id : id
	},function(D){		
		$("#hid").val(D.id);
		$("#btnSave").val("Edit");				
		$("#goldcatagory").val(D.goldcatagory);
		$("#goldrate").val(D.goldrate);
		$("#assetrate").val(D.assetrate);
		$("#marketrate").val(D.marketrate);		
		$("#printval").val(D.printval);		
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/r_gold_rate/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}