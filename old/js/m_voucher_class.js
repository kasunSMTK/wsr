$(document).ready(function(){

	load_list();
	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_areaname").val());
	});




});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Area saving success");
					clear_form();
					load_list();					
/*				}else if (D == 2){					
					showMessage("e","Error, Duplicate Code");*/

				}else if(D.s == 2){
					showMessage("e","Code already exist");
				}else{					
					showMessage("e",D.m);
				}
			}

		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_voucher_class",{
			code : code
		},function(D){

			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_voucher_class",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#bc").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	if ($("#code").val() == ""){showMessage("e","Enter area code");	return false; }
	if ($("#description").val() == ""){showMessage("e","Enter area name");	return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_voucher_class/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.code);
		$("#btnSave").val("Edit");
		$("#code").val(D.code);		
		$("#description").val(D.description);		
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_voucher_class/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}