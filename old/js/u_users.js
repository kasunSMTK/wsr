
$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
   
    if (scroll >= 127){
        $(".div_user_create").css({ "position":"fixed","top":"10px"});
    }else{
        $(".div_user_create").css("position","absolute").css("top","100px");
    }

});


$(document).on("click",".reset_search",function(){
    $(".Search_bcname").val("");
    $('.user_search').click();
});
        
    

$(document).on("click",".add_user_zone", function(){
    var t  = '';
    var z  = $("#user_area_dropdown :selected").val();
    var zt = $("#user_area_dropdown :selected").text();

    if (z == ''){
        alert("select zone first");
        return;
    }else{

        var x = false;

        $(".div_zone_list").find('input[type=hidden]').each(function(){
            if($(this).val() == z){
                x = true;
            }
        });

        if (x){
            alert("This zone already added");
        }else{

            t += '<br><div style="padding-left:20px">';
            t += '<span>'+zt+'</span>';
            t += '<div style="float:right"><a class="reove_user_zone">remove</a></div>';
            t += '<input type="hidden" value="'+z+'" name="user_zone[]">';
            t += '</div>';

        }
    }

    $(".div_zone_list").prepend(t);

    $("#user_area_dropdown").val("");

});

$(document).on("click",".reove_user_zone", function(){

    if (confirm("Do you want remove this zone?")){

        $(this).parent().parent().remove();

    }

});

$(document).ready(function(){

    load_list();
    
    $("#nicNo").focus();


    $("#disable_login").click(function(){

        if ($(this).is(":checked")){

            if (confirm("By checking this option selected user will not able to login into the system. Do you want continue ")){

            }else{
                $(this).prop("checked",false);
            }

        }

    });

    
    $("#btnSave").click(function(){     
        save();
    }); 

    $(".user_search").click(function(){
        search($(".Search_bcname").val());
    });

    $("#btnRPass").click(function(){
        $("input[type='text']").attr('disabled','true');
        $("input[type='password']").removeAttr('disabled');
        $("input[type='password']").closest('div').css('background-color','green');
        $("#btnRPass").css('display','none');
    });



});


function search(q){

    load_list(q);

}

function save(){    

    showActionProgress("Saving...");

    if (validate_form()){
        var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: "json",
            success: function (D) {
                if (D.s == 1){
                    clear_form();                    
                    showMessage("s","User Creating success");
                    load_list();                    
                }else{                  
                    showMessage("e",D.m);
                }
            }
        });

    }

}

function validate_form(){

    if ($("#bc").val() == ""){showMessage("e","Select Branch");   return false; }
    if ($("#isAdmin").val() == ""){showMessage("e","Select User Type"); return false; }
    if ($("#discription").val() == ""){showMessage("e","Enter description"); return false; }
    if ($("#loginName").val() == ""){showMessage("e","Enter User Name"); return false; }
    if ($("#ruserPassword").val() == ""){showMessage("e","Enter Password"); return false; }
    if ($("#userPassword").val() == ""){showMessage("e","Enter Confirm Password"); return false; }
    if (!$("#ruserPassword").is(':disabled')) {
        if ($("#userPassword").val()  != $("#ruserPassword").val()){showMessage("e","Confirm Password Not Match"); return false; }
    };
    return true;    
}

function clear_form(){
    $("input").val("");
    $("#btnSave").val("Save");  
    $("#btnRPass").val("Reset Password");      
    $('#bc').val("");
    $("#hid").val(0);
    $("#btnRPass").css('display','none');
    $("input[type='password']").closest('div').css('background-color','');

    $("#cCode").val("").removeAttr("disabled");
    $("#bc").val("").removeAttr("disabled");
    $("#isAdmin").val("").removeAttr('disabled');
    $("#discription").val("").removeAttr('disabled');
    $("#loginName").val("").removeAttr('disabled');
    $("#loginName").val("").removeAttr('disabled');
    $("#userPassword").val("").removeAttr('disabled');
    $("#ruserPassword").val("").removeAttr('disabled');

    $("#isHO,#is_HO_cashier,#bc_manager,#multi_bc_login").attr({ checked: false  });
    $("#user_area_dropdown").val('');
    $(".div_zone_list").html("");
    $(".user_search").val("Search");
    $("#disable_login").prop("checked",false);
}

function load_list(q = ''){
    showActionProgress("Loading...");
    $.post("index.php/main/load/u_users",{
        s : "load_list",
        q : q
    },function(D){   
        $(".list_div").html(D.t);
        $("#cCode").val(D.max_no);
        closeActionProgress();
    },"json");
}

function setEdit(cCode){

    showActionProgress("Loading...");

    $.post("index.php/main/load_data/u_users/set_edit",{
        cCode : cCode
    },function(D){     
        clear_form(); 
        $("#hid").val(D.cCode);
        $("#btnSave").val("Edit");
        $("#btnRPass").css('display','inline-block');
        $("#disable_login").prop("disabled",false);

        if (D.isHO == "1"){            
            $("#isHO").prop({checked: true });
        }else{
            $("#isHO").prop({checked: false });
        }

        if (D.is_HO_cashier == "1"){            
            $("#is_HO_cashier").prop({checked: true });
        }else{
            $("#is_HO_cashier").prop({checked: false });
        }

        if (D.bc_manager == "1"){            
            $("#bc_manager").prop({checked: true });
        }else{
            $("#bc_manager").prop({checked: false });
        }

        if (D.multi_bc_login == "1"){            
            $("#multi_bc_login").prop({checked: true });
        }else{
            $("#multi_bc_login").prop({checked: false });
        }

        $("#cCode").val(D.cCode).attr('disabled','true');
        $("#bc").val(D.bc).attr('disabled','true');
        $("#isAdmin").val(D.isAdmin).attr('disabled','true');
        $("#discription").val(D.discription);
        $("#loginName").val(D.loginName).attr('disabled','true');
        $("#ruserPassword").val("SMTK").attr('disabled','true');
        $("#userPassword").val("****").attr('disabled','true');


        if (D.disable_login == 1){
            $("#disable_login").prop("disabled",false).prop("checked",true);
        }else{
            $("#disable_login").prop("disabled",false).prop("checked",false);
        }


        var X = D.area;

        if (X != null){

            X = X.split(",");

            var t = '<br><div style="padding-left:20px">';

            for ( n = 0 ; n < (X).length ; n++ ){
                
                $("#user_area_dropdown").val(X[n]);
                
                if (X[n] != ""){
                    t += '<div style="padding-bottom:10px;">';
                    t += '<span>'+$("#user_area_dropdown :Selected").text()+'</span>';
                    t += '<div style="float:right"><a class="reove_user_zone">remove</a></div>';
                    t += '<input type="hidden" value="'+X[n]+'" name="user_zone[]">';
                    t += '</div>';
                }
                
            }

            $("#user_area_dropdown").val("");

            t += '</div>';

        }else{
            t = '<div style="padding-left:20px;">No zone added for this user</div>';
        }

        $(".div_zone_list").html(t);


        closeActionProgress();
    },"json");  

}

function setDelete(cCode){

    if (confirm("Do you want delete this User?")){
        showActionProgress("Deleting...");
        $.post("index.php/main/delete/u_users",{
            cCode : cCode
        },function(D){ 

                if (D.s == 1){
                    showMessage("s","User Creating success");
                    clear_form();
                    load_list(); 
                    closeActionProgress();                                       
                }else{                  
                    showMessage("e",D.m);
                }


            // $(".list_div").html(D);
            // clear_form();

            // load_list();
        },"json");
    }
}