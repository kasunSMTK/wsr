<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backup extends CI_Controller {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
        $this->load->dbutil();
    }
    
    public function base_details(){
        $a['s'] = 1;
		return $a;
    }    
    
    public function backup_db(){        

        $fn = str_replace(" ", "_", str_replace(":", "_", date('y-m-d h:i:s')));

        $prefs = array(
            'tables'      => array(),                   // Array of tables to backup.
            'ignore'      => array(),                   // List of tables to omit from the backup
            'format'      => 'zip',                     // gzip, zip, txt
            'filename'    => $fn.'.sql',            // File name - NEEDED ONLY WITH ZIP FILES
            'add_drop'    => TRUE,                      // Whether to add DROP TABLE statements to backup file
            'add_insert'  => TRUE,                      // Whether to add INSERT data to backup file
            'newline'     => "\n"                       // Newline character used in backup file
          );

        
        $backup = $this->dbutil->backup($prefs);        
        
        $this->load->helper('file');
        

        $this->delete_old_backup_files('backup'); // comment this line if no need to remove old backup files
        write_file('backup/'.$fn.'.zip', $backup);        
             
        $dl_link = anchor(base_url().'backup/'.$fn.'.zip', 'Download', 'target="_self"');

        $a['dl'] = $dl_link;
        $a['s'] = 1;

        echo json_encode($a);
        
    }



    private function delete_old_backup_files($dir_path){        
        $files = glob($dir_path.'/*');
        foreach($files as $file){
            if(is_file($file)){
                unlink($file);
            }
        }
    }


}