<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class appvl_je extends CI_Model {
    
    private $sd;    
    private $max_no;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;
		return $a;
    }


    public function getApprovalRequests(){

    	$Q = $this->db->query("SELECT * , m_branches.`name` AS `bc`, u_users.`display_name` AS `oc` , m_branches.bc as `bc_`  FROM `t_journal_entry_sum` 

JOIN m_branches ON t_journal_entry_sum.`bc` = m_branches.`bc`
JOIN u_users ON t_journal_entry_sum.`oc` = u_users.`cCode`

WHERE is_approved = 0 ORDER BY t_journal_entry_sum.bc,NO DESC ");

    	if ($Q->num_rows() > 0){
    		$a['det'] = $Q->result();
    		$a['s'] = 1;
    	}else{
    		$a['s'] = 0;
    	}

    	echo json_encode($a);

    }



    public function makeAprove(){

    	$this->db->trans_begin();

    	$Q = $this->db->query("     UPDATE t_journal_entry_sum 
                                    SET is_approved = 1 
                                    WHERE bc = '".$_POST['bc']."' AND `no` = '".$_POST['no']."' LIMIT 1 ");

    	$this->max_no = $_POST['no'];

    	if ($Q){
    		$this->account_update(1); 
    		$a['s'] = 1;
    		$this->db->trans_commit();
    	}else{
    		$a['s'] = 0;
        	$this->db->trans_rollback();
    	}

    	echo json_encode($a);

    }

    public function account_update($condition){

    	$no = $this->max_no;
    	$bc = $_POST['bc'];

    	$Q1 = $this->db->query("SELECT * FROM `t_journal_entry_sum` WHERE bc='$bc' AND `no`='$no' LIMIT 1");
    	$Q2 = $this->db->query("SELECT * , 'grid_row' as `grid_row`  FROM `t_journal_entry_det` WHERE bc='$bc' AND `no`='$no' ");
    	$_POST = (array) $Q1->row();

        if ($_POST['time'] == ''){
            $_POST['time'] = '00:00:00';
        }
    	
    	$i = 0;

    	$_POST['grid_row'] 	= $Q2->num_rows();

        foreach ($Q2->result() as $r) {         
    		$_POST['h_' . $i] 	= $r->account_code;    		
    		$_POST['2_' . $i] 	= $r->dr_amount;
    		$_POST['3_' . $i] 	= $r->cr_amount;
    		$_POST['bc_' . $i] 	= $r->v_bc;
    		$_POST['vcl_' . $i] = $r->v_class;
    		$i++;
    	}

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", '37');
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $_POST['bc']);
        $this->db->delete('t_check_double_entry');

        if($condition==1){            
			$this->db->where("trans_no", $this->max_no);
			$this->db->where("trans_code", 37);
			$this->db->where("cl", $this->sd['cl']);
			$this->db->where("bc", $_POST['bc']);
			$this->db->delete("t_account_trans");            
        }

        if (isset($_POST['ref_no'])){
            $_POST['ref_no'] = $_POST['ref_no'];
        }else{
            $_POST['ref_no'] = '';
        }

    	
        $config = array(
            "ddate"         => $_POST['date'],
            "time"         	=> $_POST['time'],
            "trans_code"    => '37',
            "trans_no"      => $this->max_no,
            "op_acc"        => 0,
            "reconcile"     => 0,
            "cheque_no"     => 0,
            "narration"     => "",
            "ref_no"        => $_POST['ref_no']
        );

    	//echo $_POST['description'];
    	//exit;

        if (isset($_POST['description'])){
            $_POST['description'] = $_POST['description'];
        }else{
            $_POST['description'] = '';
        }

        $des = "JE : " . $_POST['description'];
        $this->load->model('account');
        $this->account->set_data($config);

        for ($i = 0; $i < $_POST['grid_row']; $i++) { 
            
            if ($_POST['h_' . $i] != "" && $_POST['h_' . $i] != "0") {
                
                if ($_POST['2_' . $i] > 0) {
                    $this->account->set_value21($des,$_POST['2_'.$i],"dr",$_POST['h_'.$i],$condition,'',$_POST['bc_'.$i],$_POST['vcl_'.$i],$_POST['bc']);
                }
                
                if ($_POST['3_' . $i] > 0) {
                    $this->account->set_value21($des,$_POST['3_'.$i],"cr",$_POST['h_'.$i],$condition,'',$_POST['bc_'.$i],$_POST['vcl_'.$i],$_POST['bc']);
                }

            }
        }

       if($condition==1){

        $query = $this->db->query("
             SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok 
             FROM `t_check_double_entry` t
             LEFT JOIN `m_account` a ON t.`acc_code` = a.`code`
             WHERE  t.`cl`='" . $this->sd['cl'] . "'  AND t.`bc`='" . $_POST['bc'] . "'  AND t.`trans_code`='37'  AND t.`trans_no` ='" .$this->max_no. "' AND 
             a.`is_control_acc`='0'");

          if($query->row()->ok == "0") {
            $this->db->where("trans_no", $this->max_no);
            $this->db->where("trans_code",37);
            $this->db->where("cl", $this->sd['cl']);
            $this->db->where("bc", $_POST['bc']);
            $this->db->delete("t_check_double_entry");
            return "0";
          }else {
            return "1";
          }
      }

    

    }





    public function makeRejected(){

        $this->db->trans_begin();

        $Q = $this->db->query("UPDATE t_journal_entry_sum SET is_approved = 2 WHERE bc = '".$_POST['bc']."' AND `no` = '".$_POST['no']."' LIMIT 1");        

        if ($Q){            
            $a['s'] = 1;
            $this->db->trans_commit();
        }else{
            $a['s'] = 0;
            $this->db->trans_rollback();
        }

        echo json_encode($a);

    }

    public function view(){

        $bc = $_POST['bc'];
        $no = $_POST['no'];


        $t = "<table border='1' borderColor='#cccccc' bgColor='#ffffff' width='800' class='tbl_je_view' align='center'>";




        $Q1 = $this->db->query("SELECT * , m_branches.name AS `bc_n` FROM `t_journal_entry_sum` 

JOIN m_branches ON t_journal_entry_sum.`bc` = m_branches.`bc`

WHERE t_journal_entry_sum.bc='$bc' AND `no`='$no' LIMIT 1");

        if ( $Q1->num_rows() > 0 ){


            $t .= "<tr>";
            $t .= "<td colspan='6' align='left'><div style='float:left'>".$Q1->row()->bc_n."</div> <div style='float:right'><a class='close_je_view'>Close</a></div>  </td>";            
            $t .= "</tr>";

            $t .= "<tr>";
            $t .= "<td width='150'>Branch ID</td>";
            $t .= "<td>".$Q1->row()->bc."</td>";
            $t .= "<td></td>";
            $t .= "<td colspan='3'></td>";
            $t .= "</tr>";

            $t .= "<tr>";
            $t .= "<td>Journal Type</td>";
            $t .= "<td>".$Q1->row()->journal_type."</td>";
            $t .= "<td></td>";
            $t .= "<td colspan='3'></td>";
            $t .= "</tr>";

            $t .= "<tr>";
            $t .= "<td>No</td>";
            $t .= "<td>".$Q1->row()->no."</td>";
            $t .= "<td></td>";
            $t .= "<td colspan='3'></td>";
            $t .= "</tr>";

            $t .= "<tr>";
            $t .= "<td>Date</td>";
            $t .= "<td>".$Q1->row()->date."</td>";
            $t .= "<td></td>";
            $t .= "<td colspan='3'></td>";
            $t .= "</tr>";

            $t .= "<tr>";
            $t .= "<td>Ref  No</td>";
            $t .= "<td>".$Q1->row()->ref_no."</td>";
            $t .= "<td></td>";
            $t .= "<td colspan='3'></td>";
            $t .= "</tr>";

            $t .= "<tr>";
            $t .= "<td valign='top' height='50'>Description</td>";
            $t .= "<td valign='top'>".$Q1->row()->narration."</td>";
            $t .= "<td valign='top'></td>";
            $t .= "<td colspan='3' valign='top'></td>";
            $t .= "</tr>";



            $Q2 = $this->db->query("SELECT * , m_branches.`name` AS `v_bc` , m_voucher_class.`description` AS `v_class` 

                , m_account.`description` AS `acc_desc`
                FROM `t_journal_entry_det` 


JOIN m_branches ON t_journal_entry_det.`v_bc` = m_branches.`bc`
JOIN `m_voucher_class` ON t_journal_entry_det.`v_class` = `m_voucher_class`.`code`
JOIN m_account ON t_journal_entry_det.`account_code` = m_account.`code`

WHERE t_journal_entry_det.bc='$bc' AND t_journal_entry_det.`no`='$no' ");            

            $t .= "<tr bgColor='#f1f1f1'>";
            $t .= "<td colspan=2>Account Code - Description</td>";            
            $t .= "<td align='right' width='150'>Dr Amount</td>";
            $t .= "<td align='right' width='150'>Cr Amount</td>";
            $t .= "<td>V Branch</td>";
            $t .= "<td>V Class</td>";
            $t .= "</tr>";

            $dr_tot = $cr_tot = 0;

            foreach ($Q2->result() as $r) {
                
                $t .= "<tr>";
                $t .= "<td colspan=2>".$r->account_code." - ".$r->acc_desc."</td>";
                
                $t .= "<td align='right'>".$r->dr_amount."</td>";
                $t .= "<td align='right'>".$r->cr_amount."</td>";
                $t .= "<td>".$r->v_bc."</td>";
                $t .= "<td>".$r->v_class."</td>";
                $t .= "</tr>";

                $dr_tot += $r->dr_amount;
                $cr_tot += $r->cr_amount;

            }

            $t .= "<tr bgColor='#f1f1f1'>";
            $t .= "<td></td>";
            $t .= "<td align='right'>Total</td>";            
            $t .= "<td align='right' width='150'><b>".number_format($dr_tot,2)."</td>";
            $t .= "<td align='right' width='150'><b>".number_format($cr_tot,2)."</td>";
            $t .= "<td></td>";
            $t .= "<td></td>";
            $t .= "</tr>";


            
        }

        $t .= "</table>";

        $a['s'] = 1;
        $a['data'] = $t;

        echo json_encode($a);

    }

    
}