<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_gold_audit extends CI_Model {

    private $sd;
    private $mtb;

    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();
        $this->mtb = 'tblusers';   
    }


    public function base_details(){
        
        // $a['grid_data'] = $this->grid_data();

        $this->load->model("m_branches");
        $a['bc_list'] = $this->m_branches->select();

        return $a;
    }
    


    public function grid_data(){

        $bc =$_POST['bc'];
        $fd =$_POST['fd'];
        $td =$_POST['td'];

        if ($_POST['chk_au'] == 'true' && $_POST['chk_unau'] == 'true'){
            $type = '';
        }else{

            if ( $_POST['chk_au'] == 'true' ){
                $type = ' AND L.audit = 1 ';
            }else if( $_POST['chk_unau'] == 'true' ){
                $type = ' AND L.audit = 0 ';
            }else{
                $type = '';
            }

        }

        $Q = $this->db->query(" SELECT 

U.`discription` AS `auditor`,

L.`billno`, C.`cusname` AS `cus_serno`, L.`requiredamount`,L.`ddate`,L.`totalweight`,(L.`requiredamount`-V.sumval) AS ovrad , L.audit_memo, L.audit , L.tfr_res
FROM `t_loan` L 
JOIN (  SELECT SUM(`value`) AS sumval,`loanno` FROM `t_loanitems` GROUP BY `loanno`) V ON V.`loanno`=L.`loanno` 
JOIN m_customer C ON L.`cus_serno` = C.`serno` 
LEFT JOIN u_users U ON L.`audit` = U.`cCode`
WHERE L.bc = '$bc' AND L.`ddate` BETWEEN '$fd' AND '$td' $type
ORDER BY L.`bc`,  L.`billtype`,  L.billno    ");

        $no = 1;

        $t  = '<table border="0" width="98%" align="center" class="tbl_gold_audit">';

        $t .= '<tr>';
            
            $t .='<td>Bill Number</td>';
            $t .='<td>Customer</td>';
            $t .='<td align="right">Amount</td>';
            $t .='<td align="center">Pawn Date</td>';
            $t .='<td align="center">Total Weight</td>';
            $t .='<td>Comment</td>';

        $t .= '</tr>';

        $row_no = 0;

        if ($Q->num_rows() > 0){
            
            foreach ($Q->result() as $r) {

                if (intval($r->audit) == 0 ){
                    $is_audit = "";
                }else{
                    $is_audit = "<div>Last audit done by ".$r->auditor."</div>";
                }
                
                $t .= '<tr id="row_id_'.$r->billno.'" class="tbl_ga_tr_bn selected_tr_top" cl = "'.$r->billno.'" bn="'.$r->billno.'" row_no="R_'.$row_no.'" tfr_res="'.$r->tfr_res.'">';            
                $t .= '<td width="130">'.$r->billno.'</td>';            
                $t .= '<td>'.$r->cus_serno.'</td>';
                $t .= '<td width="130" align="right">'.$r->requiredamount.'</td>';
                $t .= '<td width="100" align="center">'.$r->ddate.'</td>';
                $t .= '<td width="100" align="center">'.$r->totalweight.'</td>';
                $t .= '<td width="400"><input type="text" value="'.$r->audit_memo.'" class="bn_sum_comment" id="R_'.$row_no.'" style="width:100%;display:none; margin-bottom:4px"><span style="font-size:14px;font-family:roboto">'.$is_audit.'</span></td>';
                $t .= '</tr>';            

                $t .= '<tr class="'.$r->billno.' all_row ga_bill_det_show selected_tr_bottom"></tr>';

                $row_no++;            

            }

        }else{

            $t .= '<tr>';
            
                $t .='<td colspan="6" align="center">No data found</td>';

            $t .= '</tr>';

        }

        $t .= '</table>';


        $a['d'] = $t;

        echo json_encode($a);

    }

    
    
    public function get_bill_data(){

        $bn = $_POST['billno'];        
        
        $Q = $this->db->query("SELECT 

I.`itemname` AS `itemname`,
I.`itemcode` AS `itemcode`,
IC.`des` AS `cat_code`,
CN.`des` AS `con`,
Li.`goldweight`,
LI.`pure_weight`,
LI.`qty`,
GR.`goldcatagory` AS `goldtype`,
LI.`quality`,

LI.`goldvalue`,
LI.`value`,
LI.`bulk_items`,
LI.`denci_weight`,
LI.audit_comment,
LI.audit_goldtype,
if(LI.audit_goldweight=0,'',LI.audit_goldweight) as `audit_goldweight`,
if (LI.audit_pure_weight=0,'',LI.audit_pure_weight) as `audit_pure_weight`,
if(LI.audit_overadvance=0,'',LI.audit_overadvance) as `audit_overadvance`,
LI.`auto_no` as `nno`


FROM `t_loanitems` LI 

JOIN r_items I ON LI.`itemcode` = I.`itemcode`
JOIN `r_itemcategory` IC ON LI.`cat_code` = IC.`code`
JOIN r_condition CN ON LI.`con` = CN.`code`
JOIN `r_gold_rate` GR ON LI.`goldtype` = GR.`id`


WHERE LI.`billno` = '$bn'");

        $a['q'] = $Q->result();
        $a['row_no'] = $_POST['row_no'];
        $a['tfr_res'] = $_POST['tfr_res'];

        echo json_encode($a);
        
    }





    public function save(){ 

        $audit_goldweight = explode(",",$_POST['gw']);
        $_POST['audit_goldweight'] = $audit_goldweight;
        unset($_POST['gw']);

        $audit_pure_weight = explode(",",$_POST['pw']);
        $_POST['audit_pure_weight'] = $audit_pure_weight;
        unset($_POST['pw']);

        $audit_goldtype = explode(",",$_POST['gt']);
        $_POST['audit_goldtype'] = $audit_goldtype;
        unset($_POST['gt']);

        $audit_overadvance = explode(",",$_POST['oa']);
        $_POST['audit_overadvance'] = $audit_overadvance;
        unset($_POST['oa']);

        $article_comment = explode(",",$_POST['ac']);
        $_POST['article_comment'] = $article_comment;
        unset($_POST['ac']);

        $itemcode = explode(",",$_POST['ic']);
        $_POST['itemcode'] = $itemcode;
        unset($_POST['ic']);

        $auto_no = explode(",",$_POST['an']);
        $_POST['auto_no'] = $auto_no;
        unset($_POST['an']);

        $q = $this->db->where("billno",$_POST['billno'])->set(array(    "audit_memo"=>$_POST['bill_comment'],
                                                                        "audit"=>$this->sd['oc'],
                                                                        "tfr_res"=>$_POST['tfr_res']

                                                                    ))->limit(1)->update('t_loan');
        
        for($n=0 ; $n < count($_POST['itemcode']) ; $n++){
            
            $this->db->where(array( "billno" => $_POST['billno'],
                                    "itemcode" => $_POST['itemcode'][$n],
                                    "auto_no" => $_POST['auto_no'][$n] ))
                     
                     ->set(array(   "audit_comment"     =>$_POST['article_comment'][$n],
                                    "audit_goldweight"  =>$_POST['audit_goldweight'][$n],
                                    "audit_pure_weight" =>$_POST['audit_pure_weight'][$n],
                                    "audit_goldtype"    =>$_POST['audit_goldtype'][$n],
                                    "audit_overadvance" =>$_POST['audit_overadvance'][$n]   ))->limit(1)

                     ->update('t_loanitems');
        }


        if ($q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }



    public function search_bill_stat(){
        
        $bn = $_POST['billno'];

        $Q = $this->db->query("SELECT * FROM (SELECT * FROM t_loantranse UNION ALL SELECT * FROM t_loantranse_re_fo ) a WHERE transecode NOT IN ('A','P') AND billno = '$bn' ");

        if ($Q->num_rows() > 0){
            $s = 1;
        }else{
            $s = 0;
        }

        $a['s'] = $s;

        echo json_encode($a);

    }


}