<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_conditions extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){
        $a['code_max'] = $this->getCodeMax();
        $a['load_list'] = $this->load_list();
		return $a;
    }

    public function autocomplete(){               
        $q = $_GET['term'];
        $query = $this->db->query("SELECT * FROM r_condition WHERE (`code` LIKE '%$q%' OR `des` LIKE '%$q%') ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->code . " - ". $r->des ; }    
        echo json_encode($ary);
    }
    
    public function save(){

        if ($_POST['hid'] == "0"){
            unset($_POST['hid']);
        	$Q = $this->db->insert("r_condition",$_POST);
        }else{
            unset($_POST['code']);
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("code",$hid)->update("r_condition",$_POST);
        }

        if ($Q){
            echo 1;
        }else{
            echo 0;
        }
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $code = $_POST['code'];

       if ($this->db->query("DELETE FROM`r_condition` WHERE code = '$code' ")){
            echo 1;
       }else{
            echo 0;
       }
    }

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `r_condition` ORDER BY `code` DESC LIMIT 5 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code . "</td><td class='D'>-</td><td class='Des'> " .$R->des. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "<table>";
        $a['T'] = $T;        
        $a['max_no'] = $this->getCodeMax();
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $code = $_POST['code'];
        $R = $this->db->query("SELECT * FROM `r_condition` WHERE code = '$code' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `r_condition` WHERE (code like '%$k%' OR des like '%$k%')  LIMIT 10");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code . "</td><td class='D'>-</td><td class='Des'> " .$R->des. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "<table>";
    
        echo $T;
            
    }

    public function getArea(){

        $Q = $this->db->query("SELECT * FROM `r_condition` ORDER BY description");

        if ($Q->num_rows() > 0){

            $T = "<SELECT id='area' name='area'  class='input_ui_dropdown'><option value=''>Select area</option>";

            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->code."'>".$R->code." - ".$R->description."</option>";
            }

            $T .= "</SELECT>";

            return $T;
        }else{
            return $T;
        }

    } 

    public function getCodeMax(){
        $max_no = $this->db->query("SELECT IFNULL(MAX(CONVERT(code, SIGNED INTEGER))+1,1) AS `max_no` FROM `r_condition` ")->row()->max_no;
        if (strlen($max_no) == 1){ $max_no = "00".$max_no; }
        if (strlen($max_no) == 2){ $max_no = "0".$max_no; }
        if (strlen($max_no) == 3){ $max_no = $max_no; }
        return $max_no;
    }

}