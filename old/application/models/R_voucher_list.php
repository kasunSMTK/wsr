<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_voucher_list extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report(){

        $bc = $this->sd['bc'];
        $fd = $_POST['fd'];
        $td = $_POST['td'];
        $vs = $_POST['vou_stat'];

        if ($vs == "All"){
            $QC = "";
        }else{
            $QC = " AND VS.`status` = '$vs' ";
        }



        $no = $_POST['l_no'];        
        $bc = $this->sd['bc'];
        
        $q = $this->db->query("SELECT 

VS.`bc`,S.`nno`,S.`type`,VS.`status` ,
CONCAT(S.`paid_acc`,' - ', ACC1.`description`) AS `paid_acc_desc`,
CONCAT(D.`acc_code`,' - ', ACC2.description) AS `acc_code`,
CONCAT(D.v_bc,' - ' ,  V_BC.`name`) AS `v_bc_name`,
CONCAT(V_CLASS.`code`,' - ',V_CLASS.`description`) AS `v_class`,
D.`amount`,S.`action_date`,S.`note`, S.`payee_desc`

FROM `t_gen_vou_app_rqsts_sum` VS

JOIN `t_voucher_gl_sum` S ON VS.`bc` = S.`bc` AND VS.`nno` = S.`nno` AND VS.`paid_acc` = S.`paid_acc`
JOIN `t_voucher_gl_det` D ON S.`bc` = D.`bc` AND S.`nno` = D.`nno` AND S.`paid_acc` = D.`paid_acc`
JOIN m_account ACC1 ON S.`paid_acc` = ACC1.`code`
JOIN m_account ACC2 ON D.`acc_code` = ACC2.`code`
JOIN m_branches V_BC ON D.`v_bc` = V_BC.`bc`
JOIN m_voucher_class V_CLASS ON D.`v_class` = V_CLASS.`code`

WHERE VS.`bc` = '$bc' AND DATE BETWEEN '$fd' AND '$td' $QC
GROUP BY S.`nno`
ORDER BY S.`action_date` DESC,S.`bc`,S.`nno` ");      

        if($q->num_rows()>0){
            $r_detail['det'] = $q->result();
            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}