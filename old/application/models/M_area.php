<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_area extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){

        $_POST['oc'] = $this->sd['oc'];

        if ($_POST['hid'] == "0"){
            unset($_POST['hid']);

            // $WhClArr = array('code' => $_POST['code'],);
            // if ($this->utility->check_code_dup("m_area",$WhClArr)) {

            if ($this->db->insert("m_area",$_POST)) {                
                 $a['s'] = 1;
                // $Q = $this->db->insert("m_area",$_POST);
                }
            else {
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
            }

        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("code",$hid)->update("m_area",$_POST);
            $a['s']= 1;            
        }
        
        echo json_encode($a);

    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $code = $_POST['code'];
       
       if ($this->db->query("DELETE FROM`m_area` WHERE code = '$code' ")) {          
            $a['s'] = 1;
        }else{       
            $a['s'] = 0;            
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
        }

        echo json_encode($a);
    }





    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `m_area` ORDER BY `code` DESC LIMIT 10 ");
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;        
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $code = $_POST['code'];
        $a['R']         = $this->db->query("SELECT * FROM `m_area` WHERE code = '$code' LIMIT 1 ")->row();
        $a['bc_list']   = $this->db->query("SELECT B.`bc`,B.`name` FROM m_branches B WHERE B.`area` = '$code'")->result();
        echo json_encode($a);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `m_area` WHERE (code like '%$k%' OR description like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code ."</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }

    public function getArea($n = '' , $i = '', $c = ''){

        if ($n == ""){ $name = 'area'; }else{ $name = $n; }
        if ($i == ""){ $id = 'area'; }else{ $id = $i; }
        if ($c == ""){ $class = 'input_ui_dropdown'; }else{ $class = $c; }

        $Q = $this->db->query("SELECT * FROM `m_area` ORDER BY description");
        if ($Q->num_rows() > 0){
            $T = "<SELECT id='$id' name='$name'  class='$class'><option value=''>Select area</option>";
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->code."'>".$R->code." - ".$R->description."</option>";
            }
            $T .= "</SELECT>";
            return $T;
        }else{
            return $T;
        }

    } 

    public function getBcMax(){
        $max_no = $this->db->query("SELECT IFNULL(MAX(CONVERT(bc, SIGNED INTEGER))+1,1) AS `max_no` FROM `m_area` ")->row()->max_no;
        if (strlen($max_no) == 1){ $max_no = "00".$max_no; }
        if (strlen($max_no) == 2){ $max_no = "0".$max_no; }
        if (strlen($max_no) == 3){ $max_no = $max_no; }
        return $max_no;
    }

}