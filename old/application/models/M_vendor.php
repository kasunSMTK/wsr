<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_vendor extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){
        $a['load_list'] = $this->load_list();
		return $a;
    }
    
    public function save(){

        $_POST['oc'] = $this->sd['oc'];

        if ($_POST['hid'] == "0"){

            if (isset( $_POST['is_cash_customer'] )){
                $_POST['is_cash_customer'] = 1;
            }else{
                $_POST['is_cash_customer'] = 0;
            }

            $_POST['code'] = $this->max_code();

            unset($_POST['hid']);
            if ($this->db->insert("m_vendor",$_POST)) {                
                 $a['s'] = 1;                
            }else{
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
            }
        }else{
            
            if (isset( $_POST['is_cash_customer'] )){
                $this->db->set('is_cash_customer',0)->update('m_vendor');
                $_POST['is_cash_customer'] = 1;
            }


            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("code",$hid)->update("m_vendor",$_POST);
            $a['s']= 1;            
        }
        
        echo json_encode($a);

    }
    
    public function max_code(){

       return $this->db->query("SELECT IFNULL(MAX(CODE)+1,1) AS `max_code` FROM `m_vendor`")->row()->max_code; 

    }
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $code = $_POST['code'];
       
       if ($this->db->query("DELETE FROM`m_vendor` WHERE code = '$code' ")) {          
            $a['s'] = 1;
        }else{       
            $a['s'] = 0;            
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
        }

        echo json_encode($a);
    }





    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `m_vendor` ORDER BY `name` ");
        $T = "<table>";
        if ($Q->num_rows() > 0){


            $T .= "<tr>";
            $T .= "<td>Name</td>";
            $T .= "<td>Description</td>";
            $T .= "<td>NIC</td>";
            $T .= "<td>Cash Customer</td>";
            $T .= "<td>Action</td>";
            $T .= "</tr>";

            foreach($Q->result() as $R){

                $T .= "<tr>";                
                $T .= "<td>".$R->name."</td>";
                $T .= "<td>".$R->description."</td>";
                $T .= "<td>".$R->nic."</td>";
                $T .= "<td>".$R->is_cash_customer."</td>";
                $T .= "<td><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td>";
                $T .= "</tr>";
            }

        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "</table>";
        $a['T'] = $T;        
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $code = $_POST['code'];
        $a['R']         = $this->db->query("SELECT * FROM `m_vendor` WHERE code = '$code' LIMIT 1 ")->row();
        $a['bc_list']   = $this->db->query("SELECT B.`bc`,B.`name` FROM m_branches B WHERE B.`area` = '$code'")->result();
        echo json_encode($a);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `m_vendor` WHERE (`name` like '%$k%' OR code like '%$k%' OR description like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            

            $T .= "<tr>";
            $T .= "<td>Name</td>";
            $T .= "<td>Description</td>";
            $T .= "<td>NIC</td>";
            $T .= "<td>Cash Customer</td>";
            $T .= "<td>Action</td>";
            $T .= "</tr>";
            
            foreach($Q->result() as $R){
                $T .= "<tr>";                
                $T .= "<td>".$R->name."</td>";
                $T .= "<td>".$R->description."</td>";
                $T .= "<td>".$R->nic."</td>";
                $T .= "<td>".$R->is_cash_customer."</td>";
                $T .= "<td><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td>";
                $T .= "</tr>";
            }
        }else{
            $T = "<center>No vendors added</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }

    public function getArea($n = '' , $i = '', $c = ''){

        if ($n == ""){ $name = 'area'; }else{ $name = $n; }
        if ($i == ""){ $id = 'area'; }else{ $id = $i; }
        if ($c == ""){ $class = 'input_ui_dropdown'; }else{ $class = $c; }

        $Q = $this->db->query("SELECT * FROM `m_vendor` ORDER BY description");
        if ($Q->num_rows() > 0){
            $T = "<SELECT id='$id' name='$name'  class='$class'><option value=''>Select area</option>";
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->code."'>".$R->code." - ".$R->description."</option>";
            }
            $T .= "</SELECT>";
            return $T;
        }else{
            return $T;
        }

    } 

    public function getBcMax(){
        $max_no = $this->db->query("SELECT IFNULL(MAX(CONVERT(bc, SIGNED INTEGER))+1,1) AS `max_no` FROM `m_vendor` ")->row()->max_no;
        if (strlen($max_no) == 1){ $max_no = "00".$max_no; }
        if (strlen($max_no) == 2){ $max_no = "0".$max_no; }
        if (strlen($max_no) == 3){ $max_no = $max_no; }
        return $max_no;
    }

}