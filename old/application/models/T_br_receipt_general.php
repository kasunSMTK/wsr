<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class t_br_receipt_general extends CI_Model
{

  private $sd;
  private $mtb;
  private $max_no;
  private $bc_glob;

  function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);    
    $this->load->model('user_permissions');
    
  }
  
  public function base_details(){
    $a['current_date'] = $this->sd['current_date'];
    $a['date_change_allow'] = $this->sd['date_chng_allow'];
    $a['backdate_upto'] = $this->sd['backdate_upto'];
    $this->load->model(array('m_branches','m_voucher_class','m_bank'));    
    $a['receipt_customer_dropdown'] = $this->receipt_customer_dropdown();
    $a['get_max_no']                = $this->get_max_no();
    return $a;
  }


  public function save(){

    $this->db->trans_begin();

    if (isset($_POST['chk_print_receipt'])){
      $chk_print_receipt = 1;
      $_POST['r_is_printed'] = 1;
    }else{
      $chk_print_receipt = 0;
      $_POST['r_is_printed'] = 0;
    }
    unset($_POST['chk_print_receipt']);


    $_POST['bc'] = $this->bc_glob = $this->sd['bc'];

    if ( $_POST['hid'] == "0" || $_POST['hid'] == "") {
      $_POST['nno'] = $this->max_no   = $this->get_max_no('gl_receipt');
    }else{
      $_POST['nno'] = $this->max_no   = $_POST['hid'];
    }

    $_POST['receipt_desc'] = $_POST['voucher_desc'];
    $_POST['customer'] = $_POST['payee_desc'];   

    if ( $_POST['type'] == "cash" ){
      $_POST['cash_amount'] = $_POST['total_amount'];
      $_POST['cheque_amount'] = 0;
      $_POST['paid_to_acc'] = $_POST['from_acc'];
   
    }

    if (isset($_POST['to_acc'])){

      for ( $n = 0 ; $n < count($_POST['to_acc']) ; $n++) {

        if ( !empty($_POST['to_acc'][$n]) || $_POST['to_acc'][$n] != "" ){

          $receipt_det[] = array(
            'bc'            =>$this->sd['bc'],
            'nno'           => $this->max_no,
            'acc_code'      => $_POST['to_acc'][$n],
            'amount'        => $_POST['to_amount'][$n],
            'type'          => $_POST['type'],      
            'receipt_type'  => 'branch_receipt',
            'is_dr_acc'     => 0
            );
        }

      }

    }


    if ( $_POST['adv_settle_amount'] > 0 ){

      $receipt_det[] = array(
        'bc'            =>$this->sd['bc'],
        'nno'           => $this->max_no,
        'acc_code'      => $_POST['adv_settle_acc'],
        'amount'        => $_POST['adv_settle_amount'],
        'type'          => $_POST['type'],      
        'receipt_type'  => 'branch_receipt',
        'is_dr_acc'     => 1
      );

    }


    $q = false;        

    $_POST_TMP['saved_time'] = $_POST['time'];
    $_POST['time'] = date('H:i:s');

    $_POST['oc'] = $this->sd['oc'];

    if ($this->validation() == 1) {

            if ( $_POST['hid'] == "0" || $_POST['hid'] == "") {

              $is_reprint = 0;

              $this->account_update(1);
              
              unset($_POST['to_acc'] , $_POST['to_amount'] , $_POST['v_bc'] ); // need to use
              unset($_POST['from_acc'] , $_POST['payee_desc'],$_POST['total_amount'],$_POST['voucher_desc']);

              unset($_POST['hid'], $_POST['adv_settle_amount'] , $_POST['adv_settle_acc'] );

              $q = $this->db->insert("t_br_receipt_sum", $_POST );

              if ($q){
                if (isset($receipt_det)){
                  $this->db->insert_batch("t_br_receipt_det", $receipt_det );
                }

                if (isset($_RChq)){
                  $this->db->insert("t_cheque_received", $_RChq );
                }
              }

              $this->utility->user_activity_log($module='Branch Receipt',$action='insert',$trans_code='96',$trans_no=$_POST['nno'],$note='');

              $a['s'] = 1;

            }else{

              $is_reprint = 1;

              $_POST['time'] = $_POST_TMP['saved_time'];

              if ($this->utility->check_receipt_update_stat() == 0 ){

                $this->account_update(1);

                unset($_POST['to_acc'] , $_POST['to_amount'] , $_POST['v_bc'] ); // need to use
                unset($_POST['from_acc'] , $_POST['payee_desc'],$_POST['total_amount'],$_POST['voucher_desc']);

                unset($_POST['hid'],$_POST['adv_settle_acc'], $_POST['adv_settle_amount']);

                $q = $this->db->where(array("nno"=>$_POST['nno'],"bc"=>$_POST['bc']))->set($_POST)->limit(1)->update("t_br_receipt_sum");

                if ($q){

                  $this->db->where(array("nno"=>$_POST['nno'],"bc"=>$_POST['bc']))->delete("t_br_receipt_det");
                  if (isset($receipt_det)){
                    $this->db->insert_batch("t_br_receipt_det", $receipt_det );
                  }

                  $this->db->where(array( "trans_code"=>96, "trans_no"=>$_POST['nno']))->delete("t_cheque_received");
                  if (isset($_RChq)){
                    $this->db->insert("t_cheque_received", $_RChq );
                  }
                  
                }

                $this->utility->user_activity_log($module='Branch Receipt',$action='update',$trans_code='96',$trans_no=$_POST['nno'],$note='');

                $a['update_det'] = "Receipt updated at ".date('Y-m-d H:i:s');
                $a['s'] = 1;

              }else{
                
                $a['update_det'] = "<br><span style='color:#ffffff;background-color:red;padding:10px;font-size:14px'>Update failed or update option not enabled for your branch, please contact head office</span>";
                
                $a['s'] = 2;

              }


            }

          }else{
            $a['s'] = 2;
            $a['receipt_update_msg'] = "Invalid account entries";
          }


          if ($this->db->trans_status() === FALSE){
              $this->db->trans_rollback();
              $a['s'] = 0;
          }else{            
              $this->db->trans_commit();              
          }

          if ($a['s'] != 0){
            $a['chk_print_receipt'] = $chk_print_receipt;
          }else{
            $a['chk_print_receipt'] = 0;
          }

          $a['no'] = $_POST['nno'];

          $a['is_reprint'] = $is_reprint;

          echo json_encode($a);

  }

        public function cancel_receipt(){

          exit;
          
          $bc                 = $this->sd['bc'];
          $nno                = $_POST['nno'];
          $chk_cheque_status  = 'P';

          if ( $_POST['type'] == 'cheque' ){
            $chk_cheque_status = $this->db->query("SELECT `status` FROM `t_cheque_received` WHERE trans_code = 96 AND trans_no = $nno AND bc = '$bc' LIMIT 1")->row()->status;
          }

          if ($chk_cheque_status != 'D'){

            // cancel allow
            $q = $this->db->where(array("bc"=>$bc,"nno"=>$nno))->set('is_cancel',1)->limit(1)->update("t_br_receipt_sum");

            // update account entries for cancel process
            $this->account_cancel(1);
            $a['s'] = 1;

          }else{
            $a['reason'] = 'Unable to cancel, the Cheque received with this receipt is indicating as deposit';
            $a['s'] = 2; 
          }

          echo json_encode($a);

        }

        public function receipt_customer_dropdown(){

          $q = $this->db->query("SELECT * FROM m_receipt_customer ORDER BY `name`");

          $T = '<select id="payee_desc" name="payee_desc">';

          if ($q->num_rows() > 0){

            $T .= '<option value="">Select Receipt Customer</option>';

            foreach($q->result() as $r){
              $T .= "<option cash_customer='".$r->is_cash_customer."' value='".$r->code."'>".$r->name." - ".$r->nic." - ".$r->description."</option>";
            }
          }else{
            $T .= '<option value="">Receipt Customers not added</option>';
          }

          $T .= '</select>';

          return $T;

        }

        public function account_update($condition){

          unset(  $_POST['v_bc']  );

          $this->db->where("trans_no", $this->max_no);
          $this->db->where("trans_code", 96);
          $this->db->where("cl", $this->sd['cl']);
          $this->db->where("bc", $this->bc_glob);
          $this->db->delete("t_check_double_entry");

          if ($condition == 1) {
            if ($_POST['hid'] != "0" || $_POST['hid'] != "") {
              $this->db->where('cl', $this->sd['cl']);
              $this->db->where('bc', $this->bc_glob);
              $this->db->where('trans_code', 96);
              $this->db->where('trans_no', $this->max_no);
              $this->db->where('entry_code',0);
              $this->db->delete('t_account_trans');
            }
          }

          $config = array(
            "ddate" => $_POST['date'],
            "time" => $_POST['time'],
            "trans_code" => 96,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => $_POST['ref_no']
            );

          $des = "General Receipt";
          $this->load->model('account');
          $this->account->set_data($config);   

          if (isset($_POST['to_acc'])){

            for ($i = 0; $i < count($_POST['to_acc']); $i++) {
              if ( $_POST['to_acc'][$i] ){
                $dess = "General Receipt";              

                if ($_POST['to_amount'][$i] > 0){
                  $this->account->set_value4($dess,$_POST['to_amount'][$i],"cr", $_POST['to_acc'][$i],$condition,"","",0);
                }
                
              }
            }    

          }

          if ($_POST['type'] == "cash"){      
            $acc_code = $_POST['from_acc'];
            
            if ($_POST['total_amount'] > 0){
              $this->account->set_value4($des, $_POST['total_amount'], "dr", $acc_code, $condition,"","",0);
            }

            if ( floatval($_POST['adv_settle_amount']) > 0 ){
                $this->account->set_value4($des,$_POST['adv_settle_amount'],"dr",$_POST['adv_settle_acc'],$condition,"","",0);
            }


          }

          if ($_POST['type'] == "cheque"){  
            $recev_chq_acc_code = $this->utility->get_default_acc("CHEQUE_IN_HAND");    
            $this->account->set_value4($des."D", $_POST['total_amount'], "dr", $recev_chq_acc_code, $condition,"","",0);
          }    

          if ($condition == 0) {

            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='".$this->sd['cl']."'  AND t.`bc`='".$this->bc_glob."' AND t.`trans_code`='96'  AND t.`trans_no` ='" . $this->max_no . "' AND t.entry_code = '0' AND a.`is_control_acc`='0'");

            if ($query->row()->ok == "0") {        
              $this->db->where("trans_no", $this->max_no);
              $this->db->where("trans_code", 96);
              $this->db->where("cl", $this->sd['cl']);
              $this->db->where("bc", $this->bc_glob);
              $this->db->delete("t_check_double_entry");        
              return "0";
            } else {        
              return "1";
            }

          }

        }

        public function account_cancel($condition){

          $bc = $this->sd['bc'];   

          $config = array(
            "ddate" => $this->sd['current_date'],
            "time" => $_POST['time'],
            "trans_code" => 96,
            "trans_no" => $_POST['nno'],
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => $_POST['ref_no']
            );

          $des = "General Receipt - Cancel ";
          $this->load->model('account');
          $this->account->set_data($config);

          $q = $this->db->where(array( "bc" => $bc, "trans_code" => 96,"trans_no" => $_POST['nno']))->get('t_account_trans');

          foreach($q->result() as $r){        

            if ($r->dr_amount !=  0){
              $dess = "General Receipt - Cancel";
              $this->account->set_value4($dess,$r->dr_amount,"cr",$r->acc_code, $condition,"","", 0);
            }

            if ($r->cr_amount !=  0){
              $dess = "General Receipt - Cancel";
              $this->account->set_value4($dess,$r->cr_amount,"dr",$r->acc_code, $condition,"","", 0);
            }

          }


        }

        public function get_max_no(){
          $bc = $this->sd['bc'];
          return $this->db->query(" SELECT IFNULL(MAX(nno)+1,1) AS `max_no` FROM `t_br_receipt_sum` V WHERE  bc = '$bc' ")->row()->max_no; 
        }

        public function get_acc_list(){
          $q = $_GET['term'];
          $query = $this->db->query("SELECT MA.`code`,MA.`description` FROM m_account MA WHERE MA.`is_control_acc` = 0 AND (MA.`code` LIKE '%$q%' OR MA.`description` LIKE '%$q%') LIMIT 200 ");                
          $ary = array();    

          foreach($query->result() as $R){ 
            $ary[] = $R->code." - ".$R->description; 
          }

          echo json_encode($ary);
        }



        public function get_max_no_type()
        {

          $table_name = $_POST['table'];
          $field_name = $_POST['nno'];
          $type       = $_POST['type'];

          if (isset($_POST['hid'])) {
            if ($_POST['hid'] == "0" || $_POST['hid'] == "") {
              $this->db->select_max($field_name);
              $this->db->where("cl", $this->sd['cl']);
              $this->db->where("bc", $this->sd['branch']);
              $this->db->where("type", $type);
              echo $this->db->get($table_name)->first_row()->$field_name + 1;
            } else {
              echo $_POST['hid'];
            }
          } else {
            $this->db->select_max($field_name);
            $this->db->where("cl", $this->sd['cl']);
            $this->db->where("bc", $this->sd['branch']);
            $this->db->where("type", $type);
            echo $this->db->get($table_name)->first_row()->$field_name + 1;
          }
        }

        public function get_data()
        {


          $sql      = "SELECT 
          a.`code` AS paid_acc_code,
          a.`description` AS paid_acc_des,
          v.`note`,
          v.`sub_no` as nno,
          v.`nno` as hid_nno,
          v.`ref_no` as ref,
          c.`code` AS cat_code,
          c.`description` AS cat_des,
          g.`code` AS groups_code,
          g.`name`,
          v.`type`,
          v.is_cancel,
          v.`ddate`,
          v.`cash_amount`,
          v.`cheque_amount`,
          (v.`cash_amount` + v.cheque_amount) AS tot ,
          v.is_approved
          FROM
          t_voucher_gl_sum v 
          LEFT JOIN m_account a ON a.code = v.paid_acc 
          LEFT JOIN r_sales_category c ON c.code = v.category_id 
          LEFT JOIN r_groups g ON g.code = v.`group_id` 
          WHERE 
          v.type = '" . $_POST['type'] . "'
          AND v.sub_no = '" . $_POST['id'] . "' 
          AND v.bc = '" . $this->sd['branch'] . "' 
          AND v.cl = '" . $this->sd['cl'] . "' 
          LIMIT 1";

          $query    = $this->db->query($sql);
          $a['sum'] = $query->first_row();


          $n_no = 0;
          $n_no = $query->row()->hid_nno;

          $sql_cheque  = "SELECT * FROM opt_issue_cheque_det 
          WHERE cl='" . $this->sd['cl'] . "' AND bc='" . $this->sd['branch'] . "'
          AND trans_code='48' AND trans_no='$n_no'";
          $queryy      = $this->db->query($sql_cheque);
          $a['cheque'] = $queryy->result();



          $sql      = "SELECT 
          d.`acc_code`,
          a.`description`,
          d.`amount`,
          d.`ref_no`,
          d.v_bc,
          d.v_class
          FROM t_voucher_gl_sum 
          
          JOIN t_voucher_gl_det d
          ON t_voucher_gl_sum.`sub_no` = d.`sub_no` 
          AND t_voucher_gl_sum.`type` = d.`type`
          AND t_voucher_gl_sum.`cl` = d.`cl` 
          AND t_voucher_gl_sum.`bc` = d.`bc`  
          JOIN m_account a ON a.code = d.`acc_code`
          WHERE t_voucher_gl_sum.`type` = '" . $_POST['type'] . "' 
          AND t_voucher_gl_sum.`sub_no` ='" . $_POST['id'] . "'  AND t_voucher_gl_sum.bc='" . $this->sd['branch'] . "' AND t_voucher_gl_sum.cl='" . $this->sd['cl'] . "'
          GROUP BY d.`acc_code` ,d.`v_bc`,d.`v_class` ";
          $query    = $this->db->query($sql);
          $a['det'] = $query->result();


          echo json_encode($a);
        }


        //-----------------------------load default acc-------------------------------------------


        public function load_def_acc(){
          $sql="SELECT 
                m.acc_code,
                m.description AS acc_desc
                FROM m_default_account m
                WHERE m.code IN('UNREDEEM_ARTICLES','POSTAGE_RECEIVABLE','REDEEM_INTEREST','STAMP_FEE','ADVANCE_RECEIVED','OTHER_INCOME') ";
          $query    = $this->db->query($sql);
          $a['det'] = $query->result();

          echo json_encode($a);
        }
        //----------------------------------------------------------------------------------------



//--------------------------new pdf report 17_03_2017----------------------------------------------------
        public function PDF_report(){

          if (isset($_POST['h_v_bc'])){
            if ($_POST['h_v_bc'] != ""){
              $bc = $_POST['h_v_bc'];
            }else{
              $bc = $this->sd['branch'];
            }
          }else{
            $bc = $this->sd['branch'];
          }

          //$this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);

          $this->db->select(array('name'));
          $r_detail['company'] = $this->db->get('m_company')->result();
          $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
          $this->db->where("cl", $this->sd['cl']);
          $this->db->where("bc", $bc);
          $r_detail['branch'] = $this->db->get('m_branches')->result();

          $invoice_number      = $this->utility->invoice_format($_POST['qno']);

          $session_array       = array(      
            $invoice_number
            );

          $r_detail['session'] = $session_array;

          $this->db->where("code", $_POST['sales_type']);
          $query = $this->db->get('t_trans_code');

          if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
              $r_detail['r_type'] = $row->description;
            }
          }

         
          $r_detail['type']         = "t_br_receipt_general";
          $r_detail['dt']           = $_POST['dt'];
          $r_detail['qno']          = $_POST['qno'];
          $r_detail['voucher_type'] = $_POST['voucher_type'];

          $r_detail['voucher_no']  = $_POST['qno'];
          $r_detail['category_id'] = $_POST['category_id'];
          $r_detail['cat_des']     = $_POST['cat_des'];
          $r_detail['group_id']    = $_POST['group_id'];
          $r_detail['group_des']   = $_POST['group_des'];
          $r_detail['ddate']       = $_POST['r_ddate'];
          $r_detail['tot']         = $_POST['tot'];



          $r_detail['num'] = $_POST['tot'];

          $num = $_POST['tot'];

          $this->utility->num_in_letter($num);

//-------------------------------------------------

          $r_detail['rec'] = convertNum($num);
          ;

          $r_detail['page']        = $_POST['page'];
          $r_detail['header']      = $_POST['header'];
          $r_detail['orientation'] = $_POST['orientation'];

          $r_detail['acc_code'] = $_POST['acc_code'];
          $r_detail['acc_des']  = $_POST['acc_des'];
          $r_detail['vou_des']  = $_POST['vou_des'];

          $sql=" SELECT VD.acc_code, VD.amount, MA.description 
          FROM t_br_receipt_det VD
          JOIN m_account MA ON VD.acc_code = MA.code 
          WHERE  VD.bc='".$bc."'  AND VD.`nno` = ".$_POST['qno']." AND VD.`type`= '".$_POST['voucher_type']."'";
          //AND VD.`type`= '".$_POST['voucher_type']."'  AND VD.`paid_acc` = '".$_POST['r_paid_acc']."'
          //VD.cl='".$this->sd['cl']."' AND 
          $r_detail['dets'] = $this->db->query($sql)->result(); 

          $sql="SELECT * FROM t_br_receipt_sum 
          WHERE bc='".$bc."' 
          AND `nno` = '".$_POST['qno']."' AND `type`= '".$_POST['voucher_type']."'";

          /*$sql="SELECT * FROM t_voucher_gl_sum 
          WHERE cl='".$this->sd['cl']."' AND bc='".$bc."' 
          AND `nno` = ".$_POST['r_nno']." AND `type`= '".$_POST['voucher_type']."'
          AND paid_acc = '".$_POST['r_paid_acc']."' ";*/


          $r_detail['sum'] = $this->db->query($sql)->result(); 


          $sql="SELECT * FROM t_cheque_issued WHERE trans_code='48' 
          AND cl='".$this->sd['cl']."' AND bc='".$bc."'
          AND account = '".$_POST['r_paid_acc']."' AND trans_no = '".$_POST['r_nno']."'";

          $r_detail['cheque'] = $this->db->query($sql)->result();       



    // $this->db->select(array(
    //   'name'
    // ));
    // $this->db->where("code", $_POST['salesp_id']);
    // $query = $this->db->get('m_employee');

    //foreach ($query->result() as $row) {
      //$r_detail['employee'] = $row->name;
   // }

          $this->db->select(array(
            'loginName'
            ));
          $this->db->where('cCode', $this->sd['oc']);
          $r_detail['user'] = $this->db->get('u_users')->result();



          $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
        }

  //----------------------------------------------------------------------------------------------------------

        public function get_default_acc(){
          $a = $this->utility->get_acc('cash_acc',$this->sd['bc']);

          $sql = "SELECT * FROM m_account WHERE CODE  = '$a'";
          $query = $this->db->query($sql);

          if($query->num_rows()>0){
           $b = $query->result();
         }else{
           $b =2;
         }
         echo json_encode($b);
       }




  //--------------------------- 2016-10-22- ------------------------------------


       public function set_paying_acc(){
        if ($_POST['type'] == "cash"){  $a['acc_drop_down'] = $this->get_cash_acc($this->sd['bc']);}
        if ($_POST['type'] == "cheque"){$a['acc_drop_down'] = $this->get_bank_acc(); }
        echo json_encode($a);
      }


      public function get_cash_acc($bc = ""){

        if ($bc == ""){
          $bc = $_POST['bc'];
        }else{
          $bc = $bc;
        }

        $Q = $this->db->query("SELECT B.`cash_acc`,MA.description FROM `m_branches` B

          left join m_account MA on B.cash_acc = MA.code

          WHERE bc = '$bc' LIMIT 1");

        $T  = "<select name='from_acc' id='from_acc'>";
        
        if ($Q->num_rows() >0){
          $T .= "<option value=''>Select</option>";
          foreach ($Q->result() as $R) { 
            $T .= "<option value='".$R->cash_acc."'>".$R->cash_acc." - ".$R->description." </option>";
          }
        }else{
          $T .= "<option value=''>Cash account not added</option>";
        }
        
        $T .= "</select>";

        return $T;
      }





      public function load_vou(){

        $acc_code = $_POST['acc_code'];
        $nno      = $_POST['nno'];

        $Q = $this->db->query("SELECT * FROM `t_br_receipt_sum` RS WHERE RS.`receipt_type` = 'gl_receipt' AND RS.`nno` = '$nno' AND RS.`paid_acc` = '$acc_code' LIMIT 1 ");

        if ($Q->num_rows() > 0){
          $a['s']   = 1;
          $a['sum'] = $Q->row();
          $a['det'] = $this->db->query("

            SELECT 
            VD.`nno`,
            VD.`acc_code`,
            AC.`description` as `acc_desc`,
            VD.`amount`,
            VD.`v_bc`,
            B.`name` AS `bc_name`,
            VD.`v_class`,
            VC.`description` AS `v_class_desc`,
            VD.paid_acc,
            ACc.description as `paid_acc_desc`

            FROM `t_voucher_gl_det` VD 

            LEFT JOIN `m_account` AC ON VD.`acc_code` = AC.`code`
            LEFT JOIN `m_account` ACc ON VD.`paid_acc` = ACc.`code` 

            LEFT JOIN `m_branches` B ON VD.`v_bc` = B.`bc`
            LEFT JOIN `m_voucher_class` VC ON VD.`v_class` = VC.`code`

            WHERE VD.`nno` = '$nno' AND VD.`paid_acc` = '$acc_code' ")->result();

          if ($Q->row()->type == "cheque"){

            $a['chq'] = $this->db->where(array('trans_code' => 48,'trans_no' => $nno,'issued_to_acc' => $acc_code))->get('t_cheque_issued')->row();

          }else{
            $a['chq'] = 0;
          }


          $this->load->model('user_permissions');        
          
          $a['l1_auth'] = intval($this->user_permissions->is_auth('level_1_authorization'));
          $a['l2_auth'] = intval($this->user_permissions->is_auth('level_2_authorization'));

        }else{
          $a['s']   = 0;
        }

        echo json_encode($a);
      }


      public function get_bank_acc(){

        $Q = $this->db->query("SELECT * FROM m_account MA WHERE MA.`is_control_acc` = 0 AND MA.`is_bank_acc` = 1 ");

        $T  = "<select name='from_acc' id='from_acc' style='width:100%' class='input_text_regular_ftr'>";

        if ($Q->num_rows() >0){
          $T .= "<option value=''>Select</option>";
          foreach ($Q->result() as $R) { 
            $T .= "<option value='".$R->code."'>".$R->description." </option>";
          }
        }else{
          $T .= "<option value=''>Bank accounts not added</option>";
        }

        $T .= "</select>";

        return $T;

      }

      private function max_vou_app(){

        return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM `t_gen_vou_app_rqsts_sum`")->row()->max_no;

      }



      public function set_bank_branch(){

       /* $bank = $_POST['bank'];*/

        $q = $this->db->query("SELECT `code`,`description` FROM `m_bank_branch` WHERE `bank`= '$bank' ORDER BY description");

        $t = "<select name='bank_branch' id='bank_branch'>";

        if ($q->num_rows() > 0){

          $t .= '<option value="">Select</option>';
          foreach ($q->result() as $r) {
            $t .= '<option value="'.$r->code.'">'.$r->description.'</option>';
          }
        }else{
          $t .= '<option value="">No branches added</option>';
        }

        $t .= "</select>";

        $a['branch'] = $t;

        echo json_encode($a);

      }


      public function check_cheque_exist(){

        /*$bank        = $_POST['bank'];
        $bank_branch = $_POST['bank_branch'];
        $cheque_no   = $_POST['cheque_no'];*/
        $account     = $_POST['account'];

        $n = $this->db->query(" SELECT cheque_no 
          FROM `t_cheque_received` 
          WHERE bank = '$bank' AND branch = '$bank_branch' 
          AND cheque_no = '$cheque_no' AND account = '$account'
          LIMIT 1 ")->num_rows();

        if ( $n > 0 ){
          echo 1;
        }else{
          echo 0;
        }


      }



      public function load_receipt(){

        $bc  = $this->sd['bc'];
        $nno = $_POST['nno'];

        $q = $this->db->query(" SELECT s.*, CONCAT(acc.code, ' ',  acc.`description`) AS `acc_txt` FROM t_br_receipt_sum s
          LEFT JOIN m_account acc ON s.`paid_to_acc` = acc.`code`
          WHERE s.bc = '$bc' AND s.nno = '$nno' LIMIT 1 ");

        if ($q->num_rows() > 0){

          $qq       = $this->db->query("SELECT D.`nno`, D.`acc_code`, AC.`description` AS `acc_desc`, D.`amount`, D.`v_bc`, D.`v_class`, D.`is_dr_acc` FROM `t_br_receipt_det` D LEFT JOIN `m_account` AC ON D.`acc_code` = AC.`code` WHERE bc = '$bc' AND nno = '$nno' ");

          $a['sum'] = $q->row();
          $a['det'] = $qq->result();
          $a['s']   = 1;

        }else{

          $a['s'] = 0;

        }

        echo json_encode($a);

      }



      public function validation(){

        $status         = 1;
        $account_update = $this->account_update(0);
        
        if ($account_update != 1){
          return "Invalid account entries";
        }
        
        return $status;

      }




    }