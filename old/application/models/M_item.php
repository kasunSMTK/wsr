<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_item extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){
        $a['code_max'] = $this->getCodeMax();
        $a['category'] = $this->getCategory();
        $a['load_list'] = $this->load_list();
		return $a;
    }

    public function getItemsForBulk(){

        $q      = $_GET['term'];        
        $query  = $this->db->query("SELECT `itemcode`,`itemname` FROM `r_items` WHERE (`itemcode` LIKE '%$q%' OR `itemname` LIKE '%$q%') ");
        $ary    = array();    
        
        foreach($query->result() as $r){ $ary [] = $r->itemcode . " - ". $r->itemname ; }    
        echo json_encode($ary);
    }

    public function getItemsByCat(){               
        $cat_code = $_POST['cat_code'];
        $query = $this->db->query("SELECT `itemcode`,`itemname` FROM `r_items` WHERE `itemcate` = '$cat_code'");                        

        $opt = "<option value=''>Select Item</option>";
        
        foreach($query->result() as $r){ 
            $opt .= "<option value='".$r->itemcode."'>".$r->itemcode." - ".$r->itemname."</option>"; 
        }            
        echo $opt;
    }
    
    public function save(){

        if (isset($_POST['bulk_item_code'])){
            $_POST['bulk_item_code'] = 1;
        }else{
            $_POST['bulk_item_code'] = 0;
        }

        if ($_POST['hid'] == "0"){
            unset($_POST['hid']);
        	$Q = $this->db->insert("r_items",$_POST);
        }else{
            unset($_POST['code']);
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("itemcode",$hid)->update("r_items",$_POST);
        }

        if ($Q){
            $d = 1;
        }else{
            $d = 0;
        }


        $a['d'] = $d;
        $a['itemcate'] = $_POST['itemcate'];

        echo json_encode($a);
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $code = $_POST['code'];

       if ($this->db->query("DELETE FROM`r_items` WHERE itemcode = '$code' ")){
            echo 1;
       }else{
            echo 0;
       }
    }

    public function load_list(){

        if (isset($_POST['itemcate'])){
            $itemcate = $_POST['itemcate'];
        }else{
            $itemcate = "";
        }

        if ($itemcate == ""){
            $Q = $this->db->query("SELECT * FROM `r_items` ORDER BY `itemcode` DESC LIMIT 5 ");    
        }else{
            $Q = $this->db->query("SELECT * FROM `r_items` WHERE `itemcate` = '$itemcate' ORDER BY `itemcode`");    
        }

        
        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->itemcode . "</td><td class='D'>-</td><td class='Des'>" .$R->itemname. "</td><td class='ED'><a href=# onClick=setEdit('".$R->itemcode."')>Edit</a> | <a href=# onClick=setDelete('".$R->itemcode."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "<table>";
        $a['T'] = $T;        
        $a['max_no'] = $this->getCodeMax();
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $code = $_POST['code'];
        $R = $this->db->query("SELECT * FROM `r_items` WHERE itemcode = '$code' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `r_items` WHERE (itemcode like '%$k%' OR itemname like '%$k%') LIMIT 10 ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->itemcode . "</td><td class='D'>-</td><td class='Des'>" .$R->itemname. "</td><td class='ED'><a href=# onClick=setEdit('".$R->itemcode."')>Edit</a> | <a href=# onClick=setDelete('".$R->itemcode."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No area added</center>";
        }
        $T .= "<table>";
    
        echo $T;
            
    }     

    public function getCategory(){
        $Q = $this->db->query("SELECT * FROM `r_itemcategory` ORDER BY code");
        if ($Q->num_rows() > 0){
            $T = "<SELECT id='itemcate' name='itemcate' class='input_ui_dropdown'><option value=''>Select Item Category</option>";
            foreach ($Q->result() as $R) { $T .= "<option value='".$R->code."'>".$R->code." - ".$R->des."</option>";}
            $T .= "</SELECT>";
            return $T;
        }else{
            return $T;
        }
    } 

    public function getCodeMax(){        
        return $this->db->query("SELECT IFNULL(MAX(itemcode)+1,1) AS `max_no` FROM `r_items`")->row()->max_no;
    }

    public function loadItemByCategory(){
        $itemcate = $_POST['itemcate'];
        $Q = $this->db->query("SELECT * FROM `r_items` WHERE `itemcate` = '$itemcate'");
        $T = "";        
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= $R->itemcode . " - " .$R->itemname. "<br><a href=# onClick=setEdit('".$R->itemcode."')>Edit</a> | <a href=# onClick=setDelete('".$R->itemcode."')>Delete</a><br><br>";
            }
        }else{
            $T = "<center>No items found</center>";
        }    
        
        $a['T'] = $T;            
        $a['max_no'] = $this->getCodeMax();

        echo json_encode($a);
    }

}