<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_partpay_cancel extends CI_Model {

	private $sd;
	private $mtb;
    private $max_no;

	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
	}

	public function base_details(){		
		$a['current_date']  = $this->sd['current_date'];
        $a['mxno']          = ''; 
        return $a;
    }

    public function save(){

        $bc         = $_POST['bc'];
        $billno     = $_POST['billno'];
        $cus_serno  = $_POST['cus_serno'];
        $oc         = $this->sd['oc'];

        $this->db->trans_begin();
        
        // Reverse if any advance settlment entry exist from redeem process
        
        $this->db->query("DELETE FROM t_loan_advance_customer WHERE billno   = $billno AND `mod_tr_no` = (SELECT L.`transeno` FROM t_loantranse_re_fo L WHERE L.transecode = 'R' AND L.`billno` = $billno LIMIT 1) ");

        // Copy pawning / Part pay entries for first tables
        // Find and dont copy in $Q3 all related entries came with redeem process 

        $Q1 = $this->db->query("INSERT INTO t_loan       SELECT * FROM t_loan_re_fo L   WHERE L.`billno` = $billno LIMIT 1 ");
        $Q2 = $this->db->query("INSERT INTO t_loanitems  SELECT * FROM t_loanitems_re_fo L WHERE L.`billno` = $billno ");
        $Q3 = $this->db->query("INSERT INTO t_loantranse SELECT * FROM t_loantranse_re_fo L WHERE NOT L.transecode = 'R' AND L.`billno` = $billno ");

        if ($Q1 && $Q2 && $Q3){
            $Q1 = $this->db->query("DELETE FROM t_loan_re_fo           WHERE `billno` = $billno LIMIT 1 ");
            $Q2 = $this->db->query("DELETE FROM t_loanitems_re_fo      WHERE `billno` = $billno ");
            $Q3 = $this->db->query("DELETE FROM t_loantranse_re_fo     WHERE `billno` = $billno ");
        }

        // Reupdate customer pawning history table
        $this->db->query("  UPDATE t_cus_pawn_details_sum PDS SET PDS.`redeemed` = PDS.`redeemed` - 1 WHERE PDS.`customer_id` = '$cus_serno' LIMIT 1 ");
    
        // remove account entries that added from redeem process

        $Q4 = $this->db->query("SELECT `loanno`, `trans_no`, `trans_code`, CURRENT_DATE , `dr_amount`, `cr_amount`, `acc_code`, `description`, '$oc', `bc`, `billno`, `billtype`, `entry_code` FROM t_account_trans acc WHERE acc.`bc` = '$bc' AND acc.`trans_code` = 2 AND  acc.`billno` = $billno ");

        if ($Q4->num_rows()){

            foreach ($Q4->result() as $r) {
                
                if ( $r->dr_amount == 0){
                    $dr_a = $r->cr_amount;
                    $cr_a = 0;
                }else{
                    $cr_a = $r->dr_amount;
                    $dr_a = 0;
                }

                $this->db->query("INSERT INTO `t_account_trans` (`loanno`,    `trans_no`, `trans_code`,   `ddate`,    `dr_amount`,    `cr_amount`,    `acc_code`, `description`,  `oc`,   `bc`,   `billno`,   `billtype`, `entry_code`) VALUES($r->loanno, $r->trans_no,   $r->trans_code, CURRENT_DATE ,  $dr_a   ,  $cr_a,  $r->acc_code, '".$r->description." (canceled)', $oc,'$r->bc', $r->billno,'$r->billtype','$r->entry_code') ");                
            }

        }


        if ($this->db->trans_status() === FALSE){
            
            $this->db->trans_rollback();            
            $a['s'] = 0;

        }else{
            
            $this->db->trans_commit();    	    
            $a['s'] = 1;
            
        }

    	echo json_encode($a);	
    }

    public function getRedeemedBill(){

        $billno = $_POST['billno'];

        $Q2 = $this->db->query("SELECT L.billno, L.bc,nicno,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,L.cus_serno,C.`customer_id`,L.`stamp_fee`,L.int_cal_changed,L.`am_allow_frst_int`,  (SELECT IF (COUNT(transecode) > 0, transecode ,L.status) FROM t_loantranse_re_fo WHERE `billno` = '$billno' AND transecode NOT IN('P','A','ADV')) AS `status`,L.`int_paid_untill`,L.fmintrest FROM `t_loan_re_fo` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '$billno' LIMIT 1 ");
        
        if ($Q2->num_rows()>0){
            
            $a['loan_sum'] = $Q2->row();
            $loan_no = $Q2->row()->loanno;            
        
            $this->load->model("calculate");
            $this->load->model("t_new_pawn");
            $this->load->model("m_billtype_det");

            //$a['int'] = $this->calculate->interest($loan_no,"json","", $a['loan_sum'] ,0);
            //$a['customer_advance'] = $this->calculate->customer_advance($Q2->row()->customer_id,$billno);
            $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;
            //$a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);            
            $a['s'] = 1;          

        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }










}
