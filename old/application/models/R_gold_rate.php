<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_gold_rate extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){        
        $a['load_list'] = $this->load_list();
        $a['gold_rate_chage_det'] = $this->gold_rate_chage_det();
		return $a;
    }

    public function autocomplete(){               
        $q = $_GET['term'];
        $query = $this->db->query("SELECT id, REPLACE(REPLACE(printval, ' ', ''), '-', '~') AS printval ,goldrate FROM r_gold_rate WHERE (`goldcatagory` LIKE '%$q%' OR `printval` LIKE '%$q%')");                
        $ary = array();    
        
        foreach($query->result() as $r){ 
            $ary [] = $r->id . " - ". $r->printval . " - " . $r->goldrate ; 
        }
        
        echo json_encode($ary);
    }
    
    public function save(){

        $_POST['oc'] =$this->sd['oc'];

        if ($_POST['hid'] == "0"){
            unset($_POST['hid']);
        	$Q = $this->db->insert("r_gold_rate",$_POST);
        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);

            $Q = $this->db->query(" INSERT INTO r_gold_rate_change_log
                                    
                                    SELECT 
                                    `id`,
                                    `goldcatagory`,
                                    `goldrate`,
                                    '".$_POST['goldrate']."',
                                    `printval`,
                                    `action_date`,
                                    `oc`,
                                    `isgem`,
                                    `assetrate`,
                                    '".$_POST['assetrate']."',
                                    `marketrate`,
                                    '".$_POST['marketrate']."',
                                    NOW(),
                                    '".$this->sd['oc']."'

                                    FROM r_gold_rate WHERE id = $hid LIMIT 1 ");


            $Q = $this->db->where("id",$hid)->update("r_gold_rate",$_POST);
        }

        if ($Q){

            $a['history_log'] = $this->gold_rate_chage_det();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $id = $_POST['id'];

       if ($this->db->query("DELETE FROM`r_gold_rate` WHERE id = '$id' ")){
            echo 1;
       }else{
            echo 0;
       }
    }

    public function load_list(){
        $Q = $this->db->query("SELECT * FROM `r_gold_rate` ORDER BY `id` DESC ");
        $T = "<table border=1>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr>

                <td class='Cod'></td>
                <td class='D'></td>
                <td class='Des'>" .$R->goldcatagory. " (" .$R->goldrate.")</td>
                <td class='ED'><a href=# onClick=setEdit('".$R->id."')>Edit</a> | <a href=# onClick=setDelete('".$R->id."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
        $T .= "</table>";    
        return $T;
    }
    
    public function set_edit(){
        $id = $_POST['id'];
        $R = $this->db->query("SELECT * FROM `r_gold_rate` WHERE id = '$id' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `r_gold_rate` WHERE (id like '%$k%' OR printval like '%$k%') LIMIT 10 ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->id . " </td><td class='D'>-</td><td class='Des'> " .$R->goldcatagory. "</td><td class='ED'><a href=# onClick=setEdit('".$R->id."')>Edit</a> | <a href=# onClick=setDelete('".$R->id."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No data found</center>";
        }
        $T .= "</table>"; 
    
        echo $T;
            
    }

    public function gold_rate_chage_det(){

        $Q = $this->db->query(" SELECT L.*, U.`discription` AS `oc` 
                                FROM r_gold_rate_change_log L
                                JOIN u_users U ON L.`changed_by` = U.`cCode`
                                ORDER BY L.`changed_date` DESC");

        $T = "";

        if ( $Q->num_rows() > 0 ){

            foreach($Q->result() as $R){

                $T .= " <tr>            
                            <td>".$R->goldcatagory."</td>
                            <td>".$R->old_goldrate."</td>
                            <td>".$R->new_goldrate."</td>
                            <td>".$R->printval."</td>
                            <td>".$R->old_assetrate."</td>
                            <td>".$R->new_assetrate."</td>
                            <td>".$R->old_marketrate."</td>
                            <td>".$R->new_marketrate."</td>
                            <td>".$R->changed_date."</td>
                            <td>".$R->oc."</td>
                        </tr>";

            }

        }else{

            $T .= "<tr><td colspan='10' align='center'>Gold rate change history log is empty</td></tr>";

        }

        return $T;

    }


}