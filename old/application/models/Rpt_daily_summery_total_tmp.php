<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_daily_summery_total extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function PDF_report($_POST_){

		$bc = $_POST_['bc'];
		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];

		if ($bc == ""){
			$QBC = "";			
		}else{
			$QBC = "L.`bc`='$bc' AND ";
		}

		$Q  = $this->db->query("SELECT 

			B.`name` AS `bc_name`,
			a.bc, 
			
			SUM(a.pawn_amount) AS `pawn_amount`,
			SUM(a.int_amount) AS `int_amount`,
			SUM(a.redeem_amount) AS `redeem_amount`,			
			SUM(a.total_packets) AS `total_packets`,
			SUM(a.total_stock) AS `total_stock`,
			SUM(a.total_weight) AS `total_weight`

 FROM (


SELECT 
    
    L.bc,
    SUM(IF (LT.`transecode` = 'P',LT.`amount`,0)) AS `pawn_amount` ,
    (SUM( IF (LT.`transecode` = 'P', L.`fmintrest`,0)  ) + SUM(IF (LT.`transecode` = 'A',LT.`amount`,0)) + SUM(IF (LT.`transecode` = 'R',LT.redeem_int,0))) AS `int_amount`,
    0 AS `redeem_amount`,
    0 AS `total_stock`,
    0 AS `no_of_items`,
    0 AS `total_packets`,
    0 AS `total_weight`
  
FROM `t_loantranse` LT 
JOIN `t_loan` L 	  ON LT.`loanno` = L.`loanno` AND LT.`billno` = L.`billno` 
WHERE $QBC L.`ddate` BETWEEN '$fd' AND '$td' AND LT.`transecode` = 'P'
GROUP BY L.`bc`

/*--------------- */ UNION ALL /* ------------- */

SELECT 
    
    L.bc,
    SUM(IF (LT.`transecode` = 'P',LT.`amount`,0)) AS `pawn_amount` ,
    (SUM( IF (LT.`transecode` = 'P', L.`fmintrest`,0)  ) + SUM(IF (LT.`transecode` = 'A',LT.`amount`,0)) + SUM(IF (LT.`transecode` = 'R',LT.redeem_int,0))) AS `int_amount`,
    0 AS `redeem_amount`,
    0 AS `total_stock`,
    0 AS `no_of_items`,
    0 AS `total_packets`,
    0 AS `total_weight`
  
FROM `t_loantranse_re_fo` LT 
JOIN `t_loan_re_fo` L 	  ON LT.`loanno` = L.`loanno` AND LT.`billno` = L.`billno` 
WHERE $QBC L.`ddate` BETWEEN '$fd' AND '$td' AND LT.`transecode` = 'P'
GROUP BY L.`bc` 

/*--------------- */ UNION ALL /* ------------- */

SELECT
L.`bc`,
SUM(IF (LT.`transecode` = 'P',LT.`amount`,0)) AS `pawn_amount` ,
(SUM(IF (LT.`transecode` = 'R',LT.redeem_int,0))) AS `int_amount`,
SUM(IF (LT.`transecode` = 'R',LT.`amount`,0)) AS `redeem_amount`,
0 AS `total_stock`,	
0 AS `no_of_items`,
0 AS `total_packets`,
0 AS `total_weight`

FROM `t_loantranse_re_fo` LT	
JOIN `t_loan_re_fo` 	  L 	ON LT.`loanno` = L.`loanno` AND LT.`billno` = L.`billno`

WHERE $QBC LT.`transecode` = 'R' AND LT.`ddate` BETWEEN '$fd' AND '$td'
GROUP BY L.`bc`

 /*--------------- */ UNION ALL /* ------------- */

SELECT 
AA.bc,
0 AS `pawn_amount`,
0 AS `int_amount`,	
0 AS `redeem_amount`,	
SUM(AA.goldweight) AS `total_stock`,
0 AS `no_of_items`,
0 AS `total_packets`,
SUM(AA.pure_weight) AS `total_weight`

 FROM (

SELECT L.bc,L.`loanno`, L.`billno`,LI.pure_weight, LI.goldweight, LT.transecode AS `transecode`
FROM `t_loan` L

JOIN (SELECT lt.`bc`,GROUP_CONCAT( lt.transecode) AS `transecode` FROM `t_loantranse` lt GROUP BY lt.`billno`) LT ON L.`loanno` = L.`loanno` AND LT.`bc` = L.`bc`
JOIN (SELECT li.bc,li.`loanno`, SUM(li.`pure_weight`) AS `pure_weight`,SUM(li.`goldweight`) AS `goldweight` FROM `t_loanitems` li GROUP BY li.`loanno` ) LI ON L.`loanno` = LI.loanno AND LI.bc = L.`bc`

WHERE $QBC L.`ddate` <= '$td' AND NOT LT.transecode IN ('R')

GROUP BY L.`billno`

UNION ALL 

SELECT L.bc,L.`loanno`,L.`billno`,LI.pure_weight, LI.goldweight,LT.transecode AS `transecode`
FROM `t_loan_re_fo` L

JOIN (SELECT lt.bc, GROUP_CONCAT( lt.transecode) AS `transecode` FROM `t_loantranse_re_fo` lt GROUP BY lt.`billno`) LT ON L.`loanno` = L.`loanno` AND LT.`bc` = L.`bc`
JOIN (SELECT li.bc,li.`loanno`, SUM(li.`pure_weight`) AS `pure_weight`,SUM(li.`goldweight`) AS `goldweight` FROM `t_loanitems_re_fo` li GROUP BY li.`loanno` ) LI ON L.`loanno` = LI.loanno AND LI.bc = L.`bc`

WHERE $QBC L.`ddate` BETWEEN '$fd' AND '$td' 

GROUP BY L.`billno`

) AA WHERE NOT AA.transecode LIKE '%R%'

GROUP BY AA.bc




/*--------------- */ UNION ALL /* ------------- */
















SELECT L.bc,0 AS `pawn_amount`,0 AS `int_amount`,0 AS `redeem_amount`,0 AS `total_stock`, 0 AS `no_of_items`, 
0 AS `total_packets`,0 AS `total_weight`
FROM `t_loan` L 
JOIN `t_loanitems` LI ON L.`loanno` = LI.`loanno` AND L.`bc` = LI.`bc`
WHERE $QBC L.`ddate` <= '$td'
GROUP BY L.`bc`

UNION ALL

SELECT bc,0 AS `pawn_amount`,0 AS `int_amount`,	0 AS `redeem_amount`,0 AS `total_stock`,0 AS `no_of_items`, 
SUM(`total_packets`) AS `total_packets`,0 AS `total_weight` 

FROM (
SELECT 

L.bc,
0 AS `pawn_amount`,
0 AS `int_amount`,	
0 AS `redeem_amount`,
0 AS `total_stock`,	

1 AS `total_packets`,
0 AS `total_weight` 


FROM `t_loan` L 

JOIN `t_loanitems` LI ON L.`loanno` = LI.`loanno` AND L.`bc` = LI.`bc`

WHERE $QBC L.`ddate` <= '$td'
GROUP BY L.`bc`,L.`billno` ) AA

GROUP BY AA.`bc`
























) a

LEFT JOIN m_branches B ON a.bc = B.`bc`

GROUP BY a.bc  ");
		

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;
			$r_data['barnch']= $this->db->select("name")->where("bc",$bc)->get("m_branches")->row()->name;
			$r_data['bc'] = $QBC;
			$this->load->view($_POST['by'].'_'.'pdf',$r_data);
		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}