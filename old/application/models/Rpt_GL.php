<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_GL extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }

    function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,'XL');        
    }
    
    function PDF_report($a,$rt = 'PDF'){

        ini_set('memory_limit', '1024M');

        $process_start_time = time();

        if($_POST['bc_arry'] === null){
            $BC = " ";
            //$GL_BC_EACH = $this->db->query("SELECT bc,`name` AS `bc_name` FROM m_branches ORDER BY `name`");
        }else{
            
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            $BC = " ta.bc IN ($bc) AND ";
            //$GL_BC_EACH = $this->db->query("SELECT bc,`name` AS `bc_name` FROM m_branches WHERE bc IN ($bc) ORDER BY `name`");
        }

        if($_POST['GL_Accs'] === null){
            $GL_Accs = " ta.acc_code IN (SELECT `code` FROM `m_account`) AND ";
            //$GL_Accs_EACH = $this->db->query("SELECT `code`,`heading` FROM m_account_type WHERE is_ledger_acc = 1");
        }else{
            for ($n = 0 ; $n < count($_POST['GL_Accs']) ; $n++){            
                $gl_acc_ar[] = "'".str_replace(",","",$_POST['GL_Accs'][$n])."'";
            }

            $GL_Accs = $GL_Accs_each = implode(',', $gl_acc_ar);    
            $GL_Accs = " ta.acc_code IN ($GL_Accs) AND ";
            //$GL_Accs_EACH = $this->db->query("SELECT `code`,`heading` FROM m_account_type WHERE is_ledger_acc = 1 AND `code` IN ($GL_Accs_each)");
        }

        //echo $GL_Accs;
        //exit;


        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];        
        
        $q = $this->db->query("SELECT ta.`ddate`,ta.`acc_code`,ta.`trans_no`,ta.`dr_amount`,ta.`cr_amount`,mb.`name` AS branch_name,

            CONCAT( IFNULL(ta.description,''),' ',  IFNULL(CONCAT('- Chq no:',cis.`cheque_no`),'') , '-' , IFNULL(ma2.`description`,'') ,
    IFNULL(CONCAT(' : Class BC : ',mb2.`name`),' '), 
    IFNULL(CONCAT(ft.ref_no,' (', ft.to_acc,')', ma.`description` , ' : (' , ft.`from_acc`,')',ma3.`description` ) , '' ) 
        ) AS `description`, 

            aa.`description` AS acc_desc , ta.bc
            
            FROM t_account_trans ta 
                
                LEFT JOIN t_trans_code td ON td.`code`= ta.`trans_code` 
                LEFT JOIN m_branches mb ON mb.`bc`=ta.`bc`
                LEFT JOIN m_branches mb2 ON mb2.bc =ta.`v_bc`
                LEFT JOIN `t_fund_transfer_sum` ft ON ta.trans_code = 9 AND ta.trans_no = ft.no
                LEFT JOIN m_account aa ON ta.`acc_code` = aa.`code`

                LEFT JOIN m_account ma ON ft.`to_acc` = ma.`code`
                LEFT JOIN m_account ma3 ON ft.`from_acc` = ma3.`code`
                LEFT JOIN t_cheque_issued cis ON ta.`trans_code` = cis.`trans_code` AND ta.`trans_no` = cis.`trans_no` AND ta.`bc` = cis.`bc`
                LEFT JOIN m_account ma2 ON cis.`account` = ma2.`code`

            WHERE $BC $GL_Accs ta.`ddate` BETWEEN '$fd' AND '$td'

            ORDER BY ta.`acc_code`,ta.`bc`,ta.`ddate`,ta.`trans_no`");


        $qq = $this->db->query(" SELECT acc_code,SUM(dr_amount - cr_amount) AS `bal` 
                                 FROM t_account_trans ta
                                 WHERE $GL_Accs ta.ddate < '$fd'
                                 GROUP BY acc_code ");

        $bc_acc_op_bal = array();

        foreach ($qq->result() as $rr) {
            $bc_acc_op_bal[$rr->acc_code] = $rr->bal;
        }


        $process_end_time = time();

        if($q->num_rows()>0){
            
            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            $r_detail['bc_acc_op_bal'] = $bc_acc_op_bal;            
            $r_detail['time_spend'] = $process_end_time - $process_start_time;
            //$r_detail['GL_Accs_EACH'] = $GL_Accs_EACH->result();
            //$r_detail['GL_Bc_Each'] = $GL_BC_EACH->result();
            
            /*if ($rt == 'PDF'){
                $this->load->view($_POST['by'].'_'.'pdf',$r_detail);
            }else{
                $this->load->view($_POST['by'].'_'.'excel',$r_detail);
            }*/

            $this->load->view($_POST['by'].'_'.'excel',$r_detail);

        }else{
            echo "<script>alert('No data found');close();</script>";
        }        

    }   
    
}