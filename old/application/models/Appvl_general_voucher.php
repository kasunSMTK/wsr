<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class appvl_general_voucher extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;

        $a['vou_area_stat'] = $this->check_for_area_apply();
		return $a;
    }


    public function check_for_area_apply(){		
        
    	$oc = $this->sd['oc'];
    	
    	return $this->db->query("SELECT IF (ISNULL(U.area),0,IF (U.area = '',0,1)) AS `vou_area_stat` FROM u_users U WHERE U.`cCode` = '$oc' LIMIT 1")->row()->vou_area_stat;
    }    
    
}