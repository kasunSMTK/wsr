<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class appvl_fund_transfers extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;
        $a['date_change_allow'] = $this->sd['date_chng_allow'];
      	$a['backdate_upto'] = $this->sd['backdate_upto'];
      	$a['current_date'] = $this->sd['current_date'];
		return $a;
    }


    public function approve_cheque_withdraw_request(){

	    $no = $this->max_no = $_POST['no'];
	    
	    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 6 ){
            $a['s'] = 2;
            echo json_encode($a);
            exit;
        }

	    $_POST['hid'] = 0;
	    $_POST['date'] = $this->sd['current_date'];

	    $_POST['ln'] = "";
	    $_POST['bt'] = "";
	    $_POST['bn'] = "";

        $DATA['bc']                 = $_POST['bc'];
        $DATA['action_taken_by']    = $this->sd['oc'];
        $DATA['related_model']      = "fund transfer";
        $DATA['action_window']      = "t_fund_transfer#tabs-2";

	    
	    $this->db->trans_begin();    

	        $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 2 WHERE `no` = '$no' LIMIT 1 ");

	        $DATA['noti_description']   = "Cheque withdraw request approved";

	        $this->utility->add_notification($DATA);

	    if ($this->db->trans_status() === FALSE){
	        
	        $this->db->trans_rollback();
	        $a['s'] = "error";

	    }else{

	    	$this->utility->user_activity_log($module='Fund Transfer',$action='approve',$trans_code='9',$trans_no=$no,$note='Cheque withdraw request approved');
	        $this->db->trans_commit();        
	        $a['s'] = 1;

	    }

	    echo json_encode($a);

	}


	


	public function reject_cheque_withdraw_request(){
	    
	    $no = $_POST['no'];    

	    $DATA['bc']                 = $_POST['bc'];
        $DATA['action_taken_by']    = $this->sd['oc'];
        $DATA['related_model']      = "fund transfer";
        $DATA['action_window']      = "t_fund_transfer#tabs-2";
        $DATA['noti_description']   = "Cheque withdraw request rejected";
	    
	    $this->db->trans_begin();    

	        $this->db->query("UPDATE `t_fund_transfer_sum` 			SET `status` = 3 WHERE `no` = '$no' LIMIT 1 ");
	        $this->db->query("UPDATE `t_fnd_trnsfr_rqsts_chq_det` 	SET `status` = 3 WHERE `no` = '$no' LIMIT 1 ");
	        $this->utility->add_notification($DATA);
	        
	    if ($this->db->trans_status() === FALSE){
	        
	        $this->db->trans_rollback();
	        $a['s'] = "error";

	    }else{
	    	$this->utility->user_activity_log($module='Fund Transfer',$action='reject',$trans_code='9',$trans_no=$no,$note='Cheque withdraw request rejected');
	        $this->db->trans_commit();        
	        $a['s'] = 1;

	    }

	    echo json_encode($a);

	}


	public function approve_BtoB_rqu(){

	    $no = $this->max_no = $_POST['no'];
	    
	    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 6 ){
            $a['s'] = 2;
            echo json_encode($a);
            exit;
        }

	    $_POST['hid'] = 0;
	    $_POST['date'] = $this->sd['current_date'];

	    $_POST['ln'] = "";
	    $_POST['bt'] = "";
	    $_POST['bn'] = "";

        $DATA['bc']                 = $_POST['bc'];
        $DATA['action_taken_by']    = $this->sd['oc'];
        $DATA['related_model']      = "fund transfer";
        $DATA['action_window']      = "t_fund_transfer#tabs-2";

	    
	    $this->db->trans_begin();    

	        $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 10 WHERE `no` = '$no' LIMIT 1 ");

	        $this->account_update_BB(1);

	        $DATA['noti_description']   = "Bank to bank transfer request approved";

	        $this->utility->add_notification($DATA);

	    if ($this->db->trans_status() === FALSE){
	        
	        $this->db->trans_rollback();
	        $a['s'] = "error";

	    }else{

	    	$this->utility->user_activity_log($module='Fund Transfer',$action='approve',$trans_code='9',$trans_no=$no,$note='Bank to bank transfer request approved');
	        $this->db->trans_commit();        
	        $a['s'] = 1;

	    }

	    echo json_encode($a);

	}


	public function account_update_BB($condition) {
    
	    $R1 = $this->db->query("SELECT * FROM t_fund_transfer_sum S WHERE S.`no` = ".$this->max_no." LIMIT 1 ")->row();

	    $this->db->where("trans_no", $this->max_no);
	    $this->db->where("trans_code", 9);        
	    $this->db->where("bc", $this->sd['bc']);    
	    $this->db->delete("t_check_double_entry");

	    if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
	        $this->db->where('bc',$this->sd['bc']);
	        $this->db->where('trans_code',9);
	        $this->db->where('trans_no',$this->max_no);
	        $this->db->delete('t_account_trans');
	    }

	    $config = array(
	        "ddate" => $R1->added_date,
	        "time" => date('H:i:s'),
	        "trans_code" => 9,
	        "trans_no" => $this->max_no,
	        "op_acc" => 0,
	        "reconcile" => 0,
	        "cheque_no" => 0,
	        "narration" => "",
	        "ref_no" => ''
	    );


	    $this->load->model('account');
	    $this->account->set_data($config);

	    $acc_code1 = $R1->to_acc;

	    
	    $this->account->set_value2("Fund Receving ".$R1->from_bc."-->".$R1->to_bc, $R1->amount , "dr",       $acc_code1,$condition,"",       $_POST['ln'],$_POST['bt'],$_POST['bn'],"",$R1->from_bc,$R1->to_bc);

	    $acc_code2 = $R1->from_acc;
	    
	    
	    $this->account->set_value2("Fund Transfer ".$R1->to_bc."<--".$R1->from_bc, $R1->amount , "cr",       $acc_code2,$condition,"",       $_POST['ln'],$_POST['bt'],$_POST['bn'],"",$R1->from_bc,$R1->from_bc);

	    if($condition==0){

	        $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` 

	            WHERE  t.`cl`='C1'  

	            AND (t.`bc`='" . $this->sd['bc'] . "' OR t.`bc` = '".$R1->from_bc."' OR t.`bc` = '".$R1->to_bc."'   ) 

	            AND t.`trans_code`='9'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");

	        if ($query->row()->ok == "0") {
	            
	            $this->db->where("trans_no", $this->max_no);
	            $this->db->where("trans_code", 9);                            
	            $this->db->where("bc", $this->sd['bc']);
	            $this->db->delete("t_check_double_entry");
	            
	            return "0";
	        } else {
	            return "1";
	        }
	    }
	}

    
}