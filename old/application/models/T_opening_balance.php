<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class t_opening_balance extends CI_Model {

    private $sd;
    private $tb_sum;
    private $tb_det;
    private $tb_trance;
    private $tb_items;
    private $tb_cost_log;
    private $tb_branch;
    private $tb_acc_trance;
    private $tb_cus;
    private $h = 297;
    private $w = 210;
    private $mod='003';

    function __construct() {
        parent::__construct();

        $this->sd = $this->session->all_userdata();
        $this->load->database($this->sd['db'], true);
        $this->load->model('user_permissions');

        $this->tb_sum = $this->tables->tb['t_opening_bal_sum'];
        $this->tb_det = $this->tables->tb['t_opening_bal_det'];
        $this->tb_account_type = $this->tables->tb['m_account_type'];
        $this->tb_check_double_entry = $this->tables->tb['t_check_double_entry'];
        $this->tb_account = $this->tables->tb['m_account'];
        $this->tb_branch = $this->tables->tb['s_branches'];
        

        
        
        $line_no = $this->get_line_no();

        
    }

    public function base_details() {
        $this->load->model('m_option_setup');
        //$a['max_no'] = $this->max_no();
        $a['sd'] = $this->sd;
        $a['grid'] = $this->m_option_setup->get_grid(); 
        $a['open_bal_date'] =$this->utility->get_open_bal_date();
        $a['line_no'] = $this->get_line_no();
        $a['bc_list'] = $this->bc_list();
        return $a;
    }

    public function bc_list(){

        $q = $this->db->query('SELECT bc,`name` FROM m_branches ORDER BY `name`');

        $t = '';

        if ($q->num_rows() > 0){
            $t .= '<option value="">Select</option>';
            foreach ($q->result() as $r) {                    
                $t .= '<option value="'.$r->bc.'">'.$r->name.'</option>';
            }
        }else{
            $t .= '<option>No branches added</option>';
        }

        return $t;

    }

    private function max_no() {        
        $this->db->select_max("no");
        return $this->db->get($this->tb_sum)->first_row()->no + 1;
    }

    public function get_line_no(){
        $sql="SELECT count(code) as line_no FROM m_account";
        $query=$this->db->query($sql);

        return (int)$line_no = $query->row()->line_no+1;
    }

    public function validation() {
        $status = 1;
        
        $this->max_no = $this->max_no();
        
        $is_account = $this->validation->check_is_account2("0_",$_POST['grid_row']);
        if ($is_account != 1) {
            return $is_account;
        }
        $account_update = $this->account_update(0);
        if ($account_update != 1) {
            return "Invalid account entries";
        }
        
        return $status;
    }

    public function save() {
        
        $this->db->trans_begin();
        error_reporting(E_ALL); 
        function exceptionThrower($type, $errMsg, $errFile, $errLine) { 
          throw new Exception($errMsg); 
        }
        set_error_handler('exceptionThrower'); 
        try {


            $_POST_TMP['time']  = $_POST['time'];
            $_POST['time']      = date('H:i:s');

            
            $validation = $this->validation();
            
            if ($_POST['hid'] == "" || $_POST['hid'] == 0) {
                $this->max_no = $this->max_no();
            }else{
                $this->max_no = $_POST['hid'];            
            }
            
            if ($validation == 1) {
                $a = array(
                    "date" => $_POST['date'],
                    "no" => $this->max_no,
                    "ref_no" => $_POST['ref_no'],
                    "note" => $_POST['note'],
                    "dr_amount" => $_POST['tot_dr'],
                    "cr_amount" => $_POST['tot_cr'],
                    'bc' => $_POST['bc'],
                    'cl' => $this->sd['cl'],
                    "oc" => $this->sd['oc']
                );

                for ($i = 0; $i < $_POST['grid_row']; $i++) {
                    if ($_POST['h_' . $i] != "" && $_POST['h_' . $i] != "0") {
                        $a1[] = array(
                            "no" => $this->max_no,
                            "account_code" => $_POST['h_' . $i],
                            "type" => $_POST['4_' . $i],
                            "dr_amount" => $_POST['1_' . $i],
                            "cr_amount" => $_POST['2_' . $i],
                            "bc" => $_POST['bc'],
                            "cl" => $this->sd['cl'],
                        );
                    }
                }

                

                if ($_POST['hid'] == "" || $_POST['hid'] == 0) {
                    if($this->user_permissions->is_add('t_opening_balance')){
                        $a['time'] = $_POST['time'] = date('H:i:s');
                        $this->account_update(1);
                        $this->db->insert("t_opening_bal_sum", $a);
                        if (count($a1)) {
                            $this->db->insert_batch($this->tb_det, $a1);
                        }
                        $this->utility->user_activity_log($module='Opening Balance',$action='insert',$trans_code='97',$trans_no=$this->max_no,$note='');
                        echo $this->db->trans_commit();
                    }else{
                        $this->db->trans_commit();
                        echo "No permission to save records";
                    }    
                } else {
                    if($this->user_permissions->is_edit('t_opening_balance')){
                        $_POST['time'] = $a['time'] = $_POST_TMP['time'];
                        $this->account_update(1);
                        $this->set_delete();
                        
                        $this->db->where('cl', $this->sd['cl']);
                        $this->db->where('bc', $_POST['bc']);
                        $this->db->limit(1);
                        $this->db->update($this->tb_sum, $a);
                        if (count($a1)) {
                            $this->db->insert_batch($this->tb_det, $a1);
                        }
                        $this->utility->user_activity_log($module='Opening Balance',$action='update',$trans_code='97',$trans_no=$this->max_no,$note='');
                        echo $this->db->trans_commit();
                    }else{
                        $this->db->trans_commit();
                        echo "No permission to edit records";
                    }     
                }
            }else{
                echo $validation;
                $this->db->trans_commit();
            }
        }catch(Exception $e){ 
            $this->db->trans_rollback();
            echo $e->getMessage()."Operation fail please contact admin"; 
        }         
    }

    public function account_update($condition) {
        //$this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code",97);
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $_POST['bc']);
        $this->db->delete($this->tb_check_double_entry);

        if ($condition == 1) {
            if ($_POST['hid'] != "0" || $_POST['hid'] != "") {
                //$this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 97);
                $this->db->where("cl", $this->sd['cl']);
                $this->db->where("bc", $_POST['bc']);
                $this->db->delete("t_account_trans");
            }
        }

        $config = array(
            "ddate" => $_POST['date'],
            "time" => $_POST['time'],
            "trans_code" => 97,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => $_POST['ref_no']
        );

        $des = "OPBAL : " . $_POST['note'];
        $this->load->model('account');
        $this->account->set_data($config);

        for ($i = 0; $i < $_POST['grid_row']; $i++) {
            if ($_POST['h_' . $i] != "" && $_POST['h_' . $i] != "0") {
                if ($_POST['1_' . $i] > 0) {
                    $this->account->set_value2($des, $_POST['1_' . $i], "dr", $_POST['h_' . $i], $condition,"","","","",$_POST['bc']);
                }
                if ($_POST['2_' . $i] > 0) {
                    $this->account->set_value2($des, $_POST['2_' . $i], "cr", $_POST['h_' . $i], $condition,"","","","",$_POST['bc']);
                }
            }
        }
                    
        $this->db->query("UPDATE `t_account_trans`
        SET bc = '".$_POST['bc']."'
        WHERE   `entry_code` = '".$_POST['bc']."' 
        AND     `trans_code` = 97 
        AND     `trans_no`   = '".$this->max_no."' ");

        if ($condition == 0) {

            $query = $this->db->query("
                   SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok 
                   FROM `t_check_double_entry` t
                   LEFT JOIN `m_account` a ON t.`acc_code` = a.`code`
                   WHERE  t.`cl`='" . $this->sd['cl'] . "'  AND t.`bc`='" . $_POST['bc'] . "'  AND t.`trans_code`='97'  AND t.`trans_no` ='" . $this->max_no . "' AND 
                   a.`is_control_acc`='0'");

            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 97);
                $this->db->where("cl", $this->sd['cl']);
                $this->db->where("bc", $_POST['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }

    public function get_next_no() {
        if (isset($_POST['hid'])) {
            if ($_POST['hid'] == "0" || $_POST['hid'] == "") {
                $field = "no";
                $this->db->select_max($field);
                $this->db->where("cl", $this->sd['cl']);
                $this->db->where("bc", $_POST['bc']);
                return $this->db->get($this->tb_sum)->first_row()->$field + 1;
            } else {
                return $_POST['hid'];
            }
        } else {
            $field = "no";
            $this->db->select_max($field);
            $this->db->where("cl", $this->sd['cl']);
            $this->db->where("bc", $_POST['bc']);
            return $this->db->get($this->tb_sum)->first_row()->$field + 1;
        }
    }

    public function load() {

        $this->db->select(array($this->tb_sum . '.time',$this->tb_sum . '.no', $this->tb_sum . '.date', $this->tb_sum . '.is_cancel', $this->tb_sum . '.ref_no', $this->tb_sum . '.note'));
        //$this->db->where($this->tb_sum . '.no', $_POST['id']);
        $this->db->where('bc', $_POST['bc']);
        $this->db->where('cl', $this->sd['cl']);
        $this->db->limit(1);
        $a['sum'] = $this->db->get($this->tb_sum)->first_row();

        if (isset($a["sum"]->no)) {
            $this->db->select(array('account_code', $this->tb_account . '.description AS acc_des', 'dr_amount', 'cr_amount', $this->tb_det . '.type', $this->tb_account_type . '.heading'));
            $this->db->join($this->tb_account, $this->tb_account . ".code = " . $this->tb_det . ".account_code", "INNER");
            $this->db->join($this->tb_account_type, $this->tb_account_type . ".rtype = " . $this->tb_det . ".type", "INNER");
            $this->db->where($this->tb_det . ".no", $a["sum"]->no);
            $this->db->where('bc', $_POST['bc']);
            $this->db->where('cl', $this->sd['cl']);
            $this->db->group_by('account_code,auto_num');
            $this->db->order_by($this->tb_det . ".auto_num", 'ACS');
            $a['det'] = $this->db->get($this->tb_det)->result();
        }

        echo json_encode($a);
    }

    public function delete() {
        $this->db->trans_begin();
        error_reporting(E_ALL); 
        function exceptionThrower($type, $errMsg, $errFile, $errLine) { 
          throw new Exception($errMsg); 
        }
        set_error_handler('exceptionThrower'); 
        try { 
            if($this->user_permissions->is_delete('t_opening_balance')){

                $this->db->where("trans_no", $_POST['id']);
                $this->db->where("trans_code", '1');
                $this->db->where("cl", $this->sd['cl']);
                $this->db->where("bc", $_POST['bc']);
                $this->db->delete("t_account_trans");

                $this->db->where("no", $_POST['id']);
                $this->db->where("bc", $_POST['bc']);
                $this->db->where("cl", $this->sd['cl']);
                $this->db->limit(1);
                $this->db->update($this->tb_sum, array("is_cancel" => $this->sd['oc']));
                $this->utility->save_logger("DELETE",1,$this->max_no,$this->mod);
                echo $this->db->trans_commit();
            }else{
                echo "No permission to delete records";
                $this->db->trans_commit();
            }    
        }catch(Exception $e){ 
          $this->db->trans_rollback();
          echo "Operation fail please contact admin"; 
        }   

    }

    public function set_delete() {        
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $_POST['bc']);
        $this->db->delete($this->tb_det);
    }

      public function PDF_report() {

        $sql = "SELECT 
      `m_supplier`.`codex`, 
      `m_supplier`.`name`, 
      `m_supplier`.`address1`, 
      IFNULL (`m_supplier`.`address2`,'') address2,
      IFNULL (`m_supplier`.`address3`,'') address3, 
      IFNULL (`m_supplier`.`email`,'') email,
      IFNULL (`m_supplier_contact`.`tp`,'') tp FROM (`m_supplier`) 
      LEFT JOIN `m_supplier_contact` ON `m_supplier_contact`.`code`=`m_supplier`.`code` WHERE `m_supplier`.`code`='" . $r_detail['supplier'] . "' LIMIT 1";

        //$r_detail['suppliers'] = $this->db->query($sql)->result();

        $invoice_number= $this->utility->invoice_format($_POST['qno']);
        $session_array = array(
             $this->sd['cl'],
             $_POST['bc'],
             $invoice_number
        );
        $r_detail['session'] = $session_array;

        // $this->db->SELECT(array('serial_no', 'item'));
        // $this->db->FROM('t_serial');
        // $this->db->WHERE('t_serial.cl', $this->sd['cl']);
        // $this->db->WHERE('t_serial.bc', $_POST['bc']);
        // $this->db->WHERE('t_serial.trans_type', '3');
        // $this->db->WHERE('t_serial.trans_no', $_POST['qno']);
       // $r_detail['serial'] = $this->db->get()->result();


        $no = $_POST['qno'];
        $bc = $_POST['bc'];

        $sql1 = "SELECT 
                `m_cluster`.`description`,
                `m_cluster`.`code`,
                `m_branches`.`name`,
                `m_branches`.`bc`,
                `t_opening_bal_sum`.`date`,
                `t_opening_bal_sum`.`no`,
                `t_opening_bal_sum`.`ref_no` 
                 FROM
                `t_opening_bal_sum` 
                INNER JOIN `m_cluster` 
                ON `m_cluster`.`code` = `t_opening_bal_sum`.`cl` 
                INNER JOIN `m_branches` 
                ON `m_branches`.`bc` = `t_opening_bal_sum`.`bc` 
                WHERE `t_opening_bal_sum`.`no` = '$no' ";
        $sql2 = "SELECT 
                  `t_opening_bal_det`.`account_code`,
                  `m_account`.`description`,
                  `m_account_type`.`heading`,
                  `t_opening_bal_det`.`dr_amount`,
                  `t_opening_bal_det`.`cr_amount`,
                  `t_opening_bal_sum`.`date`
                   FROM `t_opening_bal_sum`
                   INNER JOIN
                  `t_opening_bal_det`
                   ON
                  `t_opening_bal_det`.`no`=`t_opening_bal_sum`.`no`
                   INNER JOIN
                  `m_account`
                   ON
                  `t_opening_bal_det`.`account_code`=`m_account`.`code`
                   INNER JOIN
                  `m_account_type`
                   ON
                  `t_opening_bal_det`.`type`=`m_account_type`.`rtype`
                   WHERE `t_opening_bal_sum`.`no`='$no'";

        $sql2.=" GROUP BY `t_opening_bal_det`.`account_code`, `t_opening_bal_det`.`auto_num` 
                 ORDER BY `t_opening_bal_det`.`auto_num`";

//var_dump($sql2);
//excit();
//$this->sd['cl']
//        $_POST['bc']
        $r_detail['qno'] = $_POST['qno'];
        $r_detail['jtype'] = $_POST['jtype'];
        $r_detail['jtype_desc'] = $_POST['jtype_desc'];
        $r_detail['page'] = $_POST['page'];
        $r_detail['header'] = $_POST['header'];
        $r_detail['orientation'] = $_POST['orientation'];
        $r_detail['type'] = $_POST['type'];

 
        $query = $this->db->query($sql1);
        $query2 = $this->db->query($sql2);


//        if ($query->num_rows > 0) {
//            $r_detail['det'] = $query->result();
//        } else {
//            $r_detail['det'] = 2;
//        }
        
        $this->db->select(array('name'));
        $r_detail['company'] = $this->db->get('m_company')->result();
        $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
        $this->db->where("cl", $this->sd['cl']);
        $this->db->where("bc", $_POST['bc']);
        $r_detail['branch'] = $this->db->get('m_branches')->result();

        $r_detail['opn_bl_head'] = $query->result();
        $r_detail['jrn_en_body'] = $query2->result();
        $r_detail['cl'] = $cluster = $this->sd['cl'];

        $r_detail['bc'] = $_POST['bc'];
//      echo $_POST['by'];
//         echo '<pre>'.print_r($r_detail,true).'</pre>';
//          exit;
        
        $this->load->view($_POST['by'] . '_' . 'pdf', $r_detail);
        
    }

    public function get_account(){
        $this->db->select(array('m_account.code','m_account.description','m_account_type.heading','m_account_type.rtype'));
        $this->db->join('m_account_type', 'm_account_type.code = m_account.type', "INNER");
        $this->db->where("m_account.code",$this->input->post('code'));
        $this->db->where("is_control_acc",'0');
        $this->db->where("m_account_type.rtype !=",'1');
        $this->db->where("m_account_type.rtype !=",'2');
        

        $this->db->limit(1);
        $query=$this->db->get('m_account');
        if($query->num_rows() > 0){
            $data['a']=$query->result();
        }else{
            $data['a']=2;
        }
        echo json_encode($data);
    }

}
