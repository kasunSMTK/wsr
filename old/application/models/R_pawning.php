<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class r_pawning extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1; 
        $a['IfPDF'] = $this->utility->IfPDF();
        $a['isAdmin'] = $this->sd['isAdmin'];

        $this->load->model('m_branches');
        $a['bc_dropdown'] = $this->m_branches->select_own_bc();
        $a['billtype_dropdown'] = $this->billtype_dropdown();
        $a['expense_dropdown'] = $this->expense_dropdown();
        $a['bc_dropdown_multi'] = $this->m_branches->bc_dropdown_multi();

        $a['is_bc_user'] = $this->m_branches->is_bc_user();

        $a['set_AI_gold_rate_inputs'] = $this->set_AI_gold_rate_inputs();
        $a['set_ecd_dropdown'] = $this->set_ecd_dropdown();

		return $a;
    }   


    public function billtype_dropdown(){


        $T = "<select id='r_billtype' name='r_billtype'  class='input_ui_dropdown input_text_regular_new_pawn' style='border:2px solid Green; padding:3px'>";

        $q = $this->db->query("SELECT billtype FROM `r_bill_type_sum` WHERE is_active = 1 GROUP BY billtype");
        
        $T .= "<option value=''>All</option>";
        foreach ($q->result() as $r) {
            $T .= "<option value='".$r->billtype."'>".$r->billtype."</option>";            
        } $T .= "</select>";

        return $T;
    }


     public function expense_dropdown(){
        

        $Q = $this->db->query(" SELECT t.code,t.heading 
                                FROM m_account_type t 
                                LEFT JOIN m_account a ON a.code = t.code  
                                WHERE t.rtype = '2' AND NOT t.code = '2' ");

        if($Q->num_rows() > 1){
           
            $T = "<select id='r_expense' name='r_expense'  class='input_ui_dropdown input_text_regular_new_pawn' style='border:2px solid Green; padding:3px'>";
            $T .= "<option value=''>All</option>";
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->code."'>".$R->code." - ".$R->heading."</option>";
            }

             $T .= "</select>";
            return $T;
        }else{
            return "Expense Accounts Not found";
        }



        return $T;
    }

    

    public function getAcc(){

        $q      = $_GET['term'];        
        $query  = $this->db->query("SELECT `code`,`heading` FROM m_account_type WHERE is_ledger_acc = 1 AND CODE LIKE '%$q%' OR heading LIKE '%$q%'");
        $ary    = array();    
        
        foreach($query->result() as $r){ $ary [] = $r->code . " - ". $r->heading ; }    
        echo json_encode($ary);
    }
    


    public function set_AI_gold_rate_inputs(){

        $q = $this->db->query(" SELECT id,CONCAT(`printval`, ' (',`goldcatagory`,')') AS `desc` FROM `r_gold_rate` ");

        $t = '<table>';

        foreach ($q->result() as $r) {

            $t .= '<tr><td>'.$r->desc.'</td><td><input type="hidden" class="ccgr_id" value="'.$r->id.'"><input id="r_ccgr" class="input_text_regular_new_pawn amount ccgr_val" style="border:1px solid Green;" placeholder="Current market value" ></td></tr>';
        }

        $t .= '</table>';

        return $t;

    }


    public function set_ecd_dropdown(){

        $Q = $this->db->query(" SELECT `code`,`description` FROM m_voucher_class WHERE is_exp = 1 AND acc_cat = 2 ");

        if($Q->num_rows() > 1){
           
            $T = "<select id='ecd_select' name='ecd_select' class='input_ui_dropdown input_text_regular_new_pawn' style='border:2px solid Green; padding:3px'>";
            
            $T .= "<option value=''>All Classes</option>";
            
            foreach ($Q->result() as $R) {
                $T .= "<option value='".$R->code."'>".$R->code." - ".$R->description."</option>";
            }

             $T .= "</select>";
            return $T;
        }else{
            return "Expense Accounts Not found";
        }

        return $T;

    }


}