<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_cheque_status extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
        $a['chq_stat'] = $this->chq_stat();
		return $a;
    }



    public function chq_stat(){

        if (isset($_POST['json'])){

            $fd = $_POST['fd'];
            $td = $_POST['td'];
            $json = true;            

        }else{
            
            $fd = date('Y-m-d');
            $td = date('Y-m-d');
            $json = false;

        }

        $q = $this->db->query(" SELECT A.`no`,D.description AS `bank`,B.cheque_no,B.amount,C.`name` AS `bc_name`,
A.`date_time`,A.`status` AS `ss`,A.`to_bc`

FROM t_fund_transfer_sum A

JOIN t_fnd_trnsfr_rqsts_chq_det B ON A.`no` = B.`no`
JOIN m_branches C ON A.`to_bc` = C.`bc`
JOIN m_account D ON B.`bank` = D.`code`

WHERE A.`transfer_type` = 'cheque_withdraw' AND DATE(A.`date_time`) BETWEEN '$fd' AND '$td' 

ORDER BY A.`to_bc`,A.`date_time` DESC ");

        if ($q->num_rows() > 0){

            if ($json){
                $ttx = '<span class="title_hid_txt">(Approved)</span>';
            }else{
                $ttx = '';
            }


                $t  = '<table class="tbl_cheque_status" width="100%" id="table">';

                $s = $sx = '';

            foreach ($q->result() as $r) {

                if ($s != $r->to_bc){
                    $t .= '<tr class="row_bc" cl="'.str_replace(" ", "", $r->bc_name ).'">';
                    $t .= '<td colspan="2">'.$r->bc_name.'</td>';
                    $t .= '<td width="150"><input placeholder="Search cheque no" class="chq_stat_cchu_no_search" cl = "'.$r->to_bc.'_chq" ></td>';
                    $t .= '<td colspan="3" align="right"><input type="checkbox" value="'.$r->to_bc.'" class="print_bc"></td>';
                    $t .= '</tr>';
                    
                    $t .= '<tr class="'.str_replace(" ", "", $r->bc_name).' all_rows" style="display:none" bgColor="#f9f9f9">';
                    $t .= '<td>Fund Request No</td>';
                    $t .= '<td>Bank</td>';
                    $t .= '<td>Cheque No</td>';
                    $t .= '<td>Amount</td>';                
                    $t .= '<td>Status</td>';
                    $t .= '<td>Date amd Time</td>';
                    $t .= '</tr>';
                    
                    $s = $r->to_bc;
                    $sx= $r->to_bc."_chq";
                }                

                $t .= '<tr class="'.str_replace(" ", "", $r->bc_name).' all_rows" style="display:none">';
                $t .= '<td>'.$r->no.'</td>';
                $t .= '<td>'.$r->bank.'</td>';
                $t .= '<td class="'.$sx.'">'.$r->cheque_no.'</td>';
                $t .= '<td>'.$r->amount.'</td>';                
                
                $st = '';

                if ($r->ss == 1){
                    $st = '<span style="color:#000000">Branch waiting for head office respond</span>';
                }elseif ($r->ss == 2) {
                    $st = '<span style="color:green">Accepted by head office. Branch must print voucher</span>';
                }elseif ($r->ss == 3) {
                    $st = '<span style="color:red">Request reject by head office</span>';
                }elseif ($r->ss == 4) {
                    $st = '<span style="color:#333333">Voucher print from branch.</span>';
                }elseif ($r->ss == 6) {
                    $st = '<span style="color:red">Request canceled by head office</span>';
                }elseif ($r->ss == 10) {
                    $st = '<span style="color:green"><b>Transaction completed</b></span>';                
                }else{
                    $st = $r->ss;
                }


                $t .= '<td>'.$st.'</td>';


                $t .= '<td>'.$r->date_time.'</td>';
                $t .= '</tr>';

            }

                $t .= '</table>';

        }else{

            $t = 'No data found';

        }

        


        if ($json){
            $a['data'] = $t;
            echo json_encode($a);
        }else{
            return $t;
        }

    }
        
    
}