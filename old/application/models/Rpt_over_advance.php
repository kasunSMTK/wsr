<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_over_advance extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    ini_set('memory_limit','1024M');
    ini_set('max_execuation_time',900);

	/*echo "Report unavailable";
	exit;*/

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = "   AND L.bc IN ($bc) ";
        }
    }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND l.`billtype`='".$_POST['r_billtype']."'";
    }
   
    $sql="SELECT 

B.name as `bc`,
L.`ddate`,
L.`billno`,
C.`cusname` as `cus_name`,
C.`address` AS `cus_addr`,
@A:=L.`requiredamount` as requiredamount,
@B:=IFNULL(ADV.adv_bal,0) AS `adv_bal`,
@C:=acurd_int_cal_non_redeem(L.`bc`,L.`billno`,'$td') AS `accrued_interest`,
@D:=LI.market_value,
((@A-@B+@C)-@D)  AS `over_adv_amount`,
C.mobile,
U.discription as `app_by`


FROM t_loan L 

JOIN (SELECT billno, SUM(`value`) AS market_value FROM t_loanitems li GROUP BY li.`billno`) LI ON L.`billno` = LI.billno
LEFT JOIN (SELECT billno, SUM(cr_amount - dr_amount) AS `adv_bal` FROM `t_loan_advance_customer` GROUP BY billno ) ADV ON L.`billno` = ADV.billno
JOIN  m_customer C ON L.`cus_serno` = C.`serno`

JOIN m_branches B on L.bc = B.bc
LEFT JOIN (SELECT * FROM `t_approval_history` UNION ALL SELECT * FROM `t_approval`) AH ON AH.request_type = 'EL' AND L.by_approval = AH.auto_no
  LEFT JOIN u_users U ON AH.approved_by = U.cCode

WHERE L.`ddate` <= '$td' $bc

ORDER BY L.`bc`,L.`ddate`,billno;";
     

    $query = $this->db->query($sql);


    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}