<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_pawnings extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_,$r_type = "pdf"){

    ini_set('max_execution_time', 600 );
    ini_set('memory_limit', '384M');

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        if ( !is_array($_POST['bc_arry']) ){
            
            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
            
            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
            }

            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
        }

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = "   L.bc IN ($bc) AND ";
        }
    }


    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }


    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    $ftime = $_POST['ftime'];
    $ttime = $_POST['ttime'];

    $Qtime = "";

    if ( $this->sd['isAdmin'] == 1 ){
        if (!$ftime == ""){
            $Qtime = " AND TIME(L.`action_date`) BETWEEN '$ftime' AND '$ttime'";
        }
    }

    $Q = $this->db->query("SELECT * FROM (

SELECT L.int_discount, L.bc, B.`name` AS `bc_name`,L.`loanno`,  L.`billtype`,L.`billno`,L.`bt_letter`,  L.`ddate`,C.`cusname`,  C.`address`,C.`nicno`,  C.`mobile`,LI.`totalweight`,  LI.items AS `items`,L.`requiredamount`, LI.`totalpweight`,L.`fmintrest`,IFNULL(AH.`auto_no`,'') AS `app_no`, L.`action_date`
FROM `t_loan` L 
  
JOIN (  SELECT GROUP_CONCAT(CONCAT(i.`itemname`,'(',li.`pure_weight`,')')) AS `items`,li.`bc`,li.`billtype`,li.`billno` , SUM( li.pure_weight ) as `totalpweight`, SUM( li.goldweight ) as `totalweight`
    FROM `t_loanitems` li 
    JOIN `r_items` i ON li.`itemcode` = i.`itemcode`
    GROUP BY li.`bc`,li.`billtype`,li.`billno` 

) LI ON L.`bc` = LI.`bc` AND LI.`billtype` = L.`billtype` AND LI.`billno` = L.`billno`

JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
JOIN `m_branches` B ON L.`bc` = B.`bc`
LEFT JOIN t_approval_history AH ON L.`by_approval` = AH.`auto_no`

WHERE  $bc L.ddate BETWEEN '$fd' AND '$td' AND L.`status` = 'P' $billtype $Qtime



GROUP BY L.`bc`,L.`billtype`,L.`billno`

UNION ALL

SELECT L.int_discount,L.bc,B.`name` AS `bc_name`,L.`loanno`,  L.`billtype`,L.`billno`,L.`bt_letter`,  L.`ddate`,C.`cusname`,  C.`address`,C.`nicno`,  C.`mobile`,L.`totalweight`,  LI.items AS `items`,L.`requiredamount`, LI.`totalpweight`,L.`fmintrest`,IFNULL(AH.`auto_no`,'') AS `app_no`, L.`action_date`
FROM `t_loan_re_fo` L 
  
JOIN (  SELECT GROUP_CONCAT(CONCAT(i.`itemname`,'(',li.`pure_weight`,')')) AS `items`,li.`bc`,li.`billtype`,li.`billno` , SUM( li.pure_weight ) as `totalpweight`, SUM( li.goldweight ) as `totalweight`
    FROM `t_loanitems_re_fo` li 
    JOIN `r_items` i ON li.`itemcode` = i.`itemcode`
    GROUP BY li.`bc`,li.`billtype`,li.`billno` 

) LI ON L.`bc` = LI.`bc` AND LI.`billtype` = L.`billtype` AND LI.`billno` = L.`billno`

JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
JOIN `m_branches` B ON L.`bc` = B.`bc`
LEFT JOIN t_approval_history AH ON L.`by_approval` = AH.`auto_no`

WHERE  $bc L.ddate BETWEEN '$fd' AND '$td' AND L.`status` IN ('P','AM','R','RN','L','PO') $billtype  $Qtime




GROUP BY L.`bc`,L.`billtype`,L.`billno`

) aa


ORDER BY aa.bc, aa.loanno, aa.ddate


");

    if($Q->num_rows() > 0){

        $r_data['list'] = $Q->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;
        $r_data['isAdmin'] = $this->sd['isAdmin'];

        if ($r_type == "pdf"){            
            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{
            $this->load->view($_POST['by'].'_'.'excel',$r_data);
        }

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

    ini_set('memory_limit', '128M');

}





public function Excel_report()
    {
        $this->PDF_report($_POST,"Excel");
    }


}