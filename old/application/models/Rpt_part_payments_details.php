<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_part_payments_details extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
		return $a;
    }   

    public function PDF_report($_POST_){

        $bc = $_POST_['bc'];
        $fd = $_POST_['from_date'];
        $td = $_POST_['to_date'];
        
        // if ($bc == ""){
        //     $QBC = "";            
        // }else{
        //     $QBC = "LT.`bc`='$bc' AND ";            
        // }

        if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
            $QBC = ""; 
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
            $QBC = ""; 
        }else{
            $QBC = "   L.bc IN ($bc) AND ";
            $bc   = "   L.bc IN ($bc) AND ";
        }
    }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }
        
        $Q = $this->db->query("SELECT 

L.`loanno`,
LT.`billno`,
BT.`billtype`,
LI.date_time AS `action_date`,
LI.payments AS `amount`,
C.cusname,
C.address,
C.`nicno`,
C.`mobile`,
L.`totalweight`

FROM `t_loan` L 

JOIN `t_loantranse` LT ON L.`bc` = LT.`bc` AND L.`loanno` = LT.`loanno`
JOIN (SELECT GROUP_CONCAT(t_lt.`action_date`) AS `date_time` , GROUP_CONCAT(t_lt.amount) AS `payments`, t_lt.`loanno` FROM t_loantranse t_lt WHERE t_lt.`transecode` = 'A' GROUP BY t_lt.`loanno` ORDER BY action_date) LI ON L.`loanno` = LI.loanno
JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
JOIN `r_bill_type_sum` BT ON L.`billtype` = BT.`billtype`  AND L.`bc` = BT.`bc`

WHERE $QBC LT.`ddate` BETWEEN '$fd' AND '$td' $billtype 
GROUP BY LT.`loanno`
ORDER BY LT.`loanno`,LT.`action_date`");

        if($Q->num_rows() > 0){

            $r_data['list'] = $Q->result();
            $r_data['fd'] = $fd;
            $r_data['td'] = $td;

            $this->load->view($_POST_['by'].'_'.'pdf',$r_data);
        }else{
            echo "<script>location='default_pdf_error'</script>";
        }

    }
    
}