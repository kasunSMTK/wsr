    <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_interest_incme extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function Excel_report(){
    
    $data['from_date'] = $_POST['from_date'];
    $data['to_date'] = $_POST['to_date'];
    $date['bc_arry'] = $_POST['bc_arry'];

    $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);

    /*foreach ($_POST['bc_arry'] as $bc) {
        echo $bc."<br>";
    }

*/    $this->PDF_report($data,'XL');

}


public function PDF_report($_POST_,$rptType='PDF'){

    ini_set('max_execution_time', 300 );    
    
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    
    // $bc = $_POST['bc'];
    // if ($bc == ""){
    //     $QBC = "";        
    // }else{
    //     $QBC = "L.`bc`='$bc' AND ";        
    // }

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }    
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
            $QBC = "";
        }else{
            $QBC = "L.`bc`IN ($bc) AND ";
            $bc   = "   L.bc IN ($bc) AND ";
        }
    
        
    }

    

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }

    $sql = "SELECT a.bc, b.name,IFNULL(a.billtype,'B. Receipt Redeem') AS `billtype`, SUM(a.`pwning_int`) AS `pwning_int` , SUM(a.redeem_int) AS `redeem_int` , SUM(a.`pwning_int`) + SUM(a.redeem_int) AS `total` FROM (

SELECT bc,billtype,(cr_amount - dr_amount) AS `pwning_int`, 0 AS `redeem_int` FROM t_account_trans L
WHERE $QBC acc_code = '10101' AND ddate BETWEEN '$fd' AND '$td' $billtype

UNION ALL

SELECT bc,billtype, 0 ,(cr_amount - dr_amount) AS `redeem_int` FROM t_account_trans L
WHERE $QBC acc_code = '10102' AND ddate BETWEEN '$fd' AND '$td' $billtype

) a

JOIN m_branches b ON a.bc = b.bc 

GROUP BY a.bc , a.billtype";
           

    
    $query = $this->db->query($sql);


    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        if ($rptType == 'PDF'){
            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{
            $this->load->view($_POST['by'].'_'.'excel',$r_data);

            
        }

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

}