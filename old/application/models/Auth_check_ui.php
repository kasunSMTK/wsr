<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_check_ui extends CI_Model{

  private $sd;

  function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);    
    $this->load->model('user_permissions');
    
  }
  
  public function base_details(){
    $a['s'] = 0;
    return $a;
  }

}