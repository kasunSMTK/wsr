<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_part_payment extends CI_Model {
    
    private $sd;    
    private $max_no;
    
    function __construct(){
        parent::__construct();      
        $this->sd = $this->session->all_userdata();   
    }
    
    public function base_details(){     
        $a['current_date'] = $this->sd['current_date'];
        $a['max_no'] = ""; // $this->getPartPayTransNo();  
        $a['date_change_allow'] = $this->sd['date_chng_allow'];
        $a['bc_no'] = $this->sd['bc_no'];
        $a['backdate_upto'] = $this->sd['backdate_upto'];
        return $a;
    }
    
    public function save(){

        $a['s'] = 33;
        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this part payment bill and you might required re-login to continue";

        if (isset($this->sd['current_date'])){
            if (!$this->utility->is_valid_date($this->sd['current_date'])){
                echo json_encode($a);
                exit;
            }
        }else{            
            echo json_encode($a);
            exit;
        }   $a['s'] = ''; $a['msg_33'] = '';

        $this->db->trans_begin();
        $this->load->model("calculate");
        
        $loanno             = $_POST['loanno'];
        $five_days_int_cal  = $_POST['five_days_int_cal'];
        $action_date        = date('Y-m-d h:i:s');
        $a['a']             = 0;        
        $_POST['billno']    = $_POST['bc_no'].$_POST['billno'];
        $_POST['data']      = $_POST;
        
        unset($_POST['five_days_int_cal'],$_POST['bc_no']);

        if ($five_days_int_cal == 1){
            $_PT['onefive_day_int'] = 1; 
            $_PT['onefive_days']    = $_POST['no_of_int_cal_days']; // 'no_of_int_cal_days' consider if only 'five_days_int_cal' is 1
        }else{
            $_PT['onefive_day_int'] = 0; 
            $_PT['onefive_days']    = 0;
        }

        $_POST['onefive_day_int'] = $_PT['onefive_day_int'];
        $_POST['onefive_days'] = $_PT['onefive_days'];

        /*echo $_PT['onefive_day_int']."<br>";
        echo $_PT['onefive_days'];
        exit;*/

        $paying_amount  = floatval($_POST['amount']);
        $int_balance    = floatval($this->calculate->interest_balance($loanno,$five_days_int_cal));
        $adv_balance    = $this->calculate->customer_advance($_POST['customer_id'],$_POST['billno']);
        $adv_balance    = floatval($adv_balance['balance']);

        /*$x = $this->calculate-> get_int_paid_till_date($_POST['int_paid_untill'], $_POST['is_weelky_int_cal'], false, $_POST['no_of_days'], $_POST['num_of_months'], $_POST['dDate'], $five_days_int_cal, 0 ); $_POST['new_int_paid_untill'] = $x['till_date'];*/

        if ($paying_amount > 0){

            $x = $this->calculate->
            
            get_int_paid_till_date(
                $_POST['int_paid_untill'],
                $_POST['is_weelky_int_cal'],
                false,
                $_POST['no_of_days'],
                $_POST['num_of_months'],
                $_POST['dDate'],
                $five_days_int_cal,
                $_POST['is_renew_bill']
            );

            $_POST['new_int_paid_untill'] = $x['till_date'];

            $a['ALL'] = $x;

            if ( ($paying_amount + $adv_balance) > $int_balance ){ // this section added on 2017-02-20 
                

                $tot_adv_bal = ($paying_amount + $adv_balance) - $int_balance ;

                $a['section']       = 7;

                // Claim interest
                // Goto renewal process                   
                
                $a['lgv']       = number_format($this->calculate->PREVIOUS_GOLD_OF_A_LOAN($loanno),2);
                $a['cgv']       = number_format($this->calculate->CURRENT_GOLD_OF_A_LOAN($loanno),2);
                $a['articles']  = $this->calculate->get_pawn_articles($loanno);
                
                //------------ debug penal ---------------
                    
                    $ARR = $this->getPP_data($loanno,floatval($tot_adv_bal),$_POST['int_paid_untill'],$_POST);

                    $a['det']=$ARR['det'];
                    $a['dr']=$ARR['dr'];
                    $a['pi']=$int_balance;
                    $a['nb']=$ARR['nb'];
                    $a['it']=$ARR['it'];
                    $a['fd']=$ARR['fd'];
                    $a['pr']=$ARR['pr'];
                    $a['cd']=$ARR['cd'];

                //------------ debug penal end -----------

                $a['msg_html']          = $this->msg_html();
                $a['new_loan_amount']   = number_format($a['nb'],2); //number_format(floatval($_POST['loan_amount']) - $balance_amount,2);
                $a['paying_amount']     = $tot_adv_bal;
                $a['balance_amount']    = 0;
                $a['data']              = $_POST;

                $a['action']        = "cliam_int_AND_deduct_CAPITAL";




            }else if ( $paying_amount > $int_balance ){

                $tot_adv_bal = ($paying_amount - $int_balance) + $adv_balance;

                if ( $this->calculate->check_availability_of_capital_deduct_bal($tot_adv_bal,$_POST['billno'],$_POST['loan_amount']) ){

                    $a['section']       = 1;

                    // Claim interest
                    // Goto renewal process                   
                    
                    $a['lgv']       = number_format($this->calculate->PREVIOUS_GOLD_OF_A_LOAN($loanno),2);
                    $a['cgv']       = number_format($this->calculate->CURRENT_GOLD_OF_A_LOAN($loanno),2);
                    $a['articles']  = $this->calculate->get_pawn_articles($loanno);
                    
                    //------------ debug penal ---------------
                        
                        $ARR = $this->getPP_data($loanno,floatval($tot_adv_bal),$_POST['int_paid_untill'],$_POST);

                        $a['det']=$ARR['det'];
                        $a['dr']=$ARR['dr'];
                        $a['pi']=$int_balance;
                        $a['nb']=$ARR['nb'];
                        $a['it']=$ARR['it'];
                        $a['fd']=$ARR['fd'];
                        $a['pr']=$ARR['pr'];
                        $a['cd']=$ARR['cd'];

                    //------------ debug penal end -----------

                    $a['msg_html']          = $this->msg_html();
                    $a['new_loan_amount']   = number_format($a['nb'],2); //number_format(floatval($_POST['loan_amount']) - $balance_amount,2);
                    $a['paying_amount']     = $tot_adv_bal;
                    $a['balance_amount']    = 0;
                    $a['data']              = $_POST;

                    $a['action']        = "cliam_int_AND_deduct_CAPITAL";

                }else{
                    
                    if ($int_balance > 0){

                        if ($paying_amount >= $int_balance){

                            $a['section']       = 2;
                        
                            // Claim interest
                            $_POST['claim_by']  = "";                    
                            $_POST['amount']    = $int_balance;
                            $rtn                = $this->calculate->claim_int();
                            $a['int_tr_no']     = $rtn['no'];
                            $a['max_no']        = $rtn['max_no'];
                            $a['action']        = "claim_int_AND_advance_dr";                
                            $a['loanno']        = $rtn['loanno'];

                            $s3num = $paying_amount - $int_balance;

                            if ($s3num > 0){

                                $a['section']       = 3;

                                // Add to advance balance
                                $_POST['cr_amount'] = $s3num;
                                $rtn                = $this->calculate->add_customer_advance_amount();
                                $a['adv_tr_no']     = $rtn['no'];
                                $a['adv_amt_bal']   = $s3num;
                                $a['loanno']        = $rtn['loanno'];

                            }

                        }

                    }else{
                    
                        $a['section']       = 4;

                        // Add to advance balance
                        $_POST['cr_amount'] = $paying_amount;
                        $rtn                = $this->calculate->add_customer_advance_amount();
                        $a['adv_tr_no']     = $rtn['no'];
                        $a['adv_amt_bal']   = $paying_amount;                
                        $a['action']        = "advance_dr";
                        $a['loanno']        = $rtn['loanno'];

                    }

                }

            }else{

                if ($paying_amount == $int_balance){

                    $a['section']       = 5;
                    
                    // Claim interest
                    $_POST['claim_by']  = "";                    
                    $_POST['amount']    = $int_balance;
                    $rtn                = $this->calculate->claim_int();
                    $a['int_tr_no']     = $rtn['no'];
                    $a['max_no']        = $rtn['max_no'];
                    $a['action']        = "claim_int_AND_advance_dr";                
                    $a['loanno']        = $rtn['loanno'];

                }else{

                    $a['section']       = 6;

                    // Add to advance balance
                    $_POST['cr_amount'] = $paying_amount;
                    $rtn                = $this->calculate->add_customer_advance_amount();
                    $a['adv_tr_no']     = $rtn['no'];
                    $a['adv_amt_bal']   = $paying_amount;                
                    $a['action']        = "advance_dr";
                    $a['loanno']        = $rtn['loanno'];                

                }

            }
            
        }


        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        
        }else{

            if ($a['action'] == "advance_dr"){
                $this->db->trans_commit();
            }

            if ($a['action'] == "claim_int_AND_advance_dr"){
                $this->db->trans_commit();
            }

            if ($a['action'] == "cliam_int_AND_deduct_CAPITAL"){
                $this->db->trans_commit();
            }            

        }        

        echo json_encode($a);
    }

    public function proceed_to_renew_loan(){

        $a['s'] = 33;
        $a['msg_33'] = "Session expired or invalid current date setup.\n\nPlease re-enter this part payment bill and you might required re-login to continue";

        if (isset($this->sd['current_date'])){
            if (!$this->utility->is_valid_date($this->sd['current_date'])){
                echo json_encode($a);
                exit;
            }
        }else{            
            echo json_encode($a);
            exit;
        }   $a['s'] = ''; $a['msg_33'] = '';
        
        $loanno         = $_POST['data']['loanno'];        
        $loan_amount    = $_POST['data']['loan_amount'];
        $nla            = floatval(str_replace(",", "", $_POST['nla']));
        $balance_amount = $_POST['balance_amount'];
        $billtype       = $_POST['data']['billtype'];
        $current_gold_value = $_POST['current_gold_value'];        
        $action_date    = date('Y-m-d h:i:s');

        $_POST['data']['advance_total'] = $_POST['paying_amount'];

        $tr_no = ""; // this var value will assign at the line that use

        $this->load->model("calculate");
        $this->calculate->OLD_LOAN_TO_NEW_LOAN($loanno,$balance_amount,$current_gold_value,$tr_no,$billtype,$nla,$_POST['data'],"RN");
    }


    public function pay_only_interest(){
        $paying_amount = $_POST['paying_amount'];
        $balance_amount = $_POST['balance_amount'];
        $tr_no = $this->getPartPayTransNo();
        $action_date = date('Y-m-d h:i:s');
        $this->add_data_to_trans_tbls("json",$_POST['data'],$paying_amount,$balance_amount,$tr_no,$action_date,0); // This part should trigger on "Pay Interest Only" button press
    }

    public function add_data_to_trans_tbls($return_type,$_POST_,$paying_amount,$balance_amount,$tr_no,$action_date,$is_pawn_int){
        
        $this->db->trans_begin();

        $_PST['loanno']    = $_POST_['loanno'];
        $_PST['bc'] = $this->sd['bc']; 
        $_PST['billtype']  = $_POST_['billtype']; 
        $_PST['billno']    = $_POST_['billno']; 
        $_PST['ddate']     = $_POST_['ddate']; 
        $_PST['amount']    = $paying_amount; 
        $_PST['transecode']= "ADV"; 
        $_PST['transeno']  = $tr_no; 
        $_PST['discount']  = 0;  
        $_PST['is_pawn_int'] = $is_pawn_int;

        $cus_serno      = $_POST['data']['hid_customer_serno'];
        unset($_POST['data']['hid_customer_serno']);
        
        if ( floatval($_POST['data']['customer_advance_paying']) > 0 ){
            $_PST['advance_amount']    =    $_POST['data']['customer_advance_paying'];
            //$this->calculate->cr_advance_bal($cus_serno,$_PST['advance_amount'],$tr_no,"ADV_R");
        }        

        if ( floatval($_POST['data']['card_amount']) > 0 ){
            $_PST['card_amount']    = $_POST['data']['card_amount'];
            $this->calculate->add_credit_card_entry($cus_serno,$_PST['card_amount'],$tr_no);
        }        

        if ( floatval($_POST['data']['cash_amount']) > 0 ){
            $_PST['cash_amount']    = $_POST['data']['cash_amount']; 
        }



        if ($paying_amount > 0){
            $Q1 = $this->db->insert("t_loantranse",$_PST); // LOANTRANSE TABLE
        }

        unset($_PST);        
        
        $this->max_no = $tr_no;        
        $_POST['paying_amount'] = $paying_amount;
        $_POST['hid'] = 0;

        $_POST['ln'] = $_POST_['loanno'];
        $_POST['bt'] = $_POST_['billtype'];
        $_POST['bn'] = $_POST_['billno'];
        
        if (isset($_POST['ddate'])){
            $_POST['date'] = $_POST['ddate'];
        }else{
            $_POST['date'] = $_POST['data']['ddate'];
        }


        if (floatval($_POST['paying_amount']) > 0){
            $account_update =   $this->account_update(0);                                
            
            if($account_update!=1){
                $a['s'] = 0; $a['m'] = "Invalid account entries"; $this->db->trans_rollback(); $a['s'] = "error";
                echo json_encode($a);
                exit;
            }else{
                $account_update =   $this->account_update(1);
            }
        }        

        if ($balance_amount > 0){
            $_PST['client_id'] = $_POST_['customer_id']; $_PST['trans_type'] = "A";$_PST['trans_no'] = $tr_no;$_PST['loan_no'] = $_POST_['loanno'];$_PST['date'] = $this->sd['current_date'];$_PST['dr_amount'] = 0;$_PST['cr_amount'] = $balance_amount ;$_PST['note'] = "";$_PST['is_post'] = 0;$_PST['is_delete'] = 0;$_PST['bc'] =  $this->sd['bc'];$_PST['oc'] = $this->sd['oc'];$_PST['action_date'] = $action_date;
            $Q2 = $this->db->insert("t_loan_advance_trans",$_PST); // ACCOUNT TRANS TABLE                
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{            
            $this->db->trans_commit();
            $a['s']         = 1;
            $a['max_no']    = $this->getPartPayTransNo();
            $a['no']        = $tr_no;
            $a['loanno']    = $_POST_['loanno'];
        }

        
        if ($return_type == "json"){            
            echo json_encode($a);
        }else{
            return $a;
        }

    }

    private function msg_html(){

        return '

        <div class="tbl_pp_next_bill_popup">


            <div class="div_pp_det_item">
                <div>Pawn Details</div>
                <span class="det"></span>
            </div>

            <div class="div_pp_det_item">
                <div>Date Range</div>
                <span class="dr"></span>
            </div>            

            <div class="div_pp_det_item">
                <div>Payable Int</div>
                <span class="pi" style="color:red"></span>
            </div>

            <div class="div_pp_det_item">
                <div>New Balance</div>
                <span class="nb"></span>
            </div>              

            <div class="div_pp_det_item">
                <div>Interest Till</div>
                <span class="it"></span>
                <input type="hidden" id="it_class" name="it">
            </div>                        

            <div class="div_pp_det_item">
                <div>New Final Date</div>
                <span class="fd"></span>                
            </div>                                    
            

            <div class="div_pp_det_item">
                <div>Capital Deduct Amount</div>
                <span class="cd"></span>                
            </div>

        </div>



        <br>
        <div class="title_div_pp">Part Payment</div>

            
            
            <div style="height:100px;border:0px solid red;">
                
                <div style="width:232px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn">
                    <span class="text_box_title_holder_new_pawn" style="width:150px">New Loan Amount</span>
                    <input type="text" id="nla" class="input_text_large_s amount" style="width:200px">                               
                </div>

                <div style="width:232px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn">
                    <span class="text_box_title_holder_new_pawn" style="width:150px">Previous Gold Value</span>
                    <input type="text" readonly="readonly" id="lgv" class="input_text_large_s" style="width:200px">                               
                </div>

                <div style="width:232px;border:0px solid #ccc;float:left" class="text_box_holder_new_pawn">
                    <span class="text_box_title_holder_new_pawn" style="width:150px">Current Gold Value</span>
                    <input type="text" readonly="readonly" id="cgv" class="input_text_large_s" style="width:200px">                               
                </div>

            </div>

            <div id="div_itm_contain" style="border:0px solid Green;margin-bottom:20px;width:672px;height:44px; overflow:hidden">
                <div class="show_pawn_articles_div">Show Pawn Articles</div>
                <div id="div_show_items"></div>
            </div>  


            
            <div>            
                <div style="float:left">
                <input type="button" id="btnProceedRenew" class="btn_regular" value="Proceed to Renew" style="width:150px">
                <!--<input type="button" id="btnPayOnlyInt" class="btn_regular" value="Pay Only Interest" style="width:150px">-->
                </div>

                <div class="pp_procees_stat_indicator" style="float:left; margin-left:10px; margin-top:6px"></div>
                
                <div style="float:right">
                <input type="button" id="btnPPcancel" class="btn_regular" value="Cancel">
                </div>
            </div>';

    }

    public function LOAD_LOAN(){
        
        $billtype = $_POST['billtype'];
        $billno = $_POST['bc_no'].$_POST['billno'];
        $int_cal_to_DATE = $_POST['dDate'];
        $five_days_int_cal = $_POST['five_days_int_cal'];
        $bc = $this->sd['bc'];

        $Q1 = $this->db->query("SELECT L.time, L.goldvalue, L.old_bill_age, L.is_renew,L.billno,L.bc,C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,C.`customer_id`,L.int_cal_changed,L.cus_serno,L.am_allow_frst_int,L.int_paid_untill,L.is_weelky_int_cal,L.fmintrest FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '$billno' AND L.bc = '$bc' LIMIT 1 ");

        if ($Q1->num_rows() > 0){
            
            $a['loan_sum'] = $Q1->row();
            $loan_no = $Q1->row()->loanno;            
            
            $this->load->model("calculate");
            $this->load->model("t_new_pawn");
            $this->load->model("m_billtype_det");
            
            $a['int'] = $this->calculate->interest($loan_no,"json",$int_cal_to_DATE, $a['loan_sum'],$five_days_int_cal);
            $a['customer_advance'] = $this->calculate->customer_advance($Q1->row()->customer_id,$billno);
            $a['partpay_histomer'] = $this->db->select( array("amount","discount","action_date","bc") )->where(array("bc"=>$bc,"loanno"=>$loan_no,"transecode"=>"A"))->where("is_pawn_int !=",1)->order_by("action_date","desc")->get("t_loantranse")->result();
            $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;
            $a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);
            $a['s'] = 1;
            $a['section'] = 1;
        
        }else{

            $Q2 = $this->db->query("SELECT L.time,L.goldvalue, L.old_bill_age, L.is_renew, L.billno, L.bc,C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,L.int_cal_changed ,  (SELECT IF (COUNT(transecode) > 0, transecode ,L.status) FROM t_loantranse_re_fo WHERE bc = '$bc' AND `billno` = '$billno' AND transecode NOT IN('P','A','ADV')) AS `status`,L.am_allow_frst_int,L.int_paid_untill,L.fmintrest  FROM `t_loan_re_fo` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE    billno = '$billno' AND L.bc = '$bc' LIMIT 1 ");

            if ($Q2->num_rows()>0){
                $a['loan_sum']  = $Q2->row();
                $loan_no        = $Q2->row()->loanno;            
            
                $this->load->model("calculate");
                $this->load->model("t_new_pawn");
                $this->load->model("m_billtype_det");
                
                $a['int'] = $this->calculate->interest($loan_no,"json",$int_cal_to_DATE, $a['loan_sum'],$five_days_int_cal);
                $a['customer_advance'] = $this->calculate->customer_advance($Q2->row()->customer_id,$billno);
                $a['partpay_histomer'] = $this->db->select( array("amount","discount","action_date","bc") )->where(array("bc"=>$bc,"loanno"=>$loan_no,"transecode"=>"A"))->where("is_pawn_int !=",1)->order_by("action_date","desc")->get("t_loantranse")->result();
                $a['cus_info'] =  $this->t_new_pawn->getCustomerInfoByNic( $a['loan_sum']->nicno ) ;
                $a['rec'] = $this->m_billtype_det->getBillTypeInfo_all($a['loan_sum']->billtype);
                $a['s'] = 1;
                $a['section'] = 2;
            }else{
                $a['s'] = 0;
            }

        }

        echo json_encode($a);
    }    
    
    public function getSavedPartPay(){
        $bc      = $this->sd['bc'];
        $tr_no   = $_POST['tr_no'];

        $Q1 = $this->db->query("SELECT L.bc,
        LT.`ddate` AS `pp_paid_date`,   
        LT.`amount`,
        LT.`discount`,  
        LT.`bc` AS `pp_added_bc`,        
        L.`ddate` AS `pawn_date`,
        L.`ddate` AS `ddate`,
        L.`billtype`,
        L.`billno`,
        L.`loanno`,
        L.`requiredamount`,
        L.`fmintrate`,
        L.`fmintrate2`,
        L.`finaldate`,
        L.`period` ,
        L.`int_cal_changed`,
        LT.`card_amount`,
        LT.`advance_amount`,
        LT.`cash_amount`,
        L.cus_serno

      
        FROM `t_loantranse` LT 

        JOIN `t_loan` L ON LT.`bc` = L.`bc` AND LT.`billtype` = L.`billtype` AND LT.`billno` = L.`billno`        

        WHERE LT.bc = '$bc' AND LT.`transeno` = '$tr_no' AND LT.`transecode` = 'A' and LT.`is_pawn_int` != 1 ");

        if ($Q1->num_rows() > 0){            
            $a['sum'] = $Q1->row();
            $this->load->model("calculate");
            $a['int'] = $this->calculate->interest($Q1->row()->loanno,"json",$Q1->row()->pp_paid_date,$a['sum']);
            
            $a['pay_option'] = $this->calculate->get_pay_option(

                $bc, 
                $tr_no, 
                $a['sum']->cus_serno, 
                $a['sum']->card_amount, 
                $a['sum']->advance_amount, 
                $a['sum']->cash_amount,
                "A" 

            );
            
            if ($a['sum']->cash_amount > 0){ 
                $a['pay_option']['cash_amount'] = $a['sum']->cash_amount; 
            }

            $a['s'] = 1;            
        }else{
            $a['s'] = 0;
        }

       echo json_encode($a);

    }

    public function getPartPayTransNo(){
        $bc = $this->sd['bc'];
        return $this->db->query("SELECT GREATEST((SELECT IFNULL(MAX(`transeno`)+1,1) FROM `t_loantranse` WHERE bc = '$bc' AND `transecode` = 'A'),(SELECT IFNULL(MAX(`transeno`)+1,1) FROM `t_loantranse_re_fo`  WHERE bc = '$bc' AND `transecode` = 'A')) AS `max_no`")->row()->max_no;
    }

    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 4);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',4);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "trans_code" => 4,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        
        $this->load->model('account');
        $this->account->set_data($config);
           
        $cash_book          = $this->utility->get_acc('cash_acc',$this->sd['bc']);
        $ADVANCE_RECEIVED   = $this->utility->get_default_acc('ADVANCE_RECEIVED');

        $this->account->set_value2("Advance Payments", $_POST['paying_amount'] , "dr", $cash_book,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"A");                
        $this->account->set_value2("Advance Payments", $_POST['paying_amount'], "cr", $ADVANCE_RECEIVED,$condition,"",$_POST['ln'],$_POST['bt'],$_POST['bn'],"A");
                
        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='4'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 4);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }





    public function getPP_data($loanno,$paying_amount,$int_paid_untill,$LOAN_DATA){        

        $R = $this->db->query("SELECT * FROM `t_loan` WHERE loanno = '$loanno'")->row();

        $first_rage_end = 7-1;
        $second_rage_end= 30-1;

        $pawn_date      = $R->ddate;
        $current_date   = $this->sd['current_date'];
        $bc             = $this->sd['bc'];
        $age_of_pawn    = $this->db->query("SELECT DATEDIFF(DATE_ADD('$pawn_date', INTERVAL 1 MONTH),'$pawn_date') AS `first_mon_days`,(SELECT DATEDIFF('$current_date','$pawn_date')) AS `no_of_days`")->row()->no_of_days;
        $third_rage_end = $age_of_pawn;

        $det = "Pawn Date : ".$R->ddate;
        
        //----------------------------------------------------------------------------------------

        if ($R->is_weelky_int_cal == 1){ // WEEKLIY BILLS
            
            if ($age_of_pawn <= $first_rage_end){
                $dr = "Weekly bill, within first week";
                $pi = 0;
                $nb = floatval($R->requiredamount) - $paying_amount;
                $it = $this->db->query("SELECT ADDDATE('".$R->ddate."', INTERVAL ($first_rage_end) DAY) AS `till_date`")->row()->till_date;
                $fd = $this->db->query("SELECT ADDDATE('$current_date',INTERVAL ".$R->period." MONTH) AS `fd`")->row()->fd;
                $pp = $paying_amount;
                
                $pr = 0;
                $cd = $pp;
            }

            
            if ($age_of_pawn > $first_rage_end && $age_of_pawn <= $second_rage_end){
                $dr = "Weekly bill, within first month";                
                
                if ($current_date > $int_paid_untill){
                    $pi = $this->calculate->set_amount($this->calculate->cal_interest($R->requiredamount,$LOAN_DATA,($R->fmintrate+$R->fmintrate2)));
                    $pi = $pi - $_POST['paid_interest'];
                }else{
                    $pi = $this->calculate->set_amount(0);
                }

                $nb = floatval($R->requiredamount) - ($paying_amount - $pi);
                $it = $this->db->query("SELECT ADDDATE('".$int_paid_untill."', INTERVAL ($second_rage_end) DAY) AS `till_date`")->row()->till_date;
                $fd = $this->db->query("SELECT ADDDATE('$current_date',INTERVAL ".$R->period." MONTH) AS `fd`")->row()->fd;
                $pp = $paying_amount;
                
                $pr = $pi;
                $cd = $pp - $pi;
            }


            if ($age_of_pawn > $second_rage_end){
                $dr = "Weekly bill, over one month";
                
                $INT_A  = $this->calculate->cal_interest($R->requiredamount,$LOAN_DATA,$R->fmintrate+$R->fmintrate2);
                $m      = $this->get_after_months($current_date,$R->ddate);
                $det    .= " <br>Extra Months $m";
                $INT_B  = $this->calculate->cal_interest($R->requiredamount,$LOAN_DATA,($R->fmintrate+$R->fmintrate2));
                
                if ($current_date > $int_paid_untill){
                    $pi = $this->calculate->set_amount(($INT_A + ($INT_B * $m)) - $_POST['paid_interest']);                    
                }else{
                    $pi = $this->calculate->set_amount(0);
                }

                $nb     = floatval($R->requiredamount) - ($paying_amount - $pi);
                
                $x = ($m + 1) * 30;
                
                $it = $this->db->query("SELECT ADDDATE('".$R->ddate."', INTERVAL ($x) DAY) AS `till_date`")->row()->till_date;
                
                $fd = $this->db->query("SELECT ADDDATE('$current_date',INTERVAL ".$R->period." MONTH) AS `fd`")->row()->fd;
                $pp = $paying_amount;
                
                $pr = $pi;
                $cd = $pp - $pi;
            }

        }


        if ($R->is_weelky_int_cal == 0){ // MONTHLY BILLS

            if ($age_of_pawn <= $second_rage_end){
                $dr = "Monthly bill, within month";
                $pi = 0;
                $nb = floatval($R->requiredamount) - $paying_amount;
                $it = $this->db->query("SELECT ADDDATE('".$R->ddate."', INTERVAL ($second_rage_end) DAY) AS `till_date`")->row()->till_date;
                $fd = $this->db->query("SELECT ADDDATE('$current_date',INTERVAL ".$R->period." MONTH) AS `fd`")->row()->fd;
                $pp = $paying_amount;
                
                $pr = 0;
                $cd = $pp;
            }

            if ($age_of_pawn > $second_rage_end){

                $dr     = "Monthly bill, over one month";                
                $INT_A  = 0; // (($R->requiredamount * $R->fmintrate2)/100);                
                
                $m      = $this->get_after_months($current_date,$R->ddate);
                $det   .= " <br>Extra Months $m";
                $INT_B  = $this->calculate->cal_interest($R->requiredamount,$LOAN_DATA,($R->fmintrate+$R->fmintrate2));
                $pi     = $this->calculate->set_amount($INT_A + ($INT_B * $m) );                
                
                $nb = floatval($R->requiredamount) - ($paying_amount - $pi);                
                $x  = ($m + 1) * 30;                
                $it = $this->db->query("SELECT ADDDATE('".$R->ddate."', INTERVAL ($x) DAY) AS `till_date`")->row()->till_date;
                
                $fd = $this->db->query("SELECT ADDDATE('$current_date',INTERVAL ".$R->period." MONTH) AS `fd`")->row()->fd;
                $pp = $paying_amount;
                
                $pr = $pi;
                $cd = $pp - $pi;
            }

        }

        $qD = $this->db->query("SELECT @cd := '$current_date' AS `cd`, @t := FLOOR((DATEDIFF( ADDDATE(@cd, INTERVAL 1 DAY), L.`ddate` )) / 30) AS `m`, IF (@t < 1,
    
    IF (@cd <= L.`int_paid_untill`,
        L.`int_paid_untill`,
        
        IF (L.`old_o_new_billno` = 0,
            ADDDATE(L.dDate,INTERVAL 30 DAY),
            ADDDATE(L.`int_paid_untill`,INTERVAL 23 DAY)
        )
        
    )
,/*else*/
    ADDDATE(L.`ddate`, INTERVAL ((@t * 30) + 30) DAY)
  
  ) AS `int_paid_till` FROM t_loan L WHERE L.`bc` = '$bc' AND L.loanno = $loanno LIMIT 1 ");
        

        $a['det'] =  $det; // loan details
        $a['dr'] = $dr; // date range
        $a['pi'] = $pi; // payable interest
        $a['nb'] = $nb - $pi; // new balance
        $a['it'] = $qD->row()->int_paid_till; // Interest paid till ** Query might delay due to re calculate by a new thread. 
        $a['fd'] = $fd; // final date
        
        $a['pr'] = $pr; // print int receipt
        $a['cd'] = $cd+$pi; // capital deduct
        
        return $a;

    }



    private function get_after_months($current_date,$pawn_date){
        $a = floatval($this->db->query("SELECT DATEDIFF('$current_date',(SELECT (SELECT DATE_ADD('$pawn_date',INTERVAL +31 DAY)))) AS `after_first_month_days_passed`")->row()->after_first_month_days_passed);
            
        if ($a % 30 == 0){
            $m = $a/30;
        }else{
            $m = intval($a / 30) + 1;
        }        

        return $m;
    }


}