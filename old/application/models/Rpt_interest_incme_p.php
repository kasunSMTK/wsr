<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_interest_incme_p extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function base_details(){		
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $bc = $_POST_['h_bc'];
    
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

   //===================================================================================

    $sql_mon="SELECT DISTINCT
                    MONTHNAME(l.ddate) AS mon,
                    YEAR(l.ddate) AS yr
                    FROM t_loantranse l
                     WHERE l.transecode = 'A' AND l.ddate BETWEEN '$fd ' AND '$td' 
                    ORDER BY MONTH(l.ddate),YEAR(l.ddate) ";

    $r_data['months']=$this->db->query($sql_mon)->result();


    $sql="SELECT 
            MONTH(l.ddate) AS mon,
            MONTHNAME(l.ddate) AS mon_name,
            YEAR(l.ddate) AS yr,
            l.bc,
            b.name,
            l.billtype,
            SUM(IFNULL(l.amount, 0)) AS pwning_int
            FROM t_loantranse l 
            LEFT JOIN m_branches b  ON b.bc = l.bc 
            WHERE l.transecode = 'A' AND l.ddate BETWEEN '$fd ' AND '$td' ";
         
           
    if($bc!=""){

        $sql.="  AND  l.bc='".$bc."'";
    }

    if($_POST['r_billtype']!=""){

        $sql.="  AND  l.billtype='".$_POST['r_billtype']."'";
    }

    //$sql.=" GROUP BY MONTH(l.ddate),l.billtype,l.bc  ";
  /*  $sql.="GROUP BY l.bc , MONTH(l.ddate),l.billtype
            ORDER BY l.bc , MONTH(l.ddate),l.billtype";*/

    $sql.="GROUP BY l.bc,MONTH(l.ddate),l.billtype 
            ORDER BY l.bc,l.billtype,MONTH(l.ddate) ";

    $query = $this->db->query($sql);
    //$r_detail['det'] = $this->db->query($sql)->result();

  
    

   

    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

       // $this->load->view($_POST['by'].'_'.'pdf',$r_data);
   
       /*if ($r_type == "pdf"){
            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{*/

            $this->load->view($_POST['by'].'_'.'excel',$r_data);
       // }

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

public function Excel_report()
    {
        $this->PDF_report($_POST,"Excel");
    }

}