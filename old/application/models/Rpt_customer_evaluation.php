<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_customer_evaluation extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $from_val=$_POST_['txt_rpt_from_r'];
    $to_val=$_POST_['txt_rpt_to_r'];
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    $is_act=$_POST_['is_act'];
  
    $ddate= " WHERE l.ddate BETWEEN '$fd' AND '$td'";

if($is_act==null){
  $amount="";
}else{  
  $amount= " AND l.requiredamount BETWEEN '$from_val' AND '$to_val'";
}


if (isset($_POST['bc_n'])){
    if ( $_POST['bc_n'] == "" ){        
        $bc = "";
    }        
}else{

    for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
        $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
    }

    $bc = implode(',', $bc_ar);

    if ($bc == ""){
        $bc   = "";
    }else{
        $bc   = " AND  l.bc IN ($bc)  ";
    }
}


    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }



$sql="
SELECT l.`requiredamount`,b.`name` AS `bc_name`,l.bc, l.`ddate`,l.`cus_serno`,c.`cusname` , c.`nicno`,l.`billno`, li.tw AS `totalWeight` , li.pw AS `pureWeight`, li.articles AS `articles`, l.`fmintrest` 

FROM t_loan l 

JOIN (  SELECT li.billno, SUM(li.`goldweight`) AS `tw` , SUM(li.`pure_weight`) AS `pw` , GROUP_CONCAT( CONCAT(i.`itemname`,' (',li.`goldweight`,')') ) AS `articles` 
  FROM  t_loanitems li 
  JOIN r_items i ON li.`itemcode` = i.itemcode
  GROUP BY li.`billno` 
  
) li ON li.`billno` = l.`billno`

JOIN m_branches b ON l.bc = b.`bc`
JOIN m_customer c ON l.`cus_serno` = c.`serno`

$ddate $amount $bc $billtype

ORDER BY cus_serno , bc , ddate ";
    

$sql2 = "

     SELECT b.`name` AS `bc_name`, l.bc, l.`ddate`,l.`cus_serno`, c.`cusname` , c.`nicno` ,l.`billno`,li.tw AS `totalWeight` , li.pw AS `pureWeight`, li.articles AS `articles`,lt.discount,lt.amount FROM t_loan_re_fo l 

JOIN (SELECT * FROM t_loantranse_re_fo lt WHERE lt.`transecode` = 'R' GROUP BY lt.`billno` ) lt ON l.`billno` = lt.billno

JOIN (  SELECT li.billno, SUM(li.`goldweight`) AS `tw` , SUM(li.`pure_weight`) AS `pw` , GROUP_CONCAT( CONCAT(i.`itemname`,' (',li.`goldweight`,')') ) AS `articles` 
    FROM  t_loanitems_re_fo li 
    JOIN r_items i ON li.`itemcode` = i.itemcode
    GROUP BY li.`billno` 
    
) li ON li.`billno` = l.`billno`

JOIN m_branches b ON l.bc = b.`bc`
JOIN m_customer c ON l.`cus_serno` = c.`serno`

$ddate $amount $bc $billtype

ORDER BY cus_serno , bc , ddate ";

      



    if (isset($_POST['cus_eva_pawn'])){
        $query = $this->db->query($sql);
        $set1 = $this->db->query($sql)->num_rows();
    }
    
    if (isset($_POST['cus_eva_redm'])){
        $query2 = $this->db->query($sql2);
        $set2 = $this->db->query($sql2)->num_rows();
    }



    if($set1>0   || $set2>0 ){

        if (isset($_POST['cus_eva_pawn'])){
            $r_data['list'] = $query->result();            
        }
        
        if (isset($_POST['cus_eva_redm'])){
            $r_data['list2'] = $query2->result();            
        }


        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $r_data['from_val'] = $from_val;
        $r_data['to_val'] = $to_val;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}