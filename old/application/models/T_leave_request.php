<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_leave_request extends CI_Model {

    private $sd;
    private $mtb;
    private $pay_roll;
    
    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();
      $this->pay_roll = $this->load->database('payroll', TRUE);
      $this->mtb = 'tblusers';
      $this->load->helper(array('form', 'url')); 
  }

  public function base_details(){		

    $a['max_no']=$this->get_maxno();



    $a['bc'] = $this->sd['bc'];

    return $a;
}

public function save(){

//$DB2 = $this->load->database('payroll', TRUE);

    if ($_POST['hid'] == 0){
        unset($_POST['hid']);
        $Q = $this->pay_roll->insert("t_leave_request",$_POST);
    }else{
        unset($_POST['hid']);        
        $Q = $this->pay_roll->where('ref_no',$_POST['ref_no'])->update("t_leave_request",$_POST);
    }

    if ($Q){

        echo 1;
    }else{
        echo 0;
    }
}

public function get_maxno(){
    $this->pay_roll->select_max('ref_no');
    $this->pay_roll->from('t_leave_request');
    return $this->pay_roll->get()->first_row()->ref_no +1;
}

public function load(){
//var_dump($this->pay_roll);exit();
//$DB2 = $this->load->database('payroll', TRUE);

    $this->pay_roll->select(
        array('tl.ref_no',
            'tl.ddate',
            'tl.status',
            'tl.branch_code',
            'tl.emp_code',
            'tl.from_date',
            'tl.to_date',
            'tl.leave_type',
            'tl.reason',
            'tl.leave',
            'mb.branchName AS b_name',
            'em.nName'));
    $this->pay_roll->from('t_leave_request AS tl');
    $this->pay_roll->join('m_branches AS mb','mb.cCode=tl.branch_code','left');
    $this->pay_roll->join('m_employee AS em','em.emp_no=tl.emp_code','left');
    $this->pay_roll->where('tl.ref_no',$_POST['id']);
    $a['sum']=$this->pay_roll->get()->result();

    echo json_encode($a);

}

public function delete(){

    $this->pay_roll->where('ref_no',$_POST['id']);
    $result=$this->pay_roll->update('t_leave_request',array("is_cancel"=>1));

    if($result){
     echo 1; 
 }else{
    echo 0;
}

}

public function get_branch(){

    $q      = $_GET['term'];        
    $query  = $this->pay_roll->query("SELECT `cCode`,`branchName` FROM `m_branches` WHERE (`cCode` LIKE '%$q%' OR `branchName` LIKE '%$q%') ");
    $ary    = array();    

    foreach($query->result() as $r){ $ary [] = $r->cCode . " - ". $r->branchName ; }    
    echo json_encode($ary);
}

public function get_employee(){
  
    $bc     = $this->sd['bc'];
    $q      = $_GET['term'];        
    $query  = $this->pay_roll->query("SELECT `emp_no`,`nName` FROM `m_employee` WHERE bc = '$bc' AND (`emp_no` LIKE '%$q%' OR `nName` LIKE '%$q%') ");
    $ary    = array();    
    
    foreach($query->result() as $r){ $ary [] = $r->emp_no . " - ". $r->nName ; }    
    echo json_encode($ary);
}

}