<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_pcl_expense_analysis extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function base_details(){		
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_,$r_type="pdf"){    

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        if ( !is_array($_POST['bc_arry']) ){
            
            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
            
            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
            }

            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
        }

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bcc   = "";
        }else{
            $bcc   = "   A.bc IN ($bc) AND ";
        }
    }


/*    echo $bc;
    exit;*/


  if ($r_type == "pdf"){
        echo "No pdf version for this report, Please use excel version";
        exit;
    }

    $bc = $_POST_['h_bc'];
    
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    $data_range=$_POST_['h_range'];
    $account_type=$_POST_['h_acc'];



  $sql1="SELECT t.heading 
        FROM m_account_type t 
        LEFT JOIN m_account a ON a.code=t.code  
        WHERE t.rtype='2' AND NOT t.code='2' AND t.code='$account_type'";
     
        $r_data['acc'] = $this->db->query($sql1)->first_row()->heading;
   //=============================months======================================================

if( $data_range=="months"){
    $sql_mon="SELECT DISTINCT MONTHNAME(l.ddate) AS mon,
                      YEAR(l.ddate) AS yr 
                    FROM
                      t_voucher_gl_sum l 
                    WHERE l.is_approved='1' 
                      AND l.ddate BETWEEN '$fd ' AND '$td'
                    ORDER BY MONTH(l.ddate),
                      YEAR(l.ddate) ";

    $r_data['months']=$this->db->query($sql_mon)->result();

/*
    $sql="SELECT MONTH(l.ddate) AS mon,
                 MONTHNAME(l.ddate) AS mon_name,
                 YEAR(l.ddate) AS yr,
                 d.v_bc bc,
                 b.name,
                 SUM(IFNULL(d.amount, 0)) AS expense FROM   t_voucher_gl_sum l 
                LEFT JOIN t_voucher_gl_det d ON d.nno=l.nno
                LEFT JOIN m_account_type t   ON t.code=d.acc_code
                LEFT JOIN m_branches b       ON b.bc = d.v_bc 
                WHERE t.rtype = '2' AND NOT t.code='2' AND l.is_approved='1' 
                AND l.ddate 
                BETWEEN '$fd ' AND '$td' ";*/



/*  $sql="SELECT 


MONTH(C.ddate) AS mon, MONTHNAME(C.ddate) AS mon_name, YEAR(C.ddate) AS yr, A.bc, A.name, IFNULL(C.bal,0) AS expense 

FROM m_branches A 

CROSS JOIN m_account_type B 
LEFT JOIN (   SELECT v_bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal`,bc 
    FROM t_account_trans 
    WHERE ddate BETWEEN '$fd' AND '$td' /* and acc_code = '20108' */
    /*GROUP BY bc,acc_code,MONTH(ddate) ) C ON A.`bc` = C.v_bc AND B.code = C.acc_code */


/*WHERE $bcc /*C.bc NOT IN (SELECT bc FROM m_branches WHERE is_cost_center = 1) AND*/ 
/*B.`rtype` = 2 AND NOT ISNULL(MONTH(C.ddate)) "; */

  $sql = "SELECT MONTH(C.ddate) AS mon, MONTHNAME(C.ddate) AS mon_name, YEAR(C.ddate) AS yr, A.bc, A.name, SUM(IFNULL(C.bal,0)) AS expense 
FROM m_branches A 
CROSS JOIN m_account_type B 
LEFT JOIN (   SELECT bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal` 
    FROM t_account_trans 
    WHERE ddate BETWEEN '$fd' AND '$td' 
    GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code 
    
WHERE B.`rtype` = 2 AND NOT ISNULL(MONTH(C.ddate)) ";

         
           
    

    if($_POST_['h_acc']!=""){
        $sql.="  AND  C.acc_code='".$_POST_['h_acc']."'";
    }

   
    $sql.=" GROUP BY A.bc,MONTH(C.ddate) 
            ORDER BY A.bc,MONTH(C.ddate) ";

    
    $query = $this->db->query($sql);
}

 //=============================end - months======================================================
 //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- years -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
if($data_range=="years"){

    $sql_yr="SELECT DISTINCT 
                      YEAR(l.ddate) AS yr 
                    FROM
                      t_voucher_gl_sum l 
                    WHERE l.is_approved='1' 
                      AND l.ddate BETWEEN '$fd ' AND '$td'
                    ORDER BY YEAR(l.ddate) ";
   

    $r_data['yrs']=$this->db->query($sql_yr)->result();

    /*$sql="SELECT YEAR(l.ddate) AS yr,
                 d.v_bc bc,
                 b.name,
                 SUM(IFNULL(d.amount, 0)) AS expense FROM   t_voucher_gl_sum l 
                LEFT JOIN t_voucher_gl_det d ON d.nno=l.nno
                LEFT JOIN m_account_type t   ON t.code=d.acc_code
                LEFT JOIN m_branches b       ON b.bc = d.v_bc 
                WHERE t.rtype = '2' AND NOT t.code='2' AND l.is_approved='1' 
                AND l.ddate 
                BETWEEN '$fd ' AND '$td' ";*/
         
    

    $sql="SELECT 


YEAR(C.ddate) AS yr,
                 A.bc bc,
                 A.name,
                 SUM(IFNULL(C.bal, 0)) AS expense

FROM m_branches A 

CROSS JOIN m_account_type B 
LEFT JOIN (   SELECT v_bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal`,bc 
    FROM t_account_trans 
    WHERE ddate BETWEEN '$fd' AND '$td' -- and acc_code = '20108'
    GROUP BY bc,acc_code,MONTH(ddate) ) C ON A.`bc` = C.v_bc AND B.code = C.acc_code 


WHERE C.bc NOT IN (SELECT bc FROM m_branches WHERE is_cost_center = 1) AND B.`rtype` = 2 ";



    
    if($bc!=""){

        $sql.="  AND  A.bc='".$bc."'";
    }

    if($_POST_['h_acc']!=""){

        $sql.="  AND  C.acc_code='".$_POST_['h_acc']."'";
    }


   
    $sql.="GROUP BY A.bc,YEAR(C.ddate) 
           ORDER BY A.bc,YEAR(C.ddate)";
    
    $query = $this->db->query($sql);

}

//=============================end - months======================================================
 //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- years -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
if($data_range=="quarter"){

    $sql_qtr="SELECT QUARTER('$td') AS Qurter";

    $r_data['qtrs']=$this->db->query($sql_qtr)->result();


    $sql="SELECT l.ddate,
                  CASE 
                    WHEN MONTH(l.ddate) BETWEEN 1 AND 3 THEN 'Q1'
                    WHEN MONTH(l.ddate) BETWEEN 4 AND 6  THEN 'Q2'
                    WHEN MONTH(l.ddate) BETWEEN 7 AND 9 THEN 'Q3'
                    WHEN MONTH(l.ddate) BETWEEN 8 AND 12 THEN 'Q4'
                  END AS Qurter,
                 d.v_bc bc,
                 b.name,
                 SUM(IFNULL(d.amount, 0)) AS expense FROM   t_voucher_gl_sum l 
                LEFT JOIN t_voucher_gl_det d ON d.nno=l.nno
                LEFT JOIN m_account_type t   ON t.code=d.acc_code
                LEFT JOIN m_branches b       ON b.bc = d.v_bc 
                WHERE t.rtype = '2' AND NOT t.code='2' AND l.is_approved='1' 
                AND l.ddate 
                BETWEEN '$fd ' AND '$td'";  
                
         
           
    if($bc!=""){

        $sql.="  AND  d.v_bc='".$bc."'";
    }

    if($_POST_['h_acc']!=""){

        $sql.="  AND  t.code='".$_POST_['h_acc']."'";
    }

   
    $sql.="GROUP BY d.v_bc,Qurter
           ORDER BY d.v_bc,Qurter";

    
    $query = $this->db->query($sql);

} 


if($data_range=="half_year"){

    

    $sql="SELECT l.ddate,
                  CASE 
                    WHEN MONTH(l.ddate) BETWEEN 1 AND 6 THEN 'H1'
                    WHEN MONTH(l.ddate) BETWEEN 7 AND 12  THEN 'H2'
                  END AS Qurter,
                 d.v_bc bc,
                 b.name,
                 SUM(IFNULL(d.amount, 0)) AS expense FROM   t_voucher_gl_sum l 
                LEFT JOIN t_voucher_gl_det d ON d.nno=l.nno
                LEFT JOIN m_account_type t   ON t.code=d.acc_code
                LEFT JOIN m_branches b       ON b.bc = d.v_bc 
                WHERE t.rtype = '2' AND NOT t.code='2' AND l.is_approved='1' 
                AND l.ddate 
                BETWEEN '$fd ' AND '$td'";  
                
         
           
    if($bc!=""){

        $sql.="  AND  d.v_bc='".$bc."'";
    }

    if($_POST_['h_acc']!=""){

        $sql.="  AND  t.code='".$_POST_['h_acc']."'";
    }

   
    $sql.="GROUP BY d.v_bc,Qurter
           ORDER BY d.v_bc,Qurter";

    
    $query = $this->db->query($sql);

}    


   
    
   // if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $r_data['d_range'] = $data_range;
      
        

      
        $this->load->view($_POST['by'].'_'.'excel',$r_data);
    

   /* }else{
        echo "<script>location='default_pdf_error'</script>";
    }*/

}

public function Excel_report()
    {
        $this->PDF_report($_POST,"Excel");
    }

}