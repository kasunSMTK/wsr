<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class notifications extends CI_Model {
    
    private $sd;        
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['a'] = 1;        
		return $a;
    }


    public function notifications_titles(){


        $t  = " <a href = '#'><div class='notifi_holder'>You have no new request(s)</div></a> ";

        $n = $nn = 0;        

        if ( $this->sd['isAdmin'] ){

            $t = "";
        
            $bc = $this->sd['bc'];            
            $oc = $this->sd['oc'];



            $n = $this->db->query(" SELECT * FROM `t_fund_transfer_sum` s WHERE NOT transfer_type = 'cheque_withdraw' AND (`from_bc` = '$bc' OR `from_bc` = 'HO') AND `status` IN (1)  ORDER BY `no` DESC")->num_rows();
            if ($n > 0){
                $t .= " <a href = '".base_url()."?action=t_fund_transfer#tabs-3'> ";        
                $t .= " <div class='notifi_holder'><!--<div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>-->";
                $t .= " You have ($n) new incoming fund fransfer request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";
                $nn += $n;
            }



            $n = $this->db->query(" SELECT S.no FROM `t_fund_transfer_sum` S JOIN `t_fnd_trnsfr_rqsts_chq_det`   CRD ON S.`no`   = CRD.`no`  AND S.`requ_initi_bc`   = CRD.`bc` WHERE S.`transfer_type` = 'cheque_withdraw' AND S.`status` = 1 ORDER BY S.`requ_initi_bc` , S.`no` , CRD.`cheque_no`   ")->num_rows();
            if ($n > 0){
                $t .= " <a href = '".base_url()."?action=appvl_fund_transfers'> ";        
                $t .= " <div class='notifi_holder'><!--<div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>-->";
                $t .= " You have ($n) new cheque withdrawal request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";
                $nn += $n;
            }


           
            $q = $this->db->query("SELECT `area` FROM `u_users` U WHERE `cCode` = '$oc' ");
            
            if ($q->num_rows() > 0){$z = $q->row()->area; $z = explode(",",$z); $zz = array(); for ($n = 0 ; $n < count($z); $n++){ $zz[$n] = "'".$z[$n]."'"; } $z = implode(",",$zz); }else{$z = ''; }
            $this->load->model('user_permissions');        
            if ($this->user_permissions->is_auth('level_1_authorization') == 1){$q1 = " AND (S.`status` = 'F' OR S.`is_HO_cashier` = 1 )"; }else{if ($this->user_permissions->is_auth('level_2_authorization') == 1){$q1 = " AND S.`status` = 'FF' "; }else{$q1 = ""; } }
            
            $n = $this->db->query("SELECT S.no FROM `t_gen_vou_app_rqsts_sum` S WHERE S.vocuher_type = 'gl_vouchers' AND NOT (S.`status` = 'A' OR S.`status` = 'RE' OR S.`status` = 'R') AND S.`bc` IN (SELECT B.bc FROM  m_branches B WHERE B.`area` IN ($z) ) $q1 ORDER BY S.`bc` , S.`method`, S.`nno` DESC, S.`date` DESC, S.`request_datetime` ASC ")->num_rows();
            if ($n > 0){
                $t .= " <a href = '".base_url()."?action=appvl_general_voucher'> ";    
                $t .= " <div class='notifi_holder'><!--<div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>-->";
                $t .= " You have ($n) new vocuher approval request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";
                $nn += $n;            
            }



            
            $n = $this->db->query("SELECT * FROM `t_approval` WHERE record_status = 'W' AND request_type='EL' AND NOW() < (`requested_datetime` + INTERVAL 4 MINUTE) ORDER BY requested_datetime")->num_rows();
            if ($n > 0){
                $t .= " <a href = '".base_url()."?action=appvl_over_advance'> ";    
                $t .= " <div class='notifi_holder'><!--<div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>-->";
                $t .= " You have ($n) new over advance approval request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";
                $nn += $n;
            }

            

            $n = $this->db->query("SELECT * FROM `t_approval` WHERE record_status = 'W' AND request_type='DI' AND NOW() < (`requested_datetime` + INTERVAL 9 MINUTE) ORDER BY requested_datetime DESC ")->num_rows();

            if ($n > 0){

                $t .= " <a href = '".base_url()."?action=appvl_discount'> ";        
                $t .= " <div class='notifi_holder'><div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>";
                $t .= " You have ($n) new discount approval request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";

                $nn += $n;

            }
            

        }else{

            $t  = '';

            $bc = $this->sd['bc'];

            $n = $this->db->query(" SELECT `no` FROM `t_fund_transfer_sum` s WHERE `from_bc` = '$bc' AND `status` IN (1) ORDER BY NO DESC")->num_rows();
            if ($n > 0){
                $t .= " <a href = '".base_url()."?action=t_fund_transfer#tabs-3'> ";        
                $t .= " <div class='notifi_holder'><!-- <div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>-->";
                $t .= " You have ($n) new incoming fund fransfer request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";
                $nn += $n;
            }



        }


        $a['notifi_count'] = $nn;
        $a['notifications_titles'] = $t;

        echo json_encode($a);

    }

    public function update_notifi_action(){

        $q = $this->db->where('id',$_POST['notifi_id']) ->set(  array(  'status'=>2, 'notifi_view_by'=>$this->sd['oc'], 'notifi_view_datetime'=>date('Y-m-d h:i:s') ) ) ->limit(1) ->update('t_notifications');

        if ($q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
        
    }








    
}


























































/*$n = $this->db->query(" SELECT * FROM `t_fund_transfer_sum` s WHERE NOT transfer_type = 'cheque_withdraw' AND (`from_bc` = '$bc' OR `from_bc` = 'HO') AND `status` IN (1)  ORDER BY `no` DESC")->num_rows();

            if ($n > 0){

                $t .= " <a href = '".base_url()."?action=t_fund_transfer#tabs-3'> ";        
                $t .= " <div class='notifi_holder'><div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>";
                $t .= " You have ($n) new incoming fund fransfer request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";

                $nn += $n;

            }

            $n = $this->db->query(" SELECT S.no FROM `t_fund_transfer_sum` S JOIN `t_fnd_trnsfr_rqsts_chq_det`   CRD ON S.`no`   = CRD.`no`  AND S.`requ_initi_bc`   = CRD.`bc` WHERE S.`transfer_type` = 'cheque_withdraw' AND S.`status` = 1 ORDER BY S.`requ_initi_bc` , S.`no` , CRD.`cheque_no`   ")->num_rows();

            if ($n > 0){

                $t .= " <a href = '".base_url()."?action=appvl_fund_transfers'> ";        
                $t .= " <div class='notifi_holder'><div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>";
                $t .= " You have ($n) new cheque withdrawal request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";

                $nn += $n;

            }


            $oc = $this->sd['oc'];
            
            $q = $this->db->query("SELECT `area` FROM `u_users` U WHERE `cCode` = '$oc' ");
            
            if ($q->num_rows() > 0){$z = $q->row()->area; $z = explode(",",$z); $zz = array(); for ($n = 0 ; $n < count($z); $n++){ $zz[$n] = "'".$z[$n]."'"; } $z = implode(",",$zz); }else{$z = ''; }
            $this->load->model('user_permissions');        
            if ($this->user_permissions->is_auth('level_1_authorization') == 1){$q1 = " AND (S.`status` = 'F' OR S.`is_HO_cashier` = 1 )"; }else{if ($this->user_permissions->is_auth('level_2_authorization') == 1){$q1 = " AND S.`status` = 'FF' "; }else{$q1 = ""; } }
            
            $n = $this->db->query("SELECT S.no FROM `t_gen_vou_app_rqsts_sum` S WHERE S.vocuher_type = 'gl_vouchers' AND NOT (S.`status` = 'A' OR S.`status` = 'RE' OR S.`status` = 'R') AND S.`bc` IN (SELECT B.bc FROM  m_branches B WHERE B.`area` IN ($z) ) $q1 ORDER BY S.`bc` , S.`method`, s.`nno` DESC, S.`date` DESC, S.`request_datetime` ASC ")->num_rows();
            
            if ($n > 0){

                $t .= " <a href = '".base_url()."?action=appvl_general_voucher'> ";    
                $t .= " <div class='notifi_holder'><div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>";
                $t .= " You have ($n) new vocuher approval request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";

                $nn += $n;

                $n = $this->db->query("SELECT * FROM `t_approval` WHERE record_status = 'W' AND request_type='EL' AND NOW() < (`requested_datetime` + INTERVAL 4 MINUTE) ORDER BY requested_datetime")->num_rows();
                
                if ($n > 0){

                    $t .= " <a href = '".base_url()."?action=appvl_over_advance'> ";    
                    $t .= " <div class='notifi_holder'><div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>";
                    $t .= " You have ($n) new over advance approval request(s)";        
                    $t .= " </div> ";
                    $t .= " </a> ";

                    $nn += $n;

                }
            
            }


            $n = $this->db->query("SELECT * FROM `t_approval` WHERE record_status = 'W' AND request_type='DI' AND NOW() < (`requested_datetime` + INTERVAL 9 MINUTE) ORDER BY requested_datetime DESC ")->num_rows();

            if ($n > 0){

                $t .= " <a href = '".base_url()."?action=appvl_discount'> ";        
                $t .= " <div class='notifi_holder'><div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>";
                $t .= " You have ($n) new discount approval request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";

                $nn += $n;

            }*/













            /*$n = $this->db->query(" SELECT `no` FROM `t_fund_transfer_sum` s WHERE `from_bc` = '$bc' AND `status` IN (1) ORDER BY NO DESC")->num_rows();

            if ($n > 0){

                $t .= " <a href = '".base_url()."?action=t_fund_transfer#tabs-3'> ";        
                $t .= " <div class='notifi_holder'><div class='ti-wallet' style='float:left; margin-right:10px;font-size:40px'></div>";
                $t .= " You have ($n) new incoming fund fransfer request(s)";        
                $t .= " </div> ";
                $t .= " </a> ";

                $nn += $n;

            }*/