<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_branch_backdate extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){        
        $this->load->model('m_branches');
        $a['is_bc_user'] = $this->m_branches->is_bc_user();
        $a['bc_dropdown_multi'] = $this->m_branches->bc_dropdown_multi();
        $a['max_no'] = $this->max_no();
		return $a;
    }

    public function max_no(){
        return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM `m_branch_backdate`")->row()->max_no;
    }
    
    public function save(){

        if ($_POST['hid'] == "0"){
            $_POST['no'] = $this->max_no();
        }else{
            $_POST['no'] = $_POST['hid'];
        }

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        } $_POST['bc'] = implode(',', $bc_ar); unset($_POST['bc_arry']);

        $_POST['added_by'] = $this->sd['oc'];
        $_POST['status'] = "active";


        if ($_POST['hid'] == "0"){
            unset($_POST['hid']);
            if ($this->db->insert("m_branch_backdate",$_POST)) {                
                 $a['s'] = 1;                
                }
            else {
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
            }

        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);

            $_POST['action_datetime'] = date('Y-m-d h:i:s');

            $Q = $this->db->where("no",$hid)->update("m_branch_backdate",$_POST);
            $a['s']= 1;            
        }
        
        echo json_encode($a);

    }

    public function setEdit(){
        
        $no = $_POST['no'];

        $Q = $this->db->query("SELECT * FROM `m_branch_backdate` WHERE NO = '$no'");

        if ($Q->num_rows() > 0){
            
            $a['s'] = 1;

            $a['sum'] = $Q->row();

            $bc_arry = explode(",",str_replace("'", "", $Q->row()->bc));
            $a['det'] = $bc_arry;           

        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }
    
    
    public function view_list(){

        $x1 = '"';
        $x = "'";
        $x2 = '"';

        $Q = $this->db->query("SELECT *, REPLACE (m_branch_backdate.bc,$x1$x$x2,'') AS `bc` ,IF (valid_upto < CURRENT_DATE,1,0) AS `exprd` , u_users.`discription` AS `added_by`   FROM m_branch_backdate JOIN u_users ON m_branch_backdate.`added_by` = u_users.`cCode`
 ORDER BY no DESC");

        if ($Q->num_rows() > 0){
            $a['det'] = $Q->result();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }
    
    public function delete(){
	   $code = $_POST['code'];
       
       if ($this->db->query("DELETE FROM`m_area` WHERE code = '$code' ")) {          
            $a['s'] = 1;
        }else{       
            $a['s'] = 0;            
            $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Area");
        }

        echo json_encode($a);
    }


    public function end(){

        $no = $_POST['no'];
        $Q  = $this->db->query("UPDATE m_branch_backdate SET `status` = 'ended' WHERE no = '$no' LIMIT 1 ");

        if ($Q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }



    

}