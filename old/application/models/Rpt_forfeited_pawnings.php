<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_forfeited_pawnings extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   


	function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,'XL');        
    }


	public function PDF_report($_POST_,$type='PDF'){

		ini_set('Max_execution_time',1200);		
		
		if (isset($_POST['bc_n'])){
	        if ( $_POST['bc_n'] == "" ){        
	            $bc = "";
	        }        
	    }else{

	        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
	            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
	        }

	        $bc = implode(',', $bc_ar);

	        if ($bc == ""){
	            $bc   = "";
	        }else{
	            $bc   = " AND  FS.bc IN ($bc)  ";
	        }
	    }

    // r_billtype filteration 
    $billtype = "";

    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }

		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];

		$Q = $this->db->query("SELECT 
B.`name` AS bc,L.billtype,
FD.`billno`,
L.ddate AS `pawn_date`,
LI.itemname,
LI.qty,
LI.pure_weight,
LI.value AS `market_price`,
L.`requiredamount` AS `advance_received`,
acurd_int_cal(FS.`bc`,FD.billno,FS.`date`) AS `int_up_to_date`,
IFNULL((SELECT SUM(D.document_charge) AS `document_charge` FROM `t_remind_letter_det` D WHERE D.`billno` = FD.`billno` AND D.`bc` = FS.`bc` AND D.`ignore_doc_charge` = 0),0) AS `other_cost`,
FS.`date` as `fdate`,
DATEDIFF(FS.date,L.ddate) AS `no_of_days`

FROM t_forfeit_sum FS

JOIN t_forfeit_det FD ON FS.`no` = FD.`no`
JOIN ( 	SELECT * FROM t_loan_re_fo l GROUP BY l.`billno` ) L ON FD.`billno` = L.billno
JOIN ( 	SELECT li.`billno`,li.`itemcode`,i.`itemname`,li.`qty`,li.`pure_weight`,li.`value`
	FROM t_loanitems_re_fo li 
	LEFT JOIN r_items i ON li.`itemcode` = i.`itemcode` 
	/*GROUP BY li.`billno`*/ ) LI ON FD.`billno` = LI.billno
JOIN m_branches B ON L.bc = B.`bc`

WHERE FS.`date` BETWEEN '$fd' AND '$td' $bc $billtype

ORDER BY L.bc,L.billtype,L.ddate,L.billno");

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;

			if ($type == 'PDF'){
				$this->load->view($_POST['by'].'_'.'pdf',$r_data);
			}else{
				$this->load->view($_POST['by'].'_'.'excel',$r_data);
			}

		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}