    <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S_module extends CI_Model {
    
    private $sd;
    private $mtb;
    private $mod = '019';
    
    function __construct(){
	parent::__construct();
	
        $this->load->library('session');
        $this->load->library('useclass');
        
    	$this->sd = $this->session->all_userdata();
    	
        $this->mtbl  = 'u_add_user_role';
        $this->mtbl2 = 'u_add_user_role_log';
        
        $this->mtb = $this->tables->tb['u_user_role'];
    	$this->load->model('user_permissions');
    }
    
    public function base_details(){
        $this->load->model('s_users');        
        $a['table_data'] = $this->data_table();
        $a['table_data_new'] = $this->data_table_new();
        $a['user'] = $this->s_users->select();
        $a['mod_group_name'] = $this->module_cat("name='mod_group_name'"," id='mod_group_name'");
        $a['added_modules_list'] = $this->added_modules_list();
        $a['m_code'] = $this->max_no();

        return $a;
    }

    public function max_no(){
        return str_pad($this->db->query('SELECT IFNULL(MAX(m_code)+1,1) AS `max_no` FROM `u_modules`')->row()->max_no, 3,0,STR_PAD_LEFT);
    }

    public function added_modules_list(){

        $Q = $this->db->order_by("mod_group_name,m_code DESC")->order_by("")->get("u_modules");
        
        if ($Q->num_rows() > 0){            
            
            $T = "";
            $str_g = "";            
            
            foreach ($Q->result() as $R) {
                
                if ( $R->mod_group_name != $str_g ){
                    $str_g = $R->mod_group_name;
                    $T .= "<tr class='$str_g group'><td colspan='4'><div class='gmod'>".str_replace("_", " ",$str_g)."</div></td></tr>";
                }   

                $T .= "<tr class='".$str_g."_H gxt'>";
                $T .= "<td style='padding:4px;padding-left:20px'><span class='module_txt'>". str_pad($R->m_code, 3,0,STR_PAD_LEFT) ."</span></td>";
                /*$module_name = str_replace("m_", "",$R->module_name); $module_name = str_replace("r_", "",$module_name); $module_name = str_replace("u_", "",$module_name); $module_name = str_replace("s_", "",$module_name); $module_name = str_replace("t_", "",$module_name);*/
                $T .= "<td style='width:400px'><span class='module_txt'>".$R->module_name."</span></td>";                    
                $T .= "<td><a class='update_m_module' code='".$R->m_code."' desc='".$R->module_name."' group='".$R->mod_group_name."'>Update</a code='".$R->m_code."'> | <a class='remove_m_module' code='".$R->m_code."'>Remove</a></td>";
                $T .= "</tr>";

            }            
            
            $a['T'] = $T;
            $a['s'] = 1;

        }else{
            $a['T'] = "";
            $a['s'] = 0;
        }

        return $a;

    }
    
    public function data_table(){
	   return $this->db->get($this->mtb)->result();
    }
    
    public function data_table_new(){
	return $this->db->get($this->mtbl)->result();
    }
    
    public function delete(){
	   
       $Q = $this->db->where("m_code", $_POST['code'])->delete("u_modules");

       if ($Q){
            $a['s'] = 1;
       }else{
            $a['s'] = 0;
       }

       echo json_encode($a);

    }

    public function save(){
        
        $Q1 = $this->db->query("SELECT * FROM u_modules WHERE `m_code` = '".$_POST['m_code']."'  LIMIT 1");
        if ($Q1->num_rows() > 0){$q1 = 1; }else{$q1 = 0; }

        $Q2 = $this->db->query("SELECT * FROM u_modules WHERE `module_name` = '".$_POST['module_name']."'  LIMIT 1");
        
        if ($Q2->num_rows() > 0){
            $q2 = 1; 
        }else{
            $q2 = 0; 
        }
                    
        if ($_POST['code_'] == "" ){            
            
            if ($q1 > 0 || $q2 > 0){
                $a['q1'] = $q1;
                $a['q2'] = $q2;
                $a['s'] = 0;
            }else{
                unset($_POST['code_']);                
                $this->db->insert("u_modules",$_POST);
                $a['s'] = 1;
            }

        }else{            
            $this->db   ->set(array("module_name"=>$_POST['module_name'], "mod_group_name"=>$_POST['mod_group_name'] )) ->where("m_code",$_POST['code_']) ->limit(1)->update("u_modules");
            $a['s'] = 2;            
        }        

        echo json_encode($a);	
    }
    
    public function dates(){
	$t = time(); $a = array(); $x = 0;
	foreach($this->db->get($this->mtb)->result() as $r){
	    $std = new stdClass;
	    
	    if($r->type == 2){ $r->range *= 7; }
	    $tt = $t - ($r->range * 84600);
	    
	    $std->date = date("Y-m-d", $tt);
	    $std->description = $r->description;
	    $std->key = substr(md5($r->description.$t), 0, 5);
	    
	    $a[] = $std; $x++;
	}
	
	return $a;
    }
    
    public function load(){
	   $q = $this->db->where('bc',$this->sd['bc'])->where('user_id', $this->input->post('id'))->get($this->mtbl);
	   $a['d'] = $q->result();
       echo json_encode($a);
    }

    public function module_cat($name,$id){
        
        //mod_group_name
        
        $T = "<select $name $id style='padding:10px;'>";
        $T .= "<option value=''>Select</option>";
        $T .= "<option value='Master_Data'>Master Data</option>";
        $T .= "<option value='Transaction_Modules'>Transaction Modules</option>";
        $T .= "<option value='Report_Modules'>Report Modules</option>";
        $T .= "<option value='Special_Transaction_Modules'>Special Transaction  Modules</option>";
        $T .= "<option value='System_Admin_Modules'>System Admin Modules</option>";
        $T .= "</select>";
    
        return $T;

    }

}