<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class t_user_activity extends CI_Model{
  
  private $sd;
  private $mtb;
  private $max_no;
  private $bc_glob;
   
  function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);    
    $this->load->model('user_permissions');
    
  }
  
  public function base_details(){
    $a['current_date']              = $this->sd['current_date'];
    $a['date_change_allow']         = $this->sd['date_chng_allow'];
    $a['backdate_upto']             = $this->sd['backdate_upto'];

    $this->load->model('M_branches');
    $a['bc_drop'] = $this->M_branches->select();

    return $a;
  }

  public function get_data(){
        $q = $_GET['term'];
        $query = $this->db->query("SELECT cCode,discription FROM u_users WHERE cCode LIKE '%$q%' OR discription LIKE '%$q%'");                
        $ary = array();
        
        foreach($query->result() as $R){ 
            $ary[] = $R->cCode." - ".$R->discription; 
        }
        
        echo json_encode($ary);
    }


    public function dta(){

        $bc  = $_POST['bc'];
        $mod = $_POST['transaction'];
        $oc  = $_POST['cCode'];
        $fd  = $_POST['fd'];
        $td  = $_POST['td'];
        $t   = '';


        $sql = "    SELECT t_user_activity_log.*, t_trans_code.`description` AS `trans_det` , u_users.`display_name` as `oc`  , m_branches.`name` AS `bc`
                    FROM `t_user_activity_log`  
                    LEFT JOIN t_trans_code ON t_user_activity_log.`trans_code` = t_trans_code.`code`
                    JOIN u_users ON t_user_activity_log.`oc` = u_users.`cCode`
                    JOIN m_branches ON t_user_activity_log.`bc` = m_branches.`bc`

                    WHERE t_user_activity_log.bc = '$bc' AND LEFT(date_time,10) BETWEEN '$fd' AND '$td' ";

        if ( $oc != "" ){
            $sql .= " AND oc = '$oc' ";
        }

        $sql .= "  ORDER BY log_no DESC";


        $q = $this->db->query($sql);

        $t  = "<br><br><table border='0' class='tbl_user_activity' width='100%'>";

        $t .= "<tr>";
        $t .= "<td>Log No</td>";
        $t .= "<td>Date Time</td>";
        $t .= "<td>Module</td>";
        $t .= "<td>Action</td>";
        $t .= "<td>Trans Code</td>";
        $t .= "<td>Trans No</td>";
        $t .= "<td>Branch</td>";
        $t .= "<td>Operator</td>";
        $t .= "<td>Note</td>";
        $t .= "</tr>";


        if ( $q->num_rows() > 0 ){

            foreach ($q->result() as $r) {
                
                $t .= "<tr>";
                $t .= "<td>".$r->log_no."</td>";
                $t .= "<td>".$r->date_time."</td>";
                $t .= "<td>".$r->module."</td>";
                $t .= "<td>".$r->action."</td>";
                $t .= "<td>".$r->trans_det."</td>";
                $t .= "<td>".$r->trans_no."</td>";
                $t .= "<td>".$r->bc."</td>";
                $t .= "<td>".$r->oc."</td>";
                $t .= "<td>".$r->note."</td>";
                $t .= "</tr>";

            }

        }


        $t .= "</table>";



        $a['d'] = $t;
        echo json_encode($a);



    }

    


    public function get_users_by_bc(){

        $bc = $_POST['bc'];
        $Q  = $this->db->query("SELECT cCode,loginName,discription FROM u_users WHERE bc = '$bc'");

        $t  = "";
        
        if ($Q->num_rows() > 0){

            $t .= "<option value=''>All users</option>";

            foreach ($Q->result() as $r) {                
                $t .= "<option value='".$r->cCode."'>".$r->discription." (".$r->cCode.")</option>";
            }

        }else{
            $t .= "<option value=''>No users found</option>";
        }

        

        $a['d'] = $t;
        $a['s'] = 1;

        echo json_encode($a);

    }









    

}