<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pawn_ticket_first_int extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report($loanno,$fullmonth_int = 0){

        $bc = $this->sd['bc'];

        $data1 = $this->db->query("SELECT C.`cusname`,C.`address`,C.`nicno`,C.`mobile`,L.`cus_serno`,L.`bc`,L.`ddate`,L.`billtype`,L.`bt_letter`,L.`billno`,L.`loanno`,L.`action_date`,L.`requiredamount` AS `requiredamount`,L.`fmintrate`,L.`fmintrate2`,L.`finaldate`,L.`period`,L.`stamp_fee`  FROM `t_loan` L JOIN `m_customer` C ON L.cus_serno = C.serno WHERE loanno = '$loanno' Limit 1");        

        if($data1->num_rows()>0){
            $data2 = $this->db->query("SELECT LT.`ddate`,LT.`transecode`,LT.`amount`,LT.`discount`,LT.`action_date` FROM `t_loantranse` LT
                                        WHERE LT.`loanno` = '$loanno' AND LT.bc = '$bc' AND LT.`transecode` = 'A' LIMIT 1");
        	$r_detail['R']	=	$data1->row();
            $r_detail['RR']  =   $data2->row();
            $r_detail['fullmonth_int'] = $fullmonth_int;
            
        	$this->load->view($_POST['by_i'].'_'.'pdf',$r_detail);
        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}