<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_company extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';
        $this->load->helper(array('form', 'url')); 
    }
    
    public function base_details(){		
        $a['company'] = $this->getCompanyDetails();
		return $a;
    }
    
    public function save(){

if(isset($_FILES['file_0'])){

$upload_path=FCPATH.'images/';


        $fileParts = pathinfo($_FILES['file_0']['name']);
        $file_name = "logo" . '.jpg';
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['file_name'] = $file_name;
        $config['overwrite'] = true; 

        $_POST['logo_pic']=$file_name;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);
        if ($this->upload->do_upload('file_0')){

            $configR['image_library'] = 'gd2';
            $configR['create_thumb'] = TRUE;
            $configR['thumb_marker'] = "";            
            $configR['source_image'] = $upload_path.'logo.jpg';
            $config['overwrite'] = true;    
            $config['upload_path'] = $upload_path;                     
            $configR['maintain_ratio'] = true;
            $configR['width'] = 800;
            $configR['height'] = 800;



            $this->load->library('image_lib', $configR); 
            $this->image_lib->resize();

         //   echo $this->image_lib->display_errors('<p>', '</p>');

            //print_r( $this->upload->data() );
        }else{
            //echo $this->image_lib->display_errors('<p>', '</p>');
        
        }
}
        if ($_POST['hid'] == 0){
            unset($_POST['hid']);
        	$Q = $this->db->insert("m_company",$_POST);
        }else{            
            unset($_POST['hid']);
            $Q = $this->db->update("m_company",$_POST);
        }

        if ($Q){

            echo 1;
        }else{
            echo 0;
        }
    }

    public function getCompanyDetails(){
        return $this->db->query("SELECT * FROM m_company")->row();
    }
    


    public function up(){


        $fileParts = pathinfo($_FILES['image']['name']);
        $file_name = "Logo" . '.' . $fileParts['extension'];
        $config['upload_path'] = '../sew/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $config['file_name'] = $file_name;
        $config['overwrite'] = true; 
        


        $this->load->library('upload', $config);
        $this->upload->initialize($config);


        if ($this->upload->do_upload('image')){
            print_r( $this->upload->data() );
        }else{
            echo $this->upload->display_errors('<p>', '</p>');
        
        }




    }


}