<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_pawning_activity extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function base_details(){		
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_,$r_type = "pdf"){


    // $bc = $_POST_['bc'];
    
    // if ($bc == ""){
    //     $bc = "";
    // }else{
    //     $bc = "AA.bc = '$bc' AND ";
    // }

    if (isset($_POST['bc_n'])){
        
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
            $bc_AA = "";
        }        

    }else{

        if ( !is_array($_POST['bc_arry']) ){
            
            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
            
            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
            }

            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
        }        

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);
        $bc_AA= $bc;

        if ($bc == ""){
            $bc   = "";
            $bc_AA   = "";
        }else{
            $bc   = "   L.bc IN ($bc) AND ";
            $bc_AA   = "   AA.bc IN ($bc_AA) AND ";
        }

        

    }


    // r_billtype filteration 
    $billtype = "";
    $billtype_AA = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
        $billtype_AA = " AND AA.`billtype`='".$_POST['r_billtype']."'";
    }

    $fd = $_POST['from_date'];
    $td = $_POST['to_date'];

    $Q = $this->db->query("SELECT * FROM (SELECT L.bc,B.`name` AS `bc_name`,L.loanno,LT.`billno`,L.`ddate` AS `pawn_date`,L.`totalweight`,L.`requiredamount`, L.billtype,
'' AS `redeem_amount`,
'' AS `status`,
'' AS `redeem_date`,
LI.`items`,L.`cus_serno`,C.`cusname`,C.`address`,C.`nicno`,C.`mobile`,'###.##' AS `outstanding_int`,LI.pure_weight_tot

FROM `t_loan` L

JOIN (  SELECT GROUP_CONCAT(lt.`transecode`) AS transecode ,lt.amount,lt.ddate,lt.`bc`,lt.`billtype`,lt.`billno`
    FROM `t_loantranse` lt 
    GROUP BY lt.`loanno`      
     ) LT ON L.`bc` = LT.bc AND L.`billtype` = LT.billtype AND L.`billno` = LT.billno
    
JOIN (  SELECT  GROUP_CONCAT(CONCAT(' ', i.`itemname`,' (',`pure_weight`,')'  )) AS `items`, li.`bc`,li.`billtype`,li.`billno`, SUM(li.`pure_weight`) AS `pure_weight_tot`    
    FROM `t_loanitems` li 
    JOIN `r_items` i ON li.`itemcode` = i.`itemcode`    
    GROUP BY li.`loanno`  
     ) LI ON L.`bc` = LI.bc AND L.`billtype` = LI.billtype AND L.`billno` = LI.billno
     
JOIN `m_branches` B     ON L.`bc` = B.`bc`
JOIN `m_customer` C     ON L.`cus_serno` = C.`serno` 

WHERE $bc L.`status` = 'P' AND L.`ddate` BETWEEN '$fd' AND '$td' $billtype

GROUP BY L.`bc`,L.`billtype`,L.`billno`

UNION ALL

SELECT L.bc,B.`name` AS `bc_name`,L.loanno,LT.`billno`,L.`ddate` AS `pawn_date`,L.`totalweight`,L.`requiredamount`,L.billtype,
LT. `redeem_amount`,
LT.`status`,
LT.`redeem_date`,
LI.`items`,L.`cus_serno`,C.`cusname`,C.`address`,C.`nicno`,C.`mobile`,'###.##' AS `outstanding_int`,LI.pure_weight_tot

FROM `t_loan_re_fo` L

JOIN (  SELECT GROUP_CONCAT(lt.`transecode`) AS transecode ,MAX(IF (lt.`transecode` IN ('R'), lt.amount ,'')) AS `redeem_amount`,MAX(IF (lt.`transecode` IN ('R'),'Redeemed','')) AS `status`,MAX(IF (lt.`transecode` = 'R',lt.`ddate`,'')) AS `redeem_date`,lt.`bc`,lt.`billtype`,lt.`billno`
    FROM `t_loantranse_re_fo` lt 
    GROUP BY lt.`loanno`      
     ) LT ON L.`bc` = LT.bc AND L.`billtype` = LT.billtype AND L.`billno` = LT.billno
    
JOIN (  SELECT  GROUP_CONCAT(CONCAT(' ', i.`itemname`,' (',`pure_weight`,')'  )) AS `items`, li.`bc`,li.`billtype`,li.`billno`,SUM(li.`pure_weight`) AS `pure_weight_tot`    
    FROM `t_loanitems_re_fo` li 
    JOIN `r_items` i ON li.`itemcode` = i.`itemcode`    
    GROUP BY li.`loanno`  
     ) LI ON L.`bc` = LI.bc AND L.`billtype` = LI.billtype AND L.`billno` = LI.billno
     
JOIN `m_branches` B     ON L.`bc` = B.`bc`
JOIN `m_customer` C     ON L.`cus_serno` = C.`serno` 

WHERE $bc L.`status` = 'P' AND L.`ddate` BETWEEN '$fd' AND '$td' $billtype

GROUP BY L.`bc`,L.`billtype`,L.`billno`

) AS AA WHERE $bc_AA AA.pawn_date BETWEEN '$fd' AND '$td' $billtype_AA

ORDER BY AA.bc,AA.pawn_date,AA.billno");

    if($Q->num_rows() > 0){

        $r_data['list'] = $Q->result();
        $r_data['fd']   = $fd;
        $r_data['td']   = $td;
        // $X              = $this->load->model('calculate');
        // $r_data['X']    = $X;       

        if ($r_type == "pdf"){
            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{
            $this->load->view($_POST['by'].'_'.'excel',$r_data);
        }

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

public function Excel_report()
    {
        $this->PDF_report($_POST,"Excel");
    }

}