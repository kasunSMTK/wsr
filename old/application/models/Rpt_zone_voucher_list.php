<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_zone_voucher_list extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $fd       = $_POST_['from_date'];
    $td       = $_POST_['to_date'];    
    $vou_stat = $_POST['vou_stat'];
    $zone     = $this->sd['zone'];


    $z = $zone;
    $z = explode(",",$z);
    $zz = array();
    for ($n = 0 ; $n < count($z); $n++){ $zz[$n] = "'".$z[$n]."'"; }
    $z = implode(",",$zz);

    if ($vou_stat == "All"){    
        $stat = "";
    }else{
      $stat = " S.`status` = 'W' AND ";
    }

   
    $sql="SELECT 
V_BC2.`name` AS `bc_name`,
VS.`bc`,S.`nno`,S.`type`,VS.`status` ,
CONCAT(S.`paid_acc`,' - ', ACC1.`description`) AS `paid_acc_desc`,
CONCAT(D.`acc_code`,' - ', ACC2.description) AS `acc_code`,
CONCAT(D.v_bc,' - ' ,  V_BC.`name`) AS `v_bc_name`,
CONCAT(V_CLASS.`code`,' - ',V_CLASS.`description`) AS `v_class`,
D.`amount`,S.`action_date`,S.`note`, S.`payee_desc`

FROM `t_gen_vou_app_rqsts_sum` VS

JOIN `t_voucher_gl_sum` S ON VS.`bc` = S.`bc` AND VS.`nno` = S.`nno` AND VS.`paid_acc` = S.`paid_acc`
JOIN `t_voucher_gl_det` D ON S.`bc` = D.`bc` AND S.`nno` = D.`nno` AND S.`paid_acc` = D.`paid_acc`
JOIN m_account ACC1 ON S.`paid_acc` = ACC1.`code`
JOIN m_account ACC2 ON D.`acc_code` = ACC2.`code`

JOIN m_branches V_BC ON D.`v_bc` = V_BC.`bc`
JOIN m_branches V_BC2 ON VS.`bc` = V_BC2.`bc`

JOIN m_voucher_class V_CLASS ON D.`v_class` = V_CLASS.`code`

WHERE VS.`bc` IN (SELECT bc FROM `m_branches` B WHERE B.`area` IN ($z))

ORDER BY VS.`bc`, VS.`request_datetime` DESC, VS.`nno` ";
    
    $query = $this->db->query($sql);


    if($this->db->query($sql)->num_rows()>0){

        $r_data['det'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);

    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}