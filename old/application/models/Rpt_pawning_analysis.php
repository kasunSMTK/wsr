<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_pawning_analysis extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    
    if($_POST['h_bc']!=""){
        $QBC = " AND a.bc='".$_POST['h_bc']."' ";
    }else{
        $QBC = "";
    }

   
      $sql="SELECT a.bc , b.`name` AS `name` , SUM(IFNULL(a.requiredamount,0)) AS `A` , SUM(IFNULL(a.pure_weight,0)) AS `B`,( ( SUM(IFNULL(a.requiredamount,0)) / SUM(IFNULL(a.pure_weight,0)) ) * 8 ) AS `C` FROM (

    SELECT L.`ddate`,L.requiredamount , L.`bc` , L.`loanno` , SUM(LI.`pure_weight`) AS pure_weight
    FROM `t_loan` L
    JOIN  t_loanitems LI ON L.`bc` = LI.`bc` AND L.`billno` = LI.`billno`
    WHERE L.ddate BETWEEN '$fd' AND '$td' AND L.`status` = 'P'
    GROUP BY L.`bc`,L.`billtype`,L.`billno`

    UNION ALL

    SELECT L.`ddate`,L.`requiredamount` , L.`bc` , L.`loanno` , SUM(LI.`pure_weight`) AS pure_weight
    FROM `t_loan_re_fo` L   
    JOIN  t_loanitems_re_fo LI ON L.`bc` = LI.`bc` AND L.`billno` = LI.`billno`
    WHERE L.ddate BETWEEN '$fd' AND '$td' AND L.`status` IN ('P','AM','R','RN','L','PO') 
    GROUP BY L.`bc`,L.`billtype`,L.`billno`

) a 

JOIN m_branches b ON a.bc = b.`bc`

WHERE 1=1 $QBC

GROUP BY bc ";
           


    

    $query = $this->db->query($sql);


    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        // $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        $this->load->view($_POST['by'].'_'.'excel',$r_data);

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

public function Excel_report(){
    $this->PDF_report($_POST,"Excel");
}



}