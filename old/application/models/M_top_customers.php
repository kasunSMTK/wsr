<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_top_customers extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){                
        $a['a'] = '';
		return $a;
    }

    public function view_list(){

        $x1 = '"';
        $x = "'";
        $x2 = '"';

        $Q = $this->db->query("SELECT A.`cus_serno`,B.`cusname` ,B.`nicno` , B.`cus_monitor`,B.`allow_total_pawn_amount`, FORMAT(SUM(requiredamount),2) AS `total`, BA.name as bc
FROM `t_loan` A
JOIN m_customer B ON A.`cus_serno` = B.`serno`
join m_branches BA on A.bc = BA.bc
WHERE A.`status` = 'P'
GROUP BY cus_serno 
HAVING SUM(requiredamount) >= (SELECT pawner_allow_max FROM m_pawning_values)
ORDER BY SUM(requiredamount) DESC
");

        if ($Q->num_rows() > 0){
            $a['det'] = $Q->result();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }
 
    public function cus_moni_update(){

        $q = $this->db->query("UPDATE m_customer SET cus_monitor = '".$_POST['set']."' WHERE serno = '".$_POST['cus_serno']."' LIMIT 1 ");

        if ($q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        $a['set'] = $_POST['set'];

        echo json_encode($a);

    }


    public function cus_max_allow_amount(){

        $q = $this->db->query("UPDATE m_customer SET allow_total_pawn_amount = '".$_POST['max_amount']."' WHERE serno = '".$_POST['cus_serno']."' LIMIT 1 ");

        if ($q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }        

        echo json_encode($a);

    }


}