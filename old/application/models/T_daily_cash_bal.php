<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_daily_cash_bal extends CI_Model {

	private $sd;
    private $max_no;

    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function base_details(){		
      $a['date_change_allow']   = $this->sd['date_chng_allow'];
      $a['current_date']        = $this->sd['current_date'];
      return $a;
  }

  public function save(){

    $_POST['bc'] = $this->sd['bc'];
    $_POST['oc'] = $this->sd['oc'];
    
    if ($_POST['code'] == 0){
        if (   $this->db->where(array("bc"=> $this->sd['bc'], "date",$_POST['date']))->get("t_bc_daily_cash_balance")->num_rows()     == 0 ){        
            unset($_POST['code']);
            $this->db->insert("t_bc_daily_cash_balance",$_POST);
            $a['s'] = 1;
        }else{
            $a['s'] = 2;
        }
    }else{

        unset($_POST['code']);

        $this->db->where(array("bc"=>$this->sd['bc'],"date"=>$_POST['date']))->update("t_bc_daily_cash_balance",array("amount"=>$_POST['amount']));
        $a['s'] = 1;

    }

    echo json_encode($a);

  }

  public function load(){

    $Q = $this->db->where("bc",$this->sd['bc'])->order_by("date desc")->get("t_bc_daily_cash_balance");

    if ($Q->num_rows() > 0){
        $a['d'] = $Q->result();
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

  }

  public function set_edit(){

    $Q = $this->db->where(array("bc"=>$this->sd['bc'],"date"=>$_POST['d']))->get("t_bc_daily_cash_balance");

    if ($Q->num_rows() > 0){
        $a['d'] = $Q->row();
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

  }


    public function delete(){

        if ($this->db->where(array(  "bc" => $this->sd['bc'],"date" => $_POST['d']  ))->delete("t_bc_daily_cash_balance")){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }


}
