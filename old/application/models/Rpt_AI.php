<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_AI extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }

    function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,'XL');        
    }
    
    function PDF_report($a,$rt = 'PDF'){

        if ($rt == 'PDF'){
            echo "This report only available in Excel format";
            exit;
        }

        $cid    = explode(",",$_POST['ccgr_id']);
        $cival  = explode(",",$_POST['ccgr']);
        $create_datetime    = date('Y-m-d H:i:s');
        $rpt_session_id     = rand();

        for ($n = 0 ; $n < count($cid) ; $n++) {            
            
            $aaa[] = array(
                'rpt_session_id' => $rpt_session_id,
                'gold_type_id' => $cid[$n],
                'gold_mrkt_value' => $cival[$n],
                'create_datetime' => $create_datetime
            );            

        }

        $this->db->insert_batch("r_ai_gold_rate",$aaa);

        if($_POST['bc_arry'] === null){
            $BC = " ";            
        }else{
            
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            $BC = " AND L.bc IN ($bc)  ";
        }

        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];

        $this->db->trans_start();        
        
        $this->db->query("SET @NOD:=@MW:=@IR:=@WI:=''");

        $q = $this->db->query(" 

        

            SELECT '' as `Serial_No`, L.bc, L.`ddate`, L.`billtype`, L.billno,L.int_cal_changed, L.`totalweight`, L.`requiredamount` AS `pawning_advance`, 
@NOD:=DATEDIFF('".$td."',L.`ddate`) AS `number_of_days`, 
@MW:=IF(L.`is_weelky_int_cal`=1,'W','M') AS `monthly_weekly`, /* get if weelky week rate, monthly month rate */
@IR:=(L.`fmintrate` + L.`fmintrate2`) AS `int_rate`,
@WI := IF(    @MW = 'W',(SELECT rate FROM `t_loan_bill_int` WHERE loan_no = L.loanno LIMIT 1), 0.00  ) AS `Weekly_Int`,
@IC:=IF(@MW='W',L.`requiredamount`*(@WI/100),IF(@MW='M',L.`requiredamount`* (CAST(@IR AS DECIMAL(12,2)) / 100),0)) AS `interest_charged`,
@NN:=(L.`requiredamount` * (( CAST(@IR AS DECIMAL(12,2)) /100)/30) * @NOD-@IC) AS `noname`,
@AI:=IF(@NN>0,@NN,0) AS `Accrued_Interest`,
@TR:=@AI+L.`requiredamount` AS `Total_Recoverable`,
@MRKT_VAL:= IFNULL(MRKT_V.mrkt_val,'The table (r_ai_gold_rate) must have gold rates values added by user manually. This table clear after generating report by php code. Please inspect near the line no 82 in Rpt_AI.php. ') AS `Market_value`,
@MRKT_VAL - @TR AS `Impairment`

FROM `t_loan` L 

JOIN (  SELECT i.billno, SUM((i.`pure_weight` * agr.`gold_mrkt_value` ) / 8) AS mrkt_val 
        FROM t_loanitems i
        LEFT JOIN `r_ai_gold_rate` agr ON i.`goldtype` = agr.`gold_type_id`
        WHERE agr.rpt_session_id = '".$rpt_session_id."'
        GROUP BY i.billno ) MRKT_V ON L.billno = MRKT_V.billno

WHERE L.ddate <= '".$td."' $BC /* AND L.billno = '10200909910004' */
ORDER BY L.bc, L.billtype,L.ddate ");        
        //$this->db->where("rpt_session_id",$rpt_session_id)->delete("r_ai_gold_rate");
        $this->db->trans_complete();


        /*$n = 0;

        foreach ($q->result() as $r) {

            if ($n==0){
                    
                echo "pawning_advance = ". $r->pawning_advance."<br>";
                echo "number_of_days = ". $r->number_of_days."<br>";
                echo "monthly_weekly = ". $r->monthly_weekly."<br>";
                echo "int_rate = ". $r->int_rate."<br>";
                echo "Weekly_Int = ". $r->Weekly_Int."<br>";
                echo "interest_charged = ". $r->interest_charged."<br>";
                echo "noname = ". $r->noname."<br>";
                echo "Accrued_Interest = ". $r->Accrued_Interest."<br>";
                echo "Total_Recoverable = ". $r->Total_Recoverable."<br>";
                echo "Market_value = ". $r->Market_value."<br>";
                echo "Impairment = ". $r->Impairment."<br>";

            }

            $n = 1;
        }



        exit;*/




        if($q->num_rows()>0){
            
            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            

            $this->load->view($_POST['by'].'_'.'excel',$r_detail);

        }else{
            echo "<script>alert('No data found');close();</script>";
        }        

    }   
    
}