<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_opening_pawn extends CI_Model {
    
    private $sd;
    private $mtb;
    private $max_no;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){		
        $this->load->model("m_customer");

        $a['current_date']      = $this->sd['current_date'];
        $a['item_category']     = $this->getItemCategory();
        $a['gold_q']            = $this->getGoldQuality();
        //$a['bcCustomer']      = $this->getBcCustomer();        
        $a['customer']          = $this->m_customer->getCustomer();
        $a['billtype_dropdown'] = $this->billtype_dropdown();
        $a['conditions']        = $this->conditions_dropdown();
        $a['bc_no']             = $this->sd['bc_no'];

        return $a;
    }

    public function conditions_dropdown(){

        $Q = $this->db->query("SELECT * FROM `r_condition`");

        $t = "<select class='select_drp' style='width:140px' id='condition_desc'>";

        if ($Q->num_rows() > 0){

            foreach ($Q->result() as $R) {
                $t .= "<option value='".$R->code."'>".$R->code. " ". $R->des."</option>";
            }

        }else{
            $t .= "<option value=''>No conditions added</option>";
        }

        $t .= "</select>";

        return $t;
    }
    
    public function billtype_dropdown(){
        $bc = $this->sd['bc'];
        
        $query = $this->db->query("SELECT billtypeno,billtype FROM `r_bill_type_sum` WHERE (bc = '$bc' OR bc = 'all') AND is_old_bill = '1'");                
        
        $d = "";
        
        foreach($query->result() as $r){
            
            if ($r->billtype == "LG"){
                $d .= "<optgroup label='LG'>";                
                $d .= "<option bt_no='201' value='".$r->billtype."'>B</option>";
                $d .= "<option bt_no='202' value='".$r->billtype."'>C</option>";
                $d .= "<option bt_no='203' value='".$r->billtype."'>D</option>";
                $d .= "<option bt_no='204' value='".$r->billtype."'>T</option>";

            }

            if ($r->billtype == "LG1"){
                $d .= "<optgroup label='LG1'>";                
                $d .= "<option bt_no='301' value='".$r->billtype."'>E</option>";
                $d .= "<option bt_no='302' value='".$r->billtype."'>G</option>";
                $d .= "<option bt_no='303' value='".$r->billtype."'>F</option>";
                $d .= "<option bt_no='304' value='".$r->billtype."'>H</option>";
            }

            if ($r->billtype == "NG"){
                $d .= "<optgroup label='NG'>";                
                $d .= "<option bt_no='205' value='".$r->billtype."'>B</option>";
                $d .= "<option bt_no='206' value='".$r->billtype."'>C</option>";
                $d .= "<option bt_no='207' value='".$r->billtype."'>D</option>";
                $d .= "<option bt_no='208' value='".$r->billtype."'>T</option>";

            }

            if ($r->billtype == "NG1"){
                $d .= "<optgroup label='NG1'>";                
                $d .= "<option bt_no='305' value='".$r->billtype."'>E</option>";
                $d .= "<option bt_no='306' value='".$r->billtype."'>G</option>";
                $d .= "<option bt_no='307' value='".$r->billtype."'>F</option>";
                $d .= "<option bt_no='308' value='".$r->billtype."'>H</option>";
            }


        }    
        
        return $d;
    }

    public function save(){
        
        $this->db->trans_begin();
        $this->load->model(array('calculate','user_permissions'));

        $loan_no        = $this->calculate->get_next_new_loan_number();
        $bc             = $this->sd['bc'];
        $customer_id    = $_POST['customer_id'];                        
        $billtype       = $_POST['billtype'];
        
        $_POST['billno']= $_POST['bc_no'].$_POST['b_days'].$_POST['bt_no'].$_POST['billno'];
        $billno         = $_POST['billno'];
        $dDate          = $_POST['dDate'];     
        $INT            = "";   
        $action_date    = date('Y-m-d H:i:s');
        $with_int       = 0;
        $current_date   = $dDate;

        if (isset($_POST['allow_cal_with_advance'])){
            $_POST['int_with_amt'] = 1;
        }else{
            $_POST['int_with_amt'] = 0;
        }


        if (isset($_POST['allow_full_month'])){
            $_POST['is_weelky_int_cal'] = 0;
        }else{
            $_POST['is_weelky_int_cal'] = 1;
        }

        unset($_POST['allow_cal_with_advance'],$_POST['allow_full_month']);

        for ( $no = 0 ; $no < count( $_POST['A'] ) ; $no++ ){            
            
            if ($_POST['is_bulk'] != 1){
                $itemcode_ = $_POST['B'][$no];
                $_POST['bulk_items'] = "";
            }else{
                $itemcode_ = $this->db->query("SELECT itemcode FROM `r_items` WHERE bulk_item_code = 1 LIMIT 1")->row()->itemcode;
                $_POST['bulk_items'] = $_POST['B'][$no];
            }

            $loan_item_det[] = array('loanno'=>$loan_no,'bc'=>$bc,'billtype'=>$_POST['billtype'], 'billno'=>$billno, 'cat_code'=>$_POST['A'][$no], 'itemcode'=>$itemcode_,'con'=>$_POST['C'][$no],'goldweight'=>$_POST['F'][$no],'pure_weight'=>$_POST['G'][$no], 'qty'=>$_POST['E'][$no] , 'goldtype'=>$_POST['D'][$no],'quality'=>$_POST['I'][$no],'goldvalue'=>$_POST['J'][$no],'value' => $_POST['H'][$no],'bulk_items' => $_POST['bulk_items']  );
        }

        $_POST['loanno']    = $loan_no;
        $_POST['bc']        = $bc;        
        $_POST['finaldate'] = date('Y-m-d',strtotime("+".$_POST['period']." months", strtotime($dDate)));
        $_POST['status'] = "P";
        $_POST['nmintrate'] = $_POST['fMintrate'];        
        
        if ($_POST['is_pre_intst_chargeable'] == 1){
            $_POST['fmintrest'] = floatval($_POST['fmintrest']);
        }else{
            $_POST['fmintrest'] = floatval($_POST['fmintrest']);
        }       

        $x = $this->calculate->get_int_paid_till_date($last_int_paid_date = $current_date,$_POST['is_weelky_int_cal'],true,0,0,$_POST['dDate']);

        $_POST['int_paid_untill'] = $x['till_date'];

        $_POST['oc'] = $this->sd['oc']; 
        $_POST['by_approval'] = $_POST['approval_id'] != 0 ? $_POST['approval_id'] : 0;
        unset($_POST['approval_id']);

        unset( $_POST['A'],$_POST['B'],$_POST['C'],$_POST['D'],$_POST['E'],$_POST['F'],$_POST['G'],$_POST['H'],$_POST['I'],$_POST['J'],$_POST['customer_id'],$_POST['is_pre_intst_chargeable'],$_POST['bulk_items'],$_POST['is_bulk']);

        if ($this->check_bill_no(1) > 0){
            $this->db->trans_rollback();
            $a['s'] = 2;
            $a['msg'] = "This bill number already added";
            echo json_encode($a);
            exit;
        }

        if ($_POST['hid'] == 0){
            
            $this->load->model('calculate');

            unset($_POST['hid'],$_POST['bc_no'],$_POST['b_days'],$_POST['bt_no']);

            // loan tbale
        	$Q  = $this->db->insert("t_loan",$_POST);
         
            $this->calculate->loan_bill_int($loan_no,$bc,$billtype);

            if ($Q){

                // Loan Items Table
                $Q = $this->db->insert_batch("t_loanitems",$loan_item_det); 

                // Loan Trans Table advance entry ( )            
                $_POST_TRANS['loanno'] = $loan_no; $_POST_TRANS['bc'] = $bc; $_POST_TRANS['billtype'] = $_POST['billtype'];$_POST_TRANS['billno'] = $billno; $_POST_TRANS['ddate'] = $dDate; $_POST_TRANS['amount'] = $_POST['requiredamount'];$_POST_TRANS['billextendedperiod'] = ""; $_POST_TRANS['transecode'] = "P"; $_POST_TRANS['transeno'] = $loan_no;$_POST_TRANS['discount'] = 0; $_POST_TRANS['app_rec_id'] = ""; $_POST_TRANS['action_date'] = $action_date;            
                $Q = $this->db->insert("t_loantranse",$_POST_TRANS);

                // Loan trans table first ## days interest                
                
                $paying_amount = floatval($_POST['fmintrest']);

                $_PST['loanno']    = $_POST['loanno']; $_PST['bc']        = $this->sd['bc'];$_PST['billtype']  = $_POST['billtype'];$_PST['billno']    = $_POST['billno'];
                $_PST['ddate']     = $dDate; // ask what date should here  loan date + 7 days or loan date
                $_PST['amount']    = $paying_amount;
                $_PST['transecode']= "A";
                
                $this->load->model("t_part_payment");                
                $_PST['transeno']  = $this->t_part_payment->getPartPayTransNo();
                $_PST['discount']  = 0;
                $_PST['is_pawn_int'] = 1;
                                              
                if ($paying_amount > 0){    $Q1 = $this->db->insert("t_loantranse",$_PST);  $with_int = 1;   }

                $_POST['hid']   = 0;
                $_POST['date']  = $_POST['dDate'];

                $_POST['ln'] = $loan_no;
                $_POST['bt'] = $billtype;
                $_POST['bn'] = $billno;

                $account_update =   $this->account_update(0);                
                
                if($account_update!=1){
                    $a['s'] = 0;
                    $a['m'] = "Invalid account entries";
                    $this->db->trans_rollback();
                    $a['s'] = "error";
                    echo json_encode($a);
                    exit;
                }else{
                    $account_update =   $this->account_update(1);
                }



            }else{
                $a['s'] = 0;
            }

        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid']);
            $Q = $this->db->where("recid",$hid)->update("t_loan",$_POST);
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{
            
            $this->db->trans_commit();

            if ($Q){
                $a['s'] = 1;
                $a['loan_no'] = $loan_no;
                $a['INT'] = $INT;                
                $this->calculate->update_customer_pawn_details($_POST['cus_serno'],'P');

            }else{
                $a['s'] = 0;
            }
        }
        
        $a['with_int'] = $with_int;

        echo json_encode($a);

    }


    public function check_bill_no($s = 0){      

        $bc         = $this->sd['bc'];
        $billtype   = $_POST['billtype'];
        $billno     = $_POST['billno'];

        $Q_CHECK_EXIST = $this->db->query("SELECT `loanno` FROM `t_loan` WHERE `bc` = '$bc' AND `billtype` = '$billtype' AND `billno` = '$billno'UNION ALL SELECT `loanno` FROM `t_loan_re_fo` WHERE `bc` = '$bc' AND `billtype` = '$billtype' AND `billno` = '$billno'LIMIT 1 ");

    /*    //-- bill number search might be delay because this query with large amount of data 
                $Q4 = $this->db->query("SELECT L.billtype FROM `t_loan` L WHERE billno = '$billno' AND L.bc = '$bc' ");
                if ($Q4->num_rows() > 0){ $a['billtype_with_same_bill_number_A'] = $Q4->result(); }
        //---------------------------------------------------------------------------------
        //-- bill number search might be delay because this query with large amount of data 
            $Q5 = $this->db->query("SELECT L.billtype FROM `t_loan_re_fo` L WHERE billno = '$billno' AND L.bc = '$bc' ");
            if ($Q5->num_rows() > 0){ $a['billtype_with_same_bill_number_B'] = $Q5->result(); }
        //---------------------------------------------------------------------------------

    */
        
        if ($s == 1){
            return $Q_CHECK_EXIST->num_rows();
        }else{
            $a['is_bill_number_exist'] = $Q_CHECK_EXIST->num_rows();
            echo json_encode($a);
        }

    }

    public function setBillTypeValue($amount="",$goldvalue="",$return_type=""){

        $bc = $this->sd['bc'];
        if (isset($_POST['amount'])){ $amount = $_POST['amount']; }else{ $amount = $amount; }
        if (isset($_POST['goldvalue'])){ $goldvalue  = $_POST['goldvalue']; }else{ $goldvalue  = $goldvalue; }

        $Q = $this->db->query("SELECT S.billtype, S.period, D.`day_from`, D.`day_to`, D.`rate`, S.`weekly_cal` FROM  `r_bill_type_sum` S JOIN `r_bill_type_det` D ON S.`no` = D.`no` WHERE  '$amount' BETWEEN `amt_from` AND `amt_to` AND CASE WHEN `gratr_lesstn` = 'B' THEN (('$amount' / '$goldvalue') * 100) BETWEEN `percentage` AND `percentage_to` WHEN `gratr_lesstn` = 'L' THEN (('$amount' / '$goldvalue') * 100) <= `percentage` ELSE  (('$amount' / '$goldvalue') * 100) > `percentage` END AND bc = '$bc' ");

        if ($Q->num_rows() > 0){
            $billtype = $Q->first_row()->billtype;            
            
            $Q2 = $this->db->query("")->row()->billno;

            $a['bt_sum'] = $Q->result();
            $a['bt_max'] = $Q2;
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        if ($return_type == ""){
            echo json_encode($a);
        }else{
            return $a;
        }

    }

    public function LOAD_LOAN(){
        $loan_no = $_POST['loan_no'];
        $bc      = $this->sd['bc'];

        if ($this->db->query("SELECT loanno FROM t_loan WHERE loanno = '$loan_no' LIMIT 1")->num_rows() != 0){
            $tbl_name_L     = "t_loan";
            $tbl_name_LT    = "t_loantranse";
            $tbl_name_LI    = "t_loanitems";
        }else{
            $tbl_name_L     = "t_loan_re_fo";
            $tbl_name_LT    = "t_loantranse_re_fo";
            $tbl_name_LI    = "t_loanitems_re_fo";
        }

        $Q1 = $this->db->query("SELECT * , LT.`transeno` FROM `$tbl_name_L` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` JOIN `$tbl_name_LT` LT ON L.`loanno` = LT.`loanno` AND L.`bc` = LT.`bc` WHERE L.`bc` = '$bc' AND L.loanno = '$loan_no' LIMIT 1 ");
        
        if ($Q1->num_rows() > 0){
            $a['loan_sum'] = $Q1->row();
            
            $Q2 = $this->db->query("SELECT LI.`itemcode`,I.`itemname`,LI.`cat_code`,IC.`des`,C.`des` as `cdes`,LI.`con`,LI.`goldweight`,LI.`pure_weight`,LI.`goldtype`,LI.`quality`,LI.`goldvalue`,LI.`value`
            
            ,GR.`id`,GR.`goldcatagory`
            ,GC.`code`,GC.`rate`,LI.`qty`,LI.`bulk_items`

            FROM `$tbl_name_LI` LI
                JOIN `r_itemcategory` IC ON LI.`cat_code` = IC.`code`
                JOIN `r_items` I ON LI.`itemcode` = I.`itemcode`
                JOIN `r_condition` C ON LI.`con` = C.`code`
                JOIN `r_gold_rate` GR ON LI.`goldtype` = GR.`id`
                JOIN `r_gold_quality` GC ON LI.`quality` = GC.`rate`
            WHERE loanno = '$loan_no'");

            $a['loan_det'] = $Q2->result();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }

    public function CANCEL_BILL(){

        $loan_no = $_POST['loan_no'];
        $transeno= $_POST['transeno'];
        $bc      = $this->sd['bc'];

        $this->db->trans_begin();

        $C = $this->db->query("SELECT loanno FROM `t_loantranse` WHERE bc = '$bc' AND NOT transecode = 'P' AND NOT transeno = '$transeno' AND loanno = '$loan_no'")->num_rows();
          
        if ($C == 0){            
            
            // 2. if no, Clear account trans table entry.
            $Q1 = $this->db->query("DELETE FROM `t_account_trans` WHERE bc = '$bc' AND loanno = '$loan_no'");

            // 3. Update loan table status
            $Q2 = $this->db->query("UPDATE `t_loan` SET `status`='C' WHERE bc = '$bc' AND loanno = '$loan_no' LIMIT 1");

            // 4. Move data to re_fo table

            $this->load->model('calculate');
            $this->calculate->move_loan_data_to_RE_FO($loan_no,'C');
            $this->calculate->update_customer_pawn_details($_POST['cus_serno'],'C');

            if ($Q1 && $Q2){
                $this->db->trans_commit();
                $a['s'] = 1;
            }else{
                $this->db->trans_rollback();
                $a['s'] = 0;
            }

        }else{            
            // 1. Check this bill got any transactions? if yes, MUST UNABLE to cancel. return 0
            $a['s'] = 0;
        }        

        echo json_encode($a);
    }
    
    
    public function load(){
	   echo $this->load_list();  
    }
    
    public function delete(){
	   $recid = $_POST['recid'];

       if ($this->db->query("DELETE FROM`t_loan` WHERE recid = '$recid' ")){
            echo 1;
       }else{
            echo 0;
       }
    }

    public function set_edit(){
        $recid = $_POST['recid'];
        $R = $this->db->query("SELECT * FROM `t_loan` WHERE recid = '$recid' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function getCustomerInfo($cus_nic = ""){       
        
        if (isset($_POST['cus_serno'])){
            $cus_serno = $_POST['cus_serno'];
            $customer_id = $_POST['customer_id'];
        }else{
            $cus_serno = "";
            $customer_id = "";
        }
        
        if ($cus_serno != ""){

            $Q = $this->db->query("SELECT * FROM `m_customer` WHERE customer_id = '$customer_id' LIMIT 1");
            
            if ($Q->num_rows() > 0){                
                $T = "";
                $R = $Q->row();
                $T .= '<div><h3>Full Name</h3><span>'.$R->title . " " . $R->cusname .'</span></div> <div><h3>ID</h3><span>'.$R->nicno.'</span></div> <div><h3>Address 1</h3><span>'.$R->address.'</span></div>'; 

                if ($R->address2 !=""){ $T .= '<div><h3>Address 2</h3><span>'.$R->address2.'</span></div>';}
                if ($R->mobile !=""){ $T .= '<div><h3>Mobile</h3><span>'.$R->mobile.'</span></div>';}
                if ($R->telNo !=""){ $T .= '<div><h3>Phone</h3><span>'.$R->telNo.'</span></div>';}

                $a['rec']       = $T;
                $a['status']    = 1;
                $a['serno']     = $R->serno;                
                $a['customer_id'] = $R->serno . " - " .$R->nicno . " - " . $R->cusname ;                
                $a['cph']       = 0;
                $a['is_blacklisted'] = $R->isblackListed;
            }else{            
                $a['status']    = 0;
            }

        }else{

            if (isset($_POST['customer_id'])){
                $nic = $_POST['customer_id'];
            }else{
                $nic = $cus_nic;
            }
            
            $Q = $this->db->query("SELECT * FROM `m_customer` WHERE nicno = '$nic' LIMIT 1");
            
            if ($Q->num_rows() > 0){                
                $T = "";
                $R = $Q->row();
                $T .= '<div><h3>Full Name</h3><span>'.$R->title . " " . $R->cusname .'</span></div>
                    <div><h3>ID</h3><span>'.$R->nicno.'</span></div>
                    <div><h3>Address 1</h3><span>'.$R->address.'</span></div>';
                    
                    if ($R->address2 !=""){ $T .= '<div><h3>Address 2</h3><span>'.$R->address2.'</span></div>';}
                    if ($R->mobile !=""){ $T .= '<div><h3>Mobile</h3><span>'.$R->mobile.'</span></div>';}
                    if ($R->telNo !=""){ $T .= '<div><h3>Phone</h3><span>'.$R->telNo.'</span></div>';}

                $a['rec']       = $T;
                $a['status']    = 1;
                $a['serno']     = $R->serno;
                $a['customer_id'] = $R->serno . " - " .$R->nicno . " - " . $R->cusname ;
                $a['cph']       = 0;
                $a['is_blacklisted'] = $R->isblackListed;
            }else{            
                $a['status']    = 0;
            }

        }

        echo json_encode($a);

    }
    



    public function getCustomerInfoByNic($cus_nic = ""){       
        
        $nic = $cus_nic;            
            
        $Q = $this->db->query("SELECT * FROM `m_customer` WHERE nicno = '$nic' LIMIT 1");
        
        if ($Q->num_rows() > 0){                
            $T = "";
            $R = $Q->row();
            
            $T .= ' <div><h3>Full Name</h3><span>'.$R->title . " " . $R->cusname .'</span></div>
                    <div><h3>NIC</h3><span>'.$R->nicno.'</span></div>
                    <div><h3>Address 1</h3><span>'.$R->address.'</span></div>
                    <div><h3>Address 2</h3><span>'.$R->address2.'</span></div>
                    <div><h3>Mobile</h3><span>'.$R->mobile.'</span></div>
                    <div><h3>Phone</h3><span>'.$R->telNo.'</span></div>';
            
            $a = $T;
            
        }else{            
            $a['status']    = 0;
        }

        return $a;

    }



    public function getItemCategory(){
        $Q = $this->db->query("SELECT * FROM r_itemcategory ORDER BY des");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->code ." - ".$R->des." - ".$R->is_bulk."',"; }
        return  $d;
    }

    public function getGoldQuality(){
        $Q = $this->db->query("SELECT * FROM r_gold_quality ORDER BY code");
        $d = "";
        foreach ($Q->result() as $R) { $d .= "'".$R->rate ." - ".$R->code."',"; }
        return  $d;
    }

    public function getBcCustomer(){        
        $q = $_GET['term'];
        $query = $this->db->query("SELECT serno,nicno,cusname FROM m_customer WHERE nicno LIKE '%$q' OR cusname LIKE '%$q%' ORDER BY cusname limit 100");                
        $ary = array();    
        
        foreach($query->result() as $R){ 
            $ary [] = $R->serno . " - ". $R->nicno . " - " . $R->cusname ; 
        }
        
        echo json_encode($ary);
    }

    public function getBillTypeInfo(){
        $bc = $this->sd['bc'];
        $billtype = $_POST['bill_type_code'];

        $Q  = $this->db->query("SELECT S.billtype, S.period, D.`day_from`, D.`day_to`, D.`rate`, S.`weekly_cal` FROM  `r_bill_type_sum` S JOIN `r_bill_type_det` D ON S.`no` = D.`no` WHERE  S.`billtype` = '$billtype' AND bc = '$bc'  ");
        
        $Q2 = $this->db->query("")->row()->billno;

        $a['bt_sum'] = $Q->result();
        $a['bt_max'] = $Q2;
        $a['s'] = 1;       


        echo json_encode($a);
    }

    public function account_update($condition) {

        $this->db->where("trans_no", $this->max_no);
        $this->db->where("trans_code", 1);        
        $this->db->where("bc", $this->sd['bc']);
        $this->db->delete("t_check_double_entry");

        if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
            $this->db->where('bc',$this->sd['bc']);
            $this->db->where('trans_code',1);
            $this->db->where('trans_no',$this->max_no);
            $this->db->delete('t_account_trans');
        }

        $config = array(
            "ddate" => $_POST['date'],
            "trans_code" => 1,
            "trans_no" => $this->max_no,
            "op_acc" => 0,
            "reconcile" => 0,
            "cheque_no" => 0,
            "narration" => "",
            "ref_no" => ''
        );

        $des = "Opening pawning - " . $this->max_no;
        $this->load->model('account');
        $this->account->set_data($config);

        $pawn_stock         = $this->utility->get_acc('stock_acc',$this->sd['bc']); //UNREDEEM_ARTICLES
        $cash_book          = $this->utility->get_acc('cash_acc',$this->sd['bc']);
        $PAWNING_INTEREST   = $this->utility->get_default_acc('PAWNING_INTEREST');

        if (floatval($_POST['stamp_fee']) > 0){        
            $STAMP_FEE      = $this->utility->get_default_acc('STAMP_FEE');
            $this->account->set_value2("Stamp Fee - Opening", $_POST['stamp_fee'], "dr", $cash_book,$condition,"",      $_POST['ln'],$_POST['bt'],$_POST['bn'],"SF");
            $this->account->set_value2("Stamp Fee - Opening", $_POST['stamp_fee'], "cr", $STAMP_FEE,$condition,"",      $_POST['ln'],$_POST['bt'],$_POST['bn'],"SF");
        }
        
        $this->account->set_value2("Pawning - Opening", $_POST['requiredamount'], "dr", $pawn_stock,$condition,"",      $_POST['ln'],$_POST['bt'],$_POST['bn'],"P");
        $this->account->set_value2("Pawning - Opening", $_POST['requiredamount'], "cr", $cash_book,$condition,"",      $_POST['ln'],$_POST['bt'],$_POST['bn'],"P");        
        $this->account->set_value2("Interest - Opening", $_POST['fmintrest'],      "dr", $cash_book,$condition,"",      $_POST['ln'],$_POST['bt'],$_POST['bn'],"A");
        $this->account->set_value2("Interest - Opening", $_POST['fmintrest'],      "cr", $PAWNING_INTEREST,$condition,"", $_POST['ln'],$_POST['bt'],$_POST['bn'],"A");

        if($condition==0){
            
            $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND t.`bc`='" . $this->sd['bc'] . "'  AND t.`trans_code`='1'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");
            
            if ($query->row()->ok == "0") {
                $this->db->where("trans_no", $this->max_no);
                $this->db->where("trans_code", 1);                
                $this->db->where("bc", $this->sd['bc']);
                $this->db->delete("t_check_double_entry");
                return "0";
            } else {
                return "1";
            }
        }
    }

    public function set_days(){

        $cd = $_POST['d'];

        $d = $this->db->query("SELECT LPAD((SELECT (DATEDIFF('$cd', '1900-01-01') - DATEDIFF(BDL.`count_start_date`, '1900-01-01') ) AS `dd` FROM `t_billno_date_log` BDL WHERE effecting_year = DATE_FORMAT('$cd', '%Y')), 3, 0 ) AS `dd` ")->row()->dd;

        $a['days'] = $d;

        echo json_encode($a);

    }


}