<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_customer_evaluation extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_){

    $from_val=$_POST_['txt_rpt_from_r'];
    $to_val=$_POST_['txt_rpt_to_r'];
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    $is_act=$_POST_['is_act'];
  
    $ddate= " WHERE l.ddate BETWEEN '$fd' AND '$td'";

if($is_act==null){
  $amount="";
}else{  
  $amount= " AND l.requiredamount BETWEEN '$from_val' AND '$to_val'";
}


if (isset($_POST['bc_n'])){
    if ( $_POST['bc_n'] == "" ){        
        $bc = "";
    }        
}else{

    for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
        $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
    }

    $bc = implode(',', $bc_ar);

    if ($bc == ""){
        $bc   = "";
    }else{
        $bc   = " AND  l.bc IN ($bc)  ";
    }
}

// r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }

/*var_dump($is_act);
exit();
   */
    $sql="SELECT 
                a.ddate,
                c.`nicno` AS cus_serno,
                m.cusname,
                SUM(a.ntranse) AS ntranse,
                a.traseval,
                a.rdmval,
                (a.traseval - a.rdmval) AS bal 
          FROM (SELECT 
                      l.ddate,
                      l.cus_serno,
                      COUNT(l.loanno) AS ntranse,
                      SUM(IFNULL(l.requiredamount, 0)) AS traseval,
                            '0' AS rdmval 
                FROM t_loan l 
                $ddate $amount $bc $billtype
                GROUP BY l.cus_serno

          UNION ALL 

                SELECT 
                  l.ddate,
                  l.cus_serno,
                  COUNT(l.loanno) AS ntranse,
                  SUM(IFNULL(l.requiredamount, 0)) AS traseval,
                  SUM(IFNULL(l.requiredamount, 0)) AS rdmval 
                FROM t_loan_re_fo l 
                $ddate $amount $bc $billtype
                GROUP BY l.cus_serno) a 
          LEFT JOIN m_customer m ON  m.serno=a.cus_serno
          JOIN m_customer c ON a.cus_serno = c.`serno` ";
    
     

      $sql.=" GROUP BY a.cus_serno ORDER BY a.cus_serno";

    

    $query = $this->db->query($sql);


    if($this->db->query($sql)->num_rows()>0){

        $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
       echo "<script>location='default_pdf_error'</script>";
    }

}

}