<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class my_account extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){   
        $a['User'] = $this->sd['user_des'];
        $a['display_name'] = $this->db->select("IF (discription='no_name_added','',discription) AS `display_name`")->where("cCode",$this->sd['oc'])->limit(1)->get('u_users')->row()->display_name;
		return $a;
    }
    
    public function save(){

    }   

    
    public function ChangeDN(){

        $n  = $_POST['n'];
        $oc = $this->sd['oc'];
        $q  = $this->db->query("UPDATE `u_users` SET discription = '$n',`display_name`='$n' WHERE cCode= '$oc' LIMIT 1 ");

        if ($q){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }   

    public function ChangePw(){


        $cCode=$this->sd["oc"];  
        $oldInPass=md5($_POST['OlduserPassword']);
        if (isset($_POST['userPassword'])) {
            $_POST['userPassword']=md5($_POST['userPassword']); 
        }
        $OlPass=$this->db->select('userPassword')->where("cCode",$cCode)->get("u_users")->first_row('array');

        if($OlPass["userPassword"]==$oldInPass){
            unset($_POST["OlduserPassword"]);
            unset($_POST["ruserPassword"]);            

           if ($this->db->where("cCode",$cCode)->update("u_users",$_POST)) {
                 $a['s'] = 1;
                }
            else {
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno);
            }
        }else{
            $a['s'] = 2;      
            $a['m'] = "Invalid Current Password!";                       
        }



    echo json_encode($a);


    }      

}