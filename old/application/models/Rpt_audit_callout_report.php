<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_audit_callout_report extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function PDF_report(){

		ini_set('max_execution_time', 300);

		$bc = $_POST['bc'];
		$fd = $_POST['from_date'];
		$td = $_POST['to_date'];		
		$bt = $_POST['r_billtype'];

		if (isset($_POST['bc_n'])){
	        
	        if ( $_POST['bc_n'] == "" ){        
	            
	            $QBC = "";
				$QBC2 = "";
				$QBC3 = "";

				if ($bt != ""){
					$QBC3= " L.billtype = '$bt' AND ";
				}else{
					$QBC3 = "";
				}
	        
	        }        

	    }else{


	        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
	            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
	        }

	        $bc = implode(',', $bc_ar);



	        

	        if ($bc != ""){

	        	$QBC = "L.`bc` IN ($bc) AND ";
				$QBC2= "lt.`bc` IN ($bc) AND ";

			}else{
				$QBC = $QBC2 = "";
			}

			if ($bt != ""){
				$QBC3= " L.billtype = '$bt' AND ";
			}else{
				$QBC3 = "";
			}

	    }




		// if ($bc == ""){/*$QBC = ""; $QBC2 = ""; $QBC3 = ""; if ($bt != ""){$QBC3= " L.billtype = '$bt' AND "; }else{$QBC3 = ""; }*/ }else{$QBC = "L.`bc`='$bc' AND "; $QBC2= "lt.`bc`='$bc' AND "; if ($bt != ""){$QBC3= " L.billtype = '$bt' AND "; }else{$QBC3 = ""; } }

		/* check for old backup rpt_daily_summery_total.php for php based accrued_interest calculation */
		
		$Q  = $this->db->query("SELECT 

LII.rows,
LII.tot_W,
B.`name` AS `bc_name`,
L.bc,
L.`billtype`,
L.`billno`,
L.`ddate`,
CONCAT(LI.itemname,' - ' ,LI.des ) as `itemname`,
LI.qty,
LI.goldcatagory,
LPAD(LI.quality,3,0) AS `quality`,
L.`requiredamount`,
LI.total_weight,
LI.pure_weight,
((L.`requiredamount` /  LI.total_weight) * 8) AS `rate_per_souv`,
0 AS `accrued_int`,
IF (L.`by_approval` = 0,'',AH.`discription`) AS `approvals`,LI.des

FROM `t_loan` L

JOIN m_branches B ON L.`bc` = B.`bc` 

JOIN (	SELECT 	li.qty,li.itemcode,li.`bc`,li.`billno`,i.`itemname`,li.`goldtype`, CONCAT(gr.`goldcatagory`,'(',gr.printval,')') AS `goldcatagory`, li.`quality`,li.`goldweight` AS `total_weight`, li.`pure_weight`,cd.`des`
		FROM `t_loanitems` li
		JOIN `r_items` i 		ON li.`itemcode` = i.`itemcode`
		JOIN `r_gold_rate` gr 	ON li.`goldtype` = gr.`id`
		JOIN `r_condition` cd 	ON li.con = cd.code
	
) LI ON L.`bc` = LI.`bc` AND LI.`billno` = L.`billno`

LEFT JOIN (	SELECT ah.`bc`,ah.auto_no,ah.approved_by, CONCAT(approved_by,'-',u.`discription`) AS `discription`
			FROM `t_approval_history` ah 
			JOIN u_users u ON ah.`approved_by` = u.`cCode` ) AH ON AH.`bc` = L.`bc` AND AH.`auto_no` = L.`by_approval`

JOIN ( SELECT COUNT(li.`billno`) AS `rows`, SUM(li.`goldweight`) AS `tot_W`, li.`billno`,li.`bc` FROM `t_loanitems` li GROUP BY li.`billno`) LII ON L.`bc` = LII.`bc` AND LII.`billno` = L.`billno` 

WHERE $QBC $QBC3 L.`ddate` <= '$td' 
ORDER BY L.`bc` ASC, L.`billtype` ASC , L.`ddate` ASC,L.`billno`  ");
		

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;
			
			$this->load->view($_POST['by'].'_'.'pdf',$r_data);			

		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}



	public function Excel_report()
    {
        

		$bc = $_POST['bc'];
		$fd = $_POST['from_date'];
		$td = $_POST['to_date'];		
		$bt = $_POST['r_billtype'];

		if (isset($_POST['bc_n'])){
	        
	        if ( $_POST['bc_n'] == "" ){        
	            
	            $QBC = "";
				$QBC2 = "";
				$QBC3 = "";

				if ($bt != ""){
					$QBC3= " L.billtype = '$bt' AND ";
				}else{
					$QBC3 = "";
				}
	        
	        }        

	    }else{

	    	$_POST['bc_arry'] =  explode(',', $_POST['bc_arry']);

	        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
	            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
	        }

	        $bc = implode(',', $bc_ar);
			        

	        if ($bc != ""){

	        	$QBC = "L.`bc` IN ($bc) AND ";
				$QBC2= "lt.`bc` IN ($bc) AND ";

			}else{
				$QBC = $QBC2 = "";
			}

			if ($bt != ""){
				$QBC3= " L.billtype = '$bt' AND ";
			}else{
				$QBC3 = "";
			}

	    }




		// if ($bc == ""){/*$QBC = ""; $QBC2 = ""; $QBC3 = ""; if ($bt != ""){$QBC3= " L.billtype = '$bt' AND "; }else{$QBC3 = ""; }*/ }else{$QBC = "L.`bc`='$bc' AND "; $QBC2= "lt.`bc`='$bc' AND "; if ($bt != ""){$QBC3= " L.billtype = '$bt' AND "; }else{$QBC3 = ""; } }

		/* check for old backup rpt_daily_summery_total.php for php based accrued_interest calculation */
		
		$Q  = $this->db->query("SELECT 

LII.rows,
LII.tot_W,
B.`name` AS `bc_name`,
L.bc,
L.`billtype`,
L.`billno`,
L.`ddate`,
LI.itemname as `itemname`,
LI.qty,
LI.goldcatagory,
LPAD(LI.quality,3,0) AS `quality`,
L.`requiredamount`,
L.totalweight AS `billtotalweight`,
LI.total_weight,
LI.pure_weight,
((L.`requiredamount` /  LI.total_weight) * 8) AS `rate_per_souv`,
0 AS `accrued_int`,
IF (L.`by_approval` = 0,'',AH.`discription`) AS `approvals`

FROM `t_loan` L

JOIN m_branches B ON L.`bc` = B.`bc` 

JOIN (	SELECT 	li.qty,li.itemcode,li.`bc`,li.`billno`,i.`itemname`,li.`goldtype`, CONCAT(gr.`goldcatagory`,'(',gr.printval,')') AS `goldcatagory`, li.`quality`,li.`goldweight` AS `total_weight`, li.`pure_weight`
		FROM `t_loanitems` li
		JOIN `r_items` i 		ON li.`itemcode` = i.`itemcode`
		JOIN `r_gold_rate` gr 	ON li.`goldtype` = gr.`id`
	
) LI ON L.`bc` = LI.`bc` AND LI.`billno` = L.`billno`

LEFT JOIN (	SELECT ah.`bc`,ah.auto_no,ah.approved_by, CONCAT(approved_by,'-',u.`discription`) AS `discription`
			FROM `t_approval_history` ah 
			JOIN u_users u ON ah.`approved_by` = u.`cCode` ) AH ON AH.`bc` = L.`bc` AND AH.`auto_no` = L.`by_approval`

JOIN ( SELECT COUNT(li.`billno`) AS `rows`, SUM(li.`goldweight`) AS `tot_W`, li.`billno`,li.`bc` FROM `t_loanitems` li GROUP BY li.`billno`) LII ON L.`bc` = LII.`bc` AND LII.`billno` = L.`billno` 

WHERE $QBC $QBC3 L.`ddate` <= '$td' 
ORDER BY L.`bc` ASC, L.`billtype` ASC , L.`ddate` ASC,L.`billno`  ");
		

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;

			
			$this->load->view($_POST['by'].'_'.'excel',$r_data);
			

		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}
    }

	
}