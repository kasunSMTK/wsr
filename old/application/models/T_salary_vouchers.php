<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class T_salary_vouchers extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		  parent::__construct();		
		  $this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){
        $a['YM'] = $this->getYearMonth();
        $this->load->model('m_branches');
        $a['bc'] = $this->m_branches->select();
        $a['current_date'] = $this->sd['current_date'];        
        $a['no'] = "";// $this->get_nno_max();        
		    return $a;
    }

    public function getYearMonth(){

        $Y = "<select id='year' name='year' class='ym_dropdown'>";
        $cy = date('Y') - 10;
        $py = date('Y') + 10;
        for ($n = $cy ; $n <= $py ;  $n++){if (date('Y') == $n){$Y .= "<option selected='selected' value='$n'>$n</option>"; }else{$Y .= "<option value='$n'>$n</option>"; } }
        $Y .= "</select>";
        $M = '<select name="month" id="month" size="1" class="ym_dropdown"> <option value="1">1 January</option> <option value="2">2 February</option> <option value="3">3 March</option> <option value="4">4 April</option> <option value="5">5 May</option> <option value="6">6 June</option> <option value="7">7 July</option> <option value="8">8 August</option> <option value="9">9 September</option> <option value="10">10 October</option> <option value="11">11 November</option> <option value="12">12 December</option> </select>';

        $a['Y'] = $Y;
        $a['M'] = $M;

        return $a;

    }

    public function save(){      
        
        $this->db->trans_begin();

        for ($n = 0 ; $n < count($_POST['emp_no']) ; $n++ ){

            $nno        = $this->get_voucher_next_no('salary_vouchers');
            $acc_code   = $this->utility->get_acc('cash_acc',$_POST['bc'][$n]);
        
            $voucher_items_sum = array(                    
                "bc"            => $_POST['bc'][$n],
                "cl"            => $this->sd['cl'],
                "nno"           => $nno,
                "type"          => 'cash',
                "ddate"         => $_POST['date'],        
                "note"          => $_POST['nName'][$n],        
                "cash_acc"      => $acc_code,
                "paid_acc"      => $acc_code,
                "cash_amount"   => $_POST['net_sal'][$n],
                "ref_no"        => $_POST['emp_no'][$n],
                "vocuher_type"  => "salary_vouchers",
                "emp_no"        => $_POST['emp_no'][$n],
                "sal_vou_mon"   => $_POST['month'],
                "sal_vou_yr"   => $_POST['year']
            );

            $voucher_items_det = array( 
                "cl"            => $this->sd['cl'],                   
                "bc"            => $_POST['bc'][$n],
                "nno"           => $nno,
                "type"          => 'cash',                
                "acc_code"      => $acc_code,                
                "amount"        => $_POST['net_sal'][$n],
                "ref_no"        => $_POST['emp_no'][$n]            
            );

            $_POSTX_APPROVAL = array(
            
                'no'=>$nno,
                'bc'=>$_POST['bc'][$n],
                'date'=>$_POST['date'],
                'ref_no'=>$_POST['emp_no'][$n],
                'request_from'=>"",
                'method'=>'cash',
                'amount'=>$_POST['net_sal'][$n],
                'approved_amount'=>"",
                'status'=>"W",
                'comment'=>"",
                'request_by'=>$this->sd['oc'],
                'request_datetime'=>date('Y-m-d h:i:s'),
                'approved_by'=>"",
                'approved_datetime'=>"",
                'nno' => $nno,
                'vocuher_type'  => "salary_vouchers",
                "paid_acc"      => $acc_code
            );

            $this->db->insert("t_voucher_gl_sum",$voucher_items_sum);
            $this->db->insert("t_voucher_gl_det",$voucher_items_det);
            $this->db->insert('t_gen_vou_app_rqsts_sum',$_POSTX_APPROVAL);

        }


        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = 0;
        }else{            
            $a['s'] = 1;            
            $this->db->trans_commit();
        }

        echo json_encode($a);
    }
    
    
    private function get_voucher_next_no($vocuher_type){
        return $this->db->query("SELECT IFNULL(MAX(nno)+1,1) AS `max_no` FROM `t_voucher_gl_sum` WHERE  `vocuher_type` = '$vocuher_type' ")->row()->max_no;
    }
    











    public function import_salary_data(){

        if ($_POST['bc'] == ""){
            $BC     = "";
            // $BC     = " AND m_employee.bc  in('AW','BT','DG','DI','EM','KL','KN','PM','YN')"; // remove this section later
        }else{
            $BC     = " AND m_employee.bc = '".$_POST['bc']."' ";
        }

        $year   = $_POST['y'];
        $month  = $_POST['m'];

        // load pay roll database ----------------------------
            $DB2 = $this->load->database('payroll', TRUE);
        // End load pay roll database ------------------------

        $Q   = $DB2->query("SELECT m_employee.`bc`, m_employee.emp_no,m_employee.`nName`,

        ((t_pay_sheet.`basic_salary` + t_pay_sheet.`allowances` + t_pay_sheet.`buget_incentive`) - (t_pay_sheet.`deductions` + t_pay_sheet.`epf_worker_amount` + (t_pay_sheet.`no_pay_rate`*t_pay_sheet.`no_pay_days` ) + t_pay_sheet.`salary_advance` +t_pay_sheet.`loan_installments_tatal`)) AS `net_salary`

        FROM t_pay_sheet 
          
          INNER JOIN m_employee ON (m_employee.emp_no = t_pay_sheet.emp_no AND t_pay_sheet.ci = m_employee.ci) 
          INNER JOIN r_designation ON (r_designation.ccode = m_employee.designation_id) 

        WHERE y_year = '$year' AND m_month = '$month' $BC

        GROUP BY m_employee.emp_no ");

        if ($Q->num_rows() > 0){
            $a['data'] = $Q->result();
        }else{
            $a['data'] = 0;
        }

        echo json_encode($a);

    }





























}