<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class default_pdf_error_pdf extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
		return $a;
    }   
    
}