<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_pawning_adv_age extends CI_Model {

  private $sd;    

  function __construct(){
    parent::__construct();        
    $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
  }   


    public function Excel_report(){


        $_POST_['from_date'] = $_POST['from_date'];
        $_POST_['to_date'] = $_POST['to_date'];
        $_POST_['bc_arry'] = $_POST['bc_arry'];       

        $this->PDF_report($_POST_,$type= 'excel');

    }


  public function PDF_report($_POST_,$type='pdf'){

    if ($type=='excel'){
        $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
    }

    
    ini_set('max_execution_time',600);

    // if ( $_POST['bc'] == "" ){        
    //   $bc = "";
    // }else{        
    //   $bc   = " AND  t_loan.bc = '".$_POST['bc']."'";                    
    // }
    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = " AND  L.bc IN ($bc)  ";
        }
    }

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    

    if(!empty($_POST['r_billtype'])){
      $bill = "AND L.billtype='".$_POST['r_billtype']."'";
    }else{
      $bill ="";
    }

    // $Q = $this->db->query("SELECT * from a");

    $Q = $this->db->query("SELECT a.bc,a.bc_name,SUM(r1_30) AS `R30`,SUM(r1_60) AS `R60`,SUM(r1_90) AS `R90`,SUM(r1_120) AS `R120`,SUM(r1_150) AS `R150`,SUM(r1_180) AS `R180`,SUM(r1_210) AS `R210`,SUM(r1_240) AS `R240`,SUM(r1_270) AS `R270`,SUM(r1_300) AS `R300`,SUM(r1_330) AS `R330`,SUM(r1_360) AS `R360`,SUM(over_360) AS `OVR360`
    
FROM (

    SELECT L.bc,B.name AS bc_name,(DATEDIFF('$td',ddate)) AS `d`,
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 0 AND 30
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_30',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 31 AND 60
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_60',

    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 61 AND 90
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_90',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 91 AND 120
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_120',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 121 AND 150
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_150',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 151 AND 180
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_180',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 181 AND 210
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_210',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 211 AND 240
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_240',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 241 AND 270
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_270',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 271 AND 300
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_300',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 301 AND 330
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_330',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) BETWEEN 331 AND 360
    THEN (L.`requiredamount`)
    ELSE 0
    END 'r1_360',
    
    CASE
    WHEN (DATEDIFF('$td',ddate)) > 360
    THEN (L.`requiredamount`)
    ELSE 0
    END 'over_360'
    

    FROM `t_loan` L

    JOIN m_branches B ON L.bc = B.bc

    WHERE 1=1 $bc AND L.`ddate` <= '$td' 

) a GROUP BY a.bc ORDER BY bc");

    
    if($Q->num_rows() > 0){

      $r_data['list'] = $Q->result();
      $r_data['fd'] = $fd;
      $r_data['td'] = $td;

    
        if ($type=='pdf'){
            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{
            $this->load->view($_POST['by'].'_'.'excel',$r_data);
        }


    }else{
      echo "<script>location='default_pdf_error'</script>";
    }

  }

}