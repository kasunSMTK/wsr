<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fund_tr_voucher extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report($DATA){ 

  
        $bc=$this->sd['bc'];      
        $no = $_POST['v_no'];

        $Q = $this->db->query("SELECT
                                  cd.`cheque_no`,
                                  f.no,
                                  f.ref_no,
                                  f.date_time,
                                  f.requ_initi_bc,
                                  bb.name AS ini_branch,
                                  f.from_bc,
                                  b.name AS from_branch,
                                  f.to_bc,
                                  t.name AS to_branch,
                                  f.transfer_type,
                                  f.amount,
                                  f.comment,
                                  f.to_acc,
                                  m.description
                                FROM
                                  t_fund_transfer_sum f 
                                LEFT JOIN m_branches b ON b.bc=f.from_bc 
                                LEFT JOIN m_branches t ON t.bc=f.to_bc 
                                LEFT JOIN m_branches bb ON bb.bc = f.requ_initi_bc
                                LEFT JOIN m_account m ON m.code =f.to_acc 
                                LEFT JOIN `t_fnd_trnsfr_rqsts_chq_det` cd ON f.`no` = cd.`no`

                                WHERE f.no = '$no' ");

        
        if($Q->num_rows() > 0){

          $r_data['list'] = $Q->result();
          /*$r_data['fd'] = $fd;
          $r_data['td'] = $td;*/

          $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{
          echo "<script>location='default_pdf_error'</script>";
        } 	 

    }   
    
}