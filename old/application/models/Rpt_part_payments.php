<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_part_payments extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
		return $a;
    }


    public function Excel_report(){        

        $_POST_['from_date'] = $_POST['from_date'];
        $_POST_['to_date'] = $_POST['to_date'];
        $this->PDF_report($_POST_,"excel");        
    }

    public function PDF_report($_POST_,$rtype="pdf"){
        
        if (isset($_POST['bc_n'])){
            if ( $_POST['bc_n'] == "" ){
                $bc = "";
            }        
        
        }else{



            if ($rtype != "pdf"){

                $x = explode(",", $_POST['bc_arry']);
                unset($_POST['bc_arry']);

                $_POST['bc_arry'] = $x;
                

            }



            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }



            $bc = implode(',', $bc_ar);

            if ($bc == ""){
                $bc   = "";
            }else{
                $bc   = "   L.bc IN ($bc) AND ";
            }
        }

    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }


        $fd = $_POST_['from_date'];
        $td = $_POST_['to_date'];

        $Q = $this->db->query("SELECT 
LT.`bc`,
B.`name` AS `bc_name`,
'AAA' AS `loanno`,
LT.`billno`,
BT.`billtype`,
LT.`action_date` AS `action_date`,
LT.`cr_amount` AS `amount`,
C.cusname,
C.address,
C.`nicno`,
C.`mobile`,
L.`totalweight`

FROM `t_loan_advance_customer` LT

JOIN (  SELECT billno,bc,totalweight,cus_serno,billtype FROM `t_loan`
        UNION ALL
        SELECT billno,bc,totalweight,cus_serno,billtype FROM `t_loan_re_fo` ) L ON LT.`billno` = L.`billno` AND LT.`bc` = LT.`bc`

JOIN `m_customer` C ON L.`cus_serno` = C.`serno`
JOIN `r_bill_type_sum` BT ON L.`billtype` = BT.`billtype` AND L.`bc` = BT.`bc`
JOIN m_branches B ON LT.`bc` = B.`bc`

WHERE $bc  LT.`is_delete` = 0 AND LT.`trans_type` = 'ADV_P' AND LT.`date` BETWEEN '$fd' AND '$td' $billtype

ORDER BY LT.`bc`, L.`billtype`, LT.`action_date` ASC");

        if($Q->num_rows() > 0){

            $r_data['list'] = $Q->result();
            $r_data['fd'] = $fd;
            $r_data['td'] = $td;


            if ($rtype == "pdf"){
                $this->load->view($_POST['by'].'_'.'pdf',$r_data);
            }else{
                $this->load->view($_POST['by'].'_'.'excel',$r_data);
            }


        }else{
            echo "<script>location='default_pdf_error'</script>";
        }

    }
    
}