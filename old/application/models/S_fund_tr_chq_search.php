<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class s_fund_tr_chq_search extends CI_Model{
  
  function __construct(){
    parent::__construct();
    
    $this->sd = $this->session->all_userdata();
    $this->load->database($this->sd['db'], true);    
    $this->load->model('user_permissions');
    
  }
  
  public function base_details(){

    
  }


  public function s(){

    $A = $_POST['A'];
    $B = $_POST['B'];
    $C = $_POST['C'];
    $D = $_POST['D'];
    $E = $_POST['E'];
    $F = $_POST['F'];
    $G = $_POST['G'];

    $q = $this->db->query("

      SELECT 
       
      F.no,
      F.`date`,
      F.cheque_no,
      F.amount,
      B.`name` AS from_bc,
      BB.`name` AS to_bc,
      GROUP_CONCAT(CONCAT(ACC.acc_code, '-',AC.`description`)) AS `acc_desc`,
      FF.`fund_tr_opt_no` 
      
      
      FROM `t_fnd_trnsfr_rqsts_chq_det` F
      
      
      JOIN t_account_trans ACC ON F.`no` = ACC.`trans_no` AND ACc.`trans_code` = 9
      JOIN t_fund_transfer_sum FF ON F.`no` = FF.`no`
      JOIN m_branches B  ON FF.`from_bc` = B.`bc`
      JOIN m_branches BB ON FF.`to_bc` = BB.`bc`
      
      JOIN m_account AC ON ACC.`acc_code` = AC.`code`
      
      WHERE FF.`transfer_type` = 'cheque_withdraw' AND (
        
        F.no LIKE '%$G%'  AND
        F.cheque_no LIKE '%$A%'  AND
        F.`amount` LIKE '%$B%' AND
        B.`name` LIKE '%$C%' AND 
        BB.`name` LIKE '%$D%' 
      ) AND ( F.`date` BETWEEN '$E' AND '$F' )  
      
    GROUP BY F.`no`");


    $a['s'] = 1;

    if ( $q->num_rows() > 0 ){
        $a['d'] = $q->result();
    }else{
        $a['d'] = 0;
    }    

    

    $a['s'] = 1;

    echo json_encode($a);


  }




}