<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_income_statement_2 extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
		return $a;
    }   

    public function PDF_report($_POST_){

        $make_excel = true;
        $abb        = false;
        
        if (isset($_POST['bc_n'])){
            
            if ( $_POST['bc_n'] == "" ){        
                $bc = "";
            }

            $QBC = $bc;
            $QBC_EXP = $bc;

        }else{

            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    

            $QBC = " A.bc IN ($bc) AND ";
            $QBC_EXP = " ACC.v_bc IN ($bc) AND ";

        }


        $Y  = date('Y',strtotime($_POST['to_date']));
        
        $fd = $_POST_['from_date'];
        $td = $_POST_['to_date'];
        
        //$fd = $Y."-01-01";
        //$td = $Y."-12-31";

        $time_range = array($Y,($Y-1));
        $sub_tot_cLs_title = "style='border-bottom:1px solid #000000;font-size:19px;font-family:bitter'";
        $sub_tot_cLs = "style='background-color:#cccccc;color:#000000;font-weight:600'";


        $income_tot             = Array();
        $income_tot_prv         = Array();

        $exp_tot = $tot_exp_ = Array();
        $exp_tot_prv         = Array();

        $income_tot_company     = Array();
        $income_tot_prv_company = Array();

        $exp_tot_company     = Array();
        $exp_tot_prv_company = Array();

        $other_income_tot = Array();
        $other_income_tot_prv = Array();

        $other_exp_tot = 0; // if other expenses accounts are exist add to this array
        $other_exp_tot_prv = 0; // if other expenses accounts are exist add to this array

        $other_income_tot_company = Array();
        $other_income_tot_prv_company = Array();

        $other_exp_tot_company = 0; // if other expenses accounts are exist add to this array
        $other_exp_tot_prv_company = 0; // if other expenses accounts are exist add to this array

        
        $bc_q = $this->db->query("SELECT bc,`name` FROM m_branches ");
        foreach ($bc_q->result() as $bc_desc) {
            $bc_desc_ar[$bc_desc->bc] = $bc_desc->name;
        }

        $acc_q = $this->db->query("SELECT `code`, description FROM m_account WHERE CODE != ''");
        foreach ($acc_q->result() as $acc_desc) {
            $acc_desc_ar[$acc_desc->code] = $acc_desc->description;
        }

        $bcar = $this->db->query("SELECT A.`bc` FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN '$fd' AND '$td' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code WHERE $QBC B.code IN ('10101','10102','10103') GROUP BY A.`bc` ORDER BY A.bc")->result();

        foreach ($bcar as $bc){
            $bc__array[] = $bc->bc;
        }        

        $acc_array = array('10101' , '10102', '10103' );        

        $colspan_count = 0;
        
        //------------------------------ HEADER --------------------------

        $t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
        $t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';
        
        $t .= '<thead>';            

        $t .= '<tr>';            
        $t .= '<td colspan="'.($colspan_count+5).'" '.$sub_tot_cLs_title.'>Income Statement</td>';
        $t .= '</tr>';

        $t .= '<tr>';            
        $t .= '<td colspan="5">From '.$fd.' to '.$td.'</td>';
        $t .= '</tr>';

        $t .= '<tr>';            
        $t .= '<td></td>';

        foreach ($bc__array as $bc) {            
            $t .= '<td colspan="2" align="center">'. strtolower($bc_desc_ar[$bc]) ."</td>";
        }   

        $t .= '<td colspan="2" align="center">Company</td>';

        $t .= '</tr>';        
        
        $t .= '<tr>';            
        $t .= '<td></td>';

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {
                $t .= '<td align="center">'. $y."</td>";
                $colspan_count++;                
            }
        }   
            foreach ($time_range as $y) {
                $t .= '<td align="center">'. $y."</td>";                
                $colspan_count++;
            }

        $t .= '</tr>';
        $t .= '</thead>';
        
        // ------------------------------- END HEADER ---------------------------------------------



        






















        // ------------------------------- ACCOUNT TITLE---

        $t .= '<tr>';            
        $t .= '<td colspan="'.($colspan_count+1).'" '.$sub_tot_cLs_title.'>Income</td>';
        $t .= '</tr>';

        // ------------------------------------------------


        // Income account loop for selected year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN '$fd' AND '$td' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code WHERE $QBC B.`rtype` = 1 AND B.code IN ('10101','10102','10103') ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR = Array();

        foreach ($Q->result() as $AR) {               
            $AAR[$AR->bcc][$AR->YY][$AR->acc_code] = ($AR->bal);            
        }

        // Income account loop for selected end

        // Income account loop for comparison year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN ADDDATE('$fd',INTERVAL -1 YEAR) AND ADDDATE('$td',INTERVAL -1 YEAR) GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code WHERE $QBC B.`rtype` = 1 AND B.code IN ('10101','10102','10103') ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR_PRV = Array();

        foreach ($Q->result() as $AR_PRV) {               
            $AAR_PRV[$AR_PRV->bcc][$AR_PRV->YY][$AR_PRV->acc_code] = ($AR_PRV->bal);
        }

        // Income account loop for comparison end



        $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();

        foreach ($acc_array as $acc) {
            
            $t .= '<tr>';            
            $t .= '<td>'.$acc_desc_ar[$acc].'</td>';                

            foreach ($bc__array as $bc) {
                
                foreach ($time_range as $y) {                   
                        
                    if ( $AAR[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR[$bc][$y][$acc] , 2 ) .'</td>';
                        $X += floatval($AAR[$bc][$y][$acc]);
                        $tmp_y = $y;
                        $AR_GROUP_TOT[$bc][$y] += floatval($AAR[$bc][$y][$acc]);

                    }elseif( $AAR_PRV[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR_PRV[$bc][$y][$acc] , 2 ) .'</td>';
                        $Y += floatval($AAR_PRV[$bc][$y][$acc]);
                        $tmp_prv_y = $y;
                        $AR_GROUP_TOT_PRV[$bc][$y] += floatval($AAR_PRV[$bc][$y][$acc]);

                    }else{
                        $t .= '<td align="right">0</td>';
                    }

                } 

            }             

            foreach ($time_range as $y) {
                
                if ($y == $tmp_y){
                    $t .= '<td align="right">'.number_format($X,2).'</td>';
                    $X_TOT += $X;
                }else{
                    $t .= '<td align="right">'.number_format(0,2).'</td>';
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right">'.number_format($Y,2).'</td>';
                    $Y_TOT += $Y;
                }
                
            }
            
            $t .= '</tr>';

            $X = $Y = 0;

        }


        














        //------------------------- SUB TOTAL -----------------
        
        $t .= '<tr>';            
        $t .= '<td '.$sub_tot_cLs.'>Total Income</td>';

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {

                if ($AR_GROUP_TOT[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT[$bc][$y] ,2).'</td>';
                    $income_tot[$bc][$y] = $AR_GROUP_TOT[$bc][$y];

                }elseif ($AR_GROUP_TOT_PRV[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT_PRV[$bc][$y],2) .'</td>';
                    $income_tot_prv[$bc][$y] = $AR_GROUP_TOT_PRV[$bc][$y];

                }else{
                    $t .= '<td align="right" '.$sub_tot_cLs.'>0</td>';
                    $income_tot[$bc][$y] = $income_tot_prv[$bc][$y] = 0;
                }

            }
        }   
            foreach ($time_range as $y) {
                
                $abb = false;

                if ($y == $tmp_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($X_TOT,2).'</td>';                
                    $abb = true;
                    $income_tot_company[$y] = $X_TOT;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($Y_TOT,2).'</td>';                
                    $abb = true;
                    $income_tot_prv_company[$y] = $Y_TOT;
                }

                if (!$abb){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(0,2).'</td>';
                }         
            }

        $t .= '</tr>';

























        //--------- reset vars -------------------

        unset($AR_GROUP_TOT , $AR_GROUP_TOT_PRV);
        $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();

        //---------- end ----------------------------





















        // ------------------------------- ACCOUNT TITLE---

        $t .= '<tr>';            
        $t .= '<td colspan="'.($colspan_count+1).'" '.$sub_tot_cLs_title.'>Expanses</td>';
        $t .= '</tr>';

        // ------------------------------------------------


        unset($acc_array,$acc_desc_ar);

        $acc_q = $this->db->query("SELECT `code`,heading FROM m_account_type WHERE rtype = 2 ORDER BY heading");
        foreach ($acc_q->result() as $acc_r) {
            $acc_array[] = $acc_r->code;
            $acc_desc_ar[$acc_r->code] = $acc_r->heading;
        }


        // Expense account loop for selected year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`,C.bc  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT v_bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal`,bc FROM t_account_trans WHERE ddate BETWEEN '$fd' AND '$td' GROUP BY bc,acc_code ) C ON A.`bc` = C.v_bc AND B.code = C.acc_code 


            WHERE $QBC C.bc NOT IN (SELECT bc FROM m_branches WHERE is_cost_center = 1) AND B.`rtype` = 2 


            ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR = Array();

        foreach ($Q->result() as $AR) {
            $AAR[$AR->bcc][$AR->YY][$AR->acc_code] = ($AR->bal);            
        }

        // Expense account loop for selected end


        // Expense account loop for comparison year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT v_bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN ADDDATE('$fd',INTERVAL -1 YEAR) AND ADDDATE('$td',INTERVAL -1 YEAR) GROUP BY v_bc,acc_code ) C ON A.`bc` = C.v_bc AND B.code = C.acc_code WHERE $QBC B.`rtype` = 2  ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR_PRV = Array();

        foreach ($Q->result() as $AR_PRV) {               
            $AAR_PRV[$AR_PRV->bcc][$AR_PRV->YY][$AR_PRV->acc_code] = ($AR_PRV->bal);
        }

        // Expense account loop for comparison end


        $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();

        foreach ($acc_array as $acc) {
            
            $t .= '<tr>';            
            $t .= '<td>'.$acc_desc_ar[$acc].'</td>';                

            foreach ($bc__array as $bc) {
                
                foreach ($time_range as $y) {                   
                        
                    if ( $AAR[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR[$bc][$y][$acc] , 2 ) .'</td>';
                        $X += floatval($AAR[$bc][$y][$acc]);
                        $tmp_y = $y;

                        if (floatval($AAR[$bc][$y][$acc]) > 0){
                            $AR_GROUP_TOT[$bc][$y] += floatval($AAR[$bc][$y][$acc]);
                        }else{
                            $AR_GROUP_TOT[$bc][$y] -= floatval($AAR[$bc][$y][$acc]);
                        }

                    }elseif( $AAR_PRV[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR_PRV[$bc][$y][$acc] , 2 ) .'</td>';
                        $Y += floatval($AAR_PRV[$bc][$y][$acc]);
                        $tmp_prv_y = $y;
                        $AR_GROUP_TOT_PRV[$bc][$y] += floatval($AAR_PRV[$bc][$y][$acc]);

                    }else{
                        $t .= '<td align="right">0</td>';
                    }

                } 

            }             

            foreach ($time_range as $y) {

                $abb = false;
                
                if ($y == $tmp_y){
                    $t .= '<td align="right">'.number_format($X,2).'</td>';
                    $X_TOT += $X;
                    $abb = true;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right">'.number_format($Y,2).'</td>';
                    $Y_TOT += $Y;
                    $abb = true;
                }               

                if (!$abb){
                    $t .= '<td align="right">'.number_format(0,2).'</td>';
                }


            }
            
            $t .= '</tr>';

            $X = $Y = 0;

        }


        unset($acc_array,$acc_desc_ar);

        $acc_q = $this->db->query("SELECT `code`,heading FROM m_account_type WHERE rtype = 2 ORDER BY heading");
        foreach ($acc_q->result() as $acc_r) {
            $acc_array[] = $acc_r->code;
            $acc_desc_ar[$acc_r->code] = $acc_r->heading;
        }


        // Expanses account loop for selected year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN '$fd' AND '$td' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code WHERE $QBC C.acc_code = '20305' ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR = Array();

        foreach ($Q->result() as $AR) {               
            $AAR[$AR->bcc][$AR->YY][$AR->acc_code] = abs($AR->bal);            
        }

        // Expanses account loop for selected end


        // Expanses account loop for comparison year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN ADDDATE('$fd',INTERVAL -1 YEAR) AND ADDDATE('$td',INTERVAL -1 YEAR) GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code WHERE $QBC C.acc_code = '20305'  ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR_PRV = Array();

        foreach ($Q->result() as $AR_PRV) {               
            $AAR_PRV[$AR_PRV->bcc][$AR_PRV->YY][$AR_PRV->acc_code] = abs($AR_PRV->bal);
        }

        // Expanses account loop for comparison end


       /* $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();*/

            
            $acc = 20305;
            
            $t .= '<tr style="color:green">';            
            $t .= '<td>Redeem Discount</td>';                

            foreach ($bc__array as $bc) {
                
                foreach ($time_range as $y) {                   
                        
                    if ( $AAR[$bc][$y][$acc] > 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR[$bc][$y][$acc] , 2 ) .'</td>';
                        $X += floatval($AAR[$bc][$y][$acc]);
                        $tmp_y = $y;
                        $AR_GROUP_TOT[$bc][$y] += floatval($AAR[$bc][$y][$acc]);

                    }elseif( $AAR_PRV[$bc][$y][$acc] > 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR_PRV[$bc][$y][$acc] , 2 ) .'</td>';
                        $Y += floatval($AAR_PRV[$bc][$y][$acc]);
                        $tmp_prv_y = $y;
                        $AR_GROUP_TOT_PRV[$bc][$y] += floatval($AAR_PRV[$bc][$y][$acc]);

                    }else{
                        $t .= '<td align="right">0</td>';
                    }

                } 

            }             

            foreach ($time_range as $y) {

                $abb = false;
                
                if ($y == $tmp_y){
                    $t .= '<td align="right">'.number_format($X,2).'</td>';
                    $X_TOT += $X;
                    $abb = true;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right">'.number_format($Y,2).'</td>';
                    $Y_TOT += $Y;
                    $abb = true;
                }               

                if (!$abb){
                    $t .= '<td align="right">'.number_format(0,2).'</td>';
                }


            }
            
            $t .= '</tr>';

            $X = $Y = 0;












        //----------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------

        $t .= '<tr>';            
        $t .= '<td '.$sub_tot_cLs.'>Total Expanses</td>';

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {

                if ($AR_GROUP_TOT[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT[$bc][$y] ,2).'</td>';
                    $exp_tot[$bc][$y] = $AR_GROUP_TOT[$bc][$y];

                }elseif ($AR_GROUP_TOT_PRV[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT_PRV[$bc][$y],2) .'</td>';
                    $exp_tot_prv[$bc][$y] = $AR_GROUP_TOT_PRV[$bc][$y];                    

                }else{
                    $t .= '<td align="right" '.$sub_tot_cLs.'>0</td>';                    
                }

            }
        }   
            foreach ($time_range as $y) {
                
                $abb = false;

                if ($y == $tmp_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($X_TOT,2).'</td>';                
                    $abb = true;
                    $exp_tot_company[$y] = $X_TOT;
                    $tot_exp_[$y] = $X_TOT;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($Y_TOT,2).'</td>';                
                    $abb = true;
                    $exp_tot_prv_company[$y] = $Y_TOT;
                }

                if (!$abb){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(0,2).'</td>';
                }
                
            }

            

        $t .= '</tr>';

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------


        



        
































        //--------- reset vars -------------------

        unset($AR_GROUP_TOT , $AR_GROUP_TOT_PRV);
        $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();

        //---------- end ----------------------------

        // New expenxes category by classes

        // ------------------------------- EXPENSE CLASS TITLE ---------------------------------

        $t .= '<tr>';            
        $t .= '<td colspan="'.($colspan_count+1).'" '.$sub_tot_cLs_title.'>Head Office Expanses</td>';
        $t .= '</tr>';

        // --------------------------------------------------------------------------------------

        // exp account loop for selected year

        $Q = $this->db->query("SELECT 

IFNULL(YEAR(ACC.ddate),'') AS YY,ACC.`v_bc`,ACC.`v_class`, VC.`description` AS `v_Class` , SUM(ACC.`dr_amount` - ACC.`cr_amount`) AS `bal`

FROM t_account_trans ACC 

JOIN m_voucher_class VC ON ACC.`v_class` = VC.`code` 
JOIN m_account_type ON ACC.`acc_code` = m_account_type.`code` 

WHERE $QBC_EXP bc IN (  SELECT bc FROM m_branches WHERE is_cost_center = 1) AND is_exp = 1 AND m_account_type.`rtype` = 2 AND ddate BETWEEN '$fd' AND '$td' 
GROUP BY v_bc,ACC.v_class ");        

        $EXP_CLS = Array();

        foreach ($Q->result() as $AR_EXP) {               
            $EXP_CLS[$AR_EXP->v_bc][$AR_EXP->YY][$AR_EXP->v_class] = ($AR_EXP->bal);            
        }

        /*echo "<pre>";
        var_dump($EXP_CLS);
        echo "</pre>";
        exit;*/


        $Q_EXP_CLASSE = $this->db->query(" SELECT `code`,`description` FROM m_voucher_class WHERE code in (001,002,003,004) ");


        foreach ($Q_EXP_CLASSE->result() as $exp_res) { // loop with exps classes 

            $t .= '<tr>';
            $t .= '<td>'. $exp_res->description .'</td>';
            
            foreach ($bc__array as $bc) {
                
                foreach ($time_range as $y) {                   
                        
                    if ( $EXP_CLS[$bc][$y][$exp_res->code] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $EXP_CLS[$bc][$y][$exp_res->code] , 2 ) .'</td>';
                        $X += floatval($EXP_CLS[$bc][$y][$exp_res->code]);
                        $tmp_y = $y;
                        $AR_GROUP_TOT[$bc][$y] += floatval($EXP_CLS[$bc][$y][$exp_res->code]);

                    }elseif( $AAR_PRV[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR_PRV[$bc][$y][$acc] , 2 ) .'</td>';
                        $Y += floatval($AAR_PRV[$bc][$y][$acc]);
                        $tmp_prv_y = $y;
                        $AR_GROUP_TOT_PRV[$bc][$y] += floatval($AAR_PRV[$bc][$y][$acc]);

                    }else{
                        $t .= '<td align="right">0</td>';
                    }

                }

            }

            foreach ($time_range as $y) {
                
                $abb = false;

                if ($y == $tmp_y){
                    $t .= '<td align="right">'.number_format($X,2).'</td>';                
                    $abb = true;
                    $X_TOT += $X;
                    $exp_tot_company[$y] = $X_TOT;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right">'.number_format($Y,2).'</td>';                
                    $abb = true;
                    $T_TOT += $Y;
                    $exp_tot_prv_company[$y] = $Y_TOT;
                }

                if (!$abb){
                    $t .= '<td align="right">'.number_format(0,2).'</td>';
                }         
            }
            
            $t .= '</tr>';

            $X = $Y = 0;

        }











        unset($acc_array,$acc_desc_ar);

        $acc_q = $this->db->query("SELECT `code`,heading FROM m_account_type WHERE rtype = 2 ORDER BY heading");
        foreach ($acc_q->result() as $acc_r) {
            $acc_array[] = $acc_r->code;
            $acc_desc_ar[$acc_r->code] = $acc_r->heading;
        }


        // Expanses account loop for selected year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN '$fd' AND '$td' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code WHERE $QBC C.acc_code = '20305' ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR = Array();

        foreach ($Q->result() as $AR) {               
            $AAR[$AR->bcc][$AR->YY][$AR->acc_code] = abs($AR->bal);            
        }

        // Expanses account loop for selected end      

        

























        





































        //------------------------- SUB TOTAL -----------------

        $t .= '<tr>';            
        $t .= '<td '.$sub_tot_cLs.'>Total Expanses</td>';

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {

                if ($AR_GROUP_TOT[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT[$bc][$y] ,2).'</td>';
                    $exp_tot[$bc][$y] = $AR_GROUP_TOT[$bc][$y];

                }elseif ($AR_GROUP_TOT_PRV[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT_PRV[$bc][$y],2) .'</td>';
                    $exp_tot_prv[$bc][$y] = $AR_GROUP_TOT_PRV[$bc][$y];                    

                }else{
                    $t .= '<td align="right" '.$sub_tot_cLs.'>0</td>';                    
                }

            }
        }   
            foreach ($time_range as $y) {
                
                $abb = false;

                if ($y == $tmp_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($X_TOT,2).'</td>';                
                    $abb = true;
                    $exp_tot_company[$y] = $X_TOT;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($Y_TOT,2).'</td>';                
                    $abb = true;
                    $exp_tot_prv_company[$y] = $Y_TOT;
                }

                if (!$abb){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(0,2).'</td>';
                }
                
            }

        $t .= '</tr>';



        //------------------------- SUB TOTAL -----------------

        $t .= '<tr>';            
        $t .= '<td '.$sub_tot_cLs.'>Net Ordinary Income</td>';

      

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {

                if ( ($income_tot[$bc][$y]) - ($exp_tot[$bc][$y]) > 0){

                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format( ($income_tot[$bc][$y]) - ($exp_tot[$bc][$y]) ,2).'</td>';                    
                }elseif (($income_tot_prv[$bc][$y]) - ($exp_tot_prv[$bc][$y]) > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format( ($income_tot_prv[$bc][$y]) - ($exp_tot_prv[$bc][$y]) ,2) .'</td>';                    
                }else{
                    $t .= '<td align="right" '.$sub_tot_cLs.'>0</td>';                    
                }

            }
        }   
            foreach ($time_range as $y) {

                //echo $y." = ".$income_tot_company[$y]." - ".$y." = ".$exp_tot_company[$y]."<br>";
                
                $abb = false;

                if ($income_tot_company[$y] > 0){ //echo "Y";
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(($income_tot_company[$y] - ($exp_tot_company[$y] + $tot_exp_[$y]) )  ,2).'</td>';                
                    $abb = true;
                }

                if ($income_tot_prv_company[$y] > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(($income_tot_prv_company[$y] - $exp_tot_prv_company[$y] ),2).'</td>';                
                    $abb = true;
                }

                if (!$abb){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(0,2).'</td>';
                }
                
            }

        $t .= '</tr>';




























        //--------- reset vars -------------------

        unset($AR_GROUP_TOT , $AR_GROUP_TOT_PRV);
        $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();

        //---------- end ----------------------------
























        // ------------------------------- ACCOUNT TITLE---

        $t .= '<tr>';            
        $t .= '<td colspan="'.($colspan_count+1).'" '.$sub_tot_cLs_title.'>Other Income</td>';
        $t .= '</tr>';

        // ------------------------------------------------


        unset($acc_array,$acc_desc_ar);

        $acc_q = $this->db->query("SELECT `code`,heading FROM m_account_type WHERE rtype = 1 AND `code` NOT IN (10101,10102,10103,1,101) ORDER BY heading");
        foreach ($acc_q->result() as $acc_r) {
            $acc_array[] = $acc_r->code;
            $acc_desc_ar[$acc_r->code] = $acc_r->heading;
        }


        // Other income account loop for selected year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN '$fd' AND '$td' GROUP BY bc,acc_code ) C ON A.`bc` = C.bc AND B.code = C.acc_code WHERE $QBC B.`rtype` = 1 AND NOT B.code IN ('10101','10102','10103') ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR = Array();

        foreach ($Q->result() as $AR) {               
            $AAR[$AR->bcc][$AR->YY][$AR->acc_code] = ($AR->bal);            
        }

        // Other income account loop for selected end


        // Other income account loop for comparison year

        $Q = $this->db->query("SELECT IFNULL(YEAR(C.ddate),'') AS YY, A.`bc` AS `bcc`,IFNULL(C.ddate,''),B.`heading`,IFNULL(C.acc_code,'') AS `acc_code`,IFNULL(C.bal,0) AS `bal`  FROM m_branches A CROSS JOIN m_account_type B LEFT JOIN ( SELECT v_bc,ddate,acc_code, SUM(cr_amount) - SUM(dr_amount) AS `bal` FROM t_account_trans WHERE ddate BETWEEN ADDDATE('$fd',INTERVAL -1 YEAR) AND ADDDATE('$td',INTERVAL -1 YEAR) GROUP BY v_bc,acc_code ) C ON A.`bc` = C.v_bc AND B.code = C.acc_code WHERE $QBC B.`rtype` = 1 AND NOT B.code IN ('10101','10102','10103') ORDER BY A.bc,YEAR(C.ddate) ");        

        $AAR_PRV = Array();

        foreach ($Q->result() as $AR_PRV) {               
            $AAR_PRV[$AR_PRV->bcc][$AR_PRV->YY][$AR_PRV->acc_code] = ($AR_PRV->bal);
        }

        // Other income account loop for comparison end


        $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();

        foreach ($acc_array as $acc) {
            
            $t .= '<tr>';            
            $t .= '<td>'.$acc.' - '.$acc_desc_ar[$acc].'</td>';                

            foreach ($bc__array as $bc) {
                
                foreach ($time_range as $y) {                   
                        
                    if ( $AAR[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR[$bc][$y][$acc] , 2 ) .'</td>';
                        $X += floatval($AAR[$bc][$y][$acc]);
                        $tmp_y = $y;
                        $AR_GROUP_TOT[$bc][$y] += floatval($AAR[$bc][$y][$acc]);

                    }elseif( $AAR_PRV[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR_PRV[$bc][$y][$acc] , 2 ) .'</td>';
                        $Y += floatval($AAR_PRV[$bc][$y][$acc]);
                        $tmp_prv_y = $y;
                        $AR_GROUP_TOT_PRV[$bc][$y] += floatval($AAR_PRV[$bc][$y][$acc]);

                    }else{
                        $t .= '<td align="right">0</td>';
                    }

                } 

            }             

            foreach ($time_range as $y) {

                $abb = false;
                
                if ($y == $tmp_y){
                    $t .= '<td align="right">'.number_format($X,2).'</td>';
                    $X_TOT += $X;
                    $abb = true;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right">'.number_format($Y,2).'</td>';
                    $Y_TOT += $Y;
                    $abb = true;
                }               

                if (!$abb){
                    $t .= '<td align="right">'.number_format(0,2).'</td>';
                }


            }
            
            $t .= '</tr>';

            $X = $Y = 0;

        }

        //------------------------- SUB TOTAL -----------------

        $t .= '<tr>';            
        $t .= '<td '.$sub_tot_cLs.'>Total Other Income</td>'; 

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {

                if ($AR_GROUP_TOT[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT[$bc][$y] ,2).'</td>';
                    $other_income_tot[$bc][$y] = $AR_GROUP_TOT[$bc][$y];

                }elseif ($AR_GROUP_TOT_PRV[$bc][$y] > 0){
                    
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format($AR_GROUP_TOT_PRV[$bc][$y],2) .'</td>';                    
                    $other_income_tot_prv[$bc][$y] = $AR_GROUP_TOT_PRV[$bc][$y];

                }else{
                    $t .= '<td align="right" '.$sub_tot_cLs.'>0</td>';                    
                }

            }
        }   
            foreach ($time_range as $y) {
                
                $abb = false;

                if ($y == $tmp_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($X_TOT,2).'</td>';                
                    $abb = true;
                    $other_income_tot_company[$y] = $X_TOT;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($Y_TOT,2).'</td>';                
                    $abb = true;
                    $other_income_tot_prv_company[$y] = $Y_TOT;
                }

                if (!$abb){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(0,2).'</td>';
                }
                
            }

        $t .= '</tr>';












        //--------- reset vars -------------------

        unset($AR_GROUP_TOT , $AR_GROUP_TOT_PRV);
        $X = $Y = $X_TOT = $Y_TOT = 0;
        $tmp_y = $tmp_prv_y = '';
        $AR_GROUP_TOT = $AR_GROUP_TOT_PRV = Array();

        //---------- end ----------------------------

        // New expenxes category by classes

        // ------------------------------- EXPENSE CLASS TITLE ---------------------------------

        $t .= '<tr>';            
        $t .= '<td colspan="'.($colspan_count+1).'" '.$sub_tot_cLs_title.'>Income Class</td>';
        $t .= '</tr>';

        // --------------------------------------------------------------------------------------

        // exp account loop for selected year

        $Q = $this->db->query("SELECT IFNULL(YEAR(ACC.ddate),'') AS YY,ACC.`v_bc`,ACC.`v_class`, VC.`description` AS `v_Class` , SUM(ACC.`dr_amount` - ACC.`cr_amount`) AS `bal`

FROM `m_voucher_class` VC

JOIN t_account_trans    ACC     ON VC.`code` = ACC.`v_class`
JOIN m_account_type     MA  ON ACC.`acc_code` = MA.`code`


WHERE $QBC_EXP MA.rtype = 1 AND  ACC.`ddate` BETWEEN '$fd' AND '$td'

GROUP BY ACC.`v_bc` , ACC.`v_class` ");        

        $EXP_CLS = Array();

        foreach ($Q->result() as $AR_EXP) {               
            $EXP_CLS[$AR_EXP->v_bc][$AR_EXP->YY][$AR_EXP->v_class] = ($AR_EXP->bal);            
        }

        


        $Q_EXP_CLASSE = $this->db->query(" SELECT `code`,`description` FROM m_voucher_class WHERE is_exp = 2 ");


        foreach ($Q_EXP_CLASSE->result() as $exp_res) { // loop with exps classes 

            $t .= '<tr>';
            $t .= '<td>'. $exp_res->description .'</td>';
            
            foreach ($bc__array as $bc) {
                
                foreach ($time_range as $y) {                   
                        
                    if ( $EXP_CLS[$bc][$y][$exp_res->code] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $EXP_CLS[$bc][$y][$exp_res->code] , 2 ) .'</td>';
                        $X += floatval($EXP_CLS[$bc][$y][$exp_res->code]);
                        $tmp_y = $y;
                        $AR_GROUP_TOT[$bc][$y] += floatval($EXP_CLS[$bc][$y][$exp_res->code]);

                    }elseif( $AAR_PRV[$bc][$y][$acc] <> 0 ){
                        
                        $t .= '<td align="right">'. number_format( $AAR_PRV[$bc][$y][$acc] , 2 ) .'</td>';
                        $Y += floatval($AAR_PRV[$bc][$y][$acc]);
                        $tmp_prv_y = $y;
                        $AR_GROUP_TOT_PRV[$bc][$y] += floatval($AAR_PRV[$bc][$y][$acc]);

                    }else{
                        $t .= '<td align="right">0</td>';
                    }

                }

            }

            foreach ($time_range as $y) {
                
                $abb = false;

                if ($y == $tmp_y){
                    $t .= '<td align="right">'.number_format($X,2).'</td>';                
                    $abb = true;
                    $X_TOT += $X;
                    $exp_tot_company[$y] = $X_TOT;
                }

                if ($y == $tmp_prv_y){
                    $t .= '<td align="right">'.number_format($Y,2).'</td>';                
                    $abb = true;
                    $T_TOT += $Y;
                    $exp_tot_prv_company[$y] = $Y_TOT;
                }

                if (!$abb){
                    $t .= '<td align="right">'.number_format(0,2).'</td>';
                }         
            }
            
            $t .= '</tr>';

            $X = $Y = 0;

        }

        















        //------------------------- SUB TOTAL -----------------

        $t .= '<tr>';            
        $t .= '<td '.$sub_tot_cLs.'>Net Other Income</td>';

      

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {

                if ($income_tot[$bc][$y] > 0){

                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format( $other_income_tot[$bc][$y] ,2).'</td>';                    
                }elseif ($income_tot_prv[$bc][$y] > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format( $other_income_tot_prv[$bc][$y] ,2) .'</td>';                    
                }else{
                    $t .= '<td align="right" '.$sub_tot_cLs.'>0</td>';                    
                }

            }
        }   
            foreach ($time_range as $y) {
                
                $abb = false;

                if ($income_tot_company[$y] > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($other_income_tot_company[$y],2).'</td>';                
                    $abb = true;
                }

                if ($income_tot_prv_company[$y] > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format($other_income_tot_prv_company[$y],2).'</td>';                
                    $abb = true;
                }

                if (!$abb){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(0,2).'</td>';
                }
                
            }

        $t .= '</tr>';












/*
        //------------------------- SUB TOTAL -----------------

        $t .= '<tr>';            
        $t .= '<td '.$sub_tot_cLs.'>Net Income</td>';

      

        foreach ($bc__array as $bc) {
            foreach ($time_range as $y) {

                if ($income_tot[$bc][$y] > 0){

                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format( 

                        (($income_tot[$bc][$y]) - ($exp_tot[$bc][$y])) + ($other_income_tot[$bc][$y] - $other_exp_tot )

                     ,2).'</td>';   

                }elseif ($income_tot_prv[$bc][$y] > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'. number_format( 

                        (($income_tot_prv[$bc][$y]) - ($exp_tot_prv[$bc][$y])) + ($other_income_tot_prv[$bc][$y] - $other_exp_tot_prv)

                     ,2) .'</td>';                    
                }else{
                    $t .= '<td align="right" '.$sub_tot_cLs.'>0</td>';                    
                }

            }
        }   
            foreach ($time_range as $y) {
                
                $abb = false;

                if ($income_tot_company[$y] > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format( ($income_tot_company[$y] - $exp_tot_company[$y]) + $other_income_tot_company[$y] ,2).'</td>';                
                    $abb = true;
                }

                if ($income_tot_prv_company[$y] > 0){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format( ($income_tot_prv_company[$y] - $exp_tot_prv_company[$y]) + $other_income_tot_prv_company[$y] ,2).'</td>';                
                    $abb = true;
                }

                if (!$abb){
                    $t .= '<td align="right" '.$sub_tot_cLs.'>'.number_format(0,2).'</td>';
                }
                
            }

        $t .= '</tr>';


        */












        $t .= '</table>';



        if (!$make_excel){
            echo $t;
        }else{
            header('Content-type: application/excel');
            $filename = 'income_statement.xls';
            header('Content-Disposition: attachment; filename='.$filename);
            $data = $t;
            echo $data;
        }


        /*if($Q->num_rows() > 0){

            $r_data['list'] = $Q->result();
            $r_data['fd'] = $fd;
            $r_data['td'] = $td;

            $this->load->view($_POST['by'].'_'.'pdf',$r_data);
        }else{
            echo "<script>location='default_pdf_error'</script>";
        }*/



    }
    
}