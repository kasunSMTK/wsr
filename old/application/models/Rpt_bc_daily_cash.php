<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_bc_daily_cash extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}   

	public function PDF_report($_POST_){


		$bc = $_POST_['bc'];
		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];

		$nic = $_POST['txt_rpt_cus_info'];

		if (isset($_POST['bc_n'])){
			if ( $_POST['bc_n'] == "" ){        
				$QBC = "";
				$QBC2 = "";
			}        
		}else{

			for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
				$bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
			}

			$bc = implode(',', $bc_ar);

			if ($bc == ""){
				$QBC = "";
				$QBC2 = "";
			}else{
				
				$QBC  = " dcb.bc IN ($bc) AND ";
				$QBC2 = " B.bc IN ($bc) AND ";
			}
		}

		



		$Q 	= $this->db->query("	SELECT B.`bc` , B.`name` as `bc_name` , IFNULL( DCB.`date` , ' - ' ) AS `date`, IFNULL( DCB.`amount` , 'Data not added') AS `amount`
									
									FROM `m_branches` B
									
									LEFT JOIN ( 	SELECT dcb.bc,dcb.date,dcb.amount 
													FROM t_bc_daily_cash_balance dcb 
													WHERE $QBC dcb.`date` BETWEEN '$fd' AND '$td' ) DCB ON B.`bc` = DCB.bc
									
									WHERE $QBC2 B.`bc_type` = 1 
									
									ORDER BY B.`bc` , DCB.`date` ");		

		if($Q->num_rows() > 0){
			
			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;
			$r_data['barnch']= $this->db->select("name")->where("bc",$bc)->get("m_branches")->row()->name;
			$r_data['bc'] = $QBC;
			//$X              = $this->load->model('calculate');
        	//$r_data['X']    = $X;       
			$this->load->view($_POST['by'].'_'.'pdf',$r_data);
		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}