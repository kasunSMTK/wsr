    <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S_add_role extends CI_Model {
    
    private $sd;
    private $mtb;
    private $mtbl;
    private $mtbl2;
    private $mod = '019';
    
    function __construct(){
	parent::__construct();
	
        $this->load->library('session');
        $this->load->library('useclass');
        
    	$this->sd = $this->session->all_userdata();
    	
        $this->mtbl  = 'u_add_user_role';
        $this->mtbl2 = 'u_add_user_role_log';
        
        $this->mtb = $this->tables->tb['u_user_role'];
    	$this->load->model('user_permissions');
    }
    
    public function base_details(){
        $this->load->model(array('s_users','m_branches'));        
        $a['table_data'] = $this->data_table();
        $a['table_data_new'] = $this->data_table_new();
        $a['user'] = $this->s_users->select();
        $a['bc'] = $this->m_branches->select();
        return $a;
    }
    
    public function data_table(){
	return $this->db->get($this->mtb)->result();
    }
    
    public function data_table_new(){
	return $this->db->get($this->mtbl)->result();
    }
    
    private function delete(){
	   // $this->db->where("bc", $this->sd['bc']);
	   $this->db->where("user_id", $this->input->post('loginName'));	
       $this->db->delete($this->mtbl);
    }
    
    public function save(){
    //this->user_permissions->verify_permission("save",$this->mod);
    $this->db->trans_begin();
    $this->delete();
        
	$a = array();
	foreach($this->data_table() as $r){ 

            //echo $this->input->post('is_active'.$r->role_id)."<br>";
	   
            if($this->input->post('is_active'.$r->role_id)==1){ 
                
                $a = array(
                    "user_id"=>$this->input->post('loginName'),
                    "date_from"=>$this->input->post('date_to'.$r->role_id),
                    "date_to"=>$this->input->post('date_from'.$r->role_id),
                    "role_id"=>$this->input->post('role_id'.$r->role_id),
                    
                    "bc"=>$this->db->query("SELECT bc FROM u_users WHERE cCode = '".$this->input->post("loginName")."' LIMIT 1 ")->row()->bc,

                    "cl"=>$this->sd['cl']
                );
                $this->db->insert($this->mtbl, $a);
                $this->db->insert($this->mtbl2, $a);
            }
	}
	echo $this->db->trans_commit();

	//redirect(base_url()."?action=s_add_role");
	
    }
    
    public function dates(){
	$t = time(); $a = array(); $x = 0;
	foreach($this->db->get($this->mtb)->result() as $r){
	    $std = new stdClass;
	    
	    if($r->type == 2){ $r->range *= 7; }
	    $tt = $t - ($r->range * 84600);
	    
	    $std->date = date("Y-m-d", $tt);
	    $std->description = $r->description;
	    $std->key = substr(md5($r->description.$t), 0, 5);
	    
	    $a[] = $std; $x++;
	}
	
	return $a;
    }
    
    public function load(){
	   $q = $this->db->where('user_id', $this->input->post('id'))->get($this->mtbl);
	   $a['d'] = $q->result();
       echo json_encode($a);
    }




    public function set_bc_users(){

        $bc = $_POST['bc'];
        $Q = $this->db->query("SELECT * FROM u_users WHERE bc = '$bc'");
        $dd = "";


        if ( $Q->num_rows() > 0 ){
            
            $dd .= "<option>Select user</option>";

            foreach( $Q->result() as $r ){
                $dd .= "<option title='".$r->loginName."' value='".$r->cCode."'>".$r->loginName." - ".$r->display_name."</option>";
            }

        }else{
            $dd = "<option>No users added for this branch</option>";
        }

        $a['dd'] = $dd;

        echo json_encode($a);

    }








}