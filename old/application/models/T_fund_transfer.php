<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_fund_transfer extends CI_Model {

	private $sd;
    private $max_no;

    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function get_bc_name($bc){
    $res = $this->db->get_where('m_branches', array('bc'=>$bc));

    if ($res->num_rows() > 0){
        return $res->first_row()->name;        
    }else{
        return "Branch name not found";
    }

  }

  public function base_details(){		
      
      $a['isAdmin'] = $this->sd['isAdmin'];
      $a['current_date'] = $this->sd['current_date'];
      $a['date_change_allow'] = $this->sd['date_chng_allow'];
      $a['backdate_upto'] = $this->sd['backdate_upto'];
      
      $a['cash_cheque_grid'] = $this->cash_cheque_grid();
      $a['bc_code_name'] = $this->sd['bc'] ." - ". $this->sd['user_branch'];
      $a['bc'] = $this->sd['bc'];
      
      $a['my_requ'] = 65;
      $a['incomg_requ'] = 154;

      $a['my_requests']     = $this->my_requests();
      $a['my_requests_HO']  = $this->my_requests_HO();

      $a['incomming_requests']      = $this->incomming_requests();
      $a['incomming_requests_HO']   = $this->incomming_requests_HO();  


          

      $a['ho_cash_acc'] = $this->ho_cash_acc();

      $a['bc_list_with_cash_acc_codes_f'] = $this->bc_list_with_cash_acc_codes("from_bc");
      $a['bc_list_with_cash_acc_codes_t'] = $this->bc_list_with_cash_acc_codes("to_bc");

      $a['fnud_tr_bnk_dd_from'] = $this->get_bank_accounts_dropdown('BB_from_acc');
      $a['fnud_tr_bnk_dd_to'] = $this->get_bank_accounts_dropdown('BB_to_acc');

      $a['headoffice_code'] = $this->headoffice_code();

      return $a;
  }    


  public function get_bank_accounts_dropdown($name){

        $q = $this->db->query("SELECT `code`,`description` FROM m_account WHERE is_bank_acc = 1 ORDER BY description");

        $d = '<select class="input_text_regular_ftr" style="width:100%" name="'.$name.'">';

        if ($q->num_rows() > 0){

            $d .= '<option value="">Select bank acc</option>';

            foreach ($q->result() as $r) {
                $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }

        }


        $d .= '</select>';

        return $d;

  }

  public function get_bank_acc_dropdown(){

    $this->load->model('t_voucher_general');
    $a['bnk_dd'] = $this->t_voucher_general->get_bank_acc();

    echo json_encode($a);

  }

  public function headoffice_code(){
    return $this->db->query("SELECT bc FROM m_branches WHERE is_hO = 1 LIMIT 1 ")->row()->bc;
  }

  public function bc_list_with_cash_acc_codes($name){

    $q = $this->db->query("SELECT B.`bc`, B.`name`, B.`cash_acc` FROM `m_branches` B WHERE NOT B.`is_ho` = 1");
    
    $d = '<select class="from_bc input_text_regular_ftr">';
    
    if ($q->num_rows() > 0){
        $d .= "<option>Select Branch</option>";
        foreach ($q->result() as $r) {            
            $d .= "<option bc='".$r->bc."' cash_acc='".$r->cash_acc."'>".$r->name."</option>";
        }
    }else{
        $d .= "<option>no branches found</option>";
    }

    $d .= "</select>";

    return $d;

  }

  public function ho_cash_acc(){

    $q = $this->db->query("SELECT B.`cash_acc` FROM `m_branches` B WHERE B.`is_ho` = 1 ");

    if ($q->num_rows()  > 0){
        return $q->row()->cash_acc;
    }else{
        return "HO cash acc not set";
    }

  }
  
    public function save_and_print_ftr_voucher(){

        $no = $_POST['no'];       

        if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 6 ){
            $a['s'] = 2;
            echo json_encode($a);
            exit;
        }

        $dt = date('Y-m-d');
        $is_v_re_p = $_POST['is_v_re_p'];
        
        $tt = $_POST['tt'];

        $fb = $_POST['fb'];

        if ($is_v_re_p == 0){            
            
            $Q = $this->db->query("UPDATE `t_fund_transfer_sum` SET `is_v_print` = 1 ,`status` = 4  WHERE `no` = $no LIMIT 1 ");
           
            $DATA['action_taken_by']    = $this->sd['oc'];
            $DATA['related_model']      = "fund transfer";
            
            $this->utility->user_activity_log($module='Fund Transfer',$action='print',$trans_code='9',$trans_no=$no,$note='voucher print');

        }
        
        if ($is_v_re_p == 1){
            $a['s'] = 1;
            $a['v_no']= $no;            
            $this->utility->user_activity_log($module='Fund Transfer',$action='re-print',$trans_code='9',$trans_no=$no,$note='voucher re-print');
        }else{
            if ($Q){
                $a['s'] = 1;
                $a['v_no']= $no;                 
            }else{
                $a['s'] = 0;
            }
        }

        echo json_encode($a);

    }


    

  public function save(){

    /*var_dump($_POST);
    exit;*/

    $this->db->trans_begin();
        
        $_POST['no'] = $this->get_max_no();
        $_POST['transfer_type'] = $_POST['tt_opt'];
        $_POST['date_time'] = date('Y-m-d h:i:s');
        $_POST['status'] = 1;
        $_POST['requ_initi_bc'] = $this->sd['bc'];
        $_POST['requ_initi_by'] = $this->sd['oc'];
        $_POST['requ_initi_date_time'] = $_POST['date_time'];
        $_POST['added_date'] = $this->sd['current_date'];

        
        if ($_POST['transfer_type'] == "bank_deposit"){
            $_POST['is_bank_acc'] = 1;
            $_POST['ref_no'] = $_POST['bnk_acc_desc'] ;
        }else{
            $_POST['is_bank_acc'] = 0;
            $_POST['ref_no'] = $_POST['ref_no'] ;
        }

        if ($_POST['fund_tr_opt_no'] == 1 && $_POST['transfer_type'] == "bank_deposit"){
            $_POST['from_acc'] = $this->utility->get_default_acc('CASH_IN_HAND');
        }

        if ($_POST['fund_tr_opt_no'] == 4){
            $_POST['to_bc']     = $this->sd['bc'];            
            $_POST['to_acc']    = $this->utility->get_default_acc('CASH_IN_HAND');
        }


        if ($_POST['fund_tr_opt_no'] == 5){

            $_POST['from_bc']     = "HO";            
            $_POST['from_acc']    = $_POST['BB_from_acc'];
            
            $_POST['to_bc']     = "HO";            
            $_POST['to_acc']    = $_POST['BB_to_acc'];

            unset($_POST['BB_from_acc'],$_POST['BB_to_acc']);
        }


        // notification set

        if ( $_POST['fund_tr_opt_no'] == 1 && $_POST['transfer_type'] == "cash" ){
            
            /*$DATA['bc']                 = $_POST['from_bc'];
            $DATA['action_taken_by']    = $this->sd['oc'];
            $DATA['related_model']      = "fund transfer";
            $DATA['action_window']      = "t_fund_transfer#tabs-3";
            $DATA['noti_description']   = "Head office requesting cash. amount of Rs. ".$_POST['amount'];*/
            
            $this->utility->user_activity_log($module='Fund Transfer',$action='insert',$trans_code='9',$trans_no=$_POST['no'],$note="Head office requesting cash. amount of Rs. ".$_POST['amount']." from ".$_POST['from_bc']);

        }

        if ( $_POST['fund_tr_opt_no'] == 1 && $_POST['transfer_type'] == "bank_deposit" ){
            
            /*$DATA['bc']                 = $_POST['from_bc'];
            $DATA['action_taken_by']    = $this->sd['oc'];
            $DATA['related_model']      = "fund transfer";
            $DATA['action_window']      = "t_fund_transfer#tabs-3";
            $DATA['noti_description']   = "Head office asking deposit cash amount of Rs. ".$_POST['amount']." into ".$_POST['bnk_acc_desc'] ;*/
            
            $this->utility->user_activity_log($module='Fund Transfer',$action='insert',$trans_code='9',$trans_no=$_POST['no'],$note="Head office asking deposit cash amount of Rs. ".$_POST['amount']." into ".$_POST['bnk_acc_desc']." from ".$_POST['from_bc']);

        }

        if ( $_POST['fund_tr_opt_no'] == 2 && $_POST['transfer_type'] == "cash" ){
            
            /*$DATA['bc']                 = $_POST['from_bc'];
            $DATA['action_taken_by']    = $this->sd['oc'];
            $DATA['related_model']      = "fund transfer";
            $DATA['action_window']      = "t_fund_transfer#tabs-3";
            $DATA['noti_description']   = "Head office requesting handover cash amount of ".$_POST['amount']." to ".$_POST['to_bc'];*/
            
            $this->utility->user_activity_log($module='Fund Transfer',$action='insert',$trans_code='9',$trans_no=$_POST['no'],$note="Head office requesting handover cash amount of ".$_POST['amount']." to ".$_POST['to_bc']." from ".$_POST['from_bc']);

            //----------------------------------------------------------------------------------------------------

            /*$DATA['bc']                 = $_POST['to_bc'];
            $DATA['action_taken_by']    = $this->sd['oc'];
            $DATA['related_model']      = "fund transfer";
            $DATA['action_window']      = "t_fund_transfer#tabs-2";
            $DATA['noti_description']   = "Head office initialized a fund transfer from ".$_POST['from_bc']." to your branch. Transfer amount is Rs ".$_POST['amount'];*/
            
            $this->utility->user_activity_log($module='Fund Transfer',$action='insert',$trans_code='9',$trans_no=$_POST['no'],$note="Head office initialized a fund transfer from ".$_POST['from_bc']." to your branch. Transfer amount is Rs ".$_POST['amount']);

        }


        if ( $_POST['fund_tr_opt_no'] == 4 && $_POST['transfer_type'] == "cash" ){
            
            /*$DATA['bc']                 = 'HO';
            $DATA['action_taken_by']    = $this->sd['oc'];
            $DATA['related_model']      = "fund transfer";
            $DATA['action_window']      = "t_fund_transfer#tabs-3";
            $DATA['noti_description']   = $this->sd['bc']." requesting cash amount of ".$_POST['amount'];*/
            
            $this->utility->user_activity_log($module='Fund Transfer',$action='insert',$trans_code='9',$trans_no=$_POST['no'],$note=$this->sd['bc']." requesting cash amount of ".$_POST['amount']." from head office");

        }

        










        unset($_POST['tt_opt'], $_POST['bnk_acc_desc']);

        $this->db->insert("t_fund_transfer_sum",$_POST);
        

    if ($this->db->trans_status() === FALSE){
        
        $this->db->trans_rollback();
        $a['s'] = "error";

    }else{

        $this->db->trans_commit();        
        $a['s'] = 1;

    }

    echo json_encode($a);

}



public function load(){

    $no = $_POST['no'];
    $Q = $this->db->query("SELECT S.*,ACC.display_text AS `bank_name`,BC.name AS `i_bc_name` FROM `t_chq_reg_sum` S JOIN( SELECT * FROM m_account WHERE is_bank_acc = 1) ACC ON S.`bank_acc` = ACC.code JOIN( SELECT * FROM m_branches) BC ON S.`issue_bc` = BC.bc WHERE S.`no` = $no LIMIT 1");

    if ($Q->num_rows() > 0){
        $QQ = $this->db->query("SELECT * FROM `t_chq_reg_det` WHERE NO = $no");
        $a['sum'] = $Q->row();
        $a['det'] = $QQ->result();
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}

public function get_max_no(){
    return $this->db->query("SELECT IFNULL(MAX(`no`)+1,1) AS `max_no` FROM `t_fund_transfer_sum`")->row()->max_no;
}

public function auto_com(){               
    $q = $_GET['term'];
    $query = $this->db->query("SELECT * FROM m_account WHERE is_bank_acc = 1 AND (`code` LIKE '%$q%' OR `description` LIKE '%$q%' ) ");                
    $ary = array();    
    foreach($query->result() as $r){ $ary [] = $r->code . " - ". $r->description ; }    
    echo json_encode($ary);
}


public function i_bc_auto_com(){               
    $q = $_GET['term'];
    $query = $this->db->query("SELECT bc,`name` FROM m_branches WHERE (`bc` LIKE '%$q%' OR `name` LIKE '%$q%' ) ");                
    $ary = array();    
    foreach($query->result() as $r){ $ary [] = $r->bc . " - ". $r->name ; }    
    echo json_encode($ary);
}

public function bnk_acc_name(){               
    $q = $_GET['term'];
    $query = $this->db->query("SELECT A.`code`,A.`description` FROM  m_account A WHERE A.`is_bank_acc` = 1 AND A.`is_control_acc` = 0 AND (A.`code` LIKE '%$q%' OR A.`description` LIKE '%$q%')");
    $ary = array();    
    foreach($query->result() as $r){ $ary [] = $r->code . " - ". $r->description ; }    
    echo json_encode($ary);
}


public function cash_cheque_grid(){

    $bc = $this->sd['bc'];
    $T = "";

    $Q = $this->db->query(" SELECT
S.no,
S.`added_date`,
S.`ref_no`,
SS.`bank_acc`,
SS.`memo`,
CD.`chq_no`,
CD.`amount`,
ACC.description,
BC.name,
IFNULL(S.`status`, 0) AS `status`,
S.`comment`,

IF ( S.status = 3 OR ISNULL(S.status) , 1 ,0 ) AS `s`

FROM `t_chq_reg_det` CD 

LEFT JOIN `t_fnd_trnsfr_rqsts_chq_det` D    ON CD.`branch` = D.`bc` AND CD.`chq_no` = D.`cheque_no` AND NOT D.`status` IN (2,3,6) 
LEFT JOIN `t_fund_transfer_sum` S       ON D.`bc` = S.`to_bc` AND D.`no` = S.`no`
LEFT JOIN `t_chq_reg_sum` SS            ON CD.`no` = SS.`no` AND CD.`branch` = SS.`issue_bc`

JOIN( SELECT * FROM m_account WHERE is_bank_acc = 1) ACC ON SS.`bank_acc` = ACC.code 
JOIN( SELECT * FROM m_branches) BC ON SS.`issue_bc` = BC.bc 

WHERE CD.branch = '$bc' AND CD.`status` = 'P'  ");


    if ($Q->num_rows() > 0){
        
        $n = 0;
        
        foreach($Q->result() as $r){     


            if ($r->status == 0 || $r->status == 1 || $r->status == 3){
            
                $action = "";
                $tx = "";
                $tx_di = "";

                if ($r->status == 0){
                    $action   = "<input type='button' value='Send Request' class='send_chq_wdr_rqu btn_regular' style='width:100px'>";
                }

                if ($r->status == 1){
                    $action   = "<input type='button' value='Waiting...' class='' no='".$r->no."'>";
                    $tx = $r->comment ;
                    $tx_di = "disabled='disabled'";
                }               

                if ($r->status == 3){
                    $action =  "<input type='button' value='Rejected' class='' no='".$r->no."'><br>";                
                    $action .= "<input type='button' value='Re-Send Request' class='send_chq_wdr_rqu btn_regular' style='width:150px; margin-top:3px'>";
                    $tx = $r->comment ;
                }

                $T .= " <tr>                                                                                               
                            <td>(".($n+1).") ".$r->description." 
                                <input type='hidden' class='bank_code' value='".$r->bank_acc."'>
                                <input type='hidden' class='chq_no'     value='".$r->chq_no."'>
                                <input type='hidden' class='amount'    value='".$r->amount."'>
                            </td>            
                            <td align='center'>".$r->chq_no."</td>
                            <td align='right'>".number_format($r->amount,2)."</td>                                                                
                            <td><input type='text' class='chqu_wrd_comment input_text_regular_ftr' value='$tx' $tx_di ></td>
                            <td align='right'>$action</td>            
                        </tr>";

                $n++;

            }            

        }

        $T .="<input type='hidden' id='chq_num_rows' value='".$n."'>";

    }else{

        $T .= ' <tr>
                <td colspan="5" align="center">
                    <span>No cheques added for this branch, Please contact head office</span>                        
                </td>
            </tr>';

        $T .="<input type='hidden' id='chq_num_rows' value='1'>";

    }

    return $T;

}


public function my_requests(){

    $bc = $this->sd['bc'];
    $a['my_requ_rc'] = "";

    $Q = $this->db->query(" SELECT
                            S.* , CHQ_DET.`bank`,CHQ_DET.`cheque_no`,CHQ_DET.`amount`  as `chq_amount`
                            FROM `t_fund_transfer_sum` S 
                            LEFT JOIN `t_fnd_trnsfr_rqsts_chq_det` CHQ_DET ON S.`no` = CHQ_DET.`no` 
                            WHERE (S.`to_bc` = '$bc' AND S.`fund_tr_opt_no` = 2) OR S.`requ_initi_bc` = '$bc' 
                            AND S.`status` IN (1,2,3,4,6,10) 
                            ORDER BY no DESC ");

    $T = "";

    if ($Q->num_rows() > 0){

        $a['my_requ_rc'] = $Q->num_rows(); 
        
        $n = 0;
        $desc = "";

        foreach($Q->result() as $r){

            $desc   = ""; 
            $action = "";

            if ($r->fund_tr_opt_no == 1){
                if ($r->transfer_type == "cash"){ $desc = "Requesting handover cash amount of ".$r->amount . " from ".$this->get_bc_name($r->from_bc); }
                if ($r->transfer_type == "bank_deposit"){ $desc = "Requesting deposit cash amount of ".$r->amount." to bank account number ".$r->to_acc; }

                if ($r->status == 1){   $action  = "<input type='button' value='Waiting'            class='' no='".$r->no."'>"; }
                if ($r->status == 2){   $action .= "<input type='button' value='Voucher Printed'    class='' no='".$r->no."'>"; }
                if ($r->status == 10){  $action .= "<input type='button' value='Fund Received'      class='' no='".$r->no."'>"; }
            }

            if ($r->fund_tr_opt_no == 2){
                
                if ($r->transfer_type == "cash"){ $desc = $r->requ_initi_bc. " initialized a fund transfer from ".$this->get_bc_name($r->from_bc)." to your branch. Transfer amount is Rs ".$r->amount; }
                if ($r->transfer_type == "bank_deposit"){ $desc = $this->get_bc_name($r->requ_initi_bc)." initialized a fund transfer (cash deposit) from ".$this->get_bc_name($r->from_bc)." branch. Transfer amount is ".$r->amount; }

                if ($r->status == 1){   $action .= "<input type='button' value='Waiting for ".$this->get_bc_name($r->from_bc)." Response'>"; }
                if ($r->status == 2){   $action .= "<input type='button' value='Print Voucher' class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' bc='".$r->to_bc."' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>";
                                        $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='2'>"; }
                if ($r->status == 3){   $action .= "<input type='button' value='Rejected'>"; }
                if ($r->status == 4){   $action .= "<input type='button' value='Re-Print Voucher' class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>"; }
                if ($r->status == 6){   $action .= "<input type='button' value='Request canceled'>"; }
                if ($r->status == 10){  $action .= "<input type='button' value='Fund Received'    class='' no='".$r->no."'>"; }

            }

            if ($r->fund_tr_opt_no == 3){
                
                if ($r->transfer_type == "cheque_withdraw"){ $desc = "Requesting Cheque withdraw amount of ".$r->chq_amount; }
                
                if ($r->status == 1){   $action  = "<input type='button' value='Waiting' class='' no='".$r->no."'>";
                                        $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='2'>"; }
                
                if ($r->status == 2){   $action .= "<input type='button' value='Accepted'>";
                                        $action .= "<input type='button' value='Print Voucher'      class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' bc='".$r->to_bc."' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>";
                                        $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='2'>"; }
                
                if ($r->status == 3){   $action .= "<input type='button' value='Request Rejected'>"; }

                if ($r->status == 4){   $action .= "<input type='button' value='Re-Print Voucher'   class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' bc='".$r->to_bc."' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>";
                                        $action .= "<input type='button' value='Complete Transaction' class='cheque_withdrawal_complete_transaction'  no='".$r->no."'>";
                                        $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='2'>"; }
                
                if ($r->status == 6){   $action .= "<input type='button' value='Request canceled'>"; }
                
                if ($r->status == 10){  $action .= "<input type='button' value='Fund Received' no='".$r->no."'>"; }
            
            }

            if ($r->fund_tr_opt_no == 4){
                
                if ($r->transfer_type == "cash"){ $desc = "Requesting cash amount of ".$r->amount . " from ".$this->get_bc_name($r->from_bc)  ; }
                
                if ($r->status == 1){   $action  = "<input type='button' value='Waiting'       class='' no='".$r->no."'>";
                                        $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque_EEE' no='".$r->no."' tab_no='2'>"; }
                
                if ($r->status == 2){   $action .= "<input type='button' value='Accepted'>";
                                        $action .= "<input type='button' value='Print Voucher' class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' >"; }
                
                if ($r->status == 3){   $action .= "<input type='button' value='Request Rejected'>"; }

                if ($r->status == 4){   $action .= "<input type='button' value='Re-Print Voucher' class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' >";
                                        $action .= "<input type='button' value='Complete Transaction'   class='cheque_withdrawal_complete_transaction'  no='".$r->no."'>"; }
                
                if ($r->status == 6){   $action .= "<input type='button' value='Request canceled'>"; }
                
                if ($r->status == 10){  $action .= "<input type='button' value='Fund Received'          class='' no='".$r->no."'>"; }
            
            }


            if ($r->is_bank_acc == 1){
                $bnk_acc = $r->to_acc;
            }else{
                $bnk_acc = '';
            }

            

            

            $T .="  <tr style='border-top:1px solid #999999'>
                        <td>".$r->no."</td>            
                        <td>$desc</td>            
                        <td>".$r->to_bc."</td>
                        <td>".$r->transfer_type."</td>
                        <td>".$bnk_acc."</td>
                        <td align='right' <span style='font-size:13px;font-weight:bold'>".number_format($r->amount,2)."</span></td>                                
                        <td><span style='font-size:11px;color:#666666'>".$r->requ_initi_date_time."</span></td>            
                        <td>$action</td>            
                    </tr>";

            if ($r->fund_tr_opt_no == 3){

                $T .= " <tr>

                            <td></td>
                            <td colspan='2'><b style='font-weight:600'>Bank</b> : ".$r->bank."</td>
                            <td colspan='2'><b style='font-weight:600'>Cheque No</b> : ".$r->cheque_no." </td>                            
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>


                ";

            }
            
        }
        
        $a['d'] = $T;

    }else{

        $T .= '<tr> <td colspan="9" align="center" height="40" style="background-color:#f1f1f1;color:#000000;font-size: 12px">No incomming requestes found</td> </tr>';

        $a['d'] = $T;
    }

    return $a;
}

public function my_requests_HO(){

    $bc = $this->sd['bc'];
    $a['my_requ_rc'] = "";

    $Q = $this->db->query(" SELECT *, CONCAT(B.`name`,' (',B.`bc`,')') AS `from_bc_n` , CONCAT( BB.`name` , ' (' , BB.bc , ')' ) AS `to_bc_n` FROM `t_fund_transfer_sum` S LEFT JOIN m_branches B ON S.`from_bc` = B.`bc` LEFT JOIN m_branches BB ON S.`to_bc` = BB.`bc` WHERE S.`requ_initi_bc` = '$bc' AND `status` IN (1,2,3,4,6,10) ORDER BY no DESC");

    $T = "";

    if ($Q->num_rows() > 0){

        $a['my_requ_rc'] = $Q->num_rows(); 
        
        $n = 0;

        foreach($Q->result() as $r){ 
            
            $action = "";
            $desc   = "";

            if ($r->fund_tr_opt_no == 1){
                
                if ($r->transfer_type == "cash"){           
                    $desc = "Requesting handover cash amount of ".$r->amount . " from ".$this->get_bc_name($r->from_bc); 

                    if ($r->status == 1){   $action .= "<input type='button' value='Waiting' class=''>";
                                            $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque_AAA' no='".$r->no."' tab_no='2'>"; }
                    
                    if ($r->status == 2){   $action .= "<input type='button' value='Print Voucher' class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' tab_no='2' bc='".$r->to_bc."' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>"; 
                                            $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque_BBB' no='".$r->no."' tab_no='2'>"; }
                    
                    if ($r->status == 3){   $action .= "<input type='button' value='Rejected or Canceled'>"; }
                    
                    if ($r->status == 4){   $action  .= "<input type='button' value='Re-Print Voucher' class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' tab_no='2' bc='".$r->to_bc."' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>";
                                            $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque_CCC' no='".$r->no."' tab_no='2'>"; }

                    if ($r->status == 6){   $action .= "<input type='button' value='Request canceled'>"; }

                    if ($r->status == 10){  $action .= "<input type='button' value='Fund Received'>";
                                            /*$action .= "<input type='button' value='Cancel this Transaction' class='cancel_tfr_transaction' no='".$r->no."' tab_no='2'>";*/ }

                }
                
                if ($r->transfer_type == "bank_deposit"){
                    $desc = "Requesting deposit cash amount of ".$r->amount." to bank account number ".$r->ref_no ." from <span style='font-weight:600'>".$r->from_bc_n."</span>"; 

                    if ($r->status == 1){   $action .= "<input type='button' no='".$r->no."' value='Waiting'>"; 
                                            $action .= "<input type='button' no='".$r->no."' value='Cancel Request' class='cancel_tfr_reque_DDD' tab_no='2'>"; }
                    
                    if ($r->status == 2){   $action .= "<input type='button' value='Accepted'>";
                                            $action .= "<input type='button' value='Waiting for ".$this->get_bc_name($r->from_bc)." Response'>";
                                            $action .= "<input type='button' no='".$r->no."' value='Cancel Request' class='cancel_tfr_reque_DDD' tab_no='2'>"; }
                    
                    if ($r->status == 3){   $action .= "<input type='button' value='Rejected'>"; }
                    
                    if ($r->status == 4){   $action .= "<input type='button' value='Voucher Printed from ".$this->get_bc_name($r->from_bc)."'>";
                                            $action .= "<input type='button' value='Waiting for Response'>"; }

                    if ($r->status == 6){   $action .= "<input type='button' value='Request canceled'>"; }

                    if ($r->status == 10){  $action .= "<input type='button' value='Fund Received'>"; }
                
                }
            

            }

            if ($r->fund_tr_opt_no == 2){
                if ($r->transfer_type == "cash"){ $desc = "Requesting handover cash amount of ".$r->amount . " to " . $r->to_bc_n ; }
                if ($r->transfer_type == "bank_deposit"){ $desc = $this->get_bc_name($r->to_bc)." requesting deposit cash amount of ".$r->amount." to bank account number ".$r->to_acc; }
            
                if ($r->status == 1){   $action .= "<input type='button' value='Waiting'                class='' no='".$r->no."'>"; }
                if ($r->status == 2){   $action .= "<input type='button' value='Request Accepted'        class='' no='".$r->no."'>"; }
                if ($r->status == 3){   $action .= "<input type='button' value='Rejected'>"; }

                if ($r->status == 4){   $action .= "<input type='button' value='Voucher Printed from ".$this->get_bc_name($r->to_bc)."'>";
                                        $action .= "<input type='button' value='".$this->get_bc_name($r->from_bc)." Wait for collection officer'>"; }

                if ($r->status == 10){  $action .= "<input type='button' value='Transaction Completed'  class='' no='".$r->no."'>"; }
            }

            if ($r->fund_tr_opt_no == 3){
                if ($r->transfer_type == "cheque_wdr"){ $desc = "Requesting cheque withdraw amount of ".$r->amount; }
            }            

            if ($r->is_bank_acc == 1){
                $bnk_acc = $r->ref_no;
            }else{
                $bnk_acc = '';
            }

            

            

            $T .="  <tr>
                        <td>".$r->no."</td>            
                        <td>$desc</td>            
                        <td>".$this->get_bc_name($r->to_bc)."</td>
                        <td>".$r->transfer_type."</td>
                        <td>".$bnk_acc."</td>
                        <td align='right' <span style='font-size:13px;font-weight:bold'>".number_format($r->amount,2)."</span></td>                                
                        <td><span style='font-size:11px;color:#666666'>".$r->requ_initi_date_time."</span></td>            
                        <td>$action</td>            
                    </tr>";            
            
        }
        
        $a['d'] = $T;

    }else{

        $T .= '<tr> <td colspan="9" align="center" height="40" style="background-color:#f1f1f1;color:#000000;font-size: 12px">No incomming requestes found</td> </tr>';

        $a['d'] = $T;
    }

    return $a;
}

public function incomming_requests(){

    $bc = $this->sd['bc'];
    $a['incmg_requ_rc'] = "";

    $Q = $this->db->query(" SELECT * FROM `t_fund_transfer_sum` s WHERE `from_bc` = '$bc' AND `status` IN (1,2,3,4,6,10) ORDER BY no DESC ");

    $T = "";

    if ($Q->num_rows() > 0){

        $a['incmg_requ_rc'] = $Q->num_rows(); 
        
        $n      = 0;
        $desc   = "";

        foreach($Q->result() as $r){ 

            $action = "";

            if ($r->fund_tr_opt_no == 1){
                
                if ($r->transfer_type == "cash"){ 
                    $desc = $this->get_bc_name($r->to_bc)." requesting handover cash amount of ".$r->amount; 

                    if ($r->status == 1) {  $action .= "<input type='button' value='Accept' class='ftr_accept_request' no='".$r->no."' bc='".$r->to_bc."'>"; 
                                            $action .= "<input type='button' value='Reject' class='ftr_reject_request' no='".$r->no."' bc='".$r->to_bc."'>";  }
                    
                    if ($r->status == 2) {  $action .= "<input type='button' value='Waiting for ".$this->get_bc_name($r->to_bc)." Response'>"; 
                                            $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque_BBB' no='".$r->no."' tab_no='3'>"; }
                    
                    if ($r->status == 3) {  $action .= "<input type='button' value='Rejected or Canceled'>";  }
                    
                    if ($r->status == 4) {  $action .= "<input type='button' value='Wait for collection officer from ".$this->get_bc_name($r->to_bc)."' class='' no='".$r->no."' >";
                                            $action .= "<input type='button' value='Complete Transaction' class='ftr_complete_transaction' no='".$r->no."' tab_no='3' >";
                                            $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='3'>";  }

                    if ($r->status == 6) {  $action .= "<input type='button' value='Request canceled'>";  }

                    if ($r->status == 10){  $action .= "<input type='button' value='Transaction Completed'>";  }
                
                }
                

                if ($r->transfer_type == "bank_deposit"){
                    
                    $desc = $r->to_bc." requesting deposit cash amount of ".$r->amount." to bank account number ".$r->ref_no; 

                    if ($r->status == 1) {  $action .= "<input type='button' value='Accept' class='ftr_accept_request' no='".$r->no."' bc='".$r->to_bc."' >"; 
                                            $action .= "<input type='button' value='Reject' class='ftr_reject_request' no='".$r->no."' bc='".$r->to_bc."' >";  }
                    
                    if ($r->status == 2) {  $action .= "<input type='button' value='Print Voucher'  class='ftr_print_vocuher' no='".$r->no."' is_v_re_p='".$r->is_v_print."' tab_no='3' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>";
                                            $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='3'>";  }
                    
                    if ($r->status == 3) {  $action .= "<input type='button' value='Rejected'>";  }
                    
                    if ($r->status == 4) {  $action  .= "<input type='button' value='Re-Print Voucher' class='ftr_print_vocuher'            no='".$r->no."' is_v_re_p='".$r->is_v_print."' tab_no='3' bc='".$r->to_bc."' transfer_type='".$r->transfer_type."' from_bc='".$r->from_bc."'>";
                                            $action .= "<input type='button' value='Complete Transaction' class='ftr_complete_transaction' no='".$r->no."' >"; 
                                            $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='3'>"; }

                    if ($r->status == 6) {  $action .= "<input type='button' value='Request canceled'>";  }

                    if ($r->status == 10){  $action .= "<input type='button' value='Transaction Completed'>";  }
                
                }
                
            }

            if ($r->fund_tr_opt_no == 2){
                
                if ($r->transfer_type == "cash"){           $desc = $this->get_bc_name($r->requ_initi_bc)." requesting handover cash amount of ".$r->amount . " to " . $this->get_bc_name($r->to_bc) ; }
                if ($r->transfer_type == "bank_deposit"){   $desc = $this->get_bc_name($r->to_bc)." requesting deposit cash amount of ".$r->amount." to bank account number ".$this->get_bc_name($r->to_acc); }
                
                if ($r->status == 1){                     
                    $action .= "<input type='button' value='Accept' class='ftr_accept_request' no='".$r->no."' bc='".$r->to_bc."'>"; 
                    $action .= "<input type='button' value='Reject' class='ftr_reject_request' no='".$r->no."' bc='".$r->to_bc."'>";                    
                }

                if ($r->status == 2){   $action .= "<input type='button' value='Accepted'>";
                                        $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='3'>"; }

                if ($r->status == 3){ $action .= "<input type='button' value='Rejected'>"; }
                if ($r->status == 4){ $action .= "<input type='button' value='Wait for collection officer' class='' no='".$r->no."' >"; 
                                      $action .= "<input type='button' value='Complete Transaction' class='ftr_complete_transaction' no='".$r->no."' >"; 
                                      $action .= "<input type='button' value='Cancel Request' class='cancel_tfr_reque' no='".$r->no."' tab_no='3'>"; }

                if ($r->status == 6){ $action .= "<input type='button' value='Request canceled'>"; }

                if ($r->status == 10){$action .= "<input type='button' value='Transaction Completed' no='".$r->no."' >"; }
            }            

            if ($r->is_bank_acc == 1){
                $bnk_acc = $r->ref_no;
            }else{
                $bnk_acc = '';
            }



            $T .="  <tr>
                        <td>".$r->no."</td>            
                        <td>$desc</td>            
                        <td>".$this->get_bc_name($r->to_bc)."</td>
                        <td>".$r->transfer_type."</td>
                        <td>".$bnk_acc."</td>
                        <td align='right' <span style='font-size:13px;font-weight:bold'>".number_format($r->amount,2)."</span></td>                                
                        <td><span style='font-size:11px;color:#666666'>".$r->requ_initi_date_time."</span></td>            
                        <td>$action</td>            
                    </tr>";            
            
        }
        
        $a['d'] = $T;

    }else{

        $T .= '<tr> <td colspan="9" align="center" height="40" style="background-color:#f1f1f1;color:#000000;font-size: 12px">No incomming requestes found</td> </tr>';

        $a['d'] = $T;
    }

    return $a;
}

public function incomming_requests_HO(){

    $bc = $this->sd['bc'];
    $a['incmg_requ_rc'] = "";

    $Q = $this->db->query(" SELECT * FROM `t_fund_transfer_sum` s WHERE NOT transfer_type = 'cheque_withdraw' AND (`from_bc` = '$bc' OR `from_bc` = 'HO') AND `status` IN (1,2,3,4,6)  ORDER BY `no` DESC");

    $T = "";

    if ($Q->num_rows() > 0){

        $a['incmg_requ_rc'] = $Q->num_rows(); 
        
        $n      = 0;
        $desc   = "";

        $rows   = $Q->num_rows();

        foreach($Q->result() as $r){ 
            
            $action     = "";

            if ($r->fund_tr_opt_no == 2){
                
                if ($r->transfer_type == "cash"){           $desc = $this->get_bc_name($r->requ_initi_bc)." requesting handover cash amount of ".$r->amount . " to " . $this->get_bc_name($r->to_bc) ; }
                if ($r->transfer_type == "bank_deposit"){   $desc = $this->get_bc_name($r->to_bc)." requesting deposit cash amount of ".$r->amount." to bank account number ".$this->get_bc_name($r->to_acc); }
                
                if ($r->status == 1){                     
                    $action .= "<input type='button' value='Accept' class='ftr_accept_request' no='".$r->no."' >"; 
                    $action .= "<input type='button' value='Reject' class='ftr_reject_request' no='".$r->no."' >";                    
                }

                if ($r->status == 2){ $action .= "<input type='button' value='Accepted'>"; }
                if ($r->status == 3){ $action .= "<input type='button' value='Rejected'>"; }
                if ($r->status == 4){ $action .= "<input type='button' value='Wait for collection officer' class='' no='".$r->no."' >"; 
                                      $action .= "<input type='button' value='Complete Transaction' class='ftr_complete_transaction' no='".$r->no."' >"; }
                if ($r->status == 10){$action .= "<input type='button' value='Transaction Completed' no='".$r->no."' >"; }
            }             

            if ($r->fund_tr_opt_no == 4){
                
                if ($r->transfer_type == "cash"){ $desc = $this->get_bc_name($r->requ_initi_bc)." requesting cash amount of ".$r->amount; }
                if ($r->transfer_type == "bank_deposit"){ $desc = "Description not set"; }

                if ($r->status == 1){

                    $action  .= "<b style='font-weight:600'>
                                    Forward to:
                                    ".$this->bc_list_with_cash_acc_codes("from_bc")."
                                    <input type='hidden' name='from_bc'     class='fb'>
                                    <input type='hidden' name='from_acc'    class='fa'>
                                </b>";
                    
                    $action .= "<input type='button' value='Forward' class='ftr_forward_request' no='".$r->no."' bc='".$r->to_bc."'>"; 
                    $action .= "<input type='button' value='Reject'  class='ftr_reject_request'  no='".$r->no."' bc='".$r->to_bc."'>";

                }


                if ($r->status == 2){   $action  .= "<input type='button' value='Re-Print Voucher' class='ftr_print_vocuher'            no='".$r->no."' is_v_re_p='".$r->is_v_print."' >"; 
                                        $action  .= "<input type='button' value='Complete Transaction' class='ftr_complete_transaction' no='".$r->no."' >"; 
                }

                if ($r->status == 3){   $action  .= "<input type='button' value='Rejected'>"; }

                if ($r->status == 6){   $action  .= "<input type='button' value='Request canceled'>"; }

                

            }           


            if ($r->is_bank_acc == 1){
                $bnk_acc = $r->to_acc;
            }else{
                $bnk_acc = '';
            }


            $T .="  <tr style='border-top:1px solid #999999'>
                        <td>".$rows--."</td>            
                        <td>$desc</td>            
                        <td>".$this->get_bc_name($r->to_bc)."</td>
                        <td>".$r->transfer_type."</td>
                        <td>".$bnk_acc."</td>
                        <td align='right' <span style='font-size:13px;font-weight:bold'>".number_format($r->amount,2)."</span></td>                                
                        <td><span style='font-size:11px;color:#666666'>".$r->requ_initi_date_time."</span></td>            
                        <td>$action</td>            
                    </tr>";   

            
            
        }
        
        $a['d'] = $T;

    }else{

        $T .= '<tr> <td colspan="9" align="center" height="40" style="background-color:#f1f1f1;color:#000000;font-size: 12px">No incomming requestes found</td> </tr>';

        $a['d'] = $T;
    }

    return $a;
}

public function update_incomming_request(){


    $no = $_POST['no'];

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 6 ){
        $a['s'] = 14;
        echo json_encode($a);
        exit;
    }


    $_POST['hid'] = 0;
    $_POST['ln'] = "";
    $_POST['bt'] = "";
    $_POST['bn'] = "";

    $no = $this->max_no = $_POST['no'];    
    
    $this->db->trans_begin();    

        $account_update =   $this->account_update(0);

        if($account_update!=1){
            $a['s'] = 0;
            $a['m'] = "Invalid account entries";
            echo json_encode($a);
            exit;

        }else{
            $account_update = $this->account_update(1);
            $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 10 , `bc_aprvd_by` = ".$this->sd['oc']." , `bc_aprvd_date_time`='".date('Y-m-d H:i:s')."'  WHERE `no` = '$no' LIMIT 1 ");
        }


    if ($this->db->trans_status() === FALSE){
        
        $this->db->trans_rollback();
        $a['s'] = "error";

    }else{

        $this->db->trans_commit();        
        $a['s'] = 1;

    }

    echo json_encode($a);

}



public function account_update($condition) {
    
    $R1 = $this->db->query("SELECT * FROM t_fund_transfer_sum S WHERE S.`no` = ".$this->max_no." LIMIT 1 ")->row();

    $this->db->where("trans_no", $this->max_no);
    $this->db->where("trans_code", 9);        
    $this->db->where("bc", $this->sd['bc']);    
    $this->db->delete("t_check_double_entry");

    if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
        $this->db->where('bc',$this->sd['bc']);
        $this->db->where('trans_code',9);
        $this->db->where('trans_no',$this->max_no);
        $this->db->delete('t_account_trans');
    }

    $config = array(
        "ddate" => $R1->added_date,
        "time" => date('H:i:s'),
        "trans_code" => 9,
        "trans_no" => $this->max_no,
        "op_acc" => 0,
        "reconcile" => 0,
        "cheque_no" => 0,
        "narration" => "",
        "ref_no" => ''
    );


    $this->load->model('account');
    $this->account->set_data($config);

    $acc_code = $R1->to_acc;

    
    $this->account->set_value2("Fund Receving ".$R1->from_bc."-->".$R1->to_bc, $R1->amount , "dr",       $acc_code,$condition,"",       $_POST['ln'],$_POST['bt'],$_POST['bn'],"",$R1->from_bc,$R1->to_bc);

    $cash_book = $this->utility->get_default_acc('CASH_IN_HAND');
    
    
    $this->account->set_value2("Fund Transfer ".$R1->to_bc."<--".$R1->from_bc, $R1->amount , "cr",       $cash_book,$condition,"",       $_POST['ln'],$_POST['bt'],$_POST['bn'],"",$R1->from_bc,$R1->from_bc);

    if($condition==0){

        $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` 

            WHERE  t.`cl`='C1'  

            AND (t.`bc`='" . $this->sd['bc'] . "' OR t.`bc` = '".$R1->from_bc."' OR t.`bc` = '".$R1->to_bc."'   ) 

            AND t.`trans_code`='9'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");

        if ($query->row()->ok == "0") {
            
            $this->db->where("trans_no", $this->max_no);
            $this->db->where("trans_code", 9);                            
            $this->db->where("bc", $this->sd['bc']);
            $this->db->delete("t_check_double_entry");
            
            return "0";
        } else {
            return "1";
        }
    }
}


public function send_cheque_withdraw_request(){

    // var_dump($_POST);
    // exit;

    $this->db->trans_begin();
        
        $_POST['no'] = $this->get_max_no();
        $_POST['transfer_type'] = $_POST['tt_opt'];
        $_POST['date_time'] = date('Y-m-d h:i:s');
        $_POST['status'] = 1;
        $_POST['requ_initi_bc'] = $this->sd['bc'];
        $_POST['requ_initi_by'] = $this->sd['oc'];
        $_POST['requ_initi_date_time'] = $_POST['date_time'];
        $_POST['is_bank_acc'] = $_POST['transfer_type'] != "cash" ? 1 : 0 ;
        $_POST['fund_tr_opt_no'] = 3;
        $_POST['to_bc'] = $this->sd['bc'];
        $_POST['to_acc'] = $this->utility->get_default_acc('CASH_IN_HAND');
        $_POST['added_date'] = $this->sd['current_date'];

        $chq_no = $_POST['chq_no'];
        $bank_code = $_POST['bank_code'];

        unset($_POST['tt_opt'],$_POST['bank_code'],$_POST['chq_no']);

        $this->db->insert("t_fund_transfer_sum",$_POST);

        $data['no'] = $_POST['no'];
        $data['bank'] = $bank_code;
        $data['cheque_no'] =  $chq_no;
        $data['amount'] = $_POST['amount'];
        $data['bc'] = $this->sd['bc'];
        $data['date'] = date('Y-m-d');


        $Q3 = $this->db  ->select("no","cheque_no")
                        ->where(array("bank"=>$bank_code,"cheque_no"=>$chq_no,"bc"=>$this->sd['bc']) )
                        ->get("t_fnd_trnsfr_rqsts_chq_det");

        if ( $Q3->num_rows() == 0 ){

            $this->db->insert("t_fnd_trnsfr_rqsts_chq_det",$data);            

        }else{

            $this->db   ->set(array("status"=>3,"old_trf_no"=>$Q3->row()->no))
                        ->where(array("bank"=>$bank_code,"cheque_no"=>$chq_no,"bc"=>$this->sd['bc']) )
                        ->limit(1)
                        ->update("t_fnd_trnsfr_rqsts_chq_det");

            $this->db->insert("t_fnd_trnsfr_rqsts_chq_det",$data);            
            
        }

        /*$DATA['bc']                 = "ACC";
        $DATA['action_taken_by']    = $this->sd['oc'];
        $DATA['related_model']      = "fund transfer";
        $DATA['action_window']      = "appvl_fund_transfers";
        $DATA['noti_description']   = "A Cheque withdraw request added by ".$_POST['to_bc']." amount of Rs. ".$data['amount'];*/

        $this->utility->user_activity_log($module='Fund Transfer',$action='insert',$trans_code='9',$trans_no=$data['no'],
            $note="A Cheque withdraw request added by ".$_POST['to_bc']." amount of Rs. ".$data['amount']." from head office");


    if ($this->db->trans_status() === FALSE){
        
        $this->db->trans_rollback();
        $a['s'] = "error";

    }else{

        $this->db->trans_commit();        
        $a['s'] = 1;

    }
    
    echo json_encode($a);

}


public function update_cheque_withdrawal_request(){

    $_POST['hid']  = 0;

    $_POST['ln'] = "";
    $_POST['bt'] = "";
    $_POST['bn'] = "";
    $no          = $this->max_no = $_POST['no'];    
    
    $_POST['date'] = $this->db->query("SELECT IFNULL(`added_date`,CURRENT_DATE) as `added_date` FROM `t_fund_transfer_sum` WHERE `no` = '$no' LIMIT 1 ")->row()->added_date;

    $this->db->trans_begin();    

        $account_update =   $this->account_update_cheque_withdrawal(0);

        if($account_update!=1){
            $a['s'] = 0;
            $a['m'] = "Invalid account entries";

            exit;

        }else{
            $account_update = $this->account_update_cheque_withdrawal(1);
            $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 10 WHERE `no` = '$no' LIMIT 1 ");
            $this->utility->user_activity_log($module='Fund Transfer',$action='complete',$trans_code='9',$trans_no=$no,$note='cheque withdrawal');
        }

    if ($this->db->trans_status() === FALSE){
        
        $this->db->trans_rollback();
        $a['s'] = "error";

    }else{

        $this->db->trans_commit();        
        $a['s'] = 1;

    }

    echo json_encode($a);

}


public function account_update_cheque_withdrawal($condition) {
    
    $R1 = $this->db->query("SELECT S.`from_bc`, S.`from_acc`, S.`to_bc`, S.`to_acc`, S.`amount`, CD.`bank`, CD.`cheque_no` 
                            FROM t_fund_transfer_sum S JOIN `t_fnd_trnsfr_rqsts_chq_det` CD ON S.`no` = CD.`no` 
                            WHERE S.`no` = ".$this->max_no." LIMIT 1 ")->row();

    $this->db->where("trans_no", $this->max_no);
    $this->db->where("trans_code", 9);        
    $this->db->where("bc", $this->sd['bc']);    
    $this->db->delete("t_check_double_entry");

    if ($_POST['hid'] != "0" || $_POST['hid'] != "") {            
        $this->db->where('bc',$this->sd['bc']);
        $this->db->where('trans_code',9);
        $this->db->where('trans_no',$this->max_no);
        $this->db->delete('t_account_trans');
    }

    $config = array(
        "ddate" => $_POST['date'],
        "time" => date('H:i:s'),
        "trans_code" => 9,
        "trans_no" => $this->max_no,
        "op_acc" => 0,
        "reconcile" => 0,
        "cheque_no" => 0,
        "narration" => "",
        "ref_no" => ''
    );


    $this->load->model('account');
    $this->account->set_data($config);

    // BC cash acc                         Dr
    // bank acc tha cheque issued          Cr

    $cash_book = $this->utility->get_default_acc('CASH_IN_HAND');
    $this->account->set_value2("Cheque withdraw(".$R1->cheque_no.") from ".$R1->bank, $R1->amount , "dr",       $cash_book,$condition,"",       $_POST['ln'],$_POST['bt'],$_POST['bn'],"",$R1->to_bc);

    $acc_code = $R1->bank;
    $this->account->set_value2("Cheque withdraw(".$R1->cheque_no.") from ".$R1->bank, $R1->amount , "cr",       $acc_code ,$condition,"",       $_POST['ln'],$_POST['bt'],$_POST['bn'],"",$R1->to_bc);

    if($condition==0){

        $query = $this->db->query("SELECT (IFNULL( SUM( t.`dr_amount`),0) = IFNULL(SUM(t.`cr_amount`),0)) AS ok FROM `t_check_double_entry` t LEFT JOIN `m_account` a ON t.`acc_code` = a.`code` WHERE  t.`cl`='C1'  AND (t.`bc`='" . $this->sd['bc'] . "' OR t.`bc` = '".$R1->from_bc."'   ) AND t.`trans_code`='9'  AND t.`trans_no` ='" . $this->max_no . "' AND a.`is_control_acc`='0'");

        if ($query->row()->ok == "0") {
            $this->db->where("trans_no", $this->max_no);
            $this->db->where("trans_code", 9);                
            $this->db->where("bc", $this->sd['bc']);            
            $this->db->delete("t_check_double_entry");
            return "0";
        } else {
            return "1";
        }
    }
}

public function accept_request(){

    $no = $_POST['no'];

    $q = $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum');

    if ( $q->row()->status != 6 ){
  
        $q2 = $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 2 WHERE `no` = '$no' LIMIT 1 ");

        /*$DATA['bc']                 = $_POST['bc'];
        $DATA['action_taken_by']    = $this->sd['oc'];
        $DATA['related_model']      = "fund transfer";
        $DATA['action_window']      = "t_fund_transfer#tabs-2";
        $DATA['noti_description']   = "Your cash request accepted.";*/

        $this->utility->user_activity_log($module='Fund Transfer',$action='accept',$trans_code='9',$trans_no=$no,$note='cash request accepted');

        if ($q2){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

    }else{
        
        $a['s'] = 2;

    }
    

    echo json_encode($a);

}

public function reject_request(){

    $no = $_POST['no'];

    $q = $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 3 WHERE `no` = '$no' LIMIT 1 ");                    
    
    // $DATA['bc']                 = $_POST['bc'];
    // $DATA['action_taken_by']    = $this->sd['oc'];
    // $DATA['related_model']      = "fund transfer";
    // $DATA['action_window']      = "t_fund_transfer#tabs-2";
    // $DATA['noti_description']   = "Cash request rejected";
    $this->utility->user_activity_log($module='Fund Transfer',$action='reject',$trans_code='9',$trans_no=$no,$note='Cash request rejected');

    if ($q){
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}

public function forward_request(){

    $no         = $_POST['no'];

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 6 ){
        $a['s'] = 2;
        echo json_encode($a);
        exit;
    }

    $new_no     = $this->get_max_no();
    $datetime   = date("Y-m-d H:i:s");
    $from_bc    = $_POST['from_bc'];
    $from_acc   = $_POST['from_acc'];
    $requ_initi_date_time = $datetime;    

    $Q = $this->db->query("  INSERT INTO t_fund_transfer_sum 
                        SELECT '$new_no', `ref_no`, '$datetime', '2', '$from_bc', '$from_acc', `to_bc`, `to_acc`, `transfer_type`, `amount`, `comment`, '1', 'HO', `requ_initi_bc`,'$requ_initi_date_time', `ho_aprvd_by`, `ho_aprvd_date_time`, `bc_aprvd_by`, `bc_aprvd_date_time`, '1', 'HO', `is_bank_acc`, `is_v_print`, `added_date` 
                        FROM t_fund_transfer_sum S 
                        WHERE S.`no` = $no ");

    if ($Q){
        
        $Q2 = $this->db->query("UPDATE `t_fund_transfer_sum` SET `status` = 5 WHERE `no` = $no LIMIT 1 ");
        
        $DATA['action_taken_by']    = $this->sd['oc'];
        $DATA['related_model']      = "fund transfer";

        // initiator

        $DATA['bc']                 = $_POST['bc'];
        $DATA['action_window']      = "t_fund_transfer#tabs-2";
        $DATA['noti_description']   = "Your cash request forwarded to ".$from_bc . " branch.";
        $this->utility->add_notification($DATA);

        // responder

        $DATA['bc']                 = $from_bc;        
        $DATA['action_window']      = "t_fund_transfer#tabs-3";
        $DATA['noti_description']   = "You have incoming cash transfer request from head office.";
        $this->utility->add_notification($DATA);
    }

    if ($Q2){

        if ($Q2){
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }

}


public function cancel_tfr_request(){

    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fund_transfer_sum');
    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fnd_trnsfr_rqsts_chq_det');

    if ( $Q ){
        $this->utility->user_activity_log($module='Fund Transfer',$action='cancel',$trans_code='9',$trans_no=$_POST['no'],$note='cancel by branch ');
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}


public function cancel_tfr_request_AAA(){

    $no = $_POST['no'];

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 2 ){
        $a['s'] = 11;
        echo json_encode($a);
        exit;
    }

    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fund_transfer_sum');
    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fnd_trnsfr_rqsts_chq_det');

    if ( $Q ){
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}


public function cancel_tfr_request_BBB(){

    $no = $_POST['no'];

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 4 ){
        $a['s'] = 12;
        echo json_encode($a);
        exit;
    }

    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fund_transfer_sum');
    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fnd_trnsfr_rqsts_chq_det');

    if ( $Q ){
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}


public function cancel_tfr_request_CCC(){

    $no = $_POST['no'];

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 10 ){
        $a['s'] = 13;
        echo json_encode($a);
        exit;
    }

    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fund_transfer_sum');
    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fnd_trnsfr_rqsts_chq_det');

    if ( $Q ){
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}


public function cancel_tfr_request_DDD(){

    $no = $_POST['no'];

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 2 ){
        $a['s'] = 15;
        echo json_encode($a);
        exit;
    }

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 4 ){
        $a['s'] = 16;
        echo json_encode($a);
        exit;
    }

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 10 ){
        $a['s'] = 17;
        echo json_encode($a);
        exit;
    }

    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fund_transfer_sum');
    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fnd_trnsfr_rqsts_chq_det');

    if ( $Q ){
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}


public function cancel_tfr_request_EEE(){

    $no = $_POST['no'];

    if ( $this->db->where("no",$no)->limit(1)->get('t_fund_transfer_sum')->row()->status == 2 ){
        $a['s'] = 15;
        echo json_encode($a);
        exit;
    }    

    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fund_transfer_sum');
    $Q = $this->db->where("no",$_POST['no'])->set("status",6)->limit(1)->update('t_fnd_trnsfr_rqsts_chq_det');

    if ( $Q ){
        $this->utility->user_activity_log($module='Fund Transfer',$action='cancel',$trans_code='9',$trans_no=$no,$note='cancel by branch');
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}


public function cancel_completed_transaction(){
    
    $Q = $this->db->query("UPDATE t_fund_transfer_sum SET `status` = 3 WHERE `no` = ".$_POST['no']." LIMIT 1 ");
    $Q = $this->db->query("DELETE FROM t_account_trans WHERE trans_code = 9 AND trans_no = ".$_POST['no']." LIMIT 2 ");

    if ( $Q ){
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}

public function get_fund_transfer_data(){

    $no = $_POST['no'];

    $q = $this->db->query("SELECT S.`transfer_type`,FBC.`name` AS `from_bc`,TBC.`name` AS `to_bc`,S.`amount`,S.`status`,S.`comment`

FROM t_fund_transfer_sum S
 
JOIN m_branches FBC ON S.`from_bc` = FBC.`bc`
JOIN m_branches TBC ON S.`to_bc`   = TBC.`bc`

WHERE `no` = $no LIMIT 1 ");

    if ($q->num_rows() > 0){

        $a['d'] = $q->row();
        $a['s'] = 1;

    }else{

        $a['s'] = 0;

    }

    echo json_encode($a);

}

public function cancel_fund_transfer(){

    $no = $_POST['no'];

    $q = $this->db->where("no",$no)->limit(1)->get("t_fund_transfer_sum");

    if ($q->num_rows() > 0){    

        $this->db->trans_begin();    

        $this->db->query("UPDATE t_fund_transfer_sum SET `status` = 3 WHERE NO = $no LIMIT 1");
        $this->db->query("UPDATE `t_fnd_trnsfr_rqsts_chq_det` SET `status` = 3 WHERE NO = 1 LIMIT 1 ");
        $this->db->query("DELETE FROM t_account_trans WHERE trans_code = 9 AND trans_no = $no LIMIT 2");
        $this->db->query("DELETE FROM `t_check_double_entry` WHERE trans_code = 9 AND trans_no = $no LIMIT 2");        

        if ($this->db->trans_status() === FALSE){
            
            $this->db->trans_rollback();
            $a['s'] = 0;

        }else{

            $this->db->trans_commit();        
            $a['s'] = 1;

        }

    }else{
        $a['s'] = 0;
    }


    echo json_encode($a);

}






}
