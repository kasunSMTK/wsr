<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_daily_summery_total extends CI_Model {
	
	private $sd;    
	
	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}
	
	public function base_details(){		
		$a['max_no'] = 1;        
		return $a;
	}

	public function Excel_report(){

		$DATA['from_date'] 	= $_POST['from_date'];
		$DATA['to_date'] 	= $_POST['to_date'];
		$DATA['rtype']		= "excel";

		$this->PDF_report($DATA);

	}

	public function PDF_report($_POST_){

		if (!isset($_POST_['rtype'])){
			$_POST_['rtype'] = 'pdf';
		}

		$fd = $_POST_['from_date'];
		$td = $_POST_['to_date'];
		
		if (isset($_POST['bc_n'])){
	        if ( $_POST['bc_n'] == "" ){        
	            $QBC  = "";
	            $QBC2 = "";
	        }        
	    }else{

	        if ( !is_array($_POST['bc_arry']) ){
	            
	            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
	            
	            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
	                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
	            }

	            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
	        }

	        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
	            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
	        }

	        $bc = implode(',', $bc_ar);

	        if ($bc == ""){
	            $QBC   = "";
	            $QBC2  = "";
	        }else{
	            $QBC   = " L.bc IN ($bc) AND ";
	            $QBC2  = " lt.`bc` IN ($bc) AND ";
	        }
	    }

		/* check for old backup rpt_daily_summery_total.php for php based accrued_interest calculation */

		ini_set('max_execution_time', 300);

		$Q  = $this->db->query("SELECT  B.`name` AS `bc_name`, 
	a.bc, 
	SUM(a.pawn_amount) AS `pawn_amount`, 
	SUM(a.redeem_amount) AS `redeem_amount`, 
	SUM(a.p_int) AS `p_int`,
	SUM(a.r_int) AS `r_int`,
	SUM(a.int_amount) AS `int_amount`, 
	SUM(a.g_tax) AS `g_tax`,
	0 AS `postage`,

	SUM(a.total_stock) AS `total_stock`, 
	SUM(a.total_weight) AS `total_weight`,
	a.total_packets AS `total_packets`,
	sum(a.oc) AS `oc`
	

	FROM (

 		/* Q1 */

		SELECT 	

		a.bc,		
		0 AS `pawn_amount`, 
		0 AS `redeem_amount`, 
		0 AS `p_int`,
		0 AS `r_int`,
		0 AS `int_amount`, 
		0 AS `g_tax`,

		SUM(a.goldweight) AS `total_stock`, 		
		SUM(a.pure_weight) AS `total_weight`, 
		SUM(a.total_packets) AS `total_packets`,
		0 AS `oc`

		FROM  (	SELECT 	L.bc,LI.W AS `goldweight`,LI.P AS `pure_weight`, 1 AS total_packets				
				FROM `t_loan` L 
				JOIN (SELECT li.bc,li.`billno`, SUM(li.`goldweight`) AS `W`, li.pure_weight AS `P` FROM `t_loanitems` li GROUP BY li.`bc`,li.`billno`) LI ON L.`bc` = LI.bc AND L.`billno` = LI.billno
				WHERE $QBC L.`ddate` <= '$td'

				UNION ALL

				SELECT L.bc,LI.W AS `goldweight`,LI.P AS `pure_weight`,0 AS total_packets
				FROM `t_loan_re_fo` L 
				JOIN 	  (	SELECT li.bc,li.`billno`,SUM(li.`goldweight`) AS `W`,li.pure_weight AS `P` 
				FROM `t_loanitems_re_fo` li 
				GROUP BY li.`bc`,li.`billno`) LI ON L.`bc` = LI.bc AND L.`billno` = LI.billno 

				LEFT JOIN ( SELECT lt.ddate, lt.`bc`,lt.billno FROM `t_loantranse_re_fo` lt 	
				WHERE $QBC2 lt.`ddate` <= '$td' AND lt.`transecode` = 'R'
				GROUP BY lt.`bc`,lt.`billno` ) LT ON L.`bc` = LT.bc AND LT.billno = L.billno 

				WHERE $QBC NOT L.`status` = 'C' AND L.`ddate` <= '$td'  AND ISNULL(LT.billno)

		) a GROUP BY a.bc

		/* Q1 END */

		/* Q2 */

		UNION ALL

		SELECT 
				a.bc,
				SUM(a.pawn_amount) AS `pawn_amount`,		
				SUM(a.redeem_amount) AS `redeem_amount`,		
				SUM(p_int) AS `p_int`,		
				SUM(r_int) AS `r_int`,
				(SUM(p_int) + SUM(r_int)) AS `total_interest`,
				SUM(a.g_tax) AS `g_tax`,
				
				0 AS `total_stock`, 		
				0 AS `total_weight`, 
				0 AS `total_packets`,
				0 AS `oc`

			FROM (	SELECT 	L.bc,
						L.`requiredamount` AS `pawn_amount`,
						0 AS `redeem_amount`,
						L.`fmintrest` AS `p_int`,
						0 AS `r_int`,
						0 AS `total_interest`,
						0 AS g_tax

				FROM `loan_tblz_union` L
				WHERE $QBC L.`status` IN ('P','AM','R','RN','L','PO') AND L.ddate BETWEEN '$fd' AND '$td'

		UNION ALL

			SELECT 	LT.`bc`,
				0 AS pawn_amount,
				L.`requiredamount` AS `redeem_amount`, 
				0 AS p_int,
				LT.`redeem_int` AS r_int,		
				LT.`redeem_int` AS `total_interest`,	
				L.stamp_fee AS `g_tax`			

			FROM `t_loantranse_re_fo` LT
			JOIN `t_loan_re_fo` L 	  ON LT.`loanno` = L.`loanno` AND LT.BC = L.BC
			WHERE $QBC LT.`transecode` IN ('R') AND LT.ddate BETWEEN '$fd' AND '$td'

		) a GROUP BY a.bc 




 

		/* Q2 END */

		/* Q3 */

		union all


		SELECT 	

		a.bc,		
		0 AS `pawn_amount`, 
		0 AS `redeem_amount`, 
		0 AS `p_int`,
		0 AS `r_int`,
		0 AS `int_amount`, 
		0 AS `g_tax`,

		0 AS `total_stock`, 		
		0 AS `total_weight`, 
		0 AS `total_packets`,
		/*SUM(IF (a.`int_with_amt` = 1,IF (a.`transecode` = 'P',(a.`amount` - a.`fmintrest`),0),IF (a.`transecode` = 'P',(a.`amount`),0))) AS `ocx`,*/
		SUM(a.`amount`) AS `oc`


		FROM  (
			
			SELECT L.bc, L.`requiredamount` AS `oc` ,L.int_with_amt, L.`status` AS `transecode`, L.`requiredamount` AS `amount`, L.fmintrest
			FROM `t_loan` L 
			WHERE $QBC L.`ddate` <= '$td'

			/*UNION ALL

			SELECT L.bc, L.`requiredamount` AS `oc` ,L.int_with_amt,L.`status` AS `transecode` ,  L.`requiredamount` AS `amount`, L.fmintrest
			FROM `t_loan_re_fo` L
			JOIN (SELECT * FROM `t_loantranse_re_fo` lt WHERE lt.`transecode` IN ('R') GROUP BY lt.`bc`,lt.`billno`) LT ON L.`bc` = LT.bc AND L.`billno` = LT.billno

			WHERE $QBC L.`ddate` <= '$td' AND LT.ddate > '$td'*/
		
		) a GROUP BY a.bc

	

) a

LEFT JOIN m_branches B ON a.bc = B.`bc`

GROUP BY a.bc  ");
		

		if($Q->num_rows() > 0){

			$r_data['list'] = $Q->result();			
			$r_data['fd'] = $fd;
			$r_data['td'] = $td;
			$r_data['barnch']= $this->db->select("name")->where("bc",$bc)->get("m_branches")->row()->name;
			$r_data['bc'] = $QBC;
			
			if ($_POST_['rtype'] == 'pdf' ){
				$this->load->view($_POST['by'].'_'.'pdf',$r_data);
			}else{
				$this->load->view($_POST['by'].'_'.'excel',$r_data);
			}

		}else{
        	echo "<script>location='default_pdf_error'</script>";
		}

	}
	
}