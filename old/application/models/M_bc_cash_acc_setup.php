<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_bc_cash_acc_setup extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){        
        $a['bc_list_dropdown'] = $this->bc_list_dropdown();
		return $a;
    }

    public function bc_list_dropdown(){

        $q = $this->db->query("SELECT bc,name FROM m_branches ORDER BY name");

        $d = "<select id='dp_bc_acc_item' style='padding:7px; width:100%'>";

        if ($q->num_rows() > 0){
            $d .= "<option value=''>Select</option>";
            foreach ($q->result() as $r) {
                $d .= "<option value='".$r->bc."'>".$r->name."</option>";
            }

        }else{
            $d .= "<option value=''>no branches added</option>";
        }

        $d .= "</select>";

        return $d;

    }

    public function add_acc(){

        if ( $this->db->where(array("bc"=>$_POST['bc'],"cash_acc"=>$_POST['cash_acc']) )->get('m_branch_cash_acc_setup')->num_rows() == 0){
            $this->db->insert('m_branch_cash_acc_setup',$_POST);
            $a['s'] = 1;
        }else{
            $a['s'] = 2;
        }

        echo json_encode($a);

    }


    public function load_data(){

        $q = $this->db->query("SELECT BS.`bc`, B.`name` AS `bc_name`, ACC.`code` , CONCAT(ACC.`code`, ' - ', ACC.`description`) AS `desc`
FROM m_branch_cash_acc_setup BS 
JOIN m_account ACC ON BS.`cash_acc` = ACC.`code`
JOIN m_branches B ON BS.`bc` = B.`bc` 
ORDER BY name");

        $a['det'] = $q->result();
        echo json_encode($a);

    }




    public function remove_acc(){


        $q1 = $this->db->query("SELECT acc_code FROM `t_account_trans` WHERE bc = '".$_POST['bc']."' AND acc_code = '".$_POST['cash_acc']."' LIMIT 1 ");

        if ($q1->num_rows() == 0){
            $q2=$this->db->where(array("bc"=>$_POST['bc'],"cash_acc"=>$_POST['cash_acc']) )->delete('m_branch_cash_acc_setup');

            if ($q2){
                $a['s'] = 1;
            }else{
                $a['s'] = 0;
            }

        }else{
            $a['s'] = 2;
        }

        echo json_encode($a);

    }


}