<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_over_adv_by_approval extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }

    function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,'XL');        
    }
    
    function PDF_report($a,$rt = 'PDF'){

        if ($rt == 'PDF'){
            echo "This report only available in Excel format";
            exit;
        }

        if($_POST['bc_arry'] === null){
            $BC = " ";            
        }else{
            
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            $BC = " AND L.bc IN ($bc)  ";
        }

        $fd = $_POST["from_date"];
        $td = $_POST["to_date"];

        $q = $this->db->query(" SELECT L.`requiredamount`, IFNULL(AH.requested_by, ( SELECT uuu.discription FROM u_users uuu WHERE uuu.cCode = L.oc )) AS requested_by,B.`name` AS bc, L.ddate, L.billno, SUM(L.`goldvalue` - L.`requiredamount`) AS `extra_amount` , LI.quality_changed , LI.items,LI.total_weight, LI.pure_weight,C.`cusname`, C.`address`, IF (L.by_approval = 0,CONCAT('Branch Manager - ', (SELECT discription FROM u_users WHERE bc_manager = 1 AND bc = L.`bc` AND disable_login = 0 LIMIT 1)  ) ,AH.appby) AS `approved_by`

FROM `t_loan` L

JOIN (  SELECT  SUM(li.`goldweight`) AS `total_weight` , SUM(li.`pure_weight`) AS `pure_weight`,
        bc,billno,IF((SUM(li.quality) - COUNT(li.billno) * 100) <> 0 , 1,0) AS `quality_changed` ,  
        GROUP_CONCAT(i.`itemname` ,'(',li.`pure_weight`,')',' ' , con.`des` , ' Q' , li.`quality`, '\n') AS `items`

    FROM t_loanitems li 
    JOIN r_items i ON li.`itemcode` = i.`itemcode`
    JOIN `r_condition` con ON li.`con` = con.`code`
    GROUP BY li.`bc`, li.`billno`) LI ON L.`bc` = LI.`bc` AND L.`billno` = LI.`billno`
    
    LEFT JOIN (      SELECT uu.`discription` AS `requested_by`,ah.`auto_no`,ah.`bc`, CONCAT(ah.`comment` ,' - ',u.`discription`) AS `appby`  
                FROM t_approval_history ah 
                JOIN u_users u ON ah.`approved_by` = u.`cCode`
                JOIN u_users uu ON ah.`requested_by` = uu.`cCode` ) AH ON AH.auto_no = L.`by_approval` AND AH.bc = L.`bc`
    
JOIN m_customer C ON L.`cus_serno` = C.`serno`
JOIN m_branches B ON L.`bc` = B.`bc`

WHERE L.ddate <= '$td' AND L.`goldvalue` - L.`requiredamount` < 0 $BC

GROUP BY L.billno
ORDER BY L.`bc`,L.`ddate` ");        


        if($q->num_rows()>0){
            
            $r_detail['det'] = $q->result();
            $r_detail['fd'] = $fd;
            $r_detail['td'] = $td;
            

            $this->load->view($_POST['by'].'_'.'excel',$r_detail);

        }else{
            echo "<script>alert('No data found');close();</script>";
        }        

    }   
    
}