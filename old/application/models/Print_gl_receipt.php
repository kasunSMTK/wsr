<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Print_gl_receipt extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report(){

        $bc = $this->sd['bc'];
        $nno = $_POST['rpt_nno'];

        $data1 = $this->db->query("SELECT * FROM `t_receipt_gl_sum` WHERE bc = '$bc' AND  nno = $nno ");        

        if($data1->num_rows()>0){
            
            $data2 = $this->db->query("SELECT D.*, A.`description` FROM `t_receipt_gl_det` D JOIN `m_account` A ON D.`acc_code` = A.`code` WHERE bc = '$bc' AND  nno = $nno ");
            $data3 = $this->db->query("SELECT B.`description`, C.* FROM `t_cheque_received` C JOIN `m_bank`  B ON C.`bank` = B.`code` WHERE trans_code = 49 AND trans_no = $nno ");
            
            
        	$r_detail['R']	 = $data1->row();
            $r_detail['RR']  = $data2->result();
            $r_detail['RRR'] = $this->db->query("SELECT * FROM `m_branches` WHERE bc = '$bc'")->row();
        	$r_detail['RRRR']  = $data3->result();

            $this->load->view($_POST['by'].'_'.'pdf',$r_detail);

        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}