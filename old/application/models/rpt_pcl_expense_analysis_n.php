<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_pcl_expense_analysis_n extends CI_Model {

    private $sd;    
    
    function __construct(){
        parent::__construct();		
        $this->sd = $this->session->all_userdata();		
    }

    public function base_details(){		
        $a['max_no'] = 1;        
        return $a;
    }   

    public function PDF_report($_POST_,$type='pdf'){


        ini_set('max_execution_time', 600 );


        // var_dump($_POST_['bc_arry']);

        $st  = '';

        if($_POST['bc_arry'] === null){
            $BC = " ";            
        }else{
            
            for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
            }

            $bc = implode(',', $bc_ar);    
            
            $BC = " bc IN ($bc) AND  ";
            $BC_MONTHLY = "bc IN ($bc) AND  ";
        }

        

        $xbc = array();
        $nn = 0;
        foreach ( $_POST['bc_arry'] as $b ){
            $xbc[$nn] = $b;
            $nn++;
        }


      
        $fd = $_POST['from_date'];
        $td = $_POST['to_date'];        
        $data_range = $_POST['h_range'];  

            
        
        if ($data_range == "months") {

            /*$Q = $this->db->query("SELECT a.bc , DATE_FORMAT(ddate,'%Y-%m') AS `m` ,  SUM(IFNULL(a.bal,0)) AS pawn_int FROM (

                                        SELECT A.bc , ddate ,  C.bal
                                        FROM m_branches A 
                                        CROSS JOIN m_account_type B 

                                        LEFT JOIN (     SELECT v_bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal` 
                                                FROM t_account_trans 
                                                WHERE ddate BETWEEN '$fd' AND '$td'
                                                GROUP BY v_bc,acc_code,DATE_FORMAT(ddate,'%Y-%m') 

                                              ) C ON A.`bc` = C.v_bc AND B.code = C.acc_code 

                                        WHERE A.$BC B.`rtype` = 2 AND NOT ISNULL(MONTH(C.ddate)) 

                                    UNION ALL
                                        
                                        SELECT bc , ddate ,  IFNULL(SUM(dr_amount) - SUM(cr_amount),2) AS pawn_int
                                        FROM t_account_trans 
                                        WHERE $BC acc_code = '20305' AND ddate BETWEEN '$fd' AND '$td'
                                        GROUP BY bc,DATE_FORMAT(ddate,'%Y-%m')

                                    ) a

                                    GROUP BY a.bc,DATE_FORMAT(a.ddate,'%Y-%m')
                                    ORDER BY a.bc,MONTH(a.ddate)");*/


            $Q = $this->db->query("

                SELECT a.bc , ddate AS `m` ,  SUM(IFNULL(a.bal,0)) AS pawn_int FROM (

                    SELECT AC.`v_bc` as bc , DATE_FORMAT(AC.`ddate`,'%Y-%m') AS `ddate` , SUM(AC.`dr_amount` - AC.`cr_amount`) AS `bal`
                    FROM `t_account_trans` AC 
                    JOIN m_account_type B ON AC.`acc_code` = B.`code` 
                    WHERE AC.v_$BC_MONTHLY AC.ddate BETWEEN '$fd' AND '$td' AND B.`rtype` = 2 AND AC.v_bc NOT IN ( SELECT bc FROM m_branches WHERE is_cost_center = 1) 
                    GROUP BY AC.`v_bc`, DATE_FORMAT(AC.`ddate`,'%Y-%m')
                    
                UNION ALL 

                    SELECT bc , DATE_FORMAT(`ddate`,'%Y-%m') AS `ddate` , SUM(dr_amount - cr_amount) AS bal
                    FROM t_account_trans 
                    WHERE acc_code = '20305' AND ddate BETWEEN '$fd' AND '$td'
                    GROUP BY bc,DATE_FORMAT(`ddate`,'%Y-%m'),acc_code
                    
                UNION ALL

                    SELECT ACC.`v_bc` AS `bc`,DATE_FORMAT(ACC.ddate,'%Y-%m') AS m,SUM(ACC.`dr_amount` - ACC.`cr_amount`) AS `bal` 
                    FROM t_account_trans ACC 
                    JOIN m_voucher_class VC ON ACC.`v_class` = VC.`code` 
                    JOIN m_account_type ON ACC.`acc_code` = m_account_type.`code` 
                    WHERE v_bc IN ( SELECT bc FROM m_branches WHERE is_cost_center = 1) 
                    AND is_exp = 1 AND m_account_type.`rtype` = 2 AND ddate BETWEEN '$fd' AND '$td' 
                    GROUP BY v_bc ,  DATE_FORMAT(ACC.ddate,'%Y-%m')


                ) a GROUP BY a.bc, m ");

        
        }

        













































































        if ($data_range == "years") {           

            $Q = $this->db->query(" SELECT A.bc , DATE_FORMAT(ddate,'%Y') AS `m` ,  SUM(IFNULL(C.bal,0)) AS pawn_int
                                    FROM m_branches A 
                                    CROSS JOIN m_account_type B 
                                    LEFT JOIN (     SELECT bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal` 
                                    FROM t_account_trans 
                                    WHERE ddate BETWEEN '$fd' AND '$td' 
                                    GROUP BY bc,acc_code,DATE_FORMAT(ddate,'%Y') ) C ON A.`bc` = C.bc AND B.code = C.acc_code 

                                    WHERE A.$BC B.`rtype` = 2 AND NOT ISNULL(MONTH(C.ddate)) 
                                    GROUP BY bc,DATE_FORMAT(ddate,'%Y')
                                    ORDER BY bc,DATE_FORMAT(ddate,'%Y') ");
        }

        if($data_range == "quarter"){            

            $Q =$this->db->query("  SELECT A.bc,YEAR(ddate) AS `y`, QUARTER(ddate) AS `q`, SUM(bal) AS `pawn_int`
                                    FROM m_branches A 
                                    CROSS JOIN m_account_type B 
                                    LEFT JOIN (     SELECT bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal` 
                                    FROM t_account_trans 
                                    WHERE ddate BETWEEN '$fd' AND '$td' 
                                    GROUP BY bc,acc_code,YEAR(ddate), QUARTER(ddate) ) C ON A.`bc` = C.bc AND B.code = C.acc_code 

                                    WHERE A.$BC B.`rtype` = 2 AND NOT ISNULL(MONTH(C.ddate)) 
                                    GROUP BY bc,YEAR(ddate), QUARTER(ddate)
                                    ORDER BY bc,YEAR(ddate), QUARTER(ddate) ");        

        }


        if($data_range == "half_year"){
            /*$Q =$this->db->query("SELECT bc, YEAR(ddate) AS y,FLOOR( ( MONTH(ddate) - 1 ) / 6 ) AS q, SUM(cr_amount) - SUM(dr_amount) AS `pawn_int` FROM t_account_trans WHERE $BC acc_code = '10101' AND ddate BETWEEN '$fd' AND '$td' GROUP BY bc,YEAR(ddate), FLOOR( ( MONTH(ddate) - 1 ) / 6 ) ORDER BY y, q ");*/

            $Q = $this->db->query(" SELECT A.bc, YEAR(ddate) AS Y,FLOOR( ( MONTH(ddate) - 1 ) / 6 ) AS q, SUM(bal) AS `pawn_int`
                                    FROM m_branches A 
                                    CROSS JOIN m_account_type B 
                                    LEFT JOIN (     SELECT bc,ddate,acc_code, SUM(dr_amount) - SUM(cr_amount) AS `bal` 
                                                    FROM t_account_trans 
                                                    WHERE ddate BETWEEN '$fd' AND '$td' 
                                                    GROUP BY bc,acc_code    ) C ON A.`bc` = C.bc AND B.code = C.acc_code 

                                    WHERE A.$BC B.`rtype` = 2 AND NOT ISNULL(MONTH(C.ddate)) 

                                    GROUP BY bc,YEAR(ddate), FLOOR( ( MONTH(ddate) - 1 ) / 6 ) 
                                    ORDER BY Y, q  ");
        
        }

            

        if ($Q->num_rows() > 0){

            $r_data['list']     = $Q->result();
            $r_data['fd']       = $fd;
            $r_data['td']       = $td;
            $r_data['d_range']  = $data_range;
            $r_data['bcc']  = $this->db->select(array('bc','name'))->get('m_branches')->result();
            $r_data['selct_bc'] = $xbc;

            
            if ($type == 'Excel'){
                $this->load->view($_POST['by'].'_'.'excel',$r_data);       
            }else{
                echo 'PDF version not available for this report';
            }


        }else{
            echo "<script>alert('No data');</script>";
        }

      

    }


    public function Excel_report(){
        $_POST['bc_arry'] = explode(",", $_POST['bc_arry']);
        $this->PDF_report($_POST,"Excel");
    }

}