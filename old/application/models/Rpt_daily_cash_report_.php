<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_daily_cash_report extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['max_no'] = 1;        
		return $a;
    }   

    public function PDF_report($_POST_){


        if ( $this->sd['isAdmin'] == 1 ){
            $branch = $_POST_['bc']; 

            if (isset($_POST['bc_n'])){
                if ( $_POST['bc_n'] == "" ){        
                    $bcC = "";
                }        
            }else{

                for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
                    $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
                }

                $bcC = implode(',', $bc_ar);

                if ($bcC == ""){
                    $bcC   = "";
                    $bc_for_op_bal_query = "";
                }else{
                    $bc_for_op_bal_query = " t_account_trans.bc IN ($bcC) AND ";
                    $bcC   = "   ta.bc  IN ($bcC) AND ";
                }
            } 


        }else{
            $branch = $this->sd['bc'];            
            $bc_for_op_bal_query = " t_account_trans.bc = '$branch' AND "; 
        }

        // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND ta.`billtype`='".$_POST['r_billtype']."'";
    }

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
    
    $cluster=$_POST['cluster'];
    //$branch=$_POST['branch'];

    $cl=$this->sd['cl'];
    $bc=$branch;

    $this->db->select(array('name'));
    $r_detail['company'] = $this->db->get('m_company')->result();

    $this->db->select(array('name', 'address', 'telno', 'faxno', 'email'));
    $this->db->where("cl", $this->sd['cl']);
    $this->db->where("bc", $this->sd['branch']);
    $r_detail['branch'] = $this->db->get('m_branches')->result();

    $bc_acc_code = $_POST['bc_acc_det_list'];

    $this->db->where('code','$bc_acc_code');

    $query=$this->db->get('m_account');
    foreach($query->result() as $row){
        $r_detail['account_det']=$row->description;
    }

    $sql="SELECT t_account_trans.bc, SUM(dr_amount-cr_amount) AS op, SUM(dr_amount) AS dr, SUM(cr_amount) AS cr , B.`name` AS `bc_name`
FROM t_account_trans 

JOIN m_branches B ON t_account_trans.`bc` = B.`bc` 
    WHERE $bc_for_op_bal_query acc_code='$bc_acc_code' AND ddate<'$fd'
    GROUP BY t_account_trans.bc ";
    
    //$r_detail['op']=$this->db->query($sql)->first_row()->op;
    $r_detail['op_bal']=$this->db->query($sql)->result();

    if ($this->sd['isAdmin'] == 1){
        $qqq = ' concat(ta.action_date , " - ", ta.description ) as `description` ';
    }else{
        $qqq = ' ta.description ';
    }


    $sqll="SELECT ta.*,td.`description` AS det,$qqq, td.`code` as t_code, 
    mb.`name` AS branch_name 
    FROM t_account_trans ta 
    LEFT JOIN t_trans_code td ON td.`code`= ta.`trans_code` 
    LEFT JOIN m_branches mb ON mb.`bc`=ta.`bc`
    WHERE $bcC  ta.ddate!='' $billtype
    AND ta.ddate BETWEEN '$fd' AND '$td'";

    if(!empty($cluster)){
        $sqll.=" AND  ta.`cl` = '$cluster'";
    }
    
    if(!empty($branch)){
        $sqll.=" AND ta.`bc` = '$branch'";
    }  
    
    $sqll.=" AND ta.acc_code='$bc_acc_code'";
    
    
    $sqll.=" ORDER BY ta.bc, ta.ddate, ta.auto_no ";

    $r_detail['all_acc_det']=$this->db->query($sqll)->result();

    $cl=$this->sd['cl'];
    $bc=$this->sd['branch'];
    $r_detail['acc_code']=$_POST['acc_code'];
    $r_detail['page']='A4';
    $r_detail['orientation']='P';  
    $r_detail['dfrom']=$fd;
    $r_detail['dto']=$td;

    



    if($this->db->query($sqll)->num_rows()>0){
     $this->load->view('r_account_report_pdf',$r_detail);
 }else{
    echo "<script>alert('No Data');window.close()</script>";
}

    }
    
}