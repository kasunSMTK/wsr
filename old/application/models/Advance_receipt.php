<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class advance_receipt extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report($tr_no_and_cus_id){
    
        $tr_no_and_cus_id = explode("~~~",$tr_no_and_cus_id);
        $tr_no = $tr_no_and_cus_id[0];
        $cus_id = $tr_no_and_cus_id[1];       
        $bc = $this->sd['bc'];

        $data1 = $this->db->query("SELECT C.`cusname`,C.`address`,C.`nicno`,C.`mobile`, AC.trans_no as `arn`, ' - ' AS `cus_serno`, AC.bc AS `bc`, AC.date AS `ddate`, ' - ' AS `action_date`, AC.`dr_amount` AS `requiredamount`, ' - ' AS `fmintrate`, ' - ' AS `finaldate`, ' - ' AS `period` FROM `t_loan_advance_customer` AC JOIN `m_customer` C ON AC.`client_id` = C.`customer_id` WHERE AC.`trans_no` = '$tr_no' AND AC.`client_id` = '$cus_id'LIMIT 1");
        
        if($data1->num_rows()>0){            
        	$r_detail['R']	=	$data1->row();            
        	$this->load->view($_POST['by'].'_'.'pdf',$r_detail);
        }else{
        	echo "<script>alert('No data found');close();</script>";
        }		 

    }   
    
}