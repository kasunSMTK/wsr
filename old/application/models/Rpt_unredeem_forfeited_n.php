<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_unredeem_forfeited_n extends CI_Model {

  private $sd;    

  function __construct(){
    parent::__construct();        
    $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
  }   

  public function PDF_report($_POST_){

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = "   WHERE  a.bc IN ($bc)  ";
        }
    }




    

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];


    if(!empty($_POST['r_billtype'])){
      $bill = "AND t_loan.billtype='".$_POST['r_billtype']."'";
    }else{
      $bill ="";
    }

    $Q = $this->db->query("
SELECT B.`name` as bc_name ,  a.bc , SUM(a.no_of_bills_30) as `no_of_bills_30` , SUM(a.no_of_bills_60) as `no_of_bills_60` , SUM(a.no_of_bills_90) as `no_of_bills_90` FROM (

SELECT L.`bc`, 1 AS `no_of_bills_30` , 0 AS `no_of_bills_60` , 0 AS no_of_bills_90
FROM t_loan L
WHERE L.`finaldate` BETWEEN CURRENT_DATE AND ADDDATE(CURRENT_DATE,INTERVAL 30 DAY)

UNION ALL

SELECT L.`bc`, 0 AS `no_of_bills_30` , 1 AS `no_of_bills_60` , 0 AS no_of_bills_90
FROM t_loan L
WHERE L.`finaldate` BETWEEN ADDDATE(CURRENT_DATE, INTERVAL 31 DAY) AND ADDDATE(CURRENT_DATE,INTERVAL 30 DAY)

UNION ALL

SELECT L.`bc`, 0 AS `no_of_bills_30` , 0 AS `no_of_bills_60` , 1 AS no_of_bills_90
FROM t_loan L
WHERE L.`finaldate` BETWEEN ADDDATE(CURRENT_DATE, INTERVAL 61 DAY) AND ADDDATE(CURRENT_DATE,INTERVAL 30 DAY)

) a 

JOIN m_branches B ON a.bc = B.bc

$bc

GROUP BY a.bc

ORDER BY a.bc");

    
    if($Q->num_rows() > 0){

      $r_data['list'] = $Q->result();
      $r_data['fd'] = $fd;
      $r_data['td'] = $td;

      $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
      echo "<script>location='default_pdf_error'</script>";
    }

  }

}