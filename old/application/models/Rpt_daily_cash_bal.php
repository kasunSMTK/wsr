<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_daily_cash_bal extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();        
      $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_,$r_type = "pdf"){


    if ($r_type == "pdf"){
        echo "This report can be in excel format only";
        exit;
    }

    ini_set('max_execution_time', 600 );
    ini_set('memory_limit', '384M');

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        if ( !is_array($_POST['bc_arry']) ){
            
            $_POST['bc_arry'] = explode(",",$_POST['bc_arry']);
            
            for ( $n = 0 ; $n < count($_POST['bc_arry']) ; $n++) {                
                $_POST['bc_arry_tmp'][$n] = $_POST['bc_arry'][$n].",";
            }

            $_POST['bc_arry'] = $_POST['bc_arry_tmp'];
        }

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = "   L.bc IN ($bc) AND ";
        }
    }


    // r_billtype filteration 
    $billtype = "";
    if(isset($_POST['r_billtype']) && $_POST['r_billtype'] != ""){
        $billtype = " AND L.`billtype`='".$_POST['r_billtype']."'";
    }


    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    $ftime = $_POST['ftime'];
    $ttime = $_POST['ttime'];

    $Qtime = "";

    if ( $this->sd['isAdmin'] == 1 ){
        if (!$ftime == ""){
            $Qtime = " AND TIME(L.`action_date`) BETWEEN '$ftime' AND '$ttime'";
        }
    }

    $Q = $this->db->query("
SELECT b.name AS bc, SUM(op_bal) AS `op_bal` , SUM(dr_tot) AS `dr_tot` , SUM(cr_tot) AS `cr_tot` FROM (

SELECT bc, SUM(dr_amount) - SUM(cr_amount)  AS `op_bal` , 0 AS `dr_tot` , 0 AS `cr_tot`
FROM t_account_trans L
WHERE $bc ddate < '$td' AND acc_code = '303003' 
GROUP BY bc

UNION ALL

SELECT bc, 0 AS `op_bal` , SUM(dr_amount) AS `dr_tot` , SUM(cr_amount) AS `cr_tot`
FROM t_account_trans L
WHERE $bc ddate = '$td' AND acc_code = '303003'
GROUP BY bc

) a JOIN m_branches b ON a.bc = b.bc  GROUP BY bc");

    if($Q->num_rows() > 0){

        $r_data['list'] = $Q->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;
        $r_data['isAdmin'] = $this->sd['isAdmin'];

        if ($r_type == "pdf"){            
            //$this->load->view($_POST['by'].'_'.'pdf',$r_data);
            echo "This report can be in excel format only";
        }else{
            $this->load->view($_POST['by'].'_'.'excel',$r_data);
        }

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

    ini_set('memory_limit', '128M');

}





public function Excel_report()
    {
        $this->PDF_report($_POST,"Excel");
    }


}