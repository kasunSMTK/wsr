<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_setup_pawning_value extends CI_Model {
    
    private $sd;    
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    }
    
    public function base_details(){		
        $a['D'] = $this->get_data();
        return $a;
    }
    
    public function save(){
        $_POST['oc'] = $this->sd['oc'];

        if ($_POST['hid'] == 0){
            unset($_POST['hid']);
        	$Q = $this->db->insert("m_pawning_values",$_POST);
        }else{
            $hid = $_POST['hid'];
            unset($_POST['hid'],$_POST['oc']);
            $Q = $this->db->update("m_pawning_values",$_POST);
        }

        if ($Q){
            echo 1;
        }else{
            echo 0;
        }
    }    
    
    public function get_data(){	    
        return $this->db->query("SELECT * FROM m_pawning_values")->row();  

    }

    public function load(){                

    }   
    

}