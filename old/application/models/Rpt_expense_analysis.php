<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_expense_analysis extends CI_Model {

    private $sd;    
    
    function __construct(){
      parent::__construct();		
      $this->sd = $this->session->all_userdata();		
  }

  public function base_details(){		
    $a['max_no'] = 1;        
    return $a;
}   

public function PDF_report($_POST_,$r_type="pdf"){

    if ($r_type == "pdf"){
        echo "No pdf version for this report, Please use excel version";
        exit;
    }

    $bc = $_POST_['h_bc'];
    
    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];
   // $data_range=$_POST_['h_range'];

   //=============================branches======================================================


    $sql_bc="SELECT DISTINCT bc, name FROM m_branches ";

    $r_data['branch']=$this->db->query($sql_bc)->result();


    $sql="SELECT 
              t.code,
              t.heading,
              g.v_bc AS bc,
              SUM(IFNULL(g.amount,0)) AS expense 
            FROM
              m_account_type t 
              LEFT JOIN m_account a 
                ON a.code = t.code 
            LEFT JOIN  t_voucher_gl_det g ON g.acc_code= a.code
            LEFT JOIN  t_voucher_gl_sum gs ON g.nno= gs.nno
            WHERE t.rtype = '2' AND NOT t.code='2' AND gs.is_approved='1'
            AND ddate BETWEEN '$fd ' AND '$td'";
         
           
    if($bc!=""){

        $sql.="  AND g.v_bc='".$bc."'";
    }


   
    $sql.="GROUP BY g.v_bc 
           ORDER BY g.v_bc";

    
    $query = $this->db->query($sql);


 //=============================end - months======================================================
 
    if($this->db->query($sql)->num_rows()>0){

       $r_data['list'] = $query->result();
        $r_data['fd'] = $fd;
        $r_data['td'] = $td;

        $r_data['d_range'] = $data_range;        
        
        $this->load->view($_POST['by'].'_'.'excel',$r_data);    

    }else{
        echo "<script>location='default_pdf_error'</script>";
    }

}

public function Excel_report()
    {
        $this->PDF_report($_POST,"Excel");
    }

}