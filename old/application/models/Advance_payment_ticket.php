<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class advance_payment_ticket extends CI_Model {
    
    private $sd;
    private $mtb;    
    
    function __construct(){
	   parent::__construct();	
	   $this->sd = $this->session->all_userdata();		   
    }

    public function base_details(){
	   $a['report'] = '';	
	   return $a;
    }
    
    function PDF_report($tr_no_and_loanno){


        /*echo var_dump($_POST);
        exit;*/
    
        $tr_no_and_loanno = explode("~~~",$tr_no_and_loanno);
        $tr_no = $tr_no_and_loanno[0];
        $loanno = $tr_no_and_loanno[1];
        $tbl = $tr_no_and_loanno[2];
        $bc = $this->sd['bc'];

        if ($tbl == "from_master_table"){
            $tbl_loan       = "t_loan";
            $tbl_loantranse = "t_loantranse";
        }else{
            $tbl_loan       = "t_loan_re_fo";
            $tbl_loantranse = "t_loantranse_re_fo";
        }
          

        $data1 = $this->db->query("SELECT C.`cusname`,C.`address`,C.`nicno`,C.`mobile`,L.`cus_serno`,L.`bc`,L.`ddate`,L.`billtype`,L.`billno`,L.`loanno`,L.`action_date`,L.`requiredamount`,L.`fmintrate`,L.`finaldate`,L.`period` FROM `$tbl_loan` L JOIN `m_customer` C ON L.cus_serno = C.serno WHERE loanno = '$loanno' Limit 1");


        $data2 = $this->db->query("SELECT LT.`trans_no`, LT.`date` as `ddate`, LT.`cr_amount` as `amount`, LT.`action_date`,
            (SELECT SUM(cr_amount) FROM t_loan_advance_customer WHERE is_delete = 0 AND billno=LT.`billno`) AS amount_tot
            FROM `t_loan_advance_customer` LT WHERE LT.`trans_no` = '$tr_no'   AND LT.bc = '$bc'   AND LT.`trans_type` = 'ADV_P' LIMIT 1");   


        if($data1->num_rows()>0){                    
            $r_detail['R']	=	$data1->row();
            $r_detail['RR'] =   $data2->row();
            $r_detail['bc'] =   $this->sd['bc'];
            $r_detail['user_des'] =   $this->sd['user_des'];
        	$this->load->view($_POST['by'].'_'.'pdf',$r_detail);                    
        }else{
            echo "<script>close();</script>";
        }        		 

    }   
    
}