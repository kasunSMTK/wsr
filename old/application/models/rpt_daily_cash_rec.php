<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rpt_daily_cash_rec extends CI_Model {

  private $sd;    

  function __construct(){
    parent::__construct();        
    $this->sd = $this->session->all_userdata();       
  }

  public function base_details(){       
    $a['max_no'] = 1;        
    return $a;
  }   

  public function PDF_report($_POST_){

    if (isset($_POST['bc_n'])){
        if ( $_POST['bc_n'] == "" ){        
            $bc = "";
        }        
    }else{

        for ($n = 0 ; $n < count($_POST['bc_arry']) ; $n++){            
            $bc_ar[] = "'".str_replace(",","",$_POST['bc_arry'][$n])."'";
        }

        $bc = implode(',', $bc_ar);

        if ($bc == ""){
            $bc   = "";
        }else{
            $bc   = " AND  D.bc IN ($bc)  ";
        }
    }

    $fd = $_POST_['from_date'];
    $td = $_POST_['to_date'];

    $Q = $this->db->query("SELECT D.ddate, D.`bc`, B.`name` AS `name`, D.`system_cash` as `cash_bal` , D.`maunal_cash` as `physical_bal`, D.`difference` 
FROM t_day_cash_bal D 

JOIN m_branches B ON D.`bc` = B.`bc`

WHERE D.`ddate` BETWEEN '$fd' AND '$td' $bc ");

    
    if($Q->num_rows() > 0){

      $r_data['list'] = $Q->result();
      $r_data['fd'] = $fd;
      $r_data['td'] = $td;

      $this->load->view($_POST['by'].'_'.'pdf',$r_data);
    }else{
      echo "<script>location='default_pdf_error'</script>";
    }

  }

}