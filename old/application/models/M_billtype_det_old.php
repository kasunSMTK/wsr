<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_billtype_det extends CI_Model {
    
    private $sd;
    private $mtb;
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();
		$this->mtb = 'tblusers';        
    }
    
    public function base_details(){			
        $a['maxno'] = $this->getMaxno();
        $a['load_list'] = $this->load_list();
        $this->load->model("m_branches");
        $a['branches'] = $this->m_branches->select();
        $a['branches_list_chk'] = $this->branches_list();
		return $a;
    }

    public function branches_list(){
        $Q = $this->db->query("SELECT bc,`name` FROM `m_branches` WHERE bc_type = 1 ORDER BY `name`"); 
        if ($Q->num_rows() > 0){
            $t = ""; foreach ($Q->result() as $R) {
                $t .= '<div class="lbl_bcs" style="border:0px solid red"><label><input type="checkbox" class="chkbc_" name="chk_bc[]" value="'.$R->bc.'"> '.strtoupper($R->bc).' - '.strtoupper($R->name).' </label></div>';
                 } }else{$t = 'No branches found'; }
        return $t;
    }
    
    public function save(){

        if (isset($_POST['chk_bc'])){
            
            $_POST_SUM['billtype'] = $_POST['billtype']; $_POST_SUM['amt_from'] = $_POST['amt_from']; $_POST_SUM['amt_to'] = $_POST['amt_to']; $_POST_SUM['period'] = $_POST['period']; $_POST_SUM['is_active'] = 1; $_POST_SUM['oc'] = $this->sd['oc']; $_POST_SUM['percentage'] = $_POST['percentage']; $_POST_SUM['percentage_to'] = $_POST['percentage_to']; $_POST_SUM['gratr_lesstn'] = $_POST['gratr_lesstn']; $_POST_SUM['billtypeno'] = $_POST['billtypeno']; if (isset($_POST['weekly_cal'])){$_POST_SUM['weekly_cal'] = 1;}else{$_POST_SUM['weekly_cal'] = 0;}

            $bc_array = array();

            for ($num = 0 ; $num < count( $_POST['chk_bc'] ) ; $num++){
                $bc_array[$num] = $_POST['chk_bc'][$num];
            }

            $bc_in_list = '"' . implode('", "', $_POST['chk_bc']) . '"';
            isset($_POST['is_old_bill']) ? $_POST_SUM['is_old_bill'] = 1 : $_POST_SUM['is_old_bill'] = 0;

            if ( $_POST['hid'] == "0" ){

                if ($this->check_for_billtype_exist($_POST_SUM['billtype'])){
                    $a['s'] = 0;
                    $a['m'] = "Bill type name already exist";                    
                    $a['maxno'] = $this->getMaxno();                    
                    echo json_encode($a);
                    exit;
                }

                if ($_POST_SUM['is_old_bill'] != 1){
                    if ($this->check_amount_range_exist(    $_POST['amt_from'], $_POST['amt_to'],$bc_in_list,$_POST['gratr_lesstn'], $_POST_SUM['billtype'], $_POST_SUM['is_old_bill'] )){
                        $a['s'] = 0;
                        $a['m'] = "Invalid <u>Amount From</u> or <u>Amount To</u> value(s). Some bill type already in this amount range";                    
                        $a['maxno'] = $this->getMaxno();                    
                        echo json_encode($a);
                        exit;
                    }
                }

            }else{                

                if ($_POST_SUM['is_old_bill'] == 0){
                    if ($this->check_amount_range_exist($_POST['amt_from'],$_POST['amt_to'],$bc_in_list,$_POST['gratr_lesstn'],$_POST['billtype'],$_POST_SUM['is_old_bill'])){
                        $a['s'] = 0;
                        $a['m'] = "Invalid <u>Amount From</u> or <u>Amount To</u> value(s). Some bill type already in this amount range";                    
                        $a['maxno'] = $this->getMaxno();                    
                        echo json_encode($a);
                        exit;   
                    }
                }

                

                // this ection added for update log table (2017-09-11)

                $log_no                 = $this->get_bt_log_no();
                $log_updated_datetime   = date('Y-m-d H:i:s');
                $updated_by             = $this->sd['oc'];

                // Select det table for current saved bill type
                $Q_CURRENT_DET = $this->db->query("INSERT INTO r_bill_type_det_log SELECT '$log_no',`no`,`day_from`,`day_to`,`rate`,`is_daily_cal` FROM `r_bill_type_det` WHERE NO IN (SELECT `no` FROM `r_bill_type_sum` WHERE billtype= '".$_POST['billtype']."' )");

                // Select sum table for current saved bill type
                $Q_CURRENT_SUM = $this->db->query("INSERT INTO r_bill_type_sum_log SELECT '$log_no','$log_updated_datetime', '$updated_by', 1 , `no`, `billtype`, `amt_from`, `amt_to`, `period`, `bc`, `oc`, `is_active`, `allow_pre_int`, `action_date`, `percentage`, `gratr_lesstn`, `weekly_cal`, `percentage_to`, `billtypeno`, `is_old_bill`, `is_R_bill`, `r1`, `r2`, `r3` FROM `r_bill_type_sum`  WHERE billtype = '".$_POST['billtype']."' ");


                $this->db->query("UPDATE `r_bill_type_sum_log` SET bt_log_max = 0 WHERE billtype = '".$_POST['billtype']."' AND log_no != '$log_no' ");


                // End this ection added for update log table (2017-09-11)




                // Delete det table by bill type
                $this->db->query("DELETE FROM `r_bill_type_det` WHERE NO IN (SELECT `no` FROM `r_bill_type_sum` WHERE billtype= '".$_POST['billtype']."' )");

                // Delete Sum table by bill type                            
                $this->db->query("DELETE FROM `r_bill_type_sum`  WHERE billtype = '".$_POST['billtype']."' ");
            }


            $q1 = $this->db->query("SELECT bc FROM `m_branches` WHERE bc_type = '1' ORDER BY bc");
            
            if ($q1->num_rows() > 0){
                
                foreach ($q1->result() as $r) { // ------ All branch loop

                    if (in_array($r->bc,$bc_array)){

                        if (!$this->check_for_bc_exist($r->bc,$_POST['billtype'])){                            

                            $no = $this->getMaxno();
            
                            $_POST_SUM['no'] = $no;
                            $_POST_SUM['bc'] = $r->bc;
                            
                            $_POST_SUM['r1'] = $_POST['r1'];
                            $_POST_SUM['r2'] = $_POST['r2'];
                            $_POST_SUM['r3'] = $_POST['r3'];
                            
                            for ($noo = 0 ; $noo < count( $_POST['day_from'] ) ; $noo++){if (isset($_POST['rad_is_dailt'])){ if ( $_POST['rad_is_dailt'] == $noo ){ $is_daily_cal = 1; }else{ $is_daily_cal = 0; } }else{ $is_daily_cal = 0; } if (!$_POST['day_from'][$noo] == "" ){$_POST_DET[$noo] = array("no"=>$no,"day_from"=>$_POST['day_from'][$noo],"day_to"=>$_POST['day_to'][$noo],"rate"=>$_POST['rate'][$noo],"is_daily_cal"=>$is_daily_cal); } }
                            
                            unset($_POST['hid']);

                            if($this->db->insert("r_bill_type_sum",$_POST_SUM)){                
                                if (isset($_POST_DET)){ $this->db->insert_batch("r_bill_type_det",$_POST_DET); }
                                $a['s'] = 1;
                                $a['data'] = $_POST_SUM;
                            }else {
                                $a['s'] = 0; 
                                $a['n'] = $this->db->conn_id->errno; 
                                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Bill Type");
                            }

                        }

                    }

                }

            }

        }else{
            $a['s'] = 0;
            $a['m'] = "No branches select";
        }       
        
        $a['maxno'] = $this->getMaxno();
        
        echo json_encode($a);
    }

    private function check_for_bc_exist($bc,$billtype){

        $q = $this->db->query("SELECT bc FROM `r_bill_type_sum` WHERE bc = '$bc' AND  billtype = '$billtype' ");

        if ($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    private function check_for_billtype_exist($billtype){

        $q = $this->db->query("SELECT billtype FROM `r_bill_type_sum` WHERE billtype = '$billtype' LIMIT 1 ");

        if ($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }
    
    
    public function load(){
	   echo $this->load_list($_POST['bc']);  
    }
    
    public function delete(){

	   $billtype = $_POST['billtype'];

       if ($this->used_billtype_check($billtype)){
            $a['s'] = 0; 
            $a['n'] = $this->db->conn_id->errno; 
            $a['m'] = "Unable to delete, bill type in use";

       }else{

           if ($this->db->query("DELETE FROM`r_bill_type_det` WHERE NO IN (SELECT NO FROM `r_bill_type_sum` WHERE billtype = '$billtype')")){
                $this->db->query("DELETE FROM `r_bill_type_sum` WHERE billtype = '$billtype' ");
                $a['s'] = 1;
            }else {
                
                $a['s'] = 0; 
                $a['n'] = $this->db->conn_id->errno; 
                $a['m'] = $this->utility->showErrorMSG($this->db->conn_id->errno,"Bill Type");
            }
        }

        echo json_encode($a);
    }

    public function load_list($bc=""){

        $Q = $this->db->query("SELECT no, billtype, period, amt_from, amt_to FROM `r_bill_type_sum` WHERE is_active = 1 GROUP BY billtype ORDER BY amt_from ");        

        $T = "<table border=1 class='tbl_bt'>";

        $T .= "<tr>";
        $T .= "<td>Bill Type</td>";
        $T .= "<td style='text-align:right' >Amt From</td>";
        $T .= "<td style='text-align:right' >Amt To</td>";
        $T .= "<td style='text-align:right' >Period</td>";        
        $T .= "<td style='text-align:right' >Action</td>";
        $T .= "</tr>";
        

        if ($Q->num_rows() > 0){            
            
            foreach($Q->result() as $R){
                

                $T .= "<tr>";
                $T .= "<td>".$R->billtype."</td>";
                $T .= "<td style='text-align:right' >".$R->amt_from."</td>";
                $T .= "<td style='text-align:right' >".$R->amt_to."</td>";
                $T .= "<td style='text-align:center' >".$R->period."</td>";                
                $T .= "<td style='text-align:right' ><a href=# onClick=setEdit('".$R->billtype."')>Edit</a> | <a href=# onClick=setDelete('".$R->billtype."')>Delete</a></td>";
                $T .= "</tr>";

            //    $T .= "<tr><td class='Cod'>".$R->no . "</td><td class='D'>-</td><td class='Des'>Rs " .$R->amt_from. " To Rs".$R->amt_to." </td><td class='ED'><a href=# onClick=setEdit('".$R->no."')>Edit</a> | <a href=# onClick=setDelete('".$R->no."')>Delete</a></td></tr>";
            

            }

        }else{
            $T = "<center>No bill type rates found</center>";
        }
        




        $T .= "</table>";
        $a['T'] = $T;        
    
        return json_encode($a);   
    }
    
    public function set_edit(){
        $no = $_POST['code'];
        $R = $this->db->query("SELECT * FROM `r_bill_type_sum` WHERE no = '$no' LIMIT 1")->row();
        echo json_encode($R);
    }
    
    public function search(){
        $k = $_POST['skey'];
        $Q = $this->db->query("SELECT * FROM `bill_types` WHERE (code like '%$k%' OR description like '%$k%') ");

        $T = "<table>";
        if ($Q->num_rows() > 0){            
            foreach($Q->result() as $R){
                $T .= "<tr><td class='Cod'>".$R->code . "</td><td class='D'>-</td><td class='Des'>" .$R->description. "</td><td class='ED'><a href=# onClick=setEdit('".$R->code."')>Edit</a> | <a href=# onClick=setDelete('".$R->code."')>Delete</a></td></tr>";
            }
        }else{
            $T = "<center>No bill types found</center>";
        }
        $T .= "</table>";
    
        echo $T;
            
    }     

    public function getMaxno(){
        return $max_no = $this->db->query("SELECT IFNULL(MAX(CONVERT(NO, SIGNED INTEGER))+1,1) AS `max_no` FROM `r_bill_type_sum` ")->row()->max_no;
    }

    public function autocomplete(){               
        $q = $_GET['term'];
        $bc = $this->sd['bc'];

        $query = $this->db->query("SELECT * FROM r_bill_type_sum WHERE billtype like '%$q%' and bc = '$bc' ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->billtype; }    
        echo json_encode($ary);
    }

    public function autocomplete_pawn_add_bill_type(){               
        $q = $_GET['term'];
        $bc = $this->sd['bc'];
        
        $query = $this->db->query("SELECT billtype FROM `r_bill_type_sum` WHERE billtype LIKE '%$q%' AND (bc = '$bc' OR bc = 'all') ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->billtype; }    
        echo json_encode($ary);
    }

    public function getBillTypeInfo(){                
        $bill_type_code = $_POST['bill_type_code'];        
        $bc = $this->sd['bc']; 
        $Q = $this->getBillDayRange($bill_type_code);
        
        if ($Q->num_rows() > 0){            
            $T = "";
            $R = $Q->row();

            $T .= "Code <br><span>" . $R->billtype . "</span><br><br>";            
            $T .= "Period <br><span>" . $R->period . "</span><br><br>"; 
            $T .= "Rate <br><span>" . $R->rate . "</span><br><br>";

            $a['rec']       = $T;
            $a['status']    = 1;
            $a['det']       = $R;
            
            $billtype =  $bill_type_code;

            $this->load->model('Calculate');
            
            $a['bill_max_no'] = $this->Calculate->next_billno($bc,$billtype);
        
        }else{            
            $a['status']    = 0;
        }
        echo json_encode($a);
    }


    public function getBillDayRange($bill_type_code,$loan_no="",$bc = ""){
        if ($bc == ""){
            $bc = $this->sd['bc'];
        }else{
            $bc = $bc;
        }
        
        if (isset($bill_type_code)){ 
            $bill_type_code = $bill_type_code; 
        }else{ 
            $bill_type_code = $_POST['bill_type_code'];
        }

        if ($loan_no == ""){
            return $this->db->query("SELECT billtype,period , day_from, day_to AS `first_int_charge_period` ,rate,allow_pre_int FROM `r_bill_type_sum` BTS JOIN `r_bill_type_det` BTD ON BTS.`no` = BTD.`no` WHERE BTS.bc = '$bc' AND BTS.billtype = '$bill_type_code' ORDER BY CAST(day_from AS UNSIGNED) ASC");
        }else{
            return $this->db->query("SELECT billtype,period , day_from, day_to AS `first_int_charge_period` ,rate,allow_pre_int FROM `t_loan_bill_int` BTS WHERE BTS.bc = '$bc' AND BTS.billtype = '$bill_type_code' AND loan_no = '$loan_no'ORDER BY CAST(day_from AS UNSIGNED) ASC");
        }
    }

    public function getBillTypeInfo_all($bt){                
        $bill_type_code = $bt;        
        $bc = $this->sd['bc'];
        $Q = $this->db->query("SELECT billtype,period , MIN(day_from), day_to AS `first_int_charge_period` ,rate FROM `r_bill_type_sum` BTS JOIN `r_bill_type_det` BTD ON BTS.`no` = BTD.`no` WHERE BTS.billtype = '$bill_type_code' AND BTS.`bc` = '$bc' LIMIT 1 ");
        if ($Q->num_rows() > 0){            
            $T = "";
            $R = $Q->row();
            $T = '<div><h3>Code</h3><span>'.$R->billtype.'</span></div> <div><h3>Period</h3><span>'.$R->period.'</span></div> <div><h3>Interest Rate</h3><span>'.$R->rate.'</span></div>';
        }else{            
            $T    = 0;
        }
        return  $T;
    }


    public function setUpdate(){
        $billtype = $_POST['billtype'];
        
        $Q = $this->db->query("SELECT * FROM `r_bill_type_sum` WHERE billtype = '$billtype'");

        if ($Q->num_rows() > 0){
            $a['sum'] = $Q->row();
            $no = $Q->row()->no;
            $a['det'] = $this->db->query("SELECT * FROM `r_bill_type_det` WHERE NO = '$no'")->result();
            $a['bc_list'] = $this->db->query("SELECT bc FROM `r_bill_type_sum` WHERE billtype = '$billtype' ORDER BY bc")->result();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);
    }

    private function used_billtype_check($billtype){

        $q = $this->db->query("SELECT billtype FROM `t_loan` WHERE billtype = '$billtype' LIMIT 1");

        if ($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    private function check_amount_range_exist($newStart,$newEnd,$bc_in_list,$pr,$billtype="",$is_old_bill){
        
        if ($billtype == ""){
            $qury = "";
        }else{
            $qury = " AND NOT billtype = '$billtype' ";
        }

        if ($is_old_bill == 1){ // Modified 2016-09-08
            $btL = "billtype = '$billtype' AND ";
        }else{
            $btL = "";
        }

        $q2 = $this->db->query("SELECT billtype FROM `r_bill_type_sum` WHERE $btL bc IN ($bc_in_list) AND `gratr_lesstn` = '$pr' AND ((amt_from BETWEEN $newStart AND $newEnd) OR (amt_to BETWEEN $newStart AND $newEnd) OR ($newStart BETWEEN amt_from AND amt_to)) $qury AND NOT is_old_bill = 1 ");
        
        if ($q2->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }


    public function load_bc_billtypes(){
        
        $bc = $_POST['bc'];

        if ($bc == ""){
            $q = $this->db->query("SELECT billtype FROM `r_bill_type_sum` GROUP BY billtype");
        }else{        
            $q  = $this->db->query("SELECT billtype FROM `r_bill_type_sum` BTS WHERE BTS.`bc` = '$bc' AND BTS.`is_active` = 1");
        }
        
        $a['bt_det'] = $q->result();
        
        echo json_encode($a);

    }

    public function get_bt_log_no(){

        return $this->db->query("SELECT IFNULL(MAX(log_no)+1,1) AS `log_no` FROM `r_bill_type_sum_log`")->row()->log_no;

    }

}