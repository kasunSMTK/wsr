<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_cheque_issue extends CI_Model {

	private $sd;

	function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
	}

	public function base_details(){		
		$a['current_date'] = $this->sd['current_date'];
        $a['max_no']= $this->get_max_no();
        return $a;
    }    

    public function save(){
    	
        $this->db->trans_begin();       

        if ($_POST['hid'] == "0"){
            $max_no = $this->get_max_no();
        }else{
            $max_no = $_POST['hid'];
        }

        $det = Array();
        
        $_POST['no'] = $max_no;
        $_POST['oc'] = $this->sd['oc'];
        
        for ($n = 0 ; $n < count( $_POST['cheque_no'] ) ;$n++){
            
            $det[] = Array(
                
                "no"=>$max_no,
                "chq_no"=>$_POST['cheque_no'][$n],
                "amount"=>$_POST['c_amount'][$n],
                "branch"=>$_POST['issue_bc'],
                "received_date"=>$_POST['date'],
                "status"=>"P",
                "status_date"=>$_POST['date'],
                "oc"=>$this->sd['oc']
            );

        }        

        
        $hid = $_POST['hid'];

        unset($_POST['c_amount'],$_POST['cheque_no'],$_POST['i_bc_name'],$_POST['hid']);       
        
        if ($hid == "0"){
            $Q = $this->db->insert('t_chq_reg_sum',$_POST);
            $Q = $this->db->insert_batch('t_chq_reg_det',$det);
                 $this->utility->user_activity_log($module='Cheque Issue',$action='insert',$trans_code='',$trans_no=$max_no,$note='');
        }else{
            $Q = $this->db->where('no',$hid)->delete('t_chq_reg_det');
            $Q = $this->db->where('no',$hid)->update('t_chq_reg_sum', $_POST); 
            $Q = $this->db->insert_batch('t_chq_reg_det',$det);
                 $this->utility->user_activity_log($module='Cheque Issue',$action='update',$trans_code='',$trans_no=$max_no,$note='');
        }


        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $a['s'] = "error";
        }else{
            
            $this->db->trans_commit();

            if ($Q){
                $a['nmax'] = $this->get_max_no();
                $a['s'] = 1;
            }else{
                $a['s'] = 0;
            }
        }

        echo json_encode($a);

    }

    public function load(){

        $no = $_POST['no'];
        $Q = $this->db->query("SELECT S.*,ACC.display_text AS `bank_name`,BC.name AS `i_bc_name` FROM `t_chq_reg_sum` S JOIN( SELECT * FROM m_account WHERE is_bank_acc = 1) ACC ON S.`bank_acc` = ACC.code JOIN( SELECT * FROM m_branches) BC ON S.`issue_bc` = BC.bc WHERE S.`no` = $no LIMIT 1");
        
        if ($Q->num_rows() > 0){
            $QQ = $this->db->query("SELECT * FROM `t_chq_reg_det` WHERE NO = $no");
            $a['sum'] = $Q->row();
            $a['det'] = $QQ->result();
            $a['s'] = 1;
        }else{
            $a['s'] = 0;
        }

        echo json_encode($a);

    }

    public function get_max_no(){
        return $this->db->query("SELECT IFNULL(MAX(NO)+1,1) AS `max_no` FROM `t_chq_reg_sum`")->row()->max_no;
    }


    public function auto_com(){               
        $q = $_GET['term'];
        $query = $this->db->query("SELECT * FROM m_account WHERE is_bank_acc = 1 AND (`code` LIKE '%$q%' OR `description` LIKE '%$q%' ) ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->code . " - ". $r->description ; }    
        echo json_encode($ary);
    }


    public function i_bc_auto_com(){               
        $q = $_GET['term'];
        $query = $this->db->query("SELECT bc,`name` FROM m_branches WHERE (`bc` LIKE '%$q%' OR `name` LIKE '%$q%' ) ");                
        $ary = array();    
        foreach($query->result() as $r){ $ary [] = $r->bc . " - ". $r->name ; }    
        echo json_encode($ary);
    }
    
}
