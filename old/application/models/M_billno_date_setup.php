<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_billno_date_setup extends CI_Model {
    
    private $sd;   
    
    function __construct(){
		parent::__construct();		
		$this->sd = $this->session->all_userdata();		
    } 

    public function base_details(){		        
        $a['d'] = $this->get_billno_date_setup();
		return $a;
    }

    public function get_billno_date_setup(){

        $Q = $this->db->get('t_billno_date_log');

        $t = "";


        foreach ($Q->result() as $R) {
            
            $t .="<tr>
                <td>".$R->count_start_date."</td>
                <td>".$R->effecting_year."</td>
                <td>".$R->deduct_days_count."</td>
                <td>".$R->last_bt_n_max_no."</td>
                <td>".$R->datetime."</td>
                <td>".$R->oc."</td>
            </tr>";

        }


        return $t;

    }
}