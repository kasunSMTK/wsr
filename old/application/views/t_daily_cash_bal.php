<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $( "#date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2050"
        });
    });

</script>   

    <div class="page_contain_new_pawn">
        <div class="page_main_title_new_pawn">
            <div style="width:240px;float:left"><span>Daily Cash Balance</span></div>                 
        </div>
    </div>

    <form action="<?=base_url()?>index.php/main/save/t_daily_cash_bal" method="post" id="form_"><br>

        <table align="center" border="0" width="400" cellpadding="0" cellspacing="0">            
            <tr>
                <td>
                    
                    Date <br>
                    <input class="input_text_regular_new_pawn <?=$date_change_allow?>" type="text" id="date" name="date" style="margin-top:10px;font-size:22px;width:100%; border: 2px solid Green" readonly="readonly" value="<?=$current_date?>"><br><br>

                    Amount<br><br>
                    <input class="input_text_large amount" type="text" style="width:100%;border:2px solid green;" id="amount" name="amount"><br><br>

                    <div style="text-align:right"><input value="Save" class="btn_regular" id="btnSave" type="button"></div>

                    <input type="hidden" name="code" value="0" id="code">

                </td>                
            </tr>

            <tr>
                <td><br><br>
                    <div class="daily_cash_bal"></div>
                </td>
            </tr>


        </table>

    </form>


</body>
</html>