<?php

	$make_excel = 1;

	$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
  	$t .= '<table border="1" borderColor=#eaeaea class="tbl_income_statment" cellspacing="0" cellpadding="0">';
	
	$t .= "<tr>";
	$t .= "<td>Expense Distribution Class/Account Wise</td>";	
	foreach ($bcc as $b) {$t .= "<td></td>"; }
	$t .= "<td></td>";	
	$t .= "</tr>";

	$t .= "<tr>";
	$t .= "<td>From ".$fd." to ".$td."</td>";
	foreach ($bcc as $b) {$t .= "<td></td>"; }	
	$t .= "<td></td>";
	$t .= "</tr>";	
	
	$t .= "<tr>";		
	$t .= "<td><b>Expense Account</b></td>";	
	foreach ($bcc as $bc) {$t .= "<td><b>".$bc->bc_name."</b></td>"; }
	$t .= "<td><b>Account Total</b></td>";
	$t .= "</tr>";

	$row = array();
	$acc_tot = $all_bc_tot = 0;

	foreach ($list as $r) {		
		$row[$r->bc][$r->acc_code] = $r->bal;
	}












	foreach ($acc as $account) {
		
		$t .= "<tr>";		
		$t .= "<td>".$account->description."</td>";	

		foreach ($bcc as $bc) {
			
			$t .= "<td align='right'>".d($row[$bc->bc][$account->code])."</td>"; 			
			$acc_tot += $row[$bc->bc][$account->code];

		}

		$t .= "<td align='right'><b>".d($acc_tot)."</b></td>";
		$t .= "</tr>";

		$all_bc_tot += $acc_tot;

		$acc_tot = 0;
	}



	$bc_tot 	= 0;
	$bc_tot_ar 	= array();

	foreach($bcc as $bc){

		foreach($list as $r ){
			if ($r->bc == $bc->bc){
				$bc_tot += $r->bal;
			}
		}

		$bc_tot_ar[$bc->bc] = $bc_tot;
		$bc_tot = 0;

	}


	$t .= "<tr>";	
	$t .= "<td><b>Branch Total</b></td>";
	
	foreach($bcc as $bc){

		//$t .= "<td>".$bc_tot_ar[$bc->bc]."</td>";	
		$t .= "<td></td>";	
	
	}

	$t .= "<td>".d($all_bc_tot)."</td>";
	$t .= "</tr>";


	




	
	$t .= '</table>';

	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'accrued_interest.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    exit;


	function d($a){
		return number_format($a,2);
	}

?>