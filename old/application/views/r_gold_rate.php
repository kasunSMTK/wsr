<div class="page_contain">
	<div class="page_main_title"><span>Gold Rate</span></div><br>

	<form method="post" action="<?=base_url()?>index.php/main/save/r_gold_rate" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add Gold Rate</b>
				</td>
				<td>
					<b>Exist Gold Rate</b>
				</td>			
			</tr>		

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Category</span> 
						<input class="input_text_regular" type="text" name="goldcatagory" id="goldcatagory" maxlength="10">
					</div>
				</td>			
				<td rowspan="4">
					<div class="exits_text_box_holder">
						<div class="text_box_title_holder" style="width:40px;border:0px solid red">Code</div> 
						<input class="nic_feild input_text_regular" style="width:194px" type="text" id="Search_code">
						<input type="button" value="Search"	name="btnSearch" id="btnSearch" class="btn_regular">					
					</div>

					<div class="list_div">
						<!-- <?=$load_list?> -->
					</div>
				</td>
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Pawning Rate</span> 
						<input class="currency amount input_text_regular" type="text" name="goldrate" id="goldrate" maxlength="9">
					</div>
				</td>
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Asset Rate</span> 
						<input class="currency amount input_text_regular" type="text" name="assetrate" id="assetrate" maxlength="9">
					</div>
				</td>
			</tr>

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Market Rate</span> 
						<input class="currency amount input_text_regular" type="text" name="marketrate" id="marketrate" maxlength="9">
					</div>
				</td>
			</tr>		

			<tr>
				<td height="10">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-13px">Print Value</span> 
						<input class="input_text_regular" type="text" name="printval" id="printval" maxlength="20">
					</div>
				</td>
			</tr>			

			<tr>
				<td>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>				
			</tr>					
		</table>

	</form> <br> <br> <br>

	
	<div class="page_main_title"><span>Gold Rate Change History</span></div><br>

	<table border="0" align="center" class="tbl_gold_rate_change_log">
		<thead>
			<tr>			
				<td>Gold Category</td>
				<td>Old Gold Rate</td>
				<td>New Gold Rate</td>
				<td>Print Value</td>
				<td>Old Asset Rate</td>
				<td>New Asset Rate</td>
				<td>Old Market Rate</td>
				<td>New Market Rate</td>
				<td>Changed Date Time</td>
				<td>Updated By</td>
			</tr>
		</thead>
		<tbody class="tbl_gold_rate_changed_log_det">
			<?=$gold_rate_chage_det?>
		</tbody>
	</table>

	<br><br><br><br>

</div>
</body>
</html>