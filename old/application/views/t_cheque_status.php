<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

	
	$(function() {
		$( "#fd,#td" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2025",
			onSelect: function(date){
				get_chq_details(date);				
			}
		});
	});
	

</script>




<div class="page_contain_new_pawn">
	
	<div class="discount_title_holder">	

		<table border="0" width="100%">
		
		<tr>

			<td valign="middle">
				<div style="width:250px;border:0px solid red;float:left;text-align: left;">				
					<span style="font-family: bitter; font-size: 19px;color: Green ">Cheque Withdrawal</span>
				</div>
				<div style="width:250px;border:0px solid red;float:left;text-align: left;">				
					<a href="?action=t_cash_trasfer_status"><span style="font-family: bitter; font-size: 19px; ">Cash Transfer </span></a>
				</div>
				<div style="width:250px;border:0px solid red;float:left;text-align: left;">				
					<a href="?action=t_cash_deposit_status"><span style="font-family: bitter; font-size: 19px; ">Cash Deposit</span></a>
				</div>
			</td>
			<td width="200" align="left">
				<span class="text_box_title_holder_new_pawn">From Date</span><br>
				<input class="input_text_regular_new_pawn" type="text" id="fd" name="fd" style="border:2px solid green" readonly="readonly" value="<?=date('Y-m-d')?>">				
			</td>
			<td width="100">				
				<span class="text_box_title_holder_new_pawn">To Date</span><br>
				<input class="input_text_regular_new_pawn" type="text" id="td" name="td" style="border:2px solid green" readonly="readonly" value="<?=date('Y-m-d')?>">				
			</td>

			<td width="100" style="padding-left: 10px;">
				<div style="height: 19px;"></div>
				<input type="button" value="Print" id="print_chq_stat" class="btn_regular">
			</td>

		</tr>

		</table>


	</div><br><br>
	

	<div class="chq_stat"><?=$chq_stat?></div>

	

	<form id="print_excel" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' id='by' value='r_print_fund_transfer_report' class="report" />                        
            <input type="hidden" class="input_active_num" name="fd_r" id="fd_r" />
            <input type="hidden" class="input_active_num" name="td_r" id="td_r" />    
            <input type="hidden" class="input_active_num" name="tr_type" id="tr_type" />
            <input type="hidden" 						  name="bc_r" id="bc_r" />            
        </div>                  
    </form>

</div>



</body>
</html>