<?php

    $make_excel = 1;

    if ($d_range == 'months'){

        $start    = (new DateTime($fd))->modify('first day of this month');
        $end      = (new DateTime($td))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);
        
        $t  = '<table border="1">';

        $t .= '<tr>';
        $t .= '<td colspan="4"><h2>Interest Income Schedule - Total (Month Wise) </h2></td>';        
        $t .= '</tr>';

        $t .= '<tr>';
        $t .= '<td colspan="4"><h4>From '.$fd.' to '.$td.'</h4></td>';        
        $t .= '</tr>';


        // create bc array for loop branches
        
        $BC_ar = array();
        $BC_na = array();
        $YM_ar = array();
        $YM_BAL = array();
        $MON_TOT = array();

        foreach ($list as $bc_list) {
            $YM_BAL[$bc_list->bc][$bc_list->m] = $bc_list->total_int;
        }           

        foreach ($bcc as $BCC) {
            $BC_ar[] = $BCC->bc;
            $BC_na[$BCC->bc] = $BCC->name;
        }        

        foreach ($period as $dt) {
            $YM_ar[] = $dt->format("Y-m");
        }

        
        // loop thru branches and draw months coloums

        $t .= '<tr>';
        $t .= '<td><b>Branch</b></td>';

        foreach ($YM_ar as $months) {
            $t .= '<td align="right"><b>'.$months.'</b></td>';
        }

        $t .= '<td align="right"><b>Branch Total</b></td>';
        $t .= '</tr>';

        $bcTotal = $monthTotal = 0;

        for ($n = 0 ; $n < count($BC_ar) ; $n++){           

            if (count($selct_bc) > 0){

                if (in_array($BC_ar[$n], $selct_bc)){
                
                    $t .= '<tr>';
                    $t .= '<td>'.$BC_na[$BC_ar[$n]].'</td>';

                        for($nn = 0 ; $nn < count($YM_ar) ; $nn++ ){
                            $t .= '<td align="right">'.d($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]).'</td>';
                            $bcTotal += floatval($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]);
                        }

                    $t .= '<td align="right"><b>'.d($bcTotal).'</b></td>';
                    $t .= '</tr>';
                                
                    $bcTotal = 0;

                }

            }else{

                $t .= '<tr>';
                $t .= '<td>'.$BC_na[$BC_ar[$n]].'</td>';

                    for($nn = 0 ; $nn < count($YM_ar) ; $nn++ ){
                        $t .= '<td align="right">'.d($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]).'</td>';
                        $bcTotal += floatval($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]);
                    }

                $t .= '<td align="right"><b>'.d($bcTotal).'</b></td>';
                $t .= '</tr>';
                            
                $bcTotal = 0;

            }

        }        

        
        $tot = 0;

        for($nn = 0 ; $nn < count($YM_ar) ; $nn++ ){
            for ($n = 0 ; $n < count($BC_ar) ; $n++){
                foreach ($list as $r) {
                    if ($r->m == $YM_ar[$nn]){
                        $tot += $r->total_int;
                    }
                }                
                $MON_TOT[$YM_ar[$nn]] = $tot;
                $tot  = 0;
            }
        }

        $t .= '<tr>';
        $t .= '<td>Month Total</td>';

        // loop thru date range and draw range total

        $total = 0;

        foreach ($YM_ar as $months) {
            $t .= '<td align="right"><b>'.d($MON_TOT[$months]).'</b></td>';
            $total += $MON_TOT[$months];
        }

        $t .= '<td align="right"><b>'.d($total).'</b></td>';
        $t .= '</tr>';

       
        $t .= '</table>';

    }

    if ($d_range == 'years'){

        $period = new DatePeriod(new DateTime($fd), new DateInterval('P1Y'), new DateTime($td.' +1 day'));
        
        foreach ($period as $date) {
            $dates[] = $date->format("Y");
        }

        $period = $dates;

        foreach ($period as $y) {
            $period[] =  $y.'<br>';
        }
        
        $t  = '<table border="1">';

        $t .= '<tr>';
        $t .= '<td colspan="4"><h2>Interest Income Schedule - Total (Year Wise) </h2></td>';        
        $t .= '</tr>';

        $t .= '<tr>';
        $t .= '<td colspan="4"><h4>From '.$fd.' to '.$td.'</h4></td>';        
        $t .= '</tr>';


        // create bc array for loop branches
        
        $BC_ar = array();
        $BC_na = array();
        $YM_ar = array();
        $YM_BAL = array();
        $MON_TOT = array();

        foreach ($list as $bc_list) {
            $YM_BAL[$bc_list->bc][$bc_list->m] = $bc_list->total_int;
        }           

        foreach ($bcc as $BCC) {
            $BC_ar[] = $BCC->bc;
            $BC_na[$BCC->bc] = $BCC->name;
        }        

        //$period = array('2016','2017','2018');

        foreach ($period as $dt) {
            //$YM_ar[] = $dt->format("Y-m");
            $YM_ar[] = $dt;
        }

        
        // loop thru branches and draw months coloums

        $t .= '<tr>';
        $t .= '<td><b>Branch</b></td>';

        foreach ($YM_ar as $months) {
            $t .= '<td align="right"><b>'.$months.'</b></td>';
        }

        $t .= '<td align="right"><b>Branch Total</b></td>';
        $t .= '</tr>';

        $bcTotal = $monthTotal = 0;

        for ($n = 0 ; $n < count($BC_ar) ; $n++){           

            if (count($selct_bc) > 0){

                if (in_array($BC_ar[$n], $selct_bc)){
                
                    $t .= '<tr>';
                    $t .= '<td>'.$BC_na[$BC_ar[$n]].'</td>';

                        for($nn = 0 ; $nn < count($YM_ar) ; $nn++ ){
                            $t .= '<td align="right">'.d($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]).'</td>';
                            $bcTotal += floatval($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]);
                        }

                    $t .= '<td align="right"><b>'.d($bcTotal).'</b></td>';
                    $t .= '</tr>';
                                
                    $bcTotal = 0;

                }

            }else{

                $t .= '<tr>';
                $t .= '<td>'.$BC_na[$BC_ar[$n]].'</td>';

                    for($nn = 0 ; $nn < count($YM_ar) ; $nn++ ){
                        $t .= '<td align="right">'.d($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]).'</td>';
                        $bcTotal += floatval($YM_BAL[$BC_ar[$n]][$YM_ar[$nn]]);
                    }

                $t .= '<td align="right"><b>'.d($bcTotal).'</b></td>';
                $t .= '</tr>';
                            
                $bcTotal = 0;

            }

        }        

        
        $tot = 0;

        for($nn = 0 ; $nn < count($YM_ar) ; $nn++ ){
            for ($n = 0 ; $n < count($BC_ar) ; $n++){
                foreach ($list as $r) {
                    if ($r->m == $YM_ar[$nn]){
                        $tot += $r->total_int;
                    }
                }                
                $MON_TOT[$YM_ar[$nn]] = $tot;
                $tot  = 0;
            }
        }

        $t .= '<tr>';
        $t .= '<td>Month Total</td>';

        // loop thru date range and draw range total

        $total = 0;

        foreach ($YM_ar as $months) {
            $t .= '<td align="right"><b>'.d($MON_TOT[$months]).'</b></td>';
            $total += $MON_TOT[$months];
        }

        $t .= '<td align="right"><b>'.d($total).'</b></td>';
        $t .= '</tr>';

       
        $t .= '</table>';  

    }

    if ($d_range == 'quarter'){

        // years loop
        $period = new DatePeriod(new DateTime($fd), new DateInterval('P1Y'), new DateTime($td.' +1 day'));
        
        foreach ($period as $date) {
            $years[] = $date->format("Y");
        }

        // quarters loop
        $quarter = array(1,2,3,4);
        
        // branch loop
        foreach ($bcc as $BCC) {
            $BC_ar[] = $BCC->bc;
            $BC_na[$BCC->bc] = $BCC->name;
        }

        // data loop        
        foreach ($list as $bc_list) {
            $YM_BAL[$bc_list->bc][$bc_list->y][$bc_list->q] = $bc_list->total_int;
        }

        
        $t = '<table border="1" width="100%" cellspacing="0" cellpadding="0" borderColor="#eaeaea">';

        $t .= '<tr>';
        $t .= '<td colspan="4"><h2>Interest Income Schedule - Total (Quarter Wise) </h2></td>';        
        $t .= '</tr>';

        $t .= '<tr>';
        $t .= '<td>From '.$fd.' to '.$td.'</td>';
        foreach ($years as $yrs) { $t .= '<td colspan="4" align="center"><b>'.$yrs.'</b></td>'; }
        $t .= '<td align="right"><b>Branch Total</b></td>';
        $t .= '</tr>';

        $t .= '<tr>';
        $t .= '<td><b>Branch</b></td>';
        foreach ($years as $yrs) {foreach ($quarter as $q) {$t .= '<td align="right"><b>Q'.$q.'</b></td>';}}
        $t .= '<td></td>';
        $t .= '</tr>';

        $bc_tot = $all_bc_tot = 0;


        for ($n = 0 ; $n < count($BC_ar) ; $n++){

            $t .= '<tr>';
            $t .= '<td>'.$BC_na[$BC_ar[$n]].'</td>';

            foreach ($years as $y) {
                
                foreach ($quarter as $q) {                    
                    
                    $t .= '<td align="right">'.d($YM_BAL[$BC_ar[$n]][$y][$q],2).'</td>';

                    $bc_tot += $YM_BAL[$BC_ar[$n]][$y][$q];
                    $all_bc_tot += $YM_BAL[$BC_ar[$n]][$y][$q];
                }

            }

            // branch total here
            $t .= '<td align="right"><b>'.d($bc_tot,2).'</b></td>';
            $bc_tot = 0;
            $t .= '</tr>';

        }

        $tot = 0;        
        
        foreach ($years as $y) {                            
            foreach ($quarter as $q) {                
                foreach ($list as $r) {
                    if ($r->y == $y && $r->q == $q){
                        $tot += $r->total_int;
                    }
                }

                $YQ_TOT[$y][$q] = $tot;
                $tot  = 0;
            }
        }


        $t .= '<tr>';
        $t .= '<td><b>Quarter Total</b></td>';
        foreach ($years as $yrs) { foreach ($quarter as $q) { $t .= '<td align="right"><b>'.d($YQ_TOT[$yrs][$q],2).'</b></td>'; } }
        $t .= '<td align="right"><b><font color="green">'.d($all_bc_tot,2).'</font></b></td>';
        $t .= '</tr>';

        $t .= '</table>';        

    }

    if ($d_range == 'half_year'){

        // years loop
        $period = new DatePeriod(new DateTime($fd), new DateInterval('P1Y'), new DateTime($td.' +1 day'));
        
        foreach ($period as $date) {
            $years[] = $date->format("Y");
        }

        // quarters loop
        $quarter = array(0,1);
        
        // branch loop
        foreach ($bcc as $BCC) {
            $BC_ar[] = $BCC->bc;
            $BC_na[$BCC->bc] = $BCC->name;
        }

        // data loop        
        foreach ($list as $bc_list) {
            $YM_BAL[$bc_list->bc][$bc_list->y][$bc_list->q] = $bc_list->total_int;
        }

        
        $t = '<table border="1" width="100%" cellspacing="0" cellpadding="0" borderColor="#eaeaea">';

        $t .= '<tr>';
        $t .= '<td colspan="4"><h2>Interest Income Schedule - Total (Half Year Wise) </h2></td>';        
        $t .= '</tr>';

        $t .= '<tr>';
        $t .= '<td>From '.$fd.' to '.$td.'</td>';
        foreach ($years as $yrs) { $t .= '<td colspan="2" align="center"><b>'.$yrs.'</b></td>'; }
        $t .= '<td align="right"><b>Branch Total</b></td>';
        $t .= '</tr>';

        $t .= '<tr>';
        $t .= '<td><b>Branch</b></td>';
        foreach ($years as $yrs) {foreach ($quarter as $q) {

            if ($q == 0){ $hx = 'JAN-JUN'; }
            if ($q == 1){ $hx = 'JUL-DEC'; }

            $t .= '<td align="right"><b>'.$hx.'</b></td>';


        }}
        $t .= '<td></td>';
        $t .= '</tr>';

        $bc_tot = $all_bc_tot = 0;


        for ($n = 0 ; $n < count($BC_ar) ; $n++){

            $t .= '<tr>';
            $t .= '<td>'.$BC_na[$BC_ar[$n]].'</td>';

            foreach ($years as $y) {
                
                foreach ($quarter as $q) {                    
                    
                    $t .= '<td align="right">'.d($YM_BAL[$BC_ar[$n]][$y][$q],2).'</td>';

                    $bc_tot += $YM_BAL[$BC_ar[$n]][$y][$q];
                    $all_bc_tot += $YM_BAL[$BC_ar[$n]][$y][$q];
                }

            }

            // branch total here
            $t .= '<td align="right"><b>'.d($bc_tot,2).'</b></td>';
            $bc_tot = 0;
            $t .= '</tr>';

        }

        $tot = 0;        
        
        foreach ($years as $y) {                            
            foreach ($quarter as $q) {                
                foreach ($list as $r) {
                    if ($r->y == $y && $r->q == $q){
                        $tot += $r->total_int;
                    }
                }

                $YQ_TOT[$y][$q] = $tot;
                $tot  = 0;
            }
        }


        $t .= '<tr>';
        $t .= '<td><b>Half Year Total</b></td>';
        foreach ($years as $yrs) { foreach ($quarter as $q) { $t .= '<td align="right"><b>'.d($YQ_TOT[$yrs][$q],2).'</b></td>'; } }
        $t .= '<td align="right"><b><font color="green">'.d($all_bc_tot,2).'</font></b></td>';
        $t .= '</tr>';

        $t .= '</table>';        

    }


    if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'excel.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    function d($a){
        return number_format($a,2);
    }


?>