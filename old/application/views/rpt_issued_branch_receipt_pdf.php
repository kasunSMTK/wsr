<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetAutoPageBreak(TRUE, 0);
	$this->pdf->AddPage();

	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
	
	$total 	= 0;
	$sb 	= '';

	$bc_tot = 0;

	$first_r_passed = false;

	foreach ($det as $r) {

		$h = 4 * 	(	max(1, 	$this->pdf->getNumLines($r->det,100),
								$this->pdf->getNumLines($r->receipt_desc,30),
								$this->pdf->getNumLines($r->payee_info,20),
								$this->pdf->getNumLines($r->r_name,20)
							) );


		if (strtoupper($sb) != strtoupper($r->bcc) ){


			if ($first_r_passed){
				$this->pdf->SetFont('helvetica', 'B', 8);		
				$this->pdf->MultiCell(10,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(17,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20,  $h, 'Total',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20,  $h, number_format($bc_tot,2),	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(17,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(100, $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 8);
				$bc_tot = 0;
				$this->pdf->ln(2);
			}
			
			$first_r_passed = true;

			$this->pdf->SetFont('helvetica', 'B', 10);	
			$this->pdf->MultiCell(0,  0, "Branch Receipt List ", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
			$this->pdf->SetFont('helvetica', '', 8);		
			$this->pdf->MultiCell(0,  0, "From :".$fd. " To : ".$td,				$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->ln(4);

			$this->pdf->SetFont('helvetica', 'B', 13);
			$this->pdf->MultiCell(0,0, $r->b_name,				$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->SetFont('helvetica', '', 8);
			$sb = $r->bcc;


			$this->pdf->MultiCell(10,  0, "No",				$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(17,  0, "Date",			$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20,  0, "Paid Acc",		$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20,  0, "Amount",			$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(30,  0, "Description",	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20,  0, "Payee Info",		$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20,  0, "P.Name",			$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20,  0, "Billno",			$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(17,  0, "Bill P.Date",	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(100,  0, "Receipt items",	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		}
		

		$this->pdf->MultiCell(10,  $h, $r->nno,				$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(17,  $h, $r->date,			$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, $r->paid_to_acc,		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, number_format($r->cash_amount,2),		$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  $h, $r->receipt_desc,	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, $r->payee_info." ",	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, $r->r_name,			$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, $r->r_billno,		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(17,  $h, $r->r_pawn_date,		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(100,  $h, $r->det." ",		$border = '01', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		
		$bc_tot += $r->cash_amount;
		$bc_all_tot += $r->cash_amount;

	}

		$this->pdf->SetFont('helvetica', 'B', 8);		
		$this->pdf->MultiCell(10,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(17,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, 'Total',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, number_format($bc_tot,2),	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(17,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(100, $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);


		$this->pdf->ln(10);

		$this->pdf->SetFont('helvetica', 'B', 11);		
		$this->pdf->MultiCell(67,  $h, "All Branch Total ". number_format($bc_all_tot,2),	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(17,  $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(100, $h, '',		$border = '01', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);



	$this->pdf->Output("bc_receipt-list.pdf", 'I');

?>