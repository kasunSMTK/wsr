<?php if($this->user_permissions->is_view('r_transaction_list_voucher')){ ?>
<!-- <h2 style="text-align: center;">Transaction Reports - Voucher </h2> -->
<link rel="stylesheet" href="<?=base_url()?>css/report.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

  $(function() {
    $(".input_date_down_future").datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:''"

  });
});
</script>
<div class="page_main_title"><span>Transaction Reports - Voucher & Receipt</span></div> 
<div class="dframe" id="r_view2" style="width: 1100px;">
    <form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">

        <fieldset style="width: 1000px; margin-left:50px;">

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px;">
                <tr>
                    <td style="width:180px; padding-left:10px; padding-right:10px;">From</td>
                    <td style="width:110px;">
                        <input type="text" class="input_date_down_future" id="from" name="from" value="<?=date('Y-m-d')?>" style="width: 80px;" />

                    </td>
                    <td style="width:40px;"> To </td>
                    <td>
                        <input type="text" class="input_date_down_future" id="to" name="to"value="<?=date('Y-m-d')?>" style="width: 80px;"  /> &nbsp; &nbsp;
                        <input type="checkbox" name="chkdate1" id="chkdate1" value="1" checked>
                    </td>
                </tr>


            </table>
        </fieldset>    

        <fieldset style="width: 1000px; margin-left:50px; margin-top:2px;">

            <div id="report_view" style="overflow: auto; ">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px;" id="middle" name="middle" >

                    <tr>
                        <!-- <td style="padding-left:10px; padding-right:10px; width:83px;">Cluster</td> -->
                        <td style="padding-left:26px;"><?php //echo $cluster; ?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px; padding-right:10px; width:83px;"><br/>Branch</td>
                        <td  style="padding-left:26px;"><br/><?php echo $branch; ?> </td>

                    </tr>
                    <input type="hidden" id="d_cl" value='<?php echo $d_cl ?>' name="d_cl"/>
                    <input type="hidden" id="d_bc" value='<?php echo $d_bc ?>' name="d_bc"/>

                    <tr>    
                        <td style="width:83px; padding-left:10px; padding-right:10px;">Account Code</td>

                        <td style="width:450px; padding-left:26px;"><input type="text" class="input_txt" value="" id="acc_code" name="acc_code" style="width:450px;" />
                            <input type="text" class="hid_value"  readonly="readonly" id="acc_code_des" name="acc_code_des" style="width: 250px;">

                        </td>


                    </tr>
                    <tr><td colspan="3"><br/><hr> <br/></td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; height:310px;" id="middle2" name="middle2">
                    <tr>
                        <td style="padding-left:20px;"> <!-- Voucher lists -->
                            <table  width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; height:310px; padding-left:20px;" id="middle1" name="middle1">
                                <tr>  <td style="text-align:left; font-size: 14px; font-weight:700; padding-left:80px;"> <b></b> </td>  </tr>


                                <tr>
                                    <?php if($this->user_permissions->is_view('r_general_voucher_summery')){ ?>
                                    <td colspan="2">
                                        <input type='radio' name='by' v='r_general_voucher_summery' value="r_general_voucher_summery" class="report" />General Voucher List (Summery)
                                    </td> 
                                    <?php } ?>               
                                </tr>
                        <!--     <tr>
                            <?php if($this->user_permissions->is_view('r_cancelled_general_voucher_summery')){ ?>
                            <td colspan="2">
                                <input type='radio' name='by' title='r_cancelled_general_voucher_summery' value="r_cancelled_general_voucher_summery" class="report" />Cancelled General Voucher List (Summery)
                            </td>
                            <?php } ?>                
                        </tr> -->

                        <tr>
                            <?php if($this->user_permissions->is_view('r_general_voucher_details')){ ?>
                            <td colspan="2">
                                <input type='radio' name='by' title='r_general_voucher_details' value="r_general_voucher_details" class="report" />General Voucher List (Details)
                            </td> 
                            <?php } ?>               
                        </tr>

                        <tr>
                            <?php if($this->user_permissions->is_view('r_payable_invoice_summery')){ ?>
                            <td colspan="2">
                                <input type='radio' name='by' title='r_payable_invoice_summery' value="r_payable_invoice_summery" class="report" />Payable Invoice (Summery)
                            </td>
                            <?php } ?>                   
                        </tr>

                        <tr>
                            <?php if($this->user_permissions->is_view('r_payable_invoice_details')){ ?>
                            <td colspan="2">
                                <input type='radio' name='by' title='r_payable_invoice_details' value="r_payable_invoice_details" class="report" />Payable Invoice (Details)
                            </td> 
                            <?php } ?>              
                        </tr>
                    </table>
                </td>
                <td> 
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; height:250px;" id="middle2" name="middle2">
                        <tr><td style="text-align:left; font-size: 14px; font-weight:700; padding-left:80px;"> <b></b></td></tr>

                        <?php if($this->user_permissions->is_view('r_general_recipt_summery')){ ?>
                        <tr>
                            <td colspan="2">
                                <input type='radio' name='by' value='r_general_recipt_summery' title="r_general_recipt_summery" class="report" />General Receipt List (Summery)
                            </td>               
                        </tr>
                        <?php } ?>

                        <?php if($this->user_permissions->is_view('r_cancelled_general_recipt_summery')){ ?>
                        <tr>
                            <td colspan="2">
                                <input type='radio' name='by' value='r_cancelled_general_recipt_summery' title="r_cancelled_general_recipt_summery" class="report" />Cancelled General Receipt List (Summery)
                            </td>               
                        </tr>
                        <?php } ?>

                        <!--     <?php if($this->user_permissions->is_view('r_general_recipt_Details')){ ?>
                        <tr>
                            <td colspan="2">
                                <input type='radio' name='by' value='r_general_recipt_Details' title="r_general_recipt_Details" class="report" />General Receipt List (Details)
                            </td>               
                        </tr>
                        <?php } ?> -->

                        <?php if($this->user_permissions->is_view('r_recievalble_invoice_summery')){ ?>
                        <tr>
                            <td colspan="2">
                                <input type='radio' name='by' value='r_recievalble_invoice_summery' title="r_recievalble_invoice_summery" class="report" />Receivalble Invoice (Summery)
                            </td>               
                        </tr>
                        <?php } ?>

                        <?php if($this->user_permissions->is_view('r_recievable_invoice_details')){ ?>
                        <tr>
                            <td colspan="2">
                                <input type='radio' name='by' value='r_recievable_invoice_details' title="r_recievable_invoice_details" class="report" />Receivalble Invoice (Details)
                            </td>               
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="2">
                                <input type='radio' name='by' value='r_general_recipt_summery_br' title="r_general_recipt_summery_br" class="report" />Branch Receipt List (Summery)
                            </td>               
                        </tr>

                    </table>
                </td>
            </tr>
        </table>

    </div>
    <div style="text-align: right; padding-top: 7px; padding-right: 10px; padding-bottom: 7px;">


        <button class="btn_regular" id="btnReset">Reset</button> 
        <input type="hidden" name="type" id="type"  value=""/>
        <input type="button" class="btn_regular" value="Exit" id="btnExit" value="Exit">
        <button class="btn_regular" id="btnPrint">Print PDF</button>
    </div>
</fieldset>

<!-- <input type="hidden" id='by' name='by' value='' value="" class="report"> -->
<input type="hidden" name='page' value='A4' value="A4" >
<input type="hidden" name='orientation' value='P' value="P" >
<input type="hidden" id='type' name='type' value='' value="" >
<input type="hidden" name='dd' value="<?=date('Y-m-d')?>" id="dd" >


</form>
</div>

<?php } ?>