<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetAutoPageBreak(TRUE, 0);
	$this->pdf->AddPage();

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	

	$this->pdf->MultiCell(0, 0, "Branch Issued Cheques", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell(0, 0, "From ".$fd. " to ". $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	

	$this->pdf->MultiCell(0, 0, $bc_name , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	
	$this->pdf->setX(0);
	$this->pdf->ln();


	$this->pdf->SetFont('helvetica', '', 8);	

		$this->pdf->MultiCell(10,  0, "No",					$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(18,  0, "Issued Date",		$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(30,  0, "Bank Account",		$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(35,  0, "Cheque No",			$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(20,  0, "Amount",				$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(30,  0, "Employee",			$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(30,  0, "Added By",			$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(30,  0, "Date Time",			$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->MultiCell(30,  0, "Memo",				$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
	
		$st = '';

	foreach($Q as $r){

		$h = 5 * (max(1,$this->pdf->getNumLines($r->chq_no,35),$this->pdf->getNumLines($r->memo,30)));
			
		$this->pdf->MultiCell(10, $h, $r->no,								$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(18, $h, $r->date,							$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(30, $h, $r->bank_acc.' - '.$r->acc_desc,	$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(35, $h, $r->chq_no,						$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(20, $h, $r->amount,						$border = 'B', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(30, $h, $r->employee,						$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(30, $h, $r->oc,							$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(30, $h, $r->action_datetime,				$border = 'B', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		$this->pdf->MultiCell(30, $h, $r->memo,							$border = 'B', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $h = $h, $valign = 'H', $fitcell = false);		
		

	}


	$this->pdf->Output("bc_cheque_issued.pdf", 'I');
	

?>