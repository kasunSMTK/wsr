<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Advance Payments Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();	

	$this->pdf->SetFont('helvetica', '', 10);	

	$this->pdf->MultiCell(10, 1, "S.No", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Bill Type", 'B','L', 0, 0, '', '', false, '', 0);		
	$this->pdf->MultiCell(25, 1, "Bill No", 'B','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(80, 1,"Cus Name / Address", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(23, 1, "NIC", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(25, 1, "Mobile", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(25, 1, "Total Weight", 'B','C', 0, 0, '', '', '', '', 0);	
	$this->pdf->MultiCell(35, 1, "Paid DateTime", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Amount", 'B','R', 0, 1, '', '', '', '', 0);
	
	$this->pdf->SetFont('', '', 10);	

	$no 	= 1;
	$bc_tot = 0;

	$first_round_passes = false;

	foreach($list as $r){

		if ($r->bc != $st){	

			$bc_name = $r->bc_name;
			$st 	 = $r->bc;
			
			$this->pdf->SetFont('helvetica', 'B', 8);	
			
			if ( $first_round_passes ){
				
				$this->pdf->MultiCell(10, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
				$this->pdf->MultiCell(20, $h, '' , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
				$this->pdf->MultiCell(25, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);		
				$this->pdf->MultiCell(80, $h, '' , $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
				$this->pdf->MultiCell(23, $h, '' , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
				$this->pdf->MultiCell(25, $h, '' , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
				$this->pdf->MultiCell(25, $h, dd($bc_tot_w) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
				$this->pdf->MultiCell(35, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
				$this->pdf->MultiCell(30, $h, number_format($bc_tot,2) , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		
			}

			$this->pdf->MultiCell(0,5, $bc_name, $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);		
		
			$first_round_passes = true;
			$bc_tot = 0;
			$bc_tot_w = 0;

		}else{

			$bc_name = "";

		}
		
		$h = 5 * (max(1,$this->pdf->getNumLines($r->cusname." - ".$r->address,80),$this->pdf->getNumLines($r->action_date,35),$this->pdf->getNumLines($r->amount,30))) - 5;
		
		$this->pdf->SetFont('helvetica', '', 8);	
		$this->pdf->MultiCell(10, $h, $no , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->billtype , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(25, $h, $r->billno , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);		
		$this->pdf->MultiCell(80,$h, $r->cusname." - ".$r->address , $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(23, $h, $r->nicno , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(25, $h, $r->mobile , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(25, $h, $r->totalweight , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(35, $h, str_replace(",", "<br>",$r->action_date) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$this->pdf->MultiCell(30, $h, number_format($r->amount,2) , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
		$no++;

		$bc_tot += $r->amount;
		$bc_tot_w += $r->totalweight;

	}


	// last branch total
	$this->pdf->SetFont('helvetica', 'B', 8);	
	$this->pdf->MultiCell(10, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
	$this->pdf->MultiCell(20, $h, '' , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
	$this->pdf->MultiCell(25, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);		
	$this->pdf->MultiCell(80, $h, '' , $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
	$this->pdf->MultiCell(23, $h, '' , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
	$this->pdf->MultiCell(25, $h, '' , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
	$this->pdf->MultiCell(25, $h, dd($bc_tot_w) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
	$this->pdf->MultiCell(35, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);
	$this->pdf->MultiCell(30, $h, number_format($bc_tot,2) , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='T', $fitcell=false);



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function dd($number) {
	    return number_format($number, 3, '.', ',');
	}


	$this->pdf->Output("PDF.pdf", 'I');

?>