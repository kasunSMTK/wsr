<?php



	$this->pdf = new TCPDF("L", PDF_UNIT, 'A3', true, 'UTF-8', false);

	$this->pdf->SetPrintHeader(false);

	$this->pdf->SetPrintFooter(false);

	$this->pdf->AddPage();	



	$this->pdf->SetFont('helvetica', '', 28);	

	//$this->pdf->setY(10);

	$this->pdf->setX(0);

	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));

	$this->pdf->Line(5, 0, 0, 0, $style);



	$this->pdf->MultiCell(0, 0, "Interest Income Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	

	$this->pdf->SetFont('helvetica', '', 15);	

	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->setX(0);

	$this->pdf->ln();

	

	$tot_pwnint=$tot_rdeem=$full_tot=(float)0;

	$this->pdf->SetFont('helvetica', '', 8);	

	$this->pdf->setX(15);
	
	$this->pdf->MultiCell(60, 1, "Branch", 'B','C', 0, 0, '', '', false, '', 0);	

	$this->pdf->MultiCell(30, 1, "Pawning Int", 'B','L', 0, 0, '', '', false, '', 0);//

	$this->pdf->MultiCell(30, 1, "Redeem Int", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(30, 1, "Total", 'B','C', 0, 1, '', '', false, '', 0);

	//$this->pdf->ln();



	$heigh=1; $bdr='B'; $y=0;
	foreach($list as $r){
		$this->pdf->setX(15);

		$biltype1= $r->billtype;

		if($biltype1==$biltype2){

		}else{

			if($y==1){
				$this->pdf->ln();
				$this->pdf->setX(15);
				$this->pdf->SetFont('helvetica', 'B', 8);		
				
				$this->pdf->MultiCell(60, 1, "Total", 'B','R', 0, 0, '', '', '', '', 0);
				$this->pdf->MultiCell(30, 1, number_format($tot_pwnint,2), 'B','R', 0, 0, '', '', '', '', 0);
				$this->pdf->MultiCell(30, 1, number_format($tot_rdeem,2), 'B','R', 0, 0, '', '', '', '', 0);
				$this->pdf->MultiCell(30, 1, number_format($full_tot,2), 'B','R', 0, 0, '', '', '', '', 0);
				$this->pdf->ln();
				$this->pdf->ln();
				$y=0; 
				$tot_pwnint=$tot_rdeem=$full_tot=(float)0;
		
			}

			$this->pdf->setX(15);
			$this->pdf->SetFont('helvetica', 'B', 8);
			$this->pdf->MultiCell(60, 1, "Bill Type : ".$r->billtype, '','L', 0, 1, '', '', '', '', 0);
			$this->pdf->ln();
		}


		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->MultiCell(60, $heigh, $r->bc." - ".$r->name,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,number_format($r->pwning_int,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,number_format($r->redeem,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,number_format($r->total,2),  $bdr, 'R', 0, 1, '', '', true, 0, false, true, 0);
         

		$this->pdf->ln();
	
		$biltype2=$r->billtype;
		$tot_pwnint+=$r->pwning_int;
		$tot_rdeem+=$r->redeem;
		$full_tot+=$r->total;
		$y=1;
		
	}

	$this->pdf->setX(15);
	$this->pdf->SetFont('helvetica', 'B', 8);		
	
	$this->pdf->MultiCell(60, 1, "Total", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, number_format($tot_pwnint,2), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, number_format($tot_rdeem,2), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, number_format($full_tot,2), 'B','R', 0, 0, '', '', '', '', 0);
	

	$this->pdf->Output("PDF.pdf", 'I');



?>