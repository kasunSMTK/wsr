<?php
	
	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	
	
	$this->pdf->SetFont('helvetica', '', 28);		
	$this->pdf->MultiCell(0, 0, "Interest Income Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->SetFont('helvetica', '', 8);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->ln();

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);


	$st = '';
	$fr = false;

	$bc_pawn_int_tot = $bc_redm_int_tot = 0;

	$bc_pawn_int_gra_tot = $bc_redm_int_gra_tot = 0;

	$first_round_passed = false;

	foreach ($list as $r) {

		if ( $st != $r->bc ){

			if ($fr){
				//$this->pdf->ln();
			}else{
				$fr = true;
			}

			$st = $r->bc;

			$h = 4 * (max( 1 ,$this->pdf->getNumLines($r->name,40)));


			if ($first_round_passed){

				$this->pdf->SetFont('helvetica', 'B', 8);	
				$this->pdf->MultiCell(40, 0, "", 			'B','L', 0, 0, '', '', false, '', 0);	
				$this->pdf->MultiCell(30, 0, "", 			'B','C', 0, 0, '', '', false, '', 0);	
				$this->pdf->MultiCell(40, 0, number_format($bc_pawn_int_tot,2), 	'B','R', 0, 0, '', '', false, '', 0);	
				$this->pdf->MultiCell(40, 0, number_format($bc_redm_int_tot,2), 	'B','R', 0, 0, '', '', false, '', 0);	
				$this->pdf->MultiCell(40, 0, number_format(($bc_pawn_int_tot+$bc_redm_int_tot),2), 	'B','R', 0, 1, '', '', false, '', 0);
				$this->pdf->SetFont('helvetica', '', 8);	
				$this->pdf->ln();
				$bc_pawn_int_tot = $bc_redm_int_tot = 0;
			}

			$first_round_passed = true;

			
			$this->pdf->MultiCell(40, 0, "Branch", 				'B','L', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(30, 0, "Bill type", 			'B','C', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(40, 0, "Pawning Interest", 	'B','R', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(40, 0, "Redeem Interest", 	'B','R', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(40, 0, "Total Interest", 		'B','R', 0, 1, '', '', false, '', 0);
			
			$this->pdf->MultiCell(40, $h, $r->bc . ' - ' . $r->name , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(30, $h, $r->billtype , 				$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(40, $h, $r->pwning_int , 				$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(40, $h, $r->redeem_int , 				$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(40, $h, $r->total , 					$border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);

		}else{

			$h = 4 * (max( 1 ,$this->pdf->getNumLines($r->name,40)));
			
			$this->pdf->MultiCell(40, $h, "" , 							$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(30, $h, $r->billtype , 				$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(40, $h, $r->pwning_int , 				$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(40, $h, $r->redeem_int , 				$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$this->pdf->MultiCell(40, $h, $r->total , 					$border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);


		}


		$bc_pawn_int_tot += $r->pwning_int;
		$bc_redm_int_tot += $r->redeem_int;

		$bc_pawn_int_gra_tot += $r->pwning_int;
		$bc_redm_int_gra_tot += $r->redeem_int;

	}


	$this->pdf->SetFont('helvetica', 'B', 8);	
	$this->pdf->MultiCell(40, 0, "", 			'B','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(30, 0, "", 			'B','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(40, 0, number_format($bc_pawn_int_tot,2), 	'B','R', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(40, 0, number_format($bc_redm_int_tot,2), 	'B','R', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(40, 0, number_format(($bc_pawn_int_tot+$bc_redm_int_tot),2), 	'B','R', 0, 1, '', '', false, '', 0);
	$this->pdf->SetFont('helvetica', '', 8);
	$this->pdf->ln(5);


	$this->pdf->SetFont('helvetica', 'B', 8);	
	$this->pdf->MultiCell(40, 0, "", 			'B','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(30, 0, "All Branch Total", 'B','R', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(40, 0, number_format($bc_pawn_int_gra_tot,2), 	'B','R', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(40, 0, number_format($bc_redm_int_gra_tot,2), 	'B','R', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(40, 0, number_format(($bc_pawn_int_gra_tot+$bc_redm_int_gra_tot),2), 	'B','R', 0, 1, '', '', false, '', 0);
	$this->pdf->SetFont('helvetica', '', 8);



	$this->pdf->Output("PDF.pdf", 'I');



?>