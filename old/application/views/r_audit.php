<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

	$(function() {
		$( "#from_date,#to_date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2050"
		});
	});	

</script>

<div class="page_contain_new_pawn">
	
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Audit Reports</span></div>		
	</div>

	<div class="rpt_form">
		<form id="print_report_pdf" action="<?php echo site_url(); ?>/reports/generate_r" method="post" target="_blank">

			<table border="0" cellpassing="0" cellspacing="0">
				<tr>
					<td colspan="2" width="200">
						From Date<div style="padding-top:4px"></div>
						<input name="from_date" id="from_date" class="input_text_regular_new_pawn" style="border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
					</td>
					<td>
						To Date<div style="padding-top:4px"></div>
						<input name="to_date" id="to_date" class="input_text_regular_new_pawn" style="border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
					</td>

					<td style="padding-left:25px;">
						Branch <div style="padding-top:4px"></div>
						<?=$bc_dropdown?>
					</td>

					<td style='padding-left:25px'>Bill Type<div style="padding-top:4px"></div><?=$billtype_dropdown?></td>

					<td width="90"><div style="height: 10px;"></div><label><input type="checkbox" class="chk_au" name="chk_au" value="B"> Audited </label></td>
					
					<td width="105"><div style="height: 10px;"></div><label><input type="checkbox" class="chk_unau" name="chk_unau" value="C"> Unaudited </label></td>

				</tr>
			</table><br>

			<table border="0" cellpassing="0" cellspacing="0">				
				
				<tr>
					<td colspan="4">						
						<div class="opt_holder" style="padding:10px">							
							<label><input type="radio" name="by" value="rpt_internal_audit_article"> Internal Audit Article Report</label>
						</div> 
					</td>
				</tr>
				
			</table>


	        <div style="text-align:left; padding-top: 7px;">
	        	<input type="button" id="btnGenerate" 	src="about:blank" class="btn_regular" value="Generate PDF" style="width:184px;font-family:'bitter'">
	        	<input type="button" id="btnGenerateEx" src="about:blank" class="btn_regular" value="Generate Excel" style="width:183px;font-family:'bitter'">	        	
	        </div>                  
	    </form>
	</div>


	<form id="print_excel_form" method="post" target="_blank">		
		
		<input type="hidden" name="from_date" id="from_date_ex">
		<input type="hidden" name="to_date" id="to_date_ex">
		<input type="hidden" name="chk_au" id="chk_au_ex">
		<input type="hidden" name="chk_unau" id="chk_unau_ex">
		<input type="hidden" name="by" id="by_ex">
		<input type="hidden" name="bc" id="by_bc">

	</form>

	<?php 

	//$IfPDF

	?>

</div>
</body>
</html>