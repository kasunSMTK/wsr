<div class="page_contain">
	<div class="page_main_title"><span>System Default Values</span></div><br>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_setup_pawning_value" id="form_">
		

		<table cellpadding="0" cellspacing="0" border="0" align="center" style="margin:auto">

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:150px">Minimum Advance Value</span>
						<input class="input_text_regular amount" type="text" name="min_advance_amount" id="min_advance_amount" value="<?=$D->min_advance_amount?>" maxlength="300" tabindex="1" style="width: auto;" /> Rs
					</div>
				</td>				
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:150px">Branch Max Cash Balance</span>
						<input class="input_text_regular amount" type="text" name="bc_max_cash_bal" id="bc_max_cash_bal" value="<?=$D->bc_max_cash_bal?>" maxlength="300" tabindex="1" style="width: auto;" /> Rs
					</div>
				</td>				
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:150px">Branch Min Cash Balance</span>
						<input class="input_text_regular amount" type="text" name="bc_min_cash_bal" id="bc_min_cash_bal" value="<?=$D->bc_min_cash_bal?>" maxlength="300" tabindex="1" style="width: auto;" /> Rs
					</div>
				</td>				
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:150px">Daily Pawning Gap</span>
						<input class="input_text_regular amount" type="text" name="daily_pawn_gap" id="daily_pawn_gap" value="<?=$D->daily_pawn_gap?>" maxlength="300" tabindex="1" style="width: auto;" /> hr
					</div>
				</td>				
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:150px">Default Pawning Limit</span>
						<input class="input_text_regular IntVal" type="text" name="default_pawn_limit" id="default_pawn_limit" value="<?=$D->default_pawn_limit?>" maxlength="300" tabindex="1" style="width: auto;text-align: right;" />
					</div>
				</td>				
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:180px">Manager Allowed Extra Amount</span>
						<input class="input_text_regular amount" type="text" name="extra_pawn_manager_allowed_amount" id="extra_pawn_manager_allowed_amount" value="<?=$D->extra_pawn_manager_allowed_amount?>" maxlength="100" tabindex="1" style="width: 245px;" /> Rs
					</div>
				</td>				
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:180px">Top Customer Entering Amount</span>
						<input class="input_text_regular amount" type="text" name="pawner_allow_max" id="pawner_allow_max" value="<?=$D->pawner_allow_max?>" maxlength="100" tabindex="1" style="width: 245px;" /> Rs
					</div>
				</td>				
			</tr>

			<!-- <tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width:180px">Stamp Duty</span>
						<input class="input_text_regular amount" type="text" name="stamp_duty" id="stamp_duty" value="<?=$D->stamp_duty?>" maxlength="100" tabindex="1" style="width: 245px;" /> Rs
					</div>
				</td>				
			</tr>			 -->

			<tr>
				<td><br>
					<input type="button" value="Update" name="btnUpdate" id="btnUpdate" 	class="btn_regular">					
					<input type="hidden" value="1" 		name="hid" 		 id="hid" >
				</td>
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>