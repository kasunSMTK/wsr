<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

	$(function() {
		$( "#from_date,#to_date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2050"
		});
	});	

</script>

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Pawning Reports</span></div>		
	</div>


	<div class="rpt_form">
		<form id="print_report_pdf" action="<?php echo site_url(); ?>/reports/generate_r" method="post" target="_blank">

			<table border="0" cellpassing="0" cellspacing="0">
				<tr>
					<td colspan="2" width="200">
						From Date<div style="padding-top:4px">
						<input name="from_date" id="from_date" class="input_text_regular_new_pawn" style="border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
					</td>
					<td colspan="2">
						To Date<div style="padding-top:4px">
						<input name="to_date" id="to_date" class="input_text_regular_new_pawn" style="border:2px solid Green" readonly="readonly" value="<?=date('Y-m-d')?>">
					</td>
				</tr>
			</table><br>

			<table border="0" cellpassing="0" cellspacing="0">
				<tr>					
					<td colspan="4">						
						<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_pawnings"> Pawnings </div> </label>
					</td>				
				</tr>
				<tr>
					<td colspan="4">
						<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_redeem"> Redeem </div> </label>
					</td>				
				</tr>
				<tr>
					<td colspan="4">
						<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_part_payments"> Advance Payments </div> </label>
					</td>				
				</tr>
				<tr>
					<td colspan="4">
						<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_part_payments_details"> Advance Payments Details </div> </label>
					</td>				
				</tr>
				<tr>
					<td colspan="4">
						<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_daily_cash_report"> Daily Cash Report </div> </label>
					</td>				
				</tr>
				<tr>
					<td colspan="4">						
						<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_forfeited_pawnings">Forfeited Pawnings </div> </label>
					</td>				
				</tr>
				<tr>
					<td colspan="4">						
						<label> <div class="opt_holder"> <input type="radio" name="by" value="rpt_current_stock">Current Stock</div></label>
					</td>				
				</tr>
			</table>


	        <div style="text-align:left; padding-top: 7px;">
	        	<input type="button" id="btnGenerate" src="about:blank" class="btn_regular" value="Generate">	        	
	        </div>                  
	    </form>
	</div>

	<?php 

	//$IfPDF

	?>

</div>
</body>
</html>