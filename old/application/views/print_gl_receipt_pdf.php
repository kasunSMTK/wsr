<?php

	$this->pdf = new TCPDF("P", PDF_UNIT, 'RANARA', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 8);	
	$this->pdf->MultiCell(0, 10, "$RRR->name <br> $RRR->address <br> Tel : $RRR->telno, Fax : $RRR->faxno, Email : $RRR->email ",$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 10, $valign = 'M', $fitcell = false);	
		
	$this->pdf->SetFont('helvetica', 'B', 10);	
	$this->pdf->setY(25);
	$this->pdf->MultiCell(80, 0, "GENERAL RECEIPT", 	$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 8);	

	$this->pdf->MultiCell(36, 0, "No :", 						$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(15, 0," ".$R->nno, 						$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	if ($R->type == "cash"){
		$c_acc_code = "Cash ". $R->cash_acc;
		$c_amount = $R->cash_amount;
	}

	if ($R->type == "cheque"){
		$c_acc_code = "Cheque ". $R->cheque_acc;
		$c_amount = $R->cheque_amount;
	}
		
	$this->pdf->MultiCell(20, 0, "Account", 	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(91, 0, $c_acc_code,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->MultiCell(15, 0, "Type  :", 													$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, $R->type , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	
	$this->pdf->MultiCell(20, 0, "Balance", 	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(91, 0, "0.00", 		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		
	$this->pdf->MultiCell(15, 0, "Date :", 		$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, $R->ddate, 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(20, 0, "Narration", 	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(91, 0, "XXXXXXXX", 	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
	
	$this->pdf->MultiCell(15, 0, "Sub No :", 	$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25, 0, $R->sub_no, 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->MultiCell(20, 0, "Amount", 		$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $c_amount, 	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->ln(5);																																//$this->pdf->MultiCell(40, 0, "Pawn Date", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);$this->pdf->MultiCell(40, 0, $R->ddate, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	$this->pdf->MultiCell(40, 0, "Final Date", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);$this->pdf->MultiCell(40, 0, $R->finaldate, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	$this->pdf->MultiCell(40, 0, "Interest Rate", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);$this->pdf->MultiCell(40, 0, $R->fmintrate, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	$this->pdf->MultiCell(40, 0, "Period", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);$this->pdf->MultiCell(40, 0, $R->period, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	


		$this->pdf->MultiCell(20, 0, "Acc Code", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(40, 0, "Description",	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, "Amount", 		$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(40, 0, "Memo", 		$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$a_tot = 0;

	foreach ($RR as $rr) {
		
		$this->pdf->MultiCell(20, 0, $rr->acc_code, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(40, 0, $rr->description, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, $rr->amount, 		$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(40, 0, "", 				$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$a_tot += $rr->amount;

	}

		$this->pdf->MultiCell(20, 0, "", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(40, 0, "", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, number_format($a_tot,2),$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(40, 0, "", 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
		$this->pdf->ln(5);

	if ($R->type == "cheque"){

		$this->pdf->MultiCell(60, 0, "Bank", 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, "Cheque No",	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, "Amount", 		$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20, 0, "Realized Date",	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		$c_tot = 0;

		foreach ($RRRR as $rrrr) {
			
			$this->pdf->MultiCell(60, 0, $rrrr->description . " " .$rrrr->branch . " " .$rrrr->account, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, $rrrr->cheque_no, 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, $rrrr->amount, 	$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, 0, $rrrr->bank_date, 	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			$c_tot += $rr->amount;

		}

	}

	
	
	$this->pdf->ln(25);

	$this->pdf->MultiCell(75, 0, "...........................................", 		$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(75, 0, "...........................................", 		$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	

	$this->pdf->MultiCell(75, 0, "Prepared By", 		$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '0', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(75, 0, "Pawner", 		$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	
		
		
	

	$this->pdf->Output("partpayment_ticket.pdf", 'I');

	function dd($a){
		return number_format($a,2);
	}

?>