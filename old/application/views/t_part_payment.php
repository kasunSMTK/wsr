<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

	last_max_no = "<?=$max_no?>";

	$(function() {
		$( ".date_ch_allow" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			//minDate: 0,
			yearRange: "1930:2050",
			onSelect: function(date){
				change_current_date(date);
			}	
		});
	});

	$(function() {
		$( ".date_ch_allow_n" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			minDate:'<?=$backdate_upto?>',
			maxDate:0,
			yearRange: "1930:2025",
			onSelect: function(date){
				change_current_date(date);				
			}
		});
	});

	function date_change_response_call(){
		$("#btnReset").click();
	}	

</script>

<div class="page_contain_new_pawn">

	<div class="msg_pop_up_bg_PP"></div>
	<div class="msg_pop_up_sucess_PP">Text Here...</div>

	<div class="page_main_title_new_pawn">
		<div style="width:240px;float:left"><span>Part Payments To Bill</span></div>		
		<div style="float:left;float:right"><input type="text" class="input_text_regular_new_pawn <?=$date_change_allow?>" style="width:80px" id="ddate" value="<?=$current_date?>"></div>		
	</div>

	<form method="post" action="<?=base_url()?>index.php/main/save/t_part_payment" id="form_">
		
		<table border="0" cellpadding="0" cellspacing="0" align="center" >
			<tr>
				<td valign="top" align="left" style="padding-left:20px;border:0px solid red;width:963px">

					<div class="pawn_msg"></div>

					<div class="billType_det_holder">
						<table>
							<tr>								
								<td>
									<!--<div class="text_box_holder_new_pawn" style="width:468px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Bill Type</span>
									</div>-->
								</td>
								<td>
									<table>
									<tr>
										<td>
											<div class="text_box_holder_new_pawn" style="width:100px;border-bottom:none;float:left;">
												<span class="text_box_title_holder_new_pawn">Branch Code</span>
												<input class="input_text_regular_new_pawn txt_bc_no" type="text" id="bc_no" name="bc_no" style="width:90px" value="<?=$bc_no?>" readonly="readonly" maxlength="3">
												<div class="clear_bc_no">X</div>
											</div>
										</td>
										<td>
											<div class="text_box_holder_new_pawn" style="width:204px;border-bottom:none;float:right">
												<span class="text_box_title_holder_new_pawn">Bill Number</span>
												<input class="input_text_regular_new_pawn" type="text" id="billno" name="billno" style="width:197px">
												
												<input type="hidden" id="hid_loan_no" name="hid_loan_no">
												<input type="hidden" id="hid_customer_id" name="hid_customer_id">
												<input type="hidden" id="hid_customer_serno" name="hid_customer_serno">
												<input type="hidden" id="bill_code_desc">
												<input type="hidden" name="billtype" id="billtype">
												<input type="hidden" name="int_paid_untill" id="int_paid_untill">
												<input type="hidden" name="is_weelky_int_cal" id="is_weelky_int_cal">
												<input type="hidden" name="fmintrest" id="fmintrest">
												<input type="hidden" name="num_of_months" id="num_of_months" value="0">
												<input type="hidden" name="time" id="time" value="0">
											</div>
										</td>
										<td>
											<div class="text_box_holder_new_pawn" style="width:204;border-bottom:none;float:right">
												<span class="text_box_title_holder_new_pawn" style="width: 200px">Manual Book Number</span>
												<input class="input_text_regular_new_pawn" type="text" id="manual_bill_no" style="width:197px">												
											</div>
										</td>
									</tr>
									</table>
								</td>																
								<td>
									<div class="text_box_holder_new_pawn" style="width:110px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">No</span>
										<input class="input_text_regular_new_pawn" type="text" id="no" name="no" style="width:65px" value="<?=$max_no?>">
									</div>
								</td>
							</tr>
						</table>
					</div>



					<div class="pp_pawn_det_holder">
						<br>
						<div class="title_div_pp">Pawn</div>

						<table width="100%">
							<tr>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Pawn Date</span>
										<input class="input_text_regular_new_pawn" type="text" id="dDate" name="dDate" style="width:145px" readonly="readonly">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Period</span>
										<input class="input_text_regular_new_pawn" type="text" id="period" name="period" style="width:145px" readonly="readonly">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Final Date</span>
										<input class="input_text_regular_new_pawn" type="text" id="finaldate" name="finaldate" style="width:145px" readonly="readonly">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Loan Amount</span>
										<input class="input_text_regular_new_pawn" type="text" id="loan_amount" name="loan_amount" style="width:145px" readonly="readonly">
									</div>
								</td>

								<td width="100">
									<div class="text_box_holder_new_pawn" style="width:120px;border:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn">Gold Value</span>
										<input class="input_text_regular_new_pawn" type="text" id="goldvalue" name="goldvalue" style="width:110px" readonly="readonly">
									</div>
								</td>
								
								<td width="100%">
									<div class="text_box_holder_new_pawn" style="width:100%;border:1px solid #ccc;border-left:none;border-right:none">
										<span class="text_box_title_holder_new_pawn">Number of Days</span>
										<input class="input_text_regular_new_pawn" type="text" id="no_of_days" name="no_of_days" style="width:100px" readonly="readonly">
									</div>
								</td>
							</tr>
							<!-- <tr>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-top:none">
										<span class="text_box_title_holder_new_pawn">Loan Amount</span>
										<input class="input_text_regular_new_pawn" type="text" id="loan_amount" name="loan_amount" style="width:145px" readonly="readonly">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none;border-top:none">
										<span class="text_box_title_holder_new_pawn">Interest Rate</span>
										<input class="input_text_regular_new_pawn" type="text" id="fmintrate" name="fmintrate" style="width:145px" readonly="readonly">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:156px;border:1px solid #ccc;border-left:none;border-top:none">
										<span class="text_box_title_holder_new_pawn" style="width:150px;">Interest <span style="font-size:11px">(Up to <?=date('Y-m-d')?>)</span></span>
										<input class="input_text_regular_new_pawn" type="text" id="interest" name="interest" style="width:145px" readonly="readonly">
									</div>
								</td>
								<td width="158">
									<div class="text_box_holder_new_pawn" style="width:100%;border-bottom:1px solid #ccc;border-left:none">
										<span class="text_box_title_holder_new_pawn" style="width:150px;">Paid Advance Total</span>
										<input class="input_text_regular_new_pawn" type="text" id="" name="" style="width:145px" readonly="readonly">
									</div>
								</td>
							</tr>	-->					
							
						</table>

					</div>


					<div class="pp_pawn_det_holder" style="border-top:none">						
						<br>
						<div class="title_div_pp">Paid Interest</div> <div class="paid_upto"></div>

						<table width="100%" style="border:0px solid red">

							<tr>
								<td>
									<div style="border-bottom:1px dotted #ccc;height:33px;border:0px solid red;background-color:#f5f5f5">
									<div style="width:50px;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">No</span></div>
									<div style="width:175px;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">Date and Time</span></div>
									<div style="width:145px;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">Discount</span></div>
									<div style="width:145px;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">Paid Amount</span></div>
									<div style="width:100px;float:left" class="text_box_holder_new_pawn"><span class="text_box_title_holder_new_pawn">Paid Branch</span></div>
									</div>	

									<div id="pp_list" style="height: 70px;overflow-y : auto"></div>
								</td>
							</tr>							
							
						</table>

					</div>


					<div class="pp_pawn_det_holder" style="border-top:none">
						
						<div style="border:0px solid red; height:25px;">
							<div class="title_div_pp"  style="float:left">Payment</div>

							<div style="border:0px solid red;float: left; margin-left:142px">
								<label><input type="checkbox" class="cal_5" >Allow interest calculate between 1 to 5 days</label>
							</div>
							
							<div class="msg_int_cal_days" style="border:0px solid red;float: left; margin-left:148px;width:326px; text-align: center; display: none; padding: 5px; background-color: Green;color:#ffffff">
								Last month interest calculate only for <span class="int_chrd_days">10</span> day(s)
							</div>

						</div><br>


						<table width="100%" style="border:0px solid red">							

							<tr>
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:202px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn" style="width:200px">Paid Interest Total</span>
										<input class="input_text_large" type="text" id="paid_interest" name="paid_interest" style="width:190px" readonly="readonly">								
									</div>
								</td>
								
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:202px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Redeem Interest</span>
										<input class="input_text_large" type="text" id="balance" style="width:190px" readonly="readonly">								
									</div>
								</td>

								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:206px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn" style="width:296px">Customer Advance</span>
										<input class="input_text_large" type="text" id="customer_advance" name="customer_advance" style="width:195px" readonly="readonly"> 
										<input type="hidden" id="customer_advance_paying" name="customer_advance_paying">
										<input type="hidden" id="card_number" name="card_number">
										<input type="hidden" id="card_amount" name="card_amount">
										<input type="hidden" id="cash_amount" name="cash_amount">										
									</div>
								</td>

								<td>							
									<div class="text_box_holder_new_pawn" style="width:270px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Paying Amount</span>
										<input class="input_text_large amount" type="text" id="paying_amount" name="paying_amount" style="width:305px">
									</div>
								</td>
								<td width="100%" style="border-bottom:1px solid #ccc"></td>
							</tr>
							
						</table>						

					</div>
					
					<br><br>
					<div class="">
						<input type="button" value="Save" class="btn_regular" id="btnSave">												
						<input type="button" value="Print" class="btn_regular_disable" id="btnPrint">						
						<div style="float:right; padding-right:20px"><input type="button" value="Reset" class="btn_regular_disable" id="btnReset"></div>
						<input type="hidden" name="hid" id="hid" value="0">
						<input type="hidden" name="no_of_int_cal_days" id="no_of_int_cal_days" value="0">
						<input type="hidden" name="is_renew_bill" id="is_renew_bill" value="0">
						
					</div>

				</td>

				<td valign="top" style="border-left:1px solid #ccc;background-color:#fcfcfc;width:250px">

					<div class="div_right_details">
						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Information</a></div>
							<div class="div_new_pawn_info_holder" id="cus_info_div"></div>
						</div>

						<div>							
							<div style="padding:10px"><a class="slider_tab">Bill Type Information</a></div>
							<div class="div_new_pawn_info_holder" id="billtype_info_div"></div>
						</div>

						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Pawn Information</a></div>
							<div class="div_new_pawn_info_holder" id=""></div>
						</div>
					</div>

				</td>
			</tr>
		</table>

	</form>

	<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' 		id='by'  />            
            <input type="hidden" name="r_no" 	id="r_no"  />            
            <input type="hidden" name="r_no_adv" id="r_no_adv" value="0" />
            <input type="hidden" name="r_no_adv_amt" id="r_no_adv_amt" value="0" />            
        </div>                  
    </form>

    <!-- if renewed, issue new loan pawn ticket, new loan interest receipt then, old loan interest receipt -->

    <form id="new_loan_print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' 		id='by' 	class="by1" />            
            <input type="hidden" name="r_no" 	id="r_no" 	class="r_no1"  />  
        </div>                  
    </form>

    <form id="new_loan_first_int_print_pdf" action="<?php echo site_url(); ?>/reports/generate_i" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by_i' id='by_i'		class="by2" />            
            <input type="hidden" name="r_no_i" id="r_no_i" 	class="r_no2" />  
        </div>                  
    </form>
    

</div>
</body>
</html>