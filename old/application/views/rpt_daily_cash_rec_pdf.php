<?php



$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);

$this->pdf->SetPrintHeader(false);

$this->pdf->SetPrintFooter(false);

$this->pdf->AddPage();	



$this->pdf->SetFont('helvetica', '', 28);	

$this->pdf->setY(10);

$this->pdf->setX(0);



$style = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(204, 204, 204));

//$this->pdf->Line(5, 0, 0, 0, $style);



$this->pdf->MultiCell(0, 0, "Daily Cash Reconciliation", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);



$this->pdf->SetFont('helvetica', '', 15);	

$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->setX(0);

$this->pdf->ln();





	

	$this->pdf->SetFont('', '', 7);		
	$this->pdf->SetFont('helvetica', 'B', 8);		
	$this->pdf->SetFont('', '', 7);	

	$heigh = 5;

	$s = '';

	foreach($list as $r){

		if ( $r->bc != $s ){
			$this->pdf->SetFont('helvetica', 'B', 8);	
			$this->pdf->MultiCell(40, 1, "Branch", 'B','l', 0, 0, '', '', false, '', 0);	
			$this->pdf->MultiCell(30, 1, "Date", 'B','C', 0, 0, '', '', false, '', 0);//
			$this->pdf->MultiCell(30, 1, "Book Balance", 'B','R', 0, 0, '', '', false, '', 0);//
			$this->pdf->MultiCell(30, 1, "Physical Balance", 'B','R', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(30, 1, "Different", 'B','R', 0, 0, '', '', false, '', 0);
			$this->pdf->SetFont('helvetica', '', 8);	
			$this->pdf->Ln();

			$this->pdf->MultiCell(40, $heigh,$r->bc ." - ".$r->name,'B', 'L',false, 0, '', '', true, 0, false, true, $heigh,'M' ,false);			
			$s = $r->bc;
		}else{
			$this->pdf->MultiCell(40, $heigh,'','B', 'L',false, 0, '', '', true, 0, false, true, $heigh,'M' ,false);
		}

		$this->pdf->MultiCell(30, $heigh,$r->ddate,'B', 'C',false, 0, '', '', true, 0, false, true, $heigh,'M' ,false);
		$this->pdf->MultiCell(30, $heigh,number_format($r->cash_bal,2),'B', 'R',false, 0, '', '', true, 0, false, true, $heigh,'M' ,false);
		$this->pdf->MultiCell(30, $heigh,number_format($r->physical_bal,2),'B', 'R',false, 0, '', '', true, 0, false, true, $heigh,'M' ,false);
		$this->pdf->MultiCell(30, $heigh,number_format($r->difference,2),'B', 'R',false, 1, '', '', true, 0, false, true, $heigh,'M' ,false);
		$no++;

	}

	$this->pdf->Output("PDF.pdf", 'I');



	?>