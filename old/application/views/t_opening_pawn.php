<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

	$(function() {
		$( "#dDate" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2025",
			onSelect: function(date){
				set_days(date);				
				$("#customer_id").focus().select();
			}
		});
	});

	names 		= [<?=$item_category?>];
	bcCustomer 	= [];	
	gold_q 		= [<?=$gold_q?>];

	function set_days(date){
		$.post("index.php/main/load_data/t_opening_pawn/set_days",{
			d : date
		},function(D){			
			$("#b_days").val(D.days);
		},"json");
	}

</script>

<style type="text/css">
	.ui-autocomplete-loading { background:url('http://www.suddenlink.com/sites/all/themes/suddenlink/images/ajax-loader.gif') no-repeat right center  }	
</style>

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Opening Pawn</span></div>		
	</div>

	<form method="post" action="<?=base_url()?>index.php/main/save/t_opening_pawn" id="form_">
		
		<table border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" style="margin:auto" width="100%">
			<tr>
				<td width="70%" valign="top" align="left" style="padding-left:20px">

					<div class="cus_n_bltyp_holder">
						<table>
							<tr>

								<td>
									<div class="text_box_holder_new_pawn" style="width:154px;border-right:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Date</span>
										<input class="input_text_regular_new_pawn" type="text" id="dDate" name="dDate" style="width:142px" readonly="readonly">
									</div>
								</td>

								<td colspan="2">
									<div class="text_box_holder_new_pawn" style="width:195px; border:none">
										<div style="float:right"><input type="checkbox" id="chkShowSugg"></div>
										<span class="text_box_title_holder_new_pawn">Customer ID</span>
										<input class="input_text_regular_new_pawn" type="text" name="customer_id" id="customer_id" style="width:186px" value="">
										<input type="hidden" name="cus_serno" id="cus_serno" value="">
									</div>
								</td>
								
								<td>
									<div class="text_box_holder_new_pawn" style="width:70px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Bill Type</span>										

										<!-- <input class="input_text_regular_new_pawn" type="text" id="bill_code_desc" style="width:138px"> -->
										
										<select class="select_drp" id="bill_code_desc" style="width:60px">
										<option value="">---</option>
										<?=$billtype_dropdown?>
										</select>

										<input type="hidden" name="billtype" id="billtype">
										<input type="hidden" name="bt_letter" id="bt_letter">

									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:180px;border-bottom:none">
										<span>Bill Number</span><br>
										
										<div style="width:170px;border:0px solid red;margin-top:5px">	
										<input class="opn_bn NumOnly" type="text" id="bc_no"	name="bc_no" 	style="width:35px;margin:-2px;border-radius:4px 0px 0px 4px" readonly="readonly" value="<?=$bc_no?>">
										<input class="opn_bn NumOnly" type="text" id="b_days" 	name="b_days"	style="width:40px;margin:-2px;border-radius:0px 0px 0px 0px" readonly="readonly">										
										<input class="opn_bn NumOnly" type="text" id="bt_no"	name="bt_no"	style="width:40px;margin:-2px;border-radius:0px 0px 0px 0px" readonly="readonly">
										<input class="opn_bn NumOnly" type="text" id="billno" 	name="billno" 	style="width:60px;margin:-2px;border-radius:0px 4px 4px 0px">
										</div>

									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:120px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Int. Rate <div style="float:right"><input type="checkbox" id="allow_full_month" name="allow_full_month" value="1"></div></span>
										<input class="input_text_regular_new_pawn amount" type="text" name ="fMintrate"  id="fMintrate"  style="width: 50px; border: 2px solid Red">
										<input class="input_text_regular_new_pawn amount" type="text" name ="fMintrate2" id="fMintrate2" style="width:50px;border:1px solid #fff">
										<input type="hidden" name="is_pre_intst_chargeable" id="is_pre_intst_chargeable" value="0">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:76px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Period</span>
										<input class="input_text_regular_new_pawn NumOnly" type="text" id="period" name="period" style="width:60px">
									</div>
								</td>								
								<td>
									<div class="text_box_holder_new_pawn" style="width:200px;border-bottom:none; text-align: right">
										<span class="text_box_title_holder_new_pawn" style="width: 232px;">Loan No</span>
										<input class="input_text_regular_new_pawn" type="text" id="loan_no" style="width:243px;border:2px solid Green">
									</div>
								</td>
							</tr>
						</table>
					</div>


					<div id="add_new_cus_front_pawn">
						    
					<div style="margin:20px">						   
						
							<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto;">

								<tr>
									<td>
										<b>Add New Customer</b><br><br>
									</td>
									<td></td>				
								</tr>		

								<tr>									
									
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder" id="SP454">Title</span> 						
											<div style="padding:5px;">
												<label style="margin-right:50px;"><input type="radio" name="title" value="Mrs. " tabindex="1"> Mrs</label>
												<label style="margin-right:50px;"><input type="radio" name="title" value="Miss. "  tabindex="1"> Miss</label>
												<label style="margin-right:00px;"><input type="radio" name="title" value="Mr. "  tabindex="1"> Mr</label>
											</div>						
										</div>										
									</td>

									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Other Names</span> 
											<input class="input_text_regular_pawn_front" type="text" id="other_name" tabindex="8">
										</div>
									</td>

								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<div class="input_title_holder">
												
												<select class="customer_id_dropdown" id="customer_id_type">
													<option value="nic">NIC</option>
													<option value="pp">Passport</option>
													<option value="dl">Drving License</option>
													<option value="gm">Grama Niladari #####</option>
												</select>

											</div> 
											<input class="nic_feild input_text_regular_pawn_front" type="text" id="nicno" tabindex="2">
										</div>
									</td>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Phone No</span> 
											<input class="contct_no_feild input_text_regular_pawn_front" type="text" id="telNo" tabindex="9">
										</div>
									</td>

								</tr>

								<tr>
									<td>										
										<div class="input_div_holder">
											<span class="input_title_holder">Full Name</span> 
											<input class="input_text_regular_pawn_front" type="text" id="cusname" tabindex="3">
										</div>
									</td>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Address 2</span> 
											<input class="input_text_regular_pawn_front" type="text"  id="address2" tabindex="10">
										</div>										
									</td>

								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Address 1</span> 
											<input class="input_text_regular_pawn_front" type="text" id="address" tabindex="4">
										</div>
									</td>			
									<td>					
										
									</td>
								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Mobile No</span> 
											<input class="contct_no_feild input_text_regular_pawn_front" type="text" id="mobile" tabindex="5">
										</div>						
									</td>			
									<td>
									</td>
								</tr>			

								<tr>
									<td>
										<input type="button" value="Save" 	id="btnFrontSave" 			class="btn_regular" tabindex="6">
										<input type="button" value="Cancel" id="btnCancelFrontCusInput"	class="btn_regular" tabindex="7">
										<input type="hidden" value="0" 		id="hid" >
									</td>
								</tr>		
								
							</table>

						</form>
					</div>

				</div>

					<div class="canceled_bill_msg"><div class="msg_txt" style="padding:10px;">This bill was canceled</div></div>

					<div class="loan_dtl_contain">

						<div class="text_box_holder_new_pawn" style="width:155px;float:left">
							<span class="text_box_title_holder_new_pawn">Category</span>
							<input class="input_text_regular_new_pawn" type="text" id="cat_code_desc" style="width:140px" value="">
							<input type="hidden" id="cat_code" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:195px;float:left">
							<span class="text_box_title_holder_new_pawn">Item</span>
							<select class="select_drp" style="width:185px" id="item_code"><option value="">Select Item</option></select>
						</div>

						<div class="text_box_holder_new_pawn" style="width:150px;float:left">
							<span class="text_box_title_holder_new_pawn">Condition</span>
							


							<!-- <input class="input_text_regular_new_pawn" type="text" id="condition_desc" style="width:137px" value=""> -->
							<?=$conditions?>

							<input type="hidden" id="condition" value="">



						</div>

						<div class="text_box_holder_new_pawn" style="width:110px;float:left">
							<span class="text_box_title_holder_new_pawn">Karatage</span>
							<input class="input_text_regular_new_pawn" type="text" id="gold_type_desc" style="width:97px" value="">
							<input type="hidden" id="goldcatagory" value="">
							<input type="hidden" id="gold_rate" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:60px;float:left">
							<span class="text_box_title_holder_new_pawn">Quality</span>
							<input class="input_text_regular_new_pawn" type="text" id="gold_qulty" style="width:47px" value="100">
							<input type="hidden" id="gold_qulty_rate" value="100.00">
							<input type="hidden" id="gold_qulty_code" value="NQ1">							
						</div>

						<div class="text_box_holder_new_pawn" style="width:60px;float:left">
							<span class="text_box_title_holder_new_pawn">Qty</span>
							<input class="input_text_regular_new_pawn NumOnly" type="text" id="qty" style="width:47px" maxlength="5" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:76px;float:left">
							<span class="text_box_title_holder_new_pawn">T.Weight</span>
							<input class="input_text_regular_new_pawn FloatVal3" type="text" id="t_weigth" style="width:63px" maxlength="7" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">P.Weight</span>
							<input class="input_text_regular_new_pawn FloatVal3" type="text" id="p_weigth" style="width:60px" maxlength="7" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:110px;float:left">
							<span class="text_box_title_holder_new_pawn">Value</span>
							<input class="input_text_regular_new_pawn" type="text" id="value" style="width:100px; text-align:right" readonly="readonly" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:50px;float:left">
							<span class="text_box_title_holder_new_pawn">Action</span>
							<input type="button" value="Add" class="btn_regular" style="width:60px" id="btnAddtoList">
						</div>

					</div>

					<div id="item_list_holder"></div>

					<div id="item_list_tot_holder">
						<div class="loan_dtl_contain_det" style="background-color: #f8f8f8">
							<div class="text_box_holder_new_pawn" style="width:155px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:195px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:150px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:110px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:60px;float:left"></div>

							<div class="text_box_holder_new_pawn" style="width:60px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" id="tqty" style="width:47px;background-color:#f8f8f8;font-size:16px; padding:0px;text-align:center;border:none" >								
							</div>

							<div class="text_box_holder_new_pawn" style="width:76px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" id="totalweight" name="totalweight" style="width:63px;background-color:#f8f8f8;font-size:16px; padding:0px;text-align:center;border:none" >								
							</div>

							<div class="text_box_holder_new_pawn" style="width:73px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" id="totalpweight" style="width:60px;background-color:#f8f8f8;font-size:16px; padding:0px;text-align:center;border:none">								
							</div>

							<div class="text_box_holder_new_pawn" style="width:110px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" name="goldvalue" id="goldvalue" style="width:100px;background-color:#f8f8f8; text-align:right;font-size:16px; padding:0px;text-align:right;border:none">								
							</div>

							<div class="text_box_holder_new_pawn" style="width:50px;float:left"></div>								
						</div>
					</div>

					


















					<div style="border:0px solid red; margin-bottom:20px; margin-top:-10px; background-color:#f1f1f1">
						<table width="100%" style="border:0px solid red">
							<tr>
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:240px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Required Amount</span>
										<input class="input_text_large amount" type="text" style="width:230px;border:2px solid green;" id="requiredamount" name="requiredamount">
									</div>
								</td>
								
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:240px;border-bottom:1px solid #ccc">
										<div style="border:0px solid red; width:55px;float:left"><span class="text_box_title_holder_new_pawn">Interest</span></div>
										

										<div style="border:0px solid red; width:140px;float:right;position: absolute;margin-left: 90px; margin-top:-4px" >
											<label>
												
												<div style="float:left" id="divchkh"><input type="checkbox" id="allow_cal_with_advance" name="allow_cal_with_advance"></div>

												<span style="position: absolute;font-size:12px; margin-top:4px">Interest with advance</span>
											</label>
										</div>

										<input class="input_text_large amount" type="text" id="fmintrest" name="fmintrest"  style="width:230px">
									</div>
								</td>
								<td>							
									<div class="text_box_holder_new_pawn" style="width:240px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Stamp Fee</span>
										<input class="input_text_large amount" type="text" style="width:230px" name="stamp_fee" id="stamp_fee">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:340px;border-bottom:1px solid #ccc;background-color:#eaeaea">
										<span class="text_box_title_holder_new_pawn">Payable Amount</span>
										<input class="input_text_large amount" type="text" id="payable_amount" style="width:328px" readonly="readonly">
									</div>
								</td>								
								<td width="100%" style="border-bottom:1px solid #ccc"></td>
							</tr>
							
						</table>
					</div>




























					
					<div class="item_list_btn_holder">
						<input type="button" value="Save" class="btn_regular" id="btnSave">
						<!-- <input type="button" value="Delete" class="btn_regular" id="btnDelete"> -->
						<input type="button" value="Print" class="btn_regular_disable" id="btnPrint">
						<div style="float:right">
							<input type="button" value="Cancel" class="btn_regular_disable" id="btnCancel" disabled="disabled">
							<input type="button" value="Reset" 	class="btn_regular_disable" id="btnReset">
						</div>
						<input type="hidden" name="hid" id="hid" value="0">
						<input type="hidden" id="happid" value="0">
						<input type="hidden" id="is_discount_approved" value="0">
						<input type="hidden" id="discount" value="0">
						<input type="hidden" id="approval_id" name="approval_id" value="0">
						<input type="hidden" id="hid_trans_no" value="">
						<input type="hidden" id="int_cal_changed" name="int_cal_changed" value="0">
						<input type="hidden" id="is_bulk" name="is_bulk" value="0">
					</div>

				</td>

				<td width="30%" valign="top" style="border-left:1px solid #ccc;background-color:#fcfcfc">

					<div class="div_right_details">
						
						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Information</a></div>
							<div class="div_new_pawn_info_holder" id="cus_info_div"></div>
						</div>

						<div>							
							<div style="padding:10px"><a class="slider_tab">Bill Type Information</a></div>
							<div class="div_new_pawn_info_holder" id="billtype_info_div"></div>
						</div>

						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Pawn Information</a></div>
							<div class="div_new_pawn_info_holder" id="pawn_info">

								<div class="cph_c" style="float:left; margin-right:10px; margin-top:10px">
									<span class="cph_txt">Pawnings</span><br>
									<span class="cph_count cph_p" style="font-size:52px"></span>
								</div>
								<div class="cph_c" style="float:left; margin-right:10px; margin-top:10px">
									<span class="cph_txt">Redeems</span><br>
									<span class="cph_count cph_r" style="font-size:52px"></span>
								</div>
								<div class="cph_c" style="float:left; margin-right:10px">
									<span class="cph_txt">Forfiedted</span><br>
									<span class="cph_count cph_f" style="font-size:52px"></span>
								</div>
								<div class="cph_c" style="float:left; margin-right:10px">
									<span class="cph_txt">Canceled</span><br>
									<span class="cph_count cph_cn" style="font-size:52px"></span>
								</div>

							</div>
						</div>
						
					</div>
				</td>
			</tr>
		</table>

	</form>

	<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' id='by' value='pawn_ticket' class="report" />            
            <input type="hidden" class="input_active_num" name="r_no" id="r_no" title="r_no" value=""  />  
        </div>                  
    </form>

    <form id="first_int_print_pdf" action="<?php echo site_url(); ?>/reports/generate_i" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by_i' id='by_i' value='pawn_ticket_first_int' class="report" />            
            <input type="hidden" class="input_active_num" name="r_no_i" id="r_no_i" />  
        </div>                  
    </form>

    

</div>
</body>
</html>