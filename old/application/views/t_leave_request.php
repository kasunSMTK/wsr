<link rel="stylesheet" href="<?=base_url()?>css/report.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>
	$(function() {
		$(".input_date_down").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:''"

		});
	});
</script>
<div class="page_contain">

	<div class="page_main_title"><span>Leave Request</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/t_leave_request" id="form_" enctype="multipart/form-data" >
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">
			<tr> 
				<td style="width:90px;"> 
				</td>
				<td>
					<div class=""> 
						<span class="text_box_title_holder">Ref No</span>
						<input class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px; text-align: right;text-align:right;"type="text" name="ref_no" id="ref_no" value="<?=$max_no?>" maxlength="50">
					</div>
				</td>
			</tr>
			<tr> 
				<td> 
				</td>
				<td>
					<div class=""> 
						<span class="text_box_title_holder">Date</span>
						<input class="input_text_regular_new_pawn input_date_down" value="<?=date('Y-m-d');?>" style="border:1px solid #ccc;width:100px;text-align:right;"  type="text" name="ddate" id="ddate" value="" maxlength="50">
					</div>
				</td>
			</tr>

			<tr>
			</tr>
			<td> 
				<div class="text_box_holder">
					<span class="text_box_title_holder">Status</span>
					<input class="input_text_regular" type="text" name="status" id="status" value="" maxlength="50" style="border:1px solid #eaeaea;border-radius: 4px">
				</div>
			</td>
			<tr>

			</tr>
			<tr>
				<td style="height:20px">
					<div class="text_box_holder" style="height:40px">
						
						<div style="border:0px solid red;width:200px;float:left">
							<span class="text_box_title_holder">Branch </span> 
							<input class="input_text_regular SpRemove" type="text" name="branch_code" id="branch_code" value="<?=$bc?>" readonly="readonly" maxlength="3" maxlength="3" style="width:100px;border:1px solid #eaeaea;border-radius: 4px">
						</div>

						<div style="border:0px solid red;width:230px;float:right">
							<input class="input_text_regular SpRemove" type="text"  id="branch_des"  style="width:225px;border:1px solid #eaeaea;border-radius: 4px" readonly="readonly">
						</div>
					</div>
				</td>							
			</tr>
			<tr>
				<td style="height:20px">
					<div class="text_box_holder" style="height:40px">
						
						<div style="border:0px solid red;width:200px;float:left">
							<span class="text_box_title_holder">Emp No </span> 
							<input class="input_text_regular SpRemove" type="text" name="emp_code" id="emp_code"  style="width:100px;border:1px solid #eaeaea;border-radius: 4px">
						</div>

						<div style="border:0px solid red;width:230px;float:right">
							<input class="input_text_regular SpRemove" type="text"  id="emp_des"  style="width:225px;border:1px solid #eaeaea;border-radius: 4px" readonly="readonly">
						</div>
					</div>
				</td>							
			</tr>
			<tr>
				<td style="height:20px">
					<div class="text_box_holder" style="height:40px">
						
						<div style="border:0px solid red;width:200px;float:left;">
							<span class="">From </span> 
							<input class=" input_text_regular_new_pawn important input_date_down" type="text" value="<?=date('Y-m-d');?>"name="from_date" id="from_date" style="width:150px;text-align: right;">
						</div>

						<div style="border:0px solid red;width:200px;float:right;">
							<span class="">To </span> 
							<input class="input_text_regular_new_pawn input_date_down" type="text" value="<?=date('Y-m-d');?>" name="to_date" id="to_date" style="width:150px;text-align: right;" >
						</div>
					</div>
				</td>							
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Leave Type</span> 
						
						<select id='leave_type' name='leave_type' class='input_ui_dropdown'>
							<option value=''>Select Leave Type</option>
							<option value='1'>Annual</option>
							<option value='2'>Casual</option>
							<option value='3'>Medical </option>
						</select>

					</div>
				</td>							
			</tr>
			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Reason</span> 
						<textarea class="msg_create_textarea" name="reason" id="reason"></textarea>
					</div>
				</td>			
			</tr>
			<tr>
				<td>
					<div class="text_box_holder">
						<label><input type="radio" class='rd' name="leave" id="sh_leave" value="SL"  tabindex="6" >Short Leave</label><br>
					</div>
					<div class="text_box_holder">
						<label><input type="radio" class='rd' name="leave" id="h_day" value="HD"  tabindex="6">Half Day</label><br>
					</div>
					<div class="text_box_holder">
						<label><input type="radio" class='rd' name="leave" id="f_day" value="FD"  tabindex="6">Full Day</label><br>
					</div>
					<div class="text_box_holder">
						<label><input type="radio" class='rd' name="leave" id="du_leave" value="DL"  tabindex="6">Duty Leave</label><br>
					</div>
			</td>
		</tr>
		<tr>
		</tr>			
		<tr>

			<td style="height:20px">
				<div style="margin:top 100px;border: 15px;">
					<input type="button" value="Save" name="btnSave" 	 id="btnSave" 	class="btn_regular">
					<input type="button" class="btn_regular" id="btnDelete1" value="Delete" />					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >

				</div>	
			</td>					
		</tr>		

	</table>

</form>

</div>
</body>
</html>