<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading("Pawning Activity Report");

$r  =  $this->excel->NextRowNum();
$this->excel->getActiveSheet()->setCellValue('A'.$r,"Date From  " .$fd. "  To". $td);

$key    =   $this->excel->NextRowNum();
$n      =   0;
$st     =   "";
$c=$d=$j=0; $billType_tot=(float)0;
$sub_tot[]='';

$lettr = array("B","C","D","E", "F", "G","H", "I","J", "K","L", "M","N", "O","P", "Q","R", "S","T", "U","V", "W","X", "Y","Z", "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",);

foreach ($months as $value) {

    $months=$value->mon."   ".$value->yr;

    $y_mon=$value->mon;
    $y_month[]= $y_mon; 
    $curr_month0=$y_month[0];
    $arr_con=count($y_month);  // count array elenemts   

}
  $arr_size=sizeof($y_month);


$r  =  $this->excel->NextRowNum();

$this->excel->getActiveSheet()->setCellValue('A'.$r,"Bill Type");


for($x =0; $x <=$arr_size; $x++) {
        
   $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,$y_month[$x]."  ".$value->yr);
   $this->excel->SetBorders('A'.$r.":".$lettr[$x].$r);
   $y++; $d=$x;
}
$this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");
$this->excel->SetBlank();


foreach($list as $value){ 
    $curr_month=$value->mon_name;
    $bc1=$value->bc;

    if($bc1==$bc2){

        if( $bill_code!= $value->billtype){

            if($j=="1"){
                $key=$key-1;
                $a=$a+0;
                
                $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
                $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,  number_format($billType_tot,2));
                $billType_tot=0;
                $key++;

            }
            $j=1;

            $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->billtype);
            $b=$key;
            $key++;

            $bill_code= $value->billtype;

            for($x=0; $x<$arr_size; $x++){

                if($y_month[$x] ==$curr_month){
             
                   $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format($value->pwning_int,2));
                    
                    $billType_tot+=$value->pwning_int; 
                   
                    $sub_tot[$x]+=(float)$value->pwning_int;
                     $curr_month=$value->mon_name;
                     $b=$b; //current row value
                     $c=$x+1; //next month row 
                     
                   // $total+=$sub_tot1;
                    $a=$x+1;
                }else{
                    $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2)); 
                     $curr_month=$value->mon_name;
                    $a=$x+1; $b=$b;
                }

            }

              $bill_code= $value->billtype;   
         }else{
        
            // =============same branch same bill types======================================
        if($bill_code== $value->billtype){

             $curr_month=$value->mon_name;
            for($x=$c; $x<$arr_size; $x++){
  
                if($y_month[$x] ==$curr_month){
                    //$this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  $curr_month);
                    $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format($value->pwning_int,2));
                    $sub_tot[$x]+=(float)$value->pwning_int;
                    $curr_month=$value->mon_name;
                    $billType_tot+=$value->pwning_int;

                    $b=$b;
                    $a=$x+1;
                }else{
                   // $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  $value->mon_name."1");
                    // break;
                   // $curr_month=$value->mon_name;
                   // if($y_month[$x] !=$curr_month){
                       //$this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2)); 
                        $curr_month=$value->mon_name;
                        $a=$x+1;
                        $b=$b;

                   // }else{

                       // $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format($value->pwning_int,2));
                       // $sub_tot[$x]+=(float)$value->pwning_int;
                      //  $curr_month=$value->mon_name;
                      //  $billType_tot+=$value->pwning_int;
                      //  $a=$x+1; 
                      //  $b=$b;
                      //   break;
                   // }
                }

            }


        }

            // =============end of same branch same bill types=================================
         }

    }else{

        if($j=="1"){ // ==================row wise total=================
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,  number_format($billType_tot,2));
            $billType_tot=0;
            
            $this->excel->SetBlank();
            $key++; 


             //=======================branch wise total=================================
            //horizonal line- @the end of each branch end
            $this->excel->SetBlank();
            $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"BC",12,"","");
            $this->excel->getActiveSheet()->setCellValue('A'.$key,"Total");
            
            for($x=0; $x<$arr_size; $x++){
                    $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"BC",12,"","");
                    $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,  number_format($sub_tot[$x],2));
                    $total+=$sub_tot[$x];
                    $sub_tot[$x]=0;
                    $a=$x+1; 
            }

            $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  number_format($total,2));
            $total=0;
            $key=$key+2;

              //=======================end of branch wise total=================================
        }
        $j=1;

        $billType_tot=0;
        $this->excel->getActiveSheet()->setCellValue('A'.$key,"Barnch  :  ".$value->bc." - ".$value->name);
        $bc2=$value->bc;
        $this->excel->SetBlank();
        $this->excel->SetBlank();
        $key++;
        
        $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->billtype);
        $this->excel->SetBlank();
        $bill_code= $value->billtype;

        for($x=0; $x<$arr_size; $x++){

            if($y_month[$x] ==$curr_month){

              // $rep_tot+=$value->collection;
            
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,  number_format($value->pwning_int,2));
                $sub_tot[$x]+=(float)$value->pwning_int;
                 $billType_tot+=$value->pwning_int; 
                 $curr_month=$value->mon_name;
                 $b=$key; //current row value
                 $c=$x+1; //next month row 
                //break;
               // $total+=$sub_tot1;
                $a=$x;
            }else{
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,  number_format(0,2)); 
                 $curr_month=$value->mon_name;
                $a=$x; $b=$key;
            }

        }

      $key++; $j=1; 


    }

   // $key++;

}


if($j=="1"){ // ==================row wise total=================
    $key=$key-1;
    $a=$a+0;
    $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,  number_format($billType_tot,2));
     $billType_tot=0;
      $key++;


    //=======================branch wise total=================================
      //horizonal line- @the end of each branch end
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Total");
    
    for($x=0; $x<$arr_size; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"BC",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,  number_format($sub_tot[$x],2));
            $total+=$sub_tot[$x];
            $sub_tot[$x]=0;
            $a=$x+1; 
    }

    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  number_format($total,2));
      $total=0;
      $key=$key+2; 
}
$j=1;


$this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));

?>