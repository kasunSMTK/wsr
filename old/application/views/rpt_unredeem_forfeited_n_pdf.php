<?php

$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->AddPage();	

$this->pdf->SetFont('helvetica', '', 20);	
$this->pdf->setY(10);
$this->pdf->setX(0);

$style = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(204, 204, 204));

$this->pdf->MultiCell(0, 0, "Unredeemed Article to Be Forfeited", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->SetFont('helvetica', 'B', 10);

	$this->pdf->MultiCell(60, 0, 'Branch', $border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, '1-30', $border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, '31-60', $border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, '61-90', $border = '01', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->SetFont('helvetica', '', 10);

foreach($list as $r){

	$this->pdf->MultiCell(60, 0, $r->bc_name, $border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $r->no_of_bills_30, $border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $r->no_of_bills_60, $border = '01', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(40, 0, $r->no_of_bills_90, $border = '01', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

}



$this->pdf->Output("PDF.pdf", 'I');



?>