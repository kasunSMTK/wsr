<div class="page_contain">
	<div class="page_main_title"><span>Company</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_company" id="form_" enctype="multipart/form-data" >
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">
			
			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Name</span> 
						<input class="input_text_regular" type="text" name="name" id="name" value="<?=$company->name?>" maxlength="50">
					</div>
				</td>							
			</tr>
			
			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Address</span> 
						<input class="input_text_regular" type="text" name="address" id="address" value="<?=$company->address?>" maxlength="200">
					</div>
				</td>			
			</tr>			

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Phone No</span> 
						<input class="contct_no_feild input_text_regular NumOnly" type="text" name="telNos" id="telNos"  value="<?=$company->telNos?>"  maxlength="10">
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">FAX No</span> 
						<input class="contct_no_feild input_text_regular NumOnly" type="text" name="faxNos" id="faxNos" value="<?=$company->faxNos?>" maxlength="10">
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Email</span> 
						<input class="email_feild input_text_regular" type="text" name="email" id="email" value="<?=$company->email?>" maxlength="50">
					</div>
				</td>			
			</tr>			

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Logo</span> 
	    				<input type="file" name="image" id="image" style="border:none">
					</div>
				</td>			
			</tr>

			<tr>
				<td>
					<input type="button" value="Update" name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="1" 		name="hid" 		 id="hid" >
				</td>							
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>