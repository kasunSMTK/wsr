<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<!-- t_day_cach_bal -->
<script>

    $(function() {
        $("#bank_date,#fd,#td").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });

    $(function() {
        $( ".date_ch_allow" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2025",
            onSelect: function(date){
                change_current_date(date);              
            }
        });
    });

    $(function() {
        $( ".date_ch_allow_n" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            minDate:'<?=$backdate_upto?>',
            maxDate:0,
            yearRange: "1930:2025",
            onSelect: function(date){
                change_current_date(date);              
            }
        });
    });

</script>

<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:250px;float:left"><span>Daily Cash Balance</span></div>     
    </div><br>

    <form method="post" action="<?=base_url()?>index.php/main/save/t_day_cach_bal" id="form_">

        <table border="0" align="center" cellpadding="0" cellspacing="0" width="900" class="tbl_voucher">

            <tr>
                <td colspan="4" class="div_vou_hin"></td>
            </tr>

            <tr>
                <td width="110"></td>
                <td></td>
                <td width="100" align="right">No</td>
                <td width="100" style="border-right: 1px solid #eaeaea">
                    <input type="text" name="nno" id="nno" value="<?=$get_max_no?>">
                    <input type="hidden" name="hid" id="hid" value="0">                    
                    <input type="hidden" id="hid_type">
                    <input type="hidden" id="time" name="time" value="0">
                    
                </td>
                <td></td>
            </tr>

            <tr>
                <td width="150" style="border-bottom:  0px solid #eaeaea;padding: 0px;font-family: bitter;font-size: 19px;">System Cash</td>
                <td><input type="text" class="text input_text_large amount" id="sys_cash" name="sys_cash" value="<?=$sys_cash?>" style="width: 200px;font-family: bitter;font-size: 22px;" readonly="readonly"></td> 
                <td align="right">Date</td>
                <td style="border-right: 1px solid #eaeaea">

                
                <input class="input_text_regular_new_pawn <?=$date_change_allow?>" type="text" id="date" name="date" style="width:100%" readonly="readonly" value="<?=$current_date?>">


                </td>                
            </tr>
            <tr>
                <td></td>
                <td></td>
                
            </tr>
            
            <tr> <td colspan="2"></td> </tr>

            <tr> <td colspan="2"></td> </tr>

            <tr>
                <td  style="border-bottom:  0px solid #eaeaea;padding: 10px 0px 10px 0px;font-family: bitter;font-size: 19px;">
                Manual Cash</td>                                
                <td><input type="text" class="text input_text_large amount" id="manual_cash" name="manual_cash" style="width: 200px;font-family: bitter;font-size: 22px;"></td>
                <td></td>                
            </tr>

            <tr> <td colspan="5"></td> </tr>
            
            <tr>
                <td style="border-bottom:  0px solid #eaeaea;padding: 10px 0px 10px 0px;font-family: bitter;font-size: 19px;"> Differance </td>                
                 <td><input type="text" class="text input_text_large amount" id="dif_cash" name="dif_cash" style="width: 200px;font-family: bitter;font-size: 22px;" readonly="readonly"></td> <!-- readonly="readonly" -->
            </tr>

             <tr>
                <td style="border-bottom: 0px solid #eaeaea;padding: 10px 0px 10px 0px;font-family: bitter;font-size: 19px;">Account </td>                
                <td>     
                    <table border="0" width="100%" class="voucher_acc_list">
                        <tbody class="voucher_acc_list_tbody">
                        
                        </tbody>
                    </table>

                </td>
            </tr>

            <tr>
                <td colspan="5">
                    
                    <div class="div_app_controls_holder" style="float: left">
                    <input type="button" class="btn_regular"  id="btnSave" value='Save' />
                    <input type="button" class="btn_regular_disable"  id="btnCancel" value='Cancel' />
                    <input type="button" class="btn_regular"  id="btnPrint" value='Print' />
                    </div>

                    <div class="div_app_controls_holder" style="float: right;display: none">

                        <input type="button" class="btn_regular"            id="btnApprove" value='Approve' />
                        <input type="button" class="btn_regular"            id="btnForward" value='Forward' />
                        <input type="button" class="btn_regular"            id="btnReject"  value='Reject' />
                        
                    </div>

                    <input type="button" class="btn_regular" id="btnReset" value='Reset'/>
                </td>                                                
            </tr>
            <tr>
                <td colspan="5" class="receipt_update_msg" style="color: green; padding-top: 10px"></td>
            </tr>

            <tr>
                <td colspan="5"><br><br>
                    <?=$list?>
                </td>
            </tr>
            
        </table>

    </form>





</div>

<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate_r" method="post" target="_blank">
       <input type="hidden" name='by' value='t_day_cach_bal'  class="report">
       <input type="hidden" name='page' value='A5' value="A5" >
       <input type="hidden" name='orientation' value='P' value="P" >
       <input type="hidden" name='type' value='t_day_cach_bal' value="t_day_cach_bal" >
       <input type="hidden" name='recivied'  value=""  id='recivied'>
       <input type="hidden" name='header' value='false' value="false" >
       <input type="hidden" name='qno' value='' title="" id="qno" >

       
       <input type="hidden" name='voucher_type' value='' value="" id="voucher_type">
       <input type="hidden" name='dd' value="<?=date('Y-m-d')?>" id="dd" >
       <input type="hidden" name="sales_type" id="sales_type" value="" value="" >
       <input type="hidden" name='dt' value="" id="dt" >

       <input type="hidden" name='ddate' value="" id="ddate">

       <input type="hidden" name='org_print' title="" value="1" id="org_print">

    </form>


</html>