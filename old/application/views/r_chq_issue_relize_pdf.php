<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->pdf->setPrintHeader($header,$type,$duration);
$this->pdf->setPrintHeader(true,$type);
$this->pdf->setPrintFooter(true);

    //print_r($purchase);
$this->pdf->SetFont('helvetica', 'B', 16);
    $this->pdf->AddPage("P","A4");   // L or P amd page type A4 or A3

    foreach($branch as $ress){
      $this->pdf->headerSet3($company,$ress->name,$ress->address,$ress->tp,$ress->fax,$ress->email);
    }
    $this->pdf->Ln();
    $this->pdf->SetFont('helvetica', 'BUI',10);
    $this->pdf->Cell(0, 6, 'ISSUED CHEQUES',0,false, 'L', 0, '', 0, false, 'M', 'M');
    $this->pdf->Ln();

    $this->pdf->SetFont('helvetica', '',9);
    $this->pdf->Cell(0, 6, 'Date From '. $dfrom.' To '.$dto ,0,false, 'L', 0, '', 0, false, 'M', 'M');

    $this->pdf->Ln(3);

    $border="1";  
    $border1="TBR"; 
    $border2="B";

    $this->pdf->SetX(15);
    $this->pdf->SetFont('helvetica','B',9);



    $border="TB"; $bdr="1";
    $this->pdf->SetX(15);
    $this->pdf->MultiCell(25,6, "Date", $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(35,6, "Account No", $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(25,6, "Cheque No", $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(35,6, "Amount", $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(25,6, "Realize Date", $bdr, 'C', 0, 1, '', '', true, 0, false, true, 0);

    
    $this->pdf->SetFont('helvetica','',9);

    $S = "";

    $TOT = $G_TOT = 0; $X = false;

    foreach ($row as $R) {


      $this->pdf->SetX(15);
      $this->pdf->MultiCell(25,0, $R->date, $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);   
      $this->pdf->MultiCell(35,0, $R->acc_no, $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
      $this->pdf->MultiCell(25,0, $R->cheque_no, $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
      $this->pdf->MultiCell(35,0, number_format($R->amount,2), $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
      $this->pdf->MultiCell(25,0, $R->realize_date, $bdr, 'R', 0, 1, '', '', true, 0, false, true, 0);

          $G_TOT += (float)$R->amount;

    }

   // $this->pdf->ln();
    $this->pdf->SetX(15);
    $this->pdf->SetFont('helvetica','B',9);            
    $this->pdf->MultiCell(85,0, "Total", "0", 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(35,0, number_format($G_TOT,2), "tB", 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(22,0, "", "0", 'L', 0, 1, '', '', true, 0, false, true, 0);
    $this->pdf->SetFont('helvetica','',9);

    $this->pdf->Output("cheques_issue_realize".date('Y-m-d').".pdf", 'I');

    ?>