<?php

	$make_excel = true;

	$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
	$t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';

	$t .= '<tr>';	
	$t .= '<td>Unredeemed Articles Maturity Analysis</td>';
	for($n=0;$n < 13 ;$n++){ $t .= '<td></td>';}
	$t .= '</tr>';	
	
	$t .= '<tr>';	
	$t .= "<td>Between " .$fd . " and " . $td ."</td>";
	for($n=0;$n < 13 ;$n++){ $t .= '<td></td>';}
	$t .= '</tr>';	

	$s_bc = '';

	$first_round_passed = false;

	$A= $B= $C= $D= $E= $F= $G= $H= $I= $J= $K= $L= $M= 0;

	$t .= '<tr>';
	$t .= '<td>'. "Bill Type".'</td>';
	$t .= '<td>'. "1- 30" .'</td>';
	$t .= '<td>'. "31-60" .'</td>';
	$t .= '<td>'. "61-90" .'</td>';
	$t .= '<td>'. "91-120" .'</td>';
	$t .= '<td>'. "121-150" .'</td>';
	$t .= '<td>'. "151-180" .'</td>';
	$t .= '<td>'. "181-210" .'</td>';
	$t .= '<td>'. "211-240" .'</td>';
	$t .= '<td>'. "241-270" .'</td>';
	$t .= '<td>'. "271-300" .'</td>';
	$t .= '<td>'. "301-330" .'</td>';
	$t .= '<td>'. "331-360" .'</td>';
	$t .= '<td>'. "Over 360" .'</td>';
	$t .= '</tr>';
	

	foreach($list as $r){

		if ( $s_bc != $r->bc ){

			if ($first_round_passed){
				
				/*$t .= '<tr>';
				$t .= '<td>'. "Total".'</td>';
				$t .= '<td>'. number_format($A,2).'</td>';
				$t .= '<td>'. number_format($B,2).'</td>';
				$t .= '<td>'. number_format($C,2).'</td>';
				$t .= '<td>'. number_format($D,2).'</td>';
				$t .= '<td>'. number_format($E,2).'</td>';
				$t .= '<td>'. number_format($F,2).'</td>';
				$t .= '<td>'. number_format($G,2).'</td>';
				$t .= '<td>'. number_format($H,2).'</td>';
				$t .= '<td>'. number_format($I,2).'</td>';
				$t .= '<td>'. number_format($J,2).'</td>';
				$t .= '<td>'. number_format($K,2).'</td>';
				$t .= '<td>'. number_format($L,2).'</td>';
				$t .= '<td>'. number_format($M,2).'</td>';
				$t .= '</tr>';*/

				$A= $B= $C= $D= $E= $F= $G= $H= $I= $J= $K= $L=$M= 0;
			}
			
			$first_round_passed = true;			
			
			$t .= '<tr>';
			$t .= '<td>'.$r->bc_name.'</td>';
			for($n=0;$n < 13 ;$n++){ $t .= '<td></td>';}
			$t .= '</tr>';			
			

			/*$t .= '<tr>';
			$t .= '<td>'. "Bill Type".'</td>';
			$t .= '<td>'. "1- 30" .'</td>';
			$t .= '<td>'. "31-60" .'</td>';
			$t .= '<td>'. "61-90" .'</td>';
			$t .= '<td>'. "91-120" .'</td>';
			$t .= '<td>'. "121-150" .'</td>';
			$t .= '<td>'. "151-180" .'</td>';
			$t .= '<td>'. "181-210" .'</td>';
			$t .= '<td>'. "211-240" .'</td>';
			$t .= '<td>'. "241-270" .'</td>';
			$t .= '<td>'. "271-300" .'</td>';
			$t .= '<td>'. "301-330" .'</td>';
			$t .= '<td>'. "331-360" .'</td>';
			$t .= '<td>'. "Over 360" .'</td>';
			$t .= '</tr>';*/
			

			$s_bc = $r->bc;
		}

		$t .= '<tr>';
		$t .='<td>'. $r->billtype .'</td>';
		$t .='<td>'. number_format($r->R30,2).'</td>';
		$t .='<td>'. number_format($r->R60,2).'</td>';
		$t .='<td>'. number_format($r->R90,2).'</td>';
		$t .='<td>'. number_format($r->R120,2).'</td>';
		$t .='<td>'. number_format($r->R150,2).'</td>';
		$t .='<td>'. number_format($r->R180,2).'</td>';
		$t .='<td>'. number_format($r->R210,2).'</td>';
		$t .='<td>'. number_format($r->R240,2).'</td>';
		$t .='<td>'. number_format($r->R270,2).'</td>';
		$t .='<td>'. number_format($r->R300,2).'</td>';
		$t .='<td>'. number_format($r->R330,2).'</td>';
		$t .='<td>'. number_format($r->R360,2).'</td>';
		$t .='<td>'. number_format($r->OVR360,2).'</td>';
		$t .= '</tr>';

		$A += $r->R30;
		$B += $r->R60;
		$C += $r->R90;
		$D += $r->R120;
		$E += $r->R150;
		$F += $r->R180;
		$G += $r->R210;
		$H += $r->R240;
		$I += $r->R270;
		$J += $r->R300;
		$K += $r->R330;
		$L += $r->R360;
		$M += $r->OVR360;



	}


		/*$t .= '<tr>';
		$t .='<td>'. "Total" .'</td>';
		$t .='<td>'. number_format($A,2) .'</td>';
		$t .='<td>'. number_format($B,2) .'</td>';
		$t .='<td>'. number_format($C,2) .'</td>';
		$t .='<td>'. number_format($D,2) .'</td>';
		$t .='<td>'. number_format($E,2) .'</td>';
		$t .='<td>'. number_format($F,2) .'</td>';
		$t .='<td>'. number_format($G,2) .'</td>';
		$t .='<td>'. number_format($H,2) .'</td>';
		$t .='<td>'. number_format($I,2) .'</td>';
		$t .='<td>'. number_format($J,2) .'</td>';
		$t .='<td>'. number_format($K,2) .'</td>';
		$t .='<td>'. number_format($L,2) .'</td>';
		$t .='<td>'. number_format($M,2) .'</td>';
		$t .= '</tr>';
*/
		

	$t .= '</table>';

    if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'excel.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }



?>