<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
/*
$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading(ucfirst($d_range)." Wise N of Transactions");

$r  =  $this->excel->NextRowNum();

$this->excel->getActiveSheet()->setCellValue('A'.$r,"Date From  " .$fd. "  To". $td);

if($b_type!=""){ 
  $this->excel->getActiveSheet()->setCellValue('A'.$r,"Bill Type :  ".$b_type);
}



$key    =   $this->excel->NextRowNum();

$g=$c=$d=$j=$b=0; $branch_tot_P=$branch_tot_R=(float)0; 
$sub_tot_P[]=$sub_tot_R[]='';


$lettr = array("B","D", "F","H","J","L","N","P","R","T","V","X","Z",
               "AB","AD","AF","AH","AJ","AL","AN","AP","AR","AT","AV","AX","AZ",
               "BB","BD","BF","BH","BJ","BL","BN","BP","BR","BT","BV","BX","BZ",
               "CB","CD","CF","CH","CJ","CL","CN","CP","CR","CT","CV","CX","CZ",
               "DB","DD","DF","DH","DJ","DL","DN","DP","DR","DT","DV","DX","DZ",
               "EB","ED","EF","EH","EJ","EL","EN","EP","ER","ET","EV","EX","FZ",
               "FB","FD","FF","FH","FJ","FL","FN","FP","FR","FS","FT","FV","FX","FZ",);


$lettr1 = array("C","E", "G", "I", "K", "M", "O", "Q", "S", "U", "W","Y",
               "AA","AC","AE","AG","AI","AK","AM","AO","AQ","AS","AU","AW","AY",
               "BA","BC","BE","BG","BI","BK","BM","BO","BQ","BS","BU","BW","BY",
               "CA","CC","CE","CG","CI","CK","CM","CO","CQ","CS","CU","CW","CY",
               "DA","DC","DE","DG","DI","DK","DM","DO","DQ","DS","DU","DW","DY",
               "EA","EC","EE","EG","EI","EK","EM","EO","EQ","ES","EU","EW","EY",
               "FA","FC","FE","FG","FI","FK","FM","FO","FQ","FS","FU","FW","FY",);

//=======================================months====================================================
if($d_range=="months"){
    foreach ($months as $value) {

        $months=$value->mon."   ".$value->yr;

        $y_mon=$value->mon;
        $y_month[]= $y_mon; 
        $curr_month0=$y_month[0];
        $arr_con=count($y_month);  // count array elenemts   

    }

    $arr_size=sizeof($y_month);


    $r  =  $this->excel->NextRowNum();

    $this->excel->SetFont('A'.$r.":".'A'.$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");

    $y=0;
    for($x =0; $x <=$arr_size; $x++) {
       $this->excel->SetFont($lettr[$x].$r.":".$lettr[$x].$r,"BC",12,"","");  
       $this->excel->setMerge($lettr[$x].$r.":".$lettr1[$y].$r); 
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,$y_month[$x]."  ".$value->yr);
       
       $d=$x; $y++; 
    }
    $this->excel->setMerge($lettr[$d].$r.":".$lettr[$d].$r); 
    $this->excel->SetFont($lettr[$d].$r.":".$lettr[$d].$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");
    $this->excel->SetBlank();
    $key++;

     for($x =0; $x <=$arr_size; $x++) {
       $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"BC",12,"","");  
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,"Pawning");
       $this->excel->SetFont($lett1[$x].$key.":".$lett1[$x].$key,"BC",12,"","");
       $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,"Redeem");
       $d=$x;
        $y++; 
    }
      $key++;

foreach($list as $value){ 
  $curr_month=$value->mon_name;
  $bc1=$value->bc;

  if($bc1==$bc2){
      for($x=1; $x<$arr_size; $x++){
            if($y_month[$x] ==$curr_month){
               
            if(($value->stats)=="P"){
              $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b, (float)$value->loans);
              $sub_tot_P[$x]+=(float)$value->loans;
              $branch_tot_P+=(float)$value->loans; 
              $g=1;
            }

            if(($value->stats)=="R"){
             
                if($g==0){
                   //$this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2));
                    $branch_tot_P+=0;
                   $sub_tot_P[$x]+=0; 
                   $g=1;

                }
                $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b, (float)$value->loans);
                $sub_tot_R[$x]+=(float)$value->loans;
                $branch_tot_R+=$value->loans;
                 $g=0; 
            }else{
              //$this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b,  number_format(0,2));
               $sub_tot_R[$x]+=0;
              $sub_tot_P[$x]+=0; 
            }
                
               
                
                $curr_month=$value->mon_name;
                $bc2=$value->bc;
                $b=$b; //current row value
                $c=$x+1; //next month row 
            }else{
               
                $curr_month=$value->mon_name;
                $a=$x+1; $b=$b;
                $sub_tot_R[$x]+=0;
                $sub_tot_P[$x]+=0; 
            }
      }
    //$key=$b;
  }else{
    
    //==========================branch wise total===================================
    if($j==1){
        //vertical line- @the end of each branch end
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr1[$d].$key,"B",12,"","");
            //$this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
            $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
            //$sub_tot[$x]=0;
           $branch_tot_P=$branch_tot_R=0;
            $key++; 
    }
    $j=1;
    //==============================================================================

    $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
    $this->excel->SetBlank();
    
      
    for($x=0; $x<$arr_size; $x++){
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,0);
      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,0);  
       if($y_month[$x] ==$curr_month){
            
            if(($value->stats)=="P"){
              $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->loans);
              $sub_tot_P[$x]+=(float)$value->loans;
              $branch_tot_P+=$value->loans; 
              $g=1;
            }

            if(($value->stats)=="R"){
              if($g==0){
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, number_format(0,2));
                $sub_tot_P[$x]+=0;
                $branch_tot_P+=0; 
                $g=1;
              }
                $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, (float)$value->loans);
                $sub_tot_R[$x]+=(float)$value->loans;
                $branch_tot_R+=$value->loans;
                $g=0; 
            }else{
              $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,  number_format(0,2));
              $sub_tot_R[$x]+=0;
              $branch_tot_R+=0; 
            }

            
             
            
            $curr_month=$value->mon_name;
            $bc2=$value->bc;
            
            $c=$x+1; //next month row 
            $b=$key; //current row value


       }else{
          
            $curr_month=$value->mon_name;
            
            $branch_tot_P+=0;
            $branch_tot_R+=0; 
            $sub_tot_R[$x]+=0;
            $sub_tot_P[$x]+=0; 
            $a=$x+1; $b=$b;
       }
    }
    $key++;
  }

  
}
//==========================branch wise total===================================
    if($j==1){
        //vertical line- @ the end of last branch 
        
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
            $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
            //$sub_tot[$x]=0;
           $branch_tot_P=$branch_tot_R=0;
            $key++; 
    }
    $j=1;
//==============================================================================

//==========================Grand Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
    
    for($x=0; $x<$arr_size; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot_P[$x]);
            $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, $sub_tot_R[$x]);
            $total_P+=$sub_tot_P[$x];
            $total_R+=$sub_tot_R[$x];
            $sub_tot_P[$x]=$sub_tot_R[$x]=0;
            $a=$x+1; 
    }

    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total_P);
    $this->excel->getActiveSheet()->setCellValue($lettr1[$a].$key,  $total_R);
    $total=0;
    $key=$key+2;

}

//* - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - 
//=======================================years====================================================

if($d_range=="year"){
  $y=$g=0;
    foreach ($yrs as $value) {

        $years=$value->yr;

        $y_yrs=$value->yr;
        $y_years[]= $y_yrs; 
        
        $arr_con=count($y_years);  // count array elenemts   

    }

    $arr_size=sizeof($y_years);


    $r  =  $this->excel->NextRowNum();

    $this->excel->SetFont('A'.$r.":".'A'.$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");


    for($x =0; $x <=$arr_size; $x++) {
       $this->excel->SetFont($lettr[$x].$r.":".$lettr[$x].$r,"BC",12,"",""); 
       $this->excel->setMerge($lettr[$x].$r.":".$lettr1[$y].$r);   
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,$y_years[$x]);
       
       $y++; $d=$x;
    }

    $key    =   $this->excel->NextRowNum();
     for($x =0; $x <=$arr_size; $x++) {
       $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"BC",12,"","");  
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,"Pawning");
       $this->excel->SetFont($lett1[$x].$key.":".$lett1[$x].$key,"BC",12,"","");
       $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,"Redeem");
       $d=$x;
        $y++; 
    }
      $key++;

    $this->excel->SetFont($lettr[$d].$r.":".$lettr[$d].$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");

    $key++;

    foreach($list as $value){
       $curr_yr=$value->yr;
       $bc1=$value->bc;

        if($bc1==$bc2){

            for($x=0; $x<$arr_size; $x++){
                  
                  if($y_years[$x] ==$value->yr){
                     
                      if(($value->stats)=="P"){
                        $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b, (float)$value->loans);
                        $sub_tot_P[$x]+=(float)$value->loans;
                        $branch_tot_P+=(float)$value->loans; 
                        $g=1;
                      }

                      if(($value->stats)=="R"){

                           if($g==0){
                              $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b, number_format(0,2));
                              $sub_tot_P[$x]+=0;
                              $branch_tot_P+=0; 
                              $g=1;

                              $curr_yr=$value->yr;
                              $bc2=$value->bc;
                            }
                   
                          $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b, (float)$value->loans);
                          $sub_tot_R[$x]+=(float)$value->loans;
                          $branch_tot_R+=$value->loans;
                           $g=1; 
                      }else{
                        $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b,  number_format(0,2));
                      }
                      
                     
                      $curr_yr=$value->yr;
                      $bc2=$value->bc;
                      $b=$b; //current row value
                      $c=$x+1; //next month row
                  }else{
                      $curr_yr=$value->yr;
                      $a=$x+1; $b=$b;
                  }
          }
        }else{
          //==========================branch wise total===================================
          if($j==1){
              //vertical line- @the end of each branch end
                  $key=$key-1;
                  $a=$a+0;
                   $this->excel->SetFont($lettr[$d].$key.":".$lettr1[$d].$key,"B",12,"","");
                  //$this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot);
                  $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
                  $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
                  //$sub_tot[$x]=0;
                 $branch_tot_P=$branch_tot_R=0;
                  $key++; 
          }
          $j=1;

          //==============================================================================

          $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
          $this->excel->SetBlank();

          for($x=0; $x<$arr_size; $x++){
             if($y_years[$x] ==$value->yr){

                  if(($value->stats)=="P"){
                    $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->loans);
                    $sub_tot_P[$x]+=(float)$value->loans;
                    $branch_tot_P+=$value->loans; 

                       $curr_yr=$value->yr;
                       $bc2=$value->bc;
                       $g=1;
                  }

                  if(($value->stats)=="R"){
                    if($g==0){
                      $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, number_format(0,2));
                      $sub_tot_P[$x]+=0;
                      $branch_tot_P+=0; 
                      $g=1;

                      $curr_yr=$value->yr;
                      $bc2=$value->bc;
                    }
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, (float)$value->loans);
                      $sub_tot_R[$x]+=(float)$value->loans;
                      $branch_tot_R+=$value->loans;
                      $g=0; 
                      $curr_yr=$value->yr;
                      $bc2=$value->bc;
                  }else{
                        
                        $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,  number_format(0,2));
                  }
                  
                   
                  $c=$x+1; //next month row 
                  $b=$key; //current row value

              }else{
          
                   $curr_yr=$value->yr;
                  $bc2=$value->bc;
                  $branch_tot_P+=0;
                  $branch_tot_R+=0; 
                  $a=$x+1; $b=$b;
             }


      
          }
            $key++;
        }
    }

    //==========================branch wise total===================================
    if($j==1){
        //vertical line- @ the end of last branch 
      
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr1[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
            $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
            //$sub_tot[$x]=0;
           $branch_tot_P=$branch_tot_R=0;
            $key++; 
    }
    $j=1;
//==============================================================================


//==========================Grand Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
    
    for($x=0; $x<$arr_size; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr1[$x].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot_P[$x]);
            $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, $sub_tot_R[$x]);
            $total_P+=$sub_tot_P[$x];
            $total_R+=$sub_tot_R[$x];
            $sub_tot_P[$x]=$sub_tot_R[$x]=0;
            $a=$x+1; 
    }
    $this->excel->SetFont($lettr[$x].$key.":".$lettr1[$x].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total_P);
    $this->excel->getActiveSheet()->setCellValue($lettr1[$a].$key,  $total_R);
    $total=0;
    $key=$key+2;

}


//=======================================end of years====================================================
//=======================================Quarter====================================================
if($d_range=="quarter"){

  $qtr="";
  foreach ($qtrs as $value) {

        $qtr=$value->Qurter;
    }

    $r  =  $this->excel->NextRowNum();

    $this->excel->SetFont('A'.$r.":".'A'.$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");


    for($x =0; $x <=$qtr; $x++) {
      $a=$x+1;
       $this->excel->SetFont($lettr[$x].$r.":".$lettr[$x].$r,"BC",12,"","");   
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,"Quarter ".$a);
       
       $y++; $d=$x;
    }

     $key    =   $this->excel->NextRowNum();
     for($x =0; $x  <=$qtr; $x++) {
       $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"BC",12,"","");  
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,"Pawning");
       $this->excel->SetFont($lett1[$x].$key.":".$lett1[$x].$key,"BC",12,"","");
       $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,"Redeem");
       $d=$x;
        $y++; 
    }
      $key++;

    $this->excel->SetFont($lettr[$d].$r.":".$lettr[$d].$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");
    $this->excel->SetBlank();
    $key++;

    $g=0;
    foreach($list as $value){
     $curr_qtrs=$value->Qurter;
     $bc1=$value->bc;

      if($bc1==$bc2){

          for($x=0; $x<=$qtr; $x++){
                  $a=$x+1;
                  $q_num='Q'.$a;
                if($q_num ==$curr_qtrs){
           
                    if(($value->stats)=="P"){
                        $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b, (float)$value->loans);
                        $sub_tot_P[$x]+=(float)$value->loans;
                        $branch_tot_P+=(float)$value->loans; 
                       // $g=1;
                      }

                      if(($value->stats)=="R"){

                           if($g==0){
                               $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b, number_format(0,2));
                              $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b, number_format(0,2));
                              $sub_tot_P[$x]+=0;
                              $branch_tot_P+=0; 
                              $g=1;

                             $curr_qtrs=$value->Qurter;
                              $bc2=$value->bc;
                            }
                   
                          $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b, (float)$value->loans);
                          $sub_tot_R[$x]+=(float)$value->loans;
                          $branch_tot_R+=$value->loans;
                          $bc2=$value->bc;
                           $g=1; 
                      }else{
                        $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b,  number_format(0,2));
                         $sub_tot_R[$x]+=(float)0;
                         $branch_tot_R+=0;
                      }
                      
                     
                     $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                      $b=$b; //current row value
                      $c=$x+1; //next month row
                  }
                else{
                    //$this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2));
                    //$this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b,  number_format(0,2));  
                    $curr_qtrs=$value->Qurter;
                    $bc2=$value->bc;
                    $a=$x+1; $b=$b;
                }
          }

      }else{


      //==========================branch wise total===================================
      if($j==1){
          //vertical line- @ the end of last branch 
          
              $key=$key-1;
              $a=$a+0;
               $this->excel->SetFont($lettr[$d].$key.":".$lettr1[$d].$key,"B",12,"","");
              $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
              $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
              //$sub_tot[$x]=0;
             $branch_tot_P=$branch_tot_R=0;
              $key++; 
      }
      $j=1;
      //==============================================================================

         $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
         
         for($x=0; $x<=$qtr; $x++){
            $a=$x+1;
            $q_num='Q'.$a;

            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,  number_format(0,2));
            $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,  number_format(0,2));
               
            if($q_num ==$curr_qtrs){

                  
                if(($value->stats)=="P"){
                    $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->loans);
                    $sub_tot_P[$x]+=(float)$value->loans;
                    $branch_tot_P+=$value->loans; 

                       $curr_qtrs=$value->Qurter;
                       $bc2=$value->bc;
                       $g=1;
                  }

                  if(($value->stats)=="R"){
                    if($g==0){
                      $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, number_format(0,2));
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, number_format(0,2));
                      $sub_tot_P[$x]+=0;
                      $branch_tot_P+=0; 
                      $g=1;

                      $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                    }
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, (float)$value->loans);
                      $sub_tot_R[$x]+=(float)$value->loans;
                      $branch_tot_R+=$value->loans;
                      $g=0; 
                      $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                 
                  }else{
                        
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,  number_format(0,2));
                      $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                        
                  }
                  
                   
                  $c=$x+1; //next month row 
                  $b=$key; //current row value
 
            }
         }
         
        $key++;
  }

}// end of quarters foreach
    

 //==========================branch wise total===================================
      if($j==1){
          //vertical line- @ the end of last branch 
          
              $key=$key-1;
              $a=$a+0;
               $this->excel->SetFont($lettr[$d].$key.":".$lettr1[$d].$key,"B",12,"","");
              $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
              $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
              //$sub_tot[$x]=0;
             $branch_tot_P=$branch_tot_R=0;
              $key++; 
      }
      $j=1;
  //==============================================================================

  //==========================Grand Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
    
    for($x=0; $x<=$qtr; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr1[$x].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot_P[$x]);
            $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, $sub_tot_R[$x]);
            $total_P+=$sub_tot_P[$x];
            $total_R+=$sub_tot_R[$x];
            $sub_tot_P[$x]=$sub_tot_R[$x]=0;
            $a=$x; 
    }
    $this->excel->SetFont($lettr[$x].$key.":".$lettr1[$x].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total_P);
    $this->excel->getActiveSheet()->setCellValue($lettr1[$a].$key,  $total_R);
    $total=0;
    $key=$key+2;




}

//=======================================End of Quarter====================================================

//=======================================Quarter====================================================
if($d_range=="half_year"){
  //assume that year start on 01 of janyary & End on 31 of December

  $num_hy=2; $j=0;

  $r  =  $this->excel->NextRowNum();

  $date = DateTime::createFromFormat("Y-m-d", $fd);
  $day=$date->format("Y");

  $this->excel->SetFont('A'.$r.":".'G'.$r,"BC",12,"","");   
  $this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");
  $this->excel->setMerge("B".$r.":"."C".$r);  
  $this->excel->getActiveSheet()->setCellValue('B'.$r,$day."-01-01 To  ".$day."-06-30 ");
   $this->excel->setMerge("D".$r.":"."E".$r);  
  $this->excel->getActiveSheet()->setCellValue('D'.$r,$day."-07-01 To  ".$day."-12-31 ");  
   $this->excel->setMerge("F".$r.":"."G".$r);  
  $this->excel->getActiveSheet()->setCellValue('F'.$r,"Total");

  $key++;

  $key    =   $this->excel->NextRowNum();
     for($x =0; $x  <=2; $x++) {
       $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"BC",12,"","");  
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,"Pawning");
       $this->excel->SetFont($lett1[$x].$key.":".$lett1[$x].$key,"BC",12,"","");
       $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,"Redeem");
       $d=$x;
        $y++; 
    }
      $key++;


        foreach($list as $value){
           $curr_qtrs=$value->Qurter;
           $bc1=$value->bc;

            if($bc1==$bc2){

              for($x=0; $x<=$num_hy; $x++){
                  $a=$x+1;
                  $q_num='H'.$a;

                if($q_num ==$curr_qtrs){
           
                  if(($value->stats)=="P"){
                        $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b, (float)$value->loans);
                        $sub_tot_P[$x]+=(float)$value->loans;
                        $branch_tot_P+=(float)$value->loans; 
                       // $g=1;
                  }

                  if(($value->stats)=="R"){

                      if($g==0){
                        $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b, number_format(0,2));
                        $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b, number_format(0,2));
                        $sub_tot_P[$x]+=0;
                        $branch_tot_P+=0; 
                        $g=1;

                       $curr_qtrs=$value->Qurter;
                        $bc2=$value->bc;
                      }
                   
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b, (float)$value->loans);
                      $sub_tot_R[$x]+=(float)$value->loans;
                      $branch_tot_R+=$value->loans;
                      $bc2=$value->bc;
                       $g=1; 
                  }else{
                    $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b,  number_format(0,2));
                     $sub_tot_R[$x]+=(float)0;
                     $branch_tot_R+=0;
                  }
                      
                     
                    $curr_qtrs=$value->Qurter;
                    $bc2=$value->bc;
                    $b=$b; //current row value
                    $c=$x+1; //next month row
              }else{
                    //$this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2));
                    //$this->excel->getActiveSheet()->setCellValue($lettr1[$x].$b,  number_format(0,2));  
                    $curr_qtrs=$value->Qurter;
                    $bc2=$value->bc;
                    $a=$x+1; $b=$b;
                    
                    $sub_tot_P[$x]+=0;
                    $$sub_tot_R[$x]+=0;
                }
            }

          }else{


              //==========================branch wise total===================================
                if($j==1){
                    //vertical line- @ the end of last branch 
                 
                        $key=$key-1;
                        $a=$a+0;
                         $this->excel->SetFont($lettr[$d].$key.":".$lettr1[$d].$key,"B",12,"","");
                        $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
                        $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
                        //$sub_tot[$x]=0;
                       $branch_tot_P=$branch_tot_R=0;
                        $key++; 
                }
                $j=1;
              //==============================================================================


              $this->excel->getActiveSheet()->setCellValue('A'.$key,$value->bc." - ".$value->name);
              $this->excel->SetBlank();
              $bc2=$value->bc;

              for($x=0; $x<=$num_hy; $x++){
                 $a=$x+1;
                $q_num='H'.$a;
                 
                 $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key,  number_format(0,2));
                 $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,  number_format(0,2));
            


            if($q_num ==$curr_qtrs){

                  
                if(($value->stats)=="P"){
                    $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->loans);
                    $sub_tot_P[$x]+=(float)$value->loans;
                    $branch_tot_P+=$value->loans; 

                       $curr_qtrs=$value->Qurter;
                       $bc2=$value->bc;
                       $g=1;
                  }

                  if(($value->stats)=="R"){
                    if($g==0){
                      $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, number_format(0,2));
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, number_format(0,2));
                      $sub_tot_P[$x]+=0;
                      $branch_tot_P+=0; 
                      $g=1;

                      $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                    }
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, (float)$value->loans);
                      $sub_tot_R[$x]+=(float)$value->loans;
                      $branch_tot_R+=$value->loans;
                      $g=0; 
                      $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                 
                  }else{
                        
                      $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key,  number_format(0,2));
                      $curr_qtrs=$value->Qurter;
                      $bc2=$value->bc;
                      $sub_tot_P[$x]+=0;
                      $$sub_tot_R[$x]+=0; 
                  }
                  
                   
                  $c=$x+1; //next month row 
                  $b=$key; //current row value
 
            }
              }
              $key++;
        }

      }// end of quarters foreach

      //==========================branch wise total===================================
      if($j==1){
          //vertical line- @ the end of last branch 
          
              $key=$key-1;
              $a=$a+0;
               $this->excel->SetFont($lettr[$d].$key.":".$lettr1[$d].$key,"B",12,"","");
              $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$branch_tot_P);
              $this->excel->getActiveSheet()->setCellValue($lettr1[$d].$key,$branch_tot_R);
              //$sub_tot[$x]=0;
             $branch_tot_P=$branch_tot_R=0;
              $key++; 
      }
      $j=1;
  //==============================================================================

  //==========================Grand Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Grand Total");
    
    for($x=0; $x<=$num_hy; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr1[$x].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot_P[$x]);
            $this->excel->getActiveSheet()->setCellValue($lettr1[$x].$key, $sub_tot_R[$x]);
            $total_P+=$sub_tot_P[$x];
            $total_R+=$sub_tot_R[$x];
            $sub_tot_P[$x]=$sub_tot_R[$x]=0;
            $a=$x; 
    }
    $this->excel->SetFont($lettr[$x].$key.":".$lettr1[$x].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total_P);
    $this->excel->getActiveSheet()->setCellValue($lettr1[$a].$key,  $total_R);
    $total=0;
    $key=$key+2;

}


$this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));
*/
?>