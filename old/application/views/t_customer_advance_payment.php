<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

  $(function() {
    $( ".date_ch_allow" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:2025",
      onSelect: function(date){
        change_current_date(date);        
      }
    });
  });
  
  $(function() {
    $( "#ddate" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      minDate: 0,
      yearRange: "1930:2050",
      onSelect: function(date){
        change_current_date(date);        
      } 
    });
  });

  $(function() {
    $( ".date_ch_allow_n" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      minDate:'<?=$backdate_upto?>',
      maxDate:0,
      yearRange: "1930:2025",
      onSelect: function(date){
        change_current_date(date);        
      }
    });
  });

  bcCustomer  = []; 
  defualt_tr_no = [<?=$max_no?>]; 

  function date_change_response_call(){
    $("#btnReset").click();
  } 

    $( function() {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show all bills" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "ui-state-hover custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
 
    $( "#billno" ).combobox();
    $( "#toggle" ).on( "click", function() {
      $( "#combobox" ).toggle();
    });
  } );



</script>

<style type="text/css">
  
.ui-state-hover{
  color:#ffffff;
}

  .ui-autocomplete-loading { background:url('http://www.suddenlink.com/sites/all/themes/suddenlink/images/ajax-loader.gif') no-repeat right center  } 
  
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;    
    background-color: #eaeaea;
    color: #ffffff;
    border-radius: 0px 4px 4px 0px;
  }
  .custom-combobox-input {
    border: 1px solid Green;
    padding: 4px;
    border-radius: 4px 0px 0px 4px; 
    width: 580px;
  }

</style>

<div class="page_contain_new_pawn">

  <div class="msg_pop_up_bg_PP"></div>
  <div class="msg_pop_up_sucess_PP">Text Here...</div>


  <form method="post" action="<?=base_url()?>index.php/main/save/t_customer_advance_payment" id="form_">
  
    <div class="page_main_title_new_pawn">
      <div style="width:240px;float:left"><span>Customer Advance Payment</span></div>   
      <div style="float:left;float:right"><input type="text" class="input_text_regular_new_pawn <?=$date_change_allow?>" style="width:80px" id="date" name="date" value="<?=$current_date?>"></div>   
    </div>  
    
    <table border="0" cellpadding="1" cellspacing="1" class="tbl_new_pawn" style="margin:auto" width="100%">
      <tr>
        <td width="50%" valign="top" align="left" style="padding-left:20px">

          <div class="pawn_msg"></div>

          <div class="billType_det_holder">
            <table border="0">
              <tr>                
                <td>
                  <div class="text_box_holder_new_pawn" style="width:624px;border-bottom:none">
                    
                    <div style="float: left;border:0px solid red;width:530px;">
                    <span class="text_box_title_holder_new_pawn" style="width:250px">Customer Name / NIC / Passport No</span>
                    <input class="input_text_regular_new_pawn" type="text" id="cus_adv_id" style="width:512px;border:1px solid Green">
                    </div>

                    <div style="float: right;border:0px solid green; width: 70px">
                    <span class="text_box_title_holder_new_pawn">No</span>
                    <input class="input_text_regular_new_pawn" type="text" id="no" name="no" style="width:65px;border:1px solid Green" value="<?=$max_no?>">
                    </div>
                    
                    <input type="hidden" name="client_id" id="client_id">

                  </div>
                </td>               
                <td>
                  <div class="text_box_holder_new_pawn" style="width:110px;border-bottom:none">
                    
                  </div>
                </td>
              </tr>

              <tr>                
                <td>
                  <div class="text_box_holder_new_pawn" style="width:624px;border-bottom:none">
                    <span class="text_box_title_holder_new_pawn" style="width:250px">Customer Bills</span>
                      
                      <div class="ui-widget">
                        
                        <select  style='width:300px;border:1px solid #000000' name='billno' id='billno' class="cus_bills"></select>
                        
                      </div>
                  </div>
                </td>               
                <td>
                  <div class="text_box_holder_new_pawn" style="width:110px;border-bottom:none">
                    <span class="text_box_title_holder_new_pawn"></span>                    
                  </div>
                </td>
              </tr>
            </table>
          </div><br>

          <div class="pp_pawn_det_holder" style="border-top:none">
            
            <div style="border:0px solid red; height:25px;">
              <div class="title_div_pp"  style="float:left">Advance Payment History</div>               
            </div>

            <div class="adv_pay_history">Select customer to get data</div><br>

          </div>





          <div class="pp_pawn_det_holder" style="border-top:none">
            
            <div style="border:0px solid red; height:25px;">
              <div class="title_div_pp"  style="float:left">Payment</div>               
            </div>

            <table width="100%" style="border:0px solid red">

              <tr>
                
                <td style="border:0px solid red">
                  <div class="text_box_holder_new_pawn" style="width:202px;border-bottom:1px solid #ccc">
                    <span class="text_box_title_holder_new_pawn">Paying Amount</span>
                    <input class="input_text_large amount" type="text" id="cr_amount" name="cr_amount" style="width:190px">               
                  </div>
                </td>

                <td style="border:0px solid red">
                  <!-- <div class="text_box_holder_new_pawn" style="width:202px;border-bottom:1px solid #ccc">
                    <span class="text_box_title_holder_new_pawn" style="border:0px solid red;width:190px;">Payable Interest for Selected Bill</span>
                    <input class="input_text_large amount" type="text" id="payable_int" name="payable_int" style="width:190px" readonly="readonly">
                  </div> -->
                </td>

                <td style="border:0px solid red" align="right">
                  <div class="text_box_holder_new_pawn" style="width:290px;border-bottom:1px solid #ccc">                                       
                    <span class="text_box_title_holder_new_pawn" style="width:272px;text-align:right;border:0px solid red">Total Advance Balance</span>
                    <input class="input_text_large amount" type="text" id="adv_bal" style="width:240px" readonly="readonly" disabled="disabled">                
                  </div>
                </td>
                
                <td width="100%" style="border-bottom:1px solid #ccc"></td>
              </tr>
              
            </table>

          </div>
          
          <br><br>
          <div class="">
            <input type="button" value="Save" class="btn_regular" id="btnSave">                       
            <input type="button" value="Print" class="btn_regular_disable" id="btnPrint" disabled>            
            <div style="float:right; padding-right:20px"><input type="button" value="Reset" class="btn_regular_disable" id="btnReset"></div>
            <input type="hidden" name="hid" id="hid" value="0">
            <input type="hidden" name="loanno" id="loanno" value="0">
            <input type="hidden" name="billtype_opt"  id="billtype_opt"  />
            <input type="hidden" name="billtype_selected"   id="billtype_selected"/>
            <input type="hidden" id="payable_int" name="payable_int">
            <input type="hidden" id="selected_bill_amount" value="0">
            <input type="hidden" id="bill_adv_total" value="0">

            
          </div>

        </td>

        <td width="50%" valign="top" style="border-left:1px solid #ccc;background-color:#fcfcfc">

          <div class="div_right_details">
            <div>             
              <div style="padding:10px"><a class="slider_tab">Customer Information</a></div>
              <div class="div_new_pawn_info_holder" id="cus_info_div"></div>
            </div>
            
          </div>

        </td>
      </tr>
    </table>

  </form>

  <form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
          <input type='hidden' name='by'    id='by'  />            
            <input type="hidden" name="r_no"  id="r_no"  />
        </div>                  
    </form>


</div>
</body>
</html>