<?php



	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);

	$this->pdf->SetPrintHeader(false);

	$this->pdf->SetPrintFooter(false);

	$this->pdf->AddPage();	



	$this->pdf->SetFont('helvetica', '', 28);	

	$this->pdf->setX(0);

	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));

	$this->pdf->Line(5, 0, 0, 0, $style);



	$this->pdf->MultiCell(0, 0, "Black Listed Customer List", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	

	$this->pdf->SetFont('helvetica', '', 15);	

	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->setX(0);

	$this->pdf->ln();

	

	$tot_transval=$tot_bal=(float)0;

	$this->pdf->SetFont('helvetica', '', 8);	

	$this->pdf->setX(15);
	
	$this->pdf->MultiCell(40, 1, "Branch", 'B','C', 0, 0, '', '', false, '', 0);	

	$this->pdf->MultiCell(20, 1, "ID No", 'B','L', 0, 0, '', '', false, '', 0);//

	$this->pdf->MultiCell(30, 1, "Customer Name", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(50, 1, "Address", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(40, 1, "No of Transactions", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(40, 1, "Transaction Value", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(40, 1, "Balance Receivable", 'B','C', 0, 1, '', '', false, '', 0);
	//$this->pdf->ln();



	 $bdr='B'; $y=0;
	foreach($list as $r){
		$this->pdf->setX(15);

		

		$heigh = 5 * (	max(1,$this->pdf->getNumLines($r->cusname,30),$this->pdf->getNumLines($r->address,50) ) );

		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->MultiCell(40, $heigh, $r->bc." - ".$r->name,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(20,  $heigh,$r->nicno,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,$r->cusname,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(50,  $heigh, $r->address,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(40,  $heigh,$r->ntranse."  ",  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(40,  $heigh,number_format($r->traseval,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(40,  $heigh,number_format($r->bal,2),  $bdr, 'R', 0, 1, '', '', true, 0, false, true, 0);
         
	
		$tot_transval+=$r->traseval;
		$tot_bal+=$r->bal;
		
	}

	$this->pdf->setX(15);
	$this->pdf->SetFont('helvetica', 'B', 8);		
	
	$this->pdf->MultiCell(140, 1, "", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(40, 1, "Total", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(40, 1, number_format($tot_transval,2), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(40, 1, number_format($tot_bal,2), 'B','R', 0, 0, '', '', '', '', 0);
	

	$this->pdf->Output("PDF.pdf", 'I');



?>