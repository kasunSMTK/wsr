<?php

    
    $make_excel = 1;

    $t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
    $t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';

    $branch_name="";

    //set header -----------------------------------------------------------------------------------------
    
    foreach($branch as $ress){
        $branch_name=$ress->name;        
    }    
    
    $t .= '<tr><td>Trial Balance Reoprt</td></tr>';    
    $t .= "<tr><td>From : ".$dfrom." To ".$dto." </td></tr>";        


    $t .= '<tr>';
    $t .= '<td>Code</td>';
    $t .= '<td>Description</td>';
    $t .= '<td>Dr</td>';
    $t .= '<td>Cr</td>';
    $t .= '</tr>';

    $dr_tot = $cr_tot = 0;    

    foreach($trial_balance as $r){

        $t .= '<tr>';
        $t .= '<td>'.$r->code.'</td>';
        $t .= '<td>'.$r->description.'</td>';
        $t .= '<td>'.number_format($r->Dr_Amount,2).'</td>';
        $t .= '<td>'.number_format($r->Cr_Amount,2).'</td>';
        $t .= '</tr>';

        $dr_tot += $r->Dr_Amount;
        $cr_tot += $r->Cr_Amount;

    }   

    $rt_PnL = $profit_or_loss;


    if ($rt_PnL < 0){
        $rt_PnL_L = $rt_PnL;
        $rt_PnL_R = 0;
        $dr_tot += $rt_PnL_L;
    }else{
        $rt_PnL_L = 0;
        $rt_PnL_R = $rt_PnL;
        $cr_tot += $rt_PnL_R;
    }


    $t .= '<tr>';
    $t .= '<td>'.''.'</td>';
    $t .= '<td>'.'Retained Profit/Loss'.'</td>';
    $t .= '<td>'.number_format($rt_PnL_L,2).'</td>';
    $t .= '<td>'.number_format($rt_PnL_R,2).'</td>';
    $t .= '</tr>';
    

    $t .= '<tr>';
    $t .= '<td>'.''.'</td>';
    $t .= '<td>'.''.'</td>';
    $t .= '<td>'.number_format($dr_tot,2).'</td>';
    $t .= '<td>'.number_format($cr_tot,2).'</td>';
    $t .= '</tr>';

    $t .= '<tr>';
    $t .= '<td>'.''.'</td>';
    $t .= '<td>'.'Diff '. number_format($dr_tot - $cr_tot,2).'</td>';
    $t .= '<td>'.''.'</td>';
    $t .= '<td>'.''.'</td>';
    $t .= '</tr>';


    $t .= '</table>';

    if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'income_statement.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }


    exit;



    $this->pdf->setPrintHeader($header,$type,$duration);
    $this->pdf->setPrintHeader(false,$type);
    $this->pdf->setPrintFooter(true);
    
    $this->pdf->SetFont('helvetica', 'B', 16);
    $this->pdf->AddPage($orientation,$page); 

    $branch_name="";

    //set header -----------------------------------------------------------------------------------------
    
    foreach($branch as $ress){
        $branch_name=$ress->name;        
    }
    
    $this->pdf->SetLineStyle(array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
    
    $this->pdf->setY(22);$this->pdf->SetFont('helvetica', 'B',12);
    $this->pdf->Cell(180, 1,"Trial Balance Reoprt  ",0,false, 'L', 0, '', 0, false, 'M', 'M');
    $this->pdf->Ln();

    $this->pdf->setY(25);$this->pdf->Cell(60, 1,"",'T',0, 'L', 0);$this->pdf->Ln(); 

    $this->pdf->setY(28);$this->pdf->SetFont('helvetica', '', 8);
    $this->pdf->Cell(180, 1, "From : ".$dfrom." To : ".$dto ,0,false, 'L', 0, '', 0, false, 'M', 'M');
    $this->pdf->Ln();

    $this->pdf->SetFont('helvetica', 'B', '9');
    $this->pdf->MultiCell(25, 0,'Code' ,            'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(100, 0,'Description' ,    'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30, 0, 'Dr' ,             'B', 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30, 0, 'Cr' ,             'B', 'R', 0, 1, '', '', true, 0, false, true, 0);

    $this->pdf->SetFont('helvetica', '', '8');

    $dr_tot = $cr_tot = 0;

    $this->pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

    foreach($trial_balance as $r){

        $this->pdf->MultiCell(25, 0, $r->code ,             'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(100, 0, $r->description ,     'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30, 0, number_format($r->Dr_Amount,2) ,        'B', 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30, 0, number_format($r->Cr_Amount,2) ,        'B', 'R', 0, 1, '', '', true, 0, false, true, 0);

        $dr_tot += $r->Dr_Amount;
        $cr_tot += $r->Cr_Amount;

    }   

    $rt_PnL = $profit_or_loss;


    if ($rt_PnL < 0){
        $rt_PnL_L = $rt_PnL;
        $rt_PnL_R = 0;
        $dr_tot += $rt_PnL_L;
    }else{
        $rt_PnL_L = 0;
        $rt_PnL_R = $rt_PnL;
        $cr_tot += $rt_PnL_R;
    }


    $this->pdf->MultiCell(25, 0, '' ,             'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(100, 0, 'Retained Profit/Loss' ,     'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30, 0, number_format($rt_PnL_L,2) ,        'B', 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30, 0, number_format($rt_PnL_R,2) ,        'B', 'R', 0, 1, '', '', true, 0, false, true, 0);    

    

    $this->pdf->SetFont('helvetica', 'B', '9');
    $this->pdf->MultiCell(25, 0,'' ,            'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(100, 0,'' ,    'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30, 0, number_format($dr_tot,2) ,             'B', 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30, 0, number_format($cr_tot,2) ,             'B', 'R', 0, 1, '', '', true, 0, false, true, 0);

    
    $this->pdf->MultiCell(25,   0,'' ,'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(100,  0,'Diff '. number_format($dr_tot - $cr_tot,2) ,'B', 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30,   0,'' ,'B', 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30,   0,'' ,'B', 'R', 0, 1, '', '', true, 0, false, true, 0);


    $this->pdf->Output("TB".date('Y-m-d').".pdf", 'I');


    exit;



















































































































































//----------------------------------------------------------------------------------------------------

$lineRight=204;
$lineLeft=6;

$this->pdf->SetFont('helvetica', 'B', 16);

$y=$this->pdf->GetY();
$this->pdf->line($lineLeft,$y,$lineRight,$y);//160
$this->pdf->SetFont('helvetica', 'B', '10');

$y=$this->pdf->setY(40);
$this->pdf->line(10,$y,100,$y);

$this->pdf->SetX(8);

// $this->pdf->MultiCell(15, 1, "Code", '0', 'R', 0, 0, '', '', 1, 0, 0);
// $this->pdf->MultiCell(70,1, "Description ", '0', 'L', 0, 0, '', '', 1, 0, 0);
// $this->pdf->MultiCell(25, 1, "Dr. Amount ", '0', 'R', 0, 0, '', '', 1, 0, 0);
// $this->pdf->MultiCell(25, 1, "Cr. Amount ", '0', 'R', 0, 0, '', '', 1, 0, 0);
// $this->pdf->MultiCell(5, 1, "", '0', 'R', 0, 0, '', '', 0, 0, 0);
// $this->pdf->MultiCell(25, 1, "Dr. Amount ", '0', 'R', 0, 0, '', '', 1, 0, 0);
// $this->pdf->MultiCell(25, 1, "Cr. Amount ", '0', 'R', 0, 1, '', '', 1, 0, 0);

$y=$this->pdf->GetY();
//$this->pdf->line(10,$y,100,$y);

$y=$this->pdf->GetY();
//$this->pdf->line($lineLeft,$y,$lineRight,$y);


 $t1=0;
 $t11=0;
 $count=0;
 $t2;
 $t22;
 $bal=0;
 $balance=0;
 $b=0;
 $tbal=0;

 $this->pdf->SetFont('helvetica', 'B', '8');


        // $r = array();

        // $b=$r[8];

$this->pdf->MultiCell(15, 5, '', '', '1', 0, 0, '', '', false, '', 0);
$this->pdf->MultiCell(70, 5, '', '', '1', 0, 0, '', '', false, '', 0);        

$this->pdf->MultiCell(30, 5, 'Dr', '1', 'C', 0, 0, '', '', false, '', 0);        
$this->pdf->MultiCell(30, 5, 'Cr', '1', 'R', 0, 0, '', '', false, '', 0);        

$this->pdf->MultiCell(5, 5, '', '#', '1', 0, 1, '', '', false, '', 0);         

//$this->pdf->MultiCell(25, 5, 'Dt : '.$dfrom, '1', 'C', 0, 0, '', '', false, '', 0);        
//$this->pdf->MultiCell(25, 5, $dto, '1', 'C', 0, 1, '', '', false, '', 0);        
        
foreach($trial_balance as $r){

$this->pdf->SetX(9);

$aa = $this->pdf->getNumLines($r->description, 50);
$heigh=5*$aa;

$this->pdf->SetFont('helvetica', '', '9');
$this->pdf->MultiCell(25, $heigh, $r->code,  0, 'L', 0, 0, '', '', true, 0, false, true, 0);
$this->pdf->MultiCell(59, $heigh, $r->description, 0, 'L', 0, 0, '', '', true, 0, false, true, 0);

$balance=$r->dr_Asat - $r->cr_Asat;
    if($balance>0)
    {
    $this->pdf->MultiCell(30, $heigh, d($balance), '1', 'R', 0, 0, '', '', '', '', 0, 0);
    $this->pdf->MultiCell(30, $heigh, d(0), '1', 'R', 0, 0, '', '', '', '', 0, 0);
    $t1 =$balance+$t1;
    }
    else
    {
    $this->pdf->MultiCell(30, $heigh, d(0), '1', 'R', 0, 0, '', '', '', '', 0, 0);
    $this->pdf->MultiCell(30, $heigh, d(-($balance)), '1', 'R', 0, 0, '', '', '', '', 0, 0);
    $t2= -($balance)+$t2;
    }
   
 $this->pdf->MultiCell(5, $heigh, '', '#', 'L', 0, 1, '', '', false, '', 0);   
    
$balance1=$r->dr_range -$r->cr_range;
    /*if($balance1>0)
    {
    $this->pdf->MultiCell(25, $heigh, d($balance1), '1', 'R', 0, 0, '', '', '', '', 0, 0);
    $this->pdf->MultiCell(25, $heigh, d(0), '1', 'R', 0, 1, '', '', '', '', 0, 0);
    $t11 =$balance1+$t11;
    }
    else
    {
    $this->pdf->MultiCell(25, $heigh, d(0), '1', 'R', 0, 0, '', '', '', '', 0, 0);
    $this->pdf->MultiCell(25, $heigh, d(-($balance1)), '1', 'R', 0, 1, '', '', '', '', 0, 0);
    $t22= -($balance1)+$t22;
    }*/

$count=$count+1;
$balance=0;
$balance1=0;
}

 $this->pdf->SetFont('', 'B', '');



$y=$this->pdf->GetY();

$this->pdf->SetFont('', 'B', '9');
$this->pdf->MultiCell(25, 1, "", '0', 'R', 0, 0, '', '', '', '', 0, 0);
$this->pdf->MultiCell(53, 1, "Total", '0', 'C', 0, 0, '', '', '', '', 0, 0);
$this->pdf->MultiCell(30, 1, d($t1), '1', 'R', 0, 0, '', '', '', '', 0, 0);
$this->pdf->MultiCell(30, 1, d($t2), '1', 'R', 0, 0, '', '', '', '', 0, 0);
$this->pdf->MultiCell(5, 1, '', '0', 'R', 0, 1, '', '', '', '', 0, 0);
//$this->pdf->MultiCell(25, 1, d($t11), '1', 'R', 0, 0, '', '', '', '', 0, 0);
//$this->pdf->MultiCell(25, 1, d($t22), '1', 'R', 0, 0, '', '', '', '', 0, 0);


function d($number) {
    return number_format($number, 2, '.', ',');
}
function dd($number,$decimals) {
    return number_format($number, $decimals, '.', ',');
}
    
    $this->pdf->Output("TB".date('Y-m-d').".pdf", 'I');

?>
        


