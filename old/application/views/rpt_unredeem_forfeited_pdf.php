<?php


$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetPrintFooter(false);
$this->pdf->AddPage();	

$this->pdf->SetFont('helvetica', '', 28);	
$this->pdf->setY(10);
$this->pdf->setX(0);

$style = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(204, 204, 204));
$this->pdf->MultiCell(0, 0, "Unredeemed Article to Be Forfeited", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->SetFont('helvetica', '', 15);	
$this->pdf->MultiCell(0, 0, " Date from " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->setX(0);
$this->pdf->ln();


$this->pdf->SetFont('helvetica', 'B', 9);
$this->pdf->MultiCell(40,1, "BC"  , 	$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(25,1, "1-30 Days"  , 	$border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(25,1, "31-60 Days"  , 	$border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(25,1, "61-90"  , 	$border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(25,1, "91-120 Days"  , $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(25,1, "121> Days"  , 	$border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(25,1, "Total"  ,	$border = '1', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->SetFont('helvetica', '', 8);	

$A=$B=$C=$D=$E=0;

foreach ($list as $r) {

	$this->pdf->MultiCell(40,1, $r->bc_name , $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($r->no_of_bills_30,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($r->no_of_bills_60,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($r->no_of_bills_90,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($r->no_of_bills_120,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($r->no_bills_over_120,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format(($r->no_of_bills_30+$r->no_of_bills_60+$r->no_of_bills_90+$r->no_of_bills_120+$r->no_bills_over_120),2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


	$A += $r->no_of_bills_30;
	$B += $r->no_of_bills_60;
	$C += $r->no_of_bills_90;
	$D += $r->no_of_bills_120;
	$E += $r->no_bills_over_120;

}

	$this->pdf->MultiCell(40,1, 'Total' , $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($A,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($B,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($C,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($D,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format($E,2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell(25,1, number_format(($A+$B+$C+$D+$E),2) , $border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->Output("PDF.pdf", 'I');



?>