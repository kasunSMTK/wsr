<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading("Pawning Activity Report");

$r  =  $this->excel->NextRowNum();
$this->excel->getActiveSheet()->setCellValue('A'.$r,"Date From ".$fd." To ".$td);

$r  =  $this->excel->NextRowNum();

$this->excel->getActiveSheet()->setCellValue('A'.$r,"BC/S.No");
$this->excel->getActiveSheet()->setCellValue('B'.$r,"Bill No");
$this->excel->getActiveSheet()->setCellValue('C'.$r,"Pawn Date");
$this->excel->getActiveSheet()->setCellValue('D'.$r,"Cus Name / Address" );
$this->excel->getActiveSheet()->setCellValue('E'.$r,"NIC");
$this->excel->getActiveSheet()->setCellValue('F'.$r,"Mobile");
$this->excel->getActiveSheet()->setCellValue('G'.$r,"Total Weight");
$this->excel->getActiveSheet()->setCellValue('H'.$r,"Articales");
$this->excel->getActiveSheet()->setCellValue('I'.$r,"Status");
$this->excel->getActiveSheet()->setCellValue('J'.$r,"Loan Amount");
$this->excel->getActiveSheet()->setCellValue('K'.$r,"Outstanding Int");
$this->excel->getActiveSheet()->setCellValue('L'.$r,"Re. Amount");
$this->excel->getActiveSheet()->setCellValue('M'.$r,"Re. Date");
$this->excel->getActiveSheet()->setCellValue('N'.$r,"Signature");

$key    =   $this->excel->NextRowNum();
$n      =   0;
$st     =   "";

foreach($list as $row){    

    if ($list[$n]->bc != $st){
        // show
        $bc_name = $list[$n]->bc_name;
        $st = $list[$n]->bc;
        
        $this->excel->getActiveSheet()->setCellValue('A'.$key, $bc_name );
        $key    =   $this->excel->NextRowNum();

    }else{
        $bc_name = "";
    }
    
    $this->excel->getActiveSheet()->setCellValue('A'.$key,($n+1));
    $this->excel->getActiveSheet()->setCellValue('B'.$key,$list[$n]->billno);
    $this->excel->getActiveSheet()->setCellValue('C'.$key,$list[$n]->pawn_date);
    $this->excel->getActiveSheet()->setCellValue('D'.$key,$list[$n]->cusname);
    $this->excel->getActiveSheet()->setCellValue('E'.$key,$list[$n]->nicno);
    $this->excel->getActiveSheet()->setCellValue('F'.$key,$list[$n]->mobile);
    $this->excel->getActiveSheet()->setCellValue('G'.$key,$list[$n]->totalweight);
    $this->excel->getActiveSheet()->setCellValue('H'.$key,$list[$n]->items);
    $this->excel->getActiveSheet()->setCellValue('I'.$key,$list[$n]->status);
    $this->excel->getActiveSheet()->setCellValue('J'.$key,$list[$n]->requiredamount);


    $Q1 = $this->db->query("SELECT C.nicno,C.`customer_id`,L.billtype,L.loanno,L.ddate,L.requiredamount,L.fmintrate,L.fmintrate2,L.period,L.finaldate,L.status,L.old_o_new_billno,C.`customer_id`,L.int_cal_changed,L.cus_serno,L.am_allow_frst_int FROM `t_loan` L JOIN `m_customer` C ON L.`cus_serno` = C.`serno` JOIN `r_bill_type_sum` B ON L.`billtype` = B.`billtype`  AND L.`bc` = B.`bc` WHERE billno = '".$list[$n]->billno."' AND L.bc = '".$list[$n]->bc."' LIMIT 1 ");
    
    $a['loan_sum'] = $Q1->row();

    //int_balance
//=============error occur============= 
    if (is_null($a['loan_sum'])){
        $int_bal = 0;
    }else{
        
        /*$payable_int_bal = $X->calculate->
        interest($r->loanno,$return_mode = "json",$td, $a['loan_sum'] , $five_days_int_cal=0);
        $int_bal = number_format($payable_int_bal['int_balance'],2);*/

        $int_bal = 0;
    }

    $this->excel->getActiveSheet()->setCellValue('K'.$key,$int_bal);
    $this->excel->getActiveSheet()->setCellValue('L'.$key,$list[$n]->redeem_amount);
    $this->excel->getActiveSheet()->setCellValue('M'.$key,$list[$n]->redeem_date);
    $this->excel->getActiveSheet()->setCellValue('N'.$key,"");
    
    
    $key++;
    $n++;

}

$this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));

?>