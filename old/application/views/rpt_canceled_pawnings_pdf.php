<?php



	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);

	$this->pdf->SetPrintHeader(false);

	$this->pdf->SetPrintFooter(false);

	$this->pdf->AddPage();	



	$this->pdf->SetFont('helvetica', '', 28);	

	$this->pdf->setY(10);

	$this->pdf->setX(0);

	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));

	$this->pdf->Line(5, 0, 0, 0, $style);



	$this->pdf->MultiCell(0, 0, "Cancel Pawning Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	

	$this->pdf->SetFont('helvetica', '', 15);	

	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->setX(0);

	$this->pdf->ln();

	



	$this->pdf->SetFont('helvetica', '', 8);	

	$this->pdf->MultiCell(10, 1, "S.No", 'B','C', 0, 0, '', '', false, '', 0);	

	$this->pdf->MultiCell(13, 1, "Bill Type", 'B','L', 0, 0, '', '', false, '', 0);//

	$this->pdf->MultiCell(25, 1, "Bill No", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(20, 1, "Pawn Date", 'B','C', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(74, 1,"Cus Name / Address", 'B','L', 0, 0, '', '', false, '', 0);

	$this->pdf->MultiCell(20, 1, "NIC", 'B','L', 0, 0, '', '', '', '', 0);

	$this->pdf->MultiCell(15, 1, "T.Weight", 'B','C', 0, 0, '', '', '', '', 0);

	$this->pdf->MultiCell(55, 1, "Articales", 'B','L', 0, 0, '', '', '', '', 0);

	$this->pdf->MultiCell(20, 1, "Loan Amount", 'B','R', 0, 0, '', '', '', '', 0);

	$this->pdf->MultiCell(25, 1, "Cancel By", 'B','R', 0, 1, '', '', '', '', 0);


	$this->pdf->SetFont('', '', 7);

	



	$no = 1;
	$tot_amt = $TWeight = 0;
	$sub_tot_loop_start = false;
	$first_r = false;
	$sub_tot_tw = $sub_tot_la = 0;


	foreach($list as $r){

		if ($r->bc != $st){
			
			if (sub_tot_loop_start){
				if ($first_r){
					
					$this->pdf->SetFont('helvetica', 'B', 8);		
					$this->pdf->MultiCell(145, 1,"", 'B','L', 0, 0, '', '', false, '', 0);
					$this->pdf->MultiCell(17, 1, "Total", 'B','L', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(15, 1, d3($sub_tot_tw), 'B','C', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(55, 1, "", 'B','L', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(20, 1, d($sub_tot_la), 'B','R', 0, 1, '', '', '', '', 0);

					$sub_tot_tw = $sub_tot_la = 0;
				}

				$first_r = true;
			}

			$sub_tot_tw += $r->totalweight;					
			$sub_tot_la += $r->requiredamount;
			
			
			$bc_name = $r->bc_name;
			$st 	 = $r->bc;
			$sub_tot_loop_start = false;

			$this->pdf->SetFont('helvetica', 'B', 8);	
			$this->pdf->MultiCell(0,5, $bc_name, $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);		

		}else{
			$sub_tot_tw += $r->totalweight;					
			$sub_tot_la += $r->requiredamount;

			$bc_name = "";
			$sub_tot_loop_start = true;
		}

		$this->pdf->SetFont('', '', 8);

		$h = 4 * (max(1,$this->pdf->getNumLines($r->bc_name,25),$this->pdf->getNumLines($r->items,55),$this->pdf->getNumLines($r->cusname,74)));
		$this->pdf->MultiCell(10, $h, $no , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(13, $h, $r->billtype , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(25, $h, $r->billno , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->ddate , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(74,$h, $r->cusname. " - " . $r->address , $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->nicno , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(15, $h, d3($r->totalweight) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(55, $h, $r->items , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, d($r->requiredamount) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(25, $h, $r->display_name , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);

		$no++;

		$tot_amt += $r->requiredamount;
		$TWeight += $r->totalweight;

	}
	

	$this->pdf->SetFont('helvetica', 'B', 8);		
	$this->pdf->MultiCell(145, 1,"", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(17, 1, "Total", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($sub_tot_tw), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(55, 1, "", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($sub_tot_la), 'B','R', 0, 1, '', '', '', '', 0);



	






























	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}





	$this->pdf->Output("PDF.pdf", 'I');



?>