<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $("#date,#bank_date,#fd,#td").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });

</script>

<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:250px;float:left"><span>Salary Vouchers</span></div>     
    </div><br>

    <form method="post" action="<?=base_url()?>index.php/main/save/t_print_salary_voucher" id="form_">

        <table border="0" class="tbl_voucher_print" align="center">
            <thead>
                <tr>
                    <td>BC</td>
                    <td>Voucher No</td>
                    <td>Paid From</td>
                    <td>Paid To</td>                    
                    <td align="right">Amount</td>
                    <td>Status</td>
                    <!-- <td align="center"><input type='checkbox' class='chk_all_vou_prints' style='width:20px;height:20px'></td> -->
                    <td><!-- <input type='button' value='Print All' class='btn_vou_print_all btn_regular'> --></td>
                </tr>
            </thead>
            <tbody class="tbl_vou_print_list"><?=$vou_print_list?></tbody>
        </table>

    </form>
   
    <form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
            <input type='hidden' name='by' id='by'  />            
            <input type="hidden" name="bc" id="bc"  />
            <input type="hidden" name="r_nno" id="nno"  />
            <input type="hidden" name="r_paid_acc" id="paid_acc"  />



           <input type="hidden" name='page' value='A4' value="A4" >
           <input type="hidden" name='orientation' value='P' value="P" >
           <input type="hidden" name='type' value='t_voucher' value="t_voucher" >
           <input type="hidden" name='recivied'  value=""  id='recivied'>
           <input type="hidden" name='header' value='false' value="false" >
           
           <input type="hidden" name='r_ddate'  value="" id="r_ddate">
           
           <input type="hidden" name='h_v_bc' id="h_v_bc" value="">

           
           <input type="hidden" name='voucher_type' value='' value="" id="voucher_type">
           <input type="hidden" name='dd' value="<?=date('Y-m-d')?>" id="dd" >
           <input type="hidden" name="sales_type" id="sales_type" value="" value="" >
           <input type="hidden" name='dt' value="" id="dt" >
           <input type="hidden" name='supp_id' value="" id="supp_id" >
           <input type="hidden" name='p_hid_nno' value="" id="p_hid_nno">
           <input type="hidden" name='voucher_no' value="" id="voucher_no">
           <input type="hidden" name='category_id' value="" id="category_id">
           <input type="hidden" name='cat_des' value="" id="cat_des">
           <input type="hidden" name='group_id' value="" id="group_id">
           <input type="hidden" name='group_des' value="" id="group_des">
           <input type="hidden" name='ddate' value="" id="ddate">
           <input type="hidden" name='tot' value="" id="tot">
           <input type="hidden" name='acc_code' value="" id="acc_code">
           <input type="hidden" name='acc_des' value="" id="acc_des">
           <input type="hidden" name='vou_des' value='' value="" id="vou_des">
           <input type="hidden" name='org_print' title="" value="1" id="org_print">

        </div>                  
    </form>


</div>

</html>