<?php

    $make_excel = 1;

    $t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
    $t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';

    $t .= '<tr><td> Given Period Pawning Analysis </td></tr>';
    $t .= '<tr><td> Date from '.$fd.' to '.$td.' </td></tr>';    
    
    $c          =   $d=$j=$b=0; $billType_tot=(float)0; 
    $sub_tot[]  =   '';

    

    
    $t.= '<tr>';
    $t.= '<td>Branch</td>';
    $t.= '<td>Pawning Advances</td>';
    $t.= '<td>Net Weight</td>';
    $t.= '<td>Rate of Advances</td>';
    $t.= '</tr>';
    
    $x = 0;

    $A = $B = $C = 0;

    foreach($list as $rec ) {        
        
        $t.= '<tr>';
        $t.='<td>'. $rec->name  . '</td>';
        $t.='<td>'. d($rec->A) . '</td>';
        $t.='<td>'. d($rec->B) . '</td>';
        $t.='<td>'. d(($rec->A / $rec->B) * 8 ) . '</td>';
        $t.= '</tr>';

        $A += $rec->A;
        $B += $rec->B;
        $C += (($rec->A / $rec->B) * 8 );

    }    


    $t.= '<tr>';
    $t.='<td><b>Total</b></td>';
    $t.='<td><b>'. d($A) . '</b></td>';
    $t.='<td><b>'. d($B) . '</b></td>';
    $t.='<td><b>'. d($C) . '</b></td>';
    $t.= '</tr>';


    $t .= '</table>';



    if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'Redeem_Periodical_Summary.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }


    function d($a){
        return number_format($a,2);
    }

?>