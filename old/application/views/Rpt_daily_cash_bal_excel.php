<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading("Daily Cash Balance Report");

$r  =  $this->excel->NextRowNum();
//  $this->excel->getActiveSheet()->setCellValue('A'.$r,"Date From ".$fd." To ".$td);

$r  =  $this->excel->NextRowNum();

$this->excel->getActiveSheet()->setCellValue('A'.$r,"Branch");
$this->excel->getActiveSheet()->setCellValue('B'.$r,"Opening Balance");
$this->excel->getActiveSheet()->setCellValue('C'.$r,"Receipts");
$this->excel->getActiveSheet()->setCellValue('D'.$r,"Payments");
$this->excel->getActiveSheet()->setCellValue('E'.$r,"Closing Balance" );

$key    =   $this->excel->NextRowNum();
$n      =   0;
$st     =   "";

foreach($list as $row){    

    /*if ($list[$n]->bc != $st){
        // show
        $bc_name = $list[$n]->bc_name;
        $st = $list[$n]->bc;
        
        $this->excel->getActiveSheet()->setCellValue('A'.$key, $bc_name );
        $key    =   $this->excel->NextRowNum();

    }else{
        $bc_name = "";
    }*/
    
    $this->excel->getActiveSheet()->setCellValue('A'.$key,$list[$n]->bc);
    $this->excel->getActiveSheet()->setCellValue('B'.$key,$list[$n]->op_bal);
    $this->excel->getActiveSheet()->setCellValue('C'.$key,$list[$n]->dr_tot);
    $this->excel->getActiveSheet()->setCellValue('D'.$key,$list[$n]->cr_tot);
    $this->excel->getActiveSheet()->setCellValue('E'.$key, ($list[$n]->op_bal + $list[$n]->dr_tot) - $list[$n]->cr_tot );
    
    
    $key++;
    $n++;

}

$this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));

?>