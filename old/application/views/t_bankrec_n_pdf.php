<?php

		$this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);
        $this->pdf->AddPage("P","A4");
		$this->pdf->setY(10);
		$this->pdf->setX(10);

		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->MultiCell(0,  0, "USS",	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(0,  0, "Bank Reconciliation Statement",	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->MultiCell(0,  0, "Period Ending as at ".$rec_to_date,	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);


		$this->pdf->ln(15);

		$this->pdf->MultiCell(30,  0, "Ref No ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(100,  0, " : ".$sum[0]->no,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		$this->pdf->MultiCell(30,  0, "Bank Name ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(100,  0, " : ".$bank_acc_name,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		
		$this->pdf->MultiCell(30,  0, "Currency ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(100,  0, " : LKR",	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		$this->pdf->ln(10);

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->MultiCell(120,  0, "Balance as Per Cash Book",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  0, "",	$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  0, d($cash_book_bal),	$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		$this->pdf->ln(5);

		













		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->MultiCell(0,  0, "Add:",	$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(0,  0, "Unpresented Cheque",	$border = 'BLR', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		$this->pdf->MultiCell(30,	0, "Date",	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,	0, "Cheque No",	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(60,	0, "Description",	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,	0, "Amount",	$border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30, 	0, "",	$border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		$this->pdf->SetFont('helvetica', '', 8);

		$tot_cr = 0;

		foreach ($add_less_rows as $r) {

			$h = 4 * (	max(1,$this->pdf->getNumLines($r->description,60)) );
			
			if ( $r->cr > 0 ){

				$this->pdf->MultiCell(30,$h, $r->trans_date,	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(30,$h, $r->cheque_no,	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(60,$h, $r->description,	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(30,$h, d($r->cr),	$border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(30,$h, "",	$border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$tot_cr += $r->cr;
			}
		}
				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->MultiCell(30,	0, '',	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30,	0, '',	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(60,	0, '',	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30,	0, d($tot_cr),	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30, 	0, d($tot_cr),	$border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
				$this->pdf->SetFont('helvetica', '', 8);

			
		














		$this->pdf->ln(5);
		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->MultiCell(0,  0, "Less:",	$border = '01', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(0,  0, "Unrealized Deposit",	$border = 'BLR', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

		$this->pdf->MultiCell(30,	0, "Date",	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,	0, "Ref No",	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(60,	0, "Description",	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,	0, "Amount",	$border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30, 	0, "",	$border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);		
		$this->pdf->SetFont('helvetica', '', 8);

		$tot_dr = 0;

		foreach ($add_less_rows as $r) {

			$h = 4 * (	max(1,$this->pdf->getNumLines($r->description,60)) );
			
			if ( $r->dr > 0 ){

				$this->pdf->MultiCell(30,$h, $r->trans_date,	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(30,$h, $r->ref_no,	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(60,$h, $r->description,	$border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(30,$h, d($r->dr),	$border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$this->pdf->MultiCell(30,$h, "",	$border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
				$tot_dr += $r->dr;
			}
		}
				$this->pdf->SetFont('helvetica', 'B', 8);
				$this->pdf->MultiCell(30,	0, '',	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30,	0, '',	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(60,	0, '',	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30,	0, d($tot_dr),	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30, 	0, d($tot_dr),	$border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
				$this->pdf->SetFont('helvetica', '', 8);

				// $this->pdf->SetFont('helvetica', 'B', 8);
				// $this->pdf->MultiCell(30,	0, '',	$border = '01', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				// $this->pdf->MultiCell(90,	0, 'balance',	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				// $this->pdf->MultiCell(30,	0, '',	$border = '01', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				// $this->pdf->MultiCell(30, 	0, d( ($cash_book_bal + $tot_dr) - $tot_cr )  ,	$border = '01', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
				// $this->pdf->SetFont('helvetica', '', 8);

		$this->pdf->ln(5);

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->MultiCell(120,  0, "Bank Statement Balance",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  0, "",	$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(30,  0, d($statment_closing_bal),	$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);








		$this->pdf->Output("bank_rec.pdf", 'I');


	function d($s){
		return number_format($s,2);
	}



?>