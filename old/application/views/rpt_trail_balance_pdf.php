<?php

	$this->pdf = new TCPDF("l", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 22);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Outstanding Stock - $barnch", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "As at " .$td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();	

	$this->pdf->SetFont('helvetica', 'B', 8);		

	$this->pdf->MultiCell(15, 1, "P.Date", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Bill No", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(50, 1, "Customer Name", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(17, 1, "I.D No", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(42, 1, "Item", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(15, 1, "Weight", 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "P.Amount", 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(15, 1, "P.Interest", 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(10, 1, "Initial", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "R.Amount", 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(16, 1, "R.Interest", 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(16, 1, "R.Date", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(10, 1, "Initial", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(17, 1, "OutS.Stock", 'B','R', 0, 1, '', '', false, '', 0);

	$this->pdf->SetFont('', '', 7);
	$no = 1;

	$w = $pa = $ra = $os = $pi = $ri = 0;

	foreach($list as $r){	

		$h = 5 * (	max(1,$this->pdf->getNumLines($r->item,42),$this->pdf->getNumLines($r->name,50) ) );

		$this->pdf->MultiCell(15, $h, $r->p_date , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->billno , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(50, 1,  $r->name, 'B','L', 0, 0, '', '', false, '', 0);
		$this->pdf->MultiCell(17, $h, $r->nicno , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(42, $h, $r->item , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(15, $h, $r->weight , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$this->pdf->MultiCell(20, $h, $r->p_amount , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(15, $h, $r->p_interest , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$this->pdf->MultiCell(10, $h, "" , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$this->pdf->MultiCell(20, $h, $r->r_amount , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(16, $h, $r->r_interest , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);		
		
		$this->pdf->MultiCell(16, $h, $r->r_date , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(10, $h, "" , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(17, $h, $r->cus_stock , $border='B', $align='R', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);

		$no++;

		$w  += $r->weight;
		$pa += $r->p_amount;
		$ra += $r->r_amount;
		$os += $r->cus_stock;
		$pi += $r->p_interest;
		$ri += $r->r_interest;
	
	}

	$this->pdf->SetFont('', 'B', 7);

	$this->pdf->MultiCell(15, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(50, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(17, 1, "", 'B','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(42, 1, "Total", 'B','R', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(15, 1, number_format($w,3,".",""), 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, number_format($pa,2,".",","), 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(15, 1, number_format($pi,2,".",","), 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(10, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, number_format($ra,2,".",","), 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(16, 1, number_format($ri,2,".",","), 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(16, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(10, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(17, 1, number_format($os,3,".",""), 'B','R', 0, 1, '', '', false, '', 0);






































	



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}


	$this->pdf->Output("PDF.pdf", 'I');

?>