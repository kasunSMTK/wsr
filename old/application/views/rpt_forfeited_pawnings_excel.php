<?php

	$make_excel = 1;

	$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
   	$t .= '<table border="0" class="tbl_income_statment" cellspacing="0" cellpadding="0" borderColor="#eaeaea">';
   	
   	$t .= "<tr>";
	$t .= "<td colspan='14'><h3>Forfeited Report</h3></td>";
	$t .= "</tr>";
	
	$t .= "<tr>";
	$t .= "<td colspan='14'>Between $fd to $td <br><br></td>";
	$t .= "</tr>";	
	

	
	$no = 1;
	$sb = $bcs = $bts = '';
	$btQty = $bt_tot = $btMrktp = $btAdvRsvd = $btINT = $btOthrCost = 0;

	foreach($list as $r){		

		if ( $bcs != $r->bc){

			// Totals---------------------------------

			if ($first_round_passed){
				
				if ($bt_tot <> 0){
					
					$t .="<tr>";
					$t .="<td>". '' . "</td>";
					$t .="<td>". '' . "</td>";
					$t .="<td>". '' . "</td>";
					$t .="<td><b>". 'Total' . "</b></td>";
					$t .="<td align='center'><b>". $btQty . "</b></td>";
					$t .="<td align='right'><b>". d3($bt_tot) . "</b></td>";
					$t .="<td align='right'><b>". d($btMrktp) . "</b></td>";
					$t .="<td align='right'><b>". d($btAdvRsvd) . "</b></td>";
					$t .="<td align='right'><b>". d($btINT) . "</b></td>";
					$t .="<td align='right'><b>". d($btOthrCost) . "</b></td>";
					$t .="<td>". '' . "</td>";
					$t .="<td>". '' . "</td>";
					$t .="<td>". '' . "</td>";
					$t .="<td>". '' . "</td>";
					$t .="</tr>";					
					
					$btQty = $bt_tot = $btMrktp = $btAdvRsvd = $btINT = $btOthrCost = 0;

				}
				
			}

			$first_round_passed = true;

			// Totals----------------------------------

			$t .= "<tr>";
			$t .= "<td colspan='14'><b>".$r->bc."</b></td>";
			$t .= "</tr>";
			
			$bcs = $r->bc;
			$bts = '';

		}


		if ($r->billtype != $bts){


			// Totals---------------------------------

			if ($first_round_passed){

				if ($bt_tot <> 0){
				
					$t .= "<tr>";
					$t .= "<td><b>". '' . "</b></td>";
					$t .= "<td><b>". '' . "</b></td>";
					$t .= "<td><b>". '' . "</b></td>";
					$t .= "<td><b>". 'Total' . "</b></td>";
					$t .= "<td align='center'><b>". $btQty . "</b></td>";
					$t .= "<td align='center'><b>". d3($bt_tot) . "</b></td>";
					$t .= "<td align='right'><b>". d($btMrktp) . "</b></td>";
					$t .= "<td align='right'><b>". d($btAdvRsvd) . "</b></td>";
					$t .= "<td align='right'><b>". d($btINT) . "</b></td>";
					$t .= "<td align='right'><b>". d($btOthrCost) . "</b></td>";
					$t .= "<td><b>". '' . "</b></td>";
					$t .= "<td><b>". '' . "</b></td>";
					$t .= "<td><b>". '' . "</b></td>";
					$t .= "<td><b>". '' . "</b></td>";
					$t .= "</tr>";
					
					$btQty = $bt_tot = $btMrktp = $btAdvRsvd = $btINT = $btOthrCost = 0;

				}
				
			}

			$first_round_passed = true;

			// Totals----------------------------------


			$t .= "<tr>";			
			$t .= "<td colspan='14'>".$r->billtype."</td>";
			$t .= "</tr>";

			$t .= "<tr>";			
			$t .= "<td><b>". "Sno"."</b></td>";
			$t .= "<td><b>". "Bill No"."</b></td>";
			$t .= "<td><b>". "Date"."</b></td>";
			$t .= "<td><b>". "Description"."</b></td>";
			$t .= "<td><b>". "Qty"."</b></td>";
			$t .= "<td><b>". "Weight"."</b></td>";
			$t .= "<td><b>". "Market Price"."</b></td>";
			$t .= "<td><b>". "Advance Received"."</b></td>";
			$t .= "<td><b>". "Interest Up to Date"."</b></td>";
			$t .= "<td><b>". "Other Cost"."</b></td>";
			$t .= "<td><b>". "F.Date"."</b></td>";
			$t .= "<td><b>". "Checked by H/O"."</b></td>";
			$t .= "<td><b>". "Remarks"."</b></td>";
			$t .= "<td><b>". "Nod"."</b></td>";
			$t .= "</tr>";
			
			$bts = $r->billtype;
		}



		if ($sb != $r->billno){
			
			$t .= "<tr>";
			$t .= "<td>". $no ."</td>";
			$t .= "<td>". $r->billno ."</td>";
			$t .= "<td>". $r->pawn_date ."</td>";
			$t .= "<td>". $r->itemname ."</td>";
			$t .= "<td align='center'>". $r->qty ."</td>";
			$t .= "<td align='center'>". d3($r->pure_weight) ."</td>";
			$t .= "<td align='right'>". d($r->market_price) ."</td>";
			$t .= "<td align='right'>". d($r->advance_received) ."</td>";
			$t .= "<td align='right'>". d($r->int_up_to_date) ."</td>";
			$t .= "<td align='right'>". $r->other_cost ."</td>";
			$t .= "<td>". $r->fdate ."</td>";
			$t .= "<td>". "" ."</td>";
			$t .= "<td>". "" ."</td>";
			$t .= "<td>". $r->no_of_days ."</td>";
			$t .= "</tr>";

			$sb = $r->billno;

			$btAdvRsvd += $r->advance_received;
			$btINT += $r->int_up_to_date;
			$btOthrCost += $r->other_cost;

			$no++;

		}else{

			$t .= "<tr>";
			$t .= "<td>". ''."</td>";
			$t .= "<td>". ''."</td>";
			$t .= "<td>". ''."</td>";
			$t .= "<td>". $r->itemname ."</td>";
			$t .= "<td align='center'>". $r->qty ."</td>";
			$t .= "<td align='center'>". d3($r->pure_weight) ."</td>";
			$t .= "<td align='right'>". d($r->market_price) ."</td>";
			$t .= "<td>". '' ."</td>";
			$t .= "<td>". '' ."</td>";
			$t .= "<td>". '' ."</td>";
			$t .= "<td>". "" ."</td>";
			$t .= "<td>". "" ."</td>";
			$t .= "<td>". "" ."</td>";
			$t .= "<td>". "" ."</td>";
			$t .= "</tr>";

		}


		$btQty += $r->qty;
		$bt_tot += $r->pure_weight;		
		$btMrktp += $r->market_price;



		
	}

	// Totals---------------------------------

	if ($first_round_passed){
		
		$t .= "<tr>";
		$t .= "<td>". '' ."</td>";
		$t .= "<td>". '' ."</td>";
		$t .= "<td>". '' ."</td>";
		$t .= "<td><b>". 'Total'."</b></td>";
		$t .= "<td align='center'><b>". $btQty."</b></td>";
		$t .= "<td align='center'><b>". d3($bt_tot)."</b></td>";
		$t .= "<td align='right'><b>". d($btMrktp)."</b></td>";
		$t .= "<td align='right'><b>". d($btAdvRsvd)."</b></td>";
		$t .= "<td align='right'><b>". d($btINT)."</b></td>";
		$t .= "<td align='right'><b>". d($btOthrCost)."</b></td>";
		$t .= "<td>". '' . "</td>";
		$t .= "<td>". '' . "</td>";
		$t .= "<td>". '' . "</td>";
		$t .= "<td>". '' . "</td>";
		$t .= "</tr>";		
		
	}

	$t .= "</table>";

	$first_round_passed = true;

	// Totals----------------------------------



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}


	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'income_statement.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

?>