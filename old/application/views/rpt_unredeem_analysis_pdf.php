<?php
	
	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setX(0);	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	$this->pdf->MultiCell(0, 0, "Unredeem Artical Balance Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();

	
	$tot_pwnad=$tot_nt_weight=$full_tot=(float)0;
	$this->pdf->SetFont('helvetica', '', 8);	
	$this->pdf->setX(15);	
	$this->pdf->MultiCell(60, 1, "Branch", 'B','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(30, 1, "Unredeem Balance", 'B','R', 0, 0, '', '', false, '', 0);//
	$this->pdf->MultiCell(30, 1, "Net Weight", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Rate of Advances", 'B','R', 0, 1, '', '', false, '', 0);

	//$this->pdf->ln();

	$heigh=1; $bdr='B'; $y=0;
	foreach($list as $r){
		$this->pdf->setX(15);
		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->MultiCell(60, $heigh, $r->bc." - ".$r->name,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,number_format($r->oc,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,number_format($r->total_stock,2),  $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,number_format($r->roa,2),  $bdr, 'R', 0, 1, '', '', true, 0, false, true, 0);
         

		$this->pdf->ln();
	
		//$biltype2=$r->billtype;
		$tot_pwnad+=$r->oc;
		$tot_nt_weight+=$r->total_stock;
		$full_tot+=$r->roa;
		//$y=1;
		
	}

	$this->pdf->setX(15);
	$this->pdf->SetFont('helvetica', 'B', 8);		
	
	$this->pdf->MultiCell(60, 1, "Total", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, number_format($tot_pwnad,2), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, number_format($tot_nt_weight,2), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, number_format($full_tot,2), 'B','R', 0, 0, '', '', '', '', 0);
	

	$this->pdf->Output("PDF.pdf", 'I');



?>