<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'SEW', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetAutoPageBreak(TRUE, 0);
	$this->pdf->AddPage();

	// ----------- header --------------------------				
		$Q = $this->db->query("SELECT C.`name` 	AS `company_name`, B.name 		AS `bc_name`,B.bc_no AS `bc_code`, B.address 	AS `bc_address`, B.telno 	AS `bc_tel`, B.faxno 	AS `bc_fax`, B.email 	AS `bc_email` FROM `m_company` C JOIN (SELECT * FROM `m_branches`) B ON B.bc = '$bc'LIMIT 1 ")->row();
		if ($is_reprint == 0){$header_custom_data['title'] = "Advance Payment Receipt"; }else{$header_custom_data['title'] = "Pawn Ticket (Re-Print)"; }
		$this->pdf->sew_header_no_com_name($Q,$header_custom_data);
	// ----------- end header ----------------------

	$this->pdf->SetFont('helvetica', '', 9);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

	$title_w = 20;
	$title_wL= 110;
	$title_wR= 40;

	// New Format

	// Name
	$this->pdf->MultiCell($title_w,  0, "Customer: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL-1, 0, $R->cusname,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// Pawn Date
	$this->pdf->MultiCell($title_w+1,  0, "Pawn Date: ",	$border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, $R->ddate,$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	
	// Address
	$this->pdf->MultiCell($title_w,  0, "Address: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->address,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// Bill No
	$this->pdf->SetFont('helvetica', 'B', 12);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
	$this->pdf->MultiCell($title_w,  0, "Bill No: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, substr($R->billno,0,3). "   ".substr($R->billno,3,50), $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 9);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));


	// NIC
	$this->pdf->MultiCell($title_w,  0, "NIC No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->nicno,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w+1,  0, "Paid Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, $RR->ddate, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	


	// Contact
	$this->pdf->MultiCell($title_w,  0, "Contact No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $R->mobile,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	// Maturity Date
	$this->pdf->MultiCell($title_w+10,  0, "Maturity Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, $R->finaldate, $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	


	// Operator
	$this->pdf->MultiCell($title_w,  0, "Operator",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $user_des,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "Receipt No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, $RR->trans_no, $border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	


	$this->pdf->ln();$this->pdf->ln();

	// Advanced amount
	$this->pdf->SetFont('helvetica', 'B', 11);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

	$this->pdf->MultiCell($title_w+20,  0, "Advanced amount: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $RR->amount,$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, "", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$amount_in_word_1 = $this->utility->convert_number_to_words($RR->amount);
	$this->pdf->SetFont('helvetica', '', 8);	

	$this->pdf->MultiCell($title_w+20,  0, " (". substr($amount_in_word_1, 0,1) . strtolower(substr($amount_in_word_1, 1,10000)).")",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, "",$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, "", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	// Total Advance Paid
	$this->pdf->SetFont('helvetica', 'B', 11);	
	$this->pdf->MultiCell($title_w+20,  0, "Total Advance Paid: ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wL, 0, $RR->amount_tot,$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_w,  0, "",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->MultiCell($title_wR, 0, "", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);	

	$this->pdf->SetFont('helvetica', '', 9);	
	$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));


	$this->pdf->ln();$this->pdf->ln();

	// Valuer
	$this->pdf->MultiCell($title_w+29,  0, "--------------------------------",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	// Authorized officer
	$this->pdf->MultiCell($title_w+29,  0, "--------------------------------",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	// Pawner
	$this->pdf->MultiCell($title_w+29,  0, "--------------------------------",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	// Cashier
	$this->pdf->MultiCell($title_w+29,  0, "--------------------------------",	$border = '0', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	// Valuer
	$this->pdf->MultiCell($title_w+29,  0, "Valuer",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	// Authorized officer
	$this->pdf->MultiCell($title_w+29,  0, "Authorized officer",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	// Pawner
	$this->pdf->MultiCell($title_w+29,  0, "Pawner",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	// Cashier
	$this->pdf->MultiCell($title_w+29,  0, "Cashier",	$border = '0', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);




	$this->pdf->Output("partpayment_ticket.pdf", 'I');

?>