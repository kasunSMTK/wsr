<?php
	
	$make_excel = true;

		//$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
        $t = '<table border="01" class="tbl_income_statment" cellspacing="1" cellpadding="1">';
        
        $t .= '<thead>';            

        $t .= '<tr>';            
        $t .= '<td colspan="5"><h1>Interest Income Report</h1></td>';
        $t .= '</tr>';

        $t .= '<tr>';            
        $t .= '<td colspan="5">Between '.$fd.' and '.$td.'<br><br></td>';
        $t .= '</tr>';		

		$st = '';
		$fr = false;


		$bc_pawn_int_tot = $bc_redm_int_tot = 0;

		$bc_pawn_int_gra_tot = $bc_redm_int_gra_tot = 0;

		$first_round_passed = false;

		foreach ($list as $r) {

			if ( $st != $r->bc ){

				if ($fr){
					//$this->pdf->ln();
				}else{
					$fr = true;
				}

				$st = $r->bc;			


				if ($first_round_passed){

					$t .= '<tr>';
					$t .= '<td></td>';	
					$t .= '<td><b>Branch Total</b></td>';	
					$t .= '<td><b>'. number_format($bc_pawn_int_tot,2).'</b></td>';
					$t .= '<td><b>'. number_format($bc_redm_int_tot,2).'</b></td>';
					$t .= '<td><b>'. number_format(($bc_pawn_int_tot+$bc_redm_int_tot),2).'</b></td>';
					$t .= '</tr>';

					$t .= '<tr>';
					$t .= '<td colspan="5"><br></td>';	
					$t .= '</tr>';
					
					$bc_pawn_int_tot = $bc_redm_int_tot = 0;
				}

				$first_round_passed = true;

				$t .= '<tr>';
				$t .="<td>Branch</td>";
				$t .="<td>Bill Yype</td>";
				$t .="<td>Pawning Interest</td>";
				$t .="<td>Redeem Interest</td>";
				$t .="<td>Total Interest</td>";
				$t .= '</tr>';

				$t .= '<tr>';				
				$t .="<td>".$r->bc."</td>";

				if ($r->billtype == "M"){
					$t .="<td><font color='#3377ff'><b>".$r->billtype."</b></font></td>";					
				}else{
					$t .="<td>".$r->billtype."</td>";					
				}


				$t .="<td>".$r->pwning_int."</td>";
				$t .="<td>".$r->redeem_int."</td>";
				$t .="<td>".$r->total."</td>";
				$t .= '</tr>';

			}else{

				
				$t .= '<tr>';
				$t .= "<td></td>";
				$t .= "<td>".$r->billtype."</td>";
				$t .= "<td>".$r->pwning_int."</td>";
				$t .= "<td>".$r->redeem_int."</td>";
				$t .= "<td>".$r->total."</td>";
				$t .= '</tr>';

			}


			$bc_pawn_int_tot += $r->pwning_int;
			$bc_redm_int_tot += $r->redeem_int;

			$bc_pawn_int_gra_tot += $r->pwning_int;
			$bc_redm_int_gra_tot += $r->redeem_int;

		}


	$t .= '<tr>';
	$t .= '<td></td>';
	$t .= '<td><b>Branch Total</b></td>';
	$t .= '<td><b>'.number_format($bc_pawn_int_tot,2).'</b></td>';
	$t .= '<td><b>'.number_format($bc_redm_int_tot,2).'</b></td>';
	$t .= '<td><b>'.number_format(($bc_pawn_int_tot+$bc_redm_int_tot),2).'</b></td>';
	$t .= '</tr>';


	$t .= '<tr>';
	$t .= '<td colspan="5"><br></td>';	
	$t .= '</tr>';


	$t .= '<tr>';
	$t .= '<td></td>';
	$t .= '<td><b>Grand Total</b></td>';
	$t .= '<td><b>'. number_format($bc_pawn_int_gra_tot,2).'</b></td>';
	$t .= '<td><b>'. number_format($bc_redm_int_gra_tot,2).'</b></td>';
	$t .= '<td><b>'. number_format(($bc_pawn_int_gra_tot+$bc_redm_int_gra_tot),2).'</b></td>';
	$t .= '</tr>';
	

	$t .= '</table>';

	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'interest_income.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    exit;

	

?>