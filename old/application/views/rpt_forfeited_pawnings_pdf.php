<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Forfeited Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->SetFont('helvetica', '', 8);		

	
	$no = 1;
	$sb = $bcs = $bts = '';
	$btQty = $bt_tot = $btMrktp = $btAdvRsvd = $btINT = $btOthrCost = 0;

	foreach($list as $r){

		$h = 5 * max( 1, $this->pdf->getNumLines($r->itemname,35) );

		if ( $bcs != $r->bc){

			// Totals---------------------------------

			if ($first_round_passed){
				
				if ($bt_tot <> 0){

					$this->pdf->SetFont('helvetica', 'b', 8);
					$this->pdf->MultiCell( 8, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(17, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(35, 1, 'Total', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(10, 1, $btQty, $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(15, 1, d3($bt_tot), $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(22, 1, d($btMrktp), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(27, 1, d($btAdvRsvd), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(28, 1, d($btINT), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(22, 1, d($btOthrCost), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(17, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->SetFont('helvetica', '', 8);
					
					$btQty = $bt_tot = $btMrktp = $btAdvRsvd = $btINT = $btOthrCost = 0;


				}
				
			}

			$first_round_passed = true;

			// Totals----------------------------------



			$this->pdf->ln();
			$this->pdf->SetFont('helvetica', 'B', 10);		
			$this->pdf->MultiCell(0, 0, $r->bc, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->SetFont('helvetica', 'b', 8);		
			//$this->pdf->ln();			

			$this->pdf->SetFont('helvetica', '', 8);		
			$bcs = $r->bc;
			$bts = '';

		}


		if ($r->billtype != $bts){


			// Totals---------------------------------

			if ($first_round_passed){

				if ($bt_tot <> 0){
				
					$this->pdf->SetFont('helvetica', 'b', 8);
					$this->pdf->MultiCell(8, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(17, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(35, 1, 'Total', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(10, 1, $btQty, $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(15, 1, d3($bt_tot), $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(22, 1, d($btMrktp), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(27, 1, d($btAdvRsvd), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(28, 1, d($btINT), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(22, 1, d($btOthrCost), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(17, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
					$this->pdf->SetFont('helvetica', '', 8);
					$btQty = $bt_tot = $btMrktp = $btAdvRsvd = $btINT = $btOthrCost = 0;

				}
				
			}

			$first_round_passed = true;

			// Totals----------------------------------







			$this->pdf->ln();
			$this->pdf->SetFont('helvetica', 'B', 10);		
			$this->pdf->MultiCell(0, 0, $r->billtype, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			

			$this->pdf->SetFont('helvetica', 'b', 8);
			$this->pdf->MultiCell(8, 1, "Sno", '1','L', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(25, 1, "Bill No", '1','L', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(17, 1, "Date", '1','C', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(35, 1, "Description", '1','L', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(10, 1, "Qty", '1','C', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(15, 1, "Weight", '1','C', 0, 0, '', '', false, '', 0);
			$this->pdf->MultiCell(22, 1, "Market Price", '1','R', 0, 0, '', '', '', '', 0);
			$this->pdf->MultiCell(27, 1, "Advance Received", '1','R', 0, 0, '', '', '', '', 0);
			$this->pdf->MultiCell(28, 1, "Interest Up to Date", '1','R', 0, 0, '', '', '', '', 0);
			$this->pdf->MultiCell(22, 1, "Other Cost", '1','R', 0, 0, '', '', '', '', 0);
			$this->pdf->MultiCell(17, 1, "F.Date", '1','C', 0, 0, '', '', '', '', 0);
			$this->pdf->MultiCell(25, 1, "Checked by H/O", '1','L', 0, 0, '', '', '', '', 0);
			$this->pdf->MultiCell(25, 1, "Remarks", '1','R', 0, 0, '', '', '', '', 0);
			$this->pdf->MultiCell(25, 1, "Nod", '1','L', 0, 1, '', '', '', '', 0);
			$this->pdf->SetFont('helvetica', '', 8);		
			$bts = $r->billtype;
		}



		if ($sb != $r->billno){
		
			$this->pdf->MultiCell(8, 1, $no, $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, $r->billno, $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(17, 1, $r->pawn_date, $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(35, 1, $r->itemname, $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(10, 1, $r->qty, $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(15, 1, d3($r->pure_weight), $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(22, 1, d($r->market_price), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(27, 1, d($r->advance_received), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(28, 1, d($r->int_up_to_date), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(22, 1, $r->other_cost, $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(17, 1, $r->fdate, $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, "", $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, "", $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, $r->no_of_days, $border = 'BLR', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);

			$sb = $r->billno;

			$btAdvRsvd += $r->advance_received;
			$btINT += $r->int_up_to_date;
			$btOthrCost += $r->other_cost;

			$no++;

		}else{

			$this->pdf->MultiCell( 8, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(17, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(35, 1, $r->itemname, $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(10, 1, $r->qty, $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(15, 1, d3($r->pure_weight), $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(22, 1, d($r->market_price), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(27, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(28, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(22, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(17, 1, "", $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, "", $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, "", $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
			$this->pdf->MultiCell(25, 1, "", $border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);

		}


		$btQty += $r->qty;
		$bt_tot += $r->pure_weight;		
		$btMrktp += $r->market_price;



		
	}

	// Totals---------------------------------

	if ($first_round_passed){
		
		$this->pdf->SetFont('helvetica', 'b', 8);
		$this->pdf->MultiCell(8, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(17, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(35, 1, 'Total', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(10, 1, $btQty, $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(15, 1, d3($bt_tot), $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(22, 1, d($btMrktp), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(27, 1, d($btAdvRsvd), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(28, 1, d($btINT), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(22, 1, d($btOthrCost), $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(17, 1, '', $border = 'BLR', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->MultiCell(25, 1, '', $border = 'BLR', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'T', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);
		
	}

	$first_round_passed = true;

	// Totals----------------------------------



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}


	$this->pdf->Output("PDF.pdf", 'I');

?>