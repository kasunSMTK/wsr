<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 22);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Internal Audit Article", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 11);

	if ($bc == " Multiple Branches"){
		$bc = $bc;
	}else{
		$bc = $this->db->where("bc",$bc)->limit(1)->get('m_branches')->row()->name;
	}


	$this->pdf->MultiCell(0, 0, "As at " . $td . " Branch : ". $bc , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();	

	$this->pdf->SetFont('helvetica', '', 8);		

	

	$no = 0;
	$s = $dd = $bt = '';


	foreach ($bc_arr as $bc_) {

		$bc_n = $this->db->where("bc",$bc_)->limit(1)->get('m_branches')->row()->name;

		$this->pdf->SetFont('helvetica', 'B', 8);		
		$this->pdf->MultiCell(0, 5, $bc_ . " - ".$bc_n ,$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->ln(2);

		$this->pdf->MultiCell(10, 5, "No", $border = 		   '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(15, 5, "Bill Type", $border =    '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(17, 5, "Date", $border = 		   '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, 5, "Bill No", $border = 	   '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(35, 5, "Description", $border =  '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		
		$this->pdf->MultiCell(15, 5, "P.Weight", $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(12, 5, "Weight", $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		
		$this->pdf->MultiCell(18, 5, "B. Caratage", $border =  '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(12, 5, "T.Weight", $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(22, 5, "Amount", $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);

		$this->pdf->MultiCell(16, 5, "A G.Type", $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(15, 5, "A G Weig", $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(15, 5, "A P Weig", $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(18, 5, "Audit Over Advance", $border = 	   'TB', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);


		$this->pdf->MultiCell(30, 5, "Audit Comment", $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);

		$totW = $totPW = $totA = $ra = 0; 
		$nt = '';

		foreach ($list as $R) {

			if ( $R->bc == $bc_){

				if ($s != $R->billno){			
					$bn = $R->billno;
					$s = $R->billno;
					$dd = $R->ddate;
					$bt = $R->billtype;
					$no++;
					$nt = $no;
					$ra = $R->requiredamount;
					$totA += $R->requiredamount;
					$tw = $R->totalweight;
				}else{
					$bn = "";
					$dd = '';
					$bt = '';
					$nt = '';
					$ra = '';
					$tw = '';
				}

				$h = 4 * (	max(1,$this->pdf->getNumLines($R->audit_comment,30),$this->pdf->getNumLines($R->itemname,35)) );

				$this->pdf->MultiCell(10, $h, $nt,$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(15, $h, $bt,$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(17, $h, $dd,$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(25, $h, $bn,$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(35, $h, $R->itemname,$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				
				$this->pdf->MultiCell(15, $h, $R->pure_weight,$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(12, $h, $R->goldweight,$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				
				$this->pdf->MultiCell(18, $h, $R->goldcatagory,$border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				
				$this->pdf->MultiCell(12, $h, $tw,$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				
				$this->pdf->MultiCell(22, $h, $ra,$border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				
				$this->pdf->MultiCell(16, $h, $R->audit_goldtype, $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(15, $h, $R->audit_goldweight, $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(15, $h, $R->audit_pure_weight, $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(18, $h, $R->audit_overadvance, $border = 	   '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

				$this->pdf->MultiCell(30, $h, $R->audit_comment ,$border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
				


			}
				
			$totPW += $R->pure_weight;
			$totW += $R->goldweight;

		}

		$this->pdf->SetFont('helvetica', 'B', 8);
		$this->pdf->MultiCell(10, 5, "", $border = 		   	'1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(15, 5, "", $border = 		   	'1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(17, 5, "", $border = 		   	'1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(25, 5, "", $border = 	   		'1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(35, 5, "Total", $border =   	'1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		
		$this->pdf->MultiCell(15, 5, dd($totPW), $border = 	'1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(12, 5, dd($totW), $border = 	'1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		
		$this->pdf->MultiCell(18, 5, "", $border =   		'1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(12, 5,"",$border = 			'1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(22, 5, d($totA), $border = 	'1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(0, 5, "", $border = 			'1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 5, $valign = 'M', $fitcell = false);
		$this->pdf->SetFont('helvetica', '', 8);


	}


	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function dd($number) {
	    return number_format($number, 3, '.', ',');
	}

	

	$this->pdf->Output("PDF.pdf", 'I');

?>