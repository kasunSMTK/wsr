<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->pdf->setPrintHeader($header,$type,$duration);
$this->pdf->setPrintHeader(true,$type);
$this->pdf->setPrintFooter(true);

    //print_r($purchase);
$this->pdf->SetFont('helvetica', 'B', 16);
    $this->pdf->AddPage("P","A4");   // L or P amd page type A4 or A3

    foreach($branch as $ress){
      $this->pdf->headerSet3($company,$ress->name,$ress->address,$ress->tp,$ress->fax,$ress->email);
    }
    $this->pdf->Ln();
    $this->pdf->SetFont('helvetica', 'BUI',10);
    $this->pdf->Cell(30, 6,'ISSUED CHEQUES',1,false, 'L', 0, '', 0, false, 'M', 'M');
    $this->pdf->Ln();

    $this->pdf->SetFont('helvetica', '',9);
    $this->pdf->Cell(0, 6, 'Date From '. $dfrom.' To '.$dto ,0,false, 'L', 0, '', 0, false, 'M', 'M');

    $this->pdf->Ln(3);

    $border="1";  
    $border1="TBR"; 
    $border2="B";

    $this->pdf->SetX(15);
    $this->pdf->SetFont('helvetica','B',9);

    if($cus_code !=null){
      $this->pdf->SetFont('helvetica', 'B',9);
      $this->pdf->Cell(20, 1, "Customer", '0', 0, 'L', 0);
      $this->pdf->Cell(30, 1,": ".$cus_code." - ".$cus_name, '0', 0, 'L', 0);
      $this->pdf->Ln();
    }

    if($sup_code !=null){
      $this->pdf->SetFont('helvetica', 'B',9);
      $this->pdf->Cell(20, 2, "Salesman", '0', 0, 'L', 0);
      $this->pdf->Cell(30, 2,": ".$sup_code." - ".$sup_name, '0', 0, 'L', 0);
      $this->pdf->Ln();
    }

    if($asm_code !=null){
      $this->pdf->SetFont('helvetica', 'B',9);
      $this->pdf->Cell(20, 2, "Area Manager", '0', 0, 'L', 0);
      $this->pdf->Cell(30, 2,": ".$asm_code." - ".$asm_name, '0', 0, 'L', 0);
      $this->pdf->Ln();
    }
    $border="TB"; $bdr="";
    $this->pdf->SetX(15);
    $this->pdf->MultiCell(19,6, "Date", "TB", 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30,6, "Account No", "TB", 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(22,6, "Cheque No", "TB", 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30,6, "Amount", "TB", 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(22,6, "Realize Date", "TB", 'R', 0, 1, '', '', true, 0, false, true, 0);

    
    $this->pdf->SetFont('helvetica','',9);

    $S = "";

    $TOT = $G_TOT = 0; $X = false;

    foreach ($row as $R) {

      if ($S != $R->sup_code){
        if ($X==true){
          $this->pdf->SetFont('helvetica','B',9);            
          $this->pdf->MultiCell(131,0, number_format($TOT,2), 0, 'R', 0, 0, '', '', true, 0, false, true, 0);
          $this->pdf->MultiCell(22,0, "", 0, 'L', 0, 1, '', '', true, 0, false, true, 0);
          $this->pdf->SetFont('helvetica','',9);
          $TOT = 0;
        }
        $S = $R->sup_code;

        //
        $this->pdf->HaveMorePages(5* 3);
        //
        $this->pdf->SetFont('helvetica','B',9);
        $this->pdf->SetFillColor(156,155,145);
        $this->pdf->MultiCell(60,5, $R->sup_name, $border = '0', $align = 'L', $fill =true, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 6, $valign = 'M', $fitcell = false);         
        $this->pdf->SetFillColor(255,255,255);
        $this->pdf->SetFont('helvetica','',9);
        $X = true;
      }else{

      }
      //
      $this->pdf->HaveMorePages(5* 3);
      //

      $this->pdf->SetX(15);
      $this->pdf->MultiCell(19,0, $R->date, $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);   
      $this->pdf->MultiCell(30,0, $R->acc_no, $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
      $this->pdf->MultiCell(22,0, $R->cheque_no, $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
      $this->pdf->MultiCell(30,0, number_format($R->amount,2), $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
      $this->pdf->MultiCell(22,0, $R->realize_date, 0, 'R', $bdr, 1, '', '', true, 0, false, true, 0);

      $TOT +=(float)$R->amount;
      $G_TOT += (float)$R->amount;
      $X==true;
      

    }

    if ($X==true){
      $this->pdf->SetFont('helvetica','B',9);            
      $this->pdf->MultiCell(131,0, number_format($TOT,2), 0, 'R', 0, 1, '', '', true, 0, false, true, 0);
      $this->pdf->MultiCell(22,0, "", 0, 'L', 0, 1, '', '', true, 0, false, true, 0);
      $this->pdf->SetFont('helvetica','',9);
      $TOT = 0;
    }

    
  //  var_dump($G_TOT);  

    $this->pdf->ln(4);
    $this->pdf->SetFont('helvetica','B',9);            
    $this->pdf->MultiCell(91,0, "Grand Total Amount", "BT", 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(40,0, number_format($G_TOT,2), "BT", 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(22,0, "", "BT", 'L', 0, 1, '', '', true, 0, false, true, 0);
    $this->pdf->SetFont('helvetica','',9);

    $this->pdf->Output("cheques_issue_realize".date('Y-m-d').".pdf", 'I');

    ?>