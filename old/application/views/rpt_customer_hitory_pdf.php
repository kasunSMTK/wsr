<?php



	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);

	$this->pdf->SetPrintHeader(false);

	$this->pdf->SetPrintFooter(false);

	$this->pdf->AddPage();	



	$this->pdf->SetFont('helvetica', '', 28);	

	$this->pdf->setX(0);

	

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));

	$this->pdf->Line(5, 0, 0, 0, $style);



	$this->pdf->MultiCell(0, 0, "Customer History", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	

	$this->pdf->SetFont('helvetica', '', 15);	

	$this->pdf->MultiCell(0, 0, "Between " .$fd . " and " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

	$this->pdf->setX(0);

	$this->pdf->ln();

	

	$tot_transval=$tot_bal=(float)0;

	$this->pdf->SetFont('helvetica', '', 8);	

	$this->pdf->setX(15);
	
	$this->pdf->MultiCell(40, 1, "Branch", 'B','L', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(25, 1, "Bill No", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Pawning Date", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Due Date", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Redeem Date", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "Capital Amount", 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "Fofeited Capital amount", 'B','R', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "Unredeemed Balance", 'B','R', 0, 1, '', '', false, '', 0);
	//$this->pdf->ln();

	 $bdr='B'; $y=0; $CAP_TOT = $FORFI_TOT = $BAL = 0;
	foreach($list as $r){
		$this->pdf->setX(15);	

		$heigh = 0; //5 * (	max(1,$this->pdf->getNumLines($r->cusname,30) ) );

		$this->pdf->setX(15);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->MultiCell(40,  $heigh, $r->bc." - ".$r->name,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(25,  $heigh,$r->billno,  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,$r->pwningdate,  $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh, $r->finaldate,  $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(30,  $heigh,$r->redemdate,  $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(40,  $heigh,number_format($r->requiredamount,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(40,  $heigh,number_format($r->fofeited,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
        $this->pdf->MultiCell(40,  $heigh,number_format($r->bal,2),  $bdr, 'R', 0, 1, '', '', true, 0, false, true, 0);
         
	
		$tot_transval+=$r->traseval;
		$tot_bal+=$r->bal;
		
		$CAP_TOT 	+= $r->requiredamount;
		$FORFI_TOT 	+= $r->fofeited;
		$BAL 		+= $r->bal;

	}

	$this->pdf->setX(15);
	$this->pdf->SetFont('helvetica', 'B', 8);		
	
	$this->pdf->MultiCell(40,  $heigh, '',  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(25,  $heigh,'',  $bdr, 'L', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30,  $heigh,'',  $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30,  $heigh, '',  $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(30,  $heigh,'',  $bdr, 'C', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(40,  $heigh,number_format($CAP_TOT,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(40,  $heigh,number_format($FORFI_TOT,2),  $bdr, 'R', 0, 0, '', '', true, 0, false, true, 0);
    $this->pdf->MultiCell(40,  $heigh,number_format($BAL,2),  $bdr, 'R', 0, 1, '', '', true, 0, false, true, 0);
	

	$this->pdf->Output("PDF.pdf", 'I');



?>