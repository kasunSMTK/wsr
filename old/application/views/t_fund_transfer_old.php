<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $( "#date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2050"
        });
    });

</script>
    
    

    <div class="page_contain_new_pawn">
        <div class="page_main_title_new_pawn">
            <div style="width:240px;float:left"><span>Fund Transfer</span></div>                 
        </div>
    </div>

    <form action="<?=base_url()?>index.php/main/save/t_fund_transfer" method="post" id="form_">

        <table align="center" border="0" width="800" cellpadding="0" cellspacing="0">            
            <tr>
                <td>
                    <div id="tabs" style="box-shadow: none;border-top: none;">
                        <ul>
                            <li><a href="#tabs-1" class="x"><div class="">Make A New Request</div></a></li>
                            <li><a href="#tabs-2" class="x"><div class="">My Requests Staus <span class="span_notify my_requ"><?=$my_requests['my_requ_rc']?></span></div></a></li>
                            <li><a href="#tabs-3" class="x"><div class="">Incomming Requests <span class="span_notify incomg_requ"><?=$incomming_requests['incmg_requ_rc']?></span></div></a></li>
                        </ul>
                    
                        <div id="tabs-1" style="box-shadow: none">
                            <table class="tbl_chq_issue" align="center" border="0" width="800" cellpadding="0" cellspacing="0">            

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:77%;float: left">
                                        Request by <input type="text" id="bc_name" class="input_text_reg" style="width:500px" value="<?=$bc_code_name?>"><input type="hidden"  name="bc" id="bc" value="<?=$bc?>">
                                    </div>                
                                    <div class="text_box_holder_commen" style="width:23%;float: right;border-left: none;">
                                        No <input type="text" class="input_text_reg" id="no" value="<?=$max_no?>" style="width:100px">
                                    </div>
                                </td>            
                            </tr>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:77%;float: left;border-top: none;">
                                        Reference No <input type="text" class="input_text_reg" name="ref_no"  id="ref_no" style="width:500px">                    
                                    </div>                
                                    <div class="text_box_holder_commen" style="width:23%;float: right;border-top: none;border-left: none;">
                                        Date <input type="text" class="input_text_reg" name="date" id="date" value="<?=date('Y-m-d')?>" style="width:100px">
                                    </div>
                                </td>            
                            </tr>

                            <?php if ($isHO == 0){ ?>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:100%;float: left;border-top: none;padding-bottom: 12px;padding-top: 10px">
                                        <label><input type="radio" name="request_from" value="headoffice">From Head Office </label>
                                    </div>
                                </td>            
                            </tr>

                            <?php } ?>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:100%;float: left;border-top: none;">
                                        <label><input type="radio" name="request_from" value="branch">From Branch</label>
                                        <input type="text" class="input_text_reg" id="from_branch_name" style="width:300px">
                                        <input type="hidden" name="from_bc"  id="from_bc">                                        
                                    
                                        <label>Bank Account</label>
                                        <input type="text" class="input_text_reg" id="bnk_acc_name" style="width:300px">
                                        <input type="hidden" name="bank_acc"  id="bank_acc">
                                        <input type="hidden" name="is_bank_acc"  id="is_bank_acc">
                                    </div>
                                </td>            
                            </tr>

                            <tr>
                                <td height="5">&nbsp;</td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:100%;float: left">
                                        
                                        Amount <input type="text" class="amount input_text_reg" id="cash_amount" name="cash_amount" style="width:300px">
                                        
                                        <label style="margin-left: 150px;margin-right: 50px">
                                            <input type="radio" name="receiving_method" value="cash" checked>Cash
                                        </label>
                                        
                                        <?php if ($isHO == 0){ ?>
                                        <label>
                                            <input type="radio" name="receiving_method" value="cheque">Cheque Withdraw
                                        </label>
                                        <?php } ?>

                                    </div>
                                </td>            
                            </tr>        

                            <tr>
                                <td height="5">&nbsp;</td>
                            </tr>
                            
                            <?php if ($isHO == 0){ ?>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:100%;float: left;border:none;padding: 0px;">                        
                                        <div>
                                            
                                            <div class="text_box_holder_commen" style="width:40%;float: left;">
                                                <span>Bank Name</span>
                                                <input type="hidden" name="bank_code">
                                            </div>

                                            <div class="text_box_holder_commen" style="width:40%;float: left;border-left: none;">
                                                <span>Cheque Number</span>
                                                <input type="hidden" name="bank_code">
                                            </div>

                                            <div class="text_box_holder_commen" style="width:17%;float: left;border-left: none;text-align: right">
                                                <span>Amount</span>
                                                <input type="hidden" name="bank_code">
                                            </div>

                                        </div>

                                    </div>                
                                </td>            
                            </tr>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:100%;float: left;border:none;padding: 0px;">
                                        <div id="div_grid" class="cash_cheque_grid_disabled" style="overflow-y:auto ">                                                
                                            <?=$cash_cheque_grid?>
                                        </div>
                                    </div>                
                                </td>            
                            </tr>

                            <tr>
                                <td height="5">&nbsp;</td>
                            </tr>

                            <?php } ?>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:100%;float: left">
                                        Comment<br><input type="text" class="input_text_reg"  name="comment" id="comment" style="width: 785px">
                                    </div>                
                                </td>            
                            </tr>

                            <tr>
                                <td>
                                    <div class="text_box_holder_commen" style="width:100%;float: left;border-top: none;">
                                        <input name="btnRequest" type="button" value="Send" id="btnRequest" class="btn_regular"/>
                                        <input type="hidden" id="hid" name="hid" value="0">                                        
                                        <?php if ($isHO == 1){echo '<input type="hidden" name="isHO" value="1">'; }else{echo '<input type="hidden" name="isHO" value="0">'; } ?>
                                    </div>                
                                </td>            
                            </tr>                    

                            </table>
                        </div>
        
                        <div id="tabs-2"style="box-shadow: none">
                            <table class="tbl_incmg_req" align="center" border="0" width="800" cellpadding="0" cellspacing="0">    

                                <tr>
                                    <td>No</td>                        
                                    <td>Date</td>            
                                    <td>Receiving Method</td>
                                    <td align="right">Amount</td>
                                    <td align="right">Approved Amount</td>
                                    <td>Request Datetime</td>
                                    <td>Forworded Time</td>
                                    <td width="130">Status</td>
                                </tr>

                                <?=$my_requests['d']?>

                            </table>
                        </div>
            
                        <div id="tabs-3" style="box-shadow: none">    
                            <table class="tbl_incmg_req" align="center" border="0" width="800" cellpadding="0" cellspacing="0">    
                            <tr>
                                <td>No</td>            
                                <td>Date</td>            
                                <td>Receiving Method</td>
                                <td align="right">Cash Amount</td>
                                <td align="right">Approved Amount</td>
                                <td>Request Datetime</td>
                                <td>Forworded Time</td>
                                <td>Action</td>            
                            </tr>

                            <?=$incomming_requests['d']?>

                            </table>
                        </div>
        
                    </div>
    
                </td>
            </tr>
        </table>

    </form>


    <form id="print_fund_trans_voucher_pdf" action="<?php echo site_url(); ?>/reports/generate_fund_tr_voucher" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
            <input type='hidden' name='by' id='by' value='fund_tr_voucher' class="report" />            
            <input type="hidden" name="v_no" id="v_no" />            
            <input type="hidden" name="is_v_re_p" id="is_v_re_p" />            
            
        </div>                  
    </form>


</body>
</html>