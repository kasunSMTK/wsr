<?php

$this->pdf = new TCPDF("L", PDF_UNIT, 'SEW', true, 'UTF-8', false);
$this->pdf->SetPrintHeader(false);
$this->pdf->SetAutoPageBreak(TRUE, 0);
$this->pdf->AddPage();

// ----------- header --------------------------				
$Q = $this->db->query("SELECT IF (B.other_bc_name_allow = 0,C.`name`,'Shekaran Pawn Brokers.') AS `company_name`, B.name 		AS `bc_name`,B.bc_no AS `bc_code`, B.address 	AS `bc_address`, B.telno 	AS `bc_tel`, B.faxno 	AS `bc_fax`, B.email 	AS `bc_email` FROM `m_company` C JOIN (SELECT * FROM `m_branches`) B ON B.bc = '$bc'LIMIT 1 ")->row();


if ($is_reprint == 1){
	$rp = " (Reprint)";
}else{
	$rp = "";
}


$header_custom_data['title'] = "Form 9".$rp;

$this->pdf->sew_header_no_com_name($Q,$header_custom_data);
	// ----------- end header ----------------------

$this->pdf->SetFont('helvetica', '', 9);	
$this->pdf->SetLineStyle(array('width' => 0.00001, 'cap' => '', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

$this->pdf->MultiCell(22,  0, "Receipt No : ",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(22,  0, $sum->nno,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(0,  0, "Date : ".$sum->date,	$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->MultiCell(22,  0, "Name",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(0,  0, $sum->r_name,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->MultiCell(22,  0, "Bill No",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(0,  0, $sum->r_billno,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->MultiCell(22,  0, "Pawning Date",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(0,  0, $sum->r_pawn_date,	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->ln(10);

//$this->pdf->MultiCell(22,  0, "Account",	$border = 'BT', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(102,  0, "Description",	$border = 'BT', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(20,  0, "Amount",	$border = 'BT', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$tot 			= 0;
$adv_setl_amt 	= 0;

foreach ($det as $rr) {

	if ($rr->is_dr_acc == 1){
		$amount_D = "-" . $rr->amount;
	}else{
		$amount_D = $rr->amount;
	}

	if ( $rr->amount != 0 ){

		if ($rr->acc_code == 10102){ $desc = "Interest"; }
		if ($rr->acc_code == 30201){ $desc = "Capital"; }
		if ($rr->acc_code == 30211){ $desc = "Postage"; }
		if ($rr->acc_code == 40212){ $desc = "Stamp Duty"; }		

		if ($rr->acc_code == 40215){
			if ($rr->is_dr_acc == 1){
				$desc = "Advance Settlement";
			}else{
				$desc = "Advance Received";
			}
		}




		//$this->pdf->MultiCell(22,  0, 	$rr->acc_code,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(102,  0, 	$desc,	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
		$this->pdf->MultiCell(20,  0, 	number_format($amount_D,2),	$border = '0', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	}

	if ($rr->is_dr_acc == 1){
		$adv_setl_amt = $rr->amount;
	}else{
		$tot += $rr->amount;
	}

}

$tot -= $adv_setl_amt;

//$this->pdf->MultiCell(22,  0, "",	$border = 'TB', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(102,  0, "Total",	$border = 'TB', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(20,  0, number_format($tot,2),	$border = 'TB', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->ln(3);
$this->pdf->MultiCell(0,  0, "Receipt Description :". $sum->receipt_desc ,	$border = '0', $align = 'L', $fill = false, $ln = 01, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->ln(15);

$this->pdf->MultiCell(84,  0, "Pawner ............................",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
$this->pdf->MultiCell(50,  0, "Cashier ............................",	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

$this->pdf->ln(5);
$this->pdf->MultiCell(50,  0, "Operator : ".$sum->display_name."(".$sum->oc.")",	$border = '0', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);



$this->pdf->Output("pawn_ticket.pdf", 'I');

	
function d($n,$d=2){
	return 	number_format($n,$d);				
}



?>