<?php


	/*foreach ($list as  $v) {
		echo $v->receipts ." ----------------------" . $v->payments . "<br>" ;
	}

	exit;*/


	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 22);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Daily Cash Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 11);	
	$this->pdf->MultiCell(0, 0, "As at " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();	

	$this->pdf->SetFont('helvetica', '', 8);		

	$this->pdf->MultiCell(10, 1, "S.No", 			'T','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(30, 1, "Description", 	'T','L', 0, 0, '', '', false, '', 0);		
	$this->pdf->MultiCell(15, 1, "Tra.Code", 		'T','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(20, 1, "Bill No", 		'T','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(15, 1, "Bill Type", 		'T','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(22, 1, "Receipts (Rs)", 	'T','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(22, 1, "Payments (Rs)", 	'T','C', 0, 0, '', '', '', '', 0);	
	$this->pdf->MultiCell(22, 1, "Balance (Rs)", 	'T','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(26, 1, "Date Time", 		'T','R', 0, 1, '', '', '', '', 0);

	$this->pdf->SetFont('', 'B', 7);

	$opbal = 150000.00;

	$this->pdf->MultiCell(40, 	5, "Opening Bal B/F (Rs.) - Sample Data" , 	$border='BT', $align='l', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);
	$this->pdf->MultiCell(119,  5, number_format($opbal,2) , 	$border='BT', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);		
	$this->pdf->MultiCell(30,   5, "" , 						$border='BT', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=5, $valign='M', $fitcell=true);				
	
	$this->pdf->SetFont('', '', 7);
	
	$no = 1;

	foreach($list as $r){	

		$h = 4 * (max(1,$this->pdf->getNumLines($r->description,30),$this->pdf->getNumLines($r->receipts,25)));
		
		$opbal += $r->receipts;
		$opbal -= $r->payments;

		$this->pdf->MultiCell(10, $h, $no , 					$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(30, $h, $r->description , 		$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(15, $h, $r->entry_code , 			$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);		
		$this->pdf->MultiCell(20, $h, $r->billno , 				$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(15, $h, $r->billtype , 			$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(22, $h, $r->receipts , 			$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(22, $h, $r->payments , 			$border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(22, $h, number_format($opbal,2) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(30, $h, $r->action_date , 		$border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);				
		$no++;
	}



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}


	$this->pdf->Output("PDF.pdf", 'I');

?>