<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

	$(function() {
		$( ".date_ch_allow" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2025",
			onSelect: function(date){
				change_current_date(date);				
			}
		});
	});

	$(function() {
		$( ".date_ch_allow_n" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			minDate:'<?=$backdate_upto?>',
			maxDate:0,
			yearRange: "1930:2025",
			onSelect: function(date){
				change_current_date(date);				
			}
		});
	});

	names 		= [<?=$item_category?>];
	bcCustomer 	= [];
	gold_q 		= [<?=$gold_q?>];

	function date_change_response_call(){
		$("#btnReset").click();
	}

</script>

<style type="text/css">
	.ui-autocomplete-loading { background:url('http://www.suddenlink.com/sites/all/themes/suddenlink/images/ajax-loader.gif') no-repeat right center  }	
</style>

<!-- <form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="receiver"> -->
<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
    <div style="text-align:left; padding-top: 7px;">
    	<input type='hidden' name='by' id='by' value='pawn_ticket' class="report" />            
        <input type="hidden" class="input_active_num" name="r_no" id="r_no" title="r_no" value=""  />  
        <input type="hidden" id="is_reprint" name="is_reprint" value="0">
    </div>                  
</form>

<div id="pdfdiv_content">
	<iframe name="receiver" id="receiver" width="0" height="0"></iframe>
</div>	

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>New Pawn</span></div>		
	</div>

	<form method="post" action="<?=base_url()?>index.php/main/save/t_new_pawn" id="form_">	

		<table border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" align="center">
			<tr>
				<td width="70%" valign="top" align="left" style="padding-left:20px">

					<div class="cus_n_bltyp_holder">
						<table>
							<tr>
								<td colspan="2">
									<div class="text_box_holder_new_pawn" style="width:154px; border:none">
										<div style="float:right"><input type="checkbox" id="chkShowSugg" disabled="disabled"></div>
										<span class="text_box_title_holder_new_pawn">Customer ID</span>
										<input class="input_text_regular_new_pawn" type="text" name="customer_id" id="customer_id" style="width:142px" value="">
										<input type="hidden" name="cus_serno" id="cus_serno" value="">
									</div>
								</td>
								
								<td>
									<div class="text_box_holder_new_pawn" style="width:110px;border-bottom:none">
										<div style="float:left;width:20px"><span class="text_box_title_holder_new_pawn">Bill Type</span></div>
										<!-- <div style="float:right;width:20px"><input type="checkbox" name="get_r_billtype" class="get_r_billtype"></div> -->
										<div id ='selbilltype'>
										</div>										
										<input class="input_text_regular_new_pawn" type="hidden" id="bill_code_desc" style="width:98px" readonly="readony">
										<input type="hidden" name="billtype" id="billtype">
										<input type="hidden" name="bt_letter" id="bt_letter">										
										<input type="hidden" name="billtype_in_no" id="billtype_in_no">										
									</div>
								</td>

								<td>
									<div class="text_box_holder_new_pawn" style="width:178px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Bill Number</span>
										<input class="input_text_regular_new_pawn" type="text" id="billno" name="billno" style="width:170px" readonly="readonly">
										<input type="hidden" id="billtypeno" name="billtypeno">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:120px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Int. Rate <div style="float:right">
										<input type="checkbox" name="allow_full_month" id="allow_full_month" disabled checked></div></span>
										<input class="input_text_regular_new_pawn" type="text" name ="fMintrate"  id="fMintrate"  style="width:90px;border:1px solid #fff" readonly="readonly">
										<input class="input_text_regular_new_pawn" type="hidden" name ="fMintrate2" id="fMintrate2" style="width:10px;border:1px solid #fff" readonly="readonly">
										<input type="hidden" name="is_pre_intst_chargeable" id="is_pre_intst_chargeable" value="0">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:76px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Period</span>
										<input class="input_text_regular_new_pawn" type="text" id="period" name="period" style="width:60px" readonly="readonly">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:90px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Date</span>
										<input class="input_text_regular_new_pawn <?=$date_change_allow?>" type="text" id="dDate" name="dDate" style="width:80px" readonly="readonly" value="<?=$current_date?>">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:148px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn">Old Bill Number</span>
										<input class="input_text_regular_new_pawn" type="text" id="loan_no" style="width:140px;border:2px solid Green">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:160px;border-bottom:none">
										<span class="text_box_title_holder_new_pawn" style="width: 150px">Manual Book Number</span>
										<input class="input_text_regular_new_pawn" type="text" id="manual_bill_no" style="width:165px;border:2px solid Green">
									</div>
								</td>
							</tr>
						</table>
					</div>


					<div id="add_new_cus_front_pawn">
						    
					<div style="margin:20px">						   
						
							<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto;">

								<tr>
									<td>
										<b>Add New Customer</b><br><br>
									</td>
									<td></td>				
								</tr>		

								<tr>									
									
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder" id="SP454">Title</span> 						
											<div style="padding:5px;">
												<label style="margin-right:50px;"><input type="radio" name="title" value="Mrs. " 	tabindex="1"> Mrs</label>
												<label style="margin-right:50px;"><input type="radio" name="title" value="Miss. "  	tabindex="1"> Miss</label>
												<label style="margin-right:00px;"><input type="radio" name="title" value="Mr. "  	tabindex="1"> Mr</label>
											</div>						
										</div>										
									</td>

									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Other Names</span> 
											<input class="input_text_regular_pawn_front" type="text" id="other_name"  tabindex="8">
										</div>
									</td>

								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<div class="input_title_holder">
												
												<select class="customer_id_dropdown" id="customer_id_type">
													<option value="nic">NIC</option>
													<option value="pp">Passport</option>
													<option value="dl">Drving License</option>
													<option value="gm">Grama Niladari #####</option>
												</select>
												
											</div> 
											<input class="nic_feild input_text_regular_pawn_front" type="text" id="nicno" tabindex="2">
										</div>
									</td>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Phone No</span> 
											<input class="contct_no_feild input_text_regular_pawn_front" type="text" id="telNo" tabindex="9">
										</div>
									</td>

								</tr>

								<tr>
									<td>										
										<div class="input_div_holder">
											<span class="input_title_holder">Full Name</span> 
											<input class="input_text_regular_pawn_front" type="text" id="cusname" tabindex="3">
										</div>
									</td>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Address 2</span> 
											<input class="input_text_regular_pawn_front" type="text"  id="address2" tabindex="10">
										</div>										
									</td>

								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Address 1</span> 
											<input class="input_text_regular_pawn_front" type="text" id="address" tabindex="4">
										</div>
									</td>			
									<td>					
										
									</td>
								</tr>

								<tr>
									<td>
										<div class="input_div_holder">
											<span class="input_title_holder">Mobile No</span> 
											<input class="contct_no_feild input_text_regular_pawn_front" type="text" id="mobile" tabindex="5">
										</div>						
									</td>			
									<td>
									</td>
								</tr>			

								<tr>
									<td>
										<input type="button" value="Save" 	id="btnFrontSave" 			class="btn_regular" tabindex="6">
										<input type="button" value="Cancel" id="btnCancelFrontCusInput"	class="btn_regular"  tabindex="7">
										<input type="hidden" value="0" 		id="hid" >
									</td>
								</tr>		
								
							</table>

						</form>
					</div>

				</div>
					<div class="frnt_msg_hldr" ></div>
					<div class="loan_dtl_contain" style='height:32px'>
						<div class="text_box_holder_new_pawn" >
							<span class="text_box_title_holder_new_pawn" style="width:180px;">
								<input type='radio' name='is_gold1' id='is_gold'  value='0' class='rd' checked>Gold Pawn
							</span>
							<span class="text_box_title_holder_new_pawn" style="width:200px;">
								<input type='radio' name='is_gold1' id='is_nongold'  value='1' class='rd'>
								Non Gold Pawn
							</span>
							<input type='hidden' name='is_non_gold' id='is_non_gold'>
						</div>
					</div>
					<div class="clr">	
					<div class="loan_dtl_contain">

						<div class="text_box_holder_new_pawn" style="width:135px;float:left">
							<span class="text_box_title_holder_new_pawn">Category</span>
							<input class="input_text_regular_new_pawn" type="text" id="cat_code_desc" style="width:120px" value="">
							<input type="hidden" id="cat_code" value="">
							<input type="hidden" id="is_bulk_cat" value="0">
						</div>

						<div class="text_box_holder_new_pawn" style="width:175px;float:left">
							<span class="text_box_title_holder_new_pawn">Item</span>
							
							<!-- <select id="item_code" style="width:165px" class="select_drp">
								<option value="">Select Item</option>								
							</select> -->

							<select id="item_code" style="width:165px" class="select_drp">
								
							</select>


						</div>

						<div class="text_box_holder_new_pawn" style="width:140px;float:left">
							<span class="text_box_title_holder_new_pawn">Condition</span>
							<!-- <input class="input_text_regular_new_pawn" type="text" id="condition_desc" style="width:137px" value="012 - PART IMPERIER QUALITY"> -->
							<?=$conditions?>
							

							<input type="hidden" id="condition" value="">
						</div>


						<div class="text_box_holder_new_pawn" style="width:60px;float:left">
							<span class="text_box_title_holder_new_pawn">Quality</span>
							<input class="input_text_regular_new_pawn" type="text" id="gold_qulty" style="width:47px" value="100">
							<input type="hidden" id="gold_qulty_rate" value="100.00">
							<input type="hidden" id="gold_qulty_code" value="NQ1">							
						</div>

						<div class="text_box_holder_new_pawn" style="width:50px;float:left">
							<span class="text_box_title_holder_new_pawn">Qty</span>
							<input class="input_text_regular_new_pawn NumOnly" type="text" id="qty" style="width:37px" maxlength="5" value="">
						</div>

						<div class="text_box_holder_new_pawn gold" style="width:110px;float:left" >
							<span class="text_box_title_holder_new_pawn">Karatage</span>
							
							<input class="input_text_regular_new_pawn kt-cl-a" type="text" id="gold_type_desc" style="width:97px" value="">

							<input class="input_text_regular_new_pawn kt-cl-b" type="text" id="gold_type_desc_R" style="width:97px;display: none" value="">
							
							<input type="hidden" id="goldcatagory" value="">
							<input type="hidden" id="gold_rate" value="">
						</div>
						<div class="text_box_holder_new_pawn gold" style="width:76px;float:left" >
							<span class="text_box_title_holder_new_pawn">T.Weight</span>
							<input class="input_text_regular_new_pawn FloatVal3" type="text" id="t_weigth" style="width:63px" maxlength="7" value="">
						</div>

						<div class="text_box_holder_new_pawn gold" style="width:73px;float:left" >
							<span class="text_box_title_holder_new_pawn">P.Weight</span>
							<input class="input_text_regular_new_pawn FloatVal3" type="text" id="p_weigth" style="width:60px" maxlength="7" value="">
						</div>

						<div class="text_box_holder_new_pawn gold" style="width:73px;float:left" >
							<span class="text_box_title_holder_new_pawn" style="margin-left:-2px">Densi Read</span>
							<input class="input_text_regular_new_pawn IntVal" type="text" id="d_weigth" style="width:60px" maxlength="7" value="">
						</div>

						<div class="text_box_holder_new_pawn" style="width:110px;float:left">
							<span class="text_box_title_holder_new_pawn">Value</span>
							<input class="input_text_regular_new_pawn" type="text" id="value" style="width:100px; text-align:right" value="">
						</div>

						<!--electronic-->
						<div class="text_box_holder_new_pawn electronic" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">Model</span>
							<input class="input_text_regular_new_pawn elec" type="text" id="elec_model" name="elec_model" style="width:65px" value="">
						</div>
						<div class="text_box_holder_new_pawn electronic" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">IMEI</span>
							<input class="input_text_regular_new_pawn elec" type="text" id="elec_imei" name="elec_imei" style="width:65px" value="">
						</div>
						<div class="text_box_holder_new_pawn electronic" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">S/N</span>
							<input class="input_text_regular_new_pawn elec" type="text" id="elec_serno" name="elec_serno" style="width:65px" value="">
						</div>
						<div class="text_box_holder_new_pawn electronic" style="width:120px;float:left">
							<span class="text_box_title_holder_new_pawn  elec">Description</span>
							<input class="input_text_regular_new_pawn elec" type="text" id="elec_desc" name ="elec_desc" style="width:110px" value="">
						</div>


						<!--vehicle-->

						<div class="text_box_holder_new_pawn vehicle" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">Model</span>
							<input class="input_text_regular_new_pawn veh" type="text" id="veh_model" name="veh_model" style="width:65px" value="">
						</div>
						<div class="text_box_holder_new_pawn vehicle" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">Engine No</span>
							<input class="input_text_regular_new_pawn veh" type="text" id="veh_engine_no" name="veh_engine_no" style="width:65px" value="">
						</div>
						<div class="text_box_holder_new_pawn vehicle" style="width:73px;float:left">
							<span class="text_box_title_holder_new_pawn">Chassis</span>
							<input class="input_text_regular_new_pawn veh" type="text" id="veh_chassis" name="veh_chassis" style="width:65px" value="">
						</div>
						<div class="text_box_holder_new_pawn vehicle" style="width:120px;float:left">
							<span class="text_box_title_holder_new_pawn veh">Description</span>
							<input class="input_text_regular_new_pawn veh" type="text" id="veh_desc" name="veh_desc" style="width:110px" value="">
						</div>
						<div class="text_box_holder_new_pawn" style="width:40px;float:left">
							<span class="text_box_title_holder_new_pawn">Action</span>
							<input type="button" value="Add" class="btn_regular" style="width:40px" id="btnAddtoList">
						</div>


					</div>
					<!--- elcectronics
					<!-- <div class="clr">
					<div class="loan_dtl_contain electronic">

					</div>
					<!-- vehicle -->

					<!-- div class="clr">
					<div class="loan_dtl_contain vehicle">

					</div -->

					<div class="div_manager_auth_option_2">
						<div style="padding: 10px;padding-bottom: 15px">
							<table width="100%" >
								<tr>
									<td><span style="color: red;font-size: 16px">Increase Gold Quality : Manager level approval required</span> <br><span style="font-size:13px">(Please enter branch manager password for increase gold quality)</span> </td>
									<td width="200" valign="top"><span style="font-size:13px">Username</span> 	<input type="text" id="txt_man_un2" 		class="input_text_regular_new_pawn" style="border:1px solid green"></td>
									<td width="200"><span style="font-size:13px">Password</span> 				<input type="password" id="txt_man_pw2" 		class="input_text_regular_new_pawn" style="border:1px solid green"></td>
									<td width="0" valign="bottom">		<input type="button" value="Approve"  	class="btn_regular" id="btnManAuth2"></td>

									<td width="0" valign="bottom">		<input type="button" value="Reset"  	class="btn_regular" id="btnResetManAuth2"></td>									
								</tr>
							</table>
						</div>
					</div>


					<div id="item_list_holder"></div>

					<div id="item_list_tot_holder">
						<div class="loan_dtl_contain_det" style="background-color: #f8f8f8">
							<div class="text_box_holder_new_pawn" style="width:135px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:175px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:140px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:110px;float:left"></div>
							<div class="text_box_holder_new_pawn" style="width:60px;float:left"></div>

							<div class="text_box_holder_new_pawn" style="width:50px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" id="tqty" style="font-family:'bitter';font-size: 20px;width:47px;background-color:#f8f8f8;font-size:16px; padding:0px;text-align:center;border:none" >								
							</div>

							<div class="text_box_holder_new_pawn gold" style="width:76px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" id="totalweight" name="totalweight" style="font-family:'bitter';font-size: 20px;width:63px;background-color:#f8f8f8;font-size:16px; padding:0px;text-align:center;border:none" >								
							</div>

							<div class="text_box_holder_new_pawn gold" style="width:73px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" id="totalpweight" style="font-family:'bitter';font-size: 20px;width:60px;background-color:#f8f8f8;font-size:16px; padding:0px;text-align:center;border:none">								
							</div>

							<div class="text_box_holder_new_pawn gold" style="width:73px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" id="totaldweight" style="font-family:'bitter';font-size: 20px;width:60px;background-color:#f8f8f8;font-size:16px; padding:0px;text-align:center;border:none">								
							</div>

							<div class="text_box_holder_new_pawn" style="width:110px;float:left;padding:5px">							
								<input class="input_text_regular_new_pawn" type="text" name="goldvalue" id="goldvalue" style="font-family:'bitter';font-size: 20px;width:100px;background-color:#f8f8f8; text-align:right;font-size:16px; padding:0px;text-align:right;border:none">								
							</div>

							<div class="text_box_holder_new_pawn" style="width:50px;float:left"></div>								
						</div>
					</div>
					<div style="border:0px solid red; margin-bottom:20px; margin-top:-10px; background-color:#f1f1f1">
						<table width="100%" style="border:0px solid red">
							<tr>
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:240px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn">Required Amount</span>
										<input class="input_text_large amount" type="text" style="width:230px;border:2px solid green;" id="requiredamount" name="requiredamount">
									</div>
								</td>
								
								<td style="border:0px solid red">
									<div class="text_box_holder_new_pawn" style="width:240px;border-bottom:1px solid #ccc">
										<div style="border:0px solid red; width:55px;float:left"><span class="text_box_title_holder_new_pawn">Interest</span></div>
										

										<div style="border:0px solid red; width:140px;float:right;position: absolute;margin-left: 90px; margin-top:-4px;display:none" >
											<label>												
												<div style="float:left" id="divchkh"><input type="checkbox" id="allow_cal_with_advance" name="allow_cal_with_advance" checked="checked"></div>
												<span style="position: absolute;font-size:12px; margin-top:4px">Interest with advance</span>
											</label>
										</div>

										<input class="input_text_large amount" type="text" id="fmintrest" name="fmintrest"  style="width:230px" readonly="readonly">								
									</div>
								</td>
								<td>							
									<div class="text_box_holder_new_pawn" style="width:240px;border-bottom:1px solid #ccc">
										<span class="text_box_title_holder_new_pawn" style="width: 100%"><span style="color: #999999">Payable Stamp Fee <span style="font-size:11px">(When redeem)</span></span></span>
										<input class="input_text_large" type="text" style="color:#cccccc;width:230px" readonly="readonly" name="stamp_fee" id="stamp_fee">
									</div>
								</td>
								<td>
									<div class="text_box_holder_new_pawn" style="width:340px;border-bottom:1px solid #ccc;background-color:#eaeaea">
										<span class="text_box_title_holder_new_pawn" style="width:350px">Net Payable Amount</span>
										<input class="input_text_large amount" type="text" id="net_payable_amount" style="width:328px" readonly="readonly">
										<input type="hidden" id="payable_amount">
									</div>
								</td>								
								<td width="100%" style="border-bottom:1px solid #ccc"></td>
							</tr>
							
						</table>
					</div>

					<div class="div_manager_auth_option">
						<div style="padding: 10px;padding-bottom: 15px">
							<table width="100%" border="0">
								<tr>
									<td width="500"><span style="color: red">Manager level approval required</span> <br><span style="font-size:13px">(Please enter branch manager password for approve extra amount)</span> </td>
									<td width="150" valign="top" style="padding-right: 10px"><span style="font-size:13px">Username</span> 	<input type="text" id="txt_man_un" 		class="input_text_regular_new_pawn" style="border:1px solid green;width: 100%"></td>
									<td width="150" style="padding-right: 10px"><span style="font-size:13px">Password</span> 				<input type="password" id="txt_man_pw" 		class="input_text_regular_new_pawn" style="border:1px solid green;width:100%"></td>
									<td width="300" valign="bottom"><input type="button" value="Approve"  	class="btn_regular" id="btnManAuth" style="border:1px solid green">&nbsp;&nbsp;&nbsp;<input type="button" value="Forward to Head Office"  	class="btn_regular" id="btnFrdHO" style="width: 150px;border:1px solid red"></td>
								</tr>
							</table>
						</div>
					</div>
					

					<div style="padding-left:8px" class="man_bn_c">
						<span style="color: #cccccc">Manual Billno</span><br>
						<input type="text" name="manual_billno" id="manual_billno" class="input_text_regular_new_pawn" style="border: 1px solid #eaeaea; width: 230px">
					</div>

					<div class="item_list_btn_holder">
						<input type="button" value="Save" class="btn_regular" id="btnSave">
						<!-- <input type="button" value="Delete" class="btn_regular" id="btnDelete"> -->
						<input type="button" value="Print" class="btn_regular_disable" id="btnPrint">

						

						<div style="float:right">
							<input type="button" value="Cancel" class="btn_regular_disable" id="btnCancel" disabled="disabled">
							<input type="button" value="Reset" 	class="btn_regular_disable" id="btnReset">
						</div>
						<input type="hidden" name="hid" id="hid" value="0">
						<input type="hidden" id="happid" value="0">
						<input type="hidden" id="is_discount_approved" value="0">
						<input type="hidden" id="is_cancel_approved"   value="0">						
						<input type="hidden" id="discount" value="0">
						<input type="hidden" id="approval_id" name="approval_id" value="0">
						<input type="hidden" id="hid_trans_no" value="">
						<input type="hidden" id="int_cal_changed" name="int_cal_changed" value="0">
						<input type="hidden" id="is_bulk" name="is_bulk" value="0">
						<input type="hidden" id="old_loan_no">
						<input type="hidden" id="extra_pawn_manager_allowed_amount" value="<?=$extra_pawn_manager_allowed_amount?>" >
						<input type="hidden" id="extra_requ_amount" value="0" >
						<input type="hidden" id="extra_approved_amount" name="extra_approved_amount" value="0" >
						
						<input type="hidden" id="is_quality_changed" value="0" >
						<input type="hidden" id="is_quality_changed_approved" value="0" >
						<input type="hidden" id="bcc" value="">
						<input type="hidden" id="is_renew" value="0">
						<input type="hidden" id="reduced_amount" value="0">
						<input type="hidden" id="previous_billno" value="">

						<input type="hidden" id="time" value="">
						

					</div>

				</td>

				<td valign="top" style="border-left:1px solid #ccc;background-color:#fcfcfc;width: 250px">

					<div class="div_right_details">
						
						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Information</a></div>
							<div class="div_new_pawn_info_holder" id="cus_info_div"></div>
						</div>

						<div>							
							<div style="padding:10px"><a class="slider_tab">Customer Pawn Information</a></div>
							<div class="div_new_pawn_info_holder" id="pawn_info">

								<div class="cph_c" style="float:left; margin-right:10px; margin-top:10px">
									<span class="cph_txt">Pawnings</span><br>
									<span class="cph_count cph_p" style="font-size:52px"></span>
								</div>
								<div class="cph_c" style="float:left; margin-right:10px; margin-top:10px">
									<span class="cph_txt">Redeems</span><br>
									<span class="cph_count cph_r" style="font-size:52px"></span>
								</div>
								<div class="cph_c" style="float:left; margin-right:10px">
									<span class="cph_txt">Forfiedted</span><br>
									<span class="cph_count cph_f" style="font-size:52px"></span>
								</div>
								<div class="cph_c" style="float:left; margin-right:10px">
									<span class="cph_txt">Canceled</span><br>
									<span class="cph_count cph_cn" style="font-size:52px"></span>
								</div>

							</div>
						</div>
						
						<div>							
							<div style="padding:10px"><a class="slider_tab">Bill Type Information</a></div>
							<div class="div_new_pawn_info_holder" id="billtype_info_div"></div>
						</div>
						
					</div>
				</td>
			</tr>
		</table>
	</form>

    <form id="first_int_print_pdf" action="<?php echo site_url(); ?>/reports/generate_i" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by_i' id='by_i' value='pawn_ticket_first_int' class="report" />            
            <input type="hidden" class="input_active_num" name="r_no_i" id="r_no_i" />
            <input type="hidden" class="input_active_num" name="fullmonth_int" id="fullmonth_int" value="0" />
        </div>                  
    </form> 
</div>
</body>
</html>