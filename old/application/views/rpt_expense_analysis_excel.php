<?php error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$this->excel->setActiveSheetIndex(0);
$this->excel->setHeading("Branch wise Expenses Analysis");

$r  =  $this->excel->NextRowNum();
$this->excel->getActiveSheet()->setCellValue('A'.$r,"Date From  " .$fd. "  To". $td);

$key    =   $this->excel->NextRowNum();

$c=$d=$j=$b=0; $expense_tot=(float)0; 
$sub_tot[]='';

$lettr = array("B","C","D","E", "F", "G","H", "I","J", "K","L", "M","N", "O","P", "Q","R", "S","T", "U","V", "W","X", "Y","Z",
               "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",
               "BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM","BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ",
               "CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM","CN","CO","CP","CQ","CR","CS","CT","CU","CV","CW","CX","CY","CZ",
               "DA","DB","DC","DD","DE","DF","DG","DH","DI","DJ","DK","DL","DM","DN","DO","DP","DQ","DR","DS","DT","DU","DV","DW","DX","DY","DZ",
               "EA","EB","EC","ED","EE","EF","EG","EH","EI","EJ","EK","EL","EM","EN","EO","EP","EQ","ER","ES","ET","EU","EV","EW","EX","EY","FZ",
               "FA","FB","FC","FD","FE","FF","FG","FH","FI","FJ","FK","FL","FM","FN","FO","FP","FQ","FR","FS","FT","FU","FV","FW","FX","FY","FZ",);

//=======================================months====================================================
//if($d_range=="months"){
    foreach ($branch as $value) {

        $branch=$value->bc." - ".$value->name;

        $y_bc=$value->bc;
        $y_branch[]= $y_bc; 
        $y_branches[]=  $branch; 
        $arr_con=count($y_branch);  // count array elenemts   

    }

    $arr_size=sizeof($y_branch);


    $r  =  $this->excel->NextRowNum();

    $this->excel->SetFont('A'.$r.":".'A'.$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue('A'.$r,"Expenses");


    $key  =  $this->excel->NextRowNum();

    for($x =0; $x <=$arr_size; $x++) {
       $this->excel->SetFont($lettr[$x].$r.":".$lettr[$x].$r,"BC",12,"","");   
       $this->excel->getActiveSheet()->setCellValue($lettr[$x].$r,$y_branches[$x]);
       
       $y++; $d=$x;
    }

    $this->excel->SetFont($lettr[$d].$r.":".$lettr[$d].$r,"BC",12,"","");   
    $this->excel->getActiveSheet()->setCellValue($lettr[$d].$r,"Total");
    $this->excel->SetBlank();
    $key++;


foreach($list as $value){ 
  
  $curr_bc=$value->bc;
  $exp1=$value->heading;

  if($exp1==$exp2){
      for($x=1; $x<$arr_size; $x++){
            if($y_branch[$x] ==$curr_bc){
               
                $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  (float)$value->expense);
              
                $expense_tot+=$value->expense; 
               
                $sub_tot[$x]+=(float)$value->expense;
                $exp2=$value->heading;
                $curr_bc=$value->bc;
                $b=$b; //current row value
                $c=$x+1; //next month row 
            }else{
               // $this->excel->getActiveSheet()->setCellValue($lettr[$x].$b,  number_format(0,2)); 
                  $curr_bc=$value->bc;
                $a=$x+1; $b=$b;
            }
      }

  }else{

    //==========================branch wise total===================================
    if($j==1){
        //vertical line- @the end of each branch end
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$expense_tot);
            
            $expense_tot=0;
            $key++; 
    }
    $j=1;
    //==============================================================================

    
    //----------------expense name-----------------------------
    $this->excel->getActiveSheet()->setCellValue("A".$key, $value->heading);
   

    for($x=0; $x<$arr_size; $x++){

       if($y_branch[$x] ==$value->bc){
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, (float)$value->expense);
              
            $expense_tot+=$value->expense; 
               
                $sub_tot[$x]+=(float)$value->expense;
                $exp2=$value->heading;
                $curr_bc=$value->bc;
            
            $c=$x+1; //next month row 
            $b=$key; //current row value


       }else{
      
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, number_format(0,2)); 
            $curr_bc=$value->bc;
            $exp2=$value->heading;
             $sub_tot[$x]+=0;
            $a=$x+1; $b=$b;
       }
    }
    $key++;

  }
           
}

//==========================branch wise total===================================
    if($j==1){
        //vertical line- @the end of each branch end
            $key=$key-1;
            $a=$a+0;
             $this->excel->SetFont($lettr[$d].$key.":".$lettr[$d].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$d].$key,$expense_tot);
            
            $expense_tot=0;
            $key++; 
    }
    $j=1;
//==============================================================================

//==========================branch expense Total===========================================

    
    $this->excel->SetBlank();
    $this->excel->SetFont("A".$key.":".$lettr[$d].$key,"B",12,"","");
    $this->excel->getActiveSheet()->setCellValue('A'.$key,"Expense Total");
    
    for($x=0; $x<$arr_size; $x++){
            $this->excel->SetFont($lettr[$x].$key.":".$lettr[$x].$key,"B",12,"","");
            $this->excel->getActiveSheet()->setCellValue($lettr[$x].$key, $sub_tot[$x]);
            $total+=$sub_tot[$x];
            $sub_tot[$x]=0;
            $a=$x+1; 
    }

    $this->excel->getActiveSheet()->setCellValue($lettr[$a].$key,  $total);
    $total=0;
    $key=$key+2;



//* - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - * * - 
//=======================================years====================================================


$this->excel->SetOutput(array("data"=>$this->excel,"title"=>$header));

?>