<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<div class="page_contain">
	<div class="page_main_title"><span>Branches</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_branches" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add New Branch</b>
				</td>
				<td>
					<b>Exist Branches</b>
				</td>			

			</tr>			
			
			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<div class="text_box_title_holder">Area</div> 
						<?=$area?>
					</div>
				</td>			
				<td rowspan="18" valign="top">
					<div class="exits_text_box_holder">
						<div class="text_box_title_holder" style="width:40px;border:0px solid red">Name</div> 
						<input class="nic_feild input_text_regular" style="width:194px" type="text" id="Search_bcname">
						<input type="button" value="Search"	name="btnSearch" id="btnSearch" class="btn_regular">					
					</div>					

					<div class="list_div">
						<!-- <?=$load_list?> -->
					</div>

				</td>
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder" style="height:40px">
						
						<div style="border:0px solid red;width:230px;float:left">
							<span class="text_box_title_holder">Branch Code</span> 
							<input class="input_text_regular SpRemove" type="text" name="bc" id="bc" maxlength="3" maxlength="3" style="width:100px">
						</div>

						<div style="border:0px solid red;width:230px;float:right">
							<span class="text_box_title_holder">Branch No</span> 
							<input class="input_text_regular SpRemove" type="text" name="bc_no" id="bc_no" maxlength="3" maxlength="3" style="width:100px" readonly="readonly">
						</div>
					</div>
				</td>							
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Branch Type</span> 
						
						<select id='bc_type' name='bc_type' class='input_ui_dropdown'>
							<option value=''>Select Branch Type</option>
							<option value='1'>Pawning Centers</option>
							<option value='2'>Audit Department</option>
							<option value='3'>Account Department</option>
						</select>

					</div>
				</td>							
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Branch Name</span> 
						<input class="input_text_regular" type="text" name="name" id="name" maxlength="100">
					</div>
				</td>							
			</tr>
			
			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Address</span> 
						<input class="input_text_regular" type="text" name="address" id="address" maxlength="200">
					</div>
				</td>			
			</tr>			

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Phone No</span> 
						<input class="contct_no_feild input_text_regular NumOnly" type="text" name="telno" id="telno" maxlength="10">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">FAX No</span> 
						<input class="contct_no_feild input_text_regular NumOnly" type="text" name="faxno" id="faxno" maxlength="10">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">E-mail</span> 
						<input class="email_feild input_text_regular" type="text" name="email" id="email" maxlength="50">
					</div>
				</td>			
			</tr>			
			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<div class="text_box_title_holder">Cash A/C</div> 
						
						<input type="hidden" name="cash_acc" id="cash_acc">
					</div>
				</td>	
			</tr>				
			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<div class="text_box_title_holder">Stock A/C</div> 
						
						<input type="hidden" name="stock_acc" id="stock_acc">
					</div>
				</td>	
			</tr>
			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<div class="text_box_title_holder">Interest A/C</div> 
						
						<input type="hidden" name="interest_acc" id="interest_acc">
					</div>
				</td>	
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-10px">Max Cash Balance</span> 
						<input class="contct_no_feild input_text_regular amount" type="text" name="bc_max_cash_bal" id="bc_max_cash_bal" maxlength="10"  value="<?=$default_max_min_cash_balance->bc_max_cash_bal?>">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-10px">Min Cash Balance</span> 
						<input class="contct_no_feild input_text_regular amount" type="text" name="bc_min_cash_bal" id="bc_min_cash_bal" maxlength="10" value="<?=$default_max_min_cash_balance->bc_min_cash_bal?>">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<input type="checkbox" name="other_bc_name_allow" id="other_bc_name_allow">
						<span class="text_box_title_holder" style="margin-top:-20px">Swap Branch Name</span>
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<input type="checkbox" name="close_bc" id="close_bc">
						<span class="text_box_title_holder" style="margin-top:-20px">Closed Branch</span>
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:20px">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="margin-top:-10px">Pawning Start Date</span> 
						<input class="input_text_regular pawn_start_date" type="text" readonly="readonly" name="pawn_start_date" id="pawn_start_date">
					</div>
				</td>			
			</tr>

			


			<tr>
				<td style="height:20px"><br><br><br>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>							
			</tr>
			<tr>
				<td></td>				
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>