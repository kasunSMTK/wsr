<?php if($this->user_permissions->is_view('t_payable_invoice')){ ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

    $(function() {
        $(".input_date_down_future").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });

   

</script>
<!-- <script type="text/javascript" src="<?=base_url()?>js/t_payable_invoice.js"></script>-->
<div class="page_main_title"><span>Payable Invoice</span></div>
<!-- <h2 style="text-align: center;">Payable Invoice</h2> -->
<div class="msgBox">
    <div class="msgInner">Saving Success</div>
</div>
<div class="dframe" id="mframe">
    <form method="post" action="<?=base_url()?>index.php/main/save/t_payable_invoice" id="form_">
        <table style="width: 100%" border="0">
            <tr>
                <td style="width: 100px;">Payable Type</td>
                <td><input type="text" class="input_txt" id="sjournal_type" value="" style="width: 150px;" />
                    <input type="hidden" name="journal_type" id="journal_type" value="0" />
                    <input type="text" class="input_txt" value='' readonly="readonly" id="journal_type_des"  style="width: 300px;"/></td>
                <td style="width: 50px;">No</td>
                <td>
                    <input type="text" style="width:100%" class="input_active_num" name="id" id="id" value="<?=$max_no?>" />
                    <input type="hidden" id="hid" name="hid" value="0" />                </td>
            </tr>
			<tr>
                <td><span style="width: 100px;">Vendor Account </span></td>
                <td><input type="text" class="input_txt" id="saccount" value="" style="width: 150px;" />
                    <input type="hidden" name="account" id="account" value="0" />
                    <input type="text" class="input_txt" value='' readonly="readonly" id="account_des"  style="width: 300px;"/></td>
                <td style="width: 100px;">Date</td>
                <td style="width: 100px;">
                    <?php if($this->user_permissions->is_back_date('t_payable_invoice')){ ?>
                        <input type="text" class="input_date_down_future" readonly="readonly" name="date" id="date" value="<?=date('Y-m-d')?>" style="text-align:right;"/>
                    <?php } else { ?>
                        <input type="text" class="input_txt" readonly="readonly" name="date" id="date" value="<?=date('Y-m-d')?>" style="text-align:right;"/>
                    <?php } ?>    
                </td>
            </tr>
					<tr>
                <td><span style="width: 100px;">Payable Date </span></td>
                <td><input type="text" class="input_date_down_future" readonly="readonly" name="payable_date" id="payable_date" value="<?=date('Y-m-d')?>" /></td>
                <td style="width: 100px;">Ref. No</td>
                <td style="width: 100px;"><input type="text" class="input_txt" name="ref_no" id="ref_no" value="" style="width: 100%; text-align:right;" maxlength="10"/></td>
					</tr>
									<tr>
                <td><span style="width: 100px;">Description</span></td>
                <td><input name="description" type="text" class="input_txt" id="description"  style="width: 450px;" value='' maxlength="50"/></td>
                <td style="width: 100px;">&nbsp;</td>
                <td style="width: 80px;">&nbsp;</td>
									</tr>

            <tr>
                <td><!-- Narration --></td>
                <td><input name="narration" type="hidden" class="input_txt" id="narration"  style="width: 450px;" value='' maxlength="50" /></td>
                <td style="width: 100px;">&nbsp;</td>
                <td style="width: 100px;">&nbsp;</td>
            </tr>
        </table>
        
        <div class="tgrid" style="width:100%;">	
        <table style="width: 875px;" id="tgrid">
            <thead>
                <tr>
                    <th class="tb_head_th" style="width: 200px;">Account</th>
                    <th class="tb_head_th" style="width: 475px;">Description</th>
                    <th class="tb_head_th" style="width: 200px;">Amount</th>
                </tr>
            </thead><tbody>
                <?php

                    $y=$grid->value;
	                $xx=$option->autofill_payable;
                    
                    for($x=0; $x<$y; $x++){
                        echo "<tr>";
                            echo "<td><input type='hidden' name='h_".$x."' id='h_".$x."' value='0' />
                                 <input type='text' class='g_input_txt fo' id='0_".$x."' name='0_".$x."'  style='width:200px;' readonly='readonly'/></td>";
                            echo "<td style='background-color: #f9f9ec;' ><input type='text' class='g_input_txt'  id='n_".$x."' name='n_".$x."' style='width:455px;' readonly='readonly' maxlength='150'/></td>";
                            echo "<td><input type='text' class='g_input_amo cr amount' id='1_".$x."' name='1_".$x."' style='width:200px;' maxlength='11' /></td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
        </div>      
                    <div style="text-align: left; padding-top: 7px;">
                        <input type="button" class="btn_regular" id="btnExit" value="Exit" />
                        <input type="button" class="btn_regular" id="btnReset" value="Reset" />
                        <?php if($this->user_permissions->is_delete('t_payable_invoice')){ ?>
                        <input type="button" class="btn_regular" id="btnDelete1" value="Delete" />
                        <?php } ?>
                         <?php if($this->user_permissions->is_re_print('t_payable_invoice')){ ?>
                        <input type="button" class="btn_regular" id="btnPrint" value="Print" />
                        <?php } ?>
                        <?php if($this->user_permissions->is_add('t_payable_invoice')){ ?>
                        <input type="button"  class="btn_regular" id="btn_save_pi" value='Save <F8>' />
                        <?php } ?>
                        <span style="text-align:right; margin-left:116px; font-weight: bold;">Total
                            <input type="text" class="g_input_amo" name="total" id="total" value="0.00" style="width:245px; font-weight: bold;"/>
                            <input type="hidden" class="g_input_txt" name="grid_row" id="grid_row" value="<?=$y?>" style="width:100px;"/>
        					<input type="hidden" class="g_input_txt" name="autofill_payable" id="autofill_payable" value="<?=$xx?>" style="width:100px;"/>
                        </span>
                    </div>                
           <!--  </td>
            </tr> -->
            <?php 
                    if($this->user_permissions->is_print('t_payable_invoice')){ ?>
                    <input type="hidden" name='is_prnt' id='is_prnt' value="1" value="1">
                    <input type="hidden" name='time' id='time'>
                <?php } ?> 
       
    </form>
</div>
<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">

    <input type="hidden" name='by' value='t_payable_invoice' value="t_payable_invoice" class="report">
    <input type="hidden" name='page' value='A4' value="A4" >
    <input type="hidden" name='orientation' value='P' value="P" >
    <input type="hidden" name='type' value='' value="" >
    <input type="hidden" name='header' value='false' value="false" >
    <input type="hidden" name='qno' value='' value="" id="qno" >
    <input type="hidden" name='pblt' value='' value="" id="pblt" >
   <input type="hidden" name='clus' value='' value="" id="clus" >
    <input type="hidden" name='rep_sup' value='' value="" id="rep_sup" >
    <input type="hidden" name='rep_ship_bc' value='' value="" id="rep_ship_bc" >
    <input type="hidden" name='inv_date' value='' value="" id="inv_date" >
    <input type="hidden" name='inv_nop' value='' value="" id="inv_nop" >
    <input type="hidden" name='po_nop' value='' value="" id="po_nop" >
    <input type="hidden" name='po_dt' value='' value="" id="po_dt" >
    <input type="hidden" name='credit_prd' value='' value="" id="credit_prd" >
    <input type="hidden" name='rep_deliver_date' value='' value="" id="rep_deliver_date" >
    <input type="hidden" name='jtype' value='' value="" id="jtype" >
    <input type="hidden" name='jtype_desc' value='' value="" id="jtype_desc" >
    <input type="hidden" name='org_print' value='1' value="" id="org_print">

</form>
<?php } ?>

