<?php

	
	/*echo $bc_acc_op_bal['3020605'];
	exit;*/

	ini_set('memory_limit', '512M');
	ini_set('max_execution_time',600);

	$process_start_time = time();

	$make_excel = 1;
	
	$sacc = $sbc = '';	
	$bc_tot_DR = $bc_tot_CR = 0;
	$first_bc_passed = false;
	$next_acc_reached = false;
	$bc_bal = 0;
	$acc_with_OP_bal = array(3,4,5);

	$acc_tot_dr = $acc_tot_cr = $acc_tot_bal = 0;

	$tot_bc_acc_dr_bal = $tot_bc_acc_cr_bal = 0;
	$tot_bc_acc_bal = 0;

	$bc_accx = 0;

	$BAL_SHT_ACC_BAL = 0;
	$BAL_SHT_ACC_OP_BAL = 0;

	$is_first_pnl_acc = 0;

	$qax = 0;

	$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
  	$t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';

	foreach ($det as $r) {

		if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){
			$is_first_pnl_acc = 1;
		}

		if ($sacc != $r->acc_code){
			
			if ($first_bc_passed){
				
				/*$t .= '<tr>';
				$t .= '<td></td>';
				$t .= '<td></td>';
				$t .= '<td></td>';
				$t .= '<td style="font-weight:600">'.'Period Balance'.'</td>';
				$t .= '<td>'.d($bc_tot_DR).'</td>';
				$t .= '<td>'.d($bc_tot_CR).'</td>';
				$t .= '<td>'.d($bc_bal).'</td>';
				$t .= '</tr>';*/
				
				$t .= '<tr>';
				$t .= '<td style="font-weight:600;background-color:green;color:#ffffff"></td>';
				$t .= '<td style="font-weight:600;background-color:green;color:#ffffff"></td>';
				$t .= '<td style="font-weight:600;background-color:green;color:#ffffff"></td>';
				$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.strtoupper('Account Period Total and Balance').'</td>';
				$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($acc_tot_dr).'</td>';
				$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($acc_tot_cr).'</td>';

				if ($is_first_pnl_acc == 1){

					if ($qax == 0){
					$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($acc_tot_bal).'</td>';
					$qax = 1;
					}else{
					$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($BAL_SHT_ACC_BAL).'</td>';
					}

					$is_first_pnl_acc = 0;

					$acc_tot_bal = $BAL_SHT_ACC_BAL;

				}else{
					if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){
						$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($BAL_SHT_ACC_BAL).'</td>';
					}else{
						$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($acc_tot_bal).'</td>';
					}
				}


				//$t .= '<td style="font-weight:600;background-color:green;color:#ffffff"></td>';
				$t .= '</tr>';				

				$bc_tot_DR = $bc_tot_CR = 0;

				$acc_tot_bal = $acc_tot_dr = $acc_tot_cr = 0;

				$bc_bal = 0;

				$BAL_SHT_ACC_BAL = 0;
			}			
			
			$t .= '<tr><td colspan="7" style="font-size:30px">'.$r->acc_code.' - '.$r->acc_desc.'</td></tr>';
			
			$sacc = $r->acc_code;

			$next_acc_reached = true;

			$sbc = '';

		}


		if ($sbc != $r->branch_name){

			if ($next_acc_reached){
				$next_acc_reached = false;
			}else{

				if ($first_bc_passed){
					
					/*$t .= '<tr>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td style="font-weight:600">'. 'Period Balance'.'</td>';
					$t .= '<td>'. d($bc_tot_DR).'</td>';
					$t .= '<td>'. d($bc_tot_CR).'</td>';
					$t .= '<td>'. d($bc_bal).'</td>';
					$t .= '</tr>';*/

					$bc_tot_DR = $bc_tot_CR = 0;
				}

				if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){

					/*$t .= '<tr>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td style="font-weight:600">'.'Closing Balance'.'</td>';
					$t .= '<td></td>';
					$t .= '<td></td>';
					$t .= '<td>'.d($bc_bal).'</td>';
					$t .= '</tr>';						*/

				}


			}

			// set branch opening balance for each account			
				if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){
					
					if ($r->acc_code != $bc_accx){

						$bc_bal = $BAL_SHT_ACC_OP_BAL = floatval($bc_acc_op_bal[$r->acc_code]);
						$bc_accx	 = $r->acc_code;
					
					}

					// $bc_bal = 36720500.00;

					$tot_bc_acc_bal += $bc_bal;
					$tot_bc_acc_dr_bal += $bc_bal;
					$acc_tot_bal += $bc_bal;

				}else{
					//$bc_bal = 0;
				}
			// End set branch opening balance for each account

			
			
			//$t .= '<tr><td colspan="7" style="font-size:20px">'.$r->branch_name.'</td></tr>';
			
			$sbc = $r->branch_name;


			if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){

				if ($r->acc_code != $accc){

					$t .= '<tr>';
					$t .= '<td>'.''.'</td>';
					$t .= '<td>'.''.'</td>';
					$t .= '<td>'.''.'</td>';
					$t .= '<td style="font-weight:600">'.'Opening Balance'.'</td>';
					$t .= '<td>'.''.'</td>';
					$t .= '<td>'.''.'</td>';
					$t .= '<td>'.d($bc_bal).'</td>';
					//$t .= '<td></td>';
					$t .= '</tr>';

					$accc = $r->acc_code;

				}

			}

			/*$t .= '<tr style="font-weight:600">';
			$t .= '<td>Date</td>';
			$t .= '<td>Trans No</td>';
			$t .= '<td>Description</td>';
			$t .= '<td>Dr</td>';
			$t .= '<td>Cr</td>';
			$t .= '<td>Balance</td>';
			$t .= '</tr>';*/

			$first_bc_passed = true;

		}


		if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){

			$BAL_SHT_ACC_BAL += $BAL_SHT_ACC_OP_BAL;
			$BAL_SHT_ACC_BAL += $r->dr_amount;
			$BAL_SHT_ACC_BAL -= $r->cr_amount;
			$BAL_SHT_ACC_OP_BAL = 0;
		}



		$bc_bal += $r->dr_amount;
		$bc_bal -= $r->cr_amount;

		$tot_bc_acc_bal += $r->dr_amount;
		$tot_bc_acc_bal -= $r->cr_amount;

		$t .= '<tr>';
		$t .= '<td>'.$r->ddate.'</td>';
		$t .= '<td>'.$r->branch_name.'</td>';
		$t .= '<td>'.$r->trans_no.'</td>';
		$t .= '<td>'.$r->description.'</td>';
		$t .= '<td>'.d($r->dr_amount).'</td>';
		$t .= '<td>'.d($r->cr_amount).'</td>';
		$t .= '<td>'.d($bc_bal).'</td>';
		//$t .= '<td>'.d($BAL_SHT_ACC_BAL).'</td>';
		$t .= '</tr>';
		
		$bc_tot_DR += $r->dr_amount;
		$bc_tot_CR += $r->cr_amount;

		$acc_tot_dr += $r->dr_amount;
		$acc_tot_cr += $r->cr_amount;

		$acc_tot_bal += $r->dr_amount;
		$acc_tot_bal -= $r->cr_amount;

		$tot_bc_acc_dr_bal += $r->dr_amount;
		$tot_bc_acc_cr_bal -= $r->cr_amount;

	}


	

	$t .= '<tr>';
	$t .= '<td>'.''.'</td>';
	$t .= '<td>'.''.'</td>';
	$t .= '<td>'.''.'</td>';
	$t .= '<td style="font-weight:600">'.'Period Balance'.'</td>';
	$t .= '<td>'.d($bc_tot_DR).'</td>';
	$t .= '<td>'.d($bc_tot_CR).'</td>';
	$t .= '<td>'.d($bc_bal).'</td>';
	//$t .= '<td>'.''.'</td>';
	$t .= '</tr>';

	$bc_tot_DR = $bc_tot_CR = 0;

	if (in_array( substr($r->acc_code,0,1), $acc_with_OP_bal)){

		$t .= '<tr>';
		$t .= '<td>'.''.'</td>';
		$t .= '<td>'.''.'</td>';
		$t .= '<td>'.''.'</td>';
		$t .= '<td style="font-weight:600">'.'Closing Balance'.'</td>';
		$t .= '<td>'.''.'</td>';
		$t .= '<td>'.''.'</td>';
		$t .= '<td>'.d($bc_bal).'</td>';
		//$t .= '<td>'.''.'</td>';
		$t .= '</tr>';

	}


	
	$t .= '<tr>';
	$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'. ''.'</td>';
	$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'. ''.'</td>';
	$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'. ''.'</td>';
	$t .= '<td style="font-weight:600;background-color:green;color:#ffffff" style="font-weight:600">'.strtoupper('Account Period Total and Balance').'</td>';
	$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($acc_tot_dr).'</td>';
	$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($acc_tot_cr).'</td>';
	$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'.d($acc_tot_bal).'</td>';
	//$t .= '<td style="font-weight:600;background-color:green;color:#ffffff">'. ''.'</td>';
	$t .= '</tr>';
	



	//--------------------------------------ALL BC TOTAL -----------------

	/*$t .= '<tr>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff">'.''.'</td>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff">'.''.'</td>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff">'.''.'</td>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff" style="font-weight:600">'.strtoupper('All Account Period Total and Balance').'</td>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff">'.d($tot_bc_acc_dr_bal).'</td>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff">'.d($tot_bc_acc_cr_bal).'</td>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff">'.d($tot_bc_acc_bal).'</td>';
	$t .= '<td style="font-weight:600;background-color:blue;color:#ffffff">'.''.'</td>';
	$t .= '</tr>';*/

	//----------------------------------END ALL BC TOTAL -----------------
	
	
	$process_end_time = time();

	
	
	//$this->pdf->MultiCell(18, 0, ($process_end_time - $process_start_time) + $time_spend, '0','L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

	
	$t .= '</table>';

	if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'income_statement.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    exit;


	function d($a){
		return number_format($a,2);
	}

?>