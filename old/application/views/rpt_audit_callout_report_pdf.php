<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 28);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);
	$this->pdf->MultiCell(0, 0, "Audit Callout Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "As at " . $td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();

	$this->pdf->SetFont('helvetica', '', 8);	

	$this->pdf->MultiCell(10, 1, "S.No", 'B','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(13, 1, "Bill Type", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(22, 1, "Bill No", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Pawn Date", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "Item Details", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(22, 1, "Caratage", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(11, 1, "Quality", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Advance", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, "Pure W", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, "Weight", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, "TW", 'B','C', 0, 0, '', '', '', '', 0);
	
	$this->pdf->MultiCell(20, 1, "Rate Per souv", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, "Accrued Int", 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, "Approvals", 'B','L', 0, 1, '', '', '', '', 0);
	$this->pdf->SetFont('', '', 7);

	$no 				= 1;
	$tot_amt 			= $TWeight = 0;
	$sub_tot_loop_start = false;
	$first_r 			= false;
	$sub_tot_tw 		= $sub_tot_pw = $sub_tot_la = $sub_tot_souv = 0;
	$bn 				= "";
	$xx 				= '';
	$fr 				= false;

	$bill_count 		= $tot_arti_count = $bill_count_T = $tot_arti_count_T =  0;

	$_pw = $_w = $_tw 	= $ra_tot = 0;

	foreach($list as $r){
		
		$sub_tot_souv 	+= $r->rate_per_souv;
		$sub_adv_tot 	+= $r->requiredamount;
		$sub_pw_to		+= $r->pure_weight;
		$sub_tw_tot 	+= $r->total_weight;

		$_pw 			 += $r->pure_weight;
		$_w 			 += $r->total_weight;
		$_tw 			 += $r->total_weight;
		$souv 			 += $rate_per_souv;

		if ($r->bc != $st){
			
			if (sub_tot_loop_start){
				
				if ($first_r){
					
					$this->pdf->SetFont('helvetica', 'B', 7);	
	
					$this->pdf->MultiCell(11, 6, "", 'B','L', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(54, 6, " No of Bills: ".$bill_count_T, 'B','L', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(40, 6, "Total Article Count: ". $tot_arti_count_T, 'B','L', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(33, 6, "Total", 'B','R', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(20, 6, d($sub_tot_la), 'B','R', 0, 0, '', '', '', '', 0);					
					$this->pdf->MultiCell(15, 6, d3($sub_tot_pw), 'B','C', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(15, 6, d3($sub_tot_tw), 'B','C', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(15, 6, '', 'B','R', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(20, 6, '', 'B','R', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(20, 6, d(0), 'B','R', 0, 0, '', '', '', '', 0);
					$this->pdf->MultiCell(30, 6, '', 'B','L', 0, 1, '', '', '', '', 0);

					$sub_tot_tw = $sub_tot_pw = $sub_tot_la = $sub_tot_souv = $bill_count_T = $tot_arti_count_T = 0;
				}

				$first_r = true;
			}
			
			$bc_name = $r->bc_name;
			$st 	 = $r->bc;
			$sub_tot_loop_start = false;

			$this->pdf->SetFont('helvetica', 'B', 8);	
			$this->pdf->MultiCell(0,5, $bc_name, $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=5, $valign='M', $fitcell=false);		

		}else{			
			$bc_name = "";
			$sub_tot_loop_start = true;
		}



		if (!$fr){
			$btt = $r->billtype;
			$fr = true;

			$sub_adv_tot = $sub_pw_to = $sub_tw_tot = 0;

		}else{

			if ($btt == $r->billtype){
				$fr = true;
			}else{
				$fr = false;
			}

		}




		$this->pdf->SetFont('', '', 7);

		$h = 4 * (max(1,$this->pdf->getNumLines($r->bc_name,25),$this->pdf->getNumLines($r->itemname,40),$this->pdf->getNumLines($r->goldcatagory,22))); 
		
		$this->pdf->MultiCell(10, $h, $no , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);

		if ($bn != $r->billno){				
			
			$this->pdf->MultiCell(22, $h, $r->billno , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$bn = $r->billno;
			$apprl_by = $r->approvals;

			$bt = $r->billtype;
			$pd = $r->ddate;
			$ra = $r->requiredamount;
			$ra_tot += $ra;
			$sub_tot_la += $r->requiredamount;

			$tw = $r->tot_W;

			$rate_per_souv = (($r->requiredamount / $r->tot_W) * 8);
			
			$bill_count++;
			$bill_count_T++;

		}else{
			
			$this->pdf->MultiCell(22, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
			$apprl_by = '';

			$bt = '';
			$pd = '';
			$ra = '';
			$tw = '';
			$rate_per_souv = '';

		}
		
		$this->pdf->MultiCell(13, $h, $bt , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $pd , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(40, $h, $r->itemname.' ('.$r->qty.')' , $border='LRB', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(22, $h, $r->goldcatagory , $border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(11, $h, $r->quality , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, 1,  d($ra), 'B','R', 0, 0, '', '', '', '', 0);
		$this->pdf->MultiCell(15, $h, d3($r->pure_weight) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(15, $h, d3($r->total_weight) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		
		$sub_tot_tw 	+= $r->total_weight;
		$sub_tot_pw 	+= $r->pure_weight;

		if ($tw != ""){
			$this->pdf->MultiCell(15, $h, d3($tw) , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		}else{
			$this->pdf->MultiCell(15, $h, '' , $border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		}
		
		if ($rate_per_souv != ""){
			$this->pdf->MultiCell(20, $h, d($rate_per_souv) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		}else{
			$this->pdf->MultiCell(20, $h, '' , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		}
		
		$this->pdf->MultiCell(20, $h, d($r->accrued_int) , $border='B', $align='R', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(30, $h, $apprl_by , $border='B', $align='L', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);





		$no++;
		
		$TWeight += $r->total_weight;
		$PWeight += $r->pure_weight;

		$tot_arti_count 	+= $r->qty;
		$tot_arti_count_T 	+= $r->qty;
	}


	$this->pdf->SetFont('helvetica', 'B', 8);	

	$this->pdf->MultiCell(10, 1, "", 'B','C', 0, 0, '', '', false, '', 0);	
	$this->pdf->MultiCell(13, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(22, 1, "Total Bills", 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, $bill_count_T, 'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(40, 1, "", 'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(22, 1, "", 'B','L', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(11, 1, "Total", 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d($ra_tot), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($_pw), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($_w), 'B','C', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(15, 1, d3($_tw), 'B','C', 0, 0, '', '', '', '', 0);
	
	$this->pdf->MultiCell(20, 1, '', 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(20, 1, d(0), 'B','R', 0, 0, '', '', '', '', 0);
	$this->pdf->MultiCell(30, 1, "", 'B','L', 0, 1, '', '', '', '', 0);

	

	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', '');
	}

	$this->pdf->Output("PDF.pdf", 'I');

?>