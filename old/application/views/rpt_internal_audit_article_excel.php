<?php

		ini_set("max_execution_time",600);

        $make_excel = true;       
        
        $t  = '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';
        
        $t .= '<thead>';            
        
        $t .= '<tr>';            
        $t .= '<td colspan="10"><h2>Internal Audit Article</h2></td>';
        $t .= '</tr>';

        $t .= '<tr>';            
        $t .= '<td colspan="10"><h4>As at '.$td.' Branch : '.$bc_name.'</h4></td>';
        $t .= '</tr>';


        $t .= '<tr>';            
        $t .= '<td>No</td>';
        $t .= '<td>Bill Type</td>';
        $t .= '<td>Date</td>';
        $t .= '<td>Bill No</td>';
        $t .= '<td>Description</td>';
        

        $t .= '<td>P.Weight</td>';
        $t .= '<td>Weight</td>';
        

        $t .= '<td>B. Caratage</td>';
        
        $t .= '<td>T.Weight</td>';
        
        $t .= '<td>Amount</td>';
        $t .= '<td>Audit Comment</td>';
        $t .= '</tr>';

        $ra = $pw = $gw = $no = 0;


        foreach ($list as $r) {

        	if ($r->billno != $st){
        	
	        	$t .= '<tr>';            
		        $t .= '<td>'.($no+1).'</td>';
		        $t .= '<td>'.$r->billtype.'</td>';
		        $t .= '<td>'.$r->ddate.'</td>';
		        $t .= '<td>'.$r->billno.'</td>';
		        $t .= '<td>'.$r->itemname.'</td>';
		        
				
				$t .= '<td>'.$r->pure_weight.'</td>';
		        $t .= '<td>'.$r->goldweight.'</td>';
		        

		        $t .= '<td>'.$r->goldcatagory.'</td>';

		        $t .= '<td>'.$r->totalweight.'</td>';
		        
		        $t .= '<td>'.$r->requiredamount.'</td>';
		        $t .= '<td>'.$r->audit_comment.'</td>';
		        $t .= '</tr>';

		        $st = $r->billno;

		        $ra += $r->requiredamount;	        

		        $no++;

		   	}else{

		   		$t .= '<tr>';            
		        $t .= '<td></td>';
		        $t .= '<td></td>';
		        $t .= '<td></td>';
		        $t .= '<td></td>';
		        $t .= '<td>'.$r->itemname.'</td>';
		        

		        $t .= '<td>'.$r->pure_weight.'</td>';
		        $t .= '<td>'.$r->goldweight.'</td>';
		        

		        $t .= '<td>'.$r->goldcatagory.'</td>';
		        $t .= '<td></td>';
		        $t .= '<td></td>';
		        $t .= '<td>'.$r->audit_comment.'</td>';
		        $t .= '</tr>';

		   	}

		   	$pw += $r->pure_weight;
		   	$gw += $r->goldweight;

        }


			$t .= '<tr>';            
	        $t .= '<td></td>';
	        $t .= '<td></td>';
	        $t .= '<td></td>';
	        $t .= '<td></td>';
	        $t .= '<td></td>';
	        $t .= '<td>'.$pw.'</td>';
	        $t .= '<td>'.$gw.'</td>';
	        $t .= '<td></td>';
	        $t .= '<td></td>';
	        $t .= '<td>'.$ra.'</td>';
	        $t .= '<td></td>';
	        $t .= '</tr>';        

        $t .= '</table>';


        if (!$make_excel){
            echo $t;
        }else{
            header('Content-type: application/excel');
            $filename = 'Audit_article	.xls';
            header('Content-Disposition: attachment; filename='.$filename);
            $data = $t;
            echo $data;
        }



?>