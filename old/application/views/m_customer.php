<div class="page_contain">
	<div class="page_main_title"><span>Customer</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_customer" id="form_">
		<table cellpadding="0" cellspacing="0" border="0" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>
					<b>Add New Customer</b>
				</td>
				<td>
					<b>Exist Customers</b>
				</td>			
			</tr>		

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<div class="text_box_title_holder">Customer ID</div> 
						<input class="input_text_regular" type="text" name="customer_id" id="customer_id" readonly="readonly" value="" maxlength="20">
						<input  type="hidden" name="customer_no" id="customer_no" value="" maxlength="10">
					</div>
				</td>			
				<td rowspan="12" valign="top">
					<div class="exits_text_box_holder">
						<div class="text_box_title_holder" style="width:40px;border:0px solid red">NIC</div> 
						<input class="nic_feild input_text_regular" style="width:194px" type="text" id="Search_nicNo">
						<input type="button" value="Search"	name="btnSearch" id="btnSearch" class="btn_regular">					
					</div>
					
					<div class="list_div"></div>
				</td>
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder" style="padding-top:20px;">
						<span class="text_box_title_holder" style="margin-top:-10px">Title</span> 						
						<div style="margin-top:-5px">
							<label style="margin-right:50px;"><input type="radio" name="title" value="Mrs. "> Mrs</label>
							<label style="margin-right:50px;"><input type="radio" name="title" value="Miss. "> Miss</label>
							<label style="margin-right:00px;"><input type="radio" name="title" value="Mr. "> Mr</label>
						</div>						
					</div>
				</td>			
			</tr>

			<tr>				
				<td style="height:10px">
					<div class="text_box_holder">
						<div class="text_box_title_holder">NIC</div> 
						<input class="nic_feild input_text_regular SpRemove" type="text" name="nicno" id="nicno" maxlength="10">
					</div>
				</td>
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Full Name</span> 
						<input class="input_text_regular" type="text" name="cusname" id="cusname" maxlength="100">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Other Names</span> 
						<input class="input_text_regular" type="text" name="other_name" id="other_name" maxlength="100">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Address 1</span> 
						<input class="input_text_regular" type="text" name="address" id="address" maxlength="200">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Address 2</span> 
						<input class="input_text_regular" type="text" name="address2" id="address2" maxlength="150">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Mobile No</span> 
						<input class="contct_no_feild NumOnly input_text_regular" type="text" name="mobile" id="mobile" maxlength="10">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Phone No</span> 
						<input class="contct_no_feild NumOnly input_text_regular" type="text" name="telNo" id="telNo" maxlength="10">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:10px">
					<div class="text_box_holder">
						<span class="text_box_title_holder" style="width: 140px;">Max Allowed Pawning</span> 
						<input style="width: 335px" class="contct_no_feild NumOnly input_text_regular" type="text" name="max_allowed_pawn" id="max_allowed_pawn" maxlength="10">
					</div>
				</td>			
			</tr>

			<tr>
				<td style="height:10px">
					
				<label>					
					<div class="blacklist_chk"><input type="checkbox" id="isblackListed" name="isblackListed"></input>Blacklist this customer</div>
				</label>

				<textarea name="black_list_reason" class="blkreason" style="border:none;display: none; margin-left: -30px; width: 487px; padding: 10px; margin-left: 23px; background-color: #eaeaea;border:2px solid red;color: #000000 "></textarea>
					



				</div>

				<br><br>
				</td>				
			</tr>		

			<tr>
				<td style="height:10px">
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">					
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
					<input type="hidden" value="" 		name="bc" 		 id="bc" >
				</td>			
				<td>
					
				</td>
			</tr>
			<tr>
				<td></td>				
			</tr>		
			
		</table>

	</form>

</div>
</body>
</html>