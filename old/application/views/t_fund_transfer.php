<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $( ".date_ch_allow" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2025",
            onSelect: function(date){
                change_current_date(date);              
            }
        });
    });

    $(function() {
        $( ".date_ch_allow_n" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            minDate:'<?=$backdate_upto?>',
            maxDate:0,
            yearRange: "1930:2025",
            onSelect: function(date){
                change_current_date(date);              
            }
        });
    });

    function date_change_response_call(){
        $("#btnReset").click();
    }

</script>
    
    

    <div class="page_contain_new_pawn">
        <div class="page_main_title_new_pawn">
            <div style="width:240px;float:left"><span>Fund Transfer</span> - <?php if ($isHO){ echo "Head Office"; }else{ echo "Branch"; } ?> </div>                 
        

            <div style="float: right"><input class="input_text_regular_new_pawn <?=$date_change_allow?>" type="text" id="dDate" name="dDate" style="width:80px" readonly="readonly" value="<?=$current_date?>"></div>

        </div>
    </div>

    

        <table align="center" border="0" width="800" cellpadding="0" cellspacing="0">            
            <tr>
                <td>
                    <div id="tabs" style="box-shadow: none;border-top: none;">
                        <ul>
                            <li><a href="#tabs-1" class="x"><div class="">Make A New Request</div></a></li>
                            <li><a href="#tabs-2" class="x"><div class="">My Requests Status <span class="span_notify my_requ"><!-- <?=$my_requests['my_requ_rc']?> --></span></div></a></li>
                            <li><a href="#tabs-3" class="x"><div class="">Incoming Requests <span class="span_notify incomg_requ"><!-- <?=$incomming_requests['incmg_requ_rc']?> --></span></div></a></li>

                            <?php if ($isAdmin == 1){ ?>
                            <li><a href="#tabs-4" class="x"><div class="">Cancel a Fund Transfer</div></a></li>
                            <?php } ?>

                        </ul>
                    
                        <div id="tabs-1" style="box-shadow: none">
                            
                            <?php

                                if ($isHO){ ?>

                                    <form method="post" action="<?=base_url()?>index.php/main/save/t_fund_transfer" id="form_1">

                                        <div class="tab_selector_">

                                            <div class="tab_title_">Head Office <--- Branch</div>
                                            <div class="tab_content_">
                                                
                                                <div style="padding: 10px">

                                                    <table border="0" class="tbl_tt" width="840">

                                                        <tr>
                                                            <td>Transfer Type</td>
                                                            <td>Request from</td>
                                                            <td>Deposit to <span class="clx_acc_title">(Head Office Cash Account)</span></td>
                                                            <td align="right">Amount</td>
                                                            <td></td>
                                                        </tr>

                                                        <tr>
                                                            <td>                                                                   
                                                                <label><input type="radio" class="tt_opt" val="cash"           value="cash"            name="tt_opt">Cash</label> 
                                                                <label><input type="radio" class="tt_opt" val="bank_deposit"   value="bank_deposit"    name="tt_opt">Bank</label>                                                                
                                                            </td>
                                                            <td>                                                                                                                                    
                                                                <?=$bc_list_with_cash_acc_codes_f?>
                                                                <input type="hidden" name="from_bc"  class="fb"> <!-- update this hidden field when change the drop down -->
                                                                <input type="hidden" name="from_acc" class="fa"> <!-- update this hidden field when change the drop down -->
                                                            </td>
                                                            <td width='235'>                                                                   
                                                                <input type="hidden" value="HO" name="to_bc">
                                                                
                                                                <div class="div_bnk_dp_set">
                                                                <input type="text" value="<?=$ho_cash_acc?>" name="to_acc" readonly="readonly" class="class_to_acc input_text_regular_ftr" style="width:100%">
                                                                </div>
                                                                <input type="hidden" value="<?=$ho_cash_acc?>" id="hid_ho_cash_acc">
                                                            </td>

                                                            <td>
                                                                <input type="text" name="amount" class="input_text_regular_ftr amount">
                                                            </td>
                                                        
                                                            <td align="right">                                                                
                                                                <input type="button" class="btnSendRequest btn_regular" style="width: 100px" value="Send Rquest" form_id="form_1">
                                                                <input type="hidden" value="1" name="fund_tr_opt_no">                                                                
                                                                <input type="hidden" name="ref_no" class="input_text_regular_ftr">
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="5" style="padding-top: 15px">
                                                                Comment <div style="height: 10px"></div><textarea name="comment" class="input_text_regular_ftr" style="width: 100%"></textarea>
                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                            </div>

                                        </div>                                            
                                    </form>
                                    <br>
                                    <form method="post" action="<?=base_url()?>index.php/main/save/t_fund_transfer" id="form_2">

                                        <div class="tab_selector_">

                                                <div class="tab_title_">Head Office --->  Branch (A) ---> Branch (B)</div>
                                                <div class="tab_content_">
                                                    
                                                    <div style="padding: 10px">

                                                        <table border="0" class="tbl_tt" width="100%">
                                                            <tr>                                                                
                                                                <td>Transfer Type</td>
                                                                <td>Request from</td>
                                                                <td>Deposit to</td>
                                                                <td align="right">Amount</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label><input type="radio" class="tt_opt" val="cash"           value="cash"         name = "tt_opt" checked="checked">Cash</label>
                                                                </td>
                                                                <td>
                                                                    <?=$bc_list_with_cash_acc_codes_f?>
                                                                    <input type="hidden" name="from_bc"   class="fb"> <!-- update this hidden field when change the drop down -->
                                                                    <input type="hidden" name="from_acc"  class="fa"> <!-- update this hidden field when change the drop down -->
                                                                </td>
                                                                <td>
                                                                    <?=$bc_list_with_cash_acc_codes_t?>
                                                                    <input type="hidden" name="to_bc"     class="fb"> <!-- update this hidden field when change the drop down -->
                                                                    <input type="hidden" name="to_acc"    class="fa"> <!-- update this hidden field when change the drop down -->
                                                                </td>                                                

                                                                <td align="right">
                                                                    <input type="text" name="amount" class="input_text_regular_ftr amount">
                                                                </td>
                                                            
                                                                <td>
                                                                    <input type="button" class="btnSendRequest btn_regular" value="Send Rquest" form_id="form_2" style="width:100px">
                                                                    <input type="hidden" value="2" name="fund_tr_opt_no">
                                                                    <input type="hidden" name="ref_no">
                                                                </td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="5" style="padding-top: 15px">
                                                                    Comment <div style="height: 10px"></div>
                                                                    <textarea name="comment" class="input_text_regular_ftr" style="width: 100%"></textarea>
                                                                </td>
                                                            </tr>

                                                        </table>

                                                    </div>
                                                </div>
                                        </div>                                       
                                    </form>
                                    <br>
                                    <form method="post" action="<?=base_url()?>index.php/main/save/t_fund_transfer" id="form_5">

                                        <div class="tab_selector_">

                                                <div class="tab_title_">Bank to Bank</div>
                                                <div class="tab_content_">
                                                    
                                                    <div style="padding: 10px">

                                                        <table border="0" class="tbl_tt" width="100%">
                                                            <tr>                                                                
                                                                <td>Transfer Type</td>
                                                                <td>From</td>
                                                                <td>To</td>
                                                                <td align="right">Amount</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label><input type="radio" class="tt_opt" val="cash"           value="cash"         name = "tt_opt" checked="checked">Cash</label>
                                                                </td>
                                                                <td>
                                                                    <?=$fnud_tr_bnk_dd_from?>
                                                                    <input type="hidden" name="from_bc"   class="fb"> <!-- update this hidden field when change the drop down -->
                                                                    <input type="hidden" name="from_acc"  class="fa"> <!-- update this hidden field when change the drop down -->
                                                                </td>
                                                                <td>
                                                                    <?=$fnud_tr_bnk_dd_to?>
                                                                    <input type="hidden" name="to_bc"     class="fb"> <!-- update this hidden field when change the drop down -->
                                                                    <input type="hidden" name="to_acc"    class="fa"> <!-- update this hidden field when change the drop down -->
                                                                </td>                                                

                                                                <td align="right">
                                                                    <input type="text" name="amount" class="input_text_regular_ftr amount">
                                                                </td>
                                                            
                                                                <td>
                                                                    <input type="button" class="btnSendRequest btn_regular" value="Send to Approval" form_id="form_5" style="width:120px">
                                                                    <input type="hidden" value="5" name="fund_tr_opt_no">
                                                                    <input type="hidden" name="ref_no">
                                                                </td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="5" style="padding-top: 15px">
                                                                    Comment <div style="height: 10px"></div>
                                                                    <textarea name="comment" class="input_text_regular_ftr" style="width: 100%"></textarea>
                                                                </td>
                                                            </tr>

                                                        </table>

                                                    </div>
                                                </div>
                                        </div>                                       
                                    </form>



                            <?php }else{ ?>


                                <form method="post" action="<?=base_url()?>index.php/main/load_data/t_fund_transfer/send_cheque_withdraw_request" id="form_3">

                                        <div class="tab_selector_">

                                            <div class="tab_title_cheque">Cheque Withdrawal Request</div>
                                            <div class="tab_content_cheque_">
                                                
                                                <div style="padding: 10px">

                                                    <table class="tbl_incmg_req" align="center" border="0" width="1000" cellpadding="0" cellspacing="0">    
                                                        <thead>
                                                            <tr>                                                                                               
                                                                <td>Account Description</td>            
                                                                <td align='center'>Cheque Number</td>
                                                                <td align='right'>Amount</td>
                                                                <td align='right'>Comment</td>
                                                                <td align='right'>Action</td>            
                                                            </tr>
                                                        </thead>                                                        
                                                        <?= $cash_cheque_grid ?>
                                                        </table>
                                                        <input type="hidden" id="tt_opt" value="cheque_withdraw">
                                                        <input type="hidden" id="from_bc" value="<?=$headoffice_code?>">
                                                </div>

                                            </div>

                                        </div>                                            
                                </form>
                                <br>
                                <form method="post" action="<?=base_url()?>index.php/main/save/t_fund_transfer" id="form_4">

                                        <div class="tab_selector_">

                                                <div class="tab_title_">Request Cash from Head Office</div>
                                                <div class="tab_content_">
                                                    
                                                    <div style="padding: 10px">

                                                        <table border="0" class="tbl_tt">

                                                            <tr>
                                                                <td width="150">Transfer Type</td>
                                                                <td>Request from</td>
                                                                <!-- <td>Deposit to <span class="clx_acc_title">(Head Office Cash Account)</span></td> -->
                                                                <td align="right">Amount</td>
                                                                <td></td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <label><input type="radio" class="tt_opt" val="cash"           value="cash"         name = "tt_opt" checked="checked">Cash</label>
                                                                </td>
                                                                <td>                                                                    
                                                                    <input type="text" value="<?=$headoffice_code?>" class="input_text_regular_ftr">
                                                                    <input type="hidden" name="from_bc"  value="<?=$headoffice_code?>">
                                                                    <input type="hidden" name="from_acc">                                                                    
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="amount" class="input_text_regular_ftr amount">
                                                                </td>
                                                            
                                                                <td>                                                                    
                                                                    <input type="button" class="btnSendRequest btn_regular" style="width: 100px" value="Send Rquest" form_id="form_4">
                                                                    <input type="hidden" value="4" name="fund_tr_opt_no">
                                                                    <input type="hidden" name="ref_no">
                                                                    <input type="hidden" name="to_bc">
                                                                    <input type="hidden" name="to_acc">                                                                    
                                                                </td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="5" style="padding-top: 15px">
                                                                    Comment <div style="height: 10px"></div>
                                                                    <textarea name="comment" class="input_text_regular_ftr" style="width: 100%"></textarea>
                                                                </td>
                                                            </tr>

                                                        </table>

                                                    </div>
                                                </div>
                                        </div>                                       
                                </form>

                            <?php } ?>
                        </div>
        
                        <div id="tabs-2" style="box-shadow: none">

                            <table class="tbl_incmg_req" align="center" border="0" width="1000" cellpadding="0" cellspacing="0">    
                            <thead>
                                <tr>                                
                                    <td>No</td>            
                                    <td>Description</td>            
                                    <td>Transfer&nbsp;To</td>
                                    <td>Transfer&nbsp;Type</td>
                                    <td>Bank&nbsp;Account</td>
                                    <td align='right'>Amount</td>                                
                                    <td>Date&nbsp;and&nbsp;Time</td>            
                                    <td>Action</td>            
                                </tr>
                            </thead>

                            <?php if ($isHO){ ?>
                                <?=$my_requests_HO['d']?>
                            <?php }else{ ?>
                                <?=$my_requests['d']?>
                            <?php } ?>
                            </table>
                        </div>
            
                        <div id="tabs-3" style="box-shadow: none">
                            <table class="tbl_incmg_req" align="center" border="0" width="1000" cellpadding="0" cellspacing="0">    
                            <thead>
                                <tr>                                
                                    <td>No</td>            
                                    <td>Description</td>            
                                    <td>Transfer&nbsp;To</td>
                                    <td>Transfer&nbsp;Type</td>
                                    <td>Bank&nbsp;Account</td>
                                    <td align='right'>Amount</td>                                
                                    <td>Date&nbsp;and&nbsp;Time</td>            
                                    <td>Action</td>            
                                </tr>
                            </thead>

                            <?php if ($isHO){ ?>
                                <?=$incomming_requests_HO['d']?>
                            <?php }else{ ?>
                                <?=$incomming_requests['d']?>                            
                            <?php } ?>

                            </table>
                        </div>


                        <?php if ($isAdmin == 1){ ?>
                            
                            <div id="tabs-4" style="box-shadow: none">                            
                                <div class="rpt_form" style="padding-top: 20px">                                    
                                    
                                    <div style="padding-left: 10px;">
                                        <span>Transfer Number</span><br>
                                        <input type="text" id="transfer_no" class="input_text_regular_ftr">
                                        <input type="button" class="btn_regular btn_get_fund_transfer_data" value="Get Data">
                                    </div>

                                    <div class="div_fntr_det">                                            

                                        <div class="div_pp_det_item" style="border-bottom: 1px solid #eaeaea">
                                            <div>Transfer Type</div>
                                            <span class="it tt"></span>                                        
                                        </div>

                                        <div class="div_pp_det_item" style="border-bottom: 1px solid #eaeaea">
                                            <div>Amount</div>
                                            <span class="it aa"></span>                                        
                                        </div>

                                        <div class="div_pp_det_item" style="border-bottom: 1px solid #eaeaea">
                                            <div>From</div>
                                            <span class="it ff"></span>                                        
                                        </div>

                                        <div class="div_pp_det_item" style="border-bottom: 1px solid #eaeaea">
                                            <div>To</div>
                                            <span class="it tb"></span>                                        
                                        </div>


                                        <div class="div_pp_det_item" style="border-bottom: 1px solid #eaeaea">
                                            <div>Status</div>
                                            <span class="it ss"></span>                                        
                                        </div>

                                        <div class="div_pp_det_item" style="border-bottom: 1px solid #eaeaea">
                                            <div>Comment</div>
                                            <span class="it cc"></span>                                        
                                        </div>

                                        <div class="div_pp_det_item" style="border-bottom: 0px solid #eaeaea">
                                            <input type="button" class="btn_reg_reject_hover_highlight btn_cancel_fund_transfer" value="Cancel">
                                        </div>

                                        
                                    </div>
                                    
                                </div>
                            </div>

                        <?php } ?>
        
                    </div>
    
                </td>
            </tr>
        </table>




    <form id="print_fund_trans_voucher_pdf" action="<?php echo site_url(); ?>/reports/generate_fund_tr_voucher" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
            <input type='hidden' name='by' id='by' value='fund_tr_voucher' class="report" />            
            <input type="hidden" name="v_no" id="v_no" />            
            <input type="hidden" name="is_v_re_p" id="is_v_re_p" />            
        </div>                  
    </form>


</body>
</html>