<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script src="<?=base_url()?>js/custom.js"></script>

<style>
form { 
	display: block;	
} 

#progress { 
	position:relative; 
	width:400px; 
	border-bottom: 1px solid #ddd; 
	padding: 1px; 
	border-radius: 3px; 
} 

#bar { 
	background-color: Green; 
	width:0%; 
	height:2px;
} 

.hide{
	display:none;
} 

input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
    border-radius: 5px;
}

.upload_holder{
	border: 0px solid red;
	text-align: center;
	width:400px;
	height: 50px;
	position: absolute;
	top:50%;
	margin-top: -25px;
	left: 50%;
	margin-left: -200px;
}

.x{
	position: absolute;
	top: 55%;
	border: 0px solid red;
	height: 155px;
	text-align: center;
	left: 50%;
	margin-left: -200px;
}

#percent { 
	font-size: 152px;
	border:0px solid red;
	width: 400px;
}

#percent_msg { 
	font-size: 14px;
	border:0px solid red;
	width: 400px;	
	display: none;
}

</style>

<div class="page_contain_new_pawn">
    
    <div class="page_main_title_new_pawn">
        <div style="width:250px;float:left"><span>Attendance Process</span></div>     
    </div>

	<div class="upload_holder">
		<form id="myForm" action="<?=base_url()?>index.php/main/save/t_attendance_process" method="post" enctype="multipart/form-data"> 

			<label for="file" class="custom-file-upload"> <i class="fa fa-cloud-upload"></i> Upload </label> <input id="file" type="file" size="60" name="file" class="file_input_hidden"> </form>
		<br>
		<div id="progress" class="hide"> 
			<div id="bar"></div>			
		</div>		
		<br/>	
		<div id="message"></div>

		

	</div>

	<div class="x">
		<div id="percent"></div>
		<div id="percent_msg">Upload Sucess</div>
	</div>
	

</div>
</body>
</html>