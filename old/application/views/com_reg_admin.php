<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $( "#date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2050"
        });
    });

</script>   

    <div class="page_contain_new_pawn">
        <div class="page_main_title_new_pawn">
            <div style="width:240px;float:left"><span>Branch PC Register</span></div>                 
        </div>
    </div><br><br>

    <table align="center" border="0" width="400" cellpadding="0" cellspacing="0">            
        <tr>
            <td>
                
                Branch Code<br><br>
                <input class="input_text_large" type="text" style="text-align:center;width:100%;border:2px solid green;" id="bc_code" maxlength="10" ><br><br>
                
                <div style="text-align:right"><input value="Generate Code" class="btn_regular" id="btnGetCode" type="button" style="width:100%; padding:10px"></div><br><br><br>

                Register Code<br><br>
                <input class="input_text_large" type="text" style="text-align:center;width:100%;border:2px solid green;" id="reg_code"  maxlength="10"><br><br>

            </td>                
        </tr>
    </table>

</body>
</html>