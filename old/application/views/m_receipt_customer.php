<div class="page_contain">
	<div class="page_main_title"><span>Receipt Customer</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_receipt_customer" id="form_">

		<table border="0" align="center" width="1000">
		
			<tr>
				<td>
					<b>Add New Receipt Customer</b>
				</td>				
			</tr>

			<tr>
				<td>
					<div class="text_box_holder">
						<span class="text_box_title_holder">Name</span> 
						<input class="input_text_regular" type="text" name="name" id="name" tabindex="1"/>
					</div>
				</td>
			</tr>
			<tr>
				<td height="50">
					<div class="text_box_holder">
						<span class="text_box_title_holder">NIC</span> 
						<input class="input_text_regular" type="text" name="nic" id="nic" maxlength="50" tabindex="2"/>
					</div>
				</td>
			</tr>
			<tr>
				<td height="50">
					<div class="text_box_holder">
						<span class="text_box_title_holder">Description</span> 
						<input class="input_text_regular" type="text" name="description" id="description" maxlength="50" tabindex="2"/>
					</div>
				</td>
			</tr>
			<tr>
				<td height="50">
					<div class="text_box_holder">
						<div style="margin-bottom: 8px"><span class="text_box_title_holder" style="width: 100px;">Cash Customer</span> </div>
						<input type="checkbox" name="is_cash_customer" id="is_cash_customer"/>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top" style="padding-left:10px">
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">	
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>
			</tr>
					

		</table><br><br>

		<table border="0" align="center" width="1000">

			<tr>
				<td></td>

				<td align="center" width="10">
					Search
				</td>

				<td width="50">
					<div class="exits_text_box_holder">						
						<input class="nic_feild input_text_regular" type="text" id="Search_areaname" style="width: 100%">
					</div>
				</td>

				<td width="10">
					<input type="button" value="Search"	name="btnSearch" id="btnSearch" class="btn_regular">		
				</td>

			</tr>

			<tr>
				<td colspan="4"><br><br></td>
			</tr>

			<tr>
				<td colspan="4">
					<div class="list_div"></div>
				</td>				
			</tr>

		</table>		

	</form>

</div>
</body>
</html>