<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$page_title?></title>	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css" />
	<script type="text/javascript" src="<?=base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/<?=$js_file_name?>.js"></script>

	<style>

	body{
		margin: 0px;
	}

	</style>

</head>

<body>

