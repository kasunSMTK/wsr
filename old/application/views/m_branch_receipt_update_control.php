<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script> 

	$(function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2050"
		});
	});	


	$(function() {
		$( "#valid_upto,#backdate_upto" ).datepicker({
			timeOnly: false,                                   
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm tt',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2050"
		});
	});	

</script>

<style type="text/css">
	

.dropdown {
  
}

a {
  color: #000;
}

.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: 0 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #000;
  text-decoration: none;
  outline: none;
  font-size: 12px;
}

.dropdown dt a {
  background-color: #ffffff;
  display: block;
  padding: 0px 10px 0px 10px ;
  min-height: 25px;
  line-height: 24px;
  overflow: hidden;
  border: 0;
  width: 272px;

  border: 2px solid Green;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
  display: inline-block;
  padding: 0 3px 2px 0;
}

.dropdown dd ul {
  background-color: #ffffff;
  border: 1px solid #cccccc;
  color: #000;
  display: none;
  left: 0px;
  padding: 10px;

  position: absolute;
  top: 2px;
  width: 272px;
  list-style: none;
  height: 300px;
  overflow: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #000;
}

button {
  background-color: #6BBE92;
  width: 302px;
  border: 0;
  padding: 10px 0;
  margin: 5px 0;
  text-align: center;
  color: #000;
  font-weight: bold;
}

</style>

<div class="page_contain">
	<div class="page_main_title"><span>Branch Receipt Edit Control</span></div>

	<form method="post" action="<?=base_url()?>index.php/main/save/m_branch_receipt_update_control" id="form_">

		<table cellpadding="0" cellspacing="0" border="01" class="tbl_master" align="right" style="margin:auto">
			<tr>
				<td align="right">No</td>
				<td>
					<input type="text" style="padding:5px;font-size:16px;border:1px solid #eaeaea;border-radius: 4px;width: 100px" id="no" class="input_text_regular" value="<?=$max_no?>">
				</td>
			</tr>
			<tr>
				<td align="right">Date</td>
				<td>
					<input type="text" style="padding:5px;font-size:16px;border:1px solid #eaeaea;border-radius: 4px;width: 100px" id="date" name="date" class="input_text_regular" value="<?=date('Y-m-d')?>">
				</td>
			</tr>
		</table>	

		<table cellpadding="0" cellspacing="0" border="01" class="tbl_master" align="center" style="margin:auto">

			<tr>
				<td>Branch</td>
			</tr>
			<tr>
				<td>
					<dl class="dropdown">

					  <dt>
					    <a href="#" style="width:100%">

						    <?php if ( $is_bc_user != '' ){ ?>								      	
						      	<p class="multiSel"><?= $is_bc_user ?></p>  
						   	<?php }else{ ?>
						   		<span class="hida">Select</span>    
						      	<p class="multiSel"></p>
						   	<?php } ?>

					    </a>
					  </dt>

					  <dd>
					    <div class="mutliSelect">
					      <ul>

					      	<?=$bc_dropdown_multi?>

					      </ul>
					    </div>
					  </dd>						  

					</dl>

				</td>
			</tr>

			<tr>
				<td><!-- <br>Number of Days Back --></td>
			</tr>
			<tr>
				<td>
					<input type="hidden" style="border:1px solid #eaeaea;border-radius: 4px" id="backdate_upto" name="backdate_upto" class="input_text_regular" value="<?=date('Y-m-d')?>">
				</td>
			</tr>

			<tr>
				<td><br>Valid Until</td>
			</tr>
			<tr>
				<td>
					<input type="text" style="border:1px solid #eaeaea;border-radius: 4px" id="valid_upto" name="valid_upto" class="input_text_regular" value="<?=date('Y-m-d')?>" readonly='readonly'>
				</td>
			</tr>

			<tr>
				<td><br>Reason</td>
			</tr>
			<tr>
				<td>
					<textarea name="desc" id="desc" style="width:100%"></textarea>
				</td>
			</tr>

			<tr>
				<td height="10" valign="top"><br>
					<input type="button" value="Save" 	name="btnSave" 	 id="btnSave" 	class="btn_regular">
					<input type="button" value="End" 	name="btnEnd" 	 id="btnEnd" 	class="btn_regular_disable" disabled="disabled">
					<input type="hidden" value="0" 		name="hid" 		 id="hid" >
				</td>							
			</tr>

		</table>

		<table cellpadding="0" cellspacing="0" border="01" class="tbl_master" align="center" width="100%">
			<tr>
				<td valign="top"><br><br><br><b><div class="page_main_title"><span>Receipt Edit Status</span></div></b><br><br>
					

				</td>							
			</tr>		
		</table>

		<div class="backdate_list"></div>

	</form>

</div>

<script type="text/javascript">
		/*
	Dropdown with Multiple checkbox select with jQuery - May 27, 2013
	(c) 2013 @ElmahdiMahmoud
	license: http://www.opensource.org/licenses/mit-license.php
*/

$(".dropdown dt a").on('click', function() {
  $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function() {
  $(".dropdown dd ul").hide();
});

$(document).on("click",".all_bc_select",function(){

	if ( $(this).is(":checked") ){

		$(".mutliSelect").find('li').each(function(){
			$(this).find('input[type="checkbox"]').prop("checked",true);
		});

		$(".multiSel").html("<div class='all_bc_div'>All Branches <input type='hidden' value='' name='bc_n'></div>");

	}else{

		$(".mutliSelect").find('li').each(function(){
			$(this).find('input[type="checkbox"]').prop("checked",false);
		});

		$(".multiSel").find('.all_bc_div').remove();


	}

});

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});


/*$(document).on("click",".bc_each",function(){

	var unchecked = false;

	$(".bc_each").each(function(){

		if ($(this).is(":checked")){

			var title = $(this).attr("title");
			var html = '<span title="' + title + '">' + title + '</span><input type="hidden" name="bc_arry[]" value="' + title + '" title="' + title + '">';
    		
			$('.multiSel').append(html);

		}else{
			unchecked = true;
		}
	});

	if (unchecked){
		$(".all_bc_select").prop("checked",false);
	}

});*/

$('.mutliSelect input[type="checkbox"]').on('click', function() {

  $(".multiSel").parent().find('.all_bc_div').remove();

  var 	title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    	title = $(this).val() + ",";

  if ($(this).is(':checked')) {
    
    var html = '<span title="' + title + '">' + title + '</span><input type="hidden" name="bc_arry[]" value="' + title + '" title="' + title + '">';
    
    $('.multiSel').append(html);
    $(".hida").hide();
  
  } else {
    
    $('span[title="' + title + '"]').remove();
    $('input[title="' + title + '"]').remove();
    var ret = $(".hida");
    
    $('.dropdown dt a').append(ret);

  }

});
	</script>
</body>
</html>