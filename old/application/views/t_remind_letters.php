<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

	$(function() {
		$( "#fd,#td" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,			
			maxDate : '0',
			onSelect: function(date){				
				
			}
		});
	});

</script>

<div class="page_contain_new_pawn">
	<div class="page_main_title_new_pawn">
		<div style="width:150px;float:left"><span>Remind Letters</span></div>		
	</div>

	<!--<br><br><br><br>
	 <div style="text-align: center;">Remind letters print option temporary disabled. Please contact head office IT section</div> -->



	<form method="post" action="<?=base_url()?>index.php/main/save/t_remind_letters" id="form_">
		
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbl_new_pawn" style="margin:auto" width="800">
			<tr>
				<td valign="top" width="20"><div class="loan_dtl_contain" style="background-color: #f1f1f1"></div></td>
				<td valign="top" width="1000">

					<div class="loan_dtl_contain" style="background-color: #f1f1f1">

						<div class="text_box_holder_new_pawn" style="width:115px;float:left">
							<span class="text_box_title_holder_new_pawn">From Date</span>
							<input class="input_text_regular_new_pawn" type="text" id="fd" name="fd" style="width:100px" readonly="readonly" value="<?=date('Y-m-d')?>">
						</div>

						<div class="text_box_holder_new_pawn" style="width:115px;float:left">
							<span class="text_box_title_holder_new_pawn">To Date</span>
							<input class="input_text_regular_new_pawn" type="text" id="td" name="td" style="width:100px" readonly="readonly" value="<?=date('Y-m-d')?>">
						</div>

						<div class="text_box_holder_new_pawn" style="width:195px;float:left">
							<span class="text_box_title_holder_new_pawn">Bill Type</span>							
							<select class="select_drp" style="width:185px" id="billtype" name="billtype">
								<?=$bt?>
							</select>
						</div>

						<div class="text_box_holder_new_pawn" style="width:166px;float:left">
							<span class="text_box_title_holder_new_pawn">Letter Number</span>
							<select class="select_drp" style="width:155px" id="letter_no" name="letter_no">
								<option value="0">Select</option>
								<option value="1">1st</option>
								<option value="2">2nd</option>
								<option value="3">3rd</option>
							</select>

							<input type="hidden" name="days_to_cal" id="days_to_cal" value="0">

						</div>
						

						<div class="text_box_holder_new_pawn" style="width:155px;float:left">
							<span class="text_box_title_holder_new_pawn">Action</span>
							<input type="button" value="Generate " class="btn_regular" style="width:80px" id="btnGet">
							<input type="button" value="Load" class="btn_regular" style="width:60px" id="btnLoad">
						</div>

						<div class="text_box_holder_new_pawn" style="width:62px;float:left">
							<span class="text_box_title_holder_new_pawn">No</span>
							<input class="input_text_regular_new_pawn" type="text" id="no" style="width:50px" value="<?=$no?>">
							<input type="hidden" name="hid" id="hid" value="0">
						</div>

						<div class="text_box_holder_new_pawn" style="width:115px;float:left">
							<span class="text_box_title_holder_new_pawn">Date</span>
							<input class="input_text_regular_new_pawn" type="text" id="date" name="date" style="width:100px" readonly="readonly" value="<?= date('Y-m-d') ?>"
						</div>

					</div>				

				</td>

				<td valign="top" align="right">
					<div class="loan_dtl_contain" style="background-color: #f1f1f1"></div>
				</td>

			</tr>

		</table>
		
		<div class="letter_list"></div>					

		<div class="text_box_holder_new_pawn" style="width:72px;float:left">			
			<input type="button" value="Save" class="btn_regular" style="width:60px" id="btnSave">
		</div>

	</form>

	<form id="print_remind_letters" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' id='by' value='remind_letter'/>            
            <input type="hidden" name="l_no" id="l_no" />            
        </div>                  
    </form>


    <form id="print_remind_letters_list" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">
        <div style="text-align:left; padding-top: 7px;">
        	<input type='hidden' name='by' id='by' value='remind_letter_list'/>            
            <input type="hidden" name="l_list_no" id="l_list_no" />            
        </div>                  
    </form>


</div>
</body>
</html>