<?php if($this->user_permissions->is_view('t_journal_sum')){ ?>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/t_forms.css" />

<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>

<script>

    $(function() {
        $(".input_date_down_future").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });   

</script>

<style>

select{
    font-size: 13px;
    border: none;
    margin-right: 5px;
    width: auto;
    width: 120px;
    font-family: 'roboto'
}

.v_bc{
    font-size: 13px;
    font-family: 'roboto'
}

</style>


<div class="page_main_title"><span>Journal Entry</span></div>
<!-- <h2 style="text-align: center;">Journal Entry</h2> -->
<div class="msgBox">
    <div class="msgInner">Saving Success</div>
</div>
<div class="dframe" id="mframe">
    <form method="post" action="<?= base_url() ?>index.php/main/save/t_journal_sum" id="form_">
        <table style="width: 100%" border="0">

            <tr>
                <td colspan="4"><div class="div_vou_hin"></div></td>
            </tr>
        
            <tr>
                <td style="width: 100px;">Journal Type</td>
                <td><input type="text" class="input_txt" id="sjournal_type" value="" style="width: 150px;" maxlength="20" />
                    <input type="hidden" name="journal_type" id="journal_type" value="0" />
                    <input type="text" class="input_txt" value='' readonly="readonly" id="journal_type_des"  style="width: 300px;"/></td>
                <td style="width: 50px;">No</td>
                <td>
                    <input type="text" class="input_active_num" name="id" id="id" style="width:100%" value="<?= $max_no ?>" />
                    <input type="hidden" id="hid" name="hid" value="0" />                </td>
            </tr><tr>
                <td><span style="width: 100px;">Description</span></td>
                <td><input name="description" type="text" class="input_txt" id="description"  style="width: 453px;" value='' /></td>
                <td style="width: 100px;">Date <input type="hidden" name="time" id="time" value="0"> </td>
                <td style="width: 100px;">
                    <?php if($this->user_permissions->is_back_date('t_journal_sum')){ ?>
                        <input type="text" class="input_date_down_future" readonly="readonly" name="date" id="date" value="<?= date('Y-m-d') ?>" style="text-align:right;"/>
                    <?php } else { ?>
                        <input type="text" class="input_txt" readonly="readonly" name="date" id="date" value="<?= date('Y-m-d') ?>" style="text-align:right;"/>
                    <?php } ?>    
                    </td>
            </tr>

            <tr>
                <td>Narration</td>
                <td><input name="narration" type="text" class="input_txt" id="narration"  style="width: 453px;" value='' maxlength="100" /></td>
                <td style="width: 100px;">Ref. No</td>
                <td style="width: 100px;"><input type="text" class="input_txt" name="ref_no" id="ref_no" value="" style="width: 100%; text-align:right;" maxlength="10"/></td>
            </tr>
        </table>
    <div class="tgrid" style="width:100%;">

        <table id="tgrid" class="tgrid_je" width="100%">
            <thead>
                <tr>
                    <td style='width:121px'>Account</td>
                    <td style='width:121px'>Description</td>
                    <td style='width:122px'>Memo</td>
                    <td style='width:121px'>Dr</td>
                    <td style='width:121px'>Cr</td>
                    <td style='width:158px'>Branch</td>
                    <td style='width:121px'>Class</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $y = $grid->value;
                for ($x = 0; $x < $y; $x++) {
                    echo "<tr>";
                    echo "<td style='width:125px'><input type='text' class='g_input_txt fo'         id='0_" . $x . "' name='0_" . $x . "' /> <input type='hidden' name='h_" . $x . "' id='h_" . $x . "' value='0' /></td>";
                    echo "<td style='width:125px'><input type='text' class='g_input_txt'            id='n_" . $x . "' name='n_" . $x . "' maxlength='150' readonly='readonly' /></td>";
                    echo "<td style='width:125px'><input type='text' class='g_input_txt'            id='1_" . $x . "' name='1_" . $x . "' /></td>";
                    echo "<td style='width:125px'><input type='text' class='g_input_amo dr amount'  id='2_" . $x . "' name='2_" . $x . "' maxlength='20'/></td>";
                    echo "<td style='width:125px'><input type='text' class='g_input_amo cr amount'  id='3_" . $x . "' name='3_" . $x . "' maxlength='20'/></td>";
                    
                    echo "<td id='v_bc_td_".$x."'><div style='width:155px'><SELECT class='input_ui_dropdown v_bc' id='bc_".$x."' name='bc_".$x."'><option value=''>Select Branch</option>".$grid_bc."</SELECT></div></td>";
                     echo "<td id='v_class_td_".$x."'><div style='width:118px'><SELECT  class='v_class' id='vcl_".$x."' name='vcl_".$x."'><option value=''>Select Class</option>".$voucher_class_dropdown."</SELECT></div></td>";
                  
                   // ".$grid_bc."//echo "<SELECT id='bc_'".$x"' name='bc_''".$x"'  class='input_ui_dropdown v_bc'><option value=''>Select Branch</option>.$grid_bc."</td>";   
                     //echo "<td id='v_class_td_".$x."'>   <div style='width:120px'>".$voucher_class_dropdown."</div></td>";

                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>











    </div>
                    <div style="text-align: left; padding-top: 7px;">
                        <input type="button" class="btn_regular" id="btnExit" value="Exit" />
                        <input type="button" class="btn_regular" id="btnReset" value="Reset" />
                        <?php if ($this->user_permissions->is_delete('t_journal_sum')) { ?>
                            <input type="button" class="btn_regular" id="btnDelete1" value="Delete" />
                        <?php } ?>
                        <?php if ($this->user_permissions->is_re_print('t_journal_sum')) { ?>
                            <input type="button" id="btnPrint" value="Print" class="btn_regular_disable" />
                        <?php } ?>
                        <?php if ($this->user_permissions->is_add('t_journal_sum')) { ?>
                            <input type="button" class="btn_regular"  id="btnSave1" value='Save <F8>' />
                        <?php } ?>
                        <span style="text-align:right;margin-left:85px; font-weight: bold;">Total
                            <input type="text" class="g_input_amo" name="tot_dr" id="tot_dr" style="width:153px; font-weight: bold; text-align:right;"/>
                            <input type="text" class="g_input_amo" name="tot_cr" id="tot_cr" style="width:145px; font-weight: bold; text-align:right;"/>
                            <input type="hidden" class="g_input_txt" name="grid_row" id="grid_row" value="<?= $y ?>" style="width:100px;"/>
                        </span>                    </div>                

            <?php 
                    if($this->user_permissions->is_print('t_journal_sum')){ ?>
                    <input type="hidden" name='is_prnt' id='is_prnt' value="1" value="1">
                    <input type="hidden" id='is_approved' value="0">
                <?php } ?> 
        
    </form>
</div>

<form id="print_pdf" action="<?php echo site_url(); ?>/reports/generate" method="post" target="_blank">

    <input type="hidden" name='by' value='t_journal_sum' value="t_journal_sum" class="report">
    <input type="hidden" name='page' value='A4' value="A4" >
    <input type="hidden" name='orientation' value='P' value="P" >
    <input type="hidden" name='type' value='' value="" >
    <input type="hidden" name='header' value='false' value="false" >
    <input type="hidden" name='qno'  value="" id="qno" >
    <input type="hidden" name='rep_sup' value="" id="rep_sup" >
    <input type="hidden" name='rep_ship_bc'  value="" id="rep_ship_bc" >
    <input type="hidden" name='inv_date'  value="" id="inv_date" >
    <input type="hidden" name='inv_nop' value="" id="inv_nop" >
    <input type="hidden" name='po_nop'  value="" id="po_nop" >
    <input type="hidden" name='po_dt'  value="" id="po_dt" >
    <input type="hidden" name='credit_prd'  value="" id="credit_prd" >
    <input type="hidden" name='rep_deliver_date' v value="" id="rep_deliver_date" >
    <input type="hidden" name='jtype'  value="" id="jtype" >
    <input type="hidden" name='jtype_desc'  value="" id="jtype_desc" >
    <input type="hidden" name='org_print'  value="1" id="org_print">


</form>
<?php } ?>