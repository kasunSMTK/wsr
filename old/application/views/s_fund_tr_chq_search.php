<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/t_forms.css" />
<link rel="stylesheet" href="<?=base_url()?>css/jquery-ui.css">
<script src="<?=base_url()?>js/jquery-ui.js"></script>
<script>

    $(function() {
        $("#E,#F").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:''"

        });
    });

</script>

<div class="page_contain_new_pawn">
    <div class="page_main_title_new_pawn">
        <div style="width:250px;float:left"><span>Fund Transfer Cheque Search</span></div>     
    </div><br>
          
    <table border="00" align="center" cellpadding="0" cellspacing="0" width="90%" class="tbl_voucher">
       
    </table><br>        

    <table border="01" align="center" cellpadding="0" cellspacing="0" width="90%" class="tblfundc">
        <thead>
            <tr>
            <td>No <input type="text"     id="G" style="width:100px"></td>
            <td>Date <br>    <input type="text"     id="E" value="<?=date('Y-m-d')?>" readonly="readonly"style="width:60px;font-size:11px">
                        <input type="text"       id="F" value="<?=date('Y-m-d')?>" readonly="readonly"style="width:60px;font-size:11px"></td>
            <td>Cheque No <input type="text"     id="A" style="width:100px"></td>
            <td>Amount <input type="text"        id="B" style="width:100px"></td>
            <td>From Bc <input type="text"   id="C" ></td>
            <td>To Bc <input type="text"     id="D" ></td>
            <td>Acc Description (from , to)</td>
            <td><input type="button" class="btn_regular"  id="btnSearch" value='Search' /></td>
            </tr>
        </thead>
        <tbody class="fnud_tr_div"></tbody>
    </table>


</div>
</html>