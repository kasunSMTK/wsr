<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link rel="stylesheet" href="<?=base_url()?>justifiedGallery/justifiedGallery.min.css">
<script src="<?=base_url()?>justifiedGallery/jquery.justifiedGallery.min.js"></script>
    

<div class="container">
    
    <div class="container__left">
        
    <div></div>

        <div id="mygallery" >
            
            <?php

                $dir = "images/front_thumbs";
                
                if (is_dir($dir)) {
                    if ($dh = opendir($dir)) {
                        $images = array();
                        while (($file = readdir($dh)) !== false) {
                            if (!is_dir($dir.$file)) {                            
                                echo '<a><img src="images/front_thumbs/'.$file.'"/></a>';
                            }
                        } closedir($dh);                    
                    }
                }            

            ?>
            
        </div>        

    </div>

    <div class="container__right"></div>

</div>
    

<script type="text/javascript">
    
    $("#mygallery").justifiedGallery({
        "rowHeight" : 210,
        "margins" : 1,
        "randomize":true
    });

</script>

</body>
</html>