<?php

	$this->pdf = new TCPDF("L", PDF_UNIT, 'A4', true, 'UTF-8', false);

	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();

	$this->pdf->SetFont('helvetica', '', 8);
	$this->pdf->setX(9);

	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$bcs = '';
	$pa_tot = $ca_tot = $ai_tot = $mv_tot = $ova_tot = 0;
	$first_row_passed = false;

	foreach ($list as $r) {
		if ( floatval($r->over_adv_amount) > 0 ){


			if ( $bcs != $r->bc ){


				if ($first_row_passed){

					$this->pdf->SetFont('helvetica', 'B', 8);
					$this->pdf->MultiCell(17, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(25, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(65, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(18, $h, 'Total', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(22, $h, d($pa_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(22, $h, d($ca_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(22, $h, d($ai_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(20, $h, d($mv_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(30, $h, d($ova_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->MultiCell(20, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
					$this->pdf->SetFont('helvetica', '', 8);

					$pa_tot = $ca_tot = $ai_tot = $mv_tot = $ova_tot = 0;

				}

				$first_row_passed = true;



				$this->pdf->ln();
				$this->pdf->SetFont('helvetica', 'B', 10);
				$this->pdf->MultiCell(0, 1, $r->bc , $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);	
				$this->pdf->SetFont('helvetica', '', 8);
				$this->pdf->ln();

				$this->pdf->SetFont('helvetica', 'B', 8);				
				$this->pdf->MultiCell(17, 0, "Date", $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(25, 0, "Bill No", $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(65, 0, "Name and Address", $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(18, 0, "Contact No", $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(22, 0, "Paw. Advance", $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(22, 0, "Cus. Advance", $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(22, 0, "Acrd. Interest", $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20, 0, "Market Value", $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(30, 0, "Over Adv. Amount", $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->MultiCell(20, 0, "Approval", $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				$this->pdf->SetFont('helvetica', '', 8);

				$bcs = $r->bc;
			}



			$h = 4 * (	max(1,$this->pdf->getNumLines($r->cus_name. ' - ' .$r->cus_addr,65)) );		
			
			$this->pdf->MultiCell(17, $h, $r->ddate, $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $h, $r->billno, $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(65, $h, $r->cus_name. ' - ' .$r->cus_addr, $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(18, $h, $r->mobile, $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(22, $h, d($r->requiredamount), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(22, $h, d($r->adv_bal), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(22, $h, d($r->accrued_interest), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, $h, d($r->market_value), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(30, $h, d($r->over_adv_amount), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, $h, $r->app_by, $border = 'BR', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);

		
			$pa_tot += $r->requiredamount;
			$ca_tot +=	$r->adv_bal;
			$ai_tot += $r->accrued_interest;
			$mv_tot += $r->market_value;
			$ova_tot += $r->over_adv_amount;


		}	
	}


			$this->pdf->SetFont('helvetica', 'B', 8);
			$this->pdf->MultiCell(17, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(25, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(65, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(18, $h, 'Total', $border = 'BR', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(22, $h, d($pa_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(22, $h, d($ca_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(22, $h, d($ai_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, $h, d($mv_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(30, $h, d($ova_tot), $border = 'BR', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->MultiCell(20, $h, '', $border = 'BR', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = $h, $valign = 'M', $fitcell = false);
			$this->pdf->SetFont('helvetica', '', 8);



	function d($s){
		return number_format($s,2,'.',',');
	}
	

	$this->pdf->Output("over_advance.pdf", 'I');



?>