<?php

	$this->pdf = new TCPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
	$this->pdf->SetPrintHeader(false);
	$this->pdf->SetPrintFooter(false);
	$this->pdf->AddPage();	

	$this->pdf->SetFont('helvetica', '', 22);	
	$this->pdf->setY(10);
	$this->pdf->setX(0);
	
	$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(204, 204, 204));
	$this->pdf->Line(5, 0, 0, 0, $style);

	$this->pdf->MultiCell(0, 0, "Current Stock Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	$this->pdf->SetFont('helvetica', '', 15);	
	$this->pdf->MultiCell(0, 0, "As at " .$td , 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '9', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	$this->pdf->setX(0);
	$this->pdf->ln();	

	$this->pdf->SetFont('helvetica', '', 8);		

	$this->pdf->MultiCell(10, 1, "S.No", 'B',	'C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Item Code", 	'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(50, 1, "Item Name", 	'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Quantity", 	'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "No of Pcts", 'B',	'C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(25, 1, "Total Pure Weight", 'B',	'C', 0, 1, '', '', false, '', 0);

	$this->pdf->SetFont('', '', 7);	

	$no = 1;

	$pcs = $p_weight = 0;

	foreach($list as $r){	

		$h = 5 * (	max(1,$this->pdf->getNumLines($r->itemname,50) ) );
		
		$this->pdf->MultiCell(10, $h, $no , 			$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->itemcode , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(50, $h, $r->itemname , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->qty , 		$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r->pcs , 		$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(25, $h, $r->p_weight , 	$border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$no++;
	
		$pcs 		+= $r->pcs;
		$p_weight 	+= $r->p_weight;

	}

	$this->pdf->MultiCell(10, 1, "", 		'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "", 		'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(50, 1, "", 		'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Total", 	'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, $pcs, 'B',	'C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(25, 1, dd($p_weight), 'B',	'C', 0, 1, '', '', false, '', 0);




































	$this->pdf->ln(10);

	$this->pdf->SetFont('helvetica', '', 22);		
	$this->pdf->setX(10);

	$this->pdf->MultiCell(0, 0, "Forfeited Stock Report", 	$border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
	

	$this->pdf->SetFont('helvetica', '', 8);		

	$this->pdf->MultiCell(10, 1, "S.No", 'B',	'C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Item Code", 	'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(50, 1, "Item Name", 	'B','L', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "Quantity", 	'B','C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(20, 1, "No of Pcts", 'B',	'C', 0, 0, '', '', false, '', 0);
	$this->pdf->MultiCell(25, 1, "Total Pure Weight", 'B',	'C', 0, 1, '', '', false, '', 0);

	$this->pdf->SetFont('', '', 7);	

	

	foreach($list_ftd as $r2){	

		$h = 5 * (	max(1,$this->pdf->getNumLines($r2->itemname,50) ) );
		
		$this->pdf->MultiCell(10, $h, $no , 			$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r2->itemcode , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(50, $h, $r2->itemname , 	$border='B', $align='L', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r2->qty , 		$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(20, $h, $r2->pcs , 		$border='B', $align='C', $fill=false, $ln=0, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$this->pdf->MultiCell(25, $h, $r2->p_weight , 	$border='B', $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=$h, $valign='M', $fitcell=false);
		$no++;
	
	}



	function d($number) {
	    return number_format($number, 2, '.', ',');
	}


	function dd($number) {
	    return number_format($number, 3, '.', ',');
	}


	$this->pdf->Output("PDF.pdf", 'I');

?>