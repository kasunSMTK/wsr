<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$page_title?></title>	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css" />	
	<script type="text/javascript" src="<?=base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/jquery.ui.core.min.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/jquery-ui-1.8.17.custom.min.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/validation.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/main.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/input.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>js/<?=$js_file_name?>.js"></script>

	<link rel="stylesheet" href="mega-dropdown/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="mega-dropdown/css/style.css"> <!-- Resource style -->
	<script src="mega-dropdown/js/modernizr.js"></script> <!-- Modernizr -->	

	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/inputs.css" />
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/grid.css" />	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/mnu_icons.css" />

</head>

<body>

	<div class="notification_penal">	
		Data Here
	</div>

	<div class="left_hover_slider"></div>
	<div class="left_hover_slider_content">

		<br><br><br><br>

		<div class="left_icons">
			<a href="?" class='ti-home'></a>
		</div>

	</div>

	<div id="blocker"></div>
	<div id="serch_pop" style="width: 800px; ">
	    <input type="text" id="pop_search" title="" class="input_acitve" style="width: 100%;" /><br />
	    <div id="sr" style="height:400px;"></div>
	    <div style="text-align: right; padding-top: 7px;"><button id="pop_close" >Close</button></div>
	</div>
	<div id="blocker2"></div>
	<div id="serch_pop2" style="width: 800px;">
	    <input type="text" id="pop_search2" title=""  style="width: 100%;" class="input_acitve"/><br />
	    <div id="sr2" style="height:400px;"></div>
	    <div style="text-align: right; padding-top: 7px;"><button id="pop_close2" >Close</button></div>
	</div>

	<div id="blocker4"></div>
	<!-- <div id="blanket4">
	    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" />
	</div> -->
	<div id="serch_pop4" style="width: 800px;">
	    <input type="text" id="pop_search4" title=""  style="width: 100%;" /><br />
	    <div id="sr4" style="height:400px;"></div>
	    <div style="text-align: right; padding-top: 7px;">
	    <input type='button' value='Approve' title='Approve' id='approve4' style='display:none;'/>
	    <button id="pop_close4" >Close</button></div>
	</div>

	<div id="blocker"></div>
	<!-- <div id="blanket">
	    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" />
	</div> -->
	<div id="serch_pop6" style="width: 800px;">
	    <input type="text" id="pop_search6" title=""  style="width: 100%;" class="input_acitve"/><br />
	    <div id="sr6" style="height:400px;"></div>
	    <div style="text-align: right; padding-top: 7px;"><button id="pop_close6" >Close</button></div>
	</div>


	<div id="blocker"></div>
	
	<div id="serch_pop14" style="width: 800px;">
	    <input type="text" id="pop_search14" title=""  style="width: 100%;" class="input_acitve"/><br />
	    <div id="sr14" style="height:400px;"></div>
	    <div style="text-align: right; padding-top: 7px;"><button id="pop_close14" >Close</button></div>
	</div>
	<div id="blocker"></div>
	<!-- <div id="blanket"> -->
	    <!-- <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" /> -->
	</div>
	<div id="serch_pop11" style="width: 800px;">
	    <input type="text" id="pop_search11" title=""  style="width: 100%;" class="input_acitve"/><br />
	    <div id="sr11" style="height:400px;"></div>
	    <div style="text-align: right; padding-top: 7px;"><button id="pop_close11" >Close</button></div>
	</div>

	<div id="blocker"></div>
	<!-- <div id="blanket"> -->
	    <!-- <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	    <img src="<?=base_url(); ?>/img/loadingBig.gif" /> -->
	</div>
	<div id="serch_pop13" style="width: 800px;">
	    <input type="text" id="pop_search13" title=""  style="width: 100%;" class="input_acitve"/><br />
	    <div id="sr13" style="height:400px;"></div>
	    <div style="text-align: right; padding-top: 7px;"><button id="pop_close13" >Close</button></div>
	</div>


	<div class="msg_pop_up_bg"></div>
	<div class="msg_pop_up_sucess">Text Here...</div>

	<div class="company_name">
		<div class="menu_holder_div">
			<div class="cd-dropdown-wrapper" style="border:0px solid red ; margin:0px;">
					<a class="cd-dropdown-trigger " href="#0">Menu</a>
					<nav class="cd-dropdown">
						<h2>Title</h2>
						<a href="#0" class="cd-close">Close</a>
						<ul class="cd-dropdown-content">
							<!-- <li>
								<form class="cd-search">
									<input type="search" placeholder="Search...">
								</form>
							</li> -->
<!-- *** Master Data Setup Start************************************************** -->

<?php 
    function txColor(){
    $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', '0');
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    return $color;
    }
?>
							
							<?php if ($isAdmin == 1){ ?>
							<li class="has-children">
								<a href="">Master Data Setup</a>
								<ul class="cd-dropdown-icons is-hidden">
									<li class="go-back"><a href="#0">Menu</a></li>									
									<li>
										<a class="cd-dropdown-item ti-location-pin" href="?action=m_area">
											<h3>Area</h3>
											<p>Add/Edit/Delete Area</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-direction-alt" href="?action=m_branches" style="color: <?=txColor()?> !important;">
											<h3>Branches</h3>
											<p>Add/Edit/Delete Branches</p>
										</a>
									</li>									

									<li>
										<a class="cd-dropdown-item ti-files" href="?action=m_billtype_det" style="color: <?=txColor()?> !important;">
											<h3>Bill Types</h3>
											<p>Setup Bill Types Details</p>
										</a>
									</li>									

									<li>
										<a class="cd-dropdown-item ti-bag" href="?action=m_item_category">
											<h3>Item Categories</h3>
											<p>Add/Edit/Delete Item Categories</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-package" href="?action=m_item">
											<h3>Item</h3>
											<p>Add/Edit/Delete Item details</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-thumb-up" href="?action=m_conditions">
											<h3>Item Conditions</h3>
											<p>Add/Edit/Delete Item Conditions</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-id-badge" href="?action=m_company">
											<h3>Company Details</h3>
											<p>Add/Edit/Delete company details</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-stats-up" href="?action=r_gold_rate">
											<h3>Gold Rates</h3>
											<p>Setup Gold Rates</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-star" href="?action=r_gold_quality">
											<h3>Gold Quality</h3>
											<p>Setup Gold Quality Rates</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-image" href="?action=r_stampfee">
											<h3>Stamp Fees</h3>
											<p>Setup Stamp Fees</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-file " href="?action=r_document_charge">
											<h3>Document Charge</h3>
											<p>Setup Document Charge</p>
										</a>
									</li>

								</ul>
							</li>	<?php } ?>				
						

<!-- *** Master Data Setup End************************************************** -->


<!-- *** Account Setups Start************************************************** -->





						<?php if ($isAdmin == 1){ ?>
							<li class="has-children">
								<a href="">Account Setups</a>
								<ul class="cd-dropdown-icons is-hidden">
									<li class="go-back"><a href="#0">Menu</a></li>
									<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
									<li>
										<a class="cd-dropdown-item ti-book" href="?action=m_account_type">
											<h3>Account Setup</h3>
											<p>Account Setup</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-money" href="?action=m_bank">
											<h3>Bank</h3>
											<p>Bank</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-money" href="?action=m_bank_branch">
											<h3>Bank Branch</h3>
											<p>Bank Branch</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-world" href="?action=m_default_account">
											<h3>Default Accounts</h3>
											<p>Default Accounts</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-book" href="?action=r_journal_type">
											<h3>Journal Type</h3>
											<p>Journal Type</p>
										</a>
									</li>


								</ul>
							</li>
						<?php } ?>							

							<li class="has-children">
								<a href="">Pawning</a>
								<ul class="cd-dropdown-icons is-hidden">
									<li class="go-back"><a href="#0">Menu</a></li>
									<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->

									<li>
										<a class="cd-dropdown-item ti-agenda" href="?action=t_opening_pawn">
											<h3>Opening Pawn</h3>
											<p>Enter opening pawn from here</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-agenda" href="?action=t_new_pawn">
											<h3>New Pawn</h3>
											<p>Enter new pawn from here</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-bookmark-alt" href="?action=t_redeem">
											<h3>Redemption</h3>
											<p>Redemption a pawn articles</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-receipt" href="?action=t_part_payment">
											<h3>Advance Payment to Bill</h3>
											<p>Pawn Advance Payment </p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-receipt" href="?action=t_customer_advance_payment">
											<h3>Customer Advance Payment</h3>
											<p>Customer Advance Payment </p>
										</a>
									</li>	

									<?php if ($isAdmin == 1){ ?>
										<li>
											<a class="cd-dropdown-item ti-write" href="?action=t_mark_bills">
												<h3>Mark Bills</h3>
												<p>Mark bills </p>
											</a>
										</li>

									
										<li>
											<a class="cd-dropdown-item ti-calendar" href="?action=t_forfeit">
												<h3>Forfeit</h3>
												<p>Forfeit Pawn</p>
											</a>
										</li>
									<?php } ?>

									<li>
										<a class="cd-dropdown-item ti-printer" href="?action=r_pawning">
											<h3>Reports</h3>
											<p>Reports</p>
										</a>
									</li>

								</ul>
							</li>


							
							<li><a href="?action=m_customer">Customer</a></li>


							<?php if ($isAdmin == 1){ ?>
								<li class="has-children">
									<a href="">Transactions</a>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>

										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->

										<li>
											<a class="cd-dropdown-item ti-shield" href="?action=t_opening_balance">
												<h3>Opening Balance</h3>
												<p>Opening Balance</p>
											</a>
										</li>
										<li>
											<a class="cd-dropdown-item ti-zip" href="?action=t_journal_sum">
												<h3>Journal Entry</h3>
												<p>Journal Entry</p>
											</a>
										</li>
										<li>
											<a class="cd-dropdown-item ti-layout-media-overlay-alt-2" href="?action=t_receipt_general">
												<h3>General Receipt</h3>
												<p>General Receipt</p>
											</a>
										</li>
										<li>
											<a class="cd-dropdown-item ti-layout-cta-btn-left" href="?action=t_voucher_general">
												<h3>General Voucher</h3>
												<p>General Voucher</p>
											</a>
										</li>
										<li>
											<a class="cd-dropdown-item ti-wallet" href="?action=t_pettycash">
												<h3>Petty Cash</h3>
												<p>Petty Cash</p>
											</a>
										</li>
										<li>
											<a class="cd-dropdown-item ti-layout-media-overlay" href="?action=t_payable_invoice">
												<h3>Payable Invoice</h3>
												<p>Payable Invoice</p>
											</a>
										</li>
										<li>
											<a class="cd-dropdown-item ti-layout-cta-right" href="?action=t_receivable_invoice">
												<h3>Receivable Invoice</h3>
												<p>Receivable Invoice</p>
											</a>
										</li>
										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=r_account_report">
												<h3>Account Reports</h3>
												<p>Account Reports</p>
											</a>
										</li>

										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=r_transaction_list_voucher">
												<h3>Transaction Reports</h3>
												<p>Transaction Reports</p>
											</a>
										</li>

									</ul>
								</li>
							<?php } ?>

							<?php if ($isAdmin == 1){ ?>
							<li><a href="?action=t_approvals">Approval</a></li>							
							<?php } ?>

							<li class="has-children">
								<a href="">Letters</a>
								<ul class="cd-dropdown-icons is-hidden">
									<li class="go-back"><a href="#0">Menu</a></li>
									<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
									
									<li>
										<a class="cd-dropdown-item ti-notepad" href="">
											<h3>Remind Letters</h3>
											<p>Generate Remind Letters</p>
										</a>
									</li>

									<li>
										<a class="cd-dropdown-item ti-clipboard" href="">
											<h3>Letter Period Setup</h3>
											<p>Letter Period Setup</p>
										</a>
									</li>									

								</ul>
							</li>

							<?php if ($isAdmin == 1){ ?>
								<li class="has-children">
									<a href="">Audit</a>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
										
										<li>
											<a class="cd-dropdown-item ti-check-box" href="?action=t_gold_audit">
												<h3>Gold Audit</h3>
												<p>Gold Audit</p>
											</a>
										</li>

										<li>
											<a class="cd-dropdown-item ti-printer" href="">
												<h3>Reports</h3>
												<p>Reports</p>
											</a>
										</li>
									</ul>
								</li>							




								<li class="has-children">
									<a href="">Bank</a>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
										
										<li>
											<a class="cd-dropdown-item ti-book" href="?action=t_bank_entry">
												<h3>Bank Entry</h3>
												<p>Bank Entry</p>
											</a>
										</li>

										<li>
											<a class="cd-dropdown-item ti-money" href="?action=t_bankrec">
												<h3>Bank Reconciliation</h3>
												<p>Bank Reconciliation</p>
											</a>
										</li>	

										<li>
											<a class="cd-dropdown-item ti-credit-card" href="?action=t_cheque_deposit">
												<h3>Cheque Deposit</h3>
												<p>Cheque Deposit</p>
											</a>
										</li>	
										
										<li>
											<a class="cd-dropdown-item ti-credit-card" href="?action=t_cheque_issue_trans">
												<h3>Cheque Withdraw</h3>
												<p>Cheque Withdraw</p>
											</a>
										</li>

										<li>
											<a class="cd-dropdown-item ti-printer" href="?action=r_bank_entry">
												<h3>Reports</h3>
												<p>Reports</p>
											</a>
										</li>

									</ul>
								</li>


								<li class="has-children">
									<a href="">Utilities</a>
									<ul class="cd-dropdown-icons is-hidden">
										<li class="go-back"><a href="#0">Menu</a></li>
										<!-- <li class="see-all"><a href="http://www.softmastergroup.com/">Browse Services</a></li> -->
										<li>
											<a class="cd-dropdown-item ti-user" href="?action=u_users">
												<h3>User Manager</h3>
												<p>Create/Modify/Delete user accounts</p>
											</a>
										</li>

										<li>
											<a class="cd-dropdown-item ti-server" href="">
												<h3>Database Backup</h3>
												<p>Database Backup</p>
											</a>
										</li>																		

										<li>
											<a class="cd-dropdown-item ti-server" href="?action=t_attendance_process">
												<h3>Attendance Process</h3>
												<p>Attendance Process</p>
											</a>
										</li>

									</ul>
								</li>
							<?php } ?>

							
							
							<!-- <li><a href="http://www.softmastergroup.com/">Page 3</a></li> -->
						</ul> <!-- .cd-dropdown-content -->
					</nav> <!-- .cd-dropdown -->
				</div> <!-- .cd-dropdown-wrapper -->
		</div>

		<span class="company_name_title_small"><?=$company_name?></span>

		<div class="top_line_menu">
			<?php if(!$is_login_view){ ?>				
				<div class="logout_div" style="margin-right:0"> <?=anchor("main/logout", "<div class='menu_link' style='margin-right:0'>Logout</div>"); ?> </div>	
				<div class="logout_div"> <a href="#"><div class='menu_link'>My Account</div></a> </div>
				<!-- <div class="logout_div"> <a href="#"><div class='menu_link'>Home</div></a> </div> -->

				<!-- <div class="logout_div"> <a href="?action=my_account"><div class='menu_link'>My Account</div></a> </div> -->
				<div class="logout_div"> <a href="?"><div class='menu_link'>Home</div></a> </div>
			<?php } ?>			
		</div>

	</div>
<script src="mega-dropdown/js/jquery-2.1.1.js"></script>
<script src="mega-dropdown/js/jquery.menu-aim.js"></script> <!-- menu aim -->
<script src="mega-dropdown/js/main.js"></script> <!-- Resource jQuery -->
