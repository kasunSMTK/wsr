<?php

	$make_excel = 1;

	$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
	$t .= '<table border="01" width="1000" class="tbl_income_statment" cellspacing="0" cellpadding="0">';
	$t .= '<tr>';            
	$t .= '<td colspan="11"><h1>Business Summary (testing)</h1></td>';	
	$t .= '</tr>';
	$t .= '<tr>';            
	$t .= '<td colspan="11" valign=top>'."<h3>From " .$fd." to ".$td.'</h3></td>';	
	$t .= '</tr>';

	$t .= '<tr>';
	$t .= "<td><b>Branch</b></td>";
	$t .= "<td align='right'><b>Pawning</b></td>";
	$t .= "<td align='right'><b>Redeem</b></td>";
	$t .= "<td align='right'><b>Pawning Int</b></td>";
	$t .= "<td align='right'><b>Redeem Int</b></td>";
	$t .= "<td align='right'><b>Total Int</b></td>";
	$t .= "<td align='right'><b>Gov.Tax</b></td>";
	$t .= "<td align='right'><b>Postage</b></td>";
	$t .= "<td align='right'><b>Outs. Capital</b></td>";
	$t .= '</tr>';



	$no = 1;

	$pa = $ra = $ia = $ts = $noi = $tp = $tw = $oc = $pi = $ri = $gt = $pst = $ti = 0;

	foreach($list as $r){	

		$t .= '<tr>';
		$t .='<td>'. $r->bc_name.'</td>';
		$t .='<td align="right">'. d($r->pawn_amount).'</td>';
		$t .='<td align="right">'. d($r->redeem_amount).'</td>';
		$t .='<td align="right">'. d($r->p_int).'</td>';
		$t .='<td align="right">'. d($r->r_int).'</td>';
		$t .='<td align="right">'. d($r->int_amount).'</td>';
		$t .='<td align="right">'. d($r->g_tax).'</td>';
		$t .='<td align="right">'. d($r->postage).'</td>';
		$t .='<td align="right">'. d($r->oc).'</td>';
		$t .= '</tr>';
		
		$no++;	

		$pa += $r->pawn_amount;
		$ra += $r->redeem_amount;

		$pi += $r->p_int;
		$ri += $r->r_int;
		$ti += $r->int_amount;

		$gt += $r->g_tax;
		$pst += $r->postage;

		$ts += $r->total_stock;		
		$tp += $r->total_packets;
		$oc += $r->oc;		
	
	}
	

    $t .= '<tr>';
	$t .='<td>'. "<b>Total</b>".'</td>';
	$t .='<td align="right"><b>'. d($pa).'</b></td>';
	$t .='<td align="right"><b>'. d($ra).'</b></td>';
	$t .='<td align="right"><b>'. d($pi).'</b></td>';
	$t .='<td align="right"><b>'. d($ri).'</b></td>';
	$t .='<td align="right"><b>'. d($ti).'</b></td>';
	$t .='<td align="right"><b>'. d($gt).'</b></td>';
	$t .='<td align="right"><b>'. d($pst).'</b></td>';
	$t .='<td align="right"><b>'. d($oc).'</b></td>';
	$t .= '</tr>';

	$t .= '</table>';
	
    if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'bus_summery_with_manual_tr.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }

    exit;


	function d($number) {
	    return number_format($number, 2, '.', ',');
	}

	function d3($number) {
	    return number_format($number, 3, '.', ',');
	}

	//$this->pdf->Output("PDF.pdf", 'I');

?>