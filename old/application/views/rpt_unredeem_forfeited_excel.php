<?php


$make_excel = 1;

$t  = '<link rel="stylesheet" type="text/css" href="'.base_url().'css/style.css" />';
$t .= '<table border="01" class="tbl_income_statment" cellspacing="0" cellpadding="0">';

$t .= "<tr>";
$t .= "<td>Unredeemed Article to Be Forfeited</td>";
$t .= "</tr>";

$t .= "<tr>";
$t .= "<td>Date from : $td</td>";
$t .= "</tr>";


$t .= "<tr>";
$t .= "<td>Branch</td>";
$t .= "<td>1-30 Days</td>";
$t .= "<td>31-60 Days</td>";
$t .= "<td>61-90</td>";
$t .= "<td>91-120 Days</td>";
$t .= "<td>121> Days</td>";
$t .= "<td>Total</td>";
$t .= "</tr>";


$A=$B=$C=$D=$E=0;

foreach ($list as $r) {

	$t .= "<tr>";
	$t .= "<td>". $r->bc_name . "</td>";
	$t .= "<td>". number_format($r->no_of_bills_30,2) . "</td>";
	$t .= "<td>". number_format($r->no_of_bills_60,2) . "</td>";
	$t .= "<td>". number_format($r->no_of_bills_90,2) . "</td>";
	$t .= "<td>". number_format($r->no_of_bills_120,2) . "</td>";
	$t .= "<td>". number_format($r->no_bills_over_120,2) . "</td>";
	$t .= "<td>". number_format(($r->no_of_bills_30+$r->no_of_bills_60+$r->no_of_bills_90+$r->no_of_bills_120+$r->no_bills_over_120),2) . "</td>";
	$t .= "</tr>";

	$A += $r->no_of_bills_30;
	$B += $r->no_of_bills_60;
	$C += $r->no_of_bills_90;
	$D += $r->no_of_bills_120;
	$E += $r->no_bills_over_120;

}
	
	$t .= "<tr>";
	$t .= "<td>". 'Total' . "</td>";
	$t .= "<td>". number_format($A,2) . "</td>";
	$t .= "<td>". number_format($B,2) . "</td>";
	$t .= "<td>". number_format($C,2) . "</td>";
	$t .= "<td>". number_format($D,2) . "</td>";
	$t .= "<td>". number_format($E,2) . "</td>";
	$t .= "<td>". number_format(($A+$B+$C+$D+$E),2) ."</td>";
	$t .= "</tr>";

	$t .= '</table>';

    if (!$make_excel){
        echo $t;
    }else{
        header('Content-type: application/excel');
        $filename = 'unredeem_to_forfeited.xls';
        header('Content-Disposition: attachment; filename='.$filename);
        $data = $t;
        echo $data;
    }



?>