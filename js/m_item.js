$(document).ready(function(){

	load_list();
	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_condition").val());
	});

	$("#itemcate").change(function(){
		loadItemByCategory($(this).val());
	});

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				closeActionProgress();

				if (D.d == 1){
					showMessage("s","Customer saving success");
					clear_form();
					load_list(D.itemcate);					
				}else{					
					showMessage("e","Error, save failed");
				}
			}
		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_item",{
			code : code
		},function(D){
			$(".list_div").html(D);
			closeActionProgress();
			load_list();
		},"text");
	}
}

function load_list(itemcate=""){

	showActionProgress("Loading...");
	$.post("index.php/main/load/m_item",{
		s : "load_list",
		itemcate : itemcate
	},function(D){		
		$(".list_div").html(D.T);
		$("#itemcode").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	if ($("#itemcate").val() == ""){showMessage("e","Select item category");	return false; }
	if ($("#itemname").val() == ""){showMessage("e","Enter item name");	return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#itemcate").val("");
	$("#hid").val(0);
	$("#bulk_item_code").prop({ 'checked': false });
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_item/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.itemcode);
		$("#btnSave").val("Edit");		
		$("#itemcode").val(D.itemcode);		
		$("#itemcate").val(D.itemcate);		
		$("#itemname").val(D.itemname);

		if (D.bulk_item_code == 1){
			$("#bulk_item_code").prop({ 'checked': true });
		}else{
			$("#bulk_item_code").prop({ 'checked': false });
		}

		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_item/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function loadItemByCategory(itemcate){
	//$("#itemname").val("");
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/m_item/loadItemByCategory",{
		itemcate : itemcate
	},function(D){		
		$(".list_div").html(D.T);
		//$("#itemcode").val(D.max_no);
		//$("#hid").val(0);
		//$("#btnSave").val("Save");
		closeActionProgress();
	},"json");
}