$(document).ready(function(){

	$('#inactive').prop('checked', false);

	load_list();

	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_billtype").val());
	});
	
	$( "#bc_desc" ).autocomplete({
		source: "index.php/main/load_data/m_branches/autocomplete",
		select: function (event, ui) {
			var bc = ui.item.value;
				bc = bc.split(" - ");
				$("#bc").val(bc[0]);
		}
	});
	
	$( "#bill_type_desc" ).autocomplete({
		source: "index.php/main/load_data/m_billtype_det/autocomplete_pawn_add_bill_type",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#bill_type").val(bt[0]);
		}
	});	

});


function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {


				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Bill Type saving success");
					clear_form();
					load_list();					
				}else{					
					showMessage("e",D.m);
				}
			}
		});

	}

}

function setDelete(bc,bill_type){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_branch_billtype",{
			bc : bc,
			bill_type : bill_type
		},function(D){		

			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_branch_billtype",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#code").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	if ($("#bill_type").val() == ""){showMessage("e","Please select bill type");	return false; }	
	if ($("#bc").val() == ""){showMessage("e","Please select branch");	return false; }		
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$("#hid2").val(0);
	$('#inactive').prop('checked', false);
}

function setEdit(bc,bill_type){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_branch_billtype/set_edit",{
		bc : bc,
		bill_type : bill_type
	},function(D){		
		$("#hid").val(D.bc);
		$("#hid2").val(D.bill_type);
		$("#btnSave").val("Edit");
		$("#bc").val(D.bc);		
		$("#bc_desc").val(D.bc);		
		
		$("#bill_type").val(D.bill_type);
		$("#bill_type_desc").val(D.bill_type);

		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_branch_billtype/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}