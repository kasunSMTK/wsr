var page_name = getURL_pagename_var();
var Tm

$(document).ready(function(){

	getApprovalRequests('salary_voucher');

	$(".approval_tab").click(function(event) {		
		$(".approval_tab_selected").css("border-bottom","1px solid #ccc");
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".approval_tab_selected").click(function(event) {				
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".input_text_app_search").keyup(function () {
		
		var rows = $(".discount_requests .request_row").hide();
		
		if (this.value.length) {
		    var data = this.value.split(" ");
		    $.each(data, function (i, v) {
		        rows.filter(":contains('" + v + "')").show();
		    });
		} else rows.show();
	});

	$(document).on( "click", ".input_approval", function() { $(this).select();});

	$('.app_requ_link').click(function(){
		
		clearTimeout(Tm);

		var x =  $(this).attr("href");
		x = x.split("#l=");		
		page_name = x[1];
		getApprovalRequests(x[1]);

	});

	$("#btnCancelForward_bc_request").click(function(event) {
		$(".bc_cash_balance_grid").html("");
		$("#f_bc").html('Loading branches...');
		$("#bc_cash_balance").html('0.00');
		
		$(".app_popup_bg,.app_popup").css("visibility","hidden");
	});

	
	$("#btnForward_bc_request").click(function(event) {
		forward_request_to_selected_bc();		
	});

});


/*$(document).on("mouseover",".bc_cls_title",function(){
	$(this).css({"color":"green"});
})

$(document).on("mouseout",".bc_cls_title",function(){
	$(this).css({"color":"#000000"});
})*/


var clicked_bc = "";

$(document).on("click",".bc_cls_title",function(){	

	$(".bc_cls_title").each(function(){
		$(this).css({"background-color":"#ffffff","color":"#000000"});
	});

	$(this).css({"background-color":"green","color":"#ffffff"});

	var bc = clicked_bc = $(this).attr("bc");

	$(".fund_transfer_title_row").css("display","none");
	$("."+bc).css("display","block");
});





function appr(loanno, approved_amount_id,row_id,requested_amount){
	var approved_amount = $("#" + approved_amount_id ).val();
	
	if (validateAmount_no_deci(approved_amount  , "") && parseFloat(approved_amount) <= parseFloat(requested_amount)  ){
		makeAprove(loanno,approved_amount,row_id,requested_amount);
	}else{
		alert("Invalid amount")
	}
}

function makeAprove(loanno,approved_amount,row_id,requested_amount){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
		loanno : loanno,
		approved_amount : approved_amount,
		record_status : "A"
	},function(D){
		$("#"+row_id).remove();
		closeActionProgress();
	},"json");
}

function rejt(loanno, row_id){
	if(confirm("Do you want reject this request? ")){
		showActionProgress("Loading...");
		$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
			loanno : loanno,		
			record_status : "R"
		},function(D){
			$("#"+row_id).remove();
			closeActionProgress();
		},"json");
	}
}


function getApprovalRequests(page_name){

	$.ajax({
		method: "POST",
		url: "index.php/main/load_data/approvals/getApprovalRequests",				
		data : {page_name : page_name},
		dataType: "json"
	}).done(function( D ) {

		if (page_name == "salary_voucher"){			

			var T = "<br>";
			var st = '';

			if (D.discount != "0"){

				for (num = 0 ; num < D.discount.length ; num++){					
					
					row_id = "row_"+num;

					if (clicked_bc != ""){

						if ( clicked_bc == D.discount[num].bc ){
							var set_visibility = " display:block ;";
							var set_bc_title_color = " style='background-color:green;color:#ffffff' ";
						}else{
							var set_visibility = "";
							var set_bc_title_color = " style='background-color:#ffffff;color:#000000' ";
						}
						
					}

					if ( st != D.discount[num].bc ){

						T += "   <div class='bc_cls_title' bc='"+D.discount[num].bc+"' "+set_bc_title_color+" >  ";
						T += D.discount[num].bc_name ;
						T += "  </div>  ";

						T += '<div class="fund_transfer_title_row '+D.discount[num].bc+'" style="'+set_visibility+'border-bottom: 1px solid #eaeaea; padding: 10px; height: 30px;"> <div class="RR_title_content" style="width: 50px;">NO</div> <div class="RR_title_content" style="width: 100px;">Request No</div> <div class="RR_title_content" style="width:120px;">Receiving Method</div> <div class="RR_title_content" style="width:110px;text-align: right;margin-right:20px">Amount</div> <div class="RR_title_content" style="width:120px;">Request by</div> <div class="RR_title_content" style="width:150px;">Request Datetime</div> <div class="RR_title_content" style="width:200px;">Action</div> </div>';

						st = D.discount[num].bc;

					}




					T += '<div style="'+set_visibility+'border-bottom: 1px solid #eaeaea; padding: 10px; height: 30px;" class="fund_transfer_title_row '+D.discount[num].bc+'"> <div style="width: 50px;" class="RR_title_content">'+(num+1)+'</div> <div style="width: 100px;text-align:right;padding-right:50px" class="RR_title_content">'+D.discount[num].nno+'</div> <!-- <div style="width:120px;" class="RR_title_content">'+D.discount[num].bc+'</div> <div style="width:150px;" class="RR_title_content">'+D.discount[num].date+'</div> <div style="width:120px;" class="RR_title_content">HO</div>--> <!-- <div style="width:120px;" class="RR_title_content">&nbsp;</div> --> <div style="width:120px;" class="RR_title_content">'+D.discount[num].receiving_method+'</div> <div style="width:110px;text-align: right;margin-right:20px" class="RR_title_content">'+D.discount[num].amount+'</div> <div style="width:120px;" class="RR_title_content">'+D.discount[num].request_by+'</div> <div style="width:150px;" class="RR_title_content">'+D.discount[num].request_datetime+'</div> <div style="width:200px;" class="RR_title_content">';


					T += '<a target = "self" href="?action=t_salary_general&no='+D.discount[num].nno+'&status=W&paid_acc='+D.discount[num].paid_acc+'&v_app_no='+D.discount[num].no+'&bc='+D.discount[num].bc+'" class="link_app">View</a>';
					
					/*T += '<a class="link_app" onClick=appr_vou("'+D.discount[num].nno+'","A","'+D.discount[num].paid_acc+'")>Approve</a> ';*/

					/*T += '| <a class="link_app" onClick=appr_vou("'+D.discount[num].nno+'","R","'+D.discount[num].paid_acc+'")>Reject</a>';*/

					T +=' </div> </div>';

				}

			}else{ 

				$(".discount_requests").html("<div class='div_no_request'>No requests found</div>");

			}

			$(".discount_requests").html(T);
			
			Tm = setTimeout("getApprovalRequests('"+page_name+"')",6000);

			return;

		}


	});

}

function appr_ft(no,row_id,S,approved_amount,chq_amount,r_bc = ""){
	
	var approved_amount = approved_amount;			
	showActionProgress("Please wait...");	
	
	$.post("index.php/main/load_data/approvals/makeAproveOrRejectFT",{
		
		no : no,
		approved_amount : approved_amount,
		chq_amount : chq_amount,
		status : S,
		r_bc : r_bc

	},function(D){
		
		//$("#"+row_id).remove();
		closeActionProgress();

	},"json");	

}

function appr_ft_popup(no,row_id,S,approved_amount,chq_amount,d){

	$(".requ_amt").val("");
	$(".bc_cash_balance_grid").html("<span style='font-size:20px;font-family:bitter;color:Green'>Calculating branch cash balances, Please wait...</span>");
	$(".app_popup_bg,.app_popup").css("visibility","visible");

	$.post("index.php/main/load_data/approvals/get_bc_list",{
			
	},function(D){	

		$(".bc_cash_balance_grid").html(D.bc_cash_balance_grid);
		$(".requ_amt").html(approved_amount);
		
		var d = "<select id='ho_selected_bc'>";	
		for (n_bc = 0 ; n_bc < D.bc_list.length ; n_bc++){d += "<option value='"+D.bc_list[n_bc].bc+"'>"+D.bc_list[n_bc].name+"</option>"; }	d += "</select>";
		
		$("#aa").val(no);
		$("#bb").val(row_id);
		$("#cc").val(S);
		$("#dd").val(approved_amount);
		//$("#dd").val(chq_amount);
		//$("#f_bc").html(d);

	},"json");

}

function forward_request_to_selected_bc(){

	var checked = false;
	var checked_bc = "";
	var bc_cash_balance = 0;

	$(".cash_bal_option").each(function(index, el) {
		if ($(this).is(":checked")){
			checked = true;
			checked_bc = $(this).val();
			bc_cash_balance = parseFloat($(this).parent().find('input[type="hidden"]').val());
		}
	});

	if (!checked){
		showActionProgress("");	
		showMessage("e","Please select a branch");
		return;
	}

	var requested_amount = parseFloat($(".requ_amt").html());

	if (requested_amount > bc_cash_balance){
		showActionProgress("");	
		showMessage("e","Unable to pay requested amount with this branch cash balance");
		return;	
	}

	no = $("#aa").val();
	row_id = $("#bb").val();
	S = $("#cc").val();
	approved_amount = $("#dd").val();
	chq_amount = $("#dd").val();
	
	d = checked_bc; // $("#ho_selected_bc :selected").val();

	appr_ft(no,row_id,S,approved_amount,chq_amount,d);
	$("#btnCancelForward_bc_request").click();
}

function appr_ft_popup_ant(){
	alert("Update action not taken by receiving branch");
}