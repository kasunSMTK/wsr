var page_name = getURL_pagename_var();
var Tm

$(document).ready(function(){

	getApprovalRequests('discount_approval');

	$(".approval_tab").click(function(event) {		
		$(".approval_tab_selected").css("border-bottom","1px solid #ccc");
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".approval_tab_selected").click(function(event) {				
		$(".approval_tab").css("border-bottom","1px solid #ccc");
		$(this).css("border-bottom","2px solid Green");
	});

	$(".input_text_app_search").keyup(function () {
		
		var rows = $(".discount_requests_a .request_row").hide();
		
		if (this.value.length) {
		    var data = this.value.split(" ");
		    $.each(data, function (i, v) {
		        rows.filter(":contains('" + v + "')").show();
		    });
		} else rows.show();
	});

	$(document).on( "click", ".input_approval", function() { $(this).select();});

	$('.app_requ_link').click(function(){
		
		clearTimeout(Tm);

		var x =  $(this).attr("href");
		x = x.split("#l=");		
		page_name = x[1];
		getApprovalRequests(x[1]);

	});

	$("#btnCancelForward_bc_request").click(function(event) {
		$(".bc_cash_balance_grid").html("");
		$("#f_bc").html('Loading branches...');
		$("#bc_cash_balance").html('0.00');
		
		$(".app_popup_bg,.app_popup").css("visibility","hidden");
	});

	
	$("#btnForward_bc_request").click(function(event) {
		forward_request_to_selected_bc();		
	});

});

$(document).on("click",".close_item_app_popup",function(){		
	$(".article_view_inner,.article_view_outter").css("display","none");
});

$(document).on("click",".app_action_dis_view", function(){

	var billno = $(this).attr('billno');
	
	showActionProgress("Please wait...");

	$(".item_holder_app").html("");	
	
	$.post("index.php/main/load_data/approvals/app_action_dis_view",{
		billno : billno
	},function(D){		
		
		closeActionProgress();

		$(".article_view_inner,.article_view_outter").css("display","block");

		var ti = '<div style="padding:10px;padding-right:23px;text-align:right;font-size:12px"><div style="float:left;font-size:24px">'+billno+ ' | ' + D.sum.ddate + ' | ' + D.sum.requiredamount +  '</div><div class="close_item_app_popup">Close</div></div><br>';

		ti += '<div style="padding-left:15px">'+D.sum.cusname+' ('+D.sum.nicno+')</div>';
		ti += '<div style="padding-left:15px;font-size:12px;padding-top:10px">'+D.sum.address+'</div>';

		ti += '<div id="pn1" style="border-bottom:1px dotted #ccc;height:47px"><div style="width:135px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Category" style="width:120px" id="cat_code_desc" class="input_text_regular_new_pawn catC" readonly="readonly"></div><div style="width:175px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Item" style="width:165px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn itemC"></div><div style="width:140px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Condition" style="width:130px" id="condition_desc" readonly="readonly" class="input_text_regular_new_pawn conC"></div><div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Karatage" style="width:97px" id="gold_type_desc" readonly="readonly" class="input_text_regular_new_pawn gtypeC"></div><div style="width:60px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Quality" style="width:47px" id="gold_qlty" readonly="readonly" class="input_text_regular_new_pawn gquliC"></div><div style="width:50px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Qty" style="width:37px" id="qty" readonly="readonly" class="input_text_regular_new_pawn qtyC"></div><div style="width:76px;float:left" class="text_box_holder_new_pawn"><input type="text" value="T.Weught" style="width:63px" id="t_weigth" readonly="readonly" class="input_text_regular_new_pawn t_weigthC"></div><div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="P.Weight" style="width:60px" id="p_weigth" readonly="readonly" class="input_text_regular_new_pawn p_weigthC"></div><div style="width:73px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Densi Read" style="width:60px" id="d_weigth" readonly="readonly" class="input_text_regular_new_pawn d_weigthC"></div><div style="width:110px;float:left" class="text_box_holder_new_pawn"><input type="text" value="Value" style="width:100px; text-align:right" id="value" readonly="readonly" class="input_text_regular_new_pawn valueC"></div></div>';

		$(".item_holder_app").html(ti);

		$(".item_holder_app").append(D.list);

		$('.item_holder_app').find('a').each(function(){
			$(this).parent().remove();
		});
		
		$('.item_holder_app').find('input[type="hidden"]').each(function(){
			$(this).remove();
		});

		$('.item_holder_app').find('input').each(function(){
						
		});


	},"json");



});


$(document).on("click",".app_action_dis",function(){
	
	showActionProgress("Please wait...");
	
	$.post("index.php/main/load_data/approvals/app_action_dis",{
		
		stat : $(this).attr("stat"),
		nno  : $(this).attr("nno"),
		approved_amount : $(this).attr("approved_amount")

	},function(D){

		if (D.s == 1){
		
			if ( D.stat == 'A' ){
				alert("Request approved");
			}

			if ( D.stat == 'R' ){
				alert("Request rejected");
			}

			window.location = '';

		}else{
			alert("Error, action not success");
		}
		
	},"json");

});


function appr(loanno, approved_amount_id,row_id,requested_amount){
	var approved_amount = $("#" + approved_amount_id ).val();
	
	if (validateAmount_no_deci(approved_amount  , "") && parseFloat(approved_amount) <= parseFloat(requested_amount)  ){
		makeAprove(loanno,approved_amount,row_id,requested_amount);
	}else{
		alert("Invalid amount")
	}
}

function appr_vou(no,S,paid_acc){

	clearTimeout(Tm);
	
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/approvals/appr_vou",{
		no : no,		
		record_status : S,
		paid_acc : paid_acc
	},function(D){				

		window.location.href += "#l=general_voucher";
		location.reload();
		
		//window.location.hash = "";
		//window.location.href = window.location.href;
		
	},"json");
	
}

function makeAprove(loanno,approved_amount,row_id,requested_amount){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
		loanno : loanno,
		approved_amount : approved_amount,
		record_status : "A"
	},function(D){
		$("#"+row_id).remove();
		closeActionProgress();
	},"json");
}

function rejt(loanno, row_id){
	if(confirm("Do you want reject this request? ")){
		showActionProgress("Loading...");
		$.post("index.php/main/load_data/approvals/makeAproveOrReject",{
			loanno : loanno,		
			record_status : "R"
		},function(D){
			$("#"+row_id).remove();
			closeActionProgress();
		},"json");
	}
}


function getApprovalRequests(page_name){

	$.ajax({
		method: "POST",
		url: "index.php/main/load_data/approvals/getApprovalRequests",				
		data : {page_name : page_name},
		dataType: "json"
	}).done(function( D ) {

		//*************************************************************************/

		if (page_name == "discount_approval"){

			var T = "";

			if (D.discount != "0"){

				for (num = 0 ; num < D.discount.length ; num++){					
					
					row_id = "row_" + num;

					T += '<tr>';
					
					T += '<td style="border-left: none;">'+D.discount[num].nno+'</td>';
					T += '<td>'+D.discount[num].bc_name+'</td>';
					T += '<td>'+D.discount[num].requ_by+'</td>';
					T += '<td>'+D.discount[num].requested_datetime+'</td>';
					T += '<td>'+D.discount[num].request_type+'</td>';
					T += '<td>'+D.discount[num].over_adv_item_list+'</td>';
					T += '<td align="right" style="font-family:bitter;font-size:14px">'+D.discount[num].requested_amount+'</td>';
					T += '<td style="border-right: none;">';

					T += '<a class="app_action_dis_view" stat="V" nno="'+D.discount[num].nno+'" billno="'+D.discount[num].di_billno+'" >View Bill</a> | ';
					T += '<a class="app_action_dis" stat="A" nno="'+D.discount[num].nno+'" approved_amount="'+D.discount[num].requested_amount+'" >Approve</a> | ';
					T += '<a class="app_action_dis" stat="R" nno="'+D.discount[num].nno+'" >Reject</a>';

					T += '</td>';				
					
					T += '</tr>';

				}

			}else{ 

				$(".discount_requests_a").html("<div class='div_no_request'>No requests found</div>");

			}

			$(".discount_requests_a").html(T);
			
			Tm = setTimeout("getApprovalRequests('"+page_name+"')",10000);

			return;

		}		

	});

}

function appr_ft(no,row_id,S,approved_amount,chq_amount,r_bc = ""){
	
	var approved_amount = approved_amount;			
	showActionProgress("Please wait...");	
	
	$.post("index.php/main/load_data/approvals/makeAproveOrRejectFT",{
		
		no : no,
		approved_amount : approved_amount,
		chq_amount : chq_amount,
		status : S,
		r_bc : r_bc

	},function(D){
		
		//$("#"+row_id).remove();
		closeActionProgress();

	},"json");	

}

function appr_ft_popup(no,row_id,S,approved_amount,chq_amount,d){

	$(".requ_amt").val("");
	$(".bc_cash_balance_grid").html("<span style='font-size:20px;font-family:bitter;color:Green'>Calculating branch cash balances, Please wait...</span>");
	$(".app_popup_bg,.app_popup").css("visibility","visible");

	$.post("index.php/main/load_data/approvals/get_bc_list",{
			
	},function(D){	

		$(".bc_cash_balance_grid").html(D.bc_cash_balance_grid);
		$(".requ_amt").html(approved_amount);
		
		var d = "<select id='ho_selected_bc'>";	
		for (n_bc = 0 ; n_bc < D.bc_list.length ; n_bc++){d += "<option value='"+D.bc_list[n_bc].bc+"'>"+D.bc_list[n_bc].name+"</option>"; }	d += "</select>";
		
		$("#aa").val(no);
		$("#bb").val(row_id);
		$("#cc").val(S);
		$("#dd").val(approved_amount);
		//$("#dd").val(chq_amount);
		//$("#f_bc").html(d);

	},"json");

}

function forward_request_to_selected_bc(){

	var checked = false;
	var checked_bc = "";
	var bc_cash_balance = 0;

	$(".cash_bal_option").each(function(index, el) {
		if ($(this).is(":checked")){
			checked = true;
			checked_bc = $(this).val();
			bc_cash_balance = parseFloat($(this).parent().find('input[type="hidden"]').val());
		}
	});

	if (!checked){
		showActionProgress("");	
		showMessage("e","Please select a branch");
		return;
	}

	var requested_amount = parseFloat($(".requ_amt").html());

	if (requested_amount > bc_cash_balance){
		showActionProgress("");	
		showMessage("e","Unable to pay requested amount with this branch cash balance");
		return;	
	}

	no = $("#aa").val();
	row_id = $("#bb").val();
	S = $("#cc").val();
	approved_amount = $("#dd").val();
	chq_amount = $("#dd").val();
	
	d = checked_bc; // $("#ho_selected_bc :selected").val();

	appr_ft(no,row_id,S,approved_amount,chq_amount,d);
	$("#btnCancelForward_bc_request").click();
}

function appr_ft_popup_ant(){
	alert("Update action not taken by receiving branch");
}