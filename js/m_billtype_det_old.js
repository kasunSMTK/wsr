$(document).ready(function(){

	load_list();

	$('#inactive').prop('checked', false);

	$("#bc").removeAttr("Class").css("width","200px");
	$("#bc").prepend("<option value='all'>All Branch</option>").val("all");

	$("#bc").change(function(){
		clear_form();		
		load_list($(this).val())
	});
	

	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});

	$("#btnReset").click(function(){		
		clear_form();		
	});

	$("#btnSearch").click(function(){
		search($("#Search_billtype").val());
	});	

	$("#no").keypress(function(e){
		if (e.keyCode == 13){
			setUpdate($(this).val());
		}
	});

	$(".chkbc_select").click(function(){
		if ( $(this).is(":checked") ){
			$(".chkbc_").prop("checked",true);
			$(".chk_main_lable").html("Deselect all");
		}else{
			$(".chkbc_").prop("checked",false);
			$(".chk_main_lable").html("Select all");
		}
	});


	$(".rd").click(function(){
		
		if ( $(this).val() == "G" || $(this).val() == "L" ){
			$("#percentage_to").val("").attr("readonly",true);
		}

		if ( $(this).val() == "B"){
			$("#percentage_to").attr("readonly",false);
		}

	});



	$(".btnAddaRow").click(function(){
		add_rate_row();		
	});

	$(document).on('click', '.btnremoveRow', function(){	
		if (confirm("Do you want remove this interest rate row?")){
			$(this).parent().parent().remove();
		}
	});



});


function setUpdate(billtype){

	showActionProgress("Loading...");
	$.post("index.php/main/load_data/m_billtype_det/setUpdate",{
		billtype : billtype
	},function(D){				
		closeActionProgress();

		$("#hid").val(D.sum.no);
		$("#billtype").val(D.sum.billtype);
		$("#amt_from").val(D.sum.amt_from);
		$("#amt_to").val(D.sum.amt_to);		
		$("#period").val(D.sum.period);				

		$(".chkbc_").parent().parent().show();
		$(".chkbc_").attr("checked",false);

		var n = 0;
		var bc_array = Array();

		for(nno=0 ; nno < D.bc_list.length ; nno++){
			bc_array[nno] = D.bc_list[nno].bc;
		}

		$(".chkbc_").each(function(){
			if( $.inArray($(this).val(), bc_array) !== -1 ) {		
				$(this).prop("disabled",false).prop("checked",true);
			}			
		});

		$("#percentage").val( D.sum.percentage );
		$("#percentage_to").val( D.sum.percentage_to );
		$("#lt_opt,#gt_opt").attr("checked",false);
		$('input[type="radio"][value="'+D.sum.gratr_lesstn+'"]:first').prop('checked', true);
		$("#billtypeno").val( D.sum.billtypeno );

		$("#r1").val( D.sum.r1 );
		$("#r2").val( D.sum.r2 );
		$("#r3").val( D.sum.r3 );
		
		if ( D.sum.weekly_cal == 1 ){
			$("input[type='checkbox'][name='weekly_cal']").prop("checked",true);
		}else{
			$("input[type='checkbox'][name='weekly_cal']").prop("checked",false);
		}

		if ( D.sum.is_old_bill == 1 ){
			$("input[type='checkbox'][name='is_old_bill']").prop("checked",true);
		}else{
			$("input[type='checkbox'][name='is_old_bill']").prop("checked",false);
		}


		var T = "";

		for (no = 0 ; no < D.det.length ; no++){

			T += '<tr><td><input value="'+D.det[no].day_from+'" id="A'+no+'" name="day_from[]" type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;margin-bottom:5px;"></td><td><input value="'+D.det[no].day_to+'" id="B'+no+'" name="day_to[]" type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td><td><input value="'+D.det[no].rate+'" id="C'+no+'" name="rate[]" type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td><td><a class="btnremoveRow">remove</a></td>';

				if (D.det[no].is_daily_cal == 1){
					//T += '<td><input name="rad_is_dailt" type="radio" value="'+no+'" style="width:100px;text-align:center" checked></td>';
				}else{
					//T += '<td><input name="rad_is_dailt" type="radio" value="'+no+'" style="width:100px;text-align:center"></td>';
				}

			T += '</tr>';

		}



		$(".clz_billt tbody").html("").html(T);


	},"json");

}

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		
		var frm = $('#form_');
		
		if (	$("#lt_opt").is(":checked")	){ opt = "L"; }
		if (	$("#gt_opt").is(":checked")	){ opt = "G"; }
		if (	$("#bt_opt").is(":checked")	){ opt = "B"; }

		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize()+"&gratr_lesstn="+opt,
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Bill Type saving success");
					clear_form();
					load_list();		
					$("#no").val(D.maxno);
				}else{		

					var str = '';

					for (n=0;n< D.bc_list.length;n++){
						str += D.bc_list[n].bc_name+"<br>";
					}

					showMessage("e",D.m + str);
				}
			}
		});

	}

}

function setDelete(billtype){

	if (confirm("Do you want delete this bill type?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_billtype_det",{
			billtype : billtype
		},function(D){		
			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				clear_form();
				load_list();
			}else{
				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(bc=""){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_billtype_det",{
		s : "load_list",
		bc : bc
	},function(D){		
		$(".list_div").html(D.T);
		$("#code").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	
	if ($("#billtype").val() == ""){showMessage("e","Please enter bill type");	return false; }		
	if (!validateAmount($("#amt_from").val() ," from amount")){ return false; }
	if (!validateAmount($("#amt_to").val() ," to amount")){ return false; }	
	if ($("#period").val() == ""){showMessage("e","Invalid period input");	return false; }	
	
	var rd = "";
	var selected = $("input[type='radio'][name='rd']:checked");
	
	if (selected.length > 0) {
	    rd = selected.val();
	}

	if (rd == 'L' || rd == 'G'){
		if ( $("#percentage").val() =="" ){
			showMessage("e","Invalid percentage value"); $("#percentage").focus();	return false; 	
		}
	}

	if (rd == "B"){
		if ( $("#percentage").val() == "" || $("#percentage_to").val() == "" ){
			showMessage("e","Invalid percentage values"); $("#percentage").focus();	return false; 	
		}
	}

	
	var bc_selected = false; $(".chkbc_").each(function(){if ( $(this).is(" :checked") ){bc_selected = true; } });
	if (!bc_selected){showMessage("e","Please select branch");	return false; }
	
	var A = false;

	$(".clz_billt tbody tr").each(function(){
		$(this).find("input:text").each(function(){
			if($(this).val() != ""){
				A = true;
			}
		});
	});

	if (!A){
		showMessage("e","Please enter days range and interest rate");
		return false;
	}

	return true;	
}

function clear_form(){

	var tmp_no = $("#no").val();
	$("input[type=text]").val("");	
	$("#no").val(tmp_no);
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$('#inactive').prop('checked', false);
	$('#tax').prop('checked', false);
	$("input[type='checkbox'][name='weekly_cal']").prop("checked",false);
	$("input[type='checkbox'][name='is_old_bill']").prop("checked",false);
	$(".chkbc_").parent().parent().show();
	$(".chkbc_").prop("disabled",false).prop("checked",false);
}

function setEdit(billtype){


	$("#btnSave").attr("value","Update");
	$("#btnReset").attr("class","btn_regular");
	setUpdate(billtype);

	/*showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_billtype_det/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.code);
		$("#btnSave").val("Edit");
		$("#area").val(D.area);		
		$("#code").val(D.code);
		$("#description").val(D.description);
		$("#amount_from").val(D.amount_from);
		$("#amount_to").val(D.amount_to);
		$("#period").val(D.period);		
		$("#int_rate").val(D.int_rate);

		if (D.inactive == "1"){ $('#inactive').prop('checked', true); }else{ $('#inactive').prop('checked', false); }
		if (D.tax == "1"){ $('#tax').prop('checked', true); }else{ $('#tax').prop('checked', false); }

		closeActionProgress();
	},"json");	*/

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_billtype_det/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function add_rate_row(){
	var T = '<tr> <td><input id = "" name="day_from[]" 	type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;margin-bottom:5px;"></td> <td><input id = "" name="day_to[]" 	type="text" class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td> <td><input id = "" name="rate[]"		type="text"  class="input_text_regular_new_pawn" style="border:1px solid #ccc;width:100px;"></td><td><a class="btnremoveRow">remove</a></td></tr>'; 
	$(".clz_billt tbody").append(T);
}


