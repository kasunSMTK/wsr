

$(document).on("click",".btnSet",function(){

	var max_amount = $(this).parent().find(".txtcustom").val();
	var cus_serno = $(this).attr("cus_serno");

	showActionProgress("Please wait...");
	
	$.post("index.php/main/load_data/m_top_customers/cus_max_allow_amount",{
		cus_serno : cus_serno,
		max_amount : max_amount
	},function(D){		

		closeActionProgress();			

	},"json");



});


var ssss = '';

$(document).on("click",".cus_moni",function(){

	if (confirm("Do you want change customer pawning monitor status")){

		showActionProgress("Please wait...");

		ssss = $(this);
		
		$.post("index.php/main/load_data/m_top_customers/cus_moni_update",{
			cus_serno : $(this).attr("cus_serno"),
			set : $(this).attr("set")
		},function(D){		

			closeActionProgress();

			if (D.set == 1){
				ssss.attr("set",0);
				ssss.html('<span style="color:Green">On</span>');
			}else{
				ssss.attr("set",1);
				ssss.html('<span style="color:Red">Off</span>');
			}
			

		},"json");
	
	}


});


$(document).ready(function(){	
	view_list();
});

function view_list(){

	showActionProgress("Please wait...");
	
	$.post("index.php/main/load_data/m_top_customers/view_list",{
		no : $("#no").val()
	},function(D){		

		closeActionProgress();

		if ( D.s == 1 ){

		var T  = '<table cellpadding="0" cellspacing="0" border="0" class="tbl_master_n" align="center" width="100%">';
			T += '<tr>';
			T += '<td width="80">Name</td>';
			T += '<td width="80">NIC</td>';
			T += '<td width="80">Branch</td>';
			T += '<td  align="right" width="250">Max Allowed Total Amount</td>';
			T += '<td  align="right" width="80">Current Total</td>';
			T += '<td  align="center" width="80">Monitor Pawnings</td>';
			T += '</tr>';

			for (n = 0 ; n < D.det.length ; n++){
				
				T += '<tr>';
				T += '<td>'+D.det[n].cusname+'</td>';
				T += '<td>'+D.det[n].nicno+'</td>';
				T += '<td>'+D.det[n].bc+'</td>';
				T += '<td align="right" ><input type="text" value="'+D.det[n].allow_total_pawn_amount+'" class="input_text_regular_ftr amount txtcustom">&nbsp;<input type="button" class="btn_regular btnSet" value="Set" cus_serno="'+D.det[n].cus_serno+'"></td>';
				T += '<td align="right" >'+D.det[n].total+'</td>';
				
				if (D.det[n].cus_monitor == 1){
					T += '<td align="center" ><a class="cus_moni" set="0" cus_serno="'+D.det[n].cus_serno+'"><span style="color:Green">On</span></a></td>';
				}else{
					T += '<td align="center" ><a class="cus_moni"  set="1" cus_serno="'+D.det[n].cus_serno+'"><span style="color:Red">Off</span></a></td>';
				}

				T += '</tr>';

			}

			T += '</table>';

		}else{
			var T = "No data found";
		}		
		

		$(".backdate_list").html(T);

	},"json");

}