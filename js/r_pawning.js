$(document).ready(function() {
  /*var T = '';

	$(".tbl_page_holder tbody tr").each(function(){
		var td = $(this).children("td").html();
		T += '<div class="vvv">' + td + '</div>';
	});

	$(".ccc").html(T);*/

  //----------------------------------------------------------

  $("input[type='radio'][name='by']").click(function() {
    if (
      $(this).val() == "rpt_pawnings" ||
      $(this).val() == "rpt_redeem" ||
      $(this).val() == "rpt_daily_cash_report"
    ) {
      $(".C_from_time,.C_to_time").css("display", "block");
    } else {
      $(".C_from_time,.C_to_time").css("display", "none");
    }
  });

  $(window).scroll(function(event) {
    var scroll = parseFloat($(window).scrollTop());

    if (scroll > 82) {
      $(".rpt_critaria_bar").css({
        position: "fixed",
        top: "0px",
        "border-bottom": "1px solid #cccccc",
        width: "100%"
      });

      $(".zxc").css("height", "87px");
    } else {
      $(".rpt_critaria_bar").css({
        position: "absolute"
      });

      $(".zxc").css("height", "0px");

      $(".rpt_critaria_bar").removeAttr("style");
    }
  });

  $("#bc").change(function() {
    showActionProgress("Loading bill types...");

    $.post(
      "index.php/main/load_data/m_billtype_det/load_bc_billtypes",
      {
        bc: $("#bc :selected").val()
      },
      function(D) {
        closeActionProgress();
        var opt = "";

        if (D.bt_det.length > 0) {
          opt += '<option value="">All</option>';
          for (n = 0; n < D.bt_det.length; n++) {
            opt +=
              '<option value="' +
              D.bt_det[n].billtype +
              '">' +
              D.bt_det[n].billtype +
              "</option>";
          }
        } else {
          opt += '<option value="">No bill type found</option>';
        }

        $("#r_billtype").html(opt);
      },
      "json"
    );
  });

  $("#btnGenerateEx").click(function() {
    var fd = $("#from_date")
      .val()
      .replace("-", "/");
    var td = $("#to_date")
      .val()
      .replace("-", "/");
    var branch = $("#bc :selected").val();
    var account = $("#r_expense :selected").val();
    var b_type = $("#r_billtype :selected").val();
    var d_ranage = "";

    if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_interest_incme_psum"
    ) {
      d_ranage = $("#data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_interest_incme_rsum"
    ) {
      d_ranage = $("#r_data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_interest_incme_tsum"
    ) {
      d_ranage = $("#t_data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_pwing_periodical"
    ) {
      d_ranage = $("#p_data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_rdming_periodical"
    ) {
      d_ranage = $("#rdm_data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_pcl_expense_analysis_n"
    ) {
      d_ranage = $("#a_data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_ntranse_periodical"
    ) {
      d_ranage = $("#n_data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() ==
      "rpt_pawning_analysis"
    ) {
      d_ranage = $("#n_data_range :selected").val();
    } else if (
      $("input[type='radio'][name='by']:checked").val() == "rpt_pawnings"
    ) {
      d_ranage = $("#n_data_range :selected").val();
    } else {
      d_ranage = "";
    }

    if (fd <= td) {
      if ($("input[type=radio]").is(":checked")) {
        $("#from_date_ex").val($("#from_date").val());
        $("#to_date_ex").val($("#to_date").val());
        $("#h_bc").val($("#bc :selected").val());
        $("#h_range").val(d_ranage);
        $("#bill_types").val(b_type);

        $("#h_tf").val($(".tf").val());
        $("#h_tt").val($(".tt").val());

        if ($("#r_expense :selected").val() != "") {
          $("#h_acc").val($("#r_expense :selected").val());
        }

        $("#txt_rpt_cus_info_info_ex").val($("#txt_rpt_cus_info_info").val());

        var cc = (ccc = 0);
        var cid = Array();
        var qwe = Array();

        $(".ccgr_id").each(function() {
          cid[cc] = $(this).val();
          cc++;
        });

        $(".ccgr_val").each(function() {
          qwe[ccc] = $(this).val();
          ccc++;
        });

        $("#AA").val(cid);
        $("#BB").val(qwe);

        var bc_a = Array();
        var n = 0;

        $(".bc_each").each(function() {
          if ($(this).is(":checked")) {
            bc_a[n] = $(this).val();
            n++;
          }
        });

        $("#bc_arry").val(bc_a);

        if (n == 0) {
          alert("please select branch");
          return;
        }

        if (
          $("input[type='radio'][name='by']:checked").val() ==
          "rpt_audit_callout_report"
        ) {
          if (n > 1) {
            alert("Only one branch allow to select for audit callout report ");
            return;
          }
        }

        if ($("input[type='radio'][name='by']:checked").val() == "rpt_ecd") {
          /*if ( $("#ecd_select :selected").val() == "" ){
						alert("Please select class");
						return;
					}*/
        }

        $("#CC").val($("#ecd_select :selected").val());

        $("#by_ex").val($("input[type='radio'][name='by']:checked").val());
        $("#print_excel_form")
          .attr("action", "index.php/excelReports/generate")
          .submit();
      } else {
        showActionProgress("");
        showMessage("e", "Please select an option to view report");
      }
    } else {
      showActionProgress("");
      showMessage("e", "Invalid date selection");
    }
  });

  /*$('#bc option[value=""]').remove();
	$('#bc').prepend("<option value='all' selected='selected'>All Branches</option>").css("font-size",'14px');*/

  $("#bc").css("font-size", "13px");

  $("#is_act").click(function() {
    if ($("#is_act").is(":checked")) {
      $("#is_act").val(1);
      $("#txt_rpt_from_r").attr("readonly", false);
      $("#txt_rpt_to_r").attr("readonly", false);
    }
  });

  $("#bc").change(function() {
    $(this)
      .parent()
      .find('input[type="radio"]')
      .attr("checked", true);
  });

  $("#btnGenerate").click(function() {
    var fd = $("#from_date")
      .val()
      .replace("-", "/");
    var td = $("#to_date")
      .val()
      .replace("-", "/");

    if (fd > td) {
      alert("Invalid date selection");
      return;
    }

    if (!$("input[type=radio]").is(":checked")) {
      alert("Please select an option to view report");
      return;
    }

    var bc_select = false;

    $(".bc_each").each(function() {
      if ($(this).is(":checked")) {
        bc_select = true;
      }
    });

    if (!bc_select) {
      alert("Please select branch");
      return;
    }

    if ($("input[name='by']:checked").val() == "rpt_audit_callout_report") {
      var selected_count = 0;

      $(".bc_each").each(function() {
        if ($(this).is(":checked")) {
          selected_count++;
        }
      });

      if (selected_count > 1) {
        alert("Only one branch allow to select for audit callout report ");
        return;
      }
    }

    if (
      $("input[name='by']:checked").val() == "rpt_pawnings" ||
      $("input[name='by']:checked").val() == "rpt_redeem"
    ) {
      var fh = $(".f_hour :selected").val();
      var fm = $(".f_minute :selected").val();
      var th = $(".t_hour :selected").val();
      var tm = $(".t_minute :selected").val();

      if (fh == "" && fm == "" && th == "" && tm == "") {
      } else {
        if (fh == "" || fm == "" || th == "" || tm == "") {
          alert("Invalid time setup");
          return;
        }
      }
    }

    $("#print_report_pdf").submit();
  });

  $(".txtmultiaccselector").autocomplete({
    source: "index.php/main/load_data/r_pawning/getAcc",
    select: function(event, ui) {
      var bt = ui.item.value;
      bt = bt.split(" - ");
      $(this)
        .parent()
        .append(
          '<div><div style="padding: 6px;padding-top:3px" class="bulk_item_row"><input type="hidden" class="GL_Accs" name="GL_Accs[]" value="' +
            bt[0] +
            '"><span class="bulk_item_names">' +
            bt[1] +
            '</span> <div class="bulk_list_item_remove" style="float:right;font-size:9px;cursor: pointer">Remove</div></div></div>'
        );
      $("#bulk_items_add")
        .val("")
        .focus();
      event.preventDefault();

      $(this)
        .val("")
        .focus();
    }
  });

  $(document).on("click", ".bulk_list_item_remove", function(event) {
    $(this)
      .parent()
      .remove();
  });
});

$(document).on("click", ".rd_ai_click", function() {
  if ($(this).prop("checked", true)) {
    $(this)
      .parent()
      .animate({ height: "330px" });
  }
});
