
$(document).on("click",".btn_vou_print",function(){

    $("#by").val("t_print_voucher");    
    $("#voucher_type").val($(this).attr("v_type"));
    $("#bc").val($(this).attr("bc"));
    $("#nno").val($(this).attr("nno"));
    $("#paid_acc").val($(this).attr("paid_acc"));
    $("#tot").val($(this).attr("tot"));
    $("#org_print").val($(this).attr("is_printed"));
    $("#print_pdf").submit();
    location.href = "";

});


$(document).ready(function(){

    $(".btn_vou_print_all").click(function(){
        save();
    });

    $(".chk_all_vou_prints").click(function(){
        
        if ( $(this).is(":checked") ){            
            $(".chk_each_vou_print").each(function(){
                $(this).prop("checked",true);                
            });
        }else{
            $(".chk_each_vou_print").each(function(){
                $(this).prop("checked",false);                
            });
        }        

    });

});


function save(){
    
    showActionProgress("Wait...");
       
    var f = $("#form_");

    $.ajax({            
        type: f.attr('method'),
        url: f.attr('action'),
        data: f.serialize(),
        dataType: "json",
        success: function (D) {            

            closeActionProgress();                        

        }
    });
    
}