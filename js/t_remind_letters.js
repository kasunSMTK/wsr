

$(document).on("click",".print_letter_list",function(){

	$("#l_list_no").val( $(this).attr("no") );
	$("#print_remind_letters_list").submit();

});

$(document).on("click",".lbl",function(){
	
	var obj = $(this).parent().find('input[type="checkbox"]');
	var s   = obj.attr("s");
	var no = obj.val();

	if (s == "all"){
		$("#l_no").val(no);
		$("#print_remind_letters").submit();
	}


});

$(document).on("click",".b",function(){	
	
	if ($(this).is(":checked")){
		$("."+$(this).attr("cn")).prop("checked",true);
	}else{
		$("."+$(this).attr("cn")).prop("checked",false);
	}
	
});


$(document).on("mouseover",".a",function(){	
	var c = $(this).attr("cn");
	$("."+c).css("background-color","#f1f1f1");
});

$(document).on("mouseout",".a",function(){	
	var c = $(this).attr("cn");
	$("."+c).css("background-color","#ffffff");	
});


$(document).on("click",".chk_bills_all",function(){
		
		if ($(this).is(":checked")){
			$(".chk_bills").prop("checked",true);
		}else{
			$(".chk_bills").prop("checked",false);
		}

	});


$(document).ready(function(){




	$("#letter_no").change(function(){
		var a = $("#letter_no :selected").val();
		$("#days_to_cal").val( $("#billtype :selected").attr("r"+a) );
	});

	$("#btnGet").click(function(){
		get_bills();
	});

	$("#btnLoad").click(function(){
		load_bills();
	});

	$("#btnSave").click(function(){
		save();
	});

	

});

function get_bills(){

	showActionProgress("Loading...");
			
	$.post("index.php/main/load_data/t_remind_letters/get_bills",{

		fd : $("#fd").val(),
		td : $("#td").val(),
		billtype : $("#billtype").val(),
		letter_no : $("#letter_no :selected").val(),
		days_to_cal : $("#days_to_cal").val()		

	},function(D){		
		closeActionProgress();
		
		var t = '<table border="0" width="100%" class="tbl_remind_letters_list">';

		if (D.s == 1){

				t += '<tr class="tr_asa">';
				t += '<td width="20"></td>';
				
				t += '<td>1st</td>';
				t += '<td>2nd</td>';
				t += '<td>3rd</td>';

				t += '<td>Bill Number</td>';
				t += '<td>Customer Name</td>';
				t += '<td>NIC</td>';
				t += '<td align="right">Amount</td>';
				t += '<td align="center">Final Date</td>';
				t += '<td align="center">Pawn Date</td>';
				t += '<td>Added by</td>';
				t += '<td><input type="checkbox" class="chk_bills_all"> Action</td>';
				t += '</tr>';

			for(n = 0 ; n < D.d.length ; n++){

				t += '<tr>';
				t += '<td></td>';

				var fl = sl = tl = "";
				var xc = ' - ';

				
					if (D.d[n].FL == "OK"){
						fl = "style='background-color:green;color:#ffffff;text-align:center;border-right:2px solid #ffffff'";
					}else{
						fl = "style='background-color:red;color:#ffffff;text-align:center;border-right:2px solid #ffffff'";
					}


					if (D.d[n].SL == "OK"){
						sl = "style='background-color:green;color:#ffffff;text-align:center;border-right:2px solid #ffffff'";
					}else{
						sl = "style='background-color:red;color:#ffffff;text-align:center;border-right:2px solid #ffffff'";
					}

					if (D.d[n].TL == "OK"){
						tl = "style='background-color:green;color:#ffffff;text-align:center;border-right:2px solid #ffffff'";
					}else{
						tl = "style='background-color:red;color:#ffffff;text-align:center;border-right:2px solid #ffffff'";
					}

				
				t += '<td '+fl+'>'+D.d[n].FL+'</td>';
				t += '<td '+sl+'>'+D.d[n].SL+'</td>';
				t += '<td '+tl+'>'+D.d[n].TL+'</td>';

				t += '<td>'+D.d[n].billno+'<input type="hidden" name="billno[]" value="'+D.d[n].billno+'"></td>';
				t += '<td>'+D.d[n].cusname+'</td>';
				t += '<td>'+D.d[n].nicno+'</td>';
				t += '<td align="right">'+D.d[n].requiredamount+'</td>';
				t += '<td align="center">'+D.d[n].finaldate+'</td>';
				t += '<td align="center">'+D.d[n].ddate+'</td>';
				t += '<td>'+D.d[n].op+'</td>';
				

				if ($("#letter_no :selected").val() == 1){
					if (D.d[n].FL == "OK"){
						t += '<td><input type="checkbox" class="chk_bills" name="chk_seleted_bills[]" value="'+D.d[n].billno+'" disabled></td>';
					}else{
						t += '<td><input type="checkbox" class="chk_bills" name="chk_seleted_bills[]" value="'+D.d[n].billno+'"></td>';
					}
				}

				if ($("#letter_no :selected").val() == 2){
					if (D.d[n].SL == "OK"){
						t += '<td><input type="checkbox" class="chk_bills" name="chk_seleted_bills[]" value="'+D.d[n].billno+'" disabled></td>';
					}else{
						t += '<td><input type="checkbox" class="chk_bills" name="chk_seleted_bills[]" value="'+D.d[n].billno+'"></td>';
					}
				}

				if ($("#letter_no :selected").val() == 3){
					if (D.d[n].TL == "OK"){
						t += '<td><input type="checkbox" class="chk_bills" name="chk_seleted_bills[]" value="'+D.d[n].billno+'" disabled></td>';
					}else{
						t += '<td><input type="checkbox" class="chk_bills" name="chk_seleted_bills[]" value="'+D.d[n].billno+'"></td>';
					}
				}

				t += '</tr>';

			}

		}else{
			t += '<tr><td>No bills found</td></tr>';
		}

		t += '</table>';


		$(".letter_list").html(t);


	},"json");

}


function load_bills(){

	showActionProgress("Loading...");
			
	$.post("index.php/main/load_data/t_remind_letters/load_bills",{

		fd : $("#fd").val(),
		td : $("#td").val(),
		billtype : $("#billtype").val(),
		letter_no : $("#letter_no :selected").val()		

	},function(D){		
		closeActionProgress();
		
		var t = '<table border="0" width="100%" class="tbl_remind_letters_list">';

		if (D.s == 1){

				t += '<tr class="tr_asa">';
				t += '<td width="20"></td>';
				t += '<td>Batch No and Date</td>';				
				t += '<td>Bill Number</td>';
				t += '<td>Customer Name</td>';
				t += '<td>NIC</td>';
				t += '<td align="right">Amount</td>';
				t += '<td align="center">Final Date</td>';
				t += '<td align="center">Pawn Date</td>';
				t += '<td>Action</td>';
				t += '</tr>';

				var az = "";

			for(n = 0 ; n < D.d.length ; n++){

				if (az != D.d[n].no){
					
					az = D.d[n].no;
					
					t += '<tr class="a batch_row_'+D.d[n].no+'" cn="batch_row_'+D.d[n].no+'" >';
					t += '<td></td>';
					t += '<td colspan="7" style="padding-left:10px">'+az+'  -  '+D.d[n].date+'</td>';						
					t += '<td><input type="checkbox" value="'+D.d[n].no+'" class="b" s="all" cn="batch_row_chk'+D.d[n].no+'"><lable class="lbl"><a>Print Batch</a></lable> | <a class="print_letter_list" no="'+D.d[n].no+'">Print List</a></td>';
					t += '</tr>';

				}else{					
					// if any row calculation here
				}
				
				t += '<tr class="a batch_row_'+D.d[n].no+'" cn="batch_row_'+D.d[n].no+'">';
				t += '<td></td>';
				t += '<td></td>';
				t += '<td>'+D.d[n].billno+'<input type="hidden" name="billno[]" value="'+D.d[n].billno+'"></td>';
				t += '<td>'+D.d[n].cusname+'</td>';
				t += '<td>'+D.d[n].nicno+'</td>';
				t += '<td align="right">'+D.d[n].requiredamount+'</td>';
				t += '<td align="center">'+D.d[n].finaldate+'</td>';
				t += '<td align="center">'+D.d[n].ddate+'</td>';
				t += '<td><input type="checkbox" value="'+D.d[n].no+'" class="batch_row_chk'+D.d[n].no+'"><lable class="lbl"><a>Print</a></lable></td>';
				t += '</tr>';		

			}

		}else{
			t += '<tr><td>No bills found</td></tr>';
		}

		t += '</table>';


		$(".letter_list").html(t);


	},"json");

}


function save(){
	
	var frm = $('#form_'); 

	$.ajax({
		type: frm.attr('method'), 
		url: frm.attr('action'), 
		data: frm.serialize(), 
		dataType: "json", 
		success: function (D) {
			closeActionProgress();	


			if (D.s == 1){
				$("#no").val(D.no);
				$("#btnGet").click();

				$("#l_no").val(D.no_saved);
				$("#print_remind_letters").submit();
			}

		} 
	});

}