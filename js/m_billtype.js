$(document).ready(function(){

	$('#inactive').prop('checked', false);

	load_list();
	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_billtype").val());
	});
	

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Bill Type saving success");
					clear_form();
					load_list();					
				}else{					
					showMessage("e",D.m);
				}
			}
		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_billtype",{
			code : code
		},function(D){		
			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_billtype",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#code").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	if ($("#description").val() == ""){showMessage("e","Please enter description");	return false; }	
	if (!validateAmount($("#amount_from").val() ," from amount")){ return false; }
	if (!validateAmount($("#amount_to").val() ," to amount")){ return false; }
	if ($("#period").val() == ""){showMessage("e","Invalid period input");	return false; }	
	if (!validateFloat($("#int_rate").val() ," Invalid interest rate input")){ return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$('#inactive').prop('checked', false);
	$('#tax').prop('checked', false);
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_billtype/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.code);
		$("#btnSave").val("Edit");
		$("#area").val(D.area);		
		$("#code").val(D.code);
		$("#description").val(D.description);
		$("#amount_from").val(D.amount_from);
		$("#amount_to").val(D.amount_to);
		$("#period").val(D.period);		
		$("#int_rate").val(D.int_rate);

		if (D.inactive == "1"){ $('#inactive').prop('checked', true); }else{ $('#inactive').prop('checked', false); }
		if (D.tax == "1"){ $('#tax').prop('checked', true); }else{ $('#tax').prop('checked', false); }

		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_billtype/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}