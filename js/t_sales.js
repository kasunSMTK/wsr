var redeem_trans_no = "";
var last_max_no = "";
var counter = "";
var disable_monitor = "";
var op_toggle = false;
var pblamt = 0;
var pblamt_constant = 0;
var is_bt_choosed = false;
var rowCount = 0;
var n = 1;
var temp = 0;

var rowno = 0;

$(document).ready(function() {
  $(".is_old_billno").on("mouseover", function() {
    $(".opn_bill_msg").css("visibility", "visible");
  });
  $(".is_old_billno").on("mouseout", function() {
    $(".opn_bill_msg").css("visibility", "hidden");
  });

  $(".is_old_billno").click(function() {
    if ($(this).is(":checked")) {
      $("#billno").focus();
    } else {
      $("#billno").focus();
    }
  });

  $("#billno").focus();

  $("#customer_advance_paying").change(function() {
    set_val_1($(this));
  });
  $("#customer_advance_paying").keyup(function() {
    set_val_1($(this));
  });

  $("#card_amount").change(function() {
    set_val_2($(this));
    calPayAmount();
  });
  $("#card_amount").keyup(function() {
    set_val_2($(this));
    calPayAmount();
  });

  $("#cash_amount").change(function() {
    calPayAmount();
  });
  $("#cash_amount").keyup(function() {
    calPayAmount();
  });
  $(".chk_pay_opt_apply").click(function() {
    if ($(this).is(":checked")) {
      $(this)
        .parent()
        .parent()
        .find(".pay-penal-sub")
        .animate(
          {
            height: "80px"
          },
          "fast"
        );
      $(this)
        .parent()
        .parent()
        .css("background", "#f9f9f9");
    } else {
      $(this)
        .parent()
        .parent()
        .find(".pay-penal-sub")
        .animate(
          {
            height: "0px"
          },
          "fast"
        );
      $(this)
        .parent()
        .parent()
        .css("background", "#ffffff");
    }
  });
  $(".pay-close").click(function() {
    $("#btnPaymentOptions").click();
  });

  $("#btnPaymentOptions").click(function() {
    if (!op_toggle) {
      $(".op_holder").animate(
        {
          height: "400px"
        },
        "slow"
      );
      op_toggle = true;
      $("html, body").animate(
        {
          scrollTop: $(document).height()
        },
        "slow"
      );
    } else {
      $(".op_holder").animate(
        {
          height: "0px"
        },
        "slow"
      );
      op_toggle = false;
      $("html, body").animate(
        {
          scrollTop: 0
        },
        "slow"
      );
    }
  });

  $(".cal_5").click(function() {
    if ($("#billtype").val() == "") {
      $("#bill_code_desc").focus();
      return;
    }

    if ($("#billno").val() == "") {
      $("#billno").focus();
      return;
    }

    LOAD_LOAN($("#billtype").val(), $("#billno").val());
    $("#no_of_int_cal_days").val((parseFloat($("#no_of_days").val()) % 30) - 1);
  });

  $("#bill_code_desc").autocomplete({
    source: "index.php/main/load_data/m_billtype_det/autocomplete",
    select: function(event, ui) {
      var bt = ui.item.value;
      bt = bt.split(" - ");
      $("#billtype").val(bt[0]);
      $("#billno")
        .val("")
        .focus();
    }
  });

  $("#customer_id").keypress(function() {
    $("#customer_id").autocomplete({
      source: "index.php/main/load_data/t_new_pawn/getBcCustomer",
      select: function(event, ui) {
        var bt = ui.item.value;
        bt = bt.split(" - ");
        $("#cus_serno").val(bt[0]);
        //$('#cat_code_desc').focus();
        //getCustomerInfo($("#cus_serno").val() , $("#customer_id").val() );

        getItemByCustomer(bt[0]);
      },
      delay: 1000
    });
  });

  $("#btnAddtoList").click(function() {
    AddtoList();
  });

  $("#billno").keypress(function(e) {
    if (e.keyCode == 13) {
      LOAD_LOAN($("#billtype").val(), $("#billno").val());
    }
  });

  $("#discount").keyup(function(e) {
    $("#pay_bal").val(getPayableAmount());
    calPayAmount();
  });
  $("#btnSave").click(function() {
    if (validate()) {
      save();
    }
  });

  $("#btnPrint").click(function(e) {
    $("#print_pdf").submit();
  });

  $("#discount").keypress(function(e) {
    if (e.keyCode == 13) {
      $("#btnSave").focus();
    }
  });
  $("#btnCancel").click(function() {
    if ($("#hid").val() != "") {
      cancelSale();
    } else {
      alert("Load Record Before Cancel");
    }
  });
  $("#btnReset").click(function() {
    redeem_trans_no = $("#no").val();
    resetFormData();
    $("#no").val(redeem_trans_no);
    $("#btnSave")
      .attr("disabled", false)
      .attr("class", "btn_regular");
    $("#btnReset")
      .attr("disabled", true)
      .attr("class", "btn_regular_disable");
    $(".pawn_msg").fadeOut();
    $("#bill_code_desc").focus();
    $("#no").val(last_max_no);
    $("#btnPrint")
      .attr("disabled", true)
      .attr("class", "btn_regular_disable");
  });
  $("#no").keypress(function(e) {
    if (e.keyCode == 13) {
      //getSavedLoans($(this).val());
    }
  });
  //load saved details when enter nno
  $("#nno").keypress(function(e) {
    $("#hid").val($(this).val());
    if (e.keyCode == 13) {
      $("#nno").attr("disabled", "disabled");
      $("#r_no").val($("#nno").val());
      $("#is_reprint").val(1);
      $("#btnPrint")
        .attr("disabled", false)
        .attr("class", "btn_regular");
      load_data($(this).val());
    }
  });

  $("#manual_bill_no").keypress(function(e) {
    if (e.keyCode == 13) {
      showActionProgress("Please wait...");
      $.post(
        "index.php/main/load_data/t_new_pawn/get_system_billno",
        {
          manual_bill_no: $("#manual_bill_no").val(),
          cut_bc_no: 1
        },
        function(D) {
          closeActionProgress();
          if (D.s == 1) {
            $("#billno").val(D.billno);
            LOAD_LOAN(
              $("#billtype").val(),
              $("#billno").val(),
              $("#ddate").val()
            );
          } else {
            alert("Invalid manual bill number ");
          }
        },
        "json"
      );
    }
  });

  $(".nostampdutycal").click(function() {
    LOAD_LOAN($("#billtype").val(), $("#billno").val());
  });
});

//load category
$(document).on("keypress", "#cat_code_desc", function() {
  $("#cat_code_desc").autocomplete({
    source: "index.php/main/load_data/m_item_category/autocompleteAll",
    select: function(event, ui) {
      var bt = ui.item.value;
      bt = bt.split(" - ");
      $("#cat_code").val(bt[0]);

      $("#item_tag").val("");
      $("#item_tag").focus();
    }
  });
});

$(document).on("keypress", "#item_tag", function() {
  var t = $("#item_tag").val();
  var cat_code = $("#cat_code").val();

  var is_redeem = document.getElementById("is_redeem");
  console.log(is_redeem.checked);

  $("#item_tag").autocomplete({
    source:
      "index.php/main/load_data/t_tag/autocomplete?kk=" +
      t +
      "&cat_code=" +
      cat_code +
      " &is_redeem=" +
      is_redeem.checked,

    select: function(event, ui) {
      console.log(ui.item);
      var bt = ui.item.value;
      bt = bt.split(" - ");
      console.log(bt[1]);
      // alert(bt[0]);
      if (!is_redeem.checked) {
        $("#amount").val(0.0);
        $("#description").val(bt[1]);
        $("#billno").val("");
        $("#requiredamount").val(0.0);
        $("#description").attr("disabled", true);
        $("#requiredamount").attr("disabled", true);
        $("#billno").attr("readonly", false);
        $("#amount").attr("readonly", false);
      } else {
        $("#tag_no").val(bt[0]);
        getItemByTag(bt[0]);
      }
    }
  });
});

//discount amount blur calculate net amount

$(document).on("keyup", "#discount_amount", function() {
  calculateNetAmount();
});

$(document).on("click", ".opt_billtype_select", function(event) {
  $("#billtype").val($(this).val());
  $(".msg_pop_up_bg")
    .css("color", "#000000")
    .fadeOut(0);
  $(".multiple_billtype_selection")
    .css("color", "#000000")
    .fadeOut(0);
  is_bt_choosed = true;
  LOAD_LOAN($("#billtype").val(), $("#billno").val());
  $("#is_bt_choosed").val(is_bt_choosed);
});

function load_data(id) {
  showActionProgress("Loading...");
  $("#btnSave")
    .attr("disabled", true)
    .attr("class", "btn_regular_disable");
  $("#btnCancel")
    .attr("disabled", false)
    .attr("class", "btn_regular");
  $.ajax({
    url: "index.php/main/load_data/t_sales/getrecord",
    type: "POST",
    data: {
      id: id
    },
    dataType: "json",
    success: function(r) {
      if (r != 2) {
        $("#customer_id").val(
          r.sum[0].customer_id +
            " - " +
            r.sum[0].nicno +
            " - " +
            r.sum[0].cusname
        );
        var chk =
          r.sum[0].is_redeem == 1
            ? $("#is_redeem").prop("checked", true)
            : $("#is_redeem").prop("checked", false);
        $("#date").val(r.sum[0].ddate);
        $("#total_amount").val(r.sum[0].total_amount);
        $("#discount_amount").val(r.sum[0].total_discount);
        $("#net_amount").val(r.sum[0].net_amount);
        $("#note").val(r.sum[0].note);
        $("#store").val(r.sum[0].store);
        $("#employee").val(r.sum[0].employee);
        $("#advance_amount").val(r.sum[0].advance_amount);
        $("#itemlist").html("");
        for (var i = 0; i < r.det.length; i++) {
          var s = "";
          //get item description by tag
          $.ajax({
            url: "index.php/main/load_data/t_tag/getItemByTag",
            type: "POST",
            data: {
              tag: r.det[i].tag_no
            },
            dataType: "json",
            async: false,
            success: function(res) {
              var category =
                r.det[i].cat_code +
                "-" +
                r.det[i].category_dec +
                "-" +
                r.det[i].is_bulk;
              var item_tag =
                r.det[i].tag_no + "-" + r.det[i].item + "-" + r.det[i].tag_no;
              var tag_no = r.det[i].tag_no;

              var amount = r.det[i].amount;
              var description =
                res[0].gold_des + res[0].elec_des + res[0].veh_des;

              var billno = res[0].billno;
              var requiredamount = res[0].requiredamount;

              console.log(r.det[i]);

              AddToListAppend(
                s,
                category,
                item_tag,
                tag_no,
                description,
                billno,
                requiredamount,
                amount
              );
            }
          });
        }
      } else {
        alert("No record Found");
      }
      closeActionProgress();
    }
  });
}

function AddtoList() {
  var s = "";
  var category = $("#cat_code_desc").val();
  var item_tag = $("#item_tag").val();
  var tag_no = $("#tag_no").val();
  var description = $("#description").val();
  var amount = $("#amount").val();
  var billno = $("#billno").val();
  var requiredamount = $("#requiredamount").val();

  if (category == "" || category == null) {
  } else if (item_tag == "" || item_tag == null) {
  } else if (amount == "" || amount == null) {
  } else {
    AddToListAppend(
      s,
      category,
      item_tag,
      tag_no,
      description,
      billno,
      requiredamount,
      amount
    );
  }
}

function AddToListAppend(
  s,
  category,
  item_tag,
  tag_no,
  description,
  billno,
  requiredamount,
  amount
) {
  s += "<tr class='rows_record'> <td width='145px'>";
  s +=
    "<div class='text_box_holder_new_pawn' style='width:100%;border:1px solid #ccc'>									";
  s +=
    "<input class='input_text_regular_new_pawn' type='text' id='cat_code_desc' style='width:145px' value='" +
    category +
    "''>";
  s += "<input type='hidden' id='cat_code' name='cat_code' value=''>";
  s += "</div>";
  s += "</td>";
  s += "<td width='145px'>";
  s +=
    "<div class='text_box_holder_new_pawn' style='width:100%;border:1px solid #ccc'>";
  s +=
    "<input class='input_text_regular_new_pawn' type='text' id='item_tag_" +
    rowno +
    "' name='item_tag_" +
    rowno +
    "' style='width:145px' value='" +
    item_tag +
    "'>";
  s +=
    "<input type='hidden' id='tag_no_" +
    rowno +
    "' name='tag_no_" +
    rowno +
    "' value='" +
    tag_no +
    "'>";
  s += "</div>";
  s += "</td>";
  s += "<td width='250'>									";
  s +=
    "<div class='text_box_holder_new_pawn' style='width:100%;border:1px solid #ccc;border-left:none'>";
  s +=
    "<input class='input_text_regular_new_pawn' type='text' id='description' name='description' style='width:250px' value='" +
    description +
    "'>";
  s += "</div>";
  s += "</td>";
  s += "<td width='100'>									";
  s +=
    "<div class='text_box_holder_new_pawn' style='width:100%;border:1px solid #ccc;border-left:none'>";
  s +=
    "<input class='input_text_regular_new_pawn' type='text' id='billno_" +
    rowno +
    "' name='billno_" +
    rowno +
    "'  value='" +
    billno +
    "' style='width:100px'  readonly='readonly'>";
  s += "</div>";
  s += "</td>";
  s += "<td width='20'>									";
  s +=
    "<div class='text_box_holder_new_pawn' style='width:100%;border:1px solid #ccc;border-left:none'>";
  s +=
    "<input class='input_text_regular_new_pawn' type='text' id='requiredamount_" +
    rowno +
    "' name='requiredamount_" +
    rowno +
    "'  value='" +
    requiredamount +
    "' style='width:100px'  readonly='readonly'>";
  s += "</div>";
  s += "</td>";
  s += "<td width='100'>";
  s +=
    "<div class='text_box_holder_new_pawn' style='width:123px;border:1px solid #ccc;border-left:none'>";
  s +=
    "<input class='input_text_regular_new_pawn' type='text' id='amount_" +
    rowno +
    "' name='amount_" +
    rowno +
    "' value='" +
    amount +
    "' style='width:98%' readonly='readonly'>";
  s += "</div>";
  s += "</td>";
  s += "<td width='75'>";
  //s += "<div class='text_box_holder_new_pawn' style='width:75px;border:1px solid #ccc;border-left:none;'>";
  s += "</div>";
  s += "</td>";
  s += "</tr>";

  //$("#itemlist").html('');
  //alert(s);

  $("#itemlist").append(s);
  rowno++;
  calculateItemTotal();
  calculateNetAmount();
  $("#cat_code_desc").val("");
  $("#item_tag").val("");
  $("#tag_no").val("");
  $("#description").val("");
  $("#amount").val("");
  $("#billno").val("");
  $("#requiredamount").val("");
}

function getItemByTag(tag) {
  $("#description").val("");
  $.ajax({
    url: "index.php/main/load_data/t_tag/getItemByTag",
    type: "POST",
    data: {
      tag: tag
    },
    dataType: "json",
    success: function(r) {
      console.log(r[0]);
      if (r[0].is_reserved == "1") {
        alert("Item Alredy Reserved by \n " + r[0].reserved_note);
      } else {
        $("#amount").val(r[0].value);
        $("#description").val(r[0].gold_des + r[0].elec_des + r[0].veh_des);
        $("#billno").val(r[0].billno);
        $("#requiredamount").val(r[0].requiredamount);
      }
    }
  });
}
function getItemByCustomer(customer) {
  $.ajax({
    url: "index.php/main/load_data/t_tag/loadIsReceive",
    type: "POST",
    data: {
      customer: customer
    },
    dataType: "json",
    success: function(r) {
      console.log(r.det);
      console.log(r.sum[0]);
      if (r.sum.length > 0) {
        $("#total_amount").val(r.sum[0].total_amount);
        $("#net_amount").val(r.sum[0].net_amount);
        $("#advance_amount").val(r.sum[0].payment_amount);
        $("#adv_sum").val(r.sum[0].adv_sum);

        $("#itemlist").html("");
      }

      for (var i = 0; i < r.det.length; i++) {
        var s = "";

        var category =
          r.det[i].cat_code +
          "-" +
          r.det[i].category_dec +
          "-" +
          r.det[i].is_bulk;
        var item_tag =
          r.det[i].tag_no + "-" + r.det[i].item + "-" + r.det[i].tag_no;
        var tag_no = r.det[i].tag_no;

        var amount = r.det[i].amount;
        var description = r.det[0].gold_des;

        var billno = r.det[0].billno;
        var requiredamount = r.det[0].requiredamount;

        AddToListAppend(
          s,
          category,
          item_tag,
          tag_no,
          description,
          billno,
          requiredamount,
          amount
        );
      }
    }
  });
}

function resetFormData() {
  ddate = $("#ddate").val();
  bc_no = $("#bc_no").val();
  $("input:text").val("");
  $("#ddate").val(ddate);
  $("#bc_no").val(bc_no);
}

function cancelSale() {
  console.log($("#hid").val());
  var id = $("#hid").val();
  showActionProgress("Cancel...");
  $.ajax({
    url: "index.php/main/load_data/t_sales/delete_record",
    type: "POST",
    data: {
      no: id
    },
    dataType: "json",
    async: false,
    success: function(D) {
      // alert("Data inserted");
      closeActionProgress();
      if (D == 1) {
        document.location.reload(true);
      }
    }
  });
}

function calculateItemTotal() {
  var total = 0;

  $(".rows_record").each(function(i, obj) {
    var amount = parseFloat($("#amount_" + i).val());
    total += amount;
  });

  $("#total_amount").val(total);
}

function calculateNetAmount() {
  var total = parseFloat($("#total_amount").val());
  var discount = 0;
  var advance_amount =
    $("#advance_amount").val() != "" ? $("#advance_amount").val() : 0.0;

  if (parseFloat($("#discount_amount").val()) > 0) {
    discount = parseFloat($("#discount_amount").val());
  } else {
    discount = parseFloat(0);
    $("#discount_amount").val(discount);
  }

  $("#net_amount").val(total - discount);
  $("#balance_amount").val(total - discount - advance_amount);
}

function save() {
  showActionProgress("Saving...");
  $("#hid_rows").val(rowno);
  if (validate_form()) {
    var frm = $("#form_");
    $.ajax({
      type: frm.attr("method"),
      url: frm.attr("action"),
      data: frm.serialize(),
      dataType: "json",
      success: function(D) {
        // alert("Data inserted");
        if (D > 0) {
          alert("Item Saved Successfuly");

          $("#r_no").val($("#nno").val());
          $("#print_pdf").submit();
          closeActionProgress();
          document.location.reload(true);
        } else {
          alert("Somthing Wrong Try again!");
        }
      }
    });
  } else {
  }
}

function LOAD_LOAN(billtype, billno) {
  showActionProgress("Loading...");
  $.post(
    "index.php/main/load_data/t_tag/LOAD_LOAN",
    {
      billtype: billtype,
      billno: billno,
      bc_no: $("#bc_no").val()
    },
    function(D) {
      closeActionProgress();
      if (D != "0") {
        rowCount = D.loan_det.length;
        //alert(JSON.stringify(D));
        $("#rowCount").val(D.loan_det.length);
        $("#loanno").val(D.loan_sum.loanno);
        $("#goldvalue").val(D.loan_sum.goldvalue);
        $("#billtype").val(D.loan_sum.billtype);
        $("#pawn_date").val(D.loan_sum.ori_pwn_date);
        $("#forfeit_date").val(D.loan_sum.forfeit_date);
        $("#int_rate").val(D.loan_sum.fmintrate);
        $("#period").val(D.loan_sum.period);
        $("#amount").val(D.loan_sum.requiredamount);
        $("#interest").val(D.loan_sum.fmintrest);
        $("#customer").val(D.loan_sum.nicno);
        var cat_code = D.loan_sum.cat_code;
        var det = [];
        det.push(D.loan_det);
        //alert(det[0].loanno);
        set_item_grid(cat_code, det[0]);
      } else {
        alert("No Records for this BillNo or it is already tagged");
      }
    },
    "json"
  );
}

function validate() {
  var ready = true;
  if ($("#customer_id").val() == "") {
    alert("Please Select Customer");
    ready = false;
  } else if ($("#store").val() == "") {
    alert("Please Select The Store");
    ready = false;
  } else if ($("#employee").val() == "") {
    alert("Please Select The Employee");
    ready = false;
  } else {
    //if( $(".catg").val() )
  }

  return ready;
}

function LOAD_LOAN1(billtype, billno) {
  showActionProgress("Loading...");
  $("#btnSave")
    .attr("disabled", true)
    .attr("class", "btn_regular_disable");
  is_old_billno = 0;
  if ($(".is_old_billno").is(":checked")) {
    is_old_billno = 1;
  }
  $.post(
    "index.php/main/load_data/t_tag/LOAD_LOAN",
    {
      billtype: billtype,
      billno: billno,
      five_days_int_cal: $(".cal_5").is(":checked") ? 1 : 0,
      is_bt_choosed: $("#is_bt_choosed").val(),
      bc_no: $("#bc_no").val(),
      is_old_billno: is_old_billno
    },
    function(D) {
      closeActionProgress();
      if (D.s == 1) {
        $("#r_no").val(D.tr_no);
        $("#by").val("redeem_ticket");
        $("#ln").val(D.loan_sum.loanno);
        $("#is_reprint").val(1);

        if (D.loan_sum.status != "F") {
          alert("Invalid Bill No.");
          $(".pawn_msg")
            .html("")
            .fadeOut();
          $("#btnSave")
            .attr("disabled", false)
            .attr("class", "btn_regular");
          $("#btnReset")
            .attr("disabled", true)
            .attr("class", "btn_regular_disable");
        }

        $("#billcode").val(D.loan_sum.billcode);
        $("#billtype,#bill_code_desc").val(D.loan_sum.billtype);
        $("#hid_loan_no").val(D.loan_sum.loanno);
        $("#hid_cus_serno").val(D.loan_sum.cus_serno);
        $("#dDate").val(D.loan_sum.ddate);
        $("#time").val(D.loan_sum.time);
        $("#period").val(D.loan_sum.period);
        $("#finaldate").val(D.loan_sum.finaldate);
        $("#loan_amount").val(D.loan_sum.requiredamount);
        $("#fmintrate").val(D.loan_sum.fmintrate);
        // calculations from php

        if (D.loan_sum.is_renew == 1) {
          $("#no_of_days").val(
            D.int.no_of_days + "     +" + D.loan_sum.old_bill_age
          );
        } else {
          $("#no_of_days").val(D.int.no_of_days);
        }

        $("#interest").val(D.int.total_int.toFixed(2));
        $("#paid_interest").val(D.int.paid_int);
        $("#stamp_fee").val(0);
        $("#goldvalue").val(D.loan_sum.goldvalue);

        var doc_fee_tot = 0;
        if (D.doc_fees != "") {
          for (nd = 0; nd < D.doc_fees.length; nd++) {
            doc_fee_tot += parseFloat(D.doc_fees[nd].document_charge);
          }
        } else {
          //$("#doc_fee_tot").val(doc_fee_tot.toFixed(2));
        }

        $("#customer_advance").val(D.customer_advance.balance);
        $("#customer_advance_paying").val(D.customer_advance.balance);
        $("#balance").val(parseFloat(D.int.payable_tot_int_balance).toFixed(2));

        var loan_amount = parseFloat($("#loan_amount").val());
        var stamp_fee = 0;
        var doc_fee_tot = 0;
        var balance = parseFloat($("#balance").val());
        pblamt = loan_amount + balance + doc_fee_tot;
        pblamt -= parseFloat($("#customer_advance").val());
        final_payable_amount = loan_amount + balance + doc_fee_tot;
        if (final_payable_amount >= 25000) {
          if ($(".nostampdutycal").is(":checked")) {
            $("#stamp_fee").val("0.00");
          } else {
            //pblamt += 25;
            //$("#stamp_fee").val("25.00");
          }
        }

        if (D.int.int_within_10days) {
          pblamt -= D.int.refundable_int;
          $("#refundable_int").val(D.int.refundable_int.toFixed(2));
        }

        pblamt_constant = pblamt;
        $("#payable_amount").val(pblamt.toFixed(2)); //XXX
        calPayAmount();
        $("#cus_info_div")
          .css({
            height: "42px"
          })
          .html(D.cus_info)
          .animate({
            height: "398px"
          });
        $("#billtype_info_div").html(D.rec);
        is_bt_choosed = false;
      } else {
        showMessage("e", "Invalid bill type or bill number");
        $(".pawn_msg").fadeOut();
        $("#cus_info_div").html("");
        $("#btnSave")
          .attr("disabled", false)
          .attr("class", "btn_regular");
        $("#btnReset")
          .attr("disabled", true)
          .attr("class", "btn_regular_disable");
      }
    },
    "json"
  );
}

function validate_form() {
  return true;
}

function setEdit(recid) {
  showActionProgress("Loading...");
  $.post(
    "index.php/main/load_data/t_new_pawn/set_edit",
    {
      recid: recid
    },
    function(D) {
      closeActionProgress();
      $("#btnPrint")
        .attr("disabled", false)
        .attr("class", "btn_regular");
    },
    "json"
  );
}

function get_category(ind) {
  var s = "";
  $.ajax({
    url: "index.php/main/load_data/t_tag/get_cat",
    type: "POST",
    data: {
      ind: ind
    },
    dataType: "text",
    async: false,
    success: function(r) {
      s = r;
      $(document).on("change", ".catg", function(event) {
        var ind = $(this).attr("ind");
        var cat = $(this).val();
        get_item(ind, cat);
      });
    }
  });
  return s;
}

function get_item(ind, cat) {
  var s = "";
  $.ajax({
    url: "index.php/main/load_data/t_tag/get_item",
    type: "POST",
    data: {
      ind: ind,
      cat: cat
    },
    dataType: "text",
    async: false,
    success: function(r) {
      $("#it_" + ind).html("");
      $("#it_" + ind).html(r);
    }
  });

  return s;
}

function get_karatage(ind) {
  var s = "";
  $.ajax({
    url: "index.php/main/load_data/t_tag/get_karatage",
    type: "POST",
    data: {
      ind: ind
    },
    dataType: "text",
    async: false,
    success: function(r) {
      s = r;
    }
  });
  return s;
}
