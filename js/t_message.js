$(document).on('click', '.delete_msg', function(event) {
    var id = $(this).attr("id");

        if (confirm("Do you want delete this message?")){
            $.post("index.php/main/load_data/t_message/delete_message",{    
                id : id
            },function(D){              
                closeActionProgress();                
                if (D.s == 1){
                    showMessage("s","Message deleted");     
                    $(".old_message").html(D.data);                
                }else{
                    showMessage("e","Message not delete"); 
                }

            },"json");
        }
    
});

$(document).ready(function(){

    $("#hp").click(function(){

        if ($(this).is(":checked")){
            $("#hp_msg").val(1);
        }else{
            $("#hp_msg").val(0);
        }

    });

    $("#btnSend").click(function(event) {
        send_message();
    });

    $("#msg").keyup(function(event) {
        textAreaAdjust(this);
    }); 

    $("#btnSave").click(function(event) {
        save();
    }); 

        

});

function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25+o.scrollHeight)+"px";
}

function send_message(){

    if ($(".msg_create_textarea").val() == ""){
        $(".msg_create_textarea").focus();
        return false;
    }


    if ($("#hp").is(":checked")){
        hp = 1;
    }else{
        hp = 0;
    }
    
    showActionProgress("Sending...");

    $("#btnSend").attr("disabled",true).attr("class","btn_regular_disable");
    
    var frm = $("#form_");

    $.post("index.php/main/load_data/t_message/send_message",{    
        msg : $("#msg").val(),
        hp : hp
    },function(D){              
        closeActionProgress();
        $("#btnSend").attr("disabled",false).attr("class","btn_regular");
        if (D.s == 1){
            showMessage("s","Message sent"); 
            $(".old_message").html(D.data);
            $(".msg_create_textarea").val("")
            $("#hp").prop("checked",false);
        }else{
            showMessage("e","Message not sent"); 
        }

    },"json");
}


function save(){

    var frm         = $('#form_');
    var fd          = new FormData();
    var file_data   = $('input[type="file"]')[0].files;

    for(var i = 0 ; i<file_data.length ; i++){
        fd.append("userfile", file_data[i]);
    }

    var other_data = $('form').serializeArray();        

    $.each(other_data,function(key,input){
        fd.append(input.name,input.value);
    });     
    
    showActionProgress("Sending...");

    $("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");

    $.ajax({
        url:  frm.attr('action'),
        data: fd,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType : 'json',
        success: function(D){

            closeActionProgress();
            
            $("#btnSave").attr("disabled",false).attr("class","btn_regular");
            
            if (D.s == 1){                

                showMessage("s","Message sent"); 
                $(".old_message").html(D.data);
                $(".msg_create_textarea").val("");
                $("#hp_msg").val(0);
                $("#userfile").val("");
                $("#hp").prop("checked",false);
            }else{
                showMessage("e","Message not sent"); 
            }

        }
    });

}

