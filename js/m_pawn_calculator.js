var names = [];
var bcCustomer = [];
var gold_q = [];
var currentDate = "";
var ra_normal = 0;
var int_normal = 0;

function setBulkItemsShow(bulk_items){	
	var bulk_items = bulk_items.split(",");	
	var T = "";

	$(bulk_items).each(function(index, el) {
		T +='<div id="pn1" style="border-bottom:1px dotted #ccc;height:47px"><div style="width:155px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" readonly="readonly" style="width:140px" id="" class="input_text_regular_new_pawn catC"></div><div style="width:195px;float:left" class="text_box_holder_new_pawn"><span>'+bulk_items[index]+'</span></div><div style="width:150px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:137px" id="condition_desc" class="input_text_regular_new_pawn conC"></div></div></div>';
	});

	$("#item_list_holder").append(T);
}

$(document).on('click', '.bulk_list_item_remove', function(event) {
	$(this).parent().remove();
});

$(document).on('click', '.apply_bulk_items', function(event) {
	
	var b_i_names = Array();

	$(".bulk_item_names").each(function(index, el) {
	 	b_i_names[index] = $(this).html();
	});

	var str1 = b_i_names.toString();

	$("#item_code :selected").val(str1);
	$("#item_code :selected").text(str1);
	$("#condition_desc").focus();
	$(".div_bulk_items").remove();

});

$(document).on('click', '.clear_bulk_items', function(event) {
	alert("Clear");
});

$(document).on('keyup', '#bulk_items_add', function(event) {
	event.preventDefault();

	$( "#bulk_items_add" ).autocomplete({
		source: "index.php/main/load_data/m_item/getItemsForBulk",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$(this).parent().append('<div><div style="padding: 6px;padding-top:10px" class="bulk_item_row"><input type="hidden" class="bulk_item_codes" value="'+bt[0]+'"><span class="bulk_item_names">'+bt[1]+'</span> <div class="bulk_list_item_remove" style="float:right;font-size:9px;cursor: pointer">Remove</div></div></div>');
				$( "#bulk_items_add" ).val("").focus();
				event.preventDefault();
		}
	});

});

$(document).ready(function(){

	$("#gold_type_desc").focus();

	$("#billno").focus(function() {
		close_exist_msg();
	});	

	$("#billno").blur(function(){
		$.post("index.php/main/load_data/t_opening_pawn/check_bill_no",{
			billtype : $("#billtype").val(),
			billno : $(this).val()
		},function(D){

			if (D.is_bill_number_exist == 1){
				setBillStatusMSG("EX");
			}

		},"json");
	});

	
	currentDate = $("#dDate").val();
	//resetFormData();
	$("#dDate").val(currentDate);

	//$("#customer_id").focus();
	$("#btnSave").attr("disabled",false);
	$("#btnReset,#btnPrint").attr("disabled",true);

	$("#customer_id").keypress(function(){
		if ( $("#chkShowSugg").is(":checked") ){			
			$( "#customer_id" ).autocomplete({
				source: bcCustomer,
				select: function (event, ui) {					
					var bt = ui.item.value;
					bt = bt.split(" - ");
					$("#cus_serno").val(bt[0]);
					//$('#cat_code_desc').focus();
					//$("#bill_code_desc,#billtype").val("").focus();
					getCustomerInfo($("#cus_serno").val() , $("#customer_id").val() );
				}
			});
		}
	});	

	$("#chkShowSugg").click(function(){
		$("#customer_id").val("").focus();
	});
	
	$("#btnSave").click(function(){		
		save();
	});	
	
	$("#btnFrontSave").click(function(){		
		saveFront();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_nicNo").val());
	});

	$("#btnReset").click(function(){
		resetFormData();
		$("#customer_id").focus();
	});

	$("#btnCancel").click(function(){
		CANCEL_BILL( $("#loan_no").val() );
	});

	$("#btnCancelFrontCusInput").click(function(){		
		resetFrontCusInputs();
		hideCusUI();
	});

	$("#btnAddtoList").click(function(){
		calValue();
		AddtoList();		
	});

	$("#btnPrint").click(function(){
		$("#print_pdf").submit();		
		//if ($("#is_pre_intst_chargeable").val() == 1){ $("#first_int_print_pdf").submit(); }
		//$("#first_int_print_pdf").submit();
	});

	$("#customer_id").keypress(function(e){
		if (e.keyCode == 13){			
			getCustomerInfo($("#cus_serno").val() , $("#customer_id").val() );
			$("#cus_serno").val("");
			//$("#bill_code_desc,#billtype").val("");
			$("#bill_code_desc").focus();


			//$("#cat_code_desc").focus();			
		}
	});

	$("#customer_id").keypress(function(){
		$("#cus_serno").val("");
	});


	$("#bill_code_desc").keypress(function(e){
		if (e.keyCode == 13){			
			if ($("#bill_code_desc :selected").val() != "" ){
				$("#billtype").val($(this).val());
				$("#billno").focus().select();
			}
		}
	});

	$("#bill_code_desc").change(function(e){		
		if ($("#bill_code_desc :selected").val() != "" ){
			$("#billtype").val($(this).val());
			$("#billno").focus().select();
		}		
	});

	$("#billno").keypress(function(e){
		if (e.keyCode == 13){									
			if ($("#billno").val() != ""){
				$("#fMintrate").focus().select();
			}
		}
	});

	$("#fMintrate").keypress(function(e){
		if (e.keyCode == 13){									
			if ($("#fMintrate").val() != ""){
				$("#fMintrate2").focus().select();
			}
		}
	});

	$("#fMintrate2").keypress(function(e){
		if (e.keyCode == 13){									
			if ($("#fMintrate2").val() != ""){
				$("#period").focus().select();
			}
		}
	});

	$("#period").keypress(function(e){
		if (e.keyCode == 13){									
			if ($("#period").val() != ""){
				$("#cat_code_desc").focus().select();
			}
		}
	});

	$("#cat_code_desc").keypress(function(e){
		if (e.keyCode == 13){			
			$("#item_code").focus();
		}
	});
	
	$("#item_code").keypress(function(e){
		if (e.keyCode == 13){			
			$("#condition_desc").focus();
		}
	});

	$("#item_code").change(function(){		
		$("#condition_desc").focus();		
	});
	
	$("#gold_type_desc,#gold_qulty,#qty,#t_weigth,#p_weigth").blur(function(e){
		calValue();
	});

	$("#gold_type_desc").keypress(function(e){
		if (e.keyCode == 13){			
			$("#gold_qulty").focus();			
		}
	});

	$("#gold_qulty").keypress(function(e){
		if (e.keyCode == 13){			
			$("#qty").focus();
		}
	});
	
	$("#qty").keypress(function(e){
		if (e.keyCode == 13){			
			$("#t_weigth").focus();
		}
	});

	$("#t_weigth").keypress(function(e){
		if (e.keyCode == 13){			
			$("#p_weigth").focus();
		}
	});

	$("#p_weigth").keypress(function(e){
		if (e.keyCode == 13){
			calValue();
			$("#btnAddtoList").focus();
		}
	});
	
	$("#gold_qulty").keypress(function(e){
		if (e.keyCode == 13){			
			$("#qty").focus();
		}
	});

	$("#loan_no").keypress(function(e){
		if (e.keyCode == 13){
			if ( rmsps( $(this).val() ) != ""){
				LOAD_LOAN( rmsps( $(this).val() ));
			}
		}
	});

	$("#loan_no").blur(function(e){
		if ( rmsps( $(this).val()) != ""){			
			LOAD_LOAN(rmsps( $(this).val()));
		}
	});

	$(".listur").click(function(){
		alert("")
	});	

	/*$( "#bill_code_desc" ).autocomplete({
		source: "index.php/main/load_data/m_billtype_det/autocomplete_pawn_add_bill_type",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#billtype").val(bt[0]);
				//getBillTypeInfo(bt[0]);				
				$("#billno").focus().select();
		}
	});	*/

	$( "#cat_code_desc" ).autocomplete({
		source: names,
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#cat_code").val(bt[0]);
				getItemsByCat(bt[0],bt[2]);
				$("#item_code").focus();
		}
	});

	$( "#condition_desc" ).autocomplete({
		source: "index.php/main/load_data/m_conditions/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#condition").val(bt[0]);		
				$("#gold_type_desc").focus();		
		}
	});

	$( "#gold_type_desc" ).autocomplete({
		source: "index.php/main/load_data/r_gold_rate/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#goldcatagory").val(bt[0]);
				$("#gold_rate").val(bt[2]);
				$("#gold_qulty").focus().select();
		}
	});

	$( "#gold_qulty" ).autocomplete({
		source: gold_q,
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#gold_qulty_rate").val(bt[0]);				
				$("#gold_qulty_code").val(bt[1]);				
				$("#qty").focus();
		}
	});


	$(document).on( "click", "#allow_cal_with_advance", function() { 
		
		// checkbox click event
		
	});


	$("#requiredamount,#fmintrest").keyup(function(){
		setPayableAmount();
	});

	$("#requiredamount,#fmintrest").change(function(){
		setPayableAmount();
	});
	
	$("#requiredamount").keypress(function(e){
		if (e.keyCode == 13){
			if ( $("#requiredamount").val() != ""){
				$("#fmintrest").focus().select();
			}
		}
	});

	$("#fmintrest").keypress(function(e){
		if (e.keyCode == 13){
			if ( $("#fmintrest").val() != ""){
				$("#stamp_fee").focus().select();
			}
		}
	});

	$("#stamp_fee").keypress(function(e){
		if (e.keyCode == 13){
			if ( $("#stamp_fee").val() != ""){
				$("#btnSave").focus();
			}
		}
	});


});

function close_exist_msg(){
	$(".canceled_bill_msg").css("visibility","visible");
	$(".canceled_bill_msg").animate({height: "0px"});
	$(".msg_txt").html("");
}

function setPayableAmount(){

	var requiredamount 	= isNaN(parseFloat($("#requiredamount").val())) ? 0 : parseFloat($("#requiredamount").val()); 
	var fmintrest 	= isNaN(parseFloat($("#fmintrest").val())) ? 0 : parseFloat($("#fmintrest").val()); 

	var payable_amount 	= requiredamount;

	$("#payable_amount").val(payable_amount.toFixed(2) );

}


function setBTDetails(x){
	if ( validateAmount(x.toFixed(2)) ){				
		if (x > 0){				
			setBillTypeValue(x, $("#goldvalue").val() );
			calStampFee(x);
			$("#btnSave").focus();
		}
	}
}

function calStampFee(required_amount){

	if ( required_amount > 25000 ){

		var stamp_fee = ( required_amount * 2 ) / 100 ;
		$("#stamp_fee").val(stamp_fee.toFixed(2));

	}else{
		$("#stamp_fee").val(0);
	}

}

function setInt(){
	int1 = parseFloat($("#fMintrate").val());
	
	if ( $("#is_pre_intst_chargeable").val() == 0 ){
		int2 = parseFloat($("#fMintrate2").val());
	}else{
		int2 = 0;
	}
	
	rmt  = parseFloat($("#requiredamount").val());
	
	//alert(  "fmint = ("+rmt+" * ("+int1+"+"+int2+") / 100)"  )
	
	fmint = (rmt * (int1+int2) / 100);
	
	$("#fmintrest").val(fmint.toFixed(2));
	int_normal = fmint.toFixed(2);
}


function setBillTypeValue(amount,goldvalue){

	showActionProgress("Please wait...");

	if ( $("#item_list_holder").html() == "" ) { 
		showMessage("e","No pawn items in the list"); 
		$("#cat_code_desc").focus(); 
		return false; 
	}

	ra_normal = amount;

	$.ajax({
		type: "POST",
		url: "index.php/main/load_data/t_opening_pawn/setBillTypeValue",
		data:{ amount : amount , goldvalue : goldvalue },
		dataType: "json",
		success: function (D) {
			closeActionProgress();

			if (D.s != 0){

				$("#fMintrate2").val(0);

				for (no = 0 ; no < D.bt_sum.length ; no++){

					$("#bill_code_desc").val(D.bt_sum[no].billtype);
					$("#billtype").val(D.bt_sum[no].billtype);
					$("#bt_letter").val(D.bt_sum[no].letter);				
					$("#billno").val(D.bt_max);				
					
					if ( no == 0 ){ $("#fMintrate").val(D.bt_sum[no].rate);	}
					if ( no == 1 ){ $("#fMintrate2").val(D.bt_sum[no].rate); }
					
					$("#period").val(D.bt_sum[no].period);
					$("#is_pre_intst_chargeable").val(D.bt_sum[no].weekly_cal);

				}


				if ( $("#is_pre_intst_chargeable").val() == 0 ){			
					$("#allow_full_month").prop("checked",true);			
					$("#fMintrate,#fMintrate2").css("border","2px solid Red");
				}else{				
					$("#allow_full_month").prop("checked",false);
					$("#fMintrate").css("border","2px solid Red");
					$("#fMintrate2").css("border","2px solid #ffffff");
				}			

				setInt();

			}else{

				showActionProgress("Bill type not found for this advance value. Please contact admin.");
				setTimeout("closeActionProgress()",5000);
			}

		}
	});


	// Use after bill type set
	//$("#requiredamount").css("border","2px solid #ffffff");
	//setPaybleAmounts();

}

function resetFormData(){

	var selected_date = $("#dDate").val();	
	var fMintrate = $("#fMintrate").val();
	var fMintrate2 = $("#fMintrate2").val();
	var period = $("#period").val();

	$("input:text").val("");

	$("#item_list_holder").html("");
	$("#is_pre_intst_chargeable").val(0);
	$("#loan_no,#requiredamount").attr("disabled",false);
	$("#btnAddtoList").val("Add");
	$("#btnSave").val("Save").attr("disabled",false).attr("class","btn_regular");
	$("#btnAddtoList").attr("disabled",false).attr("class","btn_regular");
	$("#btnDelete").val("Delete");
	$("#btnCancel").val("Cancel");
	$("#btnReset").val("Reset").attr("disabled",true).attr("class","btn_regular_disable");
	$("#btnCancel").attr("disabled",true).attr("class","btn_regular_disable");
	$("#btnPrint").val("Print");	
	$("#stamp_fee").val(0.00);
	$("#cus_info_div").html("");
	$("#billtype_info_div").html("");	
	$("#gold_qulty").val(100);
	$("#gold_qulty_rate").val(100);
	$("#by").val("pawn_ticket");	

	$("#dDate").val(selected_date);	

	$("#fMintrate").val(fMintrate);
	$("#fMintrate2").val(fMintrate2);
	$("#fMintrate,#fMintrate2").css("border","2px solid #ffffff");

	$("#period").val(period);

	$("#hid_trans_no").val("");
	$("#allow_cal_with_advance").attr({'checked':false,'disabled' : false});
	$(".canceled_bill_msg").css("visibility","visible");
	$(".canceled_bill_msg").animate({height: "0px"});
	$(".msg_txt").html("");
	//$("#bill_code_desc,#billtype").val("");
	$("#btnPrint").attr("disabled",true).attr("class","btn_regular_disable").attr("value","Print");
	$("#cus_serno").val("");
	$("#customer_id").focus();
}

function resetFrontCusInputs(){
	//$("#customer_id").val("");
	//$("#customer_no").val("");
	$("#hid").val("0");
	$("#nicno").val("");
	$('input:radio[name=title]').removeAttr("checked");
	$("#cusname").val("");
	$("#other_name").val("");
	$("#mobile").val("");
	$("#address").val("");
	$("#address2").val("");
	$("#telNo").val("");
}

function LOAD_LOAN(loan_no){

	tot_t_weigth = 0;
	tot_p_weigth = 0;
	tqty = 0;
	tot_val = 0;
	rl_no = 0;

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_opening_pawn/LOAD_LOAN",{
		loan_no : loan_no
	},function(D){

		closeActionProgress();

		if (D.s == 1){

			$("#r_no").val(D.loan_sum.loanno);
			$("#r_no_i").val(D.loan_sum.loanno);
			$("#hid_trans_no").val(D.loan_sum.transeno);

			$("#btnPrint").attr("disabled",false).attr("class","btn_regular").attr("value","Re-Print");
			$("#btnCancel").attr("disabled",false).attr("class","btn_regular");

			$("#loan_no").attr("disabled",true);
			$("#btnReset").attr("disabled",false).attr("class","btn_regular_reset");
			$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");

			var S = D.loan_sum.status;

			if (S == "C" || S == "R" || S == "L" || S == "F" || S == "PO" || S == "RN" ){
				$("#btnCancel").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnPrint,#btnAddtoList").attr("disabled",true).attr("class","btn_regular_disable");
				setBillStatusMSG(S);
			}

			$("#hid").val(D.loan_sum.loanno);
			$("#customer_id").val(D.loan_sum.customer_id); 
			getCustomerInfo(D.loan_sum.cus_serno);
			$("#bill_code_desc").val(D.loan_sum.billtype);
			$("#billtype").val(D.loan_sum.billtype);
			$("#bt_letter").val(D.loan_sum.bt_letter);

			$("#is_pre_intst_chargeable").val(D.loan_sum.allow_pre_int);
	
			if (D.loan_sum.int_with_amt == 1){
				$("#divchkh").html('<input type="checkbox" id="allow_cal_with_advance" name="allow_cal_with_advance" checked="checked" disabled="disabled">');
			}else{
				$("#divchkh").html('<input type="checkbox" id="allow_cal_with_advance" name="allow_cal_with_advance" disabled="disabled">');
			}

			$("#allow_cal_with_advance").attr('disabled', true);

			$("#billno").val(D.loan_sum.billno);
			$("#fMintrate").val(D.loan_sum.fmintrate);
			$("#fMintrate2").val(D.loan_sum.fmintrate2);
			$("#period").val(D.loan_sum.period);
			$("#dDate").val(D.loan_sum.ddate);

			$("#stamp_fee").val(D.loan_sum.stamp_fee);
			$("#requiredamount").attr("disabled",true).val(D.loan_sum.requiredamount);	
			$("#fmintrest").val(D.loan_sum.fmintrest);	

			var paybl_amt = parseFloat( D.loan_sum.requiredamount );

			$("#payable_amount").val( paybl_amt.toFixed(2) );

			// Details

			var T = var_bulk_items = "";

			for(num = 0 ; num < D.loan_det.length ; num++){							

				cat_code_desc = D.loan_det[num].cat_code + " - " + D.loan_det[num].des;
				cat_code = D.loan_det[num].cat_code;

				item_code_desc = D.loan_det[num].itemcode + " - " + D.loan_det[num].itemname;
				item_code = D.loan_det[num].itemcode;

				condition_desc = D.loan_det[num].con + " - " + D.loan_det[num].cdes;
				condition = D.loan_det[num].con;

				gold_type_desc = D.loan_det[num].id + " - " + D.loan_det[num].goldcatagory;
				goldcatagory = D.loan_det[num].id;
				gold_rate = D.loan_det[num].goldvalue;
				value = D.loan_det[num].value;

				gold_quality = D.loan_det[num].rate;
				gold_qulty_rate = D.loan_det[num].rate;				

				qty = D.loan_det[num].qty;
				t_weigth = D.loan_det[num].goldweight;
				p_weigth = D.loan_det[num].pure_weight;				

				tot_t_weigth += parseFloat(t_weigth);
				tot_p_weigth += parseFloat(p_weigth);
				tqty += parseFloat(qty);
				tot_val += parseFloat(value);
				rl_no += 1;

				if (D.loan_det[num].bulk_items != ""){
					var_bulk_items = D.loan_det[num].bulk_items;
				}else{
					var_bulk_items = "";
				}

				T += '<div style="border-bottom:1px dotted #ccc;height:47px" id="pn'+rl_no+'"><div class="text_box_holder_new_pawn" style="width:155px;float:left"><input type="text" class="input_text_regular_new_pawn catC" id="cat_code_desc" style="width:140px" readonly="readonly" value="'+cat_code_desc+'"><input type="hidden" name="A[]" value="'+cat_code+'" class="A1"></div><div class="text_box_holder_new_pawn" style="width:195px;float:left"><input class="input_text_regular_new_pawn itemC" type="text" id="condition_desc" style="width:180px" value="'+item_code_desc+'"><input type="hidden" name="B[]" value="'+item_code+'" class="B1"></div><div class="text_box_holder_new_pawn" style="width:150px;float:left"><input class="input_text_regular_new_pawn conC" type="text" id="condition_desc" style="width:137px" value="'+condition_desc+'"><input type="hidden" name="C[]" value="'+condition+'" class="C1"></div><div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn gtypeC" type="text" id="gold_type_desc" style="width:97px" value="'+gold_type_desc+'"><input type="hidden" name="D[]" value="'+goldcatagory+'" class="D1"><input type="hidden" name="J[]" value="'+gold_rate+'" class="J1"></div><div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn gquliC" type="text" id="gold_qlty" style="width:47px" value="'+gold_quality+'"><input type="hidden" name="I[]" value="'+gold_qulty_rate+'"  class="E1"></div><div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn qtyC" type="text" id="qty" style="width:47px" value="'+qty+'"><input type="hidden" name="E[]" value="'+qty+'"  class="F1"></div><div class="text_box_holder_new_pawn" style="width:76px;float:left"><input class="input_text_regular_new_pawn t_weigthC" type="text" id="t_weigth" style="width:63px" value="'+t_weigth+'"><input type="hidden" name="F[]" value="'+t_weigth+'"  class="G1"></div><div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn p_weigthC" type="text" id="p_weigth" style="width:60px" value="'+p_weigth+'"><input type="hidden" name="G[]" value="'+p_weigth+'"  class="H1"></div><div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn valueC" type="text" id="value" style="width:100px; text-align:right" value="'+value+'"><input type="hidden" name="H[]" value="'+value+'" class="I1"></div><div class="text_box_holder_new_pawn" style="width:50px;float:left"><a class="list-ur" id="ud_'+rl_no+'" onClick=updateRow("ud_'+rl_no+'")>Update</a> <a class="list-ur" id="rl_'+rl_no+'" onClick=removeRow("rl_'+rl_no+'")>Remove</a></div></div>';
			}
			
			$("#item_list_holder").html("").prepend(T);

			var_bulk_items != "" ? setBulkItemsShow(var_bulk_items) : var_bulk_items = '';
			
			$("#totalweight").val(tot_t_weigth.toFixed(3));
			$("#totalpweight").val(tot_p_weigth.toFixed(3));
			$("#tqty").val(tqty);
			$("#goldvalue").val(tot_val.toFixed(2));			

		}else{
			showMessage("e","Invalid loan number");
		}
		
		
	},"json");
}


function CANCEL_BILL(loan_no){

	if (confirm('Do you want cancel this bill?')){

		showActionProgress("Please wait...");

		$.post("index.php/main/load_data/t_opening_pawn/CANCEL_BILL",{
			loan_no : loan_no,
			transeno: $("#hid_trans_no").val(),
			cus_serno : $('#cus_serno').val()
		},function(D){
			closeActionProgress();

			if (D.s == 1){
				showMessage("s","Bill canceled");
				resetFormData();
			}else{
				showMessage("e","Unable to cancel this bill.");
			}

		},"json");

	}

}


function calValue(){

	var gold_rate 	= parseFloat($("#gold_rate").val());
	var gold_qulty_rate = parseFloat($("#gold_qulty_rate").val());
	var p_weigth  	= parseFloat($("#p_weigth").val());
		p_weigth 	= (p_weigth / 8);

	var value 		= (gold_rate * p_weigth);
		value 		= ((value * gold_qulty_rate) / 100);	

		if (isNaN(value)){ 
			$("#value").val("");
		}else{
			$("#value").val(value.toFixed(2));
		}	
}

function clearList(){
	$("#cat_code_desc").val("");
	$("#cat_code").val("");

	$("#item_code").val("");
	$("#item_code").val("");

	$("#condition_desc").val("");
	$("#condition").val("");

	$("#gold_type_desc").val("");
	$("#goldcatagory").val("");
	$("#gold_rate").val("");
	
	$("#gold_qulty,#gold_qulty_rate").val(100.00);
	

	$("#qty").val("");
	$("#t_weigth").val("");
	$("#p_weigth").val("");
	$("#value").val("");

	$("#gold_type_desc").focus();
}

var tot_t_weigth = 0;
var tot_p_weigth = 0;
var tqty = 0;
var tot_val = 0;
var rl_no = 0;
var divEditID = "";

function AddtoList(){

	cat_code_desc = $("#cat_code_desc").val();
	cat_code = $("#cat_code").val();

	if ($("#item_code :selected").text() == "Select Item"){
		item_code_desc = "";
	}else{
		item_code_desc = $("#item_code :selected").text();
	}

	item_code = $("#item_code").val();

	condition_desc = $("#condition_desc").val();
	condition = $("#condition").val();

	gold_type_desc = $("#gold_type_desc").val();
	goldcatagory = $("#goldcatagory").val();
	gold_rate = $("#gold_rate").val();

	gold_quality = $("#gold_qulty").val();
	gold_qulty_rate = $("#gold_qulty_rate").val();	

	qty = $("#qty").val();
	t_weigth = $("#t_weigth").val();
	p_weigth = $("#p_weigth").val();
	value = $("#value").val();

	//if ( rmsps(cat_code_desc) == "" || cat_code == "" ){$("#cat_code_desc").val("").focus();return;}
	//if (item_code == ""){$("#item_code").focus();return;}
	//if (rmsps(condition_desc) == "" || condition == ""){$("#condition_desc").val("").focus();return;}
	if (rmsps(gold_type_desc) == "" || goldcatagory == "" || gold_rate == ""){$("#gold_type_desc").val("").focus();	return;	}
	if (rmsps(gold_quality) == "" || gold_qulty_rate == "" ){$("#gold_qulty").val("").focus();return;}

	if (rmsps(qty) == ""){
		$("#qty").val("").focus().select();
		return;
	}else{
		if (!validateNumber($("#qty").val())) {
			$("#qty").val("").focus().select();
			return;	
		}
	}

	if (rmsps(t_weigth) == ""){
		$("#t_weigth").val("").focus().select();
		return;
	}else{
		if (!validateNumber($("#t_weigth").val())) {
			$("#t_weigth").val("").focus().select();
			return;	
		}
	}

	if (rmsps(p_weigth) == ""){
		$("#p_weigth").val("").focus().select();
		return;
	}else{
		if (!validateNumber($("#p_weigth").val())) {
			$("#p_weigth").val("").focus().select();
			return;	
		}
	}


	tot_t_weigth += parseFloat(t_weigth);
	tot_p_weigth += parseFloat(p_weigth);
	tqty += parseFloat(qty);
	tot_val += parseFloat(value);
	rl_no += 1;


	if ($("#btnAddtoList").attr("value") != "Update"){
		var T = '<div style="border-bottom:1px dotted #ccc;height:47px" id="pn'+rl_no+'"><div class="text_box_holder_new_pawn" style="width:155px;float:left"><input type="text" readonly="readonly" class="input_text_regular_new_pawn catC" id="cat_code_desc" style="width:140px" readonly="readonly" value="'+cat_code_desc+'"><input type="hidden" name="A[]" value="'+cat_code+'" class="A1"></div><div class="text_box_holder_new_pawn" style="width:195px;float:left"><input class="input_text_regular_new_pawn itemC" type="text"  readonly="readonly"  id="condition_desc" style="width:180px" value="'+item_code_desc+'"><input type="hidden" name="B[]" value="'+item_code+'" class="B1"></div><div class="text_box_holder_new_pawn" style="width:150px;float:left"><input class="input_text_regular_new_pawn conC" type="text" readonly="readonly"  id="condition_desc" style="width:137px" value="'+condition_desc+'"><input type="hidden" name="C[]" value="'+condition+'" class="C1"></div><div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn gtypeC" type="text" readonly="readonly"  id="gold_type_desc" style="width:97px" value="'+gold_type_desc+'"><input type="hidden" name="D[]" value="'+goldcatagory+'" class="D1"><input type="hidden" name="J[]" value="'+gold_rate+'" class="J1"></div><div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn gquliC" type="text" readonly="readonly"  id="gold_qlty" style="width:47px" value="'+gold_quality+'"><input type="hidden" name="I[]" value="'+gold_qulty_rate+'"  class="E1"></div><div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn qtyC" type="text" readonly="readonly"  id="qty" style="width:47px" value="'+qty+'"><input type="hidden" name="E[]" value="'+qty+'"  class="F1"></div><div class="text_box_holder_new_pawn" style="width:76px;float:left"><input class="input_text_regular_new_pawn t_weigthC" type="text" readonly="readonly"  id="t_weigth" style="width:63px" value="'+t_weigth+'"><input type="hidden" name="F[]" value="'+t_weigth+'"  class="G1"></div><div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn p_weigthC" type="text" readonly="readonly"  id="p_weigth" style="width:60px" value="'+p_weigth+'"><input type="hidden" name="G[]" value="'+p_weigth+'"  class="H1"></div><div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn valueC" type="text" readonly="readonly"  id="value" style="width:100px; text-align:right" value="'+value+'"><input type="hidden" name="H[]" value="'+value+'" class="I1"></div><div class="text_box_holder_new_pawn" style="width:50px;float:left"><a class="list-ur" id="ud_'+rl_no+'" onClick=updateRow("ud_'+rl_no+'")>Update</a> <a class="list-ur" id="rl_'+rl_no+'" onClick=removeRow("rl_'+rl_no+'")>Remove</a></div></div>';
		$("#item_list_holder").prepend(T);		
	}else{
		var T = '<div class="text_box_holder_new_pawn" style="width:155px;float:left"><input type="text" readonly="readonly" class="input_text_regular_new_pawn catC" id="cat_code_desc" style="width:140px" readonly="readonly" value="'+cat_code_desc+'"><input type="hidden" name="A[]" value="'+cat_code+'" class="A1"></div><div class="text_box_holder_new_pawn" style="width:195px;float:left"><input class="input_text_regular_new_pawn itemC" type="text"  readonly="readonly"  id="condition_desc" style="width:180px" value="'+item_code_desc+'"><input type="hidden" name="B[]" value="'+item_code+'" class="B1"></div><div class="text_box_holder_new_pawn" style="width:150px;float:left"><input class="input_text_regular_new_pawn conC" type="text" readonly="readonly"  id="condition_desc" style="width:137px" value="'+condition_desc+'"><input type="hidden" name="C[]" value="'+condition+'" class="C1"></div><div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn gtypeC" type="text" readonly="readonly"  id="gold_type_desc" style="width:97px" value="'+gold_type_desc+'"><input type="hidden" name="D[]" value="'+goldcatagory+'" class="D1"><input type="hidden" name="J[]" value="'+gold_rate+'" class="J1"></div><div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn gquliC" type="text" readonly="readonly"  id="gold_qlty" style="width:47px" value="'+gold_quality+'"><input type="hidden" name="I[]" value="'+gold_qulty_rate+'"  class="E1"></div><div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn qtyC" type="text" readonly="readonly"  id="qty" style="width:47px" value="'+qty+'"><input type="hidden" name="E[]" value="'+qty+'"  class="F1"></div><div class="text_box_holder_new_pawn" style="width:76px;float:left"><input class="input_text_regular_new_pawn t_weigthC" type="text" readonly="readonly"  id="t_weigth" style="width:63px" value="'+t_weigth+'"><input type="hidden" name="F[]" value="'+t_weigth+'"  class="G1"></div><div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn p_weigthC" type="text" readonly="readonly"  id="p_weigth" style="width:60px" value="'+p_weigth+'"><input type="hidden" name="G[]" value="'+p_weigth+'"  class="H1"></div><div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn valueC" type="text" readonly="readonly"  id="value" style="width:100px; text-align:right" value="'+value+'"><input type="hidden" name="H[]" value="'+value+'" class="I1"></div><div class="text_box_holder_new_pawn" style="width:50px;float:left"><a class="list-ur" id="ud_'+rl_no+'" onClick=updateRow("ud_'+rl_no+'")>Update</a> <a class="list-ur" id="rl_'+rl_no+'" onClick=removeRow("rl_'+rl_no+'")>Remove</a></div>';
		$("#"+divEditID).html(T);
		$("#btnAddtoList").attr("Value","Add");
	}

	CalTotal();
	divEditID = "";
	clearList();
	$("#requiredamount,#fmintrest,#stamp_fee,#payable_amount").val("");
	
}

function CalTotal(){

	var value = 0;
	var pweight = 0;
	var tWeigth = 0;
	var qty = 0;

	$(".I1").each(function(){ value += parseFloat( $(this).val() ); });
	$(".H1").each(function(){ pweight += parseFloat( $(this).val() ); });
	$(".G1").each(function(){ tWeigth += parseFloat( $(this).val() ); });
	$(".F1").each(function(){ qty += parseFloat( $(this).val() ); });

	$("#tqty").val(qty);
	$("#totalweight").val(tWeigth.toFixed(3));
	$("#totalpweight").val(pweight.toFixed(3));
	$("#goldvalue").val(value.toFixed(2));

}

function removeRow(row_id){		
	
	if (confirm("Do you want remove this item ? ")){
		$("#"+row_id).parent().parent().remove();
		CalTotal();
	}

}

function updateRow(row_id){	

	divEditID = $("#"+row_id).parent().parent().attr("id");	

	$("#btnAddtoList").attr("value","Update");

	$("#"+row_id).parent().parent().css("background-color","#f9f9f9");

	$("#cat_code_desc").val($("#"+row_id).parent().parent().find(".catC").parent().find("input").val());
	$("#cat_code").val($("#"+row_id).parent().parent().find(".catC").parent().find(".A1").val());

	//item_code_desc = $("#"+row_id).parent().parent().find(".itemC").parent().find("input").val();
	$("#item_code").val($("#"+row_id).parent().parent().find(".itemC").parent().find(".B1").val());
	$("#item_code").val($("#"+row_id).parent().parent().find(".itemC").parent().find(".B1").val());

	$("#condition_desc").val($("#"+row_id).parent().parent().find(".conC").parent().find("input").val());
	$("#condition").val($("#"+row_id).parent().parent().find(".conC").parent().find(".C1").val());

	$("#gold_type_desc").val($("#"+row_id).parent().parent().find(".gtypeC").parent().find("input").val());
	$("#goldcatagory").val($("#"+row_id).parent().parent().find(".gtypeC").parent().find(".D1").val());
	$("#gold_rate").val($("#"+row_id).parent().parent().find(".gtypeC").parent().find(".J1").val());

	$("#gold_qulty").val($("#"+row_id).parent().parent().find(".gquliC").parent().find("input").val());
	$("#gold_qulty_rate").val($("#"+row_id).parent().parent().find(".gquliC").parent().find(".E1").val());		

	$("#qty").val($("#"+row_id).parent().parent().find(".qtyC").parent().find(".F1").val());
	$("#t_weigth").val($("#"+row_id).parent().parent().find(".t_weigthC").parent().find(".G1").val());
	$("#p_weigth").val($("#"+row_id).parent().parent().find(".p_weigthC").parent().find(".H1").val());
	$("#value").val($("#"+row_id).parent().parent().find(".valueC").parent().find(".I").val());
	
	calValue();

}

function getItemsByCat(cat_code,is_bulk){

	if (is_bulk == 1){	
		
		$("#item_code").html("<option>Bulk Items</option>");
		$("#item_code").parent().append('<div class="div_bulk_items"><input type="text" id="bulk_items_add" class="input_text_regular_new_pawn" style="border:1px solid Green;width:410px">&nbsp;&nbsp; <a class="apply_bulk_items">Apply</a> &nbsp;&nbsp;&nbsp; <a class="clear_bulk_items">Clear</a></div>');
		$("#is_bulk").val(1);
	
	}else{

		$("#item_code").html("");
		$(".div_bulk_items").remove();
		$("#is_bulk").val(0);

		$.post("index.php/main/load_data/m_item/getItemsByCat",{
			cat_code : cat_code,
			is_bulk : is_bulk
		},function(D){
			$("#item_code").html(D);
			closeActionProgress();
		},"text");

	}

}

function getBillTypeInfo(bill_type_code){
	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_opening_pawn/getBillTypeInfo",{
		bill_type_code : bill_type_code
	},function(D){	
		
		if (D.s != 0){

				$("#fMintrate2").val(0);

				for (no = 0 ; no < D.bt_sum.length ; no++){

					$("#bill_code_desc").val(D.bt_sum[no].billtype + " - " + D.bt_sum[no].letter);
					$("#billtype").val(D.bt_sum[no].billtype);
					$("#bt_letter").val(D.bt_sum[no].letter);				
					$("#billno").val(D.bt_max);				
					
					if ( no == 0 ){ $("#fMintrate").val(D.bt_sum[no].rate);	}
					if ( no == 1 ){ $("#fMintrate2").val(D.bt_sum[no].rate); }
					
					$("#period").val(D.bt_sum[no].period);
					$("#is_pre_intst_chargeable").val(D.bt_sum[no].weekly_cal);

				}


				if ( $("#is_pre_intst_chargeable").val() == 0 ){			
					$("#allow_full_month").prop("checked",true);			
					$("#fMintrate,#fMintrate2").css("border","2px solid Red");
				}else{				
					$("#allow_full_month").prop("checked",false);
					$("#fMintrate").css("border","2px solid Red");
					$("#fMintrate2").css("border","2px solid #ffffff");
				}			


			}		

		closeActionProgress();

	},"json");
}

function getCustomerInfo(cus_serno,customer_id){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/t_opening_pawn/getCustomerInfo",{
		cus_serno : cus_serno,
		customer_id : customer_id		
	},function(D){	
		
		if (D.status == 1){
			$("#btnCancelFrontCusInput").click();
			hideCusUI();

			$("#cus_info_div").css({height: "42px"}).html(D.rec).animate({height: "335px"});
			
			$(".cph_p").html(D.cph.pawnings);
			$(".cph_f").html(D.cph.forfiedted);
			$(".cph_r").html(D.cph.redeemed);
			$(".cph_cn").html(D.cph.canceled);
			
			$("#pawn_info").css({height: "42px"}).animate({height: "200px"});

			if (D.is_blacklisted == 1){
				showActionProgress("");					
				showMessage("e","Blacklisted customer");
				return;
			}

			$("#cus_serno").val(D.serno);
			$("#customer_id").val(D.customer_id);
		}

		closeActionProgress();

		if (D.status == 0){
			if(confirm("Customer not found, do you want add as new customer")){
				showCusUI();
				$('#nicno').val($("#customer_id").val());
				$("#cusname").focus();
			}else{
				$("#btnCancelFrontCusInput").click();
				hideCusUI();
			}
		}


	},"json");

}

function showCusUI(){
	$("#item_list_holder").animate({height: "0px"});
	setTimeout('$("#add_new_cus_front_pawn").animate({height: "303px"})',500);
}

function hideCusUI(){
	$("#cus_serno,#customer_id").val("").focus();
	$("#add_new_cus_front_pawn").animate({height: "0px"});
	setTimeout('$("#item_list_holder").animate({height: "200px"})',500);
}

function saveAndHideCusUI(){
	//$("#bill_code_desc,#billtype").val("").focus();
	$("#add_new_cus_front_pawn").animate({height: "0px"});
	setTimeout('$("#item_list_holder").animate({height: "200px"})',500);
}

function save(){

	if (validate_form()){		

		var goldvalue = isNaN(parseFloat($("#goldvalue").val())) ? 0 : parseFloat($("#goldvalue").val()); 
		var requiredamount = isNaN(parseFloat($("#requiredamount").val())) ? 0 : parseFloat($("#requiredamount").val());
		var app_amount = isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val()); 
		
		showActionProgress("Saving...");			

		/*if (requiredamount > goldvalue){				
			$("#requiredamount").val(  goldvalue + app_amount );
		}*/

		var frm = $('#form_'); 

		$.ajax({
			type: frm.attr('method'), 
			url: frm.attr('action'), 
			data: frm.serialize(), 
			dataType: "json", 
			success: function (D) {
				closeActionProgress(); 
				
				if (D.s == 1){
					showMessage("s","New pawn saved success"); 
					$("#r_no").val(D.loan_no); 
					$("#r_no_i").val(D.loan_no); 
					$("#print_pdf").submit(); 

					/*if (D.with_int == 1){ // This code commented due to pawn ticket and interest receipt view on the same PDF
						$("#first_int_print_pdf").submit(); 
					}*/

					resetFormData(); 
				}else if(D.s == 2){												
					setBillStatusMSG("EX");						
				}else{
					showMessage("e","Error, save failed"); 
				} 
			} 
		});


		//if ( requiredamount < goldvalue || $("#is_discount_approved").val() == 1 ){		
			// saving part must here, if aaproval part required
		//}else{
			//if (confirm("Extra advance amount approval required.\n\nDo you want send this loan to approval?")){showActionProgress("Sending approval request..."); var appid = "L"+Math.floor(Math.random() * 5345) + 3453 ; $("#happid").val(appid); $.post("index.php/main/load_data/approvals/add_approval",{loanno 			: appid, int_balance  	: 0, requested_amount: (requiredamount - goldvalue) , payable_amount 	: $("#payable_amount").val(), request_type 	: "EL", goldvalue		: goldvalue },function(D){closeActionProgress(); if (D.s == 1){$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable"); showApprovalWatingMsgBox(""); counter = setInterval(timer, 1000); /*1000 will  run it every 1 second*/ enableApprovalMonitor(appid); /* set Request approval monitor */ }else{showMessage("s", D.m ); } },"json"); }
		//}

	}

}

function setBillStatusMSG(S){	

	var t = "";

	if (S == "C"){t = "This bill was canceled"; }
	if (S == "R"){t = "This bill was redeemed"; }
	if (S == "L"){t = "This bill was marked as lost"; }
	if (S == "F"){t = "This bill was forfieted"; }
	if (S == "PO"){t = "This bill was marked as police bill"; }
	if (S == "RN"){t = "This bill was renewed"; }
	if (S == "EX"){t = "This bill number already exist"; }

	$(".canceled_bill_msg").css("visibility","visible");
	$(".canceled_bill_msg").animate({height: "40px"});	
	$(".msg_txt").html(t);
}


function saveFront(){	

	showActionProgress("Saving...");

	if (validate_form_front()){
		
		$.post("index.php/main/save/m_customer",{

			customer_id :	$("#customer_idF").val(),
			cus_serno :	$("#cus_sernoF").val(),
			hid 		: $("#hid").val(),
			nicno :	$("#nicno").val(),			
			title :	$('input:radio[name=title]:checked').val(),
			cusname :	$("#cusname").val(),
			other_name :	$("#other_name").val(),
			mobile :	$("#mobile").val(),
			address :	$("#address").val(),
			address2 :	$("#address2").val(),
			telNo :	$("#telNo").val()

		},function(D){		

			if (D.s == 1){
				closeActionProgress();
				showMessage("s","Customer saving success");
				$("#customer_id").val(D.customer_id);
				$("#cus_serno").val(D.cus_serno);
				resetFrontCusInputs();
				saveAndHideCusUI();
				getCustomerInfo($("#cus_serno").val() , $("#customer_id").val() );
				$("#bill_code_desc").focus();
			}else{
				showMessage("e",D.m);
			}


		},"json");

	}

}



function setDelete(recid){

	if (confirm("Do you want delete this doc fee?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/t_opening_pawn",{
			recid : recid
		},function(D){		
			$(".list_div").html(D);
			closeActionProgress();
			load_list();
		},"text");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/t_opening_pawn",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function validate_form(){

	if ( rmsps($("#dDate").val()) ==  "" ){ showMessage("e","Select receipt date"); $("#dDate").val("").focus(); return false; }
	if ( rmsps($("#customer_id").val()) ==  "" || $("#cus_serno").val() == "" ){ showMessage("e","Select customer"); $("#customer_id").val("").focus(); return false; }
	if ( rmsps($("#bill_code_desc").val()) ==  "" || $("#billtype").val() == "" ){ showMessage("e","Enter required amount and hit the enter key to load bill type"); $("#bill_code_desc").focus(); return false; }
	if ( rmsps($("#billno").val()) ==  "" || !validateNumber($("#billno").val()) ){ showMessage("e","Invalid bill nuner"); $("#billno").val("").focus(); return false; }
	if ( rmsps($("#fMintrate").val()) ==  "" ){ showMessage("e","Enter first 7 days interest"); $("#fMintrate").val("").focus(); return false; }
	if ( rmsps($("#fMintrate2").val()) == "" ){ showMessage("e","Enter monthly interest"); $("#fMintrate2").val("").focus(); return false; }
	if ( rmsps($("#period").val()) ==  "" ){ showMessage("e","Enter period "); $("#period").val("").focus(); return false; }
	
	if ( $("#item_list_holder").html() == "" ) { 
		showMessage("e","No pawn items in the list"); 
		$("#btnAddtoList").click();
		return false; 
	}
	
	if ( rmsps( $("#requiredamount").val() ) == "" ){ 
		showMessage("e","Enter required amount"); $("#requiredamount").focus(); 
		return false; 
	}else{
		if (!validateAmount($("#requiredamount").val())){
			showMessage("e","Invalid required amount value"); 
			$("#requiredamount").focus().select(); 
			return false;
		}
	}

	if ( rmsps($("#fmintrest").val()) ==  "" ){ showMessage("e","Enter interest amount"); $("#fmintrest").val("").focus(); return false; }
	if ( rmsps($("#stamp_fee").val()) ==  "" ){ showMessage("e","Enter stamp fee amount"); $("#stamp_fee").val("").focus(); return false; }
	
	return true;	
}

function validate_form_front(){

	//if ( rmsps($("#customer_idF").val()) ==  "" || $("#cus_sernoF").val() == "" ){ showMessage("e","Select customer"); $("#customer_idF").val("").focus(); return false; }
	
	if (!$('input:radio[name=title]').is(":checked")){ showMessage("e","Select title"); $("#SP454").focus(); return false;}

	gm = $('input:radio[name=title]:checked').val();
	if (gm == "Mr. "){ gm = "m"; }else{ gm = "f"; }
	


	if ($("#customer_id_type :selected").val() == "nic"){		
		if (!validateNIC($("#nicno").val(),gm)){			
			$("#nicno").focus().select();
			return false;
		}
	}

	if ($("#customer_id_type :selected").val() == "pp"){		
		if (!validatePassport($("#nicno").val())){
			$("#nicno").focus().select();
			return false;
		}
	}

	if ($("#customer_id_type :selected").val() == "dl"){		
		if (!validateDrvingLicense($("#nicno").val())){
			$("#nicno").focus().select();
			return false;
		}
	}

	if ($("#customer_id_type :selected").val() == "gm"){		
		if (!validateGSCertifi($("#nicno").val())){
			$("#nicno").focus().select();
			return false;
		}
	}






	

	











	if ( rmsps($("#cusname").val()) ==  ""){ showMessage("e","Enter customer full name"); $("#cusname").val("").focus(); return false; }
	
	if ($("#mobile").val() != ""){
		if (!validateMobileno(  $("#mobile").val())){ $("#mobile").focus(); return false;}
	}
	
	if ( rmsps($("#address").val()) ==  ""){ showMessage("e","Enter customer address"); $("#address").val("").focus(); return false; }	
	
	return true;	
}

function setEdit(recid){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_opening_pawn/set_edit",{
		recid : recid
	},function(D){		
		$("#hid").val(D.recid);
		$("#btnSave").val("Edit");
		
		$("#bc").val(D.bc);
		$("#fromvalue").val(D.fromvalue);
		$("#tovalue").val(D.tovalue);
		$("#stampfee").val(D.stampfee);
		$("#stampdate").val(D.stampdate);		
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/t_opening_pawn/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}