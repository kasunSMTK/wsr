

$(document).on("change",".v_bc",function(){

    get_users_by_bc( $(".v_bc :selected").val() );

});


$(document).on("keypress","#cus_search",function(){
    $( "#cus_search" ).autocomplete({
        source: "index.php/main/load_data/t_user_activity/get_data",
        select: function (event, ui) {                  
            var d = ui.item.value;
            d = d.split(" - ");      

            $("#oc").val(d[0]);

            $("#fd").focus();

        },
        delay : 1000
    });
});



$(document).ready(function(){


    $(".drop_voucher_option,#bc,#useer").change(function(){

        $(".vi").html('');

    });


    $("#cus_search").click(function(){

        $("#cus_search").val("");
        $("#oc").val("");

    });


    $("#btnShow").click(function(){

        

        if ($("#bc :selected").val() == ''){
            alert("Please select branch");
            return;
        }


        showActionProgress("Please wait...");        

        var frm = $('#form_'); 

        $.ajax({
            type: frm.attr('method'), 
            url: frm.attr('action'), 
            data: frm.serialize(), 
            dataType: "json", 
            success: function (D) {
                
                closeActionProgress();


                $(".vi").html(D.d);
                
                 
            } 
        });

    });

});

function get_users_by_bc(bc){

    showActionProgress("Please wait...");

    $.post("index.php/main/load_data/t_user_activity/get_users_by_bc",{
        bc : bc
    },function(D){      
        closeActionProgress();

        if (D.s == 1){
            
            $("#useer").html(D.d);

        }else{
            
        }
    
    },"json");

}

