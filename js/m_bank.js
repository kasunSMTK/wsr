var is_edit=0;
$(document).ready(function(){
    load_list();

    $("#btnSave").click(function(){     
        save();
    }); 
    $("#code").blur(function(){
        check_code();
    });

    $("#btnSearch").click(function(){
        search($("#Search_bankname").val());
    });

    

});


function save(){    

    showActionProgress("Saving...");

    if (validate_form()){
        var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: "json",
            success: function (D) {

                closeActionProgress();

                if (D == 1){
                    showMessage("s","Bank saving success");
                    clear_form();
                    load_list();                    
                }else if (D == 2){                  
                    showMessage("e","Error, Duplicate entry");
                }else{                  
                    showMessage("e","Error, save failed");
                }
            }
        });

    }

}


function load_list(){
    showActionProgress("Loading...");
    $.post("index.php/main/load/m_bank",{
        s : "load_list"
    },function(D){      
        $(".list_div").html(D.T);
        $("#bc").val(D.max_no);
        closeActionProgress();
    },"json");
}


function setEdit(code){

    showActionProgress("Loading...");

    $.post("index.php/main/load_data/m_bank/set_edit",{
        code : code
    },function(D){      
        $("#hid").val(D.code);
        $("#btnSave").val("Edit");
        $("#code").val(D.code);     
        $("#description").val(D.description);       
        closeActionProgress();
    },"json");  

}


function check_code(){
    var code = $("#code").val();
    $.post("index.php/main/load_data/m_bank/check_code", {
        code : code
    }, function(res){
        if(res == 1){

            if(confirm("This code ("+code+") already added. \n\n Do you need edit it?")){
                $("#code").attr("readonly","readonly"); 
                 setEdit(code);

            }else{
                $("#code").val('');
            }
        }
        else if(res == 0){
            save();
        }
    }, "text");
}



function validate_form(){
    if ($("#code").val() == ""){showMessage("e","Enter Bank code");    return false; }
    if ($("#description").val() == ""){showMessage("e","Enter Bank name");  return false; }
    return true;    
}

function clear_form(){
    $("input").val("");
    $("#btnSave").val("Save");  
    $('#btnSearch').val("Search");
    $("#hid").val(0);
}

function setDelete(code){

    if (confirm("Do you want delete this Bank?")){
        showActionProgress("Deleting...");
        $.post("index.php/main/delete/m_bank",{
            code : code
        },function(D){
            $(".list_div").html(D);
            closeActionProgress();
            load_list();
        },"text");
    }
}
function set_edit(code){
    
    loding();
    $.post("index.php/main/get_data/m_bank", {
        code : code
    }, function(res){
        alert(res);
        $("#code_").val(res.code);
        $("#code").val(res.code);
        $("#description").val(res.description);
        $("#code").attr("readonly","readonly");
        loding(); 
        input_active();
        is_edit=1;
    }, "json");
}

function search(skey){
    showActionProgress("Searching...");
    $.post("index.php/main/load_data/m_bank/search",{
        skey : skey
    },function(D){      
        $(".list_div").html(D);
        closeActionProgress();
    },"text");
}

