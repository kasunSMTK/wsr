var defualt_tr_no = "";
var bcCustomer = [];
var customer_bill_arr = [];


function set_values(v){
	
	x = v.split(" ........ ");
	$("#loanno").val(x[3]);
	$("#billno").val(x[0]);

	$("#cr_amount").focus();

	$('#selected_bill_amount').val(x[2]);

	// set_int_bal(x[3]);
	set_bill_adv_total();

}


$(document).on("change",".custom-combobox input",function(){ 
	set_values($(this).val());
});

$(document).on("blur",".custom-combobox input",function(){
	set_values($(this).val());
});

$(document).on("change","#billno",function(){
	var bo = $("#billno :selected").attr("billtype");		
	$("#billtype_opt").val(bo);		
	$("#billtype_selected").val($("#billno :selected").attr("bill_ln"));
});

$(document).on("click",".ui-menu-item",function(){
	var x = $(this).html();
		x = x.split(" ........ ");
		$("#loanno").val(x[3]);
});


$(document).on("click",".adv_remove_link",function(){
	
	if (confirm("Do you want remove this advance payment?")){
		var bc 		= $(this).attr("bc");
		var id 		= $(this).attr("id");
		var billno 	= $(this).attr("billno");
		remove_paid_advance(bc,id,billno);
	}

});

$(document).ready(function(){

	$("#cus_adv_id").focus();

	$("#btnSave").click(function(){
		save();
	});	

	$("#btnReset").click(function(){
		resetForma();
	});

	$( "#cus_bill_auto" ).autocomplete({
		source: customer_bill_arr,
		select: function (event, ui) {					
			var bt = ui.item.value;
			bt = bt.split(" - ");
			$("#client_id").val(bt[0]);
			getCusInfo(bt[0] );
			$("#cr_amount").focus();
		},
		delay : 500
	});	


	$("#btnPrint").click(function(){
		$("#print_pdf").submit();
	});	
	

	$("#cus_adv_id").keypress(function(e){		
		if (e.keyCode == 8 || e.keyCode == 46 ){$("#client_id").val(""); }
		$( "#cus_adv_id" ).autocomplete({
			source: "index.php/main/load_data/t_new_pawn/getBcCustomer",
			select: function (event, ui) {					
				var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#client_id").val(bt[0]);
				getCusInfo(bt[0] );
				$("#cr_amount").focus();
			},
			delay : 550
		});		
	});	

	$("#no").keypress(function(e){
		if (e.keyCode == 13){
			load_data();
		}
	});



});

function getCusInfo(customer_id){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/t_customer_advance_payment/cus_info_and_det",{		
		customer_id : customer_id		
	},function(D){	
		
		if (D.status == 1){			
			$("#cus_info_div").css({height: "42px"}).html(D.rec).animate({height: "398px"});
			$("#cus_serno").val(D.serno);
			$("#customer_id").val(D.customer_id);			
			$("#cat_code_desc").focus();

			//$("#loanno").val(D.cus_bill_det[0].loanno);			

			advance_paid_list(D);
			$("#btnReset").attr("class","btn_regular");
		}

		closeActionProgress();

	},"json");

}

function save(){
	showActionProgress("Saving...");

	if (validate_form()){
		
		var f = $("#form_");

		$.ajax({			
			type: f.attr('method'),
			url: f.attr('action'),
			data: f.serialize(),
			dataType: "json",
			success: function (D) {
				

				closeActionProgress();

				if(D.s == 33){
					alert(D.msg_33);
					location.href = '';
				}
				
				if (D.action == "advance_dr"){
					showMessage("s","Advance payment sucess");
					current_date = $("#ddate").val();
					var bc_no = $("#bc_no").val();				
					resetForma();
					$("#no").val(D.max_no);
					$("#billno").focus();				
					$("#r_no").val(D.no + "~~~" + D.loanno + "~~~from_master_table");				
					$("#by").val("advance_payment_ticket");				
					$("#print_pdf").submit();				
					$("#ddate").val(current_date);					
					defualt_tr_no = D.max_no;

					$(".ui-autocomplete").html("");
					$(".adv_pay_history").html("");
					$(".custom-combobox-input").val(""); // ui-widget ui-widget-content ui-state-default ui-corner-left ui-autocomplete-input
				}						
				

			}
		});

	}
}

function load_data(){

	showActionProgress("Loading...");

	$.post("index.php/main/get_data/t_customer_advance_payment",{
		no : $("#no").val()
	},function(D){
		
		closeActionProgress();

		if (D.s == 0){
			alert("No data found");
			return;
		}
		
		$("#cus_adv_id").val(D.sum.nicno + " - " + D.sum.cusname);
		$("#client_id").val(D.sum.client_id);
		$("#cr_amount").val(D.sum.cr_amount);
		$("#adv_bal").val("Adv bal here");
		
		getCusInfo(D.sum.client_id);
		
		//advance_paid_list(D);

		$("#btnSave").attr({class:"btn_regular_disable", disabled : true });
		$("#btnReset,#btnPrint").attr({class:"btn_regular", disabled : false });

		$("#r_no").val(D.sum.trans_no + "~~~" + D.sum.loanno + "~~~from_master_table");
		$("#by").val("advance_payment_ticket");
		

	},"json");

}

function resetForma(){
	var d = $("#date").val();
	$("input[type=text],[type=hidden]").val("");
	$("#date").val(d);
	$(".adv_pay_history").html("Select customer to view advance payment history");
	$("#no").val(defualt_tr_no);
	$("#cus_adv_id").focus();
	$("#btnReset,#btnPrint").attr({class:"btn_regular_disable", disabled : true });
	$("#cus_info_div").html("").animate({height:0});
	$(".cus_bills").html("");
}

function validate_form(){

	if ( $("#client_id").val() == "" || $("#cus_adv_id").val() == "" ){
		showMessage("e","Please select customer"); 
		$("#cus_adv_id,#client_id").val(""); 
		$("#cus_adv_id").focus(); 
		return; 
	}

	if ($("#billno :selected").val() == '' || $("#billno :selected").val() == undefined){
		showMessage("e","Please select bill number"); 		
		$("#billno").focus(); 
		return;
	}

	if (!validateAmount($("#cr_amount").val() ," paying amount"		)){ 
		$("#cr_amount").focus(); 
		return;
	}

	if ( parseFloat( $("#cr_amount").val() ) + parseFloat( $("#bill_adv_total").val() ) > parseFloat( $("#selected_bill_amount").val() ) ){

		showMessage("e","Paying amount ("+$("#cr_amount").val()+") + bill advance total ("+$("#bill_adv_total").val()+") cant be grater than pawning amount ("+$("#selected_bill_amount").val()+") "); 		
		return;
	}

	return true;
}

function advance_paid_list(D){

	var cus_bills_dropdown = ""; // "<select style='width:300px;border:1px solid #000000' name='billno' id='billno'>";

	if (D.cus_bill_det.length > 0){
		cus_bills_dropdown += "<option value=''>"+D.cus_bill_det.length+" Bill(s) found for this customer</option>";
		for(nn = 0 ; nn < D.cus_bill_det.length; nn++){
			cus_bills_dropdown += "<option bill_ln='"+D.cus_bill_det[nn].loanno+"' value='"+D.cus_bill_det[nn].billno+"' billtype='"+D.cus_bill_det[nn].billtype+"'>"+D.cus_bill_det[nn].billno+" ........ "+D.cus_bill_det[nn].ddate+" ........ "+D.cus_bill_det[nn].requiredamount+" ........ "+D.cus_bill_det[nn].loanno+" </option>";
			customer_bill_arr[nn] = D.cus_bill_det[nn].billno;
		}

	}else{
		cus_bills_dropdown += "<option>No bill(s) found for this customer</option>";
	}

	// cus_bills_dropdown += "</select>";

	

	$(".cus_bills").html(cus_bills_dropdown);


	if (D.cus_his != 0){

		var tot_bal = 0;
		var T = "<table class='tblAdvList' style='width:100%'>";

			T += "<tr>";
			T += "<td style='width:20px'>No</td>";
			T += "<td style='width:15px'>Date</td>";
			T += "<td>Description</td>";
			T += "<td align='right'>Amount</td>";
			T += "<td align='right'>Redeemed Adv. Amount</td>";
			T += "<td align='right'></td>";
			if (D.remove_allowed != 0){ T += "<td align='right'>Action</td>"; }
			
			T += "</tr>";

		for(n = 0 ; n < D.cus_his.length ; n++){

			if (D.cus_his[n].is_delete == 0){

				T += "<tr>";
				T += "<td style='width:20px'>"+D.cus_his[n].trans_no+"</td>";
				T += "<td style='width:15px'>"+D.cus_his[n].date+"</td>";
				T += "<td>"+D.cus_his[n].billno+ " " +D.cus_his[n].note+"</td>";
				T += "<td align='right'>"+D.cus_his[n].cr_amount+"</td>";
				T += "<td align='right'>"+D.cus_his[n].dr_amount+"</td>";
				
				tot_bal += parseFloat(D.cus_his[n].cr_amount);
				tot_bal -= parseFloat(D.cus_his[n].dr_amount);

				T += "<td align='right'></td>";

				if (D.remove_allowed != 0){ T += "<td align='right'><a class='adv_remove_link' bc='"+D.cus_his[n].bc+"' billno='"+D.cus_his[n].billno+"' id='"+D.cus_his[n].trans_no+"'>Remove</a></td>";}

				T += "</tr>";

			}else{
				T += "<tr class='adv_canceled_tr'>";
				T += "<td style='width:20px'>"+D.cus_his[n].trans_no+"</td>";
				T += "<td style='width:15px'>"+D.cus_his[n].date+"</td>";
				T += "<td>"+D.cus_his[n].billno+ " " +D.cus_his[n].note+"</td>";
				T += "<td align='right'>"+D.cus_his[n].cr_amount+"</td>";
				T += "<td align='right'>"+D.cus_his[n].dr_amount+"</td>";
				T += "<td align='right'>"+tot_bal.toFixed(2)+"</td>";
				T += "<td align='right'>Cancelled</td>";
				T += "</tr>";
			}


		}

		T += "</table>";


		
		$("#adv_bal").val(tot_bal.toFixed(2));
		$(".adv_pay_history").html(T);

	}else{
		$("#adv_bal").val(0.00);
		$(".adv_pay_history").html("Advance payments not found");						
		$(".ui-autocomplete").html("");
		$(".adv_pay_history").html("");
	}


}

function remove_paid_advance(bc,id,billno){

	showActionProgress("Please wait...");

	$.post("index.php/main/load_data/t_customer_advance_payment/remove_paid_advance",{
		bc:bc,
		id : id,
		billno : billno
	},function(D){
		closeActionProgress();		

		getCusInfo($("#client_id").val());

	},"json");

}


function set_int_bal(loanno){
	
	if (loanno == undefined || loanno == ""){
		return;
	}
	
	showActionProgress("Calculating interest...");

	$.post("index.php/main/load_data/t_customer_advance_payment/set_int_bal",{		
		loanno : loanno
	},function(D){
		closeActionProgress();

		$("#payable_int").val(D.int_bal);

	},"json");

}

function set_bill_adv_total(){

	showActionProgress("Calculating advance total...");

	$.post("index.php/main/load_data/t_customer_advance_payment/set_bill_adv_total",{		
		billno : $("#billno").val()
	},function(D){
		
		closeActionProgress();

		$("#bill_adv_total").val(D.bill_adv_bal);

	},"json");

}