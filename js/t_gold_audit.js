
var glob_expanded_row = '';

$(document).on("click",".chk_tfr_res",function(){

	if ($(this).is(":checked")){
		$(this).parent().removeClass("div_tfr_res").addClass("div_tfr_res_clicked");
	}else{
		$(this).parent().removeClass("div_tfr_res_clicked").addClass("div_tfr_res");
	}

});

$(document).on("click",".btnItemCommentSave",function(){

	var gw  = Array();
	var pw  = Array();
	var gt  = Array();
	var oa  = Array();
	var an  = Array();

	var ac = Array();
	var ic = Array();
	var n = nn = nnn = nnnn = nnnnn = nnnnnn = 0;

	$(this).parent().parent().parent().find('.gw').each(function(){
		gw[nn] = $(this).val();		
		nn++;		
	});

	$(this).parent().parent().parent().find('.pw').each(function(){
		pw[nnn] = $(this).val();		
		nnn++;		
	});

	$(this).parent().parent().parent().find('.gt').each(function(){
		gt[nnnn] = $(this).val();		
		nnnn++;		
	});

	$(this).parent().parent().parent().find('.oa').each(function(){
		oa[nnnnn] = $(this).val();		
		nnnnn++;		
	});

	$(this).parent().parent().parent().find('.article_comment').each(function(){
		ac[n] = $(this).val();
		ic[n] = $(this).attr('ic');
		n++;		
	});

	$(this).parent().parent().parent().find('.hid_nno').each(function(){
		an[nnnnnn] = $(this).val();		
		nnnnnn++;		
	});

	


	

	save(gw,pw,gt,oa,ac,ic,an,$(this).attr('row_no'),$(this));

});


$(document).on("click",".tbl_ga_tr_bn",function(){

	var billno = $(this).attr('bn');

	if (glob_expanded_row == billno ){
		return;
	}

	glob_expanded_row = billno;

	$(".bn_sum_comment").css("display","none");

	$(".selected_tr_top").css("border","0px solid green");
	$(".selected_tr_bottom").css({
		"border-bottom":"2px solid green",
		"border-left":"2px solid green",
		"border-right":"2px solid green"
	});

	$(".all_row").css("display","none");

	$(".tbl_ga_tr_bn").css({"background-color":"#ffffff","color":"#000000","font-size":"13px","font-family":"roboto"});

	$(this).css({"background-color":"Green","color":"#ffffff","font-size":"19px","font-family":"bitter"});

	//$(".bn_sum_comment").val("").css("display","none").removeAttr("id");

	$(this).find(".bn_sum_comment").css("display","table-row");


	$(this).find(".bn_sum_comment").parent().parent().css({
				"border-top":"2px solid green",
				"border-left":"2px solid green",
				"border-right":"2px solid green" });

	$("."+$(this).attr("cl")).css("display","table-row") ;

	$(".ga_bill_det_show").html("<td colspan='6'><div style='text-align:center;width:100%'><div><img src='img/loading.gif'></div>Loading bill details...</div></td>");


	$.post('index.php/main/load_data/t_gold_audit/get_bill_data',{
        billno: billno,
        row_no : $(this).attr("row_no"),
        tfr_res : $(this).attr("tfr_res")
    }, function(D) {

    	$(".ga_bill_det_show").html("");
	
    	var t = '<form method="post" action="index.php/main/save/t_gold_audit" id="form_">';

		t += '<table width="100%">';

		t += '<tr>';			
		t += '<td>Item</td>';
		t += '<td>Category</td>';
		t += '<td>Condition</td>';
		t += '<td align="center">Quality</td>';
		t += '<td align="right">Gold Value</td>';
		t += '<td align="right">Value</td>';			
		t += '<td>Bulk Items</td>';
		t += '<td align="center">Denci Weight</td>';
		t += '<td align="center">Qty</td>';
		
		
		t += '<td align="center">Gold Weight</td>';
		t += '<td align="center" width="120">Audit Gold Weight</td>';

		
		t += '<td align="center">Pure Weight</td>';
		t += '<td align="center" width="120">Audit Pure Weight</td>';
		
		
		t += '<td align="center">Gold Type</td>';
		t += '<td align="center" width="90">Audit Gold Type</td>';


		t += '<td align="right" width="160">Over Advance</td>';

		t += '<td>Audit Comment</td>';
		t += '</tr>';	


		var gw = pw = gv = v = 0;


		for(n = 0 ; n < D.q.length ; n++ ){

			t += '<tr class="tbl_audit_item_row">';							
			t += '<td>'+D.q[n].itemname+'</td>';
			t += '<td>'+D.q[n].cat_code+'</td>';
			t += '<td>'+D.q[n].con+'</td>';
			t += '<td align="center">'+D.q[n].quality+'</td>';
			t += '<td align="right">'+D.q[n].goldvalue+'</td>'; gv += parseFloat(D.q[n].goldvalue);
			t += '<td align="right">'+D.q[n].value+'</td>'; v += parseFloat(D.q[n].value);
			t += '<td>'+D.q[n].bulk_items+'</td>';
			t += '<td align="center">'+D.q[n].denci_weight+'</td>';
			t += '<td align="center">'+D.q[n].qty+'</td>';

			
			t += '<td align="center">'+D.q[n].goldweight+'</td>'; gw += parseFloat(D.q[n].goldweight);
			t += '<td align="center"><input type="text" value="'+D.q[n].audit_goldweight+'" class="bn_sum_comment gw" ic="'+D.q[n].itemcode+'" style="text-align:center;width:100%"></td>'; 

			
			t += '<td align="center">'+D.q[n].pure_weight+'</td>'; pw += parseFloat(D.q[n].pure_weight);			
			t += '<td align="center"><input type="text" value="'+D.q[n].audit_pure_weight+'" class="bn_sum_comment pw" style="text-align:center;width:100%"></td>'; 


			t += '<td align="center">'+D.q[n].goldtype+'</td>';
			t += '<td align="center"><input type="text" value="'+D.q[n].audit_goldtype+'" class="bn_sum_comment gt" ic="'+D.q[n].itemcode+'" style="text-align:center;width:100%"></td>'; 


			t += '<td align="center"><input type="text" value="'+D.q[n].audit_overadvance+'" class="bn_sum_comment oa" ic="'+D.q[n].itemcode+'" style="text-align:right;width:100%"></td>'; 

			t += '<td width="400"><input type="text" value="'+D.q[n].audit_comment+'" class="bn_sum_comment article_comment" ic="'+D.q[n].itemcode+'" style="width:100%">';

			t += '<input type="hidden" class="hid_nno" value="'+D.q[n].nno+'"></td>';
			


			t += '</tr>';

		}

			t += '<tr bgColor="#cccccc">';			
			
			t += '<td></td>';
			t += '<td></td>';
			t += '<td>Total</td>';
			

			t += '<td></td>';
			t += '<td>'+gv.toFixed(2)+'</td>';
			t += '<td>'+v.toFixed(2)+'</td>';				
			
			t += '<td></td>';
			t += '<td></td>';

			t += '<td></td>';
			

			t += '<td align="center">'+gw.toFixed(3)+'</td>';
			t += '<td></td>';
			
			t += '<td align="center">'+pw.toFixed(3)+'</td>';
			
			t += '<td></td>';
			t += '<td></td>';					

			if (D.tfr_res == 1){
				t += '<td colspan="2" align="right"><label class="div_tfr_res_clicked">	<input type="checkbox" class="chk_tfr_res" name="chk_tfr_res" checked=checked> Restrict for TFR</label></td>';
			}else{				
				t += '<td colspan="2" align="right"><label class="div_tfr_res">			<input type="checkbox" class="chk_tfr_res" name="chk_tfr_res"> Restrict for TFR</label></td>';
			}			

			t += '<td><input type="button" billno="'+billno+'" row_no="'+D.row_no+'" value="Update" class="btn_regular btnItemCommentSave"><div class="audit_msg"></div></td>';
			
			
			t += '</tr>';



		t += '</table>';
		t += '<input type="hidden" value="'+billno+'" name="billno">';
		t += '</form>';


		$(".ga_bill_det_show").html("<td colspan='6' style='padding:0px'>"+t+"</td>");


    },"json");


});


$(document).ready(function(){

	$("#btnGetBills").click(function(){
		
		if ( $("#bc :selected").val() == "" ){
			alert("Please select branch");
			return;
		}

		get_bills();

	});

	/*$(".tbl_ga_tr_bn").mouseover(function(){		
		$(this).css({"background-color":"green","color":"#ffffff","font-size":"13px","font-family":"roboto"});
	});

	$(".tbl_ga_tr_bn").mouseout(function(){		
		$(this).css({"background-color":"#ffffff","color":"#000000","font-size":"13px","font-family":"roboto"});
	});*/

});


function save(gw,pw,gt,oa,ac,ic,an,row_no,obj){
	
	$(".audit_msg").html("");
	showActionProgress("Saving...");

	var frm = $('#form_'); 

	
	if (obj.parent().parent().find(".chk_tfr_res").is(":checked")){
		var tfr_res = 1;		
		$("#row_id_"+obj.attr("billno")).attr("tfr_res",1);		
	}else{
		var tfr_res = 0;		
		$("#row_id_"+obj.attr("billno")).attr("tfr_res",0);		
	}


	$.ajax({
		type: frm.attr('method'), 
		url: frm.attr('action'), 
		data: frm.serialize()+"&bill_comment="+$("#"+row_no).val()+"&gw="+gw+"&pw="+pw+"&gt="+gt+"&oa="+oa+"&ac="+ac+"&ic="+ic+"&an="+an+"&tfr_res="+tfr_res, 
		dataType: "json", 
		success: function (D) {
			
			closeActionProgress();			

			$(".audit_msg").html("Audit comment updated");

		} 
	});
}


function get_bills(){

	glob_expanded_row = '';

	showActionProgress("Please wait...");

	$.post("index.php/main/load_data/t_gold_audit/grid_data",{
		
		bc : $("#bc :selected").val(),
		fd : $("#fd").val(),
		td : $("#td").val(),
		chk_au : $(".chk_au").is(":checked"),
		chk_unau : $(".chk_unau").is(":checked")

	},function(D){
		closeActionProgress();

		$(".tbl_gold_bill_list").html(D.d);

	},"json");


}





$(document).on("click","#btnSearch",function(){

	showActionProgress("Searching...");

	$.post("index.php/main/load_data/t_gold_audit/search_bill_stat",{
		
		billno : $("#txtSearch").val()

	},function(D){
		
		closeActionProgress();

		if ( D.s == 1 ){
			alert("This bill was redeemed");
		}else{
			alert("Invalid bill number ");
		}

	},"json");	

})