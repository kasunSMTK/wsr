$(document).ready(function(){

	$("#btnSave").click(function(){		
		save();
	});	

});

function save(){	

	showActionProgress("Updating...");

	if (validate_form()){
	
		var frm 		= $('#form_');
	    var fd 			= new FormData();
	    var file_data 	= $('input[type="file"]')[0].files;

	    for(var i = 0 ; i<file_data.length ; i++){
	        fd.append("file_"+i, file_data[i]);
	    }

	    var other_data = $('form').serializeArray();	    

	    $.each(other_data,function(key,input){
	        fd.append(input.name,input.value);
	    });	    
	    
	    $.ajax({
	        url:  frm.attr('action'),
	        data: fd,
	        contentType: false,
	        processData: false,
	        type: 'POST',
	        success: function(D){

				closeActionProgress();

				if (D == 1){
					showMessage("s","Company details update success");										
				}else{					
					showMessage("e","Error, save failed");
				}
			}
	    });


	}

}

function validate_form(){	
	
	if ($("#name").val() == ""){showMessage("e","Enter company name");	return false; }
	if ($("#address").val() == ""){showMessage("e","Enter company address");	return false; }
	if (!validatePhoneno($("#telNos").val())) { return false; }	
	if (!validateFAXno($("#faxNos").val())) { return false; }
	if (!validateEmail($("#email").val())) { return false; }

	return true;	
}

