$(document).ready(function(){

	load_list();
	set_amount_base();
	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_condition").val());
	});

	$("#is_non_gold").change(function(){
		set_amount_base();
	});

});

function set_amount_base(){
		if($("#is_non_gold").is(':checked') ){
			$("#amt_base").show();
		}else{
			$("#amt_base").hide();
		}
}

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				closeActionProgress();

				if (D == 1){
					showMessage("s","Customer saving success");
					clear_form();
					load_list();					
				}else{					
					showMessage("e","Error, save failed");
				}
			}
		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this customer?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_item_category",{
			code : code
		},function(D){
			$(".list_div").html(D);
			closeActionProgress();
			load_list();
		},"text");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_item_category",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#code").val(D.max_no);
		closeActionProgress();
	},"json");
}

function validate_form(){
	if ($("#code").val() == ""){showMessage("e","Enter condition code");	return false; }
	if ($("#des").val() == ""){showMessage("e","Enter condition");	return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$("#is_bulk").prop({ 'checked': false });
	$("#is_non_gold").prop({ 'checked': false });
	$("#is_amount_base").prop({ 'checked': false });
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_item_category/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.code);
		$("#btnSave").val("Edit");
		$("#code").val(D.code);		
		$("#des").val(D.des);

		if (D.is_bulk == 1){
			$("#is_bulk").prop({ 'checked': true });
		}else{
			$("#is_bulk").prop({ 'checked': false });
		}		
		if (D.is_non_gold == 1){
			$("#is_non_gold").prop({ 'checked': true });
		}else{
			$("#is_non_gold").prop({ 'checked': false });
		}		
		if (D.is_amount_base == 1){
			$("#is_amount_base").prop({ 'checked': true });
		}else{
			$("#is_amount_base").prop({ 'checked': false });
		}
		set_amount_base();
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_item_category/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}