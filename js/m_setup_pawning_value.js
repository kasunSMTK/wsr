$(document).ready(function(){
	
	$("#btnUpdate").click(function(){				
		save();
	});	

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				closeActionProgress();

				if (D == 1){
					showMessage("s","Values updated");					
				}else{					
					showMessage("e","Error, update failed");
				}
			}
		});

	}

}

function validate_form(){
	if (!validateAmount($("#min_advance_amount").val() ," Minimum Advance Value")){ return false; }	
	return true;	
}