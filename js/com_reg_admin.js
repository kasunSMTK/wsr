$(document).ready(function(){

    $("#bc_code").focus();

    $('#btnGetCode').click(function(){
        
        var bc_code = $("#bc_code").val();

        if ( bc_code == "" ){
            alert("Please enter branch code");
            return;
        }

        if(bc_code.length < 10){
            alert("Invalid branch code length");
            return;
        }

        get_reg_code();


    });

});

function get_reg_code(){

    showActionProgress("Please wait...");

    $.post("index.php/main/get_data/com_reg_admin",{
        bc_code : $("#bc_code").val()
    },function(D){
        
        closeActionProgress();           

        if (D.s == 1){

            $("#reg_code").val(D.reg_code);

        }else{

            alert("Invalid branch code");

        }

    },"json");

}