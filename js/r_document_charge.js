$(document).ready(function(){

	load_list();
	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_nicNo").val());
	});

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				closeActionProgress();

				if (D == 1){
					showMessage("s","Customer saving success");
					clear_form();
					load_list();
				}else{					
					showMessage("e","Error, save failed");
				}
			}
		});

	}

}

function setDelete(id){

	if (confirm("Do you want delete this doc fee?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/r_document_charge",{
			id : id
		},function(D){		
			$(".list_div").html(D);
			closeActionProgress();
			load_list();
		},"text");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/r_document_charge",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function validate_form(){
	if ($("#bc").val() == ""){ showMessage("e","Select branch"); return false; }
	if (!validateAmount($("#from_val").val() ," from amount")){ return false; }
	if (!validateAmount($("#to_val").val() ," to amount")){ return false; }
	if (!validateAmount($("#amount").val() ," document fee amount")){ return false; }
	return true;	
}

function clear_form(){
	$("input").val("");
	$("#btnSave").val("Save");	
	$('#bc').val("");
	$("#hid").val(0);
}

function setEdit(id){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/r_document_charge/set_edit",{
		id : id
	},function(D){		
		$("#hid").val(D.id);
		$("#btnSave").val("Edit");
		
		$("#bc").val(D.bc);
		$("#from_val").val(D.from_val);
		$("#to_val").val(D.to_val);
		$("#amount").val(D.amount);
		
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/r_document_charge/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}