
$(document).on("click",".rem_cash_acc",function(){

	var bc  = $(this).attr('bc');
	var acc = $(this).attr('acc');

	if (confirm("Do you want remove this account")){

		showActionProgress("Please wait...");

		$.post("index.php/main/load_data/m_bc_cash_acc_setup/remove_acc",{
			bc : bc,
			cash_acc : acc
		},function(D){

			if (D.s == 1){						
				
				closeActionProgress();
				load_data();
				alert("Account removed");
				reset();

			}else if (D.s == 2) {
				closeActionProgress();
				alert("Unable to remove this account, An account entry added under this account code.");
			}
			
		},"json");

	}

});

$(document).ready(function(){

	load_data();

	$("#btnSave").click(function(){
		add_acc();
	});

});

function add_acc(){

	var bc = $("#dp_bc_acc_item :selected").val();
	var acc = $("#hid_acc_code").val();

	if (bc == ''){
		alert("Please selected branch");
		return;
	}

	if (acc == ''){
		alert('Please select cash book account');
		return;
	}
	
	showActionProgress("Please wait...");

	$.post("index.php/main/load_data/m_bc_cash_acc_setup/add_acc",{
		bc : bc,
		cash_acc : acc
	},function(D){

		if (D.s == 1){						
			
			closeActionProgress();
			load_data();
			alert("Account added");
			reset();

		}else if(D.s == 2){			
			alert("This account already added to selected branch");
			closeActionProgress();
		}else{
			
		}

	},"json");
	
}

$(document).on("keypress",".bc_acc_txt",function(){
    $( ".bc_acc_txt" ).autocomplete({
        source: "index.php/main/load_data/t_voucher_general/get_acc_list",
        select: function (event, ui) {                  
            var d = ui.item.value;
            d = d.split(" - ");
            $("#hid_acc_code").val(d[0]);
            $("#btnSave").focus();
        },
        delay : 250
    });
});


function load_data(){	

	$.post("index.php/main/load_data/m_bc_cash_acc_setup/load_data",{
		
	},function(D){

		var T = '';
		var b = '';

		for (n = 0 ; n < D.det.length ; n++){

			if ( b !=  D.det[n].bc){
			
				T += '<tr>';
				T += '<td style="text-align:right; padding:10px 20px 10px 10px">'+D.det[n].bc_name+'</td>';
				T += '<td style="text-align:left">'+D.det[n].desc+'</td>';
				T += '<td style="text-align:center"><a bc='+D.det[n].bc+' acc='+D.det[n].code+' class="rem_cash_acc">remove</a></td>';
				T += '</tr>';

				b =  D.det[n].bc;

			}else{

				T += '<tr>';
				T += '<td style="text-align:right; padding:10px 20px 10px 10px">&nbsp;</td>';
				T += '<td style="text-align:left">'+D.det[n].desc+'</td>';
				T += '<td style="text-align:center"><a bc='+D.det[n].bc+' acc='+D.det[n].code+' class="rem_cash_acc">remove</a></td>';
				T += '</tr>';

			}

		}

		$(".bc_cash_book_acc_show").html(T);

	},"json");
}

function reset(){
	$("#dp_bc_acc_item").val("");
	$("#txt_bc_cash_book_acc").val("");
	$("#hid_acc_code").val("");
}