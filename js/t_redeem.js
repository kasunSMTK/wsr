var redeem_trans_no = "";
var last_max_no = "";
var counter = "";
var disable_monitor = "";
var op_toggle = false;
var pblamt = 0;
var pblamt_constant = 0;
var is_bt_choosed = false;

$(document).ready(function(){
	
	$(".is_old_billno").on("mouseover", function () {
		$(".opn_bill_msg").css("visibility","visible");
	});

	$(".is_old_billno").on("mouseout", function () {
		$(".opn_bill_msg").css("visibility","hidden");
	});


	$(".is_old_billno").click(function(){

		if ($(this).is(":checked")){
			$("#billno").focus();
		}else{
			$("#billno").focus();
		}

	});


	$("#billno").focus();
	
	function set_val_1(obj){
		if ( parseFloat(obj.val()) > parseFloat($("#customer_advance").val()) ){obj.val(parseFloat($("#customer_advance").val())); } 
		discount = isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
		var pa = (pblamt - obj.val()) ;
		$("#card_amount").val( pa.toFixed(2) );
	}

	function set_val_2(obj){

		if (parseFloat(obj.val()) > pblamt){obj.val(""); }
		var card_amount 	= isNaN(parseFloat($("#card_amount").val())) ? 0 : parseFloat($("#card_amount").val());
		pa = parseFloat($("#customer_advance_paying").val()) + parseFloat(card_amount);
		discount = isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
		pa = ( pblamt - pa) ;
		$("#cash_amount").val(pa.toFixed(2));		
	}

	$("#customer_advance_paying").change(function(){set_val_1($(this)); });
	$("#customer_advance_paying").keyup(function(){set_val_1($(this)); });
	

	$("#card_amount").change(function(){set_val_2($(this));calPayAmount(); });
	$("#card_amount").keyup(function(){set_val_2($(this));calPayAmount(); });
	

	$("#cash_amount").change(function(){calPayAmount(); });
	$("#cash_amount").keyup(function(){calPayAmount(); });

	$(".chk_pay_opt_apply").click(function(){
		
		if ($(this).is(":checked")){			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "80px"},'fast');			
			$(this).parent().parent().css("background","#f9f9f9");
		}else{			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "0px"},'fast');
			$(this).parent().parent().css("background","#ffffff");
		}
	});

	$(".pay-close").click(function() {
		$("#btnPaymentOptions").click();		
	});


	$("#btnPaymentOptions").click(function() {

		if (!op_toggle){
			$(".op_holder").animate({height: "400px"},'slow');
			op_toggle = true;
			 $("html, body").animate({ scrollTop: $(document).height() }, 'slow');
		}else{
			$(".op_holder").animate({height: "0px"},'slow');
			op_toggle = false;
			  $("html, body").animate({ scrollTop: 0 }, "slow");
		}

	});



	$(".cal_5").click(function(){
		
		if ($("#billtype").val() == ""){
			$("#bill_code_desc").focus();
			return;
		}
		
		if ($("#billno").val() == "" ){			
			$("#billno").focus();
			return;
		}
		
		LOAD_LOAN($("#billtype").val(),$("#billno").val());	

		$("#no_of_int_cal_days").val( ( parseFloat($("#no_of_days").val()) % 30) - 1 );

	});

	
	$( "#bill_code_desc" ).autocomplete({
		source: "index.php/main/load_data/m_billtype_det/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#billtype").val(bt[0]);				
				$("#billno").val("").focus();
		}
	});	

	$("#billno").keypress(function(e){
		if (e.keyCode == 13){
			LOAD_LOAN($("#billtype").val(),$("#billno").val());
		}
	});


	$("#discount").focus(function(){		
		$(".div_discount_reason_outer").animate({"height":"100px"});
	});

	$("#discount").blur(function(){		
		if ( $(this).val() == "" ){
			$(".div_discount_reason_outer").animate({"height":"0px"});
		}else{
			if ( !parseFloat($(this).val()) > 0 ){
				$(".div_discount_reason_outer").animate({"height":"0px"});
			}
		}
	});


	$("#discount").keyup(function(e){
		$("#pay_bal").val(getPayableAmount());
		calPayAmount();
	});

	$("#btnSave").click(function(){
		save();
	});

	$("#btnPrint").click(function(e){
		$("#print_pdf").submit();
	});

	$("#discount").keypress(function(e){
		if (e.keyCode == 13){
			$("#btnSave").focus();
		}
	});

	$("#btnReset").click(function(){
		redeem_trans_no = $("#no").val();
		resetFormData();
		$("#no").val(redeem_trans_no);
		$("#btnSave").attr("disabled",false).attr("class","btn_regular");
		$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
		$(".pawn_msg").fadeOut();
		$("#bill_code_desc").focus();
		$("#no").val(last_max_no);
		$("#btnPrint").attr("disabled",true).attr("class","btn_regular_disable");
	});

	$("#no").keypress(function(e){
		if (e.keyCode == 13){
			getSavedRedeem($(this).val());
		}
	});

	$("#manual_bill_no").keypress(function(e){

		if (e.keyCode == 13){

			showActionProgress("Please wait...");

			$.post("index.php/main/load_data/t_new_pawn/get_system_billno",{
				manual_bill_no : $("#manual_bill_no").val(),
				cut_bc_no : 1
			},function(D){
				
				closeActionProgress();

				if (D.s == 1){

					$("#billno").val(D.billno);
					LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());	

				}else{
					alert("Invalid manual bill number ");
				}

			},"json");

		}

	});


	$(".nostampdutycal").click(function(){		
		LOAD_LOAN($("#billtype").val(),$("#billno").val());
	});
	

});


$(document).on('click', '.opt_billtype_select', function(event) {
	
	$("#billtype").val( $(this).val() );
	$(".msg_pop_up_bg").css("color","#000000").fadeOut(0);
	$(".multiple_billtype_selection").css("color","#000000").fadeOut(0);
	is_bt_choosed = true;
	LOAD_LOAN($("#billtype").val(),$("#billno").val());
	$("#is_bt_choosed").val(is_bt_choosed);
	
});















function getSavedRedeem(tr_no){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_redeem/getSavedRedeem",{
		tr_no : tr_no
	},function(D){

		closeActionProgress();

		if(D.s == 1){
			
			$("#bill_code_desc").val(D.sum.billtype);
			$("#billtype").val(D.sum.billtype);
			$("#billno").val(D.sum.billno);
			$("#ddate").val(D.sum.ddate);
			$("#dDate").val(D.sum.ddate);
			$("#ln").val(D.sum.loanno);
			

			$("#period").val(D.sum.period);
			$("#finaldate").val(D.sum.finaldate);						
			$("#loan_amount").val(D.sum.requiredamount);
			
			$("#fmintrate").val(D.sum.fmintrate);
			$("#loan_amount").val(D.sum.requiredamount);
			$("#paid_interest").val(D.paid_int);	
			
			$("#payable_amount").val(D.sum.redeemed_amount); //XXX
			$("#balance").val((parseFloat(D.sum.redeem_int) + parseFloat(D.sum.discount)).toFixed(2));
			$("#discount").val(D.sum.discount);		

			if (D.pay_option.advance_amount != undefined){$("#customer_advance_paying").val(D.pay_option.advance_amount.cr_amount) }
			if (D.pay_option.card_details != undefined){$("#card_number").val(D.pay_option.card_details.card_no); $("#card_amount").val(D.pay_option.card_details.card_amount); }
			if (D.pay_option.cash_amount != undefined){$("#cash_amount").val(D.pay_option.cash_amount); }

			//$(".op_holder").animate({height: "400px"},'slow');
			op_toggle = true;
			$("html, body").animate({ scrollTop: $(document).height() }, 'slow');


			$('.pay-penal-sub').animate({height: "80px"},'fast');			
			$('.pay-opt-penal').css("background","#f9f9f9");


			$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
			$("#discount").attr("disabled",true);
			$("#btnReset").attr("disabled",false).attr("class","btn_regular");
			$("#billno,#bill_code_desc").attr("readonly","readonly");

			$("#r_no").val( $("#no").val() );
			$("#by").val("redeem_ticket");				

			$("#btnPrint").attr("disabled",false).attr("class","btn_regular");

		}else{			
			showMessage("e","Invalid loan number");
			$("#btnReset").click();
		}		
		
	},"json");
}











function resetFormData(){
	ddate = $("#ddate").val();
	bc_no = $("#bc_no").val();
	$("input:text").val("");	
	$("#ddate").val(ddate);
	$("#bc_no").val(bc_no);
	$("input:hidden").val("");	
	$("#billno,#bill_code_desc").removeAttr("readonly");
	$("#btnPaymentOptions").click();
	$("#customer_advance_paying,#card_amount,#cash_amount").val("");	
	$(".cal_5").prop("checked",false);

	$(".div_discount_reason_outer").animate({"height":"0px"});
	$("#discount_reason").val("");

}

function save(){

	if (validate_form()){

		if ( rmsps($("#discount").val()) == "" || parseFloat($("#discount").val()) <= 0 || $("#is_discount_approved").val() == 1){

			showActionProgress("Saving...");
			
			$.post("index.php/main/save/t_redeem",{

				loanno 		: $("#hid_loan_no").val(), billtype 	: $("#billtype").val(), billno 		: $("#billno").val(),
				ddate 		: $("#ddate").val(), amount 		: $("#payable_amount").val(), discount 	: $("#discount").val(),
				balance 	: $("#balance").val(),	paid_interest : $("#paid_interest").val(),  hid_cus_serno : $("#hid_cus_serno").val(),
				five_days_int_cal : $(".cal_5").is(":checked") ? 1 : 0,

				customer_advance : $("#customer_advance").val(),
				customer_advance_paying : $("#customer_advance_paying").val(),
				card_number : $("#card_number").val(),
				card_amount : $("#card_amount").val(),
				cash_amount : $("#cash_amount").val(),

				stamp_fee 			: $("#stamp_fee").val(),
				bc_no 	  			: $("#bc_no").val(),
				no_of_int_cal_days 	: $("#no_of_int_cal_days").val(),
				doc_fee_tot 		: $("#doc_fee_tot").val(),
				time 				: $("#time").val(),

				refundable_int		: $("#refundable_int").val(),
				billcode			: $("#billcode").val()

			},function(D){

				closeActionProgress();

				if (D.a == 1){
					showMessage("s","Redeem sucess")
					resetFormData();
					$("#no").val(D.max_no);
					$("#bill_code_desc").focus();
					$("#r_no").val(D.no);
					$("#by").val("redeem_ticket");				
					$("#doc_fee_r").val(D.doc_fee_r);
					$("#ln").val( D.ln );
					$("#print_pdf").submit();

				}else if(D.s == 33){

					alert(D.msg_33);
					location.href = '';
				
				}else{
					showMessage("e", D.m );
					resetFormData();
				}

			},"json");

		}else{		

			if (confirm("Discount approval required.\n\nDo you want send this loan to discount approval?")){

				showActionProgress("Sending approval request...");

				$.post("index.php/main/load_data/approvals/add_approval",{

					loanno 			: $("#hid_loan_no").val(),					
					int_balance  	: $("#balance").val(),
					requested_amount: $("#discount").val(),
					payable_amount 	: $("#payable_amount").val(),
					request_type 	: "DI",
					di_billno 		: $("#bc_no").val() + $("#billno").val(),
					discount_reason : $("#discount_reason").val(),
					time 			: $("#time").val(),
					refundable_int	: $("#refundable_int").val(),
					billcode		: $("#billcode").val()

				},function(D){

					closeActionProgress();

					if (D.s == 1){
						$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
						showApprovalWatingMsgBox("");
						counter = setInterval(timer, 1000); //1000 will  run it every 1 second
						enableApprovalMonitor(D.nno); // set Request approval monitor
					}else{					
						showMessage("s", D.m );
					}

				},"json");

			}

		}
	}

}

function LOAD_LOAN(billtype,billno){

	showActionProgress("Loading...");
	$("#btnSave").attr("disabled",true).attr('class','btn_regular_disable');

	is_old_billno = 0;

	if ($(".is_old_billno").is(":checked")){
		is_old_billno = 1;
	}

	$.post("index.php/main/load_data/t_redeem/LOAD_LOAN",{
		billtype : billtype,
		billno : billno,
		five_days_int_cal : $(".cal_5").is(":checked") ? 1 : 0,
		is_bt_choosed : $("#is_bt_choosed").val(),
		bc_no : $("#bc_no").val(),
		is_old_billno : is_old_billno
	},function(D){

		closeActionProgress();

		if (D.s == 1){

			$("#r_no").val( D.tr_no );
			$("#by").val("redeem_ticket");
			$("#ln").val(D.loan_sum.loanno);
			$("#is_reprint").val(1);

			if (D.loan_sum.status == "R"){
				$(".pawn_msg").html("This bill already redeemed").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				$("#btnPrint").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if (D.loan_sum.status == "C"){
				$(".pawn_msg").html("This bill already canceled").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if (D.loan_sum.status == "F"){
				$(".pawn_msg").html("This bill already forfeited").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if( D.loan_sum.status == "RN" || D.loan_sum.status == "AM" ){
				$(".pawn_msg").html("This bill renewed to bill number " + D.loan_sum.old_o_new_billno ).fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else{
				$(".pawn_msg").html("").fadeOut();
				$("#btnSave").attr("disabled",false).attr("class","btn_regular");
				$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
			}


			$("#billcode").val(D.loan_sum.billcode);
			$("#billtype,#bill_code_desc").val(D.loan_sum.billtype);
			$("#hid_loan_no").val(D.loan_sum.loanno);
			$("#hid_cus_serno").val(D.loan_sum.cus_serno);
			$("#dDate").val(D.loan_sum.ddate);
			$("#time").val(D.loan_sum.time);
			$("#period").val(D.loan_sum.period);
			$("#finaldate").val(D.loan_sum.finaldate);			
			$("#loan_amount").val(D.loan_sum.requiredamount);			
			$("#fmintrate").val(D.loan_sum.fmintrate);

			// calculations from php
			
			if (D.loan_sum.is_renew == 1){
				$("#no_of_days").val(D.int.no_of_days + "     +" + D.loan_sum.old_bill_age );
			}else{
				$("#no_of_days").val(D.int.no_of_days);
			}
			
			$("#interest").val((D.int.total_int).toFixed(2));
			$("#paid_interest").val(D.int.paid_int);
			$("#stamp_fee").val(0);

			$("#goldvalue").val(D.loan_sum.goldvalue);

			
			
			var doc_fee_tot = 0;

			if (D.doc_fees != ""){

				for(nd = 0 ; nd < D.doc_fees.length ; nd++ ){
					doc_fee_tot += parseFloat(D.doc_fees[nd].document_charge);
				}

				//$("#doc_fee_tot").val(doc_fee_tot.toFixed(2));

			}else{
				//$("#doc_fee_tot").val(doc_fee_tot.toFixed(2));
			}


			$("#customer_advance").val(D.customer_advance.balance);
			$("#customer_advance_paying").val(D.customer_advance.balance);

			$("#balance").val(parseFloat(D.int.payable_tot_int_balance).toFixed(2));
			
			var loan_amount 	= parseFloat($("#loan_amount").val());			
			var stamp_fee 		= 0;
			var doc_fee_tot 	= 0; //parseFloat($("#doc_fee_tot").val());
			var balance  		= parseFloat($("#balance").val());			
				pblamt			= (loan_amount + balance + doc_fee_tot);
				pblamt			-= parseFloat($("#customer_advance").val());

				final_payable_amount = (loan_amount + balance + doc_fee_tot);			

				if ( final_payable_amount >= 25000 ){

					if ( $(".nostampdutycal").is(":checked") ){
						$("#stamp_fee").val("0.00");
					}else{
						//pblamt += 25;
						//$("#stamp_fee").val("25.00");
					}

				}



				if ( D.int.int_within_10days ){
					pblamt -= D.int.refundable_int;

					$("#refundable_int").val(D.int.refundable_int.toFixed(2));
				}

				
				pblamt_constant = pblamt;
			
			$("#payable_amount").val(pblamt.toFixed(2)); //XXX
			
			calPayAmount();

			
			$("#cash_amount").val( $("#pay_bal").val() );
			$("#pay_bal").val(0);

			//$("#discount").val("0.00").focus().select();
			$("#cus_info_div").css({height: "42px"}).html(D.cus_info).animate({height: "398px"});
			$("#billtype_info_div").html(D.rec);

			is_bt_choosed = false;

			//$("#cus_info_div").html(D.cus_info);	





		}else{
			showMessage("e","Invalid bill type or bill number");
			$(".pawn_msg").fadeOut();
			$("#cus_info_div").html("");
			$("#btnSave").attr("disabled",false).attr("class","btn_regular");
			$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
		} 
		
		
	},"json");

}






function getPayableAmount(){

	var loan_amount 	= isNaN(parseFloat($("#loan_amount").val())) ? 0 : parseFloat($("#loan_amount").val());
	var stamp_fee 		= isNaN(parseFloat($("#stamp_fee").val())) ? 0 : parseFloat($("#stamp_fee").val());
	var discount 		= isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
	var balance  		= isNaN(parseFloat($("#balance").val())) ? 0 : parseFloat($("#balance").val());
	var paid_interest  	= isNaN(parseFloat($("#paid_interest").val())) ? 0 : parseFloat($("#paid_interest").val());

	if ( discount > balance ){
		discount = 0; $("#discount").val("");
	}else{
		discount = discount; 
	}

	var payable_amount = parseFloat((loan_amount + balance + stamp_fee)) - parseFloat((discount + customer_advance_paying));
	
	return payable_amount.toFixed(2);

}






function calPayAmount(){
	
	discount = isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
	pblamt = pblamt_constant;
	pblamt -= discount;
	var pa = pblamt;

	var customer_advance = isNaN(parseFloat($("#customer_advance").val())) ? 0 : parseFloat($("#customer_advance").val());
	var customer_advance_paying = isNaN(parseFloat($("#customer_advance_paying").val())) ? 0 : parseFloat($("#customer_advance_paying").val());	
	
	var card_amount = isNaN(parseFloat($("#card_amount").val())) ? 0 : parseFloat($("#card_amount").val());	
	var cash_amount = isNaN(parseFloat($("#cash_amount").val())) ? 0 : parseFloat($("#cash_amount").val());
	

	if (  (pblamt -(customer_advance_paying + card_amount)) < cash_amount ){
		$("#cash_amount").val(0);
	}


	cash_amount = isNaN(parseFloat($("#cash_amount").val())) ? 0 : parseFloat($("#cash_amount").val());

	
	var n = 0;

		n += pa;
		n -= customer_advance_paying;
		n -= card_amount;
		n -= cash_amount;

	$("#pay_bal").val( n.toFixed(2) );
}






function validate_form(){
	
	if ( rmsps($("#bill_code_desc").val()) ==  "" || rmsps($("#billtype").val()) ==  "" ){ showMessage("e","Select bill type"); $("#bill_code_desc").val("").focus(); return false; }
	if ( rmsps($("#billno").val()) ==  "" || !validateNumber($("#billno").val()) ){ showMessage("e","Select bill number"); $("#billno").val("").focus(); return false; }
		
	if ( $("#discount").val() != "" ){
		if ( parseFloat($("#discount").val()) > 0 ){
			if ( $("#discount_reason").val() == "" ){
				alert("Please enter reason for discount ");
				$("#discount_reason").focus();
				return false;
			}
		}
	}

	var startDate = $('#dDate').val().replace('-','/');
	var endDate = $('#ddate').val().replace('-','/');

	if(startDate > endDate){
		alert("Invalid redeem date");
		return false;
	}

	return true;	
}

function setEdit(recid){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_new_pawn/set_edit",{
		recid : recid
	},function(D){		
		
		closeActionProgress();
	},"json");	

}

