$(document).ready(function(){

	load_list();

	$("#nicNo").focus();
	
	$("#btnSave").click(function(){		
		save();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_bcname").val());
	});

	$("#area").change(function(){
		$("#bc").focus();
	});	
	
	$( ".pawn_start_date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: "1930:2025"
	});

});

function save(){	

	showActionProgress("Saving...");

	if (validate_form()){
		var frm = $('#form_');
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize(),
			dataType: "json",
			success: function (D) {

				//closeActionProgress();

				if (D.s == 1){
					showMessage("s","Branch saving success");
					clear_form();
					load_list();					
				}else{					
					showMessage("e",D.m);
				}
			}
		});

	}

}

function setDelete(code){

	if (confirm("Do you want delete this branch?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/m_branches",{
			code : code
		},function(D){		

			if (D.s == 1){
				$(".list_div").html(D);
				closeActionProgress();
				load_list();
			}else{

				showMessage("e",D.m);
			}

		},"json");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/m_branches",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D.T);
		$("#bc").val(D.max_no);
		$("#bc_no").val(D.bc_no);
		closeActionProgress();
	},"json");
}

function validate_form(){

	if ($("#area").val() == ""){showMessage("e","Select area");	return false; }
	if ($("#name").val() == ""){showMessage("e","Enter branch name");	return false; }	
	if ($("#bc_type :selected").val() == ""){ showMessage("e","Select branch type");	return false; }	
	
	if ($("#address").val() == ""){showMessage("e","Enter branch address");	return false; }
	//if (!validatePhoneno($("#telno").val())) { return false; }	
	//if (!validateFAXno($("#faxno").val())) { return false; }
	//if (!validateEmail($("#email").val())) { return false; }

	return true;	
}

function clear_form(){
	$("input").val("");
	$("#bc_type,#area").val("");
	$("#btnSave").val("Save");	
	$('#btnSearch').val("Search");
	$("#hid").val(0);
	$("#cash_acc").val("");
	$("#stock_acc").val("");
	$("#interest_acc").val("");
	$("#other_bc_name_allow,#close_bc").prop("checked",false);
}

function setEdit(code){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_branches/set_edit",{
		code : code
	},function(D){		
		$("#hid").val(D.bc);
		$("#btnSave").val("Edit");
		$("#area").val(D.area);		
		$("#bc").val(D.bc);		
		$("#bc_no").val(D.bc_no);
		$("#bc_type").val(D.bc_type);
		$("#name").val(D.name);
		$("#address").val(D.address);
		$("#telno").val(D.telno);
		$("#faxno").val(D.faxno);		
		$("#email").val(D.email);
		
		
		$("#cash_acc").val(D.cash_acc);
		$("#stock_acc").val(D.stock_acc);
		$("#interest_acc").val(D.interest_acc);
		
		
		$("#pawn_start_date").val(D.pawn_start_date);

		$("#bc_max_cash_bal").val(D.bc_max_cash_bal_);
		$("#bc_min_cash_bal").val(D.bc_min_cash_bal_);

		if (D.other_bc_name_allow == 1){
			$("#other_bc_name_allow").prop("checked",true);
		}else{
			$("#other_bc_name_allow").prop("checked",false);
		}

		if (D.close_bc == 1){
			$("#close_bc").prop("checked",true);
		}else{
			$("#close_bc").prop("checked",false);
		}

		closeActionProgress();
	},"json");	
}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/m_branches/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}