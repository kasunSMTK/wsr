$(document).ready(function(){
   
    $("#code").blur(function(){
        check_code();
    });

    load_list();

    $("#btnSearch").click(function(){
        search($("#Search_accname").val());
    });

     $("#btnSave").click(function(){     
        save();
    }); 

           
    $("#search_all").click(function(){
         if($("#search_all").is(':checked')){
            $.post("index.php/main/load_data/utility/get_data_table", {
                    code:$("#srchee").val(),
                    tbl:"m_default_account",
                    tbl_fied_names:"Code-Acc Code-Description",
                    fied_names:"code-acc_code-description",
                    col4:"Y",
                    search_all:"Y"
                    }, function(r){
                $("#grid_body").html(r);
                }, "text");
        }else{
            $("#srchee").keyup();
        }
      
    });


    $("#acc_code").keydown(function(e){
        if(e.keyCode == 13){
            set_cus_values($(this));
        }
    });

    $("#acc_code").blur(function(){
        set_cus_values($(this));
    });

    $("#acc_code").keydown(function(e){
        if(e.keyCode == 112){
            $("#pop_search4").val($("#acc_code").val());
            load_accf1();
            $("#serch_pop4").center();
            $("#blocker").css("display", "block");
            setTimeout("$('#pop_search4').focus()", 100);
        }
        $("#pop_search4").keyup(function(e){
            if(e.keyCode != 13 && e.keyCode != 38 && e.keyCode != 40 && e.keyCode != 112 ) { 
                 load_accf1();
            }
        }); 
        if(e.keyCode == 46){
            $("#acc_code").val("");
            $("#acc_code2").val("");
        }
    });
});

function load_accf1(){
      $.post("index.php/main/load_data/utility/f1_selection_list", {
          data_tbl:"m_account",
          field:"code",
          field2:"description",
          preview2:"Account Name",
          search : $("#pop_search4").val() 
      }, 
      function(r){
          $("#sr4").html(r);
          settings_accf1();            
      }, "text");
  }

  function settings_accf1(){
      $("#item_list .cl").click(
        function(){        
          $("#acc_code").val($(this).children().eq(0).html());
          $("#acc_code2").val($(this).children().eq(0).html());
          $("#pop_close4").click();                
      })    
  }

  function save(){    

    showActionProgress("Saving...");

    if (validate_form()){
        var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: "json",
            success: function (D) {

                closeActionProgress();

                if (D == 1){
                    showMessage("s","Account saving success");
                    clear_form();
                    load_list();                    
                }else if (D == 2){                  
                    showMessage("e","Error, Duplicate entry");
                }else if (D == 3){                  
                    showMessage("e","Error,No permission to edit data.");
                }else{                  
                    showMessage("e","Error, save failed");
                }
            }
        });

    }

}



function clear_form(){
    $("input").val("");
    $("#btnSave").val("Save");
    $("#btnReset").val("Reset");  
    $("#btnExit").val("Exit");    
    $('#btnSearch').val("Search");
    $("#code_").val(0);
}

// function save(){
//     var frm = $('#form_');
//     loding();
//     $.ajax({
//         type: frm.attr('method'),
//         url: frm.attr('action'),
//         data: frm.serialize(),
//         success: function (pid){
//             if(pid == 1){
//                 loding();
//                 sucess_msg();
//             }else if(pid == 2){
//                 set_msg("No permission to add data.");
//             }else if(pid == 3){
//                 set_msg("No permission to edit data.");
//             }else{
//                 set_msg(pid);
//             }
            
//         }
//     });
// }


function check_code(){
    loding();
    var code = $("#code").val();
    $.post("index.php/main/load_data/m_default_account/check_code", {
        code : code
    }, function(res){
        if(res == 1){
            if(confirm("This code ("+code+") already added. \n\n Do you need edit it?")){
                setEdit(code);
            }else{
                $("#code").val('');
                $("#code").attr("readonly",false);
                input_reset();
            }
        }
        loding();
    }, "text");
}

function validate_form(){
    if ($("#code").val() == ""){showMessage("e","Please Enter Code");    return false; }
     if ($("#acc_code").val() == ""){showMessage("e","Please Enter Account Code");    return false; }
    if ($("#description").val() == ""){showMessage("e"," Please Enter Description");  return false; }
    return true;    
}

function setDelete(code){

    if (confirm("Do you want delete this Account?")){
        showActionProgress("Deleting...");
        $.post("index.php/main/delete/m_default_account",{
            code : code
        },function(D){
            $(".list_div").html(D);
            closeActionProgress();
            clear_form();
            load_list();
        },"text");
    }
}
function setEdit(code){
    showActionProgress("Loading...");
    $.post("index.php/main/load_data/m_default_account/set_edit", {
        code : code
    }, function(res){
        $("#code_").val(res.code);
        $("#code").val(res.code);
        $("#code").attr("readonly", true);
        $("#des").val(res.description);
        $("#acc_code").val(res.acc_code);
        $("#acc_code2").val(res.acc_code);
       

        input_active();
        closeActionProgress();

    }, "json");
}

function set_cus_values(f){
    var v = f.val();
    v = v.split("|");
    if(v.length == 2){
        f.val(v[1]);
        $("#acc_code2").val(v[0]);
              
    }
}

function formatItems(row){
    return "<strong> " +row[0] + "</strong> | <strong> " +row[1] + "</strong>";
}

function formatItemsResult(row){
    return row[0]+"|"+row[1];
}

function load_list(){
    showActionProgress("Loading...");
    $.post("index.php/main/load/m_default_account",{
        s : "load_list"
    },function(D){      
        $(".list_div").html(D.T);
        $("#bc").val(D.max_no);
        closeActionProgress();
    },"json");
}

function search(skey){
    showActionProgress("Searching...");
    $.post("index.php/main/load_data/m_default_account/search",{
        skey : skey
    },function(D){      
        $(".list_div").html(D);
        closeActionProgress();
    },"text");
}