var redeem_trans_no = "";
var last_max_no = "";
var counter = "";
var disable_monitor = "";
var op_toggle = false;
var pblamt = 0;
var pblamt_constant = 0;
var is_bt_choosed = false;
var rowCount=0;
var n=1;
var temp=0;

$(document).ready(function(){
	
	$(".is_old_billno").on("mouseover", function () {
		$(".opn_bill_msg").css("visibility","visible");
	});
	$(".is_old_billno").on("mouseout", function () {
		$(".opn_bill_msg").css("visibility","hidden");
	});

	$(".is_old_billno").click(function(){
		if ($(this).is(":checked")){
			$("#billno").focus();
		}else{
			$("#billno").focus();
		}
	});

	$("#billno").focus();
	
	function set_val_1(obj){
		if ( parseFloat(obj.val()) > parseFloat($("#customer_advance").val()) ){obj.val(parseFloat($("#customer_advance").val())); } 
		discount = isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
		var pa = (pblamt - obj.val()) ;
		$("#card_amount").val( pa.toFixed(2) );
	}
	function set_val_2(obj){
		if (parseFloat(obj.val()) > pblamt){obj.val(""); }
		var card_amount 	= isNaN(parseFloat($("#card_amount").val())) ? 0 : parseFloat($("#card_amount").val());
		pa = parseFloat($("#customer_advance_paying").val()) + parseFloat(card_amount);
		discount = isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
		pa = ( pblamt - pa) ;
		$("#cash_amount").val(pa.toFixed(2));		
	}

	$("#customer_advance_paying").change(function(){set_val_1($(this)); });
	$("#customer_advance_paying").keyup(function(){set_val_1($(this)); });
	
	$("#card_amount").change(function(){set_val_2($(this));calPayAmount(); });
	$("#card_amount").keyup(function(){set_val_2($(this));calPayAmount(); });
	
	$("#cash_amount").change(function(){calPayAmount(); });
	$("#cash_amount").keyup(function(){calPayAmount(); });
	$(".chk_pay_opt_apply").click(function(){
		
		if ($(this).is(":checked")){			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "80px"},'fast');			
			$(this).parent().parent().css("background","#f9f9f9");
		}else{			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "0px"},'fast');
			$(this).parent().parent().css("background","#ffffff");
		}
	});
	$(".pay-close").click(function() {
		$("#btnPaymentOptions").click();		
	});

	$("#btnPaymentOptions").click(function() {
		if (!op_toggle){
			$(".op_holder").animate({height: "400px"},'slow');
			op_toggle = true;
			 $("html, body").animate({ scrollTop: $(document).height() }, 'slow');
		}else{
			$(".op_holder").animate({height: "0px"},'slow');
			op_toggle = false;
			  $("html, body").animate({ scrollTop: 0 }, "slow");
		}
	});

	$(".cal_5").click(function(){
		
		if ($("#billtype").val() == ""){
			$("#bill_code_desc").focus();
			return;
		}
		
		if ($("#billno").val() == "" ){			
			$("#billno").focus();
			return;
		}
		
		LOAD_LOAN($("#billtype").val(),$("#billno").val());	
		$("#no_of_int_cal_days").val( ( parseFloat($("#no_of_days").val()) % 30) - 1 );
	});
	
	$("#bill_code_desc" ).autocomplete({
		source: "index.php/main/load_data/m_billtype_det/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#billtype").val(bt[0]);				
				$("#billno").val("").focus();
		}
	});

	$("#billno").keypress(function(e){
		if (e.keyCode == 13){
			LOAD_LOAN($("#billtype").val(),$("#billno").val());
		}
	});

	$("#discount").focus(function(){		
		$(".div_discount_reason_outer").animate({"height":"100px"});
	});
	$("#discount").blur(function(){		
		if ( $(this).val() == "" ){
			$(".div_discount_reason_outer").animate({"height":"0px"});
		}else{
			if ( !parseFloat($(this).val()) > 0 ){
				$(".div_discount_reason_outer").animate({"height":"0px"});
			}
		}
	});

	$("#btngentag").click(function(){	

		$category="1";	
		set_tag_no($category);
	});

	$("#discount").keyup(function(e){
		$("#pay_bal").val(getPayableAmount());
		calPayAmount();
	});
	$("#btnSave").click(function(){
		if(validate()){
			save();
		}
	});
	$("#btnPrint").click(function(e){
		$("#r_no").val($("#loanno").val());
		$("#print_pdf").submit();
	});
	$("#discount").keypress(function(e){
		if (e.keyCode == 13){
			$("#btnSave").focus();
		}
	});
	$("#btnReset").click(function(){
		redeem_trans_no = $("#no").val();
		resetFormData();
		$("#no").val(redeem_trans_no);
		$("#btnSave").attr("disabled",false).attr("class","btn_regular");
		$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
		$(".pawn_msg").fadeOut();
		$("#bill_code_desc").focus();
		$("#no").val(last_max_no);
		$("#btnPrint").attr("disabled",true).attr("class","btn_regular_disable");
	});
	$("#no").keypress(function(e){
		if (e.keyCode == 13){
			getSavedRedeem($(this).val());
		}
	});
	$("#manual_bill_no").keypress(function(e){
		if (e.keyCode == 13){
			showActionProgress("Please wait...");
			$.post("index.php/main/load_data/t_new_pawn/get_system_billno",{
				manual_bill_no : $("#manual_bill_no").val(),
				cut_bc_no : 1
			},function(D){
				
				closeActionProgress();
				if (D.s == 1){
					$("#billno").val(D.billno);
					LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());	
				}else{
					alert("Invalid manual bill number ");
				}
			},"json");
		}
	});

	$(".nostampdutycal").click(function(){		
		LOAD_LOAN($("#billtype").val(),$("#billno").val());
	});
	
});


$(document).on('click', '.opt_billtype_select', function(event) {
	
	$("#billtype").val( $(this).val() );
	$(".msg_pop_up_bg").css("color","#000000").fadeOut(0);
	$(".multiple_billtype_selection").css("color","#000000").fadeOut(0);
	is_bt_choosed = true;
	LOAD_LOAN($("#billtype").val(),$("#billno").val());
	$("#is_bt_choosed").val(is_bt_choosed);
	
});

function getSavedRedeem(tr_no){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/t_tag/getSavedRedeem",{
		tr_no : tr_no
	},function(D){
		closeActionProgress();
		if(D.s == 1){
			
			$("#bill_code_desc").val(D.sum.billtype);
			$("#billtype").val(D.sum.billtype);
			$("#billno").val(D.sum.billno);
			$("#ddate").val(D.sum.ddate);
			$("#dDate").val(D.sum.ddate);
			$("#ln").val(D.sum.loanno);
			
			$("#period").val(D.sum.period);
			$("#finaldate").val(D.sum.finaldate);						
			$("#loan_amount").val(D.sum.requiredamount);
			
			$("#fmintrate").val(D.sum.fmintrate);
			$("#loan_amount").val(D.sum.requiredamount);
			$("#paid_interest").val(D.paid_int);	
			
			$("#payable_amount").val(D.sum.redeemed_amount); //XXX
			$("#balance").val((parseFloat(D.sum.redeem_int) + parseFloat(D.sum.discount)).toFixed(2));
			$("#discount").val(D.sum.discount);		
			if (D.pay_option.advance_amount != undefined){$("#customer_advance_paying").val(D.pay_option.advance_amount.cr_amount) }
			if (D.pay_option.card_details != undefined){$("#card_number").val(D.pay_option.card_details.card_no); $("#card_amount").val(D.pay_option.card_details.card_amount); }
			if (D.pay_option.cash_amount != undefined){$("#cash_amount").val(D.pay_option.cash_amount); }
			//$(".op_holder").animate({height: "400px"},'slow');
			op_toggle = true;
			$("html, body").animate({ scrollTop: $(document).height() }, 'slow');

			$('.pay-penal-sub').animate({height: "80px"},'fast');			
			$('.pay-opt-penal').css("background","#f9f9f9");

			$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
			$("#discount").attr("disabled",true);
			$("#btnReset").attr("disabled",false).attr("class","btn_regular");
			$("#billno,#bill_code_desc").attr("readonly","readonly");
			$("#r_no").val( $("#no").val() );
			$("#by").val("redeem_ticket");				
			$("#btnPrint").attr("disabled",false).attr("class","btn_regular");
		}else{			
			showMessage("e","Invalid loan number");
			$("#btnReset").click();
		}		
		
	},"json");
}

function resetFormData(){
	ddate = $("#ddate").val();
	bc_no = $("#bc_no").val();
	$("input:text").val("");	
	$("#ddate").val(ddate);
	$("#bc_no").val(bc_no);
	$("input:hidden").val("");	
	$("#billno,#bill_code_desc").removeAttr("readonly");
	$("#btnPaymentOptions").click();
	$("#customer_advance_paying,#card_amount,#cash_amount").val("");	
	$(".cal_5").prop("checked",false);
	$(".div_discount_reason_outer").animate({"height":"0px"});
	$("#discount_reason").val("");
}

function save()
{
	showActionProgress("Saving...");
	if (validate_form()){
		var frm = $('#form_');
		//define js array
		var detail = { items: [] , sum:[] };
		var i=0;

		detail.sum.push({			
		      ddate  : $("#date").val(),
		      nno  : $("#nno").val(), 
		      store  : $("#store").val(),  
		      employee  : $("#employee").val(),
		      note  : $("#note").val(), 
		      amount  : $("#amount").val(), 
		      store  : $("#store").val(),
		      employee  : $("#employee").val(),
		      note  : $("#note").val(),
		      billtype  : $("#billtype").val(),
		      is_bulk  : $("#is_bulk").val(),
		      billno  : $("#bc_no").val()+$("#billno").val(),
		      loanno  : $("#loanno").val(),
		});

		while ( i < rowCount)
		{
		    detail.items.push(
		    {
		      item_code  : $("#i_code_"+i).val(),
		      bc_no  : $("#bc_no").val(),
		      billno  : $("#bc_no").val()+$("#billno").val(),
		      tagno  : $("#tag_"+i+"").val(),
		      cat_code  : $("#c_code_"+i+"").val(),   
		      goldweight  : $("#goldweight_"+i+"").val(),  
		      pureweight  : $("#pureweight"+i+"").val(),   
		      qty  : $("#qty"+i+"").val(),
		      goldtype  : $("#goldtype"+i+"").val(),
		      quality  : $("#quality"+i+"").val(), 
		      value  : $("#value"+i+"").val(),
		      denci_weight  : $("#denci_weight"+i+"").val(),
		      elec_model  : $("#elec_model"+i+"").val(),
		      elec_imei  : $("#elec_imei"+i+"").val(),
		      elec_serno  : $("#elec_serno"+i+"").val(),
		      elec_description  : $("#elec_description"+i+"").val(),
		      veh_model  : $("#veh_model"+i+"").val(),
		      veh_engine_no  : $("#veh_engine_no"+i+"").val(),
		      veh_chassis_no  : $("#veh_chassis_no"+i+"").val(),
		      veh_description  : $("#veh_description"+i+"").val(),
		      loanno  : $("#loanno").val(),
		      goldvalue  : $("#goldvalue").val()
		    });

		    i++;
		}

		var jsonData = JSON.stringify(detail);	
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			 data: {details : jsonData },
			dataType: "json",
			success: function (D) {		
				$("#r_no").val($("#loanno").val());				
				alert("Tag Generated Successfuly");
				$("#print_pdf").submit();
				document.location.reload(true);
			}
		});
	}else{
		alert("Tag Generat Failed.");
	}
	closeActionProgress();		
}

function LOAD_LOAN(billtype,billno){

	showActionProgress("Loading...");
	$.post("index.php/main/load_data/t_tag/LOAD_LOAN",{
		billtype : billtype,
		billno : billno,
		bc_no : $("#bc_no").val(),
	},function(D){
		closeActionProgress();
		if(D !="0"){
                 if (D.tagged == 1) {
                   alert("Pawning Items Already Tagged.");
                   $("#btnReset")
                     .attr("disabled", false)
                     .attr("class", "btn_regular");
                   $("#btnSave")
                     .attr("disabled", true)
                     .attr("class", "btn_regular_disable");
                 }

                 rowCount = D.loan_det.length;
                 //alert(JSON.stringify(D));
                 $("#rowCount").val(D.loan_det.length);
                 $("#loanno").val(D.loan_sum.loanno);
                 $("#goldvalue").val(D.loan_sum.goldvalue);
                 $("#billtype").val(D.loan_sum.billtype);
                 $("#pawn_date").val(D.loan_sum.ori_pwn_date);
                 $("#forfeit_date").val(D.loan_sum.forfeit_date);
                 $("#int_rate").val(D.loan_sum.fmintrate);
                 $("#period").val(D.loan_sum.period);
                 $("#amount").val(D.loan_sum.requiredamount);
                 $("#interest").val(D.loan_sum.fmintrest);
                 $("#customer").val(D.loan_sum.nicno);
                 $("#store").val(D.loan_sum.store);
                 $("#employee").val(D.loan_sum.officer);
                 var cat_code = D.loan_sum.cat_code;
                 var det = [];
                 det.push(D.loan_det); //alert(det[0].loanno);

                 set_item_grid(cat_code, det[0], D.isedit);
               }else{
			alert("This Bill No is Not Forfeited ! ");
		}

	},"json");
}

function validate(){
var ready = true;
var bulk_ok = 1;
	if($("#billno").val()==''){
		alert('Please Select Bill No');
		ready = false;
	}else if($("#store").val()==''){
		alert('Please Select The Store');
		ready = false;
	}else if($("#employee").val()==''){
		alert('Please Select The Employee');
		ready = false;		
	}else{
		$(".catg").each(function(index){
			if($(this).val()=='--' || $(this).val()==''){
				bulk_ok =0; 
				ready = false;				
			}
		});

		if(bulk_ok == 0){
			alert('Please Select all the items in Bulk');			
		}
	}
	return ready;
}

function getPayableAmount(){
	var loan_amount 	= isNaN(parseFloat($("#loan_amount").val())) ? 0 : parseFloat($("#loan_amount").val());
	var stamp_fee 		= isNaN(parseFloat($("#stamp_fee").val())) ? 0 : parseFloat($("#stamp_fee").val());
	var discount 		= isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
	var balance  		= isNaN(parseFloat($("#balance").val())) ? 0 : parseFloat($("#balance").val());
	var paid_interest  	= isNaN(parseFloat($("#paid_interest").val())) ? 0 : parseFloat($("#paid_interest").val());
	if ( discount > balance ){
		discount = 0; $("#discount").val("");
	}else{
		discount = discount; 
	}
	var payable_amount = parseFloat((loan_amount + balance + stamp_fee)) - parseFloat((discount + customer_advance_paying));
	
	return payable_amount.toFixed(2);
}



function calPayAmount(){
	
	discount = isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val());
	pblamt = pblamt_constant;
	pblamt -= discount;
	var pa = pblamt;
	var customer_advance = isNaN(parseFloat($("#customer_advance").val())) ? 0 : parseFloat($("#customer_advance").val());
	var customer_advance_paying = isNaN(parseFloat($("#customer_advance_paying").val())) ? 0 : parseFloat($("#customer_advance_paying").val());	
	
	var card_amount = isNaN(parseFloat($("#card_amount").val())) ? 0 : parseFloat($("#card_amount").val());	
	var cash_amount = isNaN(parseFloat($("#cash_amount").val())) ? 0 : parseFloat($("#cash_amount").val());
	
	if (  (pblamt -(customer_advance_paying + card_amount)) < cash_amount ){
		$("#cash_amount").val(0);
	}

	cash_amount = isNaN(parseFloat($("#cash_amount").val())) ? 0 : parseFloat($("#cash_amount").val());
	
	var n = 0;
		n += pa;
		n -= customer_advance_paying;
		n -= card_amount;
		n -= cash_amount;
	$("#pay_bal").val( n.toFixed(2) );
}


function validate_form()
{
		return true;
		
}
function setEdit(recid){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/t_new_pawn/set_edit",{
		recid : recid
	},function(D){		
		
		closeActionProgress();
	},"json");	
}

function set_item_grid(category,det,isedit){
	var tbl = "";
	//alert(det[0][0].loanno);
	//var category = cat_code;
	
	tbl+="<table id='ffTb' class='' cellspacing='0' cellpadding='0' border='0'>";
	tbl+="<thead class=''>";
	tbl+="<tr class='' width='100%'>";
	tbl+="<th class='text_box_holder_new_pawn' style='' width='200'>Category</th>";
	tbl+="<th class='text_box_holder_new_pawn' width='275'>Item</th>";
	tbl+="<th class='text_box_holder_new_pawn' width='110'>Condition</th>";
	tbl+="<th class='text_box_holder_new_pawn' width='80'>Quality</th>";
	tbl+="<th class='text_box_holder_new_pawn' width='85'>Qty</th>";

	if(category==15){ // 15~electronic
		tbl+="<th class='text_box_holder_new_pawn' width='60'>Value</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='75'>Model</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='90'>IMEI</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='90'>S/N</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='115'>Description</th>";
	}else if(category==16){
		tbl+="<th class='text_box_holder_new_pawn' width='60'>Value</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='75'>Model</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='90'>Eng.No</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='90'>Chas.No</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='115'>Description</th>";
	}else if(category==17){
		tbl+="<th class='text_box_holder_new_pawn' width='60'>T.Weight</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='75'>P.Weight</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='90'>Value</th>";
	}else{
		tbl+="<th class='text_box_holder_new_pawn' width='60'>Karatage</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='75'>T.Weight</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='90'>P.Weight</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='90'>Densi read</th>";
		tbl+="<th class='text_box_holder_new_pawn' width='115'>Value</th>";	
	}
	
	tbl+="<th class='text_box_holder_new_pawn' width='115'>Tag No</th>";
	tbl+="</tr>";
	tbl+="</thead>";
	tbl+="<tbody id='GrDt'>";
	
//alert(category);
	
	for(var i=0; i<det.length; i++ ){
		if(category!=13){					
							tbl+="<tr id='ln_14313' bilt='3C'>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left' id='itemCode_"+i+"' name='itemCode_"+i+"'>"+det[i].cat_code+"~"+det[i].cat_des+"</td>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='left' >"+det[i].itemcode+"~"+det[i].itemname+"</td>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+det[i].condition+"</td>";
							
							if(category==15){ // 15~electronic
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].quality+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].qty+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input  class='input_text_regular_new_pawn' type='text' style='width:150px;border:1px solid green' value='"+det[i].value+"'></td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+det[i].elec_model+"</td>"; //model
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+det[i].elec_imei+"</td>"; 
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].elec_serno+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'>"+det[i].elec_description+"</td>";
							}else if(category==16){ //vehical
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].quality+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].qty+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input  class='input_text_regular_new_pawn' type='text' style='width:150px;border:1px solid green' value='"+det[i].value+"'></td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+det[i].veh_model+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+det[i].veh_engine_no+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].veh_chassis_no+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'>"+det[i].veh_description+"</td>";
							}else if(category==17){ //silver
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].quality+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input  class='input_text_regular_new_pawn' type='text' style='width:150px;border:1px solid green' value='"+det[i].value+"'></td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].goldweight+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].pure_weight+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input  class='input_text_regular_new_pawn' type='text' style='width:150px;border:1px solid green' value='"+det[i].value+"'></td>";
							}else{				
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].quality+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].qty+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+det[i].goldtype+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].goldweight+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].pure_weight+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'>"+det[i].denci_weight+"</td>";
								tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input  class='input_text_regular_new_pawn' type='text' style='width:150px;border:1px solid green' value='"+det[i].value+"'></td>";
							}

							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input id='tag_"+i+"' class='input_text_regular_new_pawn' type='text' disabled style='width:150px;border:1px solid green' name='tag_"+i+"' value='"+det[i].tag_no+"'></td>";
							tbl +="<input type='hidden' value='"+det[i].itemcode+"' txt='"+det[i].itemcode+"' name='i_code_"+i+"' id='i_code_"+i+"'>"
							tbl +="<input type='hidden' class= 'catg' value='"+det[i].cat_code+"' text='"+det[i].cat_code+"' name='c_code_"+i+"' id='c_code_"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].goldweight+"' text='"+det[i].goldweight+"' name='goldweight_"+i+"' id='goldweight_"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].pure_weight+"' text='"+det[i].pure_weight+"' name='pureweight"+i+"' id='pureweight"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].qty+"' text='"+det[i].qty+"' name='qty"+i+"' id='qty"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].goldtype+"' text='"+det[i].goldtype+"' name='goldtype"+i+"' id='goldtype"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].quality+"' text='"+det[i].quality+"' name='quality"+i+"' id='quality"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].value+"' text='"+det[i].value+"' name='value"+i+"' id='value"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].denci_weight+"' text='"+det[i].denci_weight+"' name='denci_weight"+i+"' id='denci_weight"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].elec_model+"' text='"+det[i].elec_model+"' name='elec_model"+i+"' id='elec_model"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].elec_imei+"' text='"+det[i].elec_imei+"' name='elec_imei"+i+"' id='elec_imei"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].elec_serno+"' text='"+det[i].elec_serno+"' name='elec_serno"+i+"' id='elec_serno"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].elec_description+"' text='"+det[i].elec_description+"' name='elec_description"+i+"' id='elec_description"+i+"'>"
							
							tbl +="<input type='hidden' value='"+det[i].veh_model+"' text='"+det[i].veh_model+"' name='veh_model"+i+"' id='veh_model"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].veh_engine_no+"' text='"+det[i].veh_engine_no+"' name='veh_engine_no"+i+"' id='veh_engine_no"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].veh_chassis_no+"' text='"+det[i].veh_chassis_no+"' name='veh_chassis_no"+i+"' id='veh_chassis_no"+i+"'>"
							tbl +="<input type='hidden' value='"+det[i].veh_description+"' text='"+det[i].veh_description+"' name='veh_description"+i+"' id='veh_description"+i+"'>"
							
		}else{
				rowCount = parseFloat(det[i].qty) ;
				$("#is_bulk").val(1);
				tbl+="<tr><td colspan=3> BULK ITEM :"+det[i].bulk_items+"</td><tr>";	
				for (var y = 0; y<det[i].qty; y++) {
//alert(isedit);
							if(isedit == '0'){
								var cat = get_category(y);
								var goldrate = get_karatage(y);
								var item = "<select id= id='i_code_"+y+"' style='width:100%'><option>--</option><select>";
							}else{
								var cat = det[i].cat_code+'-'+det[i].cat_des;
								var goldrate = det[i].goldtype;	
								var item = 	det[i].itemcode+'-'+det[i].itemname			
							}

							tbl+="<tr id='ln_14313' bilt='3C'>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left' id='itemCode_"+i+"' name='itemCode_"+i+"'>"+cat+"</td>"; //<input class='input_text_regular_new_pawn' type='text' value='"+det[i].cat_code+"' text='"+det[i].cat_code+"' name='c_code_"+i+"' id='c_code_"+i+"' style='width:100%'>
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='left' ><div id='it_"+y+"'>"+item+"</div></td>"; //<input class='input_text_regular_new_pawn' type='text' value='"+det[i].itemcode+"' text='"+det[i].itemcode+"' name='i_code_"+i+"' id='i_code_"+i+"' style='width:100%'>
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+det[i].condition+"</td>";							
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'><input class='input_text_regular_new_pawn' type='text' value='"+det[i].quality+"' text='"+det[i].quality+"' name='quality"+y+"' id='quality"+y+"' style='width:100%'></td>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'><input class='input_text_regular_new_pawn' type='text' value='"+det[i].qty+"' text='"+det[i].qty+"' name='qty"+y+"' id='qty"+y+"' style='width:100%'></td>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='center'>"+goldrate+"</td>"; //<input class='input_text_regular_new_pawn' type='text' value='"+det[i].goldtype+"' text='"+det[i].goldtype+"' name='goldtype"+i+"' id='goldtype"+i+"' style='width:100%'>
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'><input class='input_text_regular_new_pawn' type='text' value='"+det[i].goldweight+"' 	name='goldweight_"+y+"' id='goldweight_"+y+"' style='width:100%'></td>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'><input class='input_text_regular_new_pawn' type='text' value='"+det[i].pure_weight+"'  name='pureweight"+y+"' id='pureweight"+y+"' style='width:100%'> </td>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='right'><input class='input_text_regular_new_pawn' type='text' value='"+det[i].denci_weight+"' name='denci_weight"+y+"' id='denci_weight"+y+"' style='width:100%'></td>";
							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input  class='input_text_regular_new_pawn' type='text' style='width:150px;border:1px solid green' value='"+det[i].value+"' style='width:100%' id='value"+y+"' name ='value"+y+"'  ></td>";

							tbl+="<td class='text_box_holder_new_pawn bd' width='' align='Left'><input id='tag_"+i+"' class='input_text_regular_new_pawn' type='text' disabled style='width:150px;border:1px solid green' name='tag_"+y+"' value='"+det[i].tag_no+"'></td>";
							
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].itemcode+"' text='"+det[i].itemcode+"' name='i_code_"+i+"' id='i_code_"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].cat_code+"' text='"+det[i].cat_code+"' name='c_code_"+i+"' id='c_code_"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].goldweight+"' text='"+det[i].goldweight+"' name='goldweight_"+i+"' id='goldweight_"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].pure_weight+"' text='"+det[i].pure_weight+"' name='pureweight"+i+"' id='pureweight"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].qty+"' text='"+det[i].qty+"' name='qty"+i+"' id='qty"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].goldtype+"' text='"+det[i].goldtype+"' name='goldtype"+i+"' id='goldtype"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].quality+"' text='"+det[i].quality+"' name='quality"+i+"' id='quality"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].value+"' text='"+det[i].value+"' name='value"+i+"' id='value"+i+"'>"
							//tbl +="<input class='input_text_regular_new_pawn' type='text' value='"+det[i].denci_weight+"' text='"+det[i].denci_weight+"' name='denci_weight"+i+"' id='denci_weight"+i+"'>"
							
							tbl +="<input type='hidden' value='"+det[i].elec_model+"' text='"+det[i].elec_model+"' name='elec_model"+y+"' id='elec_model"+y+"'>"
							tbl +="<input type='hidden' value='"+det[i].elec_imei+"' text='"+det[i].elec_imei+"' name='elec_imei"+y+"' id='elec_imei"+y+"'>"
							tbl +="<input type='hidden' value='"+det[i].elec_serno+"' text='"+det[i].elec_serno+"' name='elec_serno"+y+"' id='elec_serno"+y+"'>"
							tbl +="<input type='hidden' value='"+det[i].elec_description+"' text='"+det[i].elec_description+"' name='elec_description"+y+"' id='elec_description"+y+"'>"		
							tbl +="<input type='hidden' value='"+det[i].veh_model+"' text='"+det[i].veh_model+"' name='veh_model"+y+"' id='veh_model"+y+"'>"
							tbl +="<input type='hidden' value='"+det[i].veh_engine_no+"' text='"+det[i].veh_engine_no+"' name='veh_engine_no"+y+"' id='veh_engine_no"+y+"'>"
							tbl +="<input type='hidden' value='"+det[i].veh_chassis_no+"' text='"+det[i].veh_chassis_no+"' name='veh_chassis_no"+y+"' id='veh_chassis_no"+y+"'>"
							tbl +="<input type='hidden' value='"+det[i].veh_description+"' text='"+det[i].veh_description+"' name='veh_description"+y+"' id='veh_description"+y+"'>"
				};	
		}

	}

	tbl+="</tbody";
	tbl+="</table>";

	$("#item_data").html(tbl);

}

function get_category(ind){
var s = '';
	    $.ajax({
            url: "index.php/main/load_data/t_tag/get_cat",
            type: "POST",                
            data: {ind:ind},
            dataType : 'text',
            async: false,
            success: function(r){
                s =  (r);
				$(document).on('change', '.catg', function(event) {
					var ind = $(this).attr('ind');
					var cat = $(this).val();
					get_item(ind,cat);					
				});
            }            
        });
        return s;
}


function get_item(ind,cat){
var s = '';
	    $.ajax({
            url: "index.php/main/load_data/t_tag/get_item",
            type: "POST",                
            data: {ind:ind , cat:cat},
            dataType : 'text',
            async: false,
            success: function(r){
            		$("#it_"+ind).html('');
					$("#it_"+ind).html(r);
            }
            
        }); 

        return s;
}


function get_karatage(ind){
var s = '';
	    $.ajax({
            url: "index.php/main/load_data/t_tag/get_karatage",
            type: "POST",                
            data: {ind:ind},
            dataType : 'text',
            async: false,
            success: function(r){
                s =  (r);
            }            
        });
        return s;
}
