


$(document).on("click","#btnMark", function(){
    save();
}); 

$(document).on("click","#btnReset", function(){

    document.location = '';
}); 


$(document).ready(function(){


    $(".mark_bill_stat_change").click(function(){

        if (confirm("Do you want cancel this marked bill?")){

            showActionProgress("Loading...");  
            
            $.post("index.php/main/load_data/t_mark_bills/change_mark_status",{            
                no : $(this).attr("no"),
                type : $(this).attr("type"),
                loanno : $(this).attr("loanno"),
                date : $("#dDate").val()
            },function(D){
                closeActionProgress();

                if (D.s == 1){

                    alert("Mark status changed");
                    location.href = '';

                }

            },"json");

        }

    });


    $(".search_key").on("keyup",function(){

        var f = $(this).attr("field");
        var value = $(this).val();
        
        $("#GrDt tr").each(function(index) {
            
            if (index != 0) {

                $row = $(this);

                var id = $row.find("."+f).text();

                if (id.indexOf(value) != 0) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            }
        
        });
        
        
    });


    /*$("#s_billno").on("keyup", function() {
        

    });​*/



    $("#bc").css({
        "border" : "2px solid green",
        "padding" : "4px",
        "border-radius" : "4px"
    });


    $("#bc :selected").text("All Branches");


    $('input').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#no').on('keypress', function (e) {
        if (e.keyCode == 13) {
            load_old_data();
        }
    });

    $("#mb_find").click(function(){ 
        showActionProgress("Loading...");  
        if ($("#billtype").val()=="" || $("#bill_code_desc").val()=="") {
            showMessage("e","Select Bill Type");
        }else{
            grid_data();
        }
        
    }); 

    

    $("#bill_code_desc").keypress(function(e){
        if (e.keyCode == 46 || e.keyCode == 8){ 
            $(this).val('');        
            $("#billtype").val('')
        }
    });
    


    $("#selAll").click(function(){
        if ($("#selAll").is(':checked')) {
            $("input[type='checkbox']").prop('checked','checked');            
        } else{
            $("input[type='checkbox']").removeAttr('checked');
        }; 
    }); 

    $("input[type='checkbox']").click(function(){
        chkCon();
    }); 

    $( "#bill_code_desc" ).autocomplete({
        source: "index.php/main/load_data/m_billtype_det/autocomplete_pawn_add_bill_type",
        select: function (event, ui) {
            var bt = ui.item.value;
            bt = bt.split(" - ");
            $("#billtype").val(bt[0]);
        }
    }); 

    $("input[name='mb_l_P'][type='radio']").click(function(){
        var tval=$(this).val();
        showActionProgress("Loading...");
        $.post("index.php/main/load_data/utility/get_mark_max",{
            tval:tval
        },function(D){     
            $("#no").val(D);
            closeActionProgress();
        },"text");  
    }); 


});

var s = false;

$(window).scroll(function(){
    if ($(this).scrollTop() > 150) {
        $('#ffTb thead tr:first-child').addClass('fixed');


        if (!s){
            var T = "   <tr class='adist_row'><td colspan='10' style='height:60px'>111</td></tr> ";
            $("#GrDt").prepend(T);
            s = true;
        }

    } else {
        $('#ffTb thead tr:first-child').removeClass('fixed');
        $("#GrDt").find(".adist_row").remove();
        s = false;
    }
});


function chkCon(){
    var lngth=$("input[type='checkbox']:checked.slSub").length;
    $("#chkLnth").text(lngth+"/250");
    if (lngth>250) {
            // showActionProgress("");
            // showMessage("e","Max 250");
            $("#MsgAnLnt").addClass("bcred");
            $("#msgMax").css("display","inline-block");  
            $("#btnMark").attr('disabled','true');
            $("#btnMark").removeClass("btn_regular"); 
            $("#btnMark").addClass("btn_regular_disable");           

        } else{
            $("#MsgAnLnt").removeClass("bcred");
            $("#msgMax").css("display","none");                
            $("#btnMark").removeAttr('disabled');   
            $("#btnMark").removeClass("btn_regular_disable"); 
            $("#btnMark").addClass("btn_regular");                                

        };

    }

    function grid_data(){        

        $.post("index.php/main/load_data/t_mark_bills/grid_data",{
            bn : $("#billno").val()
        },function(D){     
            $(".div_bn_det").html(D.data);
            closeActionProgress();
        },"json");  

    }

    function load_old_data(){
        showActionProgress("Loading...");
        var no=$("#no").val();
        var mb_l_P=$("input[name='mb_l_P'][type='radio']:checked").val();
        if ($("input[name='mb_l_P'][type='radio']:checked").length<1){
            $(".pAndL_hold").css('border-color','green')            
            showMessage("e","Select Mark Type");
            return false;
        }else{


            $.post("index.php/main/load_data/t_mark_bills/load_old_data",{
                no:no,mb_l_P:mb_l_P
            },function(D){  
                    // console.log(D);
                // for (var i = D.R.length - 1; i >= 0; i--) {
                //     D.R[i]
                // };
                $("#from_date").val(D.fd);
                $("#to_date").val(D.td);
                $("#bill_code_desc").val(D.bt);
                $("#GrDt").html(D.gd);

                $("#btnMark").prop('disabled', true);
                $("#mb_find").prop('disabled', true);   
                $("#btnMark").removeClass("btn_regular"); 
                $("#btnMark").addClass("btn_regular_disable"); 
                $("#mb_find").removeClass("btn_regular"); 
                $("#mb_find").addClass("btn_regular_disable");     
                $("input").prop('readonly', 'true');   
                $("input[type='checkbox']").prop('disabled', true);
                $("#no").removeAttr('readonly');
                closeActionProgress();
            },"json");  
}
}

function save(){    
    $("#btnMark").prop('disabled', true);

    showActionProgress("Saving...");

    if (validate_form()){

        $.post("index.php/main/load_data/t_mark_bills/mark_bill",{
            
            billno : $("#billno").val(),
            type : $('input[name=mb_l_P]:checked').val()

        },function(D){
            closeActionProgress();

            if (D.s == 1){

                alert("Bill mark success ");

                location.href = '';

            }

        },"json");  

        /*showActionProgress("Loading...");
        
        $.post("index.php/main/load_data/t_mark_bills/mark_bill",{
            billno : billno
        },function(D){
            
            closeActionProgress();

        },"json");*/

        /*var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data:{billno : $("#billno").val() } ,
            dataType: "json",
            success: function (D) {
                if (D.s == 1){
                    
                    showMessage("s","Bills Mark success");

                    //location.href = "";        

                }else{                  
                    showMessage("e",D.m);

                }
            }
        });*/

    }else{

    }
    
    $("#btnMark").removeAttr('disabled');

}

function validate_form(){

    //if ($("input.slSub[type='checkbox']:checked").length<1) {showMessage("e","Select Bills to Mark"); return false;};
    if ($("input[name='mb_l_P'][type='radio']:checked").length<1) {showMessage("e","Select Mark Type"); return false;};
    return true;
}