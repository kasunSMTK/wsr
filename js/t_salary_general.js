var glob_paid_acc = glob_app_load =  "";
var obj_acc_load = "";

$(document).on("keypress",".grid_amount",function(e){
    if (e.keyCode == 13){
        $(this).parent().parent().find('.v_bc').focus();        
    }
});

$(document).on("keypress",".v_bc",function(e){
    if (e.keyCode == 13){
        $(this).parent().parent().find('.v_class').focus();
    }    
});

$(document).on("keypress",".v_class",function(e){
    if (e.keyCode == 13){
        $(".grid_btn[value='Add']").focus();
    }    
});

$(document).on("blur",".grid_amount",function(){
    make_total_val();
});


$(document).on("click",".grid_btn",function(){

    if ($(this).val() == "Add"){
        if (validate_list_items()){
            add_voucher_items_to_list("",$(this));
            $("#to_acc_desc").focus();
        }
    }else{
        if (confirm("Do you want remove this entry?")){
            $(this).parent().parent().remove();
            make_total_val();
        }
    }

});

$(document).on("keypress",".xxx",function(){
    $( ".xxx" ).autocomplete({
        source: "index.php/main/load_data/t_salary_general/get_acc_list",
        select: function (event, ui) {                  
            var d = ui.item.value;
            d = d.split(" - ");      

            $(this).parent().find( "#to_acc_desc" ).val(d[0] + " " +d[1]);
            $(this).parent().find( "#to_acc").val(d[0]);

            $("#to_amount").focus();

        },
        delay : 1000
    });
});

$(document).on("keypress",".xxxx",function(){
    $( ".xxxx" ).autocomplete({
        source: "index.php/main/load_data/t_salary_general/get_acc_list",
        select: function (event, ui) {                  
            var d = ui.item.value;
            d = d.split(" - ");      

            $(this).val(d[0] + " " +d[1]);
            $(this).parent().find( "#to_acc").val(d[0]);

            $("#to_amount").focus();

        },
        delay : 1000
    });
});


function load_vou(s="",d=""){

    if (s == ""){
        acc_code    = $("#from_acc :selected").val();
        nno         = $("#nno").val();        
    }else{
        acc_code = d[2];
        nno = d[0];
    }

    showActionProgress("Please wait...");

    $.post("index.php/main/load_data/t_salary_general/load_vou",{        
        acc_code    : acc_code,
        nno         : nno
    },function(D){        
        closeActionProgress();
        
        if (D.s != 0){
                
                //--------- for approval  view -----------
                    
                    if (s != ""){
                        glob_paid_acc = D.sum.paid_acc;
                        $("#type").val(D.sum.type).change();
                        glob_app_load = true;
                        obj_acc_load = D.det;
                        $("#h_load_vou").val(d[3]);
                        $("#h_load_vou_bc").val(d[4]);
                    }

                //--------- for approval  view -----------

                $("#btnSave").val("Update");
                $("#btnCancel").prop("disabled",false).addClass("btn_regular").removeClass("btn_regular_disable");

                $("#type").val(D.sum.type);
                $("#date").val(D.sum.ddate);
                $("#ref_no").val(D.sum.ref_no);
                //$("#voucher_desc").val(D.sum.note);
                //$("#payee_desc").val(D.sum.payee_desc);
                $("#total_amount").val(D.sum.cash_amount);
                $(".total_vocher").html(D.sum.cash_amount);
                $("#voucher_type").val(D.sum.type);
                $("#hid,#r_nno").val(D.sum.nno);
                $("#r_ddate").val(D.sum.ddate);                
                $("#tot").val(D.sum.cash_amount);

                $("#cheque_no").val(D.chq.cheque_no);
                $("#bank_date").val(D.chq.bank_date);
                $("#r_paid_acc").val(D.sum.paid_acc);
                $("#h_v_bc").val(D.sum.bc);

                $("#nno").val(D.sum.nno);
                

                add_voucher_items_to_list(D,"");

                if (s != ""){
                    
                }

            if (D.sum.st == 'W'){

                $("#btnPrint,#btnCancel").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");            
                $(".div_vou_hin").css("height","0px").html('<div class="div_cou_inner_msg">Voucher approval pending</div>').animate({"height":"60px"});

            }else if (D.sum.st == 'F'){

                if (D.l1_auth == 1 || D.l2_auth == 1){
                    //$("#btnSave,#btnApprove,#btnReject").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");            
                    $("#btnPrint,#btnCancel").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");            
                    $(".div_vou_hin").css("height","0px").html('<div class="div_cou_inner_msg">Forwarded to higher level approval </div>').animate({"height":"60px"});
                }else{
                    $("#btnSave,#btnApprove,#btnReject").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");            
                    $("#btnPrint,#btnCancel").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");            
                    $(".div_vou_hin").css("height","0px").html('<div class="div_cou_inner_msg">Forwarded to higher level approval </div>').animate({"height":"60px"});
                }

            }else if (D.sum.st == 'A'){
                $("#btnPrint").prop("disabled",false).removeClass("btn_regular_disable").addClass("btn_regular");
                $("#btnCancel,#btnSave,#btnApprove,#btnReject,#btnForward").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");            
                $(".div_vou_hin").css("height","0px").html('<div class="div_cou_inner_msg" style="background-color:Green">Approved</div>').animate({"height":"60px"});
            }else if (D.sum.st == 'R' || s != "" ){
                $(".grid_btn,#btnSave,#btnCancel,btnPrint").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");
                $(".div_vou_hin").css("height","0px").html('<div class="div_cou_inner_msg">This voucher was rejected</div>').animate({"height":"60px"});
            }else{
                $("#btnPrint").prop("disabled",true).removeClass("btn_regular").addClass("btn_regular_disable");
                $("#btnCancel").prop("disabled",false).removeClass("btn_regular_disable").addClass("btn_regular");                
                $(".div_vou_hin").css("height","0px").html('<div class="div_cou_inner_msg">Voucher approval pending</div>').animate({"height":"60px"});
            }

        }else{
            alert("Voucher not found");
            
            if (s == ""){
                location.href = "";
            }

        }

    },"json");

}

function save(){

    if (validate_voucher_save()){

        showActionProgress("Saving...");

        var frm = $('#form_'); 

        $.ajax({
            type: frm.attr('method'), 
            url: frm.attr('action'), 
            data: frm.serialize(), 
            dataType: "text", 
            success: function (D) {
                closeActionProgress(); 
                
                if (D == 1 ){
                    alert("Vocuher sent for approval");                                 
                    location.href = "";
                }else if (D == 2 ){
                    alert("Vocuher updated");                                 
                    location.href = "";
                }else{
                    showMessage("e",D); 
                }
                 
            } 
        });

    }

}

function validate_voucher_save(){

    var a = $("#type :selected").val();
    var b = $("#from_acc :selected").val();

    if (a == ""){
        alert("Please select payment type");
        return false;
    }

    if (b == ''){
        alert("Please select paying account");
        return false;
    }

    var n = 0;

    $(".voucher_acc_list_tbody tr").each(function(){
        n++;
    });

    if (n == 1){
        alert("Please select receiving account");
        return false;
    }

    return true;
}

function validate_list_items(){
    
    if ($("#to_acc_desc").val() == ""){
        alert("select account");
        $("#to_acc_desc").focus();
        return false;
    }

    if ($("#to_amount").val() == ""){
        alert("Invalid amount");
        $("#to_amount").focus();
        return false;
    } 

    if ($(".v_bc").val() == ""){
        alert("Select reference branch");
        $(".v_bc").focus();
        return false;
    }

    if ($(".v_class").val() == ""){
        alert("Select reference class");
        $(".v_class").focus();
        return false;
    }

    return true;
}

function reset_voucher_items_to_list(){
    $("#to_acc_desc").val("");
    $("#to_acc").val("");
    $("#to_amount").val("");            
    $(".v_bc").val("");    
    $(".v_class").val("");
}

function add_voucher_items_to_list(D="",Obj){

    if (D == ""){

        var acc_desc    = $("#to_acc_desc").val();
        var acc         = $("#to_acc").val();
        var to_amount   = $("#to_amount").val();    
        
        var v_bc_desc        = $(".v_bc :selected").text();
        var v_bc        = $(".v_bc :selected").val();
        var v_class_desc     = $(".v_class :selected").text();
        var v_class     = $(".v_class :selected").val();

        var T = '<tr> <td><span id="acc_desc">'+acc_desc+'</span><input type="hidden" name="to_acc[]" value="'+acc+'"></td> <td align="right"><span id="to_amount">'+to_amount+'</span><input type="hidden" name="to_amount[]" value="'+to_amount+'"></td> <td>&nbsp;&nbsp;&nbsp;<span id="v_bc_desc">'+v_bc_desc+'</span><input type="hidden" name="v_bc[]" value="'+v_bc+'"></td> <td><span id="v_class_desc">'+v_class_desc+'</span><input type="hidden" name="voucher_class[]" value="'+v_class+'"></td> <td width="81" align="right"><a class="edit_voucher_item">edit</a> | <a class="remove_voucher_item">remove</a></td> </tr>';

        var new_tr = Obj.parent().parent().html();
        $(".voucher_acc_list_tbody").find('.grid_btn').val("Remove");
        $(".voucher_acc_list_tbody").prepend("<tr>"+new_tr+"</tr>");
        $("#to_acc").val("");        

    }else{

        var T = "";

        

        $(".voucher_acc_list_tbody").html("");

        for (n = 0 ; n < D.det.length ; n++){

           //alert(D.det.toSource())

            acc         = D.det[n].acc_code;
            acc_desc    = acc + " - " + D.det[n].acc_desc;
            amount      = D.det[n].amount;
            v_bc        = D.det[n].v_bc;
            v_bc_desc   = D.det[n].bc_name;
            v_class_desc= D.det[n].v_class_desc;
            v_class     = D.det[n].v_class;

            T += '<tr>';
            T += '<td colspan="5" style="border-bottom:1px solid #eaeaea;padding:10px 0px 10px 0px;font-family: bitter;font-size: 22px">' + D.det[n].payee + " - " + v_bc_desc + '</td>';
            T += '</tr>'

        }

        $(".voucher_acc_list_tbody").append(T);
    
    }


    $("#btnReset").removeClass("btn_regular_disable").addClass("btn_regular").prop("disabled",false);
    //$("#nno").prop("disabled",true);

    /*var new_tr = Obj.parent().parent().html();
    $(".voucher_acc_list_tbody").find('.grid_btn').val("Remove");
    $(".voucher_acc_list_tbody").prepend("<tr>"+new_tr+"</tr>");*/

    make_total_val( parseFloat(amount));

    //reset_voucher_items_to_list();

}

function make_editable_dropdown_ref_bc(){

    $(".xxxx").each(function(){
        
        var v_bc=$(this).parent().parent().find('#v_bc_desc').parent().find('input[type="hidden"]').val();
        var v_bc_DD = $('#bc').parent().html();
        $(".xxxx").parent().parent().find('#v_bc_desc').parent().html(v_bc_DD);        
    
    });

}

function make_editable_dropdown_v_class(){

    $(".xxxx").each(function(){
        
        var v_class=$(this).parent().parent().find('#v_class_desc').parent().find('input[type="hidden"]').val();
        var v_class_DD = $('.v_class').parent().html();
        $(".xxxx").parent().parent().find('#v_class_desc').parent().html(v_class_DD);        
    
    });

}


function make_total_val(n){    

    $(".total_vocher").html(n.toFixed(2));
    $("#total_amount").val(n);

}

$(document).on("change","#from_acc",function(){
    set_acc_bal_n_voucher_no();    
});

function set_acc_bal_n_voucher_no(){

    $("#cash_bal").val("");

    if (  $("#type").val() == "cash"  ){
        var acc_code = $("#from_acc :selected").val();
    }

    if (  $("#type").val() == "cheque"  ){
        var acc_code = $("#from_acc :selected").val();
    }

    showActionProgress("Please wait...");

    $.post("index.php/main/load_data/t_salary_general/set_acc_bal_n_voucher_no",{
        acc_code : acc_code
    },function(D){        
        closeActionProgress();

        $("#nno").val(D.data.max_no);
        
        if ($("#type").val() == "cash"){
            $("#acc_bal").val(D.data.cash_bal);
        }

        if ($("#type").val() == "cheque"){
            $("#acc_bal").val(D.data.chqu_bal);
        }

        $("#to_acc_desc").focus();

    },"json");

}

function set_paying_acc(type){

    showActionProgress("Please wait...");

    $.post("index.php/main/load_data/t_salary_general/set_paying_acc",{
        type : type
    },function(D){        
        closeActionProgress();

        $(".paying_acc_td").html(D.acc_drop_down);

        if (type == 'cash'){
            $(".chq_lable").css("color","#cccccc");
            $("#cheque_no,#bank_date").val("").prop("disabled",true);
        }else{
            $(".chq_lable").css("color","#000000");
            $("#cheque_no,#bank_date").prop("disabled",false);
        }

        if (glob_paid_acc != ""){
            $("#from_acc").val(glob_paid_acc);
            glob_paid_acc = "";
        }

        if (glob_app_load){            
            $("#from_acc").html('<option value="'+obj_acc_load[0].paid_acc+'">'+obj_acc_load[0].paid_acc+' - '+obj_acc_load[0].paid_acc_desc+'</option>');
        }

    },"json");

}



function check_for_repeat(v,scid,s){
    
    if (s == 0){ 
        var A = v.children().html();
        var B = $("#h_"+scid).parent().parent().find('.v_bc').val();
        var C = $("#h_"+scid).parent().parent().find('.v_class').val();        
        var D = "h_"+scid;
    }

    if (s == 1){ 
        var A = v.parent().parent().parent().parent().find('.fo').parent().find('input[type=hidden]').val();
        var B = v.parent().parent().parent().find('.v_bc').val();
        var C = v.parent().parent().parent().find('.v_class').val();
        var D = v.parent().parent().parent().parent().html();
    }
    
    alert(D)

    var n = 0;

    $("#tgrid tbody tr").each(function(){

        var AA = $("#h_"+n).val();

        //alert(AA);


        n++;

    });


}

function appr_vou(no,S,paid_acc){

    if (S == 'A'){
        var st = "Confirm approval";
    }

    if (S == 'R'){
        var st = "Confirm reject";
    }

    if (S == 'F'){
        var st = "Do you want forward this voucher for higher level approval ?";
    }

    if (confirm(st)){

        //clearTimeout(Tm);
        
        showActionProgress("Please wait...");
        $.post("index.php/main/load_data/approvals/appr_vou",{
            no : no,        
            record_status : S,
            paid_acc : paid_acc,
            vou_type : 'salary_vouchers'
        },function(D){              

            window.location.href += "#l=general_voucher";
            location.reload();
            
            //window.location.hash = "";
            //window.location.href = window.location.href;
            
        },"json");

    }
    
}

$(document).ready(function(){

    check_for_voucher_view();

    $("#btnForward").click(function(){

        var U = window.location.href;
        U = U.split("&");
        
        var no =  U[1];     
        if (no !=undefined){
            no = no.split("no="); 
            no = no[1];
        }else{
            er = false;
        }        

        var pa =  U[3]; 
        if (pa !=undefined){
            pa = pa.split("paid_acc="); 
            pa = pa[1];
        }else{
            er = false;
        }

        appr_vou(no,'F',pa);
        

    });

    $("#btnApprove").click(function(){

        var U = window.location.href;
        U = U.split("&");
        
        var no =  U[1];     
        if (no !=undefined){
            no = no.split("no="); 
            no = no[1];
        }else{
            er = false;
        }        

        var pa =  U[3]; 
        if (pa !=undefined){
            pa = pa.split("paid_acc="); 
            pa = pa[1];
        }else{
            er = false;
        }

        appr_vou(no,'A',pa);        

    });

    $("#btnReject").click(function(){

        var U = window.location.href;
        U = U.split("&");
        
        var no =  U[1];     
        if (no !=undefined){
            no = no.split("no="); 
            no = no[1];
        }else{
            er = false;
        }        

        var pa =  U[3]; 
        if (pa !=undefined){
            pa = pa.split("paid_acc="); 
            pa = pa[1];
        }else{
            er = false;
        }

        appr_vou(no,'R',pa);        

    });

    $(".load_pending_vou ").change(function(){        
        glob_paid_acc = $(".load_pending_vou :selected").attr("paid_acc");
        $("#type").val($(".load_pending_vou :selected").attr("type")).change();
    });


    $(".btnReset").click(function(){
        if (confirm('Do you want reset this voucher form?')){
            location.href = '';
        }
    });

    $("select").css("border","1px solid Green");
    $(".v_bc,.v_class").css("border","none");

    var voucher_loaded = false;

    $("#nno").keypress(function(e){

        if (e.keyCode == 13){

            if ( $(".paying_acc_td").html() != '<div style="text-align:right;color: #999999">select payment type first</div>' ){

                if ($('#from_acc').val() != "" ){                    
                    if (!voucher_loaded){
                        load_vou();
                        voucher_loaded = true;
                    }else{
                        alert("This vocuher already loaded. to load new vocuher press Reset button first");
                    }
                }else{
                    alert("Please select paid account");
                }

            }else{
                alert("Please select paid account");
            }

        }

    });

    $("#btnSave").click(function(){        
        save();
    });

    
       
            
    
    
    //$(".v_class").removeAttr("name");

    //$(".input_ui_dropdown").attr({"name":"bc_g[]","id":"bd_g[]"});

    //$("#btnSave").click(function(){ if (validate()){ save(); } });

    

    $("#btnSave2").click(function(){
        $("#btnSave").attr("disabled", false);
    });

    //$("#type").val("cheque");

    type = $("#type").val();
    $("#type_hid").val(type);
    
    //get_type();

    if($("#type").val()=="cheque"){
         $(".acc_types").attr("readonly",true);
         $("#cash_acc").addClass("hid_value");
         $("#cash_acc").removeClass("input_txt");
         $("#cash_acc").val("");
         $("#cash_acc_des").val("");
         $("#balance").val("");
    }

    $("#btnReset").click(function(){
        location.href="index.php?action=t_salary_general";
    });

    $("#code").blur(function(){
        check_code();
    });

    $("#type").change(function(){
        
        set_paying_acc($(this).val());

    });

    

    $("#showPayments").click(function(){
      var net_value=parseFloat($("#net").val());
      payment_opt('t_voucher',m_round(net_value));
    
    });
     
    //$("#tgrid").tableScroll({height:300});

    

     $(".fo").keypress(function(e){
        set_cid($(this).attr("id"));
        if(e.keyCode==112){
            $("#pop_search").val($("#0_"+scid).val());
            load_items(scid);
            center("#serch_pop");
            $("#blocker").css("display", "block");
            setTimeout("select_search()", 100);     
        }
        if(e.keyCode==46){
            $("#h_"+scid).val("");
            $("#0_"+scid).val("");
            $("#n_"+scid).val("");
            $("#1_"+scid).val("");
            $("#2_"+scid).val("");
            gross_amount();
        }
        $("#pop_search").keyup(function(e){
                if(e.keyCode != 13 && e.keyCode != 38 && e.keyCode != 40 && e.keyCode != 112 ) { 
                    load_items(scid);
                }
           });
    });

    $("#sales_category").change(function() {
       get_group();
    });

    

    $(".amount").blur(function(){
    
        gross_amount();
        var tot = $("#net").val();

        if($("#type").val() == "cash"){

            $("#cash").val(tot);
        }
        if($("#type").val() == "cheque"){
           
            $("#cheque_issue").val(tot);
        }
        

    });

    $("#id").keypress(function(e){
        if(e.keyCode == 13){
            $(this).blur();
            load_data($(this).val());
        }
    });

    $("#btnDelete").click(function(){
        set_delete();   
    });

    $("#btnPrint").click(function(){
        if($("#hid").val()=="0"){
          set_msg("Please load data before print","error");
          return false;
        }else{
          $("#print_pdf").submit();
        }
    });

    $("#btnShow").click(function(){
        $("#print_vou_list_pdf").submit();        
    });

});

function default_acc(){
    $.post("index.php/main/load_data/t_salary_general/get_default_acc", {
    }, function(r){
        if(r!=2){
            $("#cash_acc").val(r[0].code);
            $("#cash_acc_des").val(r[0].description);

            $.post("index.php/main/load_data/utility/get_account_balance_voucher",{
                code :r[0].code
            },   
            function(rs){
                $("#balance").val(rs); 
                input_active();
            },"json");
       }       
    }, "json");
}



function save_(){

    $("#qno").val($("#id").val());
    $("#voucher_type").val($("#type_hid").val());
    $("#ddate").val($("#date").val());
    $("#acc_code").val($("#cash_acc").val());
    $("#acc_des").val($("#cash_acc_des").val());
    $("#tot").val($("#net").val());
    $('#form_').attr('action',$('#form_id').val()+"t_salary_general");

    var frm = $('#form_');
    console.log($('#form_id').val());
    loding();
    $.ajax({
    type: frm.attr('method'),
    url: frm.attr('action'),
    data: frm.serialize(),
    success: function (pid){
            

            if(pid == 1){
                
                $("#btnSave").attr("disabled",true);
                $("#showPayments").attr("disabled",true);
                
                loding();

                alert("Voucher sent for approval");
                location.href="";

                // sucess_msg();

                /*if(confirm("Save Completed, Do You Want A print?")){                    
                    if($("#is_prnt").val()==1){$("#print_pdf").submit(); }
                    location.href="";
                }else{
                    location.href="";
                }*/

               
            }else if(pid == 2){
                alert("No permission to add data.");
            }else if(pid == 3){
                alert("No permission to edit data.");
            }else{
                alert("Error : \n"+pid);
            }
            
        }
    });
}

function get_data_table(){
    $.post("/index.php/main/load_data/t_salary_general/get_data_table", {
        
    }, function(r){
        $("#grid_body").html(r);
    }, "text");
}


function check_code(){
        loding();
    var code = $("#code").val();
    $.post("index.php/main/load_data/t_salary_general/check_code", {
        code : code
    }, function(res){
        if(res == 1){
            if(confirm("This code ("+code+") already added. \n\n Do you need edit it?")){
                set_edit(code);
            }else{
                $("#code").val('');
        $("#code").attr("readonly", false);
            }
        }
        loding();
    }, "text");
}

function validate(){
    // if($("#code").val() === $("#code").attr('title') || $("#code").val() == ""){
    //     alert("Please enter code.");
    //     $("#code").focus();
    //     return false;
    // }else 
    if($("#description").val() === $("#description").attr('title') || $("#description").val() == ""){
        set_msg("Please enter description.");
        $("#description").focus();
        return false;
    }else if($("#description").val() === $("#code").val()){
        set_msg("Please enter deferent values for description & code.");
        $("#des").focus();
        return false;
    }else if($("#sales_category").val()=="0"){
        set_msg("Please select category");
        return false;        
    }else{
        return true;
    }
}
    
function set_delete(){

    var code = $("#id").val();
    var type = $("#type_hid").val();

    if(confirm("Are you sure do you want cancel "+code+"?")){
        loding();
        $.post("index.php/main/delete/t_salary_general", {
            code : code,
            type2 :type,
             hid :$("#hid_nno").val() 
        }, function(res){
            if(res == 1){
               alert("Voucher cancel success");
               location.href = "";
            }else if(res == 2){
                alert("No permission to delete data.");
            }else{
                alert("Item deleting fail.");
            }
            loding();
        }, "text");
    }
}

function is_edit($mod)
{
    $.post("index.php/main/is_edit/user_permissions/is_edit", {
        module : $mod
        
    }, function(r){
       if(r==1)
           {
             $("#btnSave").removeAttr("disabled", "disabled");
           }
       else{
             $("#btnSave").attr("disabled", "disabled");
       }
       
    }, "json");

}
    
function set_edit(code){
    loding();
    $.post("index.php/main/get_data/t_salary_general", {
        code : code
        
    }, function(res){
        $("#code_").val(res.code);
        $("#code").val(res.code);
    $("#code").attr("readonly", true);
        $("#description").val(res.description);
        
           if(res.is_vehical == 1){
            $("#is_vehical").attr("checked", "checked");
        }else{
            $("#is_vehical").removeAttr("checked");
        }
        
        
        
       // is_edit('010');
        loding(); input_active();
    }, "json");
}

function get_group(){
    $.post("index.php/main/load_data/r_groups/select_by_category", {
        category_id : $("#sales_category").val()
    }, function(r){
       $("#groups").html(r);
    }, "text");

}

function load_data2(){

    $.post("index.php/main/load_data/utility/f1_selection_list", {
        data_tbl:"m_account",
        field:"code",
        field2:"description",
        add_query : "AND control_acc NOT IN ((SELECT `acc_code` FROM `m_default_account` WHERE `code` = 'CREDITOR_CONTROL')  AND ((SELECT `acc_code` FROM `m_default_account` WHERE `code` = 'DEBTOR_CONTROL')))",
        search : $("#pop_search").val() 
    }, function(r){   
        $("#sr").html(r);
        settings();
        
    }, "text");
}

function load_items(scid){
      $.post("index.php/main/load_data/utility/f1_selection_list", {
        data_tbl:"m_account",
        field:"code",
        field2:"description",
        search :$("#pop_search").val()
    }, function(r){
        $("#sr").html(r);
        settings2(scid);        
    }, "text");
}

function select_search(){
    $("#pop_search").focus();
   
}

function settings(){
    $("#item_list .cl").click(function(){        
        $("#cash_acc").val($(this).children().eq(0).html());
        $("#cash_acc_des").val($(this).children().eq(1).html());
        $("#pop_close").click();
        var acc_code = $(this).children().eq(0).html(); 
        $.post("index.php/main/load_data/utility/get_account_balance_voucher",{
            code : acc_code
        },   
        function(rs){
            $("#balance").val(rs); 
            input_active();
        },"json");
                
    });    
}

function settings2(scid){

    $("#item_list .cl").click(function(){
        
        //check_for_repeat($(this),scid,0);

        var v_bc    = $("#h_"+scid).parent().parent().find('.v_bc').val();
        var v_class = $("#h_"+scid).parent().parent().find('.v_class').val();

        if(check_item_exist(        $(this).children().eq(0).html(),v_bc,v_class     )){
            
            $("#0_"+scid).val($(this).children().eq(0).html());
            $("#h_"+scid).val($(this).children().eq(0).html());
            $("#n_"+scid).val($(this).children().eq(1).html());
            $("#pop_close").click();

        }else{
            alert("Account code "+$(this).children().eq(1).html()+" is already added.");
        }




    });    
}



function gross_amount(){

    var gross=loop=0;

    $(".amount").each(function(){
        var gs=parseFloat($("#1_"+loop).val());
        if(!isNaN(gs)){    
        gross=gross+gs;
        }    
        loop++;
    });
    $("#net").val(m_round(gross));
   
}

function empty_grid(){
    for(var i=0; i<25; i++){
        $("#h_"+i).val("");
        $("#0_"+i).val("");
        $("#n_"+i).val("");
        $("#1_"+i).val("");
        $("#2_"+i).val("");
    }
}

function load_data(id){
    empty_grid();
    $.post("index.php/main/load_data/t_salary_general/get_data", {
        id : id,
        type:$("#type").val()
    }, function(r){

        if (r.sum.is_approved == 0){
            alert('This voucher not approved');
            return;
        }

        $("#ref_no").val(r.sum.ref);
        $("#cash_acc").val(r.sum.paid_acc_code);
        $("#cash_acc_des").val(r.sum.paid_acc_des);
        $("#description").val(r.sum.note);
        $("#sales_category").val(r.sum.cat_code);
        $("#groups").val(r.sum.groups_code);
        $("#type").val(r.sum.type);
        $("#date").val(r.sum.ddate); 
        $("#cash").val(r.sum.cash_amount);
        $("#cheque_issue").val(r.sum.cheque_amount);
        $("#net").val(r.sum.tot); 
        $("#hid").val(r.sum.nno);
        $("#hid_nno").val(r.sum.hid_nno);
        $("#hx").val(r.sum.hid_nno);
        $("#p_hid_nno").val(r.sum.hid_nno);
        //set value for hidden fields of pdf report
        $("#qno").val(r.sum.nno);
        $("#voucher_type").val(r.sum.type);
        $("#voucher_no").val(r.sum.nno);
        $("#category_id").val(r.sum.cat_code);
        $("#cat_des").val(r.sum.cat_des);
        $("#group_id").val(r.sum.groups_code);
        $("#group_des").val(r.sum.name);
        $("#ddate").val(r.sum.ddate);
        $("#acc_code").val(r.sum.paid_acc_code);
        $("#acc_des").val(r.sum.paid_acc_des);
        $("#vou_des").val(r.sum.note);
        $("#org_print").val("");

        if(r.sum.is_cancel==1){
             $("#btnDelete").attr("disabled", true);
             $("#showPayments").attr("disabled", true);
             $("#btnSave").attr("disabled", true);
             $("#mframe").css("background-image", "url('img/cancel.png')");
        }

        for(var i=0; i<r.det.length; i++){
            $("#h_"+i).val(r.det[i].acc_code);
            $("#0_"+i).val(r.det[i].acc_code);
            $("#n_"+i).val(r.det[i].description);
            $("#1_"+i).val(r.det[i].amount);
            $("#2_"+i).val(r.det[i].ref_no);
            $("#v_bc_td_"+i).find('.v_bc').val( r.det[i].v_bc );
            $("#v_class_td_"+i).find('.v_class').val( r.det[i].v_class );


            gross_amount();
        }

         if(r.cheque.length>0){
            for(var i=0; i<r.cheque.length; i++){
                $("#bank7_"+i).val(r.cheque[i].bank);
                $("#des7_"+i).val(r.cheque[i].description);
                $("#chqu7_"+i).val(r.cheque[i].cheque_no);
                $("#amount7_"+i).val(r.cheque[i].amount);
                $("#cdate7_"+i).val(r.cheque[i].cheque_date);
            }
        }

        tot = $("#net").val();

        $("#tot").val(tot);
     
        var type = $("#type").val();
        $("#type_hid").val(type);

        $("#type").prop("disabled", true);
        load_payment_option_data($("#hid_nno").val(),"48");

        
    }, "json");
}

function get_type(){

        $.post("index.php/main/load_data/t_salary_general/get_max_no_type", {
              table:'t_voucher_gl_sum',
              nno:'sub_no',
              type:$("#type").val(),
              hid:$("#hid").val(),
          }, function(res){
              $("#id").val(res);
              //empty_grid();
              //empty_all();
          },"text");
}

function check_item_exist(id,v_bc,v_class){
    
    var v = true;
    
    $("input[type='hidden']").each(function(){
        
        if($(this).val() == id){
            v = false;
        }

    });
    
    //return v;
    return true;
}

function check_for_voucher_view(){
    
    var er = true;

    var U = window.location.href;
    U = U.split("&");
    
    var no =  U[1];     
    if (no !=undefined){
        no = no.split("no="); 
        no = no[1];
    }else{
        er = false;
    }

    var st =  U[2]; 
    if (st !=undefined){
        st = st.split("status="); 
        st = st[1];
    }else{
        er = false;
    }

    var pa =  U[3]; 
    if (pa !=undefined){
        pa = pa.split("paid_acc="); 
        pa = pa[1];
    }else{
        er = false;
    }

    var va =  U[4]; 
    if (va !=undefined){
        va = va.split("v_app_no="); 
        va = va[1];
    }else{
        er = false;
    }

    var bc =  U[5]; 
    if (bc !=undefined){
        bc = bc.split("bc="); 
        bc = bc[1];
    }else{
        er = false;
    }
    
    if (er){
        var d = Array(no,st,pa,va,bc);
        load_vou("view_voucher",d);
        $(".div_app_controls_holder").css("display","block");
    }

}