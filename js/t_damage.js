$(document).ready(function() {
  $("#item").keypress(function() {
    $("#item").autocomplete({
      source: "index.php/main/load_data/t_damage/getItemList",
      select: function(event, ui) {
        var bt = ui.item.value;
        bt = bt.split(" - ");
        $("#item_tag").val(bt[0]);
        //$('#cat_code_desc').focus();
        //getCustomerInfo($("#cus_serno").val() , $("#customer_id").val() );
      },
      delay: 1000
    });
  });
  $("#btnSave").click(function() {
    if (validate()) {
      save();
    }
  });
});

function save() {
  showActionProgress("Saving...");

  var frm = $("#form_");
  $.ajax({
    type: frm.attr("method"),
    url: frm.attr("action"),
    data: frm.serialize(),
    dataType: "json",
    success: function(D) {
      // alert("Data inserted");
      if (D > 0) {
        alert("Item Saved Successfuly");

        closeActionProgress();
        document.location.reload(true);
      } else {
        alert("Somthing Wrong Try again!");
      }
    }
  });
}

function validate() {
  var ready = true;
  if ($("#item_tag").val() == "") {
    alert("Item cannot empty!");
    ready = false;
  } else if ($("#damage_narration").val() == "") {
    alert("Narration cannot empty!");
    ready = false;
  }

  return ready;
}
