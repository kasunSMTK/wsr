$(document).ready(function() {
  $("#txtUserName").focus();

  $("#txtUserName,#txtPassword,#txtBranch").keypress(function(e) {
    $("#div_login_msg").html("");
    if (e.keyCode == 13) {
      $("#btnLogin").click();
    }
  });

  $("#btnLogin").click(function() {
    if (validate_login_inputs()) {
      $("#div_login_msg")
        .html("")
        .css("color", "#000000")
        .html('<img src="img/loading.gif"> &nbsp; &nbsp; Please wait...');

      if (typeof $.cookie("COM_ID") === "undefined") {
        com_id_exist = 0;
        com_id = "";
      } else {
        com_id_exist = 1;
        com_id = $.cookie("COM_ID");
      }

      $.post(
        "index.php/main/login/",
        {
          userName: $("#txtUserName").val(),
          userPassword: $("#txtPassword").val(),
          company: $("#txtBranch").val(),
          com_id_exist: com_id_exist,
          com_id: com_id
        },
        function(D) {
          if (D.s == 0) {
            $("#btnLogin").attr("class", "btnLogin");
            $("#div_login_msg")
              .css("color", "Red")
              .html("Invalid username, password or branch code");
          } else if (D.s == 1) {
            window.location = "";
          } else if (D.s == 2) {
            alert("Wrong company informatin");
          } else if (D.s == 3) {
            window.location = "index.php";
          } else if (D.s == 4) {
            $("#btnLogin").attr("class", "btnLogin");
            $("#div_login_msg")
              .css("color", "Red")
              .html("Access denied");
          } else {
            //alert("Unknown error");
            $("#div_login_msg")
              .css("color", "Red")
              .html("Access denied");
          }
        },
        "json"
      );
    }
  });
});

function validate_login_inputs() {
  var U = $("#txtUserName").val();
  var P = $("#txtPassword").val();
  var B = $("#txtBranch").val();

  if (U == "") {
    $("#div_login_msg").html("Enter username");
    $("#txtUserName").focus();
    return;
  }

  if (P == "") {
    $("#div_login_msg").html("Enter password");
    $("#txtPassword").focus();
    return;
  }

  if (B == "") {
    $("#div_login_msg").html("Enter branch code");
    $("#txtBranch").focus();
    return;
  }

  $("#btnLogin")
    .attr("class", "btn_login_regular_disable")
    .focus();

  return true;
}
