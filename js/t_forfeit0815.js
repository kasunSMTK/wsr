$(document).ready(function(){

    $("#btnSearch").click(function(){
        grid_data();                
    });

    $("#no").keypress(function(e) {
        if (e.keyCode == 13){

            showActionProgress("Loading...");
                
            $.post("index.php/main/get_data/t_forfeit",{
                no : $(this).val()
            },function(D){                         
                closeActionProgress();

                if (D.s == 1){
                    $("#GrDt").html(D.f_data);
                    disable_controls();
                }else{
                    showActionProgress("");
                    showMessage("e","Foreited data not found");

                    setTimeout('$("#btnReset").click()',2000)
                }

            },"json");

        }
    });

    $("#bc").change(function(){

        if( $(this).val() == "" ){

            opt = '<option value="">All</option>';
            $("#r_billtype").html(opt);

        }else{
            showActionProgress("Loading bill types...");

            $.post("index.php/main/load_data/m_billtype_det/load_bc_billtypes",{        
                bc    : $("#bc :selected").val()

            },function(D){        
                closeActionProgress();
                var opt = '';
                
                if (D.bt_det.length > 0){
                    opt += '<option value="">All</option>';
                    for(n = 0 ; n < D.bt_det.length ; n++){
                        opt += '<option value="'+D.bt_det[n].billtype+'">'+D.bt_det[n].billtype+'</option>';
                    }
                }else{
                    opt += '<option value="">No bill type found</option>';
                }
                
                $("#r_billtype").html(opt);

                grid_data();

            },"json");
        }

    });

    $("#btnForfiet").click(function(){     
        save();
    }); 

    $("#selAll").click(function(){
        if ($("#selAll").is(':checked')) {
            $("input[type='checkbox']").prop('checked','checked');            
        } else{
            $("input[type='checkbox']").removeAttr('checked');
        }; 
    }); 

    $("input[type='checkbox']").click(function(){

        var lngth=$("input[type='checkbox']:checked.slSub").length;
        $("#chkLnth").text(lngth+"/250");

        if (lngth>250) {
            // showActionProgress("");
            // showMessage("e","Max 250");
            $("#MsgAnLnt").addClass("bcred");
            $("#msgMax").css("display","inline-block");  
            $("#btnForfiet").attr('disabled','true');
            $("#btnForfiet").removeClass("btn_regular"); 
            $("#btnForfiet").addClass("btn_regular_disable");           

        } else{
            $("#MsgAnLnt").removeClass("bcred");
            $("#msgMax").css("display","none");                
            $("#btnForfiet").removeAttr('disabled');   
            $("#btnForfiet").removeClass("btn_regular_disable"); 
            $("#btnForfiet").addClass("btn_regular");                                

        };
    }); 


    $("#btnReset").click(function(){
        location.href = "";
    });


});

$(window).scroll(function(){
    if ($(this).scrollTop() > 175) {
        $('#ffTb thead tr').addClass('fixed');
    } else {
        $('#ffTb thead tr').removeClass('fixed');
    }
});

function disable_controls(){
    $("#btnForfiet").attr({
        disabled: true,
        class: 'btn_regular_disable'
    });;

    $("#btnReset").attr({
        disabled: false,
        class: 'btn_regular'
    });

}

function SlalMn(ss){
    if(ss.is(':checked')){
        $(".sel_"+ss.val()).prop('checked','checked');
    }else{
        $(".sel_"+ss.val()).removeAttr('checked');        
    }
}

function grid_data(){   

    showActionProgress("Please wait...");

    var frm = $('#form_2'); 

    $.ajax({
        type: frm.attr('method'), 
        url: frm.attr('action'), 
        data: frm.serialize(), 
        dataType: "json", 
        success: function (D) {
            closeActionProgress();            
            
            $("#GrDt").html(D.rows);
        } 
    });

}


function save(){    
    $("#btnForfiet").prop('disabled', true);

    var selBel=[];

    showActionProgress("Saving...");

    $(".slSub:checked").each(function(index){
        selBel[index]=$(this).closest('tr').attr("BilT");
    });



    $(".slSub").each(function(){
        if ( !$(this).is(":checked") ){
            $(this).parent().parent().remove();
        }
    });


    if (validate_form()){

        var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize()+"&ddate="+$("#ddate").val()+"&bc="+$("#bc :selected").val(),
            dataType: "json",
            success: function (D) {
                if (D.s == 1){
                    showMessage("s","Forfeit success");

                    for (var i = D.lnNoRm.length - 1; i >= 0; i--) {
                        $("#ln_"+D.lnNoRm[i]).remove();
                        var con=0;
                        var rmmnrw=selBel[i];
                        $("input[type='checkbox'].sel_"+rmmnrw).each(function(){
                            con++;
                        })

                        if (con==0) {
                            $(".SlalMn.bt_"+rmmnrw).remove();
                        };
                if($("input[type='checkbox'].slSub").length<1){
                    $("#GrDt").html("<tr><td colspan='11' class='text_box_holder_new_pawn bd' align='center'><h1>No data found</h1></td></tr>");
                } 

                    };
     

                $("input[type='checkbox']").removeAttr('checked');
                $("#btnForfiet").removeAttr('disabled');                    
                $("#chkLnth").text("0/250");                    
               
                $("#no").val(D.max_no);

                }else{                  
                    showMessage("e",D.m);
                    $("#btnForfiet").removeAttr('disabled');
                }
            
                alert(" Forfeited success ");

                $('#btnSearch').click();

            }
        });

}else{
    showMessage("e","Select Items to Forfeit");    
}
    $("#btnForfiet").removeAttr('disabled');
}

function validate_form(){
    if ($("input.slSub[type='checkbox']:checked").length<1) {return false;};
    return true;
}