var redeem_trans_no = "";
var last_max_no = "";
var current_date = "";
var pp_data = "";
var paying_amount_PP = 0;
var balance_amount_PP = 0;
var cgv_PP = 0;
var op_toggle = false;
var pblamt = 0;
var pblamt_constant = 0;



$(document).on("click",".pp_lnk_remove",function(){


	if (confirm("Confirm remove this transaction?")){

		showActionProgress("Please wait...");

		$.post("index.php/main/load_data/t_part_payment/remove_pp_transaction",{
			
			tcode : $(this).attr('tcode'),
			tno : $(this).attr('tno'),			
			bc : $(this).attr('bc'),
			bn : $("#bc_no").val()+$("#billno").val(),
			lst_pd_date_whn_update : $(this).attr("lst_pd_date_whn_update")
			
		},function(D){
			
			closeActionProgress();

			if (D.s == 1){

				alert("Transaction removed");

				LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());

			}else{

				alert("error, transaction not removed");
				
			}

		},"json");

	}

});



$(document).ready(function(){

	$("#billno").focus();

	//---------------------- bc_no append --------------------------------

		$(".clear_bc_no").click(function(){
			$(".txt_bc_no").removeAttr("readonly").val("").focus();
		});

		$(".txt_bc_no").keyup(function(e){		
			if ($(this).val().length == 3){
				$("#billno").focus();
			}
		});

	//---------------------- bc_no append end ---------------------------

	$("#paying_amount").click(function(){
		//$("#btnPaymentOptions").click();
	});

	function set_val_1(obj){		

		if ( parseFloat(obj.val()) > parseFloat($("#customer_advance").val()) ){
			obj.val(parseFloat($("#customer_advance").val())); 
		} 

		discount = 0;
		
		var pa = (pblamt - obj.val());

	}

	function set_val_2(obj){

		// call total cal function here
	}

	$("#customer_advance_paying").change(function(){set_val_1($(this)); calPayAmount(); });
	
	$("#customer_advance_paying").keyup(function(){
		set_val_1($(this)); calPayAmount();
	});
	

	$("#card_amount").change(function(){set_val_2($(this));calPayAmount(); });
	$("#card_amount").keyup(function(){set_val_2($(this));calPayAmount(); });
	

	$("#cash_amount").change(function(){calPayAmount(); });
	$("#cash_amount").keyup(function(){calPayAmount(); });

	$(".chk_pay_opt_apply").click(function(){
		
		if ($(this).is(":checked")){			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "80px"},'fast');			
			$(this).parent().parent().css("background","#f9f9f9");
		}else{			
			$(this).parent().parent().find('.pay-penal-sub').animate({height: "0px"},'fast');
			$(this).parent().parent().css("background","#ffffff");
		}
	});

	$(".pay-close").click(function() {
		$("#btnPaymentOptions").click();		
	});


	$("#btnPaymentOptions").click(function() {

		if (!op_toggle){
			$(".op_holder").animate({height: "400px"},'slow');
			op_toggle = true;
			 $("html, body").animate({ scrollTop: $(document).height() }, 'slow');
		}else{
			$(".op_holder").animate({height: "0px"},'slow');
			op_toggle = false;
			  $("html, body").animate({ scrollTop: 0 }, "slow");
		}

	});


	$("#bill_code_desc").focus();

	$("#transaction_type").change(function(){
		if( $(this).val() == "rn" ){
			$(".tr_action_text").html("Extra Required Amount");
		}else{
			$(".tr_action_text").html("Paying Amount");
		}
	});

	$(document).on('click', '#btnProceedRenew', function(){
		ProceedRenew();
	});

	$(document).on('click', '#btnPayOnlyInt', function(){
		pay_only_interest();
	});

	$(document).on('click', '#btnPPcancel', function(){
		resetFormData();
		closePartPayMessage();
		$("#bill_code_desc").focus();
	});

	$(document).on('click', '.show_pawn_articles_div', function(){		
		$(this).hide();
		$("#div_itm_contain").animate({height: $("#div_show_items").css("height") });		
	});	

	$(".cal_5").click(function(){
		
		if ($("#billtype").val() == ""){
			$("#bill_code_desc").focus();
			return;
		}
		
		if ($("#billno").val() == "" ){			
			$("#billno").focus();
			return;
		} 

		LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());	

		$("#no_of_int_cal_days").val( ( parseFloat($("#no_of_days").val()) % 30) - 1 );
		$(".int_chrd_days").html(  ( parseFloat($("#no_of_days").val()) % 30) - 1 );
		$(".msg_int_cal_days").css("display","block");

	});
	
	$( "#bill_code_desc" ).autocomplete({
		source: "index.php/main/load_data/m_billtype_det/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#billtype").val(bt[0]);				
				$("#billno").val("").focus();
		}
	});	

	$("#billno").keypress(function(e){
		if (e.keyCode == 13){
			LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());
		}
	});	

	$("#btnSave").click(function(){
		save();
	});

	$("#btnPrint").click(function(e){
		$("#print_pdf").submit();
	});

	$("#paying_amount").keypress(function(e){
		if (e.keyCode == 13){
			$("#btnSave").focus();
		}
	});

	$("#btnReset").click(function(){
		redeem_trans_no = $("#no").val();
		current_date = $("#ddate").val();
		resetFormData();
		$("#no").val(redeem_trans_no);
		$("#btnSave").attr("disabled",false).attr("class","btn_regular");
		$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
		$(".pawn_msg").html("").fadeOut();
		$("#bill_code_desc").focus();
		$("#no").val(last_max_no);
		$("#btnPrint").attr("disabled",true).attr("class","btn_regular_disable");		
		$("#pp_list").html("");
		$("#paying_amount").attr("disabled",false)
	});

	$("#no").keypress(function(e){
		if (e.keyCode == 13){
			load_transaction($(this).val());
		}
	});

	$("#manual_bill_no").keypress(function(e){

		if (e.keyCode == 13){

			showActionProgress("Please wait...");

			$.post("index.php/main/load_data/t_new_pawn/get_system_billno",{
				manual_bill_no : $("#manual_bill_no").val(),
				cut_bc_no : 1
			},function(D){
				
				closeActionProgress();

				if (D.s == 1){

					$("#billno").val(D.billno);
					LOAD_LOAN($("#billtype").val(),$("#billno").val(),$("#ddate").val());	

				}else{
					alert("Invalid manual bill number ");
				}

			},"json");

		}

	});


});


function enable_pay_option_inputs(){
	$("#customer_advance_paying,#card_amount,#cash_amount").removeAttr("readonly");
}

function disable_pay_option_inputs(){
	$("#customer_advance_paying,#card_amount,#cash_amount").val("");
	$("#customer_advance_paying,#card_amount,#cash_amount").attr("readonly","readonly");
}


function calPayAmount(){

	if ($('#billtype').val() == ""){
		return;
	}
	
	discount = 0;
	pblamt = pblamt_constant;
	pblamt -= discount;
	var pa = pblamt;

	var customer_advance = isNaN(parseFloat($("#customer_advance").val())) ? 0 : parseFloat($("#customer_advance").val());
	
	var customer_advance_paying = isNaN(parseFloat($("#customer_advance_paying").val())) ? 0 : parseFloat($("#customer_advance_paying").val());	
	var card_amount = isNaN(parseFloat($("#card_amount").val())) ? 0 : parseFloat($("#card_amount").val());
	var cash_amount = isNaN(parseFloat($("#cash_amount").val())) ? 0 : parseFloat($("#cash_amount").val());
	var n = 0;
		
		n += customer_advance_paying;
		n += card_amount;
		n += cash_amount;

	$("#paying_amount").val( n.toFixed(2) );
}


function pay_only_interest(){
	showActionProgress("Please wait...");
	$.post("index.php/main/load_data/t_part_payment/pay_only_interest",{		
		data : pp_data,
		paying_amount : paying_amount_PP,
		balance_amount : balance_amount_PP
	},function(D){		
		closeActionProgress();
		
		if (D.s == 1){
			showMessage("s","Part payment sucess");
			current_date = $("#ddate").val();
			resetFormData();
			$("#no").val(D.max_no);
			$("#bill_code_desc").focus();
			
			$("#r_no").val(D.no + "~~~" + D.loanno);
			
			$("#by").val("partpay_ticket");				
			$("#print_pdf").submit();
			$("#ddate").val(current_date);

			closePartPayMessage();
		}

	},"json");	
}



function load_transaction(tr_no){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_part_payment/load_transaction",{
		tr_no : tr_no
	},function(D){

		closeActionProgress();

		if(D.s == 1){			

		}else{			
			
						
		}		
		
	},"json");
}

function resetFormData(){
	setCurrent_date();	
	op_toggle = true;
	$("#btnPaymentOptions").click();
	disable_pay_option_inputs();
	ddate = $("#ddate").val();
	bc_no = $("#bc_no").val();
	$("input:text").val("");
	$("#ddate").val(ddate);	
	$("#bc_no").val(bc_no);
	$("input:hidden").val("");	
	$("#cus_info_div").css("height","0").html("");
	$("#billtype_info_div").html("");
	$("#pp_list").html("");
	$("#billno,#bill_code_desc").removeAttr("readonly");

	$(".int_chrd_days").html( "" );
	$(".msg_int_cal_days").css("display","none");
	$("#billno").focus();
	$("#is_non_gold").val(0);
	$("#cat_code").val("");
}

function setCurrent_date(){
	showActionProgress("Loading...");
	$.post("index.php/main/load_data/utility/setCurrent_date",{
	},function(D){		
		closeActionProgress();
		$("#ddate").val(D.current_date);		
	},"json");	
}

function save(){

	if (validate_form()){

		showActionProgress("Please wait...");
		
		$.post("index.php/main/save/t_part_payment",{

			loanno 		: $("#hid_loan_no").val(),
			customer_id : $("#hid_customer_id").val(),
			billtype 	: $("#billtype").val(),
			billno 		: $("#billno").val(),
			ddate 		: $("#ddate").val(),
			amount 		: $("#paying_amount").val(),
			loan_amount : $("#loan_amount").val(),
			five_days_int_cal : $(".cal_5").is(":checked") ? 1 : 0,

			customer_advance : $("#customer_advance").val(),
			customer_advance_paying : $("#customer_advance_paying").val(),
			card_number : $("#card_number").val(),
			card_amount : $("#card_amount").val(),
			cash_amount : $("#cash_amount").val(),
			hid_customer_serno : $("#hid_customer_serno").val(),
			bc_no 		: $("#bc_no").val(),
			int_paid_untill: $("#int_paid_untill").val(),
			is_weelky_int_cal: $("#is_weelky_int_cal").val(),
			no_of_days	: $("#no_of_days").val(),
			fmintrest : $("#fmintrest").val(),
			paid_interest : $("#paid_interest").val(),
			num_of_months : $("#num_of_months").val(),
			dDate 		: $("#dDate").val(),
			no_of_int_cal_days : $("#no_of_int_cal_days").val(),
			is_renew_bill : $("#is_renew_bill").val(),
			fm_int_paid : $("#fm_int_paid").val(),
			is_amount_base_int_cal : $("#is_amount_base_int_cal").val(),
			ori_pwn_date : $("#ori_pwn_date").val(),
			transaction_type : $("#transaction_type").val(),
			is_non_gold : $("#is_non_gold").val(),
			cat_code: $("#cat_code").val(),
			goldvalue: $("#goldvalue").val()


		},function(D){			
			
			closeActionProgress();

			if(D.s == 1){
				alert("Save success");
				location.href = '';
			}else if(D.s == 378){

				alert(D.msg_33);

			}else{

			}

		},"json");
	}

}

function LOAD_LOAN(billtype,billno,dDate){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_part_payment/LOAD_LOAN",{
		billtype : billtype,
		billno : billno,
		dDate : dDate,
		five_days_int_cal : $(".cal_5").is(":checked") ? 1 : 0,
		bc_no : $("#bc_no").val()
	},function(D){

		closeActionProgress();

		if (D.s == 1){

			if (D.loan_sum.status == "R"){
				$(".pawn_msg").html("This loan already redeemed").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if (D.loan_sum.status == "F"){
				$(".pawn_msg").html("This loan already forfeited").fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else if( D.loan_sum.status == "RN" || D.loan_sum.status == "AM" ){
				$(".pawn_msg").html("This bill was renewed to bill number " + D.loan_sum.old_o_new_billno ).fadeIn();
				$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnReset").attr("disabled",false).attr("class","btn_regular");
				return;
			}else{
				$(".pawn_msg").html("").fadeOut();
				$("#btnSave").attr("disabled",false).attr("class","btn_regular");
				$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
			}

			$("#billtype,#bill_code_desc").val(D.loan_sum.billtype);
			$("#hid_loan_no").val(D.loan_sum.loanno);
			$("#hid_customer_id").val(D.loan_sum.customer_id);
			$("#hid_customer_serno").val(D.loan_sum.cus_serno);
			
			$("#dDate").val(D.loan_sum.ddate);
			$("#time").val(D.loan_sum.time);
			$("#period").val(D.loan_sum.period);
			$("#finaldate").val(D.loan_sum.finaldate);			
			$("#loan_amount").val(D.loan_sum.requiredamount);			
			$("#fmintrate").val(D.loan_sum.fmintrate);
			$("#nm_rate_for_a_month").val(D.int.nm_rate_for_a_month);
			$("#fm_int_paid").val(D.loan_sum.fm_int_paid);
			$("#is_amount_base_int_cal").val(D.loan_sum.is_amt_base);
			$("#ori_pwn_date").val(D.loan_sum.ori_pwn_date);
			$("#is_non_gold").val(D.loan_sum.is_non_gold);
			$("#cat_code").val(D.loan_sum.cat_code);


			if ( D.loan_sum.is_non_gold == 1 && (D.loan_sum.cat_code == 15 || D.loan_sum.cat_code == 17) ){
				$("#transaction_type").html('<option value="pp">Part Payment</option> <option value="rn">Renew</option>');
			}else{
				$("#transaction_type").html('<option value="pp">Part Payment</option>');
			}


			// calculations from php
			
			if (D.loan_sum.is_renew == 1){
				$("#no_of_days").val(D.int.no_of_days + "     +" + D.loan_sum.old_bill_age );
			}else{
				$("#no_of_days").val(D.int.no_of_days);
			}



			$("#interest").val(D.int.total_int);
			$("#paid_interest").val(D.int.paid_int.toFixed(2)); 
			//alert(D.customer_advance.balance)
			$("#customer_advance").val(D.customer_advance.balance);
			
			var a = D.int.payable_tot_int_balance;
			var bal = a.toString().substring(0, a.toString().indexOf(".") + 3);
			$("#balance").val(bal);


			$("#goldvalue").val(D.loan_sum.goldvalue);

			op_toggle = true;
			$("#btnPaymentOptions").click();
			disable_pay_option_inputs();
			enable_pay_option_inputs();

			// partpay
			var T = ta_type = "";

			if (D.partpay_histomer.length > 0){				

				var num2 = D.partpay_histomer.length;

				for(num = 0 ; num < D.partpay_histomer.length ; num++){

					var show = false;

					if (D.partpay_histomer[num].transecode == "PP"){
						ta_type = "Interest payment";
						show = true;
						nVal = 1;
					}else if(	D.partpay_histomer[num].transecode == "P" && 
								D.partpay_histomer[num].billtypeno == "0" &&
								(D.partpay_histomer[num].amount) > 0  ){
						ta_type = "Capital Increase";
						show = true;
						nVal = 1;
					
					}else if(	D.partpay_histomer[num].transecode == "P" && 
								D.partpay_histomer[num].billtypeno == "0" &&
								(D.partpay_histomer[num].amount) < 0  ){
						ta_type = "Capital Payment";
						show = true;
						nVal = 1;
					}
					

					if (show){

						T += "<tr>";
						T += "<td>"+(num2--)+"</td>";
						T += "<td>"+D.partpay_histomer[num].trans_datetime+"</td>";					
						T += "<td>"+ta_type+"</td>";
						T += "<td align='right'>"+((D.partpay_histomer[num].amount) * nVal).toFixed(2)+"</td>";
						T += "<td align='center' style='border-right:none'> <a class='pp_lnk_remove' tcode='"+D.partpay_histomer[num].model_trans_code+"' tno='"+D.partpay_histomer[num].transeno+"' tbno='"+D.partpay_histomer[num].billtypeno+"' bc='"+D.partpay_histomer[num].bc+"' lst_pd_date_whn_update='"+D.partpay_histomer[num].lst_pd_date_whn_update+"' >remove</a> </td>";
						T += "</tr>";

					}


				}

			}else{
				T += '<tr><td colspan="5"><span class="text_box_title_holder_new_pawn" style="width:100%">No interest payments found</span></td></tr>';
			}			

			$("#pp_list_inner").html("").html(T);
			$("#paying_amount").focus();
			$("#cus_info_div").css({height: "42px"}).html(D.cus_info).animate({height: "398px"});
			$("#billtype_info_div").html(D.rec);
			$("#int_paid_untill").val(D.loan_sum.int_paid_untill);
			$("#is_weelky_int_cal").val(D.loan_sum.is_weelky_int_cal);			
			$(".paid_upto").html("Interest paid up to " + D.loan_sum.int_paid_untill);
			$("#fmintrest").val(D.loan_sum.fmintrest);
			$("#num_of_months").val(D.int.num_of_months);		


			
			console.log("int_paid_untill = " + D.loan_sum.int_paid_untill);
			console.log("capital bal = " + D.int.capital_bal);
			
			console.log("paid int = " + D.int.paid_int);
			console.log("undue payable int = " + D.int.undue_payable_int);
			console.log("ib = " + D.int.ib);
			console.log("payable tot int balance = " + D.int.payable_tot_int_balance);
			
			//console.log("no of months to int cal = " + D.int.no_of_months_to_int_cal);
			console.log("int rate = " + D.int.int_rate);

			console.log("-----------------------------------------------------------------------");

			console.log("no_of_full_months = " + D.int.no_of_full_months);
			console.log("days_remain = " + D.int.days_remain);
			console.log("noof_months = " + D.int.noof_months);
			console.log("fm_deduct_int = " + D.int.fm_deduct_int);
			console.log("monthly_int = " + D.int.monthly_int);

		}else{
			showMessage("e","Invalid bill number");
			$(".pawn_msg").html("").fadeOut();
			$("#cus_info_div").html("");
			$("#btnSave").attr("disabled",false).attr("class","btn_regular");
			$("#btnReset").attr("disabled",true).attr("class","btn_regular_disable");
			$("#btnReset").click();
		}
		
		
	},"json");
}

function getPayableAmount(){
	if (!validateNumber($("#discount").val()) || $("#discount").val() == "" ){				
		discount = 0;
		$("#discount").val("")
	}else{
		discount = parseFloat( $("#discount").val() );
	}
	var paying_amount  = parseFloat( $("#loan_amount").val() ) + parseFloat( $("#balance").val() ) - discount;
	return paying_amount.toFixed(2);
}

function validate_form(){
	if ( rmsps($("#bill_code_desc").val()) ==  "" || rmsps($("#billtype").val()) ==  "" ){ showMessage("e","Select bill type"); $("#bill_code_desc").val("").focus(); return false; }
	if ( rmsps($("#billno").val()) ==  "" || !validateNumber($("#billno").val()) ){ showMessage("e","Select bill number"); $("#billno").val("").focus(); return false; }
	if (!validateAmount($("#paying_amount").val() ," paying amount")){ $("#paying_amount").val("").focus(); return false; }

	var loan_amount 	= parseFloat($("#loan_amount").val());
	var balance     	= parseFloat($("#balance").val());
	var customer_advance= parseFloat($("#customer_advance").val());
	var paying_amount 	= parseFloat( $("#paying_amount").val() );
	var nm_rate_for_a_month = parseFloat( $("#nm_rate_for_a_month").val() ); // alert(parseFloat( $("#nm_rate_for_a_month").val() ))

	if (customer_advance > 0){
		if ((paying_amount+customer_advance) > (loan_amount+balance)){
			showMessage("e","Please use redeem window to redeem this pawn");			
			return;	
		}		
	}else{
		if (paying_amount > (loan_amount+balance) || paying_amount <= 0){
			showMessage("e","Invalid paying amount");
			$("#paying_amount").val("").focus();
			return;
		}
	}
	
	var nm_rate_for_a_month = toFixedTrunc(nm_rate_for_a_month,2);

	if (balance > 0){

		if ( paying_amount < nm_rate_for_a_month ){
			alert("Invalid paying amount. paying amount must equal or grater than "+ nm_rate_for_a_month + " (An one month interest portion for this bill)");
			$("#paying_amount").focus().select();
			return;
		}else{

			// Use a validator here if restrict payable interest only for month value. use 'nm_rate_for_a_month' var

		}		

	}

	return true;	
}

function toFixedTrunc(value, n) {
  const v = value.toString().split('.');
  if (n <= 0) return v[0];
  let f = v[1] || '';
  if (f.length > n) return `${v[0]}.${f.substr(0,n)}`;
  while (f.length < n) f += '0';
  return `${v[0]}.${f}`
}

function setEdit(recid){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_new_pawn/set_edit",{
		recid : recid
	},function(D){		
		
		closeActionProgress();
	},"json");	

}
