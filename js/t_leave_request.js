var names = [];

$(document).ready(function(){

	$("#btnSave").click(function(){		
		save();
	});	

	$("#ref_no").keypress(function (e) {
		if (e.keyCode == 13) {
			load_data($(this).val());
		}
	});

	$("#btnDelete1").click(function () {
		if ($("#hid").val() == "1")
		{
			set_delete();
		}
		else
		{
			alert("Please load a record");
		}
	});

	$( "#branch_code" ).autocomplete({
		source:"index.php/main/load_data/t_leave_request/get_branch",
		select: function (event, ui) {
			var bt = ui.item.value;
			bt = bt.split(" - ");
			$('#branch_code').val(bt[0]);
			$('#branch_des').val(bt[1]);
			event.preventDefault();		}
		});
	$( "#emp_code" ).autocomplete({
		source:"index.php/main/load_data/t_leave_request/get_employee",
		select: function (event, ui) {
			var bt = ui.item.value;
			bt = bt.split(" - ");
			$('#emp_code').val(bt[0]);
			$('#emp_des').val(bt[1]);
			event.preventDefault();		}
		});

});

function save(){	

	showActionProgress("Updating...");

	if (validate_form()){

		var frm 		= $('#form_');
		var fd 			= new FormData();
		

		
		var other_data = $('form').serializeArray();	    

		$.each(other_data,function(key,input){
			fd.append(input.name,input.value);
		});	    

		$.ajax({
			url:  frm.attr('action'),
			data: fd,
			contentType: false,
			processData: false,
			type: 'POST',
			success: function(D){

				closeActionProgress();

				if (D == 1){
					showMessage("s","Saved success");	
					location.href="";									
				}else{					
					showMessage("e","Error, save failed");
				}
			}
		});


	}

}

function validate_form(){	
	
	if ($("#branch_code").val() == ""){showMessage("e","Enter Branch");	return false; }
	if ($("#emp_code").val() == ""){showMessage("e","Enter Employee");	return false; }
	if ($("#f_date").val() == ""){showMessage("e","Enter From Date");	return false; }
	if ($("#t_date").val() == ""){showMessage("e","Enter To Date");	return false; }
	if ($("#leave_type").val() == ""){showMessage("e","Select Leave Type");	return false; }
	if ($("#reason").val() == ""){showMessage("e","Enter Reason");	return false; }
	if($("input[type='radio']:checked").length==0){showMessage("e","Select a leave");return false;}


	return true;	
}

function load_data(id) {
	empty_grid();
	$.post("index.php/main/get_data/t_leave_request/", {
		id: id
	}, function (r) {

		if (r.sum[0] != undefined) {
			$('#hid').val(1);
			$("#ref_no").val(r.sum[0].ref_no);
			$("#ddate").val(r.sum[0].ddate);
			$("#status").val(r.sum[0].status);
			$('#branch_code').val(r.sum[0].branch_code);
			$("#branch_des").val(r.sum[0].b_name);
			$("#emp_code").val(r.sum[0].emp_code);
			$("#emp_des").val(r.sum[0].nName);
			$("#from_date").val(r.sum[0].from_date);
			$("#to_date").val(r.sum[0].to_date);
			$("#leave_type").val(r.sum[0].leave_type);
			$("#reason").val(r.sum[0].reason);

			if(r.sum[0].leave=="SL"){
				$("#sh_leave").attr("checked","checked");	
			}
			if(r.sum[0].leave=="HD"){
				$("#h_day").attr("checked","checked");	
			}
			if(r.sum[0].leave=="FD"){
				$("#f_day").attr("checked","checked");	
			}

		} else {
			alert("No records");
		}
	}, "json");
}

function empty_grid(){
	$('input[type=text],textarea').val("");

}

function set_delete() {
	var id = $("#hid").val();
	if (id != 0) {
		if (confirm("Are you sure ? ")) {
			$.post("index.php/main/delete/t_leave_request", {
				id: $('#ref_no').val()
			}, function (r) {
				if (r != 1) {
					set_msg(r, "error");
				} else {
					delete_msg();

				}
			}, "text");
		}
	} else {
		alert("Please load record");
	}
}



