var names = [];
var bcCustomer = [];
var gold_q = [];
var currentDate = "";
var ra_normal = 0;
var int_normal = 0;
var int_cal_changed = false;
var int_set = false;
var move_request_to_ho = false;

function setBulkItemsShow(bulk_items){	
	var bulk_items = bulk_items.split(",");	
	var T = "";

	$(bulk_items).each(function(index, el) {
		T +='<div id="pn1" style="border-bottom:1px dotted #ccc;height:47px"><div style="width:155px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" readonly="readonly" style="width:140px" id="" class="input_text_regular_new_pawn catC"></div><div style="width:195px;float:left" class="text_box_holder_new_pawn"><span>'+bulk_items[index]+'</span></div><div style="width:150px;float:left" class="text_box_holder_new_pawn"><input type="text" value="" style="width:137px" id="condition_desc" class="input_text_regular_new_pawn conC"></div></div></div>';
	});

	$("#item_list_holder").append(T);
}

$(document).on('click', '.bulk_list_item_remove', function(event) {
	$(this).parent().remove();
});

$(document).on('click', '.apply_bulk_items', function(event) {
	
	var b_i_names = Array();

	$(".bulk_item_names").each(function(index, el) {
	 	b_i_names[index] = $(this).html();
	});

	var str1 = b_i_names.toString();

	$("#item_code :selected").val(str1);
	$("#item_code :selected").text(str1);
	$("#condition_desc").focus();
	$(".div_bulk_items").remove();

});

$(document).on('click', '.clear_bulk_items', function(event) {
	alert("Clear");
});

$(document).on('keyup', '#bulk_items_add', function(event) {
	event.preventDefault();

	$( "#bulk_items_add" ).autocomplete({
		source: "index.php/main/load_data/m_item/getItemsForBulk",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$(this).parent().append('<div><div style="padding: 6px;padding-top:10px" class="bulk_item_row"><input type="hidden" class="bulk_item_codes" value="'+bt[0]+'"><span class="bulk_item_names">'+bt[1]+'</span> <div class="bulk_list_item_remove" style="float:right;font-size:9px;cursor: pointer">Remove</div></div></div>');
				$( "#bulk_items_add" ).val("").focus();
				event.preventDefault();
		}
	});

});


$(document).on("click",".open_new_pawn_window", function(){
	window.open("index.php?action=t_new_pawn");
});


$(document).ready(function(){
	
	currentDate = $("#dDate").val();
	
	resetFormData();

	$("#btnFrdHO").click(function(){		
		move_request_to_ho = true;
		save();
	});

	$("#txt_man_un").keypress(function(e){
		if (e.keyCode == 13){
			$("#txt_man_pw").focus();
		}
	});

	$("#txt_man_pw").keypress(function(e){
		if (e.keyCode == 13){
			$("#btnManAuth").focus();
		}
	});


	$("#txt_man_un2").keypress(function(e){
		if (e.keyCode == 13){
			$("#txt_man_pw2").focus();
		}
	});

	$("#txt_man_pw2").keypress(function(e){
		if (e.keyCode == 13){
			$("#btnManAuth2").focus();
		}
	});

	$("#btnManAuth").click(function(){

		move_request_to_ho = false;

		var goldvalue = isNaN(parseFloat($("#goldvalue").val())) ? 0 : parseFloat($("#goldvalue").val()); 
		var requiredamount = isNaN(parseFloat($("#requiredamount").val())) ? 0 : parseFloat($("#requiredamount").val());		

		if (!check_for_manager_approval_allow(requiredamount,goldvalue)){

			alert("Unable to allow this amount, please sent an approval request to the head office");
			return;

		}


		
		var un = $("#txt_man_un").val();
		var pw = $("#txt_man_pw").val();

		if (un == ""){
			alert("Invalid username");
			$("#txt_man_un").focus();
			return;
		}

		if (pw == ""){
			alert("Invalid password");
			$("#txt_man_pw").focus();
			return;
		}

		showActionProgress("Verifying...");
		
		$.post("index.php/main/load_data/t_new_pawn/varify_man_crdi",{

			un :	un,
			pw :	pw,
			am : 	(requiredamount - goldvalue)

	
		},function(D){		
			closeActionProgress();

			if (D.s == 1){
				$("#is_discount_approved").val(1);
				$("#approval_id").val(D.nno);

				var goldvalue = isNaN(parseFloat($("#goldvalue").val())) ? 0 : parseFloat($("#goldvalue").val()); 
				var requiredamount = isNaN(parseFloat($("#requiredamount").val())) ? 0 : parseFloat($("#requiredamount").val());

				$("#extra_requ_amount").val( requiredamount - goldvalue );

				save();
				$("#btnManAuth").prop("disabled",true);
			}else{
				alert("Invalid username or password");
				$("#txt_man_pw").focus();
			}
		
		},"json");		

	});

	$("#btnManAuth2").click(function(){
		
		var un = $("#txt_man_un2").val();
		var pw = $("#txt_man_pw2").val();

		if (un == ""){
			alert("Invalid username");
			$("#txt_man_un2").focus();
			return;
		}

		if (pw == ""){
			alert("Invalid password");
			$("#txt_man_pw2").focus();
			return;
		}

		showActionProgress("Verifying...");
		$("#btnManAuth2").prop("disabled",true);
		
		$.post("index.php/main/load_data/t_new_pawn/varify_man_crdi",{

			un :	un,
			pw :	pw,
			am : 	''
	
		},function(D){		
			closeActionProgress();

			if (D.s == 1){
				$("#approval_id").val(0);
				$("#is_quality_changed_approved").val(1);				
				$(".div_manager_auth_option_2").animate({"height":"0px"});
				$("#txt_man_un2,#txt_man_pw2").val("");
				$("#qty").focus();
				$("#btnManAuth2").prop("disabled",false);
			}else{
				$("#is_quality_changed_approved").val(0);
				alert("Invalid username or password");
				$("#txt_man_pw2").focus();
			}
		
		},"json");		

	});

	$("#btnResetManAuth2").click(function(){
		$(".div_manager_auth_option_2").animate({"height":"0px"});
		$("#txt_man_un2,#txt_man_pw2").val("");
		$("#qty").focus().select();
		$("#gold_qulty,#gold_qulty_rate").val(100.00);
		$("#gold_qulty_code").val(100);
		$("#is_quality_changed").val(0);
	});


	// 2016-10-07 new modification

	$(".get_r_billtype").click(function(){

		if ( $(this).is(":checked") ){
			$("#item_list_holder").html("");
			check_for_R_billtype_allow();
		}else{			
			$("#goldcatagory,#gold_rate").val("");
			$(".kt-cl-b").css("display","none");
			$(".kt-cl-a").css("display","block");			
			reset_R_values();
		}

	});

	$("#dDate").val(currentDate);

	$("#customer_id").focus();
	$("#btnSave").attr("disabled",false);
	$("#btnReset,#btnPrint").attr("disabled",true);

	$("#customer_id").keypress(function(){
		if ( $("#chkShowSugg").is(":checked") ){			
			$( "#customer_id" ).autocomplete({				
				source: "index.php/main/load_data/t_new_pawn/getBcCustomer",
				select: function (event, ui) {					
					var bt = ui.item.value;
					bt = bt.split(" - ");
					$("#cus_serno").val(bt[0]);
					$('#cat_code_desc').focus();
					getCustomerInfo($("#cus_serno").val() , $("#customer_id").val() );
				},
				delay : 1000
			});
		}
	});	

	$("#condition_desc").change(function(){
		$("#condition").val( $("#condition_desc :selected").val() )
		$("#gold_type_desc").focus();		
	});

	$("#chkShowSugg").click(function(){
		$("#customer_id").val("").focus();
	});
	
	$("#btnSave").click(function(){		
		save();
	});	
	
	$("#btnFrontSave").click(function(){		
		saveFront();
	});	

	$("#btnSearch").click(function(){
		search($("#Search_nicNo").val());
	});

	$("#btnReset").click(function(){
		resetFormData();
		$("#customer_id").focus();
	});

	$("#btnCancel").click(function(){
		CANCEL_BILL( $("#loan_no").val() );
	});

	$("#btnCancelFrontCusInput").click(function(){		
		resetFrontCusInputs();
		hideCusUI();
	});

	$("#btnAddtoList").click(function(){
		calValue();
		AddtoList();		
	});

	$("#btnPrint").click(function(){
		$("#print_pdf").submit();		
		//if ($("#is_pre_intst_chargeable").val() == 1){ $("#first_int_print_pdf").submit(); }
		//$("#first_int_print_pdf").submit();
	});

	$("#customer_id").keypress(function(e){
		if (e.keyCode == 13){			
			getCustomerInfo($("#cus_serno").val() , $("#customer_id").val() );
			$("#cus_serno").val("");
			//$("#bill_code_desc").focus();
			$("#cat_code_desc").focus();			
		}
	});

	$("#customer_id").keypress(function(){
		$("#cus_serno").val("");
	});


	$("#bill_code_desc").keypress(function(e){
		if (e.keyCode == 13){			
			$("#add_new_cus_front_pawn").animate({height: "0px"});
			setTimeout('$("#item_list_holder").animate({height: "200px"});',500);					    
		}
	});

	$("#cat_code_desc").keypress(function(e){
		if (e.keyCode == 13){			
			$("#item_code").focus();
		}
	});
	
	$("#item_code").keypress(function(e){
		if (e.keyCode == 13){			
			$("#condition_desc").focus();
		}
	});

	$("#item_code").change(function(){		
		$("#condition_desc").focus();		
	});
	
	$("#condition_desc").keypress(function(e){
		if (e.keyCode == 13){			
			$("#gold_type_desc").focus();
		}
	});

	$("#gold_type_desc").keypress(function(e){
		if (e.keyCode == 13){			
			$("#gold_qulty").focus();
		}
	});

	$("#gold_qulty").keypress(function(e){
		if (e.keyCode == 13){			
			$("#qty").focus();
		}
	});
	
	$("#qty").keypress(function(e){
		if (e.keyCode == 13){			
			$("#t_weigth").focus();
		}
	});

	$("#t_weigth").keypress(function(e){
		if (e.keyCode == 13){			
			$("#p_weigth").focus();
		}
	});

	$("#p_weigth").keypress(function(e){
		if (e.keyCode == 13){
			calValue();
			$("#d_weigth").focus();
		}
	});

	$("#d_weigth").keypress(function(e){
		if (e.keyCode == 13){
			calValue();
			$("#btnAddtoList").focus();
		}
	});



	
	$("#gold_qulty").keypress(function(e){
		if (e.keyCode == 13){			
			$("#qty").focus();
		}
	});

	$("#loan_no").keypress(function(e){
		if (e.keyCode == 13){
			if ( rmsps( $(this).val() ) != ""){
				LOAD_LOAN( rmsps( $(this).val() ));
			}
		}
	});

	$("#loan_no").blur(function(e){
		if ( rmsps( $(this).val()) != ""){			
			LOAD_LOAN(rmsps( $(this).val()));
		}
	});

	$("#requiredamount").keyup(function(){		
		setPaybleAmounts();				
	});

	$("#requiredamount").keypress(function(e){		
		
		if (!$(".get_r_billtype").is(":checked")){
			$("#stamp_fee,#bill_code_desc,#billtype,#billno,#bt_letter").val("");		
			var x = 0;
			$("#stamp_fee,#fmintrest").val(x.toFixed(2));
			ra_normal = 0;
		}
		
		if (e.keyCode == 13){			
			var x = parseFloat($(this).val());			
			setBTDetails(x);			
		}

	});

	//$("#requiredamount").blur(function(e){/*var x = parseFloat($(this).val()); if (x > 0){setBTDetails(x); }*/ });

	$(".listur").click(function(){
		alert("")
	});	

	$( "#bill_code_desc" ).autocomplete({
		source: "index.php/main/load_data/m_billtype_det/autocomplete_pawn_add_bill_type",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#billtype").val(bt[0]);
				getBillTypeInfo(bt[0]);
				$("#cat_code_desc").focus();
		}
	});	

	$( "#cat_code_desc" ).autocomplete({
		source: names,
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#cat_code").val(bt[0]);
				$("#is_bulk_cat").val(bt[2]);
				getItemsByCat(bt[0],bt[2]);
				$("#item_code").focus();
		}
	});

	$( "#condition_desc" ).autocomplete({
		source: "index.php/main/load_data/m_conditions/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#condition").val(bt[0]);		
				$("#gold_type_desc").focus();		
		}
	});

	$( "#gold_type_desc" ).autocomplete({
		source: "index.php/main/load_data/r_gold_rate/autocomplete",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#goldcatagory").val(bt[0]);
				$("#gold_rate").val(bt[2]);
				$("#gold_qulty").focus().select();
		}
	});

	$( "#gold_type_desc_R" ).autocomplete({
		source: "index.php/main/load_data/r_gold_rate/autocomplete_R",
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#goldcatagory").val(bt[0]);
				$("#gold_rate").val(bt[2]);
				$("#gold_qulty").focus().select();
		}
	});

	$( "#gold_qulty" ).autocomplete({
		source: gold_q,
		select: function (event, ui) {
			var bt = ui.item.value;
				bt = bt.split(" - ");
				$("#gold_qulty_rate").val(bt[0]);				
				$("#gold_qulty_code").val(bt[1]);				
					
				
				if (bt[0] > 100){
					
					$(".div_manager_auth_option_2").animate({"height":"80px"});
					$("#txt_man_un2").focus();
					$("#is_quality_changed").val(1);

				}else{

					$(".div_manager_auth_option_2").animate({"height":"0px"});
					$("#btnManAuth2").prop("disabled",false);
					$("#txt_man_pw2,#txt_man_un2").val("");
					$("#is_quality_changed").val(0);
					$("#qty").focus();

				}
		}
	});

	$(document).on( "click", "#allow_cal_with_advance", function() { 
		int_cal_with_advance($(this),"1");
	});


	$("#gold_type_desc,#gold_qulty,#t_weigth,#p_weigth").change(function() {
		calValue();
	});


	$("#allow_full_month").click(function(){

		if (!int_cal_changed){
			int_cal_changed = true;
			$("#int_cal_changed").val(1);
		}else{
			int_cal_changed = false;
			$("#int_cal_changed").val(0);
		}

		ra_normal = parseFloat(ra_normal);
		int_normal = parseFloat(int_normal);

		$("#requiredamount").val( make_round( ra_normal ).toFixed(2));
		$("#fmintrest").val( make_round(int_normal).toFixed(2));		

		console.log("A");

		if ( $(this).is(":checked") ){
			$("#is_pre_intst_chargeable").val(0);
			$("#fMintrate,#fMintrate2").css("border","2px solid Red");
			setPaybleAmounts();
			setInt();

			int_cal_with_advance($("#allow_cal_with_advance"),"0"); // 2016-05-10
		}else{
			$("#is_pre_intst_chargeable").val(1);
			$("#fMintrate").css("border","2px solid Red");
			$("#fMintrate2").css("border","2px solid #ffffff");	
			setPaybleAmounts();
			setInt();

			int_cal_with_advance($("#allow_cal_with_advance"),"1"); // 2016-05-10
		}

	});


	$(".man_bn_c").mouseover(function(){
		$(this).find('span').css("color","#000000");
		$(this).find('input[type="text"]').css("border","1px solid Green");
	});

	$(".man_bn_c").mouseout(function(){
		$(this).find('span').css("color","#cccccc");
		$(this).find('input[type="text"]').css("border","1px solid #eaeaea");
	});

	$("#manual_bill_no").keypress(function(e){

		if (e.keyCode == 13){

			showActionProgress("Please wait...");

			$.post("index.php/main/load_data/t_new_pawn/get_system_billno",{
				manual_bill_no : $("#manual_bill_no").val(),
				cut_bc_no : 0
			},function(D){
				
				closeActionProgress();

				if (D.s == 1){

					$("#loan_no").val(D.billno);
					LOAD_LOAN(D.billno);

				}else{
					alert("Invalid manual bill number ");
				}

			},"json");

		}

	});

	$.urlParam = function(name) {
	    var results = (new RegExp("[\\?&]" + name + "=([^&#]*)")).exec(window.location.href);
	    if (results == null) return null;
	    else return results[1] || 0;
	};

	if ($.urlParam("bn") != null){
		$("#loan_no").val($.urlParam("bn"));
		LOAD_LOAN($.urlParam("bn"));
	}



});

function check_for_R_billtype_allow(){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_new_pawn/check_for_R_billtype_allow",{

	},function(D){

		if (D.s == 1){

			$("#fMintrate2").val(0);

			for (no = 0 ; no < D.bt_sum.length ; no++){

				$("#bill_code_desc").val(D.bt_sum[no].billtype);
				$("#billtype").val(D.bt_sum[no].billtype);
				$("#bt_letter").val(D.bt_sum[no].letter);				
				$("#billno").val(D.bt_max);
				$("#billtypeno").val(D.billtypeno);					
				
				if ( no == 0 ){ $("#fMintrate").val(D.bt_sum[no].rate);	}
				if ( no == 1 ){ $("#fMintrate2").val(D.bt_sum[no].rate); }
				
				$("#period").val(D.bt_sum[no].period);
				$("#is_pre_intst_chargeable").val(D.bt_sum[no].weekly_cal);

				$("#gold_type_desc_R,#goldcatagory,#gold_rate").val("");
				$(".kt-cl-a").css("display","none");
				$(".kt-cl-b").css("display","block");

			}

			closeActionProgress();

		}else{

			$(".get_r_billtype").prop("checked",false);
			setBillStatusMSG("RB");
			closeActionProgress();			

		}

	},"json");
}



function int_cal_with_advance(O,is_direct_click = 1){
		
	var ra = isNaN(parseFloat($("#requiredamount").val())) ? 0 : parseFloat($("#requiredamount").val()); 
	
	if ($("#is_pre_intst_chargeable").val() == 1){
		var int_rate = parseFloat($("#fMintrate").val());
	}else{
		var int_rate = parseFloat($("#fMintrate").val()) + parseFloat($("#fMintrate2").val());
	}	

	if (ra <= 0){
		O.attr('checked', false);
		return;
	}

	if (O.is(" :checked") || is_direct_click == 0){ 

		$("#allow_cal_with_advance").prop('checked', true);

		int_rate = int_rate.toFixed(2);		
		var a 	 = (int_rate/100);
		var a 	 = (1-a);			
		var nra  = make_round(ra / a);
		var new_int = make_round( nra - ra_normal );


		if (isNaN(nra)){
			alert("Please enter required amount and press enter button");
			nra = 0;
			new_int = 0;
			$("#requiredamount").focus().select();
		}
		
		$("#requiredamount").val(nra.toFixed(2));			
		$("#fmintrest").val(  new_int.toFixed(2) );

		console.log("B");

		nra 	 = 0;

		$("#btnSave").focus();

	}else{

		if (ra_normal == 0){
			alert("Please enter required amount and press enter button");
			$("#requiredamount").focus().select();
		}

		int_normal = parseFloat(int_normal);		

		$("#requiredamount").val( make_round( ra_normal ).toFixed(2));
		$("#fmintrest").val( make_round( int_normal ).toFixed(2) );

		console.log("C");
	
	}

	setPaybleAmounts();

}


function setBTDetails(x){
	if ( validateAmount(x.toFixed(2)) ){				
		if (x > 0){				
			setBillTypeValue(x, $("#goldvalue").val() );
			calStampFee(x);
			//$("#btnSave").focus();
		}
	}
}

function calStampFee(required_amount){
	var stamp_fee = 0;
	if ( required_amount >= 25000 ){
		stamp_fee = 25 ;
		$("#stamp_fee").val(stamp_fee.toFixed(2));
	}else{
		$("#stamp_fee").val(stamp_fee.toFixed(2));
	}

}

function setInt(){
	int1 = parseFloat($("#fMintrate").val());
	
	if ( $("#is_pre_intst_chargeable").val() == 0 ){
		int2 = parseFloat($("#fMintrate2").val());
	}else{
		int2 = 0;
	}
	
	rmt  = parseFloat($("#requiredamount").val());
	
	//alert(  "fmint = ("+rmt+" * ("+int1+"+"+int2+") / 100)"  )
	
	fmint = (rmt * (int1+int2) / 100);
	
	$("#fmintrest").val( make_round( fmint ).toFixed(2));

	console.log("E");

	int_normal = make_round( fmint ).toFixed(2);
}


function setBillTypeValue(amount,goldvalue){

	showActionProgress("Please wait...");

	if ( $("#item_list_holder").html() == "" ) { 
		showMessage("e","No pawn items in the list"); 
		$("#cat_code_desc").focus(); 
		return false; 
	}

	ra_normal = amount;
	$("#allow_full_month").prop({disabled: false	});

	$.ajax({
		type: "POST",
		url: "index.php/main/load_data/t_new_pawn/setBillTypeValue",
		data:{ amount : amount , goldvalue : goldvalue },
		dataType: "json",
		success: function (D) {
			closeActionProgress();

			if (D.s != 0){

				if (!$(".get_r_billtype").is(":checked")){

					$("#fMintrate2").val(0);

					for (no = 0 ; no < D.bt_sum.length ; no++){

						$("#bill_code_desc").val(D.bt_sum[no].billtype);
						$("#billtype").val(D.bt_sum[no].billtype);
						$("#bt_letter").val(D.bt_sum[no].letter);				
						$("#billno").val(D.bt_max);
						$("#billtypeno").val(D.billtypeno);					
						
						if ( no == 0 ){ $("#fMintrate").val(D.bt_sum[no].rate);	}
						if ( no == 1 ){ $("#fMintrate2").val(D.bt_sum[no].rate); }
						
						$("#period").val(D.bt_sum[no].period);
						
						if (D.bc == 'MR'){ // This section hardcoede on 2017-08-22 
							$("#is_pre_intst_chargeable").val(0);
						}else{
							$("#is_pre_intst_chargeable").val(D.bt_sum[no].weekly_cal);
						}

					}


					if ( $("#is_pre_intst_chargeable").val() == 0 ){			
						$("#allow_full_month").prop("checked",true);			
						$("#fMintrate,#fMintrate2").css("border","2px solid Red");
					}else{				
						$("#allow_full_month").prop("checked",false);
						$("#fMintrate").css("border","2px solid Red");
						$("#fMintrate2").css("border","2px solid #ffffff");
					}

				}

				if (amount < 5000){
					$("#allow_full_month").prop("disabled",true);
				}else{
					$("#allow_full_month").prop("disabled",false);
				}

				setInt();

				setPaybleAmounts();

				int_cal_with_advance($("#allow_cal_with_advance"),0);

				$("#btnSave").prop("disabled",false).focus();

			}else{

				showActionProgress("Bill type not found for this advance value. Please contact admin. **");
				// ** this case might occur due to system was not able to find proper bill type setup for this rage,
				// if branch back date option enabled, their should previous bill type in `r_bill_type_sum_log` table with bt_log_max = 1.
				// if no any entry in the r_bill_type_sum_log table for entered amount range, go to bill type setup and update bill type (you need to update all bill types in this situation, some might have change of interest rate.)

				setTimeout("closeActionProgress()",3000);
			}

		}
	});


	// Use after bill type set
	//$("#requiredamount").css("border","2px solid #ffffff");
	//setPaybleAmounts();

}

function resetFormData(){
	currentDate = $("#dDate").val();
	$("input:text").val("");
	$("#item_list_holder").html("");
	$("#is_pre_intst_chargeable").val(0);
	$("#loan_no,#requiredamount").attr("disabled",false);
	$("#btnAddtoList").val("Add");
	$("#btnSave").val("Save").attr("disabled",false).attr("class","btn_regular");
	$("#btnAddtoList").attr("disabled",false).attr("class","btn_regular");
	$("#btnDelete").val("Delete");
	$("#btnCancel").val("Cancel");
	$("#btnReset").val("Reset").attr("disabled",true).attr("class","btn_regular_disable");
	$("#btnCancel").attr("disabled",true).attr("class","btn_regular_disable");
	$("#btnPrint").val("Print");	
	$("#stamp_fee").val("");
	$("#cus_info_div").html("");
	$("#billtype_info_div").html("");	
	$("#gold_qulty").val(100);
	$("#gold_qulty_rate").val(100);
	$("#by").val("pawn_ticket");	
	$("#dDate").val(currentDate);
	$("#cus_serno").val("");
	$("#fMintrate,#fMintrate2").css("border","2px solid #ffffff");
	$("#hid_trans_no").val("");
	//$("#allow_cal_with_advance").attr({'checked':false,'disabled' : false});
	$(".canceled_bill_msg").css("visibility","visible");
	$(".canceled_bill_msg").animate({height: "0px"});
	$(".msg_txt").html("");
	$("#allow_full_month").prop({checked: true,disabled: true});
	$("#is_discount_approved,#is_cancel_approved").val(0);
	$("#billno").val("");

	$(".get_r_billtype").prop("checked",false);
	$(".kt-cl-b").css("display","none");
	$(".kt-cl-a").css("display","block");

	$("#item_code").val("");
	$("#item_code option[value='60']").remove();
	
	if(typeof counter != 'undefined'){
		clearTimeout(counter);	
	}

	count = 300;
	$("#btnPrint").attr("disabled",true).attr("class","btn_regular_disable").attr("value","Print");

	$("#btnManAuth").prop("disabled",false);
	$("#extra_requ_amount").val(0);
	$("#txt_man_un,#txt_man_pw").val("");

	$(".div_manager_auth_option").animate({"height":"0px"});
	$("#is_quality_changed_approved,#is_quality_changed").val(0);

	$("#customer_id").focus();

	$("#approval_id,#discount,#extra_approved_amount,#happid").val(0);

	$("#is_bulk_cat").val(0);
}

function resetFrontCusInputs(){
	//$("#customer_id").val("");
	//$("#customer_no").val("");
	$("#hid").val("0");
	$("#nicno").val("");
	$('input:radio[name=title]').removeAttr("checked");
	$("#cusname").val("");
	$("#other_name").val("");
	$("#mobile").val("");
	$("#address").val("");
	$("#address2").val("");
	$("#telNo").val("");
}

function LOAD_LOAN(billno){

	tot_t_weigth = 0;
	tot_p_weigth = 0;
	tqty = 0;
	tot_val = 0;
	rl_no = 0;

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_new_pawn/LOAD_LOAN",{
		billno : billno
	},function(D){
		closeActionProgress();
		if (D.s == 1){

			$("#r_no").val(D.loan_sum.loanno);
			$("#r_no_i").val(D.loan_sum.loanno);
			$("#hid_trans_no").val(D.loan_sum.transeno);
			$("#bcc").val(D.loan_sum.bc);
			$("#manual_billno").val(D.loan_sum.manual_billno);
			$("#previous_billno").val(D.loan_sum.old_o_new_billno);

			$("#btnPrint").attr("disabled",false).attr("class","btn_regular").attr("value","Re-Print");
			$("#btnCancel").attr("disabled",false).attr("class","btn_regular");

			$("#loan_no").attr("disabled",true);
			$("#btnReset").attr("disabled",false).attr("class","btn_regular_reset");
			$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");

			var S = D.loan_sum.status;

			if (S == "C" || S == "R" || S == "L" || S == "F" || S == "PO" || S == "RN" || S == "AM" ){
				$("#btnCancel").attr("disabled",true).attr("class","btn_regular_disable");
				$("#btnPrint,#btnAddtoList").attr("disabled",true).attr("class","btn_regular_disable");
				setBillStatusMSG(S);
			}

			$("#hid").val(D.loan_sum.loanno);
			$("#customer_id").val(D.loan_sum.customer_id);			
			getCustomerInfo(D.loan_sum.customer_id,D.loan_sum.cus_serno, 1 );
			$("#bill_code_desc").val(D.loan_sum.billtype);
			$("#billtype").val(D.loan_sum.billtype);
			$("#bt_letter").val(D.loan_sum.bt_letter);

			$("#is_pre_intst_chargeable").val(D.loan_sum.allow_pre_int);
	
			if (D.loan_sum.int_with_amt == 1){
				$("#divchkh").html('<input type="checkbox" id="allow_cal_with_advance" name="allow_cal_with_advance" checked="checked" disabled="disabled">');
			}else{
				$("#divchkh").html('<input type="checkbox" id="allow_cal_with_advance" name="allow_cal_with_advance" disabled="disabled">');
			}

			$("#allow_cal_with_advance").attr('disabled', true);

			$("#billno").val(D.loan_sum.billno);
			$("#billtypeno").val(D.loan_sum.billtypeno);
			$("#fMintrate").val(D.loan_sum.fmintrate);
			$("#fMintrate2").val(D.loan_sum.fmintrate2);
			$("#period").val(D.loan_sum.period);
			$("#dDate").val(D.loan_sum.ddate);
			$("#time").val(D.loan_sum.time);

			$("#stamp_fee").val(D.loan_sum.stamp_fee);
			$("#requiredamount").attr("disabled",true).val(D.loan_sum.requiredamount);	
			$("#fmintrest").val(D.loan_sum.fmintrest);	

			console.log("F");

			var paybl_amt = D.loan_sum.requiredamount - D.loan_sum.stamp_fee;
			setPaybleAmounts();

			//$("#payable_amount").val( paybl_amt.toFixed(2) );

			// Details

			var T = var_bulk_items = "";

			for(num = 0 ; num < D.loan_det.length ; num++){							

				cat_code_desc = D.loan_det[num].cat_code + " - " + D.loan_det[num].des;
				cat_code = D.loan_det[num].cat_code;

				item_code_desc = D.loan_det[num].itemcode + " - " + D.loan_det[num].itemname;
				item_code = D.loan_det[num].itemcode;

				condition_desc = D.loan_det[num].con + " - " + D.loan_det[num].cdes;
				condition = D.loan_det[num].con;

				gold_type_desc = D.loan_det[num].id + " - " + D.loan_det[num].goldcatagory;
				goldcatagory = D.loan_det[num].id;
				gold_rate = D.loan_det[num].goldvalue;
				value = D.loan_det[num].value;

				gold_quality = D.loan_det[num].rate;
				gold_qulty_rate = D.loan_det[num].rate;				

				qty = D.loan_det[num].qty;
				t_weigth = D.loan_det[num].goldweight;
				p_weigth = D.loan_det[num].pure_weight;
				d_weigth = D.loan_det[num].denci_weight;				



				tot_t_weigth += parseFloat(t_weigth);
				tot_p_weigth += parseFloat(p_weigth);
				tot_d_weigth += parseFloat(d_weigth);

				tqty += parseFloat(qty);
				tot_val += parseFloat(value);
				rl_no += 1;

				if (D.loan_det[num].bulk_items != ""){
					var_bulk_items = D.loan_det[num].bulk_items;
				}else{
					var_bulk_items = "";
				}

				T += '<div style="border-bottom:1px dotted #ccc;height:47px" id="pn'+rl_no+'">';

				T += '<div class="text_box_holder_new_pawn" style="width:135px;float:left"><input type="text" class="input_text_regular_new_pawn catC" id="cat_code_desc" style="width:120px" readonly="readonly" value="'+cat_code_desc+'"><input type="hidden" name="A[]" value="'+cat_code+'" class="A1"></div>';

				T += '<div class="text_box_holder_new_pawn" style="width:175px;float:left"><input class="input_text_regular_new_pawn itemC" type="text" id="condition_desc" style="width:165px" value="'+item_code_desc+'"><input type="hidden" name="B[]" value="'+item_code+'" class="B1"></div>';

				T += '<div class="text_box_holder_new_pawn" style="width:140px;float:left"><input class="input_text_regular_new_pawn conC" type="text" id="condition_desc" style="width:130px" value="'+condition_desc+'"><input type="hidden" name="C[]" value="'+condition+'" class="C1"></div>';

				T += '<div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn gtypeC" type="text" id="gold_type_desc" style="width:97px" value="'+gold_type_desc+'"><input type="hidden" name="D[]" value="'+goldcatagory+'" class="D1"><input type="hidden" name="J[]" value="'+gold_rate+'" class="J1"></div>';
				
				T += '<div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn gquliC" type="text" id="gold_qlty" style="width:47px" value="'+gold_quality+'"><input type="hidden" name="I[]" value="'+gold_qulty_rate+'"  class="E1"></div>';
				
				T += '<div class="text_box_holder_new_pawn" style="width:50px;float:left"><input class="input_text_regular_new_pawn qtyC" type="text" id="qty" style="width:37px" value="'+qty+'"><input type="hidden" name="E[]" value="'+qty+'"  class="F1"></div>'

				T += '<div class="text_box_holder_new_pawn" style="width:76px;float:left"><input class="input_text_regular_new_pawn t_weigthC" type="text" id="t_weigth" style="width:63px" value="'+t_weigth+'"><input type="hidden" name="F[]" value="'+t_weigth+'"  class="G1"></div>';

				T += '<div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn p_weigthC" type="text" id="p_weigth" style="width:60px" value="'+p_weigth+'"><input type="hidden" name="G[]" value="'+p_weigth+'"  class="H1"></div>';

				T += '<div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn d_weigthC" type="text" id="d_weigth" style="width:60px" value="'+d_weigth+'"><input type="hidden" name="L[]" value="'+d_weigth+'"  class="L1"></div>';

				T += '<div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn valueC" type="text" id="value" style="width:100px; text-align:right" value="'+value+'"><input type="hidden" name="H[]" value="'+value+'" class="I1"></div>';

				T += '<div class="text_box_holder_new_pawn" style="width:50px;float:left"><a class="list-ur" id="ud_'+rl_no+'" onClick=updateRow("ud_'+rl_no+'")>Update</a> <a class="list-ur" id="rl_'+rl_no+'" onClick=removeRow("rl_'+rl_no+'")>Remove</a></div></div>';
			}
			
			$("#item_list_holder").html("").prepend(T);
			
			var_bulk_items != "" ? setBulkItemsShow(var_bulk_items) : var_bulk_items = '';	

			$("#totalweight").val(tot_t_weigth.toFixed(3));
			$("#totalpweight").val(tot_p_weigth.toFixed(3));
			$("#totaldweight").val("");
			$("#tqty").val(tqty);
			$("#goldvalue").val(tot_val.toFixed(2));

			$("#is_reprint").val(1);
			$("#old_loan_no").val(D.loan_sum.loanno);

			if (D.loan_sum.billtype == "R"){
				$(".get_r_billtype").prop("checked",true);
			}else{
				$(".get_r_billtype").prop("checked",false);
			}


		}else{
			showMessage("e","Invalid loan number");
		}
		
		
	},"json");
}


function CANCEL_BILL(loan_no){

	$("#btnSave").attr('disabled', true);

	//if ( $("#is_cancel_approved").val() == 1 ){

	if (confirm("Do you want cancel this bill?")){

		showActionProgress("Please wait...");

		$.post("index.php/main/load_data/t_new_pawn/CANCEL_BILL",{
			
			loan_no : $("#old_loan_no").val(),
			transeno: $("#hid_trans_no").val(),
			cus_serno : $('#cus_serno').val(),
			requiredamount : $("#requiredamount").val(),
			fmintrest : $("#fmintrest").val(),
			date : $("#dDate").val(),
			billtype : $("#billtype").val(),
			billno: $("#billno").val(),
			bcc : $("#bcc").val(),
			previous_billno : $("#previous_billno").val(),
			is_renew : $("#is_renew").val(),
			time : $("#time").val()

		},function(D){
			closeActionProgress();

			if (D.s == 1){
				alert("Bill cancelled");
				resetFormData();
			}else if (D.s == 2){
				showActionProgress("");
				alert("Unable to cancel this bill. Only last will able to cancel");
			}else{
				alert("Unable to cancel this bill.");
			}

		},"json");

	}		

	/*}else{			

		if (confirm('Request an approval from head office to cancel this bill. Do you want continue?')){

			showActionProgress("Sending approval request...");

			var appid = "L"+Math.floor(Math.random()*(99999999-(10000000+1))+(10000000+1));

			$("#happid").val(appid);				

			$.post("index.php/main/load_data/approvals/add_approval",{

				loanno 			: appid,					
				int_balance  	: 0,
				requested_amount: 0,
				payable_amount 	: 0,
				request_type 	: "CN",
				goldvalue		: 0,
				billno: $("#billno").val()


			},function(D){

				closeActionProgress();

				if (D.s == 1){
					$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
					showApprovalWatingMsgBox("");
					counter = setInterval(timer, 1000); //1000 will  run it every 1 second
					enableApprovalMonitor(appid); // set Request approval monitor
				}else if(D.s == 2){
					showActionProgress("");
					showMessage("e","Unable to cancel this bill. Only last will able to cancel");
				}else{					
					showMessage("s", D.m );
				}

			},"json");

		}
	}*/

	

}

function setPaybleAmounts(){
	var requiredamount 	= isNaN(parseFloat($("#requiredamount").val())) ? 0 : parseFloat($("#requiredamount").val()); 
	var stamp_fee 		= isNaN(parseFloat($("#stamp_fee").val())) ? 0 : parseFloat($("#stamp_fee").val());  
	var fmintrest 		= isNaN(parseFloat($("#fmintrest").val())) ? 0 : parseFloat($("#fmintrest").val());  
	var payable_amount 	= requiredamount;
	var	net_payable_amount = payable_amount - fmintrest;
	
	$("#payable_amount").val(payable_amount.toFixed(2));
	
	if ($("#allow_cal_with_advance").is(":checked")){
		$("#net_payable_amount").val(net_payable_amount.toFixed(2));
	}else{
		$("#net_payable_amount").val(payable_amount.toFixed(2));
	}


}

function calValue(){

	var gold_rate 	= parseFloat($("#gold_rate").val());
	var gold_qulty_rate = parseFloat($("#gold_qulty_rate").val());
	var p_weigth  	= parseFloat($("#p_weigth").val());
		p_weigth 	= (p_weigth / 8);

	var value 		= (gold_rate * p_weigth);
		value 		= ((value * gold_qulty_rate) / 100);	

		if (isNaN(value)){ 
			$("#value").val("");
		}else{
			$("#value").val(value.toFixed(2));
		}	
}

function clearList(){
	$("#cat_code_desc").val("");
	$("#cat_code").val("");

	$("#item_code").val("");
	$("#item_code").val("");

	$("#condition_desc").val("");
	$("#condition").val("");

	$("#gold_type_desc").val("");
	$("#goldcatagory").val("");
	$("#gold_rate").val("");
	
	$("#gold_qulty,#gold_qulty_rate").val(100.00);	

	$("#qty").val("");
	$("#t_weigth").val("");
	$("#p_weigth").val("");
	$("#d_weigth").val("");
	$("#value").val("");

	$("#cat_code_desc").focus();
	$("#is_bulk_cat").val(0);
}

var tot_t_weigth = 0;
var tot_p_weigth = 0;
var tot_d_weigth = 0;
var tqty = 0;
var tot_val = 0;
var rl_no = 0;
var divEditID = "";

function AddtoList(){

	cat_code_desc = $("#cat_code_desc").val();
	cat_code = $("#cat_code").val();

	item_code_desc = $("#item_code :selected").text();
	item_code = $("#item_code").val();

	condition_desc = $("#condition_desc").val();
	condition = $("#condition").val();

	if ($(".get_r_billtype").is(":checked")){
		gt = $("#gold_type_desc_R").val();
	}else{
		gt = $("#gold_type_desc").val();		
	}	
	
	gt = gt.split(" - ");

	gold_type_desc = gt[1];

	if (gold_type_desc == undefined){
		gold_type_desc = gt[0];
	}



	goldcatagory = $("#goldcatagory").val();
	gold_rate = $("#gold_rate").val();

	gold_quality = $("#gold_qulty").val();
	gold_qulty_rate = $("#gold_qulty_rate").val();	

	qty = $("#qty").val();
	
	t_weigth = $("#t_weigth").val();
	p_weigth = $("#p_weigth").val();
	d_weigth = $("#d_weigth").val();

	value = $("#value").val();
	

	if ( rmsps(cat_code_desc) == "" || cat_code == "" ){$("#cat_code_desc").val("").focus();return;}
	if (item_code == ""){$("#item_code").focus();return;}
	if (rmsps(condition_desc) == "" || condition == ""){$("#condition_desc").val("").focus();return;}
	if (rmsps(gold_type_desc) == "" || goldcatagory == "" || gold_rate == ""){$("#gold_type_desc").val("").focus();	return;	}
	if (rmsps(gold_quality) == "" || gold_qulty_rate == "" ){$("#gold_qulty").val("").focus();return;}
	
	if ( $("#is_quality_changed").val() == 1 ){

		if ( $("#is_quality_changed_approved").val() != 1 ){
			showActionProgress("");					
			showMessage("e","Increased gold quality not approved by manager");
			return;
		}

	}


	if (rmsps(qty) == ""){
		$("#qty").val("").focus().select();
		return;
	}else{
		if (!validateNumber($("#qty").val())){
			$("#qty").val("").focus().select();
			return;	
		}
	}

	if (rmsps(t_weigth) == ""){
		$("#t_weigth").val("").focus().select();
		return;
	}else{
		if (!validateNumber($("#t_weigth").val())) {
			$("#t_weigth").val("").focus().select();
			return;	
		}
	}

	if (rmsps(p_weigth) == ""){
		$("#p_weigth").val("").focus().select();
		return;
	}else{
		if (!validateNumber($("#p_weigth").val())) {
			$("#p_weigth").val("").focus().select();
			return;	
		}
	}

	if ( parseFloat(p_weigth)  > parseFloat(t_weigth)){
		showActionProgress("");					
		showMessage("e","Invalid pure weigth");
		$("#p_weigth").select();
		return;
	}

	if (rmsps(d_weigth) == ""){
		$("#d_weigth").val("").focus().select();
		return;
	}else{
		if (!validateNumber($("#d_weigth").val())) {
			$("#d_weigth").val("").focus().select();
			return;	
		}
	}


	tot_t_weigth += parseFloat(t_weigth);
	tot_p_weigth += parseFloat(p_weigth);
	tqty += parseFloat(qty);
	tot_val += parseFloat(value);
	rl_no += 1;


	if ($("#btnAddtoList").attr("value") != "Update"){
		
		var T = '<div style="border-bottom:1px dotted #ccc;height:47px" id="pn'+rl_no+'">';

		T += '<div class="text_box_holder_new_pawn" style="width:135px;float:left"><input type="text" readonly="readonly" class="input_text_regular_new_pawn catC" id="cat_code_desc" style="width:120px" readonly="readonly" value="'+cat_code_desc+'"><input type="hidden" name="A[]" value="'+cat_code+'" class="A1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:175px;float:left"><input class="input_text_regular_new_pawn itemC" type="text"  readonly="readonly"  id="condition_desc" style="width:165px" value="'+item_code_desc+'"><input type="hidden" name="B[]" value="'+item_code+'" class="B1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:140px;float:left"><input class="input_text_regular_new_pawn conC" type="text" readonly="readonly"  id="condition_desc" style="width:130px" value="'+condition_desc+'"><input type="hidden" name="C[]" value="'+condition+'" class="C1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn gtypeC" type="text" readonly="readonly"  id="gold_type_desc" style="width:97px" value="'+gold_type_desc+'"><input type="hidden" name="D[]" value="'+goldcatagory+'" class="D1"><input type="hidden" name="J[]" value="'+gold_rate+'" class="J1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn gquliC" type="text" readonly="readonly"  id="gold_qlty" style="width:47px" value="'+gold_quality+'"><input type="hidden" name="I[]" value="'+gold_qulty_rate+'"  class="E1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:50px;float:left"><input class="input_text_regular_new_pawn qtyC" type="text" readonly="readonly"  id="qty" style="width:37px" value="'+qty+'"><input type="hidden" name="E[]" value="'+qty+'"  class="F1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:76px;float:left"><input class="input_text_regular_new_pawn t_weigthC" type="text" readonly="readonly"  id="t_weigth" style="width:63px" value="'+t_weigth+'"><input type="hidden" name="F[]" value="'+t_weigth+'"  class="G1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn p_weigthC" type="text" readonly="readonly"  id="p_weigth" style="width:60px" value="'+p_weigth+'"><input type="hidden" name="G[]" value="'+p_weigth+'"  class="H1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn d_weigthC" type="text" readonly="readonly"  id="d_weigth" style="width:60px" value="'+d_weigth+'"><input type="hidden" name="L[]" value="'+d_weigth+'"  class="L1"></div>';		

		T += '<div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn valueC" type="text" readonly="readonly"  id="value" style="width:100px; text-align:right" value="'+value+'"><input type="hidden" name="H[]" value="'+value+'" class="I1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:50px;float:left"><a class="list-ur" id="ud_'+rl_no+'" onClick=updateRow("ud_'+rl_no+'")>Update</a> <a class="list-ur" id="rl_'+rl_no+'" onClick=removeRow("rl_'+rl_no+'")>Remove</a></div></div>';

		$("#item_list_holder").prepend(T);

	}else{
		
		var T = '<div class="text_box_holder_new_pawn" style="width:135px;float:left"><input type="text" readonly="readonly" class="input_text_regular_new_pawn catC" id="cat_code_desc" style="width:120px" readonly="readonly" value="'+cat_code_desc+'"><input type="hidden" name="A[]" value="'+cat_code+'" class="A1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:175px;float:left"><input class="input_text_regular_new_pawn itemC" type="text"  readonly="readonly"  id="condition_desc" style="width:165px" value="'+item_code_desc+'"><input type="hidden" name="B[]" value="'+item_code+'" class="B1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:140px;float:left"><input class="input_text_regular_new_pawn conC" type="text" readonly="readonly"  id="condition_desc" style="width:130px" value="'+condition_desc+'"><input type="hidden" name="C[]" value="'+condition+'" class="C1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn gtypeC" type="text" readonly="readonly"  id="gold_type_desc" style="width:97px" value="'+gold_type_desc+'"><input type="hidden" name="D[]" value="'+goldcatagory+'" class="D1"><input type="hidden" name="J[]" value="'+gold_rate+'" class="J1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:60px;float:left"><input class="input_text_regular_new_pawn gquliC" type="text" readonly="readonly"  id="gold_qlty" style="width:47px" value="'+gold_quality+'"><input type="hidden" name="I[]" value="'+gold_qulty_rate+'"  class="E1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:50px;float:left"><input class="input_text_regular_new_pawn qtyC" type="text" readonly="readonly"  id="qty" style="width:37px" value="'+qty+'"><input type="hidden" name="E[]" value="'+qty+'"  class="F1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:76px;float:left"><input class="input_text_regular_new_pawn t_weigthC" type="text" readonly="readonly"  id="t_weigth" style="width:63px" value="'+t_weigth+'"><input type="hidden" name="F[]" value="'+t_weigth+'"  class="G1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn p_weigthC" type="text" readonly="readonly"  id="p_weigth" style="width:60px" value="'+p_weigth+'"><input type="hidden" name="G[]" value="'+p_weigth+'"  class="H1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:73px;float:left"><input class="input_text_regular_new_pawn d_weigthC" type="text" readonly="readonly"  id="d_weigth" style="width:60px" value="'+d_weigth+'"><input type="hidden" name="L[]" value="'+d_weigth+'"  class="L1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:110px;float:left"><input class="input_text_regular_new_pawn valueC" type="text" readonly="readonly"  id="value" style="width:100px; text-align:right" value="'+value+'"><input type="hidden" name="H[]" value="'+value+'" class="I1"></div>';

		T += '<div class="text_box_holder_new_pawn" style="width:50px;float:left"><a class="list-ur" id="ud_'+rl_no+'" onClick=updateRow("ud_'+rl_no+'")>Update</a> <a class="list-ur" id="rl_'+rl_no+'" onClick=removeRow("rl_'+rl_no+'")>Remove</a></div>';

		$("#"+divEditID).html(T);
		$("#btnAddtoList").attr("Value","Add");
	}

	CalTotal();
	divEditID = "";
	clearList();
	$("#requiredamount,#payable_amount").val("");
	
}

function CalTotal(){

	var value = 0;
	var pweight = 0;
	var tWeigth = 0;
	var dWeigth = 0;
	var qty = 0;

	$(".I1").each(function(){ value += parseFloat( $(this).val() ); });
	$(".H1").each(function(){ pweight += parseFloat( $(this).val() ); });
	$(".L1").each(function(){ dWeigth += parseFloat( $(this).val() ); });
	$(".G1").each(function(){ tWeigth += parseFloat( $(this).val() ); });
	$(".F1").each(function(){ qty += parseFloat( $(this).val() ); });

	$("#tqty").val(qty);
	$("#totalweight").val(tWeigth.toFixed(3));
	$("#totalpweight").val(pweight.toFixed(3));
	$("#totaldweight").val("");
	$("#goldvalue").val(value.toFixed(2));

}

function removeRow(row_id){		
	
	if (confirm("Do you want remove this item ? ")){
		$("#"+row_id).parent().parent().remove();
		CalTotal();
	}

}

function updateRow(row_id){	

	divEditID = $("#"+row_id).parent().parent().attr("id");	

	$("#btnAddtoList").attr("value","Update");

	$("#"+row_id).parent().parent().css("background-color","#f9f9f9");

	







	// set item dropdown list according to 

	var is_bulk = $("#is_bulk_cat").val();

	if (is_bulk == 1){	
		
		$("#item_code").html("<option>Bulk Items</option>");
		$("#item_code").parent().append('<div class="div_bulk_items"><input type="text" id="bulk_items_add" class="input_text_regular_new_pawn" style="border:1px solid Green;width:410px">&nbsp;&nbsp; <a class="apply_bulk_items">Apply</a> &nbsp;&nbsp;&nbsp; <a class="clear_bulk_items">Clear</a></div>');
		$("#is_bulk").val(1);
	
	}else{

		$("#item_code").html("");
		$(".div_bulk_items").remove();
		$("#is_bulk").val(0);

		showActionProgress("Loading items...");

		$.post("index.php/main/load_data/m_item/getItemsByCat",{
			cat_code : $("#"+row_id).parent().parent().find(".catC").parent().find(".A1").val(),
			is_bulk : is_bulk
		},function(D){
			$("#item_code").html(D);
			closeActionProgress();


			$("#cat_code_desc").val($("#"+row_id).parent().parent().find(".catC").parent().find("input").val());
			$("#cat_code").val($("#"+row_id).parent().parent().find(".catC").parent().find(".A1").val());

			$("#item_code").val($("#"+row_id).parent().parent().find(".itemC").parent().find(".B1").val());
			$("#item_code").val($("#"+row_id).parent().parent().find(".itemC").parent().find(".B1").val());

			$("#condition_desc").val($("#"+row_id).parent().parent().find(".conC").parent().find("input").val());
			$("#condition").val($("#"+row_id).parent().parent().find(".conC").parent().find(".C1").val());

			$("#gold_type_desc").val($("#"+row_id).parent().parent().find(".gtypeC").parent().find("input").val());
			$("#goldcatagory").val($("#"+row_id).parent().parent().find(".gtypeC").parent().find(".D1").val());
			$("#gold_rate").val($("#"+row_id).parent().parent().find(".gtypeC").parent().find(".J1").val());

			$("#gold_qulty").val($("#"+row_id).parent().parent().find(".gquliC").parent().find("input").val());
			$("#gold_qulty_rate").val($("#"+row_id).parent().parent().find(".gquliC").parent().find(".E1").val());		

			$("#qty").val($("#"+row_id).parent().parent().find(".qtyC").parent().find(".F1").val());
			$("#t_weigth").val($("#"+row_id).parent().parent().find(".t_weigthC").parent().find(".G1").val());
			$("#p_weigth").val($("#"+row_id).parent().parent().find(".p_weigthC").parent().find(".H1").val());
			$("#d_weigth").val($("#"+row_id).parent().parent().find(".d_weigthC").parent().find(".L1").val());
			
			$("#value").val($("#"+row_id).parent().parent().find(".valueC").parent().find(".I").val());
			
			calValue();



		},"text");

	}







	

}

function getItemsByCat(cat_code,is_bulk){
	
	if (is_bulk == 1){	
		
		$("#item_code").html("<option>Bulk Items</option>");
		$("#item_code").parent().append('<div class="div_bulk_items"><input type="text" id="bulk_items_add" class="input_text_regular_new_pawn" style="border:1px solid Green;width:410px">&nbsp;&nbsp; <a class="apply_bulk_items">Apply</a> &nbsp;&nbsp;&nbsp; <a class="clear_bulk_items">Clear</a></div>');
		$("#is_bulk").val(1);
	
	}else{

		$("#item_code").html("");
		$(".div_bulk_items").remove();
		$("#is_bulk").val(0);

		$.post("index.php/main/load_data/m_item/getItemsByCat",{
			cat_code : cat_code,
			is_bulk : is_bulk
		},function(D){
			$("#item_code").html(D);
			closeActionProgress();
		},"text");

	}

}

function getBillTypeInfo(bill_type_code){
	showActionProgress("Loading...");

	$.post("index.php/main/load_data/m_billtype_det/getBillTypeInfo",{
		bill_type_code : bill_type_code
	},function(D){	
		
		if (D.status == 1){			
			$("#billtype_info_div").css({height: "42px"}).html(D.rec).animate({height: "245px"});
			$("#billno").val(D.bill_max_no);
			$("#fMintrate").val(D.det.rate);
			$("#period").val(D.det.period);		
			$('#is_pre_intst_chargeable').val(D.det.allow_pre_int);
			setPaybleAmounts();
		}

		closeActionProgress();

	},"json");
}

function getCustomerInfo(cus_serno,customer_id,st = 0){
	
	showActionProgress("Loading...");

	if ($("#chkShowSugg").is(":checked") || st == 1){
		chkShowSugg = 1;
	}else{
		chkShowSugg = 0;
	}

	$.post("index.php/main/load_data/t_new_pawn/getCustomerInfo",{
		cus_serno : cus_serno,
		customer_id : customer_id,
		chkShowSugg : chkShowSugg
	},function(D){

		//$(".canceled_bill_msg").animate({height: "0px"});	
		
		if (D.status == 1){			
			
			$("#current_pawning_total").val(D.current_pawning_total);
			$("#cus_monitor").val(D.cus_monitor);
			$("#cus_monitor_max_amount").val(D.allow_total_pawn_amount);

			$("#btnCancelFrontCusInput").click();
			hideCusUI();


			//-----------------

				var C_Obj = D.customer_pawn_statistics;

				var t = "";

					t += "<div><h3>Full Name</h3><span>"+C_Obj.title+" "+ C_Obj.cusname +" </span></div>";
					t += "<div><h3>ID</h3><span>"+C_Obj.nicno+"</span></div>";
					t += "<div><h3>Address</h3><span>"+C_Obj.address+"</span></div>";
					
					if (C_Obj.address2 != ""){ 	t += "<div><h3>Address 2</h3><span>"+C_Obj.address2+"</span></div>"; }
					if (C_Obj.mobile != ""){ 	t += "<div><h3>Mobile</h3><span>"+C_Obj.mobile+"</span></div>"; }
					if (C_Obj.telNo != ""){ 	t += "<div><h3>Telephone</h3><span>"+C_Obj.telNo+"</span></div>"; }
					if (C_Obj.bc != ""){ 		t += "<div><h3>Customer Added Branch</h3><span>"+C_Obj.bc+"</span></div>"; }
					

					//---------------------

					if (C_Obj.alert_color == "red"){
						c = " style='background-color:red;color:#ffffff'";
						t += '<div '+c+'>Last bill '+C_Obj.last_billno+' <br> '+D.tm+'</div>';
						t += '<div '+c+'><h3>Last Bill Added Branch</h3>'+C_Obj.last_bill_bc+'</div>';
					}else{
						c = " style='background-color:green;color:#ffffff'";
						t += '<div '+c+'>Last bill '+C_Obj.last_billno+' <br> '+D.tm+'</div>';
						t += '<div '+c+'><h3>Last Bill Added Branch</h3>'+C_Obj.last_bill_bc+'</div>';
					}

					$("#cus_info_div").html(t);

					$(".cph_p").html(D.customer_pawn_statistics.pawnings);
					$(".cph_f").html(D.customer_pawn_statistics.forfiedted);
					$(".cph_r").html(D.customer_pawn_statistics.redeemed);
					$(".cph_cn").html(D.customer_pawn_statistics.canceled);
					
					$("#pawn_info").css({height: "42px"}).animate({height: "200px"});

			//-----------------

			
			
			

			if (D.customer_pawn_statistics.isblackListed == 1){
				showActionProgress("");					
				showMessage("e","Blacklisted customer");
				return;
			}
			
			$("#cus_serno").val(D.customer_pawn_statistics.serno);
			$("#customer_id").val(D.customer_pawn_statistics.nicno);			
			$("#cat_code_desc").focus();
		}

		closeActionProgress();

		if (D.status == 0){
			if(confirm("Customer not found, do you want add as new customer")){
				showCusUI();
				$('#nicno').val($("#customer_id").val());
				$("#cusname").focus();
			}else{
				$("#btnCancelFrontCusInput").click();
				hideCusUI();
			}
		}


	},"json");

}

function showCusUI(){
	$("#item_list_holder").animate({height: "0px"});
	setTimeout('$("#add_new_cus_front_pawn").animate({height: "303px"})',500);
}

function hideCusUI(){
	$("#cus_serno,#customer_id").val("").focus();
	$("#add_new_cus_front_pawn").animate({height: "0px"});
	setTimeout('$("#item_list_holder").animate({height: "200px"})',500);
}

function saveAndHideCusUI(){
	$("#bill_code_desc").val("").focus();
	$("#add_new_cus_front_pawn").animate({height: "0px"});
	setTimeout('$("#item_list_holder").animate({height: "200px"})',500);
}

function save(){

		$("#btnSave").attr('disabled', true);
	if (validate_form()){


		var goldvalue 			= isNaN(parseFloat($("#goldvalue").val())) ? 0 : parseFloat($("#goldvalue").val()); 
		var requiredamount 		= isNaN(parseFloat($("#requiredamount").val())) ? 0 : parseFloat($("#requiredamount").val());
		var app_amount 			= isNaN(parseFloat($("#discount").val())) ? 0 : parseFloat($("#discount").val()); 
		var extra_requ_amount 	= isNaN(parseFloat($("#extra_requ_amount").val())) ? 0 : parseFloat($("#extra_requ_amount").val()); 
	
		if ( requiredamount <= goldvalue || $("#is_discount_approved").val() == 1 ){
			
			showActionProgress("Saving...");			

			var frm = $('#form_'); 

			$.ajax({
				type: frm.attr('method'), 
				url: frm.attr('action'), 
				data: frm.serialize(), 
				dataType: "json", 
				success: function (D) {
					
					
					if (D.s == 1){
						closeActionProgress(); 
						showMessage("s","New pawn saved success"); 
						$("#r_no").val(D.loan_no); 
						$("#r_no_i").val(D.loan_no); 

						if ($("#allow_full_month").is(" :checked")){
							$("#fullmonth_int").val(1);
						}else{
							$("#fullmonth_int").val(0);
						}

						$("#print_pdf").submit();					

						resetFormData(); 
					
					}else if(D.s == 33){
						closeActionProgress(); 
						alert(D.msg_33);
						location.href = '';
					
					}else if(D.er['code'] == 1062){

						setTimeout("save()",500);
						return;

					}else if(D.s == 55){

						closeActionProgress(); 
						alert("Same values saved for this customer within one minute");
						location.href = '';

					}else{
						closeActionProgress(); 
						alert(D.er['message']);
						showMessage("e","Error, save failed");
						location.href = ''; 
					} 
				} 
			});
		

		}else{

			if (check_for_manager_approval_allow(requiredamount,goldvalue)  && move_request_to_ho == false  ){
				$("html, body").animate({ scrollTop: $(document).height() }, 1000);
				$(".div_manager_auth_option").animate({"height":"80px"});
				$("#txt_man_un").focus();
			}else{

				if (!move_request_to_ho){				
					var m = "Extra advance amount approval required.\n\nDo you want send this loan to approval?";
				}else{
					var m = "Confirm sending to head office approval";
				}

				if (confirm(m)){

					showActionProgress("Sending approval request...");

					var appid = "L"+Math.floor(Math.random()*(99999999-(10000000+1))+(10000000+1));

					$("#happid").val(appid);				

					$.post("index.php/main/load_data/approvals/add_approval",{

						loanno 			: appid,					
						int_balance  	: 0,
						requested_amount: (requiredamount - goldvalue) ,
						payable_amount 	: $("#payable_amount").val(),
						request_type 	: "EL",
						goldvalue		: goldvalue,
						over_adv_item_list : $("#item_list_holder").html()

					},function(D){

						closeActionProgress();

						if (D.s == 1){
							$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
							showApprovalWatingMsgBox("");
							counter = setInterval(timer, 1000); //1000 will  run it every 1 second
							enableApprovalMonitor(D.nno); // set Request approval monitor
						}else{					
							showMessage("s", D.m );
						}

					},"json");

				}

			}

		}

	}else{
		$("#btnSave").attr('disabled', false);
	}

}

function check_for_manager_approval_allow(requiredamount,goldvalue){

	var allow_amount = parseFloat($("#extra_pawn_manager_allowed_amount").val());

	if ( (requiredamount - goldvalue) > allow_amount ){
		return false;
	}else{
		return true;
	}

}


function saveFront(){	

	showActionProgress("Saving...");

	if (validate_form_front()){
		
		$.post("index.php/main/save/m_customer",{

			customer_id :	$("#customer_idF").val(),
			cus_serno :	$("#cus_sernoF").val(),
			hid 		: $("#hid").val(),
			nicno :	$("#nicno").val(),			
			title :	$('input:radio[name=title]:checked').val(),
			cusname :	$("#cusname").val(),
			other_name :	$("#other_name").val(),
			mobile :	$("#mobile").val(),
			address :	$("#address").val(),
			address2 :	$("#address2").val(),
			telNo :	$("#telNo").val()

		},function(D){		

			if (D.s == 1){
				closeActionProgress();
				showMessage("s","Customer saving success");
				$("#customer_id").val(D.customer_id);
				$("#cus_serno").val(D.cus_serno);
				resetFrontCusInputs();
				saveAndHideCusUI();
				getCustomerInfo($("#customer_id").val(),$("#cus_serno").val(), 1 );
				$("#bill_code_desc").focus();
			}else{
				showMessage("e",D.m);
			}


		},"json");

	}

}



function setDelete(recid){

	if (confirm("Do you want delete this doc fee?")){
		showActionProgress("Deleting...");
		$.post("index.php/main/delete/t_new_pawn",{
			recid : recid
		},function(D){		
			$(".list_div").html(D);
			closeActionProgress();
			load_list();
		},"text");
	}
}

function load_list(){
	showActionProgress("Loading...");
	$.post("index.php/main/load/t_new_pawn",{
		s : "load_list"
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function validate_form(){

	if ( rmsps($("#customer_id").val()) ==  "" || $("#cus_serno").val() == "" ){ showMessage("e","Select customer"); $("#customer_id").val("").focus(); return false; }


	if ( $("#cus_monitor").val() == 1 ){

		if ( parseFloat( $("#current_pawning_total").val() ) + parseFloat($("#requiredamount").val()) > parseFloat($("#cus_monitor_max_amount").val()) ){

			setBillStatusMSG("PX");
			$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
			$("#btnReset").attr("disabled",false).attr("class","btn_regular");
			alert("This customer reached maximum allowed pawning amount");
			location.href = "";
			return false;

		}		

	}else{

		if ( parseFloat( $("#current_pawning_total").val() ) + parseFloat($("#requiredamount").val()) > 1000000){

			setBillStatusMSG("PX");
			$("#btnSave").attr("disabled",true).attr("class","btn_regular_disable");
			$("#btnReset").attr("disabled",false).attr("class","btn_regular");
			alert("This customer reached maximum allowed pawning amount");
			location.href = "";
			return false;

		}

	}

	

	if ( rmsps($("#bill_code_desc").val()) ==  "" || $("#billtype").val() == "" ){ showMessage("e","Enter required amount and hit the enter key to load bill type"); 
		//$("#requiredamount").focus(); 		
		var rm = parseFloat($("#requiredamount").val());
		setBTDetails(rm);	
		return false; 
	}
	


	if ( rmsps($("#billno").val()) ==  "" || !validateNumber($("#billno").val()) ){ showMessage("e","Invalid bill nuner"); $("#billno").val("").focus(); return false; }
	
	if ( $("#item_list_holder").html() == "" ) { showMessage("e","No pawn items in the list"); $("#cat_code_desc").focus(); return false; }
	
	if ( rmsps( $("#requiredamount").val() ) == "" ){ 
		showMessage("e","Enter required amount"); $("#requiredamount").focus(); 
		return false; 
	}else{
		if (!validateAmount($("#requiredamount").val())){
			showMessage("e","Invalid required amount value"); 
			$("#requiredamount").focus().select(); 
			return false;
		}
	}
	
	return true;	
}

function validate_form_front(){

	//if ( rmsps($("#customer_idF").val()) ==  "" || $("#cus_sernoF").val() == "" ){ showMessage("e","Select customer"); $("#customer_idF").val("").focus(); return false; }
	
	if (!$('input:radio[name=title]').is(":checked")){ showMessage("e","Select title"); $("#SP454").focus(); return false;}

	gm = $('input:radio[name=title]:checked').val();
	if (gm == "Mr. "){ gm = "m"; }else{ gm = "f"; }
	
	if ($("#customer_id_type :selected").val() == "nic"){		
		if (!validateNIC($("#nicno").val(),gm)){			
			$("#nicno").focus().select();
			return false;
		}
	}

	if ($("#customer_id_type :selected").val() == "pp"){		
		if (!validatePassport($("#nicno").val())){
			$("#nicno").focus().select();
			return false;
		}
	}

	if ($("#customer_id_type :selected").val() == "dl"){		
		if (!validateDrvingLicense($("#nicno").val())){
			$("#nicno").focus().select();
			return false;
		}
	}

	if ($("#customer_id_type :selected").val() == "gm"){		
		if (!validateGSCertifi($("#nicno").val())){
			$("#nicno").focus().select();
			return false;
		}
	}

	if ( rmsps($("#cusname").val()) ==  ""){ showMessage("e","Enter customer full name"); $("#cusname").val("").focus(); return false; }
	
	if ($("#mobile").val() != ""){
		if (!validateMobileno(  $("#mobile").val())){ $("#mobile").focus(); return false;}
	}
	
	if ( rmsps($("#address").val()) ==  ""){ showMessage("e","Enter customer address"); $("#address").val("").focus(); return false; }	
	
	return true;	
}

function setEdit(recid){

	showActionProgress("Loading...");

	$.post("index.php/main/load_data/t_new_pawn/set_edit",{
		recid : recid
	},function(D){		
		$("#hid").val(D.recid);
		$("#btnSave").val("Edit");
		
		$("#bc").val(D.bc);
		$("#fromvalue").val(D.fromvalue);
		$("#tovalue").val(D.tovalue);
		$("#stampfee").val(D.stampfee);
		$("#stampdate").val(D.stampdate);		
		closeActionProgress();
	},"json");	

}


function search(skey){
	showActionProgress("Searching...");
	$.post("index.php/main/load_data/t_new_pawn/search",{
		skey : skey
	},function(D){		
		$(".list_div").html(D);
		closeActionProgress();
	},"text");
}

function setBillStatusMSG(S,data = Array()){

	var t = "";

	if (S == "C"){t = "This bill was canceled"; }
	if (S == "R"){t = "This bill was redeemed"; }
	if (S == "L"){t = "This bill was marked as lost"; }
	if (S == "F"){t = "This bill was forfeited"; }
	if (S == "PO"){t = "This bill was marked as police bill"; }
	if (S == "RN"){t = "This bill was renewed"; }
	if (S == "AM"){t = "This bill was renewed"; }
	if (S == "RB"){t = "R bill type not allow for this branch"; }
	if (S == "MX"){t = "This customer reached maximum allowed pawning count ("+ data.cus_pawn_count+"/"+data.max_allowed_pawn +") "; }
	if (S == "PX"){t = "This customer reached maximum allowed pawning amount "; }

	var T  ='<div class="canceled_bill_msg">';
		T +='<div class="msg_txt" style="padding:10px">'+t+'</div>';
		T +='</div>';

	$(".frnt_msg_hldr").append(T);

	$(".canceled_bill_msg").css("visibility","visible");
	$(".canceled_bill_msg").animate({height: "40px"});


}

function reset_R_values(){
	$("#bill_code_desc,#billtype,#bt_letter").val("");
}