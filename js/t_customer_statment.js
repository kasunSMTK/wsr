$(document).on("click",".cus_bills_0",function(){

	$(".cus_bills_0").each(function(){
		$(this).css("background-color","#ffffff");
		$(this).find('.cus_bills_a').css("color","Green");
		$(this).find('.cus_bills_b').css("color","#000000");
		$(this).find('.cus_bills_c').css("color","#666666");
	});

	$(this).css("background-color","Green");
	$(this).find('.cus_bills_a,.cus_bills_b,.cus_bills_c').css("color","#ffffff");

});


$(document).keypress(function(e){
	if (e.keyCode == 27){
		$(".cus_total_pawn_amount").html("00.00");
		$(".search_bg,.search_inner").css("visibility","hidden");		
	}
});

$(document).on("click",".cus_selected_row",function(){
	get_customer_bill_details($(this).attr("nic"));
});

function validate_customer_search(){
	var input_exist = false;
		
	$(".search_input").find('input[type="text"]').each(function(){
		if ($(this).val() != ""){
			input_exist = true;
		}
	});

	if (input_exist){
		return true;
	}else{
		return false;
	}
}

function set_button_bg(billno){

	$(".cus_bills_a").each(function(){
		
		if ($(this).html() == billno){
			$(this).parent().css("background-color","Green");
			$(this).parent().find('.cus_bills_a,.cus_bills_b,.cus_bills_c').css("color","#ffffff");
		}else{
			$(this).parent().css("background-color","#ffffff");
			$(this).parent().find('.cus_bills_a').css("color","Green");
			$(this).parent().find('.cus_bills_b').css("color","#000000");
			$(this).parent().find('.cus_bills_c').css("color","#666666");
		}

	});

}



function get_bill_statment(billno,customer_id,billtype){

	showActionProgress("Please wait...");
	set_button_bg(billno);
	
	$.post("index.php/main/load_data/t_customer_statment/get_bill_statment",{
		billtype : billtype,
		billno : billno,
		customer_id : customer_id
	},function(D){
		closeActionProgress();
		$("html, body").animate({ scrollTop: 0 }, "slow");


			var T = "";

			// if (D.sum.status == "P"){
			// 	var int_bal = D.int.payable_tot_int_balance;
			// }else{
			// 	var int_bal = (0).toFixed(2);
			// }
			//
			// var paid_int = parseFloat(D.int.paid_int);

			if (D.sum.pawningStatus == "P"){
				var int_bal = D.int.payable_tot_int_balance;
			}else{
				var int_bal = (0).toFixed(2);
			}

			var paid_int = parseFloat(D.int.paid_int);

			T += '<br><br><table border="0" class="tbl_cus_his_sum" width="100%"> <!-- <tr> <td> <div class="cus_his_cus_title"></div> <div class="cus_his_cus_val EEE"></div> </td> <td> <div class="cus_his_cus_title"></div> <div class="cus_his_cus_val FFF"></div> </td> <td> <div class="cus_his_cus_title"></div> <div class="cus_his_cus_val GGG"></div> </td> <td> <div class="cus_his_cus_title"></div> <div class="cus_his_cus_val HHH"></div> </td> </tr> --><tr> <td> <div class="cus_his_cus_title">Paid Interest</div> <div class="cus_his_cus_val AAA">'+paid_int.toFixed(2)+'</div> </td> <td> <div class="cus_his_cus_title">Interest Paid Up to</div> <div class="cus_his_cus_val BBB">'+D.sum.int_paid_untill_new+'</div> </td> <td> <div class="cus_his_cus_title">Interest Balance</div> <div class="cus_his_cus_val CCC">'+int_bal+'</div> </td> <td> <div class="cus_his_cus_title">Advance Balance</div> <div class="cus_his_cus_val DDD">'+D.customer_advance.balance+'</div> </td> </tr> </table><br><br>';
		
			T += "Payments and Receipts Transactions<br><br>";
			T += "<table class='tbl_cus_statment'>";
			T += "<tr>";
			T += "<th align='left'>Transaction Type</th>";
			T += "<th align='right'>Amount</th>";
			T += "<th align='right'>Date Time</th>";			
			T += "</tr>";

			for(n=0;n<D.det.length;n++){
				T += "<tr>";				
				T += "<td>"+D.det[n].trans_code+"</td>";
				T += "<td align='right'>"+D.det[n].amount+"</td>";
				T += "<td align='right'>"+D.det[n].action_date+"</td>";
				T += "</tr>";
			}



			var previous_bill = "";

			if (D.previous_bill != null){				
				previous_bill = "<div class='div_prev_bill'><a onClick=get_bill_statment('"+D.previous_bill.billno+"','"+D.sum.customer_id+"') class='div_prev_bill_a'>Previous Bill Number "+D.previous_bill.billno+"</a></div>";				
			}			

			var next_bill = "";

			if (D.next_bill != null){				
				next_bill = "<div class='div_next_bill'><a  onClick=get_bill_statment('"+D.next_bill.billno+"','"+D.sum.customer_id+"') class='div_prev_bill_a'>Next bill Number "+D.next_bill.billno+"</a></div>";				
			}			

			
			//T += "<tr><td align='left'>"+previous_bill+"<td><td colspan='2' align=right> "+next_bill+"</td></tr>";
			T += "<tr><td align='left'><td><td colspan='2' align=right></td></tr>";
			
			T += "</table>";

		$(".selected_bill_details").html(T);
		

	},"json");

}

$(document).ready(function(){

	//setTimeout('$("#btnCusDET").click()',1) // remove this section

	$(".search_bg,.close_cus_his_window").click(function(){
		$(".cus_total_pawn_amount").html("00.00");
		$(".search_bg,.search_inner").css("visibility","hidden");
	});

	$(".search_input").find('input[type="text"]').keypress(function(e){		
		if (e.keyCode == 13){
			$("#btnCusDET").click();
		}
	});

	$('#bc').css({
		"font-size":"14px",
		"padding" : "5px",
		"border":"1px solid #cccccc",
		"width" : "254px",
		"margin-bottom" : "10px",
		"margin-top" : "4px"
	});

	$("#btnCusDET").click(function(){
		if (validate_customer_search()){
			showActionProgress("Searching...");			
			$.post("index.php/main/load_data/t_customer_statment/cus_det",{
				bc : $("#bc").val(),				
				aa : $("#aa").val(),
				bb : $("#bb").val(),
				cc : $("#cc").val(),
				dd : $("#dd").val(),
				ee : $("#ee").val(),
				ff : $("#ff").val()
			},function(D){		
				$(".search_result").html(D.data);
				closeActionProgress();
			},"json");
		
		}else{
			alert("Please enter search criteria");			
		}
	});

});

function get_customer_bill_details(cus_ser_no){

	$(".customer_bill_list").html("00.00");

	showActionProgress("Loading...");			

	$.post("index.php/main/load_data/t_customer_statment/get_customer_bill_details",{		
		cus_ser_no : cus_ser_no
	},function(D){
		closeActionProgress();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$(".search_bg,.search_inner").css("visibility","visible");
		$(".search_customer_name").html(D.customer_data.cusname);
		$(".search_customer_address").html(D.customer_data.address);
		$(".search_customer_nic").html(D.customer_data.nicno);
		$(".search_customer_contact").html(D.customer_data.contact);		

		//-----------------------------------------------------------

		if (D.cus_bills != 0){


			$(".cus_bill_list_msg").html('<br><table> <tr> <td valign="top"> Customers Pawning Bill(s) <br><br> <div class="customer_bill_list"></div> </td> <td valign="top"> <div class="selected_bill_details"></div> </td> </tr> </table>');


			var T = "";
			var tot_pawn_amt = 0;

			for(n = 0 ; n < D.cus_bills.length ; n++){

				T += "<div class='cus_bills_0' onClick=get_bill_statment('"+D.cus_bills[n].billno+"','"+D.customer_data.customer_id+"','"+D.cus_bills[n].billtype+"')>";
				T += "<div class='cus_bills_a'>"+D.cus_bills[n].billno+"</div>";
				T += "<div class='cus_bills_b'>"+D.cus_bills[n].ddate+"</div>";
				T += "<div class='cus_bills_c'>"+D.cus_bills[n].requiredamount+"</div>";
				T += "</div>";

				if ( D.cus_bills[n].count_for_pawn_value == 1 ){
					tot_pawn_amt += parseFloat(D.cus_bills[n].requiredamount);
				}
			}		

			$(".cus_total_pawn_amount").html(tot_pawn_amt.toFixed(2));
			$(".customer_bill_list").html(T);

		}else{
			$(".cus_bill_list_msg").html("<div class='cus_bill_not_found'>Data not found</div>");
		}


	},"json");

}

