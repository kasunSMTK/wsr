var is_edit=0;
$(document).ready(function(){

    $("#sbank").focus();
  
    load_list();

    $("#btnSave").click(function(){     
        save();
    }); 
    $("#code").blur(function(){
        check_code();
    });

    $("#btnSearch").click(function(){
        search($("#Search_bankname").val());
    });



    $("#code").keyup(function(){
        if($("#sbank").val()!=""){
            if($("#code_").val()=="0"){
                 var code ="";
                 code = $("#b_branch_code").val()+"-"+$(this).val();
                 $("#branch_code").val(code);
            }           
        }else{
            showMessage("e","Please select bank first");
            closeActionProgress();
        }
    });

  

    $("#sbank").change(function(){
        set_select('bank','bank')
    });

    $("#sbank").keypress(function(e){
    if(e.keyCode == 112){
        $("#pop_search4").val($("#sbank").val());
        load_bankf1();
        center("#serch_pop4");
        $("#blocker").css("display", "block");
        setTimeout("$('#pop_search4').focus()", 100);
    }

   $("#pop_search4").keyup(function(e){
        if(e.keyCode != 13 && e.keyCode != 38 && e.keyCode != 40 && e.keyCode != 112 ) { 
             load_bankf1();

        }
    }); 

    if(e.keyCode == 46){
        $("#sbank").val("");
        $("#bank_des").val("");
    }
  });


    
    $("#sbank").blur(function(){
        set_Bank_values($(this));
    });
    
    $("#sbank").keypress(function(e){
        if(e.keyCode == 13){
            set_Bank_values($(this));
        }
    });

});

function load_bankf1(){
      $.post("index.php/main/load_data/utility/f1_selection_list", {
          data_tbl:"m_bank",
          field:"code",
          field2:"description",
          preview2:"Bank Name",
          search : $("#pop_search4").val() 
      }, 
      function(r){
          $("#sr4").html(r);
          settings_bankf1();            
      }, "text");
  }

  function settings_bankf1(){
      $("#item_list .cl").click(
        function(){        
          $("#sbank").val($(this).children().eq(0).html());
          $("#bank_des").val($(this).children().eq(1).html());
          $("#b_branch_code").val($(this).children().eq(0).html());
          $("#branch_code").val($(this).children().eq(0).html());
          $("#pop_close4").click();                
      })    
  }

function set_Bank_values(f){
    var v = f.val();
    v = v.split("~");
    if(v.length == 2){
        f.val(v[0]);
        $("#bank").val(v[0]);
        $("#bank_des").val(v[1]);
        $("#sbank").attr("class", "input_txt_f");

        $("#b_branch_code").val(v[0]);
        $("#branch_code").val(v[0]);
    }
}

function formatbtype(row){
    return "<strong> " +row[0] + "</strong> | <strong> " +row[1] + "</strong>";
}

function formatbtypeResult(row){
    return row[0]+"~"+row[1];
}

function check_delete_permission(code){

        if(confirm("Are you sure delete "+code+"?")){
        loding();
        $.post("index.php/main/delete/m_bank_branch", {
            code : code
        }, function(res){
            if(res == 1){
                loding();
                delete_msg();
            }else{
                set_msg("Item deleting fail.");
            }
            
        }, "text");
    }
}

// function save(){
//     var frm = $('#form_');
//     loding();
//     $.ajax({
//     type: frm.attr('method'),
//     url: frm.attr('action'),
//     data: frm.serialize(),
//     success: function (pid){
//             if(pid == 1){
//                 loding();
//                 sucess_msg();
//             }else if(pid == 2){
//                 set_msg("No permission to add data.");
//             }else if(pid == 3){
//                 set_msg("No permission to edit data.");
//             }else{
//                 set_msg("Error : \n"+pid);
//             }
            
//         }
//     });
// }

function save(){    

    showActionProgress("Saving...");

    if (validate_form()){
        var frm = $('#form_');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: "json",
            success: function (D) {

                closeActionProgress();

                if (D == 1){
                    showMessage("s","Branch saving success");
                    clear_form();
                    load_list();                    
                }else if (D == 2){                  
                    showMessage("e","Error, Duplicate entry");
                }else{                  
                    showMessage("e","Error, save failed");
                }
            }
        });

    }

}


function check_code(){
    var code = $("#branch_code").val();
    $.post("index.php/main/load_data/m_bank_branch/check_code", {
        code : code
    }, function(res){
        if(res == 1){
            if(confirm("This code ("+code+") already added. \n\n Do you need edit it?")){
                 setEdit(code);
            }else{
                $("#code").val('');
            }
        
        }
    }, "text");
}

function setDelete(code){

    if (confirm("Do you want delete this Bank?")){
        showActionProgress("Deleting...");
        $.post("index.php/main/delete/m_bank_branch",{
            code : code
        },function(D){
            $(".list_div").html(D);
            closeActionProgress();
            load_list();
        },"text");
    }
}


function validate_form(){
    if ($("#sbank").val() == "0"){showMessage("e","Please Select Bank");    return false; }
    if ($("#code").val() == ""){showMessage("e","PleaseEnter Code");    return false; }
    if ($("#description").val() == ""){showMessage("e"," Please Enter Description");  return false; }
    return true;    
}

function clear_form(){
    $("input").val("");
    $("#btnSave").val("Save");  
    $('#btnSearch').val("Search");
    $("#code_").val(0);
}

    
function set_delete(code){
 check_delete_permission(code);
}
    
function setEdit(code){
    showActionProgress("Loading...");
    $("#code").attr("readonly","readonly");
    $("#sbank").attr("readonly","readonly");
    
    $.post("index.php/main/load_data/m_bank_branch/set_edit",{
        code : code
    }, function(res){

        $("#code").val(res.branch_code);
        $("#sbank").val(res.bank);
        $("#bank").val(res.bank);
        $("#bank_des").val(res.bank_des);
        $("#description").val(res.description);
        $("#code_").val(res.code);
        $("#branch_code").val(res.code);
         
        //set_select('bank','bank');
        //loding(); 
        input_active();
        is_edit=1;
        closeActionProgress();

    }, "json");
}

function load_list(){
    showActionProgress("Loading...");
    $.post("index.php/main/load/m_bank_branch",{
        s : "load_list"
    },function(D){      
        $(".list_div").html(D.T);
        $("#bc").val(D.max_no);
        closeActionProgress();
    },"json");
}


function search(skey){
    showActionProgress("Searching...");
    $.post("index.php/main/load_data/m_bank_branch/search",{
        skey : skey
    },function(D){      
        $(".list_div").html(D);
        closeActionProgress();
    },"text");
}